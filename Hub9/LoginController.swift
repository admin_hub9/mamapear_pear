//
//  LoginController.swift
//  Hub9
//
//  Created by Deepak on 6/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import Alamofire
import Sentry
import CountryList
import CoreLocation




var loginView = LoginController()

class LoginController: UIViewController, GIDSignInDelegate, UITextFieldDelegate, CountryListDelegate, UITextViewDelegate, CLLocationManagerDelegate  {
    
    var window: UIWindow?
    let native = UserDefaults.standard
    var alamoFireManager : SessionManager?
    //location
    let locationManager = CLLocationManager()
    let geoCoder = CLGeocoder()

    //
    var countryList = CountryList()
    var passcode = 1
    var countrycode = 231
    var countryString = "US"
    var dict : [String : AnyObject]!
    var exists = false
    var FBname: String = String()
    var FBid: String = String()
    var FBemail: String = String()
    var FBImage: Data = Data()
    var fbEmailToUsernameuserName: String? //store fb username (username is email provided by FB)
    var fbIdToPassword: String?    //store fb password (password is ID Token provided by FB)
    var screenSize: CGRect?
    var result = [AnyObject]()
    var useremail = ""
    var userpassword = ""
    var iconClick = true
    var sentFromLogin = false
    var username = ""
    var lastname = ""
    var emailid = ""
    var pass = ""
    var socialLogin = false
    var emailsignup = false
    
    
    @IBOutlet var EmailorPhone: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var signIn: UIButton!
    @IBOutlet var facebook: UIButton!
    @IBOutlet var googlePlus: UIButton!
    
    @IBOutlet weak var passwordIcon: UIButton!
    @IBOutlet weak var selectCountryCode: UIButton!
    
    
    @IBAction func termsConditions(_ sender: Any) {
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "termsAndCondition"))! as UIViewController
        self.present(rootPage, animated: false, completion: nil)
    }
    
    
    
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func showandHidePassword(_ sender: Any) {
        if(iconClick == true) {
            password.isSecureTextEntry = false
            passwordIcon.setImage(UIImage(named: "showpassword"), for: .normal)
        }
        else
        {
            password.isSecureTextEntry = true
            passwordIcon.setImage(UIImage(named: "hidepassword"), for: .normal)
        }
        
        let tempString = password.text
        password.text = nil
        password.text = tempString
        iconClick = !iconClick
    }
    
    ///country code
    func selectedCountry(country: Country) {
        print(country.name)
        print(country.flag)
        print(country.countryCode)
        print(country.phoneExtension)
        self.countryString = country.countryCode
        
        country_lkpid(countryCode: country.name!)
        
    }
    //country lkpid
    func country_lkpid(countryCode: String)
    {
        if let urlPath = Bundle.main.url(forResource: "country", withExtension: "json") {
            
            do {
                let jsonData = try Data(contentsOf: urlPath, options: .mappedIfSafe)
                
                if let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: AnyObject] {
                    
                    if let personArray = jsonDict["response"] as? [[String: AnyObject]] {
//                        print("responce", personArray)
                        for personDict in personArray {
                            
                            if countryCode == personDict["country_name"] as! String
                            {
//                                print(personDict["country_lkpid"]!)
                                countrycode = Int((personDict["country_lkpid"] as! NSNumber ).floatValue)
                                var phonecode = Int(personDict["phone_code"] as! String)
                                selectCountryCode.setTitle("\(countryString) + \(phonecode!)", for: .normal)
                                selectCountryCode.setTitleColor(UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1), for: .normal)
                                passcode = phonecode!
                                break
                            }
                        }
                    }
                }
            }
                
            catch let jsonError {
                print(jsonError)
            }
        }
    }
    /////keybpard hide and show
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        print(textField.text)
        if (textField.text?.count)! > 0
        {
        
        if  validate(phoneNumber:  textField.text!) == true
        {
            selectCountryCode.isHidden = false
            self.emailsignup = false
            let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: selectCountryCode.frame.width + 5, height: 26))
            leftView.backgroundColor = .clear
            
            EmailorPhone.leftView = leftView
            EmailorPhone.leftViewMode = .always
            var frame: CGRect = EmailorPhone.frame
            frame.size.height = 40
            EmailorPhone.frame = frame
            EmailorPhone.layer.masksToBounds = true
            
        }
        else
        {
            self.emailsignup = true
            selectCountryCode.isHidden = true
            let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
            leftView.backgroundColor = .clear
            
            EmailorPhone.leftView = leftView
            EmailorPhone.leftViewMode = .always
            var frame: CGRect = EmailorPhone.frame
            frame.size.height = 40
            EmailorPhone.frame = frame
            EmailorPhone.layer.masksToBounds = true
        }
        }
        else
        {
            self.emailsignup = true
            selectCountryCode.isHidden = true
            let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
            leftView.backgroundColor = .clear
            
            EmailorPhone.leftView = leftView
            EmailorPhone.leftViewMode = .always
            var frame: CGRect = EmailorPhone.frame
            frame.size.height = 40
            EmailorPhone.frame = frame
            EmailorPhone.layer.masksToBounds = true
        }
        
    }
    
    @IBAction func SelectCountryButton(_ sender: Any) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    
    @IBAction func forgotPassword(_ sender: Any) {
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "sendNumber"))! as UIViewController
        self.present(rootPage, animated: false, completion: nil)
    }
    
    
    @IBAction func gmailLogin(_ sender: Any) {
    GIDSignIn.sharedInstance().delegate=self
    GIDSignIn.sharedInstance()?.presentingViewController = self
    GIDSignIn.sharedInstance().signIn()
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        //if any error stop and print the error
        if error != nil{
            print(error ?? "google error")
            return
        }
        else
        {
            print("successfully Login")
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            print("username", fullName)
            print("userId", userId!)
            print("idToken", idToken)
            print("email", email)
            print("given name", givenName, user)
            print(user.profile.email!, userId!)
            native.set(user.profile.email!, forKey: "username")
            native.set(userId!, forKey: "password")
            native.synchronize()
            CustomLoader.instance.showLoaderView()
            ThirdPartyLogin( email: user.profile.email!, password: userId!, firstname: givenName!, lastname: "")
        }
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        EmailorPhone.resignFirstResponder()
        password.resignFirstResponder()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y == 0{
                
                self.view.frame.origin.y -= 90
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += 90
            }

        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.first else { return }

        geoCoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
            guard let currentLocPlacemark = placemarks?.first else { return }
            print(currentLocPlacemark.country ?? "No country found")
            print(currentLocPlacemark.isoCountryCode ?? "No country code found")
            if currentLocPlacemark.country! != nil
            {
            self.country_lkpid(countryCode: currentLocPlacemark.country!)
            }
        }
        
    }
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.startMonitoringSignificantLocationChanges()
        }
        loginView = self
        selectCountryCode.setTitle("+ \(passcode)", for: .normal)
        selectCountryCode.setTitleColor(UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1), for: .normal)
        selectCountryCode.layer.cornerRadius = selectCountryCode.layer.frame.size.height/2
        selectCountryCode.layer.borderWidth = 1
        selectCountryCode.layer.borderColor = UIColor.gray.cgColor
        selectCountryCode.clipsToBounds = true
//        selectCountryCode.isHidden = true
        EmailorPhone.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        countryList.delegate = self
        native.set(0, forKey: "appversion")
        native.synchronize()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    
        EmailorPhone.delegate = self
        password.delegate = self
        CustomLoader.instance.hideLoaderView()
        signIn.layer.cornerRadius = signIn.layer.frame.size.height/2
        signIn.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 239/255.0, green: 130/255.0, blue: 136/255.0, alpha: 1.00).cgColor
        
//        signIn.applyendGradient(colors: [loginstartColor, loginendColor])
        let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
        leftView.backgroundColor = .clear
        EmailorPhone.leftView = leftView
        EmailorPhone.leftViewMode = .always
        EmailorPhone.layer.borderWidth = 1
        EmailorPhone.layer.borderColor = UIColor.lightGray.cgColor
        EmailorPhone.layer.cornerRadius = EmailorPhone.layer.frame.size.height/2
        EmailorPhone.placeholder = "Enter Email or phone Number"
        EmailorPhone.layer.masksToBounds = true
        // password
        let passwordleftView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 26))
        passwordleftView.backgroundColor = .clear
        let passwordrightView = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 26))
        passwordrightView.backgroundColor = .clear
        password.rightView = passwordrightView
        password.rightViewMode = .always
        password.leftView = passwordleftView
        password.leftViewMode = .always
        var passwordframe: CGRect = password.frame
        passwordframe.size.height = 40
        password.frame = passwordframe
        password.layer.cornerRadius = password.layer.frame.height/2
        password.layer.borderWidth = 1
        password.layer.borderColor = UIColor.lightGray.cgColor
        password.layer.masksToBounds = true
        self.emailsignup = true
        self.selectCountryCode.isHidden = true
//        EmailorPhone.addDoneButtonOnKeyboard()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func showandhisePassword(_ sender: Any) {
    }
    
    
    
    @IBAction func loginWithFacebook(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
    
        fbLoginManager.logIn(withReadPermissions: ["email","user_gender", "user_birthday"], from: self) { (result, error) in
            
            //if error just kick out the user
            if(error != nil )
            {
                print("Error Logging In\n")
                print(error?.localizedDescription)
                return
            }
            
            //if no error from fbLoginManager
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                // print(fbloginresult.grantedPermissions)
                
                //check user loged in or not
                if let fbUserToken = result?.token{
                    
                    //if user login with correct email
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        
                        self.getFBUserData() //call function for get acces of email, name first name, last name & profile pic
                    
                        //self.facebookForRealm(self.FBemail, id: self.FBid, name: self.FBname) // Facebook login/check/save for realm
                        
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    //#MARK: Fetch Facebook Data
    func getFBUserData() {
        facebook.isHidden = true
        //        loginActivityIndicator.startAnimating()
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name,birthday,gender, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    print("FB GraphRequest Result: \(result)")
                    let fbResult : [String:Any]? = result as? [String:Any]
                    print("fbResult", fbResult)
                    if fbResult != nil {
                        
                    }
//                    print("the access token is \(FBSDKAccessToken.current().tokenString)")

                    DispatchQueue.main.async(execute: {     // Performing actions on main queue
                        
                        // saving fetched results into intermediate variable
//                        print("gender", fbResult!["gender"] as! NSString, fbResult!["birthday"] as! NSString)
                        self.FBid = (fbResult!["id"] as! NSString) as String
                        self.FBname = (fbResult!["name"] as! NSString) as String
                        self.FBemail = (fbResult!["email"] as! NSString) as String
//                        self.FBImage = self.getProfPic(self.FBid)
                        print("dfsewfw",self.FBemail, self.FBid)
                        self.native.set((fbResult!["email"] as! NSString) as String, forKey: "username")
                        self.native.set((fbResult!["id"] as! NSString) as String, forKey: "password")
                        self.native.synchronize()
                        CustomLoader.instance.showLoaderView()
                        self.ThirdPartyLogin(email: (fbResult!["email"] as! NSString) as String, password: (fbResult!["id"] as! NSString) as String, firstname: (fbResult!["name"] as! NSString) as String, lastname: (fbResult!["last_name"] as! NSString) as String)
                        
                    })
                    self.facebook.isHidden = false
                }
            })
        }
        
    }
    // #MARK: Fetch facebook profile picture
    func getProfPic(_ fid: String) -> UIImage? {
        if (fid != "") {
            let imgURLString = "http://graph.facebook.com/" + fid + "/picture?type=large" //type=normal
            let imgURL = URL(string: imgURLString)
            let imageData = try? Data(contentsOf: imgURL!)
            let image = UIImage(data: imageData!)
            
            return image
        }
        return nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func loginButton(_ sender: Any) {
        if CheckInternet.Connection(){
            CustomLoader.instance.showLoaderView()
            loginUser()
            
            
        }
            
        else{
            
            self.Alert(Message: "Your Device is not connected with internet")
            
        }
    }
    
    ///check internet on view load
    override func viewDidAppear(_ animated: Bool) {
        
//        if CheckInternet.Connection(){
//
//            self.Alert(Message: "Connected")
//
//        }
//
//        else{
//
//            self.Alert(Message: "Your Device is not connected with internet")
//        }
        
    }
    
  ///check connection alert
    func Alert (Message: String){
        self.view.makeToast(Message, duration: 2.0, position: .top)
    }
   //remove special character from string
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    
 func loginUser()
  {
 if (EmailorPhone.text?.count)! > 0 && (password.text?.count)! > 0
  {
     CustomLoader.instance.showLoaderView()
     CustomLoader.instance.showLoaderView()
     var Pushtoken = ""
     if native.string(forKey: "B2BTokenForSNS") != nil
     {
         Pushtoken = native.string(forKey: "B2BTokenForSNS")!
     }
     let deviceId = UIDevice.current.identifierForVendor?.uuidString
 //    print(deviceId!)
 //    let country_code = native.string(forKey: "country_code")!
     var devicename = UIDevice.current.name
     devicename = removeSpecialCharsFromString(text: devicename)
     self.socialLogin=false
     let deviceinfo:[String:Any] = ["DeviceName": devicename, "DeviceModel": UIDevice.current.modelName, "DeviceVersion": UIDevice.current.systemVersion]
     let parameters: [String:Any]
     if self.emailsignup == true
     {
         parameters = [ "username":EmailorPhone.text!, "password":password.text!, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_notification_token": Pushtoken, "country_code": passcode, "signin_type": "Email"]
     }
     else
     {
         parameters = [ "username":EmailorPhone.text!, "password":password.text!, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_notification_token": Pushtoken, "country_code": passcode, "signin_type": "Phone"]
     }
     native.set(countrycode, forKey: "countryid")
     native.synchronize()
     let header = ["Content-Type": "application/json"]
     print("Successfully post---", parameters)
     CustomLoader.instance.showLoaderView()
     //
     let configuration = URLSessionConfiguration.default
     configuration.timeoutIntervalForRequest = 20
     configuration.timeoutIntervalForResource = 20
     alamoFireManager = Alamofire.SessionManager(configuration: configuration)
     //
     let b2burl = native.string(forKey: "b2burl")!
     
     alamoFireManager!.request("\(b2burl)/users/login_user/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
 //        print(response)
         guard response.data != nil else { // can check byte count instead
             CustomLoader.instance.hideLoaderView()
             UIApplication.shared.endIgnoringInteractionEvents()
             self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
             
             UIApplication.shared.endIgnoringInteractionEvents()
             return
         }
         
         switch (response.result) {
             
         case .success:
             //Success....
             
             if let json = response.result.value {
                 print("JSON: \(json)")
                 let result = response.result
 //                print("responce login", response)
                 CustomLoader.instance.hideLoaderView()
             UIApplication.shared.endIgnoringInteractionEvents()
                 if let dict1 = result.value as? Dictionary<String, AnyObject>{
                     if let invalidToken = dict1["detail"]{
                         if invalidToken as! String  == "Invalid token."
                         { // Create the alert controller
                             self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                             let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                             let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                             let appDelegate = UIApplication.shared.delegate as! AppDelegate
                             appDelegate.window?.rootViewController = redViewController
                         }
                     }
                     if (dict1["status"]as AnyObject) as! String  == "SUCCESS" || (dict1["status"]as AnyObject) as! String  == "success"
                     {   print("success")
 //                        self.blurView.alpha = 0
                         CustomLoader.instance.hideLoaderView()
                         
                         
                         self.native.set(dict1["token"], forKey: "Token")
                         self.native.synchronize()
                         let innerdict = dict1["user_data"] as AnyObject
                         self.native.set((innerdict["account_typeid"]!), forKey: "usertype")
                         self.native.set(innerdict["firebase_userid"]! as? String, forKey: "firebaseid")
                         self.native.set(innerdict["userid"]!, forKey: "userid")
                         if innerdict["notification_active"] is NSNull
                         {
                             self.native.set("0", forKey: "notification")
                         }
                         else
                         {
                         self.native.set(innerdict["notification_active"], forKey: "notification")
                         }
                         
                         if innerdict["user_purchase_modeid"] is NSNull
                         {
                             self.native.set("1", forKey: "user_purchase_modeid")
                         }
                         else
                         {
                             self.native.set(innerdict["user_purchase_modeid"], forKey: "user_purchase_modeid")
                         }
                         if innerdict["phone"] is NSNull
                         {
                         if innerdict["email"] is NSNull
                         {
                         self.native.set("", forKey: "phone")
                         }
                         else
                         {
                            self.native.set(innerdict["email"], forKey: "phone")
                         }
                         }
                         else
                         {
                            self.native.set(innerdict["phone"], forKey: "phone")
                         }
                         if innerdict["countryid"] is NSNull
                         {
                             self.native.set("", forKey: "countryid")
                         }
                         else
                         {
                         self.native.set(innerdict["countryid"]!, forKey: "countryid")
                         }
                         if innerdict["support_firebaseid"] is NSNull
                         {
                             self.native.set("", forKey: "support_firebaseid")
                         }
                         else
                         {
                             self.native.set(innerdict["support_firebaseid"], forKey: "support_firebaseid")
                         }
                         if innerdict["country_code"] is NSNull
                         {
                             self.native.set("", forKey: "country_code")
                         }
                         else
                         {
                             self.native.set(innerdict["country_code"], forKey: "country_code")
                         }
                         
                         
                         if innerdict["support_name"] is NSNull
                         {
                             self.native.set("", forKey: "support_name")
                         }
                         else
                         {
                             self.native.set(innerdict["support_name"], forKey: "support_name")
                         }
                         if innerdict["first_name"] is NSNull
                         {
                             self.native.set("", forKey: "firstname")
                         }
                         else
                         {
                             self.native.set(innerdict["first_name"]! as? String, forKey: "firstname")
                         }
                         if innerdict["last_name"] is NSNull
                         {
                             self.native.set(" ", forKey: "lastname")
                         }
                         else
                         {
                             self.native.set(innerdict["last_name"]! as? String, forKey: "lastname")
                         }
                         if innerdict["profile_pic_url"] is NSNull
                         {
                             self.native.set("", forKey: "profilepic")
                         }
                         else
                         {
                             self.native.set(innerdict["profile_pic_url"]! as? String, forKey: "profilepic")
                         }
                         self.native.synchronize()
                         if innerdict["is_user_address"] is NSNull
                         {}
                         else
                         {
                             if (innerdict["is_user_address"] as? Int)! == 0
                             {
                             self.native.set("false", forKey: "useraddress")
                             }
                             else
                             {
                              self.native.set("true", forKey: "useraddress")
                             }
                         }
                         if innerdict["profile_pic_url"] as? String != nil
                         {
                             self.native.set(innerdict["profile_pic_url"]! as? String, forKey: "profilepic")
                         }
                         else
                         {
                             self.native.set("", forKey: "profilepic")
                         }
                         self.native.synchronize()
                         print(innerdict)
 //                        print("completeprofile",innerdict["is_user_updated"] as! Int )
 //                        if innerdict["is_user_updated"] as! Int == 1
 //                        {
                             let usertype = self.native.string(forKey: "usertype")!
                             if usertype == "3" || usertype == "1"
                             {
                                 self.native.set(1, forKey: "changetype")
                                 self.native.synchronize()
                             }
                             else
                             {
                                 self.native.set(2, forKey: "changetype")
                                 self.native.synchronize()
                             }
                        if innerdict["is_user_updated"] is NSNull
                        {
                            if innerdict["is_user_updated"] as! String == "false"{
                                self.native.set(innerdict["userid"]! as? Int, forKey: "userid")
                                self.native.synchronize()
                                self.sentFromLogin = true
                                let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "InstructionScreen"))! as UIViewController
                                self.present(rootPage, animated: false, completion: nil)
                            } else {
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                           
                        }
                        else
                        {
//                            self.native.set(innerdict["userid"]! as? Int, forKey: "userid")
//                                                           self.native.synchronize()
//                                                           self.sentFromLogin = true
//                                                           let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "InstructionScreen"))! as UIViewController
//                                                           self.present(rootPage, animated: false, completion: nil)
                            
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                             
 //                        }
 //                        else
 //                        {
 //
 //                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "completeprofile"))! as UIViewController
 //                            self.present(rootPage, animated: false, completion: nil)
 //                        }
                     }
                     else
                     {
 //                        self.blurView.alpha = 0
                         CustomLoader.instance.hideLoaderView()
                         print(dict1["status"] as AnyObject)
                         print("responce",dict1["response"] as AnyObject)
                         let userdata = dict1["userid"] as AnyObject
                         print(userdata)
                         if (dict1["response"] as AnyObject) as! String == "OTP verification pending"
                         {   self.native.set(dict1["userid"]! as AnyObject, forKey: "userid")
                             self.native.synchronize()
                             self.sendUserType()
                             let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "otpController"))! as UIViewController
                             self.present(editPage, animated: false, completion: nil)
                         }
                         self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                         
                     }
                     
                     CustomLoader.instance.hideLoaderView()
                     UIApplication.shared.endIgnoringInteractionEvents()
                     return
                     
                     
                     
                 }
             }
             print("Success")
             
             break
         case .failure(let error):
             // failure...
             CustomLoader.instance.hideLoaderView()
             UIApplication.shared.endIgnoringInteractionEvents()
             if error._code == NSURLErrorTimedOut {
                 //timeout here
                 
                 self.Alert(Message: "Timeout")
 //                self.blurView.alpha = 0
                 CustomLoader.instance.hideLoaderView()
                 UIApplication.shared.endIgnoringInteractionEvents()
             }
             else
             {
               self.Alert(Message: "Unable to reach Server")
 //                self.blurView.alpha = 0
                 CustomLoader.instance.hideLoaderView()
                 UIApplication.shared.endIgnoringInteractionEvents()
             }
             print(error)
             
             break
         }
         
         
         
     }
     }
     else
     {
         CustomLoader.instance.hideLoaderView()
         UIApplication.shared.endIgnoringInteractionEvents()
     self.Alert(Message: "Invalid Number or Password")
 //    self.blurView.alpha = 0
         
     
     }
     
     
     }
    
    
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        print("REGEX")
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
    }
    
    func validation() -> Bool {
        var valid: Bool = true
        if validate(YourEMailAddress: EmailorPhone.text!) == false{
            // change placeholder color to red color for textfield email-id
            EmailorPhone.text?.removeAll()
            EmailorPhone.attributedPlaceholder = NSAttributedString(string: "Invalid Number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if (password.text?.count)! < 6
        {
            password.text?.removeAll()
            password.attributedPlaceholder = NSAttributedString(string: "invalid Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        return valid
    }
    func validate(phoneNumber: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phoneNumber == filtered
    }
    func ThirdPartyLogin(email:String,password:String,firstname:String,lastname:String)
    {
        let b2burl = native.string(forKey: "b2burl")!
        var Pushtoken = ""
        if native.string(forKey: "B2BTokenForSNS") != nil
        {
            Pushtoken = native.string(forKey: "B2BTokenForSNS")!
        }
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        print(deviceId!)
        //    let country_code = native.string(forKey: "country_code")!
        var devicename = UIDevice.current.name
        devicename = removeSpecialCharsFromString(text: devicename)
        self.socialLogin = true
        let deviceinfo:[String:Any] = ["DeviceName": devicename, "DeviceModel": UIDevice.current.modelName, "DeviceVersion": UIDevice.current.systemVersion]
        let parameters: [String:Any] = [ "email":email, "password":password, "first_name":firstname, "last_name":lastname, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_token": Pushtoken]
         username = email
         self.lastname = lastname
         emailid = email
         pass = password
        let header = ["Content-Type": "application/json"]
        print("Successfully post", parameters)
        
        Alamofire.request("\(b2burl)/users/social_signup_data/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            
            guard response.data != nil else { // can check byte count instead
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                
                
                return
            }
            
            switch response.result
            {
            case .failure(let error):
                //                print(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                }
                
            case .success:
            if let json = response.result.value {
                print("JSON social: \(json)")
                var errorval = 0
                let result = response.result
                
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    
                    if dict1["status"] as! String  == "SUCCESS" || dict1["status"] as! String  == "success"
                    {   print("success")
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if dict1["response"]  as! String == "User Created Successfully" || dict1["response"]  as! String == "User created successfully"
                        {
                            print("User created successfully")
                            if dict1["userid"] is NSNull
                            {
                                self.view.makeToast("User not found!", duration: 2.0, position: .top)
                            }
                            else
                            {

                                self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                            self.native.synchronize()
                            self.sentFromLogin = true
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        }
                        if (((dict1["token"]as AnyObject) as? String) != nil)
                        {
//                            self.blurView.alpha = 0
                            CustomLoader.instance.hideLoaderView()
                            let dict2 = (dict1["token"] as? String)!
                            print((dict1["token"] as? String) as Any )
                            
                            self.native.set((dict1["token"] as? String)!, forKey: "Token")
                            let userData = dict1["user_data"] as! NSDictionary
//                            print(userData["firebase_userid"] as? String as Any)
                            if userData["firebase_userid"] is NSNull
                            {
                                errorval = 1
                            }
                            else
                            {
                            self.native.set(userData["firebase_userid"] as? String, forKey: "firebaseid")
                            print("firebase", userData["firebase_userid"] as? String)
                            }
                            if userData["country_code"] is NSNull
                            {
                                self.native.set("", forKey: "country_code")
                            }
                            else
                            {
                                self.native.set(userData["country_code"], forKey: "country_code")
                            }
                            if userData["business_enabled"] is NSNull
                            {
                                self.native.set("0", forKey: "business_enabled")
                            }
                            else
                            {
                                self.native.set(userData["business_enabled"] as? Int, forKey: "business_enabled")
                                print("business_enabled", userData["business_enabled"] as? Int)
                            }
                            if userData["account_typeid"]! is NSNull
                            {
                                errorval = 1
                            }
                            else
                            {
                            self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                
                                print("account type", (userData["account_typeid"]! as? Int)!)}
                            if userData["userid"]! is NSNull && userData["shopid"]! is NSNull &&
                                userData["companyid"]! is NSNull
                            {
                                errorval = 1
                                
                                }
                            if errorval == 0
                            {
                                print("90909", userData["is_user_updated"] as! Int)
                                if userData["is_user_updated"] as! Int == 1
                                {
                            self.native.set((userData["account_typeid"]!), forKey: "usertype")
                            self.native.set(userData["userid"]! as? Int, forKey: "userid")
                            self.native.set(userData["shopid"]! as? String, forKey: "shopid")
                              if userData["notification_active"] is NSNull
                              {
                                self.native.set("0", forKey: "notification")
                              }
                            else
                              {
                            self.native.set(userData["notification_active"], forKey: "notification")
                              }
                                if userData["user_purchase_modeid"] is NSNull
                                {
                                    self.native.set("1", forKey: "user_purchase_modeid")
                                }
                                else
                                {
                                    self.native.set(userData["user_purchase_modeid"], forKey: "user_purchase_modeid")
                                }
                                if userData["phone"] is NSNull
                                {
                                self.native.set("", forKey: "phone")
                                }
                                else
                                {
                                self.native.set(userData["phone"], forKey: "phone")
                                }
//                            self.native.set(userData["companyid"]! as? String, forKey: "companyid")
                                if userData["first_name"] is NSNull
                                {
                                    self.native.set("", forKey: "firstname")
                                }
                                    else
                                {
                                self.native.set(userData["first_name"]! as? String, forKey: "firstname")
                                    }
                                if let lastname = userData["last_name"]
                                {
                                    self.native.set(userData["last_name"]! as? String, forKey: "lastname")
                                }
                                if userData["profile_pic_url"] is NSNull
                                {
                                    self.native.set("", forKey: "profilepic")
                                }
                                else
                                {
                                self.native.set(userData["profile_pic_url"]! as? String, forKey: "profilepic")
                                }
                                let usertype = self.native.string(forKey: "usertype")!
                                if usertype == "3" || usertype == "1"
                                {
                                    self.native.set(1, forKey: "changetype")
                                    print("login by local id........")
                                }
                                else
                                {
                                    self.native.set(2, forKey: "changetype")
                                    self.native.synchronize()
                                }
                                if userData["countryid"]! is NSNull
                                {
                                    self.native.set("0", forKey: "countryid")
                                }
                                else
                                {
                                    self.native.set(userData["countryid"]!, forKey: "countryid")
                                }
                            self.native.synchronize()
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                                }
                    else if userData["is_user_updated"] as! Int == 0
                    {
                                    self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                    self.native.set(userData["firebase_userid"] as? String, forKey: "firebaseid")
                                    self.native.set(userData["userid"]! as? String, forKey: "userid")
                                    if userData["notification_active"] is NSNull
                                    {
                                        self.native.set("0", forKey: "notification")
                                    }
                                    else
                                    {
                                    self.native.set(userData["notification_active"], forKey: "notification")
                                    }
                                    if userData["support_firebaseid"] is NSNull
                                    {
                                        self.native.set("", forKey: "support_firebaseid")
                                    }
                                    else
                                    {
                                        self.native.set(userData["support_firebaseid"], forKey: "support_firebaseid")
                                    }
                                    
                                    if userData["support_profile_pic_url"] is NSNull
                                    {
                                        self.native.set("", forKey: "support_profile_pic_url")
                                    }
                                    else
                                    {
                                        self.native.set(userData["support_profile_pic_url"], forKey: "support_profile_pic_url")
                                    }
                                    if userData["support_name"] is NSNull
                                    {
                                        self.native.set("", forKey: "support_name")
                                    }
                                    else
                                    {
                                        self.native.set(userData["support_name"], forKey: "support_name")
                                    }
                                    
                                    if userData["phone"] is NSNull
                                    {
                                    self.native.set("", forKey: "phone")
                                    }
                                    else
                                    {
                                    self.native.set(userData["phone"], forKey: "phone")
                                    }
                                    let usertype = self.native.string(forKey: "usertype")!
                                    if usertype == "3" || usertype == "1"
                                    {
                                        self.native.set(1, forKey: "changetype")
                                        self.native.synchronize()
                                    }
                                    else
                                    {
                                        self.native.set(2, forKey: "changetype")
                                        self.native.synchronize()
                                    }
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "completeprofile"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                            }
                        
                        else
                        {
//                        self.blurView.alpha = 0
                        CustomLoader.instance.hideLoaderView()
                        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                        self.present(rootPage, animated: false, completion: nil)

                        let innerdict = dict1["user_data"] as! NSDictionary
                        print(innerdict["userid"]!)
                        if innerdict["firebase_userid"]! != nil
                        {
                        print(innerdict["firebase_userid"] as? String)
                                self.native.set(innerdict["firebase_userid"] as? String, forKey: "firebaseid")
                        self.native.set(innerdict["userid"]!, forKey: "userid")
                            }
                        self.native.synchronize()
//                        print(innerdict)
                        }
                        }
                    }
                    
                    else if dict1["status"] as! String != "failure"
                    {
                     
                        print("hdiuhjhb", dict1["response"]  as! String)
//                        self.blurView.alpha = 0
                        CustomLoader.instance.hideLoaderView()
                        
                        if dict1["response"] as! String == "OTP verification pending"
                        {
                            self.sendUserType()
                            self.sentFromLogin = true
                            self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                            self.native.synchronize()
                            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "otpController"))! as UIViewController
                            self.present(editPage, animated: false, completion: nil)
                        }
                            
                        else if dict1["response"]  as! String == "User created successfully"
                        {   self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                            self.native.synchronize()
                            self.sentFromLogin = true
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        else if dict1["response"] as! String == "Contact detail missing."
                        {
                            self.sentFromLogin = true
                            self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                            self.native.synchronize()
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        else{

                            self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)

                    }
                    }
                    else if dict1["status"] as! String == "failure"
                    {
                        
                        if dict1["response"]  as! String == "User updated pending" || dict1["response"]  as! String == "User Updated Pending"
                                                                                            {
                                                                                           print("Yes2")
                                                                                             self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                                                                                self.native.synchronize()
                                                                                                self.sentFromLogin = true
                                                                                                let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "InstructionScreen"))! as UIViewController
                                                                                                self.present(rootPage, animated: false, completion: nil)
                                                                                            }
                        
                        if  dict1["response"]  as! String == "Contact detail missing."
                        {
                            self.sentFromLogin = true
                            self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                            self.native.synchronize()
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        else if dict1["response"] as! String == "OTP verification pending"
                        {
                            
                            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "otpController"))! as UIViewController
                            self.present(editPage, animated: false, completion: nil)
                        }
                   
                    else
                    {
                        self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                        }
                    }
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                    
                }
            }
            
        }
        }
    }
    
    //send otp fro user
    func sendUserType()
    {
        if EmailorPhone.text!.count>=9 && password.text!.count<14
        {
            var userCode = 1
        native.set(userCode, forKey: "changetype")
        let userid = native.string(forKey: "userid")!
        native.set(EmailorPhone.text!, forKey: "username")
         native.set(EmailorPhone.text!, forKey: "phone")
        native.set(password.text!, forKey: "password")
        native.set(passcode, forKey: "country_code")
        native.set(countrycode, forKey: "countryid")
        native.set(userCode, forKey: "usertype")
        native.set(passcode, forKey: "passcode")
        native.synchronize()
        let b2burl = native.string(forKey: "b2burl")!
        let parameters: [String:Any] = ["phone": EmailorPhone.text!,"country_code":String(passcode), "account_typeid":userCode,"userid": userid, "countryid": countrycode]
        let header = ["Content-Type": "application/json"]
        print("Successfully post", parameters)
        
        Alamofire.request("\(b2burl)/users/sendOtpVerification/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response)
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            if let json = response.result.value {
                print("JSON: \(json)")
                let result = response.result
                
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if (dict1["status"]as AnyObject) as! String  == "success"
                    {
//                        self.blurView.alpha = 0
//                        self.activity.stopAnimating()
                        print(dict1["status"]!)
                        print(dict1["response"]!)
//                        self.blurView.alpha = 0
//                        self.activity.stopAnimating()
                        self.native.set(1, forKey: "otp_res")
                        self.native.synchronize()
                        self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "otpController"))! as UIViewController
                            self.present(editPage, animated: false, completion: nil)
                        
                        
                    }
                    else
                    {
//                        self.blurView.alpha = 0
//                        self.activity.stopAnimating()
                        if dict1["response"] as! String == "OTP verification pending" &&
                            dict1["status"] as! String == "failure"
                        {
                            self.native.set(dict1["userid"], forKey: "userid")
                            self.native.synchronize()
                            self.sendUserType()
                            self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "otpController"))! as UIViewController
                                self.present(editPage, animated: false, completion: nil)
                        }
                        else
                        {
                            
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .top)
                        }
                        
                    }
                    }}
                }
                
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            
            
        }
    }
        }
        else
        {
            self.view.makeToast("Please enter valid number first!", duration: 2.0, position: .top)
        }
    }
    
}


//get Device Info
public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "i386", "x86_64":                          return "Simulator"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}




//extension UITextField{
//
//    @IBInspectable var doneAccessory: Bool{
//        get{
//            return self.doneAccessory
//        }
//        set (hasDone) {
//            if hasDone{
//                addDoneButtonOnKeyboard()
//            }
//        }
//    }
//
//    func addDoneButtonOnKeyboard()
//    {
//        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
//        doneToolbar.barStyle = .default
//        doneToolbar.isTranslucent=true
//        doneToolbar.barTintColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
//        doneToolbar.tintColor = UIColor.white
//        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let done: UIBarButtonItem = UIBarButtonItem(title: "Next", style: .done, target: self, action: #selector(self.doneButtonAction))
//
//        let items = [flexSpace, done]
//        doneToolbar.items = items
//        doneToolbar.sizeToFit()
//
//        self.inputAccessoryView = doneToolbar
//    }
//
//    @objc func doneButtonAction()
//    {
//        self.resignFirstResponder()
//    }
//}
