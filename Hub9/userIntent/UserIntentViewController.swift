//
//  UserIntentViewController.swift
//  Hub9
//
//  Created by Deepak on 14/11/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire

class UserIntentViewController: UIViewController {
    
    var native = UserDefaults.standard
    var personalUse = 0
    var isPersonal = true
    var window: UIWindow?
    

    @IBOutlet weak var wholeSaleView: UIView!
    @IBOutlet weak var wholeSaleTick: UIImageView!
    
    
    @IBOutlet weak var PersonalView: UIView!
    @IBOutlet weak var PersonalTick: UIImageView!
    
    @IBOutlet weak var nextButton: UIButton!
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
             statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
             UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        wholeSaleTick.isHidden = true
        PersonalTick.isHidden = true
        wholeSaleView.layer.borderWidth = 2
        wholeSaleView.layer.borderColor = UIColor.white.cgColor
        wholeSaleView.layer.cornerRadius = 5
        wholeSaleView.clipsToBounds=true
        wholeSaleView.dropShadow()
        PersonalView.layer.borderWidth = 2
        PersonalView.layer.cornerRadius = 5
        PersonalView.layer.borderColor = UIColor.white.cgColor
        PersonalView.clipsToBounds=true
        PersonalView.dropShadow()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func wholeSaleClick(_ sender: Any) {
        wholeSaleTick.isHidden = false
        PersonalTick.isHidden = true
        let redColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        wholeSaleView.layer.borderColor = redColor.cgColor
        PersonalView.layer.borderColor = UIColor.white.cgColor
        personalUse = 1
        
    }
    
    
    @IBAction func personalClick(_ sender: Any) {
        wholeSaleTick.isHidden = true
        PersonalTick.isHidden = false
        let redColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        PersonalView.layer.borderColor = redColor.cgColor
        wholeSaleView.layer.borderColor = UIColor.white.cgColor
        personalUse = 2
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func displayErrorAndLogOut() {
        if Util.shared.checkNetworkTapped() {
            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .center)
            
        } else {
            self.view.makeToast("You have lost connectivity to the internet. Please self.check your connection and log in again.", duration: 2.0, position: .center)
            
        }
    }
    
    @IBAction func setUserIntent(_ sender: Any) {
        
        if personalUse != 0
        {
           if personalUse == 1
           {
            isPersonal = false
            }
            else if personalUse == 2
           {
            isPersonal = true
            }
            if native.object(forKey: "Token") == nil || native.object(forKey: "Token") as! String == ""
            { self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController}
            else
            {
            var Token = native.string(forKey: "Token")!
                   if Token != nil
                   {
            CustomLoader.instance.showLoaderView()
             
             let b2burl = native.string(forKey: "b2burl")!
             let parameters: [String:Any] = ["is_personal": isPersonal]
             let header = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
             print("Successfully post", parameters, header)
             
             Alamofire.request("\(b2burl)/users/user_purchase_intent/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                 
                 
                 guard response.data != nil else { // can check byte count instead
                     self.displayErrorAndLogOut()
                     CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                     return
                 }
                     if let json = response.result.value {
                         print("JSON: \(json)")
                         let jsonString = "\(json)"
                         let result = response.result
                         
                         if let dict1 = result.value as? Dictionary<String, AnyObject>{
                             
                             switch response.result
                             {
                             case .failure(let error):
                                 //                print(error)
                                 CustomLoader.instance.hideLoaderView()
                                 UIApplication.shared.endIgnoringInteractionEvents()
                                 if let err = error as? URLError, err.code == .notConnectedToInternet {
                                     // Your device does not have internet connection!
                                     print("Your device does not have internet connection!")
                                     self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                     print(err)
                                 }
                                 else if error._code == NSURLErrorTimedOut {
                                     print("Request timeout!")
                                     self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                                 }else {
                                     // other failures
                                     self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                                 }
                             case .success:
                                 CustomLoader.instance.hideLoaderView()
                                 UIApplication.shared.endIgnoringInteractionEvents()
                                 if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                     if let invalidToken = dict1["detail"]{
                                         if invalidToken as! String  == "Invalid token."
                                         { // Create the alert controller
                                             self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                             let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                             let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                             let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                             appDelegate.window?.rootViewController = redViewController
                                         }
                                     }
                                 else if (dict1["status"]as AnyObject) as! String  == "success"
                                 {
                                    
                                     self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .bottom)
                                    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let connectPage : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "completeprofile") as UIViewController
                                    self.window = UIWindow(frame: UIScreen.main.bounds)
                                    self.window?.rootViewController = connectPage
                                    self.window?.makeKeyAndVisible()
                                     
                                 }
                                 else
                                 {
                                      self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .bottom)
                                 }
                                 
                             }
                             return
                         
                     }
                 }
                }}
                 }
            
            }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
             UIApplication.shared.endIgnoringInteractionEvents()
            self.view.makeToast("please select your intent first!", duration: 2.0, position: .center)
        }
    }
   
}


extension UIView {

  // OUTPUT 1
  func dropShadow(scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = UIColor.lightGray.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = CGSize(width: -1, height: 1)
    layer.shadowRadius = 1

    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }

  
}
