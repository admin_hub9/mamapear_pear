//
//  ReviewPaymentViewController.swift
//  Hub9
//
//  Created by Deepak on 6/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import Razorpay




var ReviewPayment = ReviewPaymentViewController()

class ReviewPaymentViewController: UIViewController, RazorpayPaymentCompletionProtocol, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {
    
//    var data = [Any]()
    var native = UserDefaults.standard
    var amount = 0.0
    var address_lkpid = ""
    var paymentmode = 1
    var RazorPaypayment_orderid = ""
    var RazorPayorderid = ""
    var RazorPayamount = ""
    var ship_insurance = 0.0
    var shippingPrice = 0.0
    var payment_id = ""
    
    
    
    
    private var razorpay: RazorpayCheckout!
    var razorpayTestKey = "rzp_live_MndzBgEajSb0UH"
    var payment_orderid = ""
    var gateway_paymentid = ""
    var orderid = ""
    var insurance = false
    var totalCost = ""
    
    
//    var data: [String: Any] = [String: Any]()
    
    
    
    @IBOutlet var DeliveryFee: UILabel!
    @IBOutlet var GST: UILabel!
    @IBOutlet var OrderCost: UILabel!
    @IBOutlet var PaymentMethod: UILabel!
    @IBOutlet var InitialPayment: DropDown!
    @IBOutlet var Address: UILabel!
    @IBOutlet var landmark: UILabel!
    @IBOutlet var addNotes: UITextField!
    @IBOutlet weak var completeOrder: UIButton!
    @IBOutlet weak var shippingCost: UILabel!
    @IBOutlet weak var CodCost: UILabel!
    @IBOutlet weak var codchargesText: UILabel!
    
    @IBOutlet weak var shipInsuranceText: UILabel!
    @IBOutlet weak var shippingInsurance: UILabel!
    @IBOutlet weak var TotalProductCost: UILabel!
    ///proceed to pay
    @IBOutlet weak var cardPayment: UIView!
    
    
    
    
    func initiateData()
    {
        self.paymentmode = PaymentAddress.selectedPayementlkpid
        if PaymentAddress.selectedPayementlkpid == 4
        {
            OrderCost.text = "\(PaymentAddress.currency)\(String(format:"%.2f" ,Double(PaymentAddress.TotalCost)-(PaymentAddress.shippingCost)))"
            if PaymentAddress.shippingCost == 0
            {
                shippingCost.text = "Free Shipping"
            }
            else
            {
                shippingCost.text = "\(PaymentAddress.currency)\(PaymentAddress.shippingCost)"
            }
            if PaymentAddress.insurance == true
            {
                self.shipInsuranceText.isHidden = false
                self.shippingInsurance.isHidden = false
                if PaymentAddress.shipInsurance == 0
                {
                    shippingInsurance.text = "Free"
                }
                else
                {
                    shippingInsurance.text = "\(PaymentAddress.currency)\(PaymentAddress.shipInsurance)"
                }
            }
            else
            {
                self.shipInsuranceText.isHidden = true
                self.shippingInsurance.isHidden = true
            }
            PaymentMethod.text = ""
            cardPayment.isHidden = false
            CodCost.isHidden = true
            codchargesText.isHidden = true
            totalCost = String(format:"%.2f", PaymentAddress.TotalCost)
            TotalProductCost.text = "\(PaymentAddress.currency)\(totalCost)"
            completeOrder.setTitle("Proceed to Pay \(PaymentAddress.currency)\(totalCost)", for: .normal)
            
            
        }
        else
        {
            OrderCost.text = "\(PaymentAddress.currency)\(String(format: "%.2f",Double(PaymentAddress.TotalCost)-(PaymentAddress.codCost)-(PaymentAddress.shippingCost)))"
            if PaymentAddress.shippingCost == 0
            {
                shippingCost.text = "Free Shipping"
            }
            else
            {
                shippingCost.text = "\(PaymentAddress.currency)\(PaymentAddress.shippingCost)"
            }
            if PaymentAddress.insurance == true
            {
                self.shipInsuranceText.isHidden = false
                self.shippingInsurance.isHidden = false
            if PaymentAddress.shipInsurance == 0
            {
                shippingInsurance.text = "Free"
            }
            else
            {
                shippingInsurance.text = "\(PaymentAddress.currency)\(PaymentAddress.shipInsurance)"
            }
            }
            else
            {
                self.shipInsuranceText.isHidden = true
                self.shippingInsurance.isHidden = true
            }
            PaymentMethod.text = "Cash on Delivery"
            cardPayment.isHidden = true
            
            if PaymentAddress.codCost == 0
            {
              CodCost.text = "Free Delivery"
            }
            else
            {
            CodCost.text = "\(PaymentAddress.currency)\(PaymentAddress.codCost)"
            }
            CodCost.isHidden = false
            codchargesText.isHidden = false
            totalCost = String(format: "%.2f", PaymentAddress.TotalCost)
            TotalProductCost.text = "\(PaymentAddress.currency)\(totalCost)"
            completeOrder.setTitle("Proceed to Pay \(PaymentAddress.currency)\(totalCost)", for: .normal)
        }
        if PaymentAddress.insurance == true{
            self.ship_insurance = PaymentAddress.shipInsurance
        }
        self.shippingPrice = PaymentAddress.shippingCost
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        razorpay = RazorpayCheckout.initWithKey(razorpayTestKey, andDelegate: self)
        ReviewPayment = self
        completeOrder.layer.cornerRadius = completeOrder.layer.frame.size.height/2
        completeOrder.layer.masksToBounds = true
        
        
    }
    
    func displayDetails()
    {
        self.OrderCost.text = "\(amount)"
    }

    @IBAction func CompleteOrder(_ sender: Any) {
        CustomLoader.instance.showLoaderView()
        parseData()
    }
   public func parseData()
    {
        var cod_charge = 0.0
        if PaymentAddress.selectedPayementlkpid == 4
        {
            cod_charge = 0.0
        }
        else
        {
            cod_charge = PaymentDetails.cod_charge
        }
        
        let address_data: [String:Any] = ["addressid": address_lkpid]
        
        let billing_address_data: [String:Any] = ["addressid": address_lkpid]
        var parameters: [String:Any] = [String:Any]()
        print("BBProductDetails.group_ownerid", PaymentAddress.group_ownerid, PaymentAddress.order_typeid)
        if PaymentAddress.order_typeid != 2 && PaymentAddress.group_ownerid == ""
        {
            parameters = ["address_data":address_data,"payment_modeid":paymentmode,"order_typeid":PaymentAddress.order_typeid, "amount":self.totalCost,"cod_charge": cod_charge, "ship_insurance": self.ship_insurance, "shipping_charge": self.shippingPrice, "remarks":addNotes.text, "billing_address_data":billing_address_data]
        }
        else if PaymentAddress.order_typeid == 2 && PaymentAddress.group_ownerid == ""
        {
            print("5765765")
            parameters = ["address_data":address_data,"payment_modeid":paymentmode,"order_typeid":PaymentAddress.order_typeid, "amount":self.totalCost,"cod_charge": cod_charge, "ship_insurance": self.ship_insurance, "shipping_charge": self.shippingPrice, "remarks":addNotes.text, "billing_address_data":billing_address_data]
        }
        else if PaymentAddress.group_ownerid != "" && BBProductDetails.order_typeid == 2
        {
            print("g78667hgdf")
            parameters = ["address_data":address_data,"payment_modeid":paymentmode,"order_typeid":PaymentAddress.order_typeid, "amount": self.totalCost,"cod_charge": cod_charge, "ship_insurance": self.ship_insurance, "shipping_charge": self.shippingPrice, "remarks":addNotes.text, "billing_address_data":billing_address_data, "group_ownerid":PaymentAddress.group_ownerid, "group_orderid":PaymentAddress.group_orderid]
            //
        }
        
        print("rrrrr",parameters)
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post", parameters)
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/create_order/"
        
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in

                print("response shop: \(response)")
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as? String == "success" && dict1["response"] as? String == "Order Created Successfully."{
                        self.RazorPaypayment_orderid = (dict1["payment_order_id"] as? String)!
                        self.RazorPayorderid = "\((dict1["order_id"] as? Int)!)"
                        self.payment_id = "\(dict1["payment_id"] as! NSNumber)"
                        self.RazorPayamount = "\(self.amount)"
                        
                        if self.paymentmode == 4
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            print("paymentmode", self.paymentmode)
                            self.initPayment((Any).self)
//
                        }
                        else
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()

                        value1.Success()
                        }
                    }
                    else{
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        let alert = UIAlertController(title: "Alert", message: "failed to create orders! please try after sometime", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }}
        }
        
    }
    
    
    
    
    
    
    
    
    
////////payment option razorpay
    @objc func initPayment(_ sender: Any)
    {
        orderid = RazorPayorderid
        gateway_paymentid = RazorPaypayment_orderid
//        amount = Double(RazorPayamount)!
        showPaymentForm()
    }
    
    
    
     internal func showPaymentForm(){
        var options: [String:Any] = [
            "name" : "BulkByte", //mandatory in paise
            "description": "purchase description",
            "order_id": self.RazorPaypayment_orderid
        ]
        
        print(options)
        if options != nil
        {
            razorpay.open(options)
        }
    }
    
    
    func Repayment()
    {
        UIApplication.shared.beginIgnoringInteractionEvents()
        let pvc = storyboard?.instantiateViewController(withIdentifier: "Repayment") as! RepaymentAlertViewController
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        present(pvc, animated: true, completion: nil)
        
        
    }
    
    
    public func onPaymentError(_ code: Int32, description str: String){
        print("error in payment")
        UIApplication.shared.beginIgnoringInteractionEvents()
        Repayment()
        
    }
    
    public func onPaymentSuccess(_ payment_id: String){
//        CustomLoader.instance.showLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        gateway_paymentid = payment_id
        generatePayment()
       
    }
 
    func generatePayment()
    {
        let parameters: [String:Any] = ["gateway_paymentid":gateway_paymentid, "payment_orderid":RazorPaypayment_orderid,"payment_id":payment_id, "amount": amount, "orderid": orderid]
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/generate_payment/"
        print(parameters, url)
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            switch response.result {
            case .success(let data as [String:Any]):
                // Yeah! Hand response
                CustomLoader.instance.hideLoaderView()
                let result = response.result
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    value1.Success()
                    self.dismiss(animated: true, completion: nil)
                    }}
            case .failure(let error):
                //                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            default:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                print("default")
            }
            print("res", response)
            
        }
        
    }
}
