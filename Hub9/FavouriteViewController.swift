//
//  FavouriteViewController.swift
//  Hub9
//
//  Created by Deepak on 5/28/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SwiftGifOrigin

var FavProductID1 = FavouriteViewController()
class FavouriteViewController: UIViewController {
    
    var native = UserDefaults.standard
    var selectedIndex = 0
    
    @IBOutlet var segmentedControl: UISegmentedControl!
    @IBOutlet var shopConatiner: UIView!
    @IBOutlet weak var dealContainer: UIView!
    
    
    override func viewWillDisappear(_ animated: Bool) {
    self.tabBarController?.tabBar.isHidden = false
    self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        UIApplication.shared.statusBarStyle = .default //Set Style
        UIApplication.shared.isStatusBarHidden = false
        segmentedControl.selectedSegmentIndex = selectedIndex
        print("segmentedControl.selectedSegmentIndex", segmentedControl.selectedSegmentIndex)
        FavouriteButton((Any).self)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//
        UIApplication.shared.isStatusBarHidden = false //Set if hidden
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if native.string(forKey: "Token")! != ""
        {
        FavProductID1 = self
        segmentedControl.addUnderlineForSelectedSegment()
             CustomLoader.instance.gifName = "bblogocorrect"

        
//         CustomLoader.instance.showLoaderView()
        
        }
        else
            {
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
    @IBOutlet var productContainer: UIView!
    
   
    
    
    @IBAction func FavouriteButton(_ sender: Any) {
        segmentedControl.changeUnderlinePosition()
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            dealContainer.isHidden = false
            productContainer.isHidden = true
            shopConatiner.isHidden = true
        //show Product view
        case 1:
            dealContainer.isHidden = true
                       productContainer.isHidden = true
                       shopConatiner.isHidden = false
           
        case 2:
           dealContainer.isHidden = true
                      productContainer.isHidden = false
                      shopConatiner.isHidden = true
                  //show Shop view
        default:
            break;
        }
        
    }
    
    
   
}
