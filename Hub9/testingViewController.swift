//
//  testingViewController.swift
//  Hub9
//
//  Created by Deepak on 11/28/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class testingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    
    
    
    var color = ["Red - S","Red - M","Red - L", "Yellow - S","Yellow - M","Yellow - L", "White - S", "White - M", "White - L"]
    var selectedIndex = [Int]()
    var selectedtext = [String]()
    var check = 0
    var count = 0
    @IBOutlet weak var testing: UITableView!
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var cellContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cellContainer.layer.cornerRadius = 8
        cellContainer.layer.masksToBounds = true
    for var i in 0..<color.count
    {
        selectedIndex.append(0)
        }
        
       
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func COnfirm(_ sender: Any) {
        
        if check == 0
        {
        check = 1
        count = 0
            for var i in 0..<selectedIndex.count
            {
                if selectedIndex[i] == 1
                {
                    count = count  + 1
                    selectedtext.append(color[i] as! String)
                    print(color[i])
                }
            }
            
        }
        else
        {
            check = 0
            count = 0
            count = color.count
        }
        testing.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex[indexPath.row] == 0
        {
        selectedIndex[indexPath.row] = 1
        
        }
        else{
            selectedIndex[indexPath.row] = 0
        }
        self.testing.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
   

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "testingtable", for: indexPath) as! testingTableViewCell
        if check == 1
        {
            cell.priceView.isHidden = false
            cell.mark.isHidden = true
            print(indexPath.row, count)
            cell.name.text = String(selectedtext[indexPath.row] as! String)
        }
        else
        {
            cell.mark.isHidden = false
            cell.priceView.isHidden = true
        if selectedIndex[indexPath.row] == 0
        {
            cell.mark.image = UIImage(named: "Uncheckmark")
        }
        else
        {
            cell.mark.image = UIImage(named: "checkmark")
        }
        }
        
        cell.name.text = color[indexPath.row]
        
        return cell
    }
    
    
}
