//
//  StripeSetupViewController.swift
//  Hub9
//
//  Created by Deepak on 06/01/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import Stripe
import Foundation


class StripeSetupViewController: UIViewController, STPPaymentCardTextFieldDelegate, STPAddCardViewControllerDelegate, UITextFieldDelegate {
    var native = UserDefaults.standard
    let paymentCardTextField = STPPaymentCardTextField()
    var isvalid = false
    
    
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var postalCode: UITextField!
    @IBOutlet weak var totalAmount: UILabel!
    
    
    @IBAction func back(_ sender: Any) {
        /**
         Simple Alert with Distructive button
         */
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postalCode.delegate=self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        if native.object(forKey: "stripe_key") != nil
        {
            if native.string(forKey: "stripe_key") != nil
            {
                if let key = native.string(forKey: "stripe_key")
                {
                    STPPaymentConfiguration.shared().publishableKey = key
                    
                }
                else
                {
                   
                    view.makeToast("Please try after sometime!", duration: 2.0, position: .center)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        self.dismiss(animated: true, completion: nil)
                    })
                    
                }
            }
        }
        
        if PaymentAddress.Stripe_amount == ""
        {
            totalAmount.isHidden = true
        }
        else
        {
        totalAmount.isHidden=false
        totalAmount.text = "Amount Payable: \(String(format:"%.2f", (PaymentAddress.Stripe_amount as NSString).floatValue))"
        }
        // Setup payment card text field
        let screenSize: CGRect = UIScreen.main.bounds
        paymentCardTextField.frame = CGRect(x: 0, y: 77, width: screenSize.width - 15, height: 50)
        paymentCardTextField.frame.origin.x = view.bounds.midX - paymentCardTextField.bounds.midX
        paymentCardTextField.delegate = self
        view.addSubview(paymentCardTextField)
        buyButton.isHidden = false;
        self.setupToHideKeyboardOnTapOnView()
        // Add payment card text field to view
        
        
    }
    
   
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y == 0{
                
                self.buyButton.frame.origin.y -= 200
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y != 0{
                
                self.buyButton.frame.origin.y += 200
            }

        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }

    // MARK: STPPaymentCardTextFieldDelegate
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        // Toggle buy button state
        
        if textField.isValid {
            isvalid = true
        }
        else
        {
           isvalid = false
        }
    }
    

    // MARK: STPAddCardViewControllerDelegate
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        // Dismiss add card view controller
        dismiss(animated: true)
    }
    
    
    

    @IBAction func Paynow(_ sender: Any) {
        if isvalid == false
        {
            self.view.makeToast("Please enter valid card details first!", duration: 2.0, position: .center)
        }
        else
        { if postalCode.text!.count>0 && postalCode.text!.count>=5
        {
        CustomLoader.instance.showLoaderView()
        let stripeCardParams = STPCardParams()
        stripeCardParams.number = paymentCardTextField.cardNumber
        stripeCardParams.expMonth = paymentCardTextField.expirationMonth
        stripeCardParams.expYear = paymentCardTextField.expirationYear
        stripeCardParams.cvc = paymentCardTextField.cvc
        stripeCardParams.addressZip = self.postalCode.text!
        STPAPIClient.shared().createToken(withCard: stripeCardParams, completion: {(token, error) -> Void in
            if let error = error {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                print("payment error", error.localizedDescription)
               self.view.makeToast(error.localizedDescription, duration: 2.0, position: .center)
            }
            else if let token = token {
                print(token)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.chargeUsingToken(token: token)
            }
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
        })}
            else
        {
            
            self.view.makeToast("Please enter valid postal code first!", duration: 2.0, position: .center)
            }
            
        }
        
    }
    
    
    
    func chargeUsingToken(token:STPToken) {
        print("token", token)
        if token != nil
        {
            PaymentAddress.stripeToken = token.tokenId as! String
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    

}
