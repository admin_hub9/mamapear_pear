//
//  AddProductViewController.swift
//  Hub9
//
//  Created by Deepak on 6/2/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore


class AddProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource {
    var imagePicker: UIImagePickerController!
    var selectedImage = [UIImage]()
    
    @IBOutlet var post: UIButton!
    @IBOutlet var cancel: UIButton!
    @IBOutlet var productCollection: UICollectionView!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedImage.count == 0 {
            return 1
        }
        
        return selectedImage.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addproduct", for: indexPath) as! AddProductCell
        if indexPath.row != 0
        {
        cell.images.layer.cornerRadius = 8
        cell.images.image = selectedImage[indexPath.row - 1]
        cell.images.clipsToBounds = true
//        cell.images.image = UIImage(named: "car")
        cell.photos.isHidden = true
        cell.camera.isHidden = true
        cell.cancel.isHidden = false
            cell.clickPhotos.isHidden = true
        let button = UIButton(frame: CGRect())
        button.layer.cornerRadius = button.layer.bounds.size.width / 2
        button.layer.masksToBounds = true
        cell.cancel.layer.cornerRadius = 0.5 * cancel.bounds.size.width
        cell.cancel.layer.masksToBounds = true
        }else{
            cell.images.layer.cornerRadius = 8
            cell.images.clipsToBounds = true
            cell.photos.isHidden = false
            cell.camera.isHidden = false
            cell.cancel.isHidden = true
            cell.clickPhotos.isHidden = false
            
            
        }
        return cell
    }

    func uploadPhotos()
    {
        let accessKey = "AKIAJYCW54FQC6MMHIDQ"
        let secretKey = "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"
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let S3BucketName = "android-image-bucket"
        let remoteName = "test.png"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
//        let image = UIImage(named: "car")
        if let data = UIImageJPEGRepresentation(selectedImage[0], 0.2){
            print(data.count)
            do {
                try data.write(to: fileURL)
            }
            catch {}
        }
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/jpeg"
        uploadRequest.acl = .bucketOwnerRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
            DispatchQueue.main.async {
                
            }
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    print("Uploaded to:\(absoluteString)")
                }
            }
            
            return nil
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImage.remove(at: indexPath.row - 1)
        productCollection.reloadData()
    }
    @IBAction func clickPhotos(_ sender: Any) {
        let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert: UIAlertAction) in
            print("Take Photo Code")
            if(UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.allowsEditing = true
                myPickerController.sourceType = .camera
                self.present(myPickerController, animated: true, completion: nil)
            }
            else
            {
                let actionController: UIAlertController = UIAlertController(title: "Camera is not available",message: "", preferredStyle: .alert)
                let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void  in
                    //Just dismiss the action sheet
                }
                actionController.addAction(cancelAction)
                self.present(actionController, animated: true, completion: nil)
            }}
        
        let gallerySelect = UIAlertAction(title: "Choose from Photos", style: .default) { (alert: UIAlertAction) in
            print("Gallery Select Code")
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        //            // Uncomment here and below at actionMenu to add a save photo action to the action list
        
        //            let savePhoto = UIAlertAction(title: "Save Photo", style: .Default, handler: { (alert: UIAlertAction) in
        //                print("Save photo code")
        //            })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
            print("Cancelled")
        }
        
        actionMenu.addAction(takePhoto)
        actionMenu.addAction(gallerySelect)
        //          actionMenu.addAction(savePhoto)
        actionMenu.addAction(cancelAction)
        
        self.present(actionMenu, animated: true, completion: nil)
    }
    //#MARK: Image picker protocol functions
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String  : Any]) {
        
        if info[UIImagePickerControllerOriginalImage] as? UIImage != nil {
            imagePicker.dismiss(animated: true, completion: nil)
//            profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            print("gallery Images:", info[UIImagePickerControllerOriginalImage] as? UIImage)
            let data = (info[UIImagePickerControllerOriginalImage] as? UIImage)
//            native.set(data, forKey: "DP")
//            native.synchronize()
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                selectedImage.insert(image, at: 0)
                self.productCollection.reloadData()
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                selectedImage.insert(image, at: 0)
                self.productCollection.reloadData()
            }
            else
            {
                print("Something went wrong")
            }
            
        }
        self.dismiss(animated: true, completion: nil)
        
        
        //        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        //        profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addproductTable", for: indexPath) as! AddProductTableCell
        cell.cellView.layer.cornerRadius = cell.cellView.layer.frame.height/2
        cell.cellView.clipsToBounds = true
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        post.layer.cornerRadius = post.layer.frame.height/2
        post.clipsToBounds = true
        cancel.layer.cornerRadius = cancel.layer.frame.height/2
        cancel.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}


//extension UIImage {
//    enum JPEGQuality: CGFloat {
//        case lowest  = 0
//        case low     = 0.25
//        case medium  = 0.5
//        case high    = 0.75
//        case highest = 1
//    }

    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
//    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
//        return UIImageJPEGRepresentation(jpegQuality.rawValue, CGFloat)
//    }
//}
