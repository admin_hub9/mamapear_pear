//
//  commentHeaderTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 21/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class commentHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var totalComments: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
