//
//  postCommentsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 21/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class postCommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var comment: UITextField!
    @IBOutlet weak var postButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    
    public func configure(text: String?, placeholder: String) {
        comment.text = text
        comment.placeholder = placeholder

        comment.accessibilityValue = text
        comment.accessibilityLabel = placeholder
    }

    func returnTextOfTextField() -> String
    {
        print(comment.text)
       return comment.text!
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    

}
