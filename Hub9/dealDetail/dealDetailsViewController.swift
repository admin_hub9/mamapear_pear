//
//  dealDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 19/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import AVKit
import  Foundation
import Alamofire
import SCLAlertView
import Cosmos
import AttributedTextView
import BadgeSwift
import Toast_Swift
import Optik
//import ReadMoreTextView

var dealProductDetails = dealDetailsViewController()

class dealDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, UITextFieldDelegate {
       
    
    let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
    let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
    let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    
       var native = UserDefaults.standard
       var ProductData = NSDictionary()
       var ProductImg = [String]()
       var ProductVideo = ""
       var ProductName = ""
       var featureTag = ""
       var user_feed_status_id = 0
       var uploadedTime = ""
       
       var product_share_link = "https://www.bulkbyte.com"
       
       var blackcolor = UIColor(red:51/255, green:51/255, blue:51/255, alpha: 1).cgColor
       var appGcolor = UIColor(red:111/255, green:204/255, blue:193/255, alpha: 1).cgColor
       
    
    
    var postCell = postCommentsTableViewCell()
    
        var priceTag = ""
        var isFeedID_liked = false
        var like_users = [AnyObject]()
        var bookmark_users = [AnyObject]()
        var likeCount = 0
        var productDescription = ""
        var isDescExpand = true
        var data = [AnyObject]()
        var owner = [String]()
        var replyStatus = false
        var comment_count = 0
       
    
        var marketplaceid = 0
        var sub_categoryid = 0
        var similarProductImage = [String]()
        var similarProductName = [String]()
        var similarProductPrice = [String]()
        var similarProductId = [Int]()
    
    //more product gride
        var moreProductImage = [String]()
        var moreProductName = [String]()
        var moreProductPrice = [String]()
        var moreProductId = [Int]()
        var user_feedid = 0
        var userid = 0
    
    
    ////edit option data
        var productLink = ""
        var productTitle = ""
        var productMarketplace = ""
        var productBrand = ""
        var productReference = ""
        var productDesc = ""
        var categoryid = 0
        var categoryText = ""
    
       var is_editing = false
        var commentText = ""
        var editIndex = 0
       var isFeedID_bookmark = false
       // add video for product
       var avPlayer = AVPlayer()
       var avPlayerLayer = AVPlayerLayer()
       var playerController = AVPlayerViewController()
       @IBOutlet weak var productDetailsTable: UITableView!
       @IBOutlet weak var faveIcon: UIImageView!
       @IBOutlet weak var footerView: UIView!
       @IBOutlet weak var UnavailableMsgContainer: UIView!
       @IBOutlet weak var unavailableText: UILabel!
    
    
    @IBOutlet weak var likeImage: UIImageView!
    @IBOutlet weak var bookmark: UIImageView!
    @IBOutlet weak var shareImage: UIImageView!
    
    @IBOutlet weak var leftButton: UIBarButtonItem!
    
    //second responder
    @IBOutlet weak var myPic2: UIImageView!
    @IBOutlet weak var commentText2: UITextField!
    @IBOutlet weak var secondResponder: UIView!
    
       
       
       @objc func comment_data(sender: UIButton)
       {
           performSegue(withIdentifier: "dealDetailComments", sender: self)
       }
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if (segue.identifier == "dealDetailComments") {
       guard let destination = segue.destination as? FeedCommentDataViewController else
       {
           debugPrint("return data")
          return
       }
             destination.user_feedid = self.user_feedid
           
           }}
       
       var btnBarBadge : MJBadgeBarButton!
       override func viewDidLoad() {
           super.viewDidLoad()
           self.tabBarController?.tabBar.isHidden = true
           CustomLoader.instance.showLoaderView()
           fetchDetails()
            self.commentText2.delegate = self
            UIView.transition(with: secondResponder, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0
                    self.secondResponder.transform = CGAffineTransform(translationX: 0, y: 800)
                    self.secondResponder.isHidden = true
            }, completion: nil)
           let token = self.native.string(forKey: "Token")!
           
           footerView.isHidden = true
           dealProductDetails = self
           productDetailsTable.isHidden = true
           productDetailsTable.tableFooterView = UIView()
           
           let loginstartColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00).cgColor
           let loginendColor = UIColor(red: 239/255.0, green: 130/255.0, blue: 136/255.0, alpha: 1.00).cgColor
           let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
           
           
           
           
       }
       
       override func viewWillAppear(_ animated: Bool) {
           
           self.tabBarController?.tabBar.isHidden = true
           
           self.navigationController?.setNavigationBarHidden(false, animated: true)
       }
       
       override func viewWillDisappear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(false, animated: true)
           self.tabBarController?.tabBar.isHidden = false
   //        senddataAddtocart = false
           
       }
       
       override func viewDidAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
          
               }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
                //location is relative to the current view
                // do something with the touched point
                debugPrint("touch?.view?.tag", touch?.view?.tag)
                if touch?.view?.tag == 32 {
                    debugPrint("inside view")
                    self.hideSecondResponder()
                    self.commentText2.resignFirstResponder()
                    self.productDetailsTable.reloadData()
                }
                else
                {
        //            dismiss(animated: true, completion: nil)
                    debugPrint("outside view")
                }
        
        self.is_editing = false
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 0
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            self.hideSecondResponder()
        }
    }
    
    
    func hideSecondResponder()
    {
        UIView.transition(with: secondResponder, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.secondResponder.transform = CGAffineTransform(translationX: 0, y: 800)
                self.secondResponder.isHidden = true
                self.productDetailsTable.reloadData()
        }, completion: nil)
    }

    
    @IBAction func menuOption(_ sender: Any) {
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                if userid != ""
                {
                if Int(userid) == self.userid
                {
                    self.myOption()
                }
                else
                {
                    self.otherUserOption()
                }
                }}
        
           }
         
        func myOption()
        {
                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

                let saveAction = UIAlertAction(title: "Delete Deal", style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    debugPrint("delete")
                 self.deleteButton()
                    
                })

                let editAction = UIAlertAction(title: "Edit Deal", style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    debugPrint("edit")
                    self.editButton()
                })
                let report = UIAlertAction(title: "Report Expired ", style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    debugPrint("expire")
                })

                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    debugPrint("Cancelled")
                })
                optionMenu.addAction(saveAction)
                optionMenu.addAction(editAction)
             if self.user_feed_status_id != 5
                {
                optionMenu.addAction(report)
                }
                optionMenu.addAction(cancelAction)
                self.present(optionMenu, animated: true, completion: nil)
    }
    
    @objc func displayImage(imgaeIndex: UIButton)
    {
        
//        FeedViewController.user_feed_id = self.feed_id[imgaeIndex.tag]
    native.set(self.data[imgaeIndex.tag]["user_id"], forKey: "feed_userid")
        native.synchronize()
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "UserFeedPost"))! as UIViewController

        self.navigationController?.pushViewController(editPage, animated: true)
        
        
    }
    
    
    
    func editButton()
    {
        
        AddDealsViewController.isediting = true
        AddDealsViewController.user_feedid = self.user_feedid
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "addDeals"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
        
    }
    
    
    func otherUserOption()
        {
                let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

                let dealReport = UIAlertAction(title: "Report Deal", style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    debugPrint("delete")
                    self.reportDeal()
                    
                })

                let report = UIAlertAction(title: "Report Expired ", style: .default, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    debugPrint("expire")
                    self.reportExpired()
                })

                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                {
                    (alert: UIAlertAction!) -> Void in
                    debugPrint("Cancelled")
                })
                optionMenu.addAction(dealReport)
                if self.user_feed_status_id != 5
                {
                    optionMenu.addAction(report)
                }
                optionMenu.addAction(cancelAction)
                self.present(optionMenu, animated: true, completion: nil)
    }
           
           /// delete button
           
           @objc func deleteButton()
           {
               debugPrint("Successfully delete post")
               let token = self.native.string(forKey: "Token")!
               var fav = 0
                   if token != ""
                   {
                       CustomLoader.instance.showLoaderView()
                   if self.native.object(forKey: "userid") != nil
                   {
                       let userid = self.native.string(forKey: "userid")!
                   
                      
                   //        debugPrint("product id:", id)
                    let parameters: [String:Any] = ["user_feedid":self.user_feedid]
                   let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                   debugPrint("Successfully post")
               
                   let b2burl = native.string(forKey: "b2burl")!
                   var url = "\(b2burl)/users/delete_user_feed/"
                   
                   debugPrint("follow product", url)
                   Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                       
                       
                       guard response.data != nil else { // can check byte count instead
                           let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                           alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                           self.present(alert, animated: true, completion: nil)
                           
                           
                           UIApplication.shared.endIgnoringInteractionEvents()
                           return
                       }
                       
                       switch response.result
                       {
                           
                           
                       case .failure(let error):
                           //                debugPrint(error)
                           CustomLoader.instance.hideLoaderView()
                           UIApplication.shared.endIgnoringInteractionEvents()
                           if let err = error as? URLError, err.code == .notConnectedToInternet {
                               // Your device does not have internet connection!
                               debugPrint("Your device does not have internet connection!")
                               self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                               debugPrint(err)
                           }
                           else if error._code == NSURLErrorTimedOut {
                               debugPrint("Request timeout!")
                               self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                           }else {
                               // other failures
                               self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                           }
                       case .success:
                           let result = response.result
                           CustomLoader.instance.hideLoaderView()
                           UIApplication.shared.endIgnoringInteractionEvents()
                           if let dict1 = result.value as? Dictionary<String, AnyObject>{
                               if let invalidToken = dict1["detail"]{
                                   if invalidToken as! String  == "Invalid token."
                                   { // Create the alert controller
                                       self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                       let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                       let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                       let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                       appDelegate.window?.rootViewController = redViewController
                                   }
                               }
                       
                       else if let json = response.result.value {
                           let jsonString = "\(json)"
                           debugPrint("res;;;;;", jsonString)
                           
                               if (dict1["status"] as? String)! == "success"
                               {
                                   self.view.makeToast("Your deal has been successfully deleted", duration: 2.0, position: .top)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                self.navigationController?.popViewController(animated: true)
                                }
                                   
                           }
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                       
                   
                   }
               }}
               }
               }}
           else
           {
               CustomLoader.instance.hideLoaderView()
               UIApplication.shared.endIgnoringInteractionEvents()
               let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                       let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                       let appDelegate = UIApplication.shared.delegate as! AppDelegate
                       appDelegate.window?.rootViewController = redViewController
                   }
               
               
           }
    
    @objc func reportDeal()
    {
        
        let token = self.native.string(forKey: "Token")!
        
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
        if self.native.object(forKey: "userid") != nil
        {
                let userid = self.native.string(forKey: "userid")!
            
             let parameters: [String:Any] = ["user_feedid":self.user_feedid]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/report_user_feed/"
            
            debugPrint("follow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    debugPrint("res;;;;;", jsonString)
                    
                        if (dict1["status"] as? String)! == "success"
                        {
                            self.view.makeToast("Your have been successfully reported this deal", duration: 2.0, position: .top)
                         DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            
                            HomeViewData.clearData()
                            self.fetchDetails()
                         }
                            
                    }
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
                
            
            }
        }}
        }
        }}
    else
    {
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
    
    @objc func reportExpired()
    {
        debugPrint("Successfully delete post")
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
            
               
            //        debugPrint("product id:", id)
             let parameters: [String:Any] = ["user_feedid":self.user_feedid]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/report_feed_expired/"
            
            debugPrint("follow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    debugPrint("res;;;;;", jsonString)
                    
                        if (dict1["status"] as? String)! == "success"
                        {
                            self.view.makeToast("This deal has merked as Expired Successfully ", duration: 2.0, position: .center)
                         DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                             HomeViewData.clearData()
                             self.fetchDetails()
                         }
                            
                    }
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
                
            
            }
        }}
        }
        }}
    else
    {
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
       
       
       @IBAction func favourite(_ sender: Any) {
           
           let token = self.native.string(forKey: "Token")!
           if token != ""
           {
               CustomLoader.instance.showLoaderView()
           var id = native.string(forKey: "FavProID")

           //        debugPrint("product id:", id)
           let parameters: [String:Any] = ["user_feedid":id!]
           let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
           debugPrint("Successfully post")
           let b2burl = native.string(forKey: "b2burl")!
           var url = ""
           if isFeedID_liked == true
           {
                url = "\(b2burl)/users/dislike_user_feed/"
           }
           else
           {
               url = "\(b2burl)/users/like_user_feed/"
           }
           debugPrint("follow product", url)
           Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in


               guard response.data != nil else { // can check byte count instead
                   let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                   self.present(alert, animated: true, completion: nil)


                   UIApplication.shared.endIgnoringInteractionEvents()
                   return
               }
            debugPrint(response)
               switch response.result
               {


               case .failure(let error):
                   //                debugPrint(error)
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   if let err = error as? URLError, err.code == .notConnectedToInternet {
                       // Your device does not have internet connection!
                       debugPrint("Your device does not have internet connection!")
                       self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                       debugPrint(err)
                   }
                   else if error._code == NSURLErrorTimedOut {
                       debugPrint("Request timeout!")
                       self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                   }else {
                       // other failures
                       self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                   }
               case .success:
                   let result = response.result
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   if let dict1 = result.value as? Dictionary<String, AnyObject>{
                       if let invalidToken = dict1["detail"]{
                           if invalidToken as! String  == "Invalid token."
                           { // Create the alert controller
                               self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                               let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                               let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                               let appDelegate = UIApplication.shared.delegate as! AppDelegate
                               appDelegate.window?.rootViewController = redViewController
                           }
                       }

               else if let json = response.result.value {
                   let jsonString = "\(json)"
                   debugPrint("res;;;;;", jsonString)
                        if self.isFeedID_liked == true
                        {
                            self.isFeedID_liked = false
                        }
                        else
                        {
                            self.isFeedID_liked = true
                        }

                        if self.isFeedID_liked == true
                        {
                            
                            self.likeImage.image = UIImage(named: "liked")
                        }
                        else
                        {
                            
                            self.likeImage.image = UIImage(named: "like")
                        }
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()

                   }
               }}
               }
           }
           else
           {
               CustomLoader.instance.hideLoaderView()
               UIApplication.shared.endIgnoringInteractionEvents()
               let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
               let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.window?.rootViewController = redViewController
           }
       }
       
       
      
    @IBAction func bookmark(_ sender: Any) {
    
                 let token = self.native.string(forKey: "Token")!
                 
                     if token != ""
                     {
                         CustomLoader.instance.showLoaderView()
                     var id = native.string(forKey: "FavProID")
                     let parameters: [String:Any] = ["user_feedid": id!]
                     let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                     debugPrint("Successfully post")
                 
                     let b2burl = native.string(forKey: "b2burl")!
                    var url = ""
                         
                    if self.isFeedID_bookmark == true
                     {
                         
                         url = "\(b2burl)/users/remove_bookmark_deal/"
                     }
                     else
                     {
                         url = "\(b2burl)/users/bookmark_deal/"
                     }
                         
                     debugPrint("bookmark api", url)
                     Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                         
                         CustomLoader.instance.hideLoaderView()
                         UIApplication.shared.endIgnoringInteractionEvents()
                         guard response.data != nil else { // can check byte count instead
                             let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                             alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                             self.present(alert, animated: true, completion: nil)
                             
                             
                             UIApplication.shared.endIgnoringInteractionEvents()
                             return
                         }
                         
                         switch response.result
                         {
                             
                             
                         case .failure(let error):
                             //                debugPrint(error)
                             CustomLoader.instance.hideLoaderView()
                             UIApplication.shared.endIgnoringInteractionEvents()
                             if let err = error as? URLError, err.code == .notConnectedToInternet {
                                 // Your device does not have internet connection!
                                 debugPrint("Your device does not have internet connection!")
                                 self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                 debugPrint(err)
                             }
                             else if error._code == NSURLErrorTimedOut {
                                 debugPrint("Request timeout!")
                                 self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                             }else {
                                 // other failures
                                 self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                             }
                         case .success:
                             let result = response.result
                             CustomLoader.instance.hideLoaderView()
                             UIApplication.shared.endIgnoringInteractionEvents()
                             if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                 if let invalidToken = dict1["detail"]{
                                     if invalidToken as! String  == "Invalid token."
                                     { // Create the alert controller
                                         self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                         let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                         let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                         appDelegate.window?.rootViewController = redViewController
                                     }
                                 }
                         
                         else if let json = response.result.value {
                             let jsonString = "\(json)"
                        if self.isFeedID_bookmark == true
                        {
                         self.isFeedID_bookmark = false
                                    }
                        else
                        {
                            self.isFeedID_bookmark = true
                                    }
                         if self.isFeedID_bookmark == true
                         {
                           
                             self.bookmark.image = UIImage(named: "bookmarkfill")
                             self.bookmark.tintColor = self.textcolor
                            self.view.makeToast("Added in your saved", duration: 2.0, position: .center)
                           }
                         else
                         {
                             
                            self.bookmark.image = UIImage(named: "bookmark")
                            self.view.makeToast("Removed from your saved", duration: 2.0, position: .center)
                             self.bookmark.tintColor = self.lightBlackColor
                         }
                                     
                             CustomLoader.instance.hideLoaderView()
                             UIApplication.shared.endIgnoringInteractionEvents()
                                     }
                         }}
                         }
                         }
                     else
                     {
                         CustomLoader.instance.hideLoaderView()
                         UIApplication.shared.endIgnoringInteractionEvents()
                         let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                         let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.window?.rootViewController = redViewController
                     }
                 
                 
             }
      
       
       @objc func openGroupView()
       {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let pvc = storyboard.instantiateViewController(withIdentifier: "gourpDealPopup") as! ViewAllGroupDealViewController
           pvc.modalPresentationStyle = .custom
           pvc.transitioningDelegate = self
           
           present(pvc, animated: true, completion: nil)
       }
       
      
       func getSimilarProduct()
       {
           
        let b2burl = native.string(forKey: "b2burl")!
        let id = native.string(forKey: "FavProID")
        var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_similar_deals/?marketplaceid=\(self.marketplaceid)&sub_categoryid=\(self.sub_categoryid)&user_feedid=\(id!)") as! URL)
           let token = self.native.string(forKey: "Token")!
           if token != ""
           {
               a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
           }
           else{
               
               a.allHTTPHeaderFields = ["Content-Type": "application/json"]
           }
           
               debugPrint("a-a-a-a-a", a)
               Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                        debugPrint(response)
                   switch response.result
                   {
                   case .failure(let error):
                       //                debugPrint(error)
                       if let err = error as? URLError, err.code == .notConnectedToInternet {
                           // Your device does not have internet connection!
                           debugPrint("Your device does not have internet connection!")
                           self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                           debugPrint(err)
                       }
                       else if error._code == NSURLErrorTimedOut {
                           debugPrint("Request timeout!")
                           self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                       }else {
                           // other failures
                           self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                       }
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       
                   
                   case .success:
                   debugPrint("responses similar product: \(response)")
                   let result = response.result
                   
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   if let dict1 = result.value as? Dictionary<String, AnyObject>{
                       if let invalidToken = dict1["detail"]{
                           if invalidToken as! String  == "Invalid token."
                           { // Create the alert controller
                               self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                               let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                               let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                               let appDelegate = UIApplication.shared.delegate as! AppDelegate
                               appDelegate.window?.rootViewController = redViewController
                           }
                       }
                       else if dict1["status"] as! String == "success"
                       {
                           
                           if let similar_marketplace_deals = dict1["similar_marketplace_deals"] as? [AnyObject]
                           {
                            for var i in 0..<similar_marketplace_deals.count
                            {
                               if similar_marketplace_deals[i]["title"] is NSNull
                               {
                                self.similarProductName.append("")
                               }
                            else
                               {
                                self.similarProductName.append((similar_marketplace_deals[i]["title"] as? String)!)
                                }
                            
                                if similar_marketplace_deals[i]["price_reference_text"] is NSNull
                                   {
                                    self.similarProductPrice.append("")
                                   }
                                else
                                   {
                                self.similarProductPrice.append((similar_marketplace_deals[i]["price_reference_text"] as? String)!)
                                }
                                
                                if similar_marketplace_deals[i]["user_feedid"] is NSNull
                                   {
                                    self.similarProductId.append(0)
                                   }
                                else
                                   {
                                    self.similarProductId.append((similar_marketplace_deals[i]["user_feedid"] as? Int)!)
                                }
                                
                                if let imagedir = similar_marketplace_deals[i]["media_url"] as? AnyObject
                                {
                                   if let imgdata = imagedir["image"] as? [AnyObject]
                                   {
                                       if imgdata.count > 0
                                       {
                                    self.similarProductImage.append(imgdata[0] as! String)
                                           
                                       }
                                   }
                               }
                            }
                               }
                            if let similar_category_deals = dict1["similar_category_deals"] as? [AnyObject]
                           {
                            for var i in 0..<similar_category_deals.count
                            {
                               if similar_category_deals[i]["title"] is NSNull
                               {
                                self.moreProductName.append("")
                               }
                            else
                            {
                                self.moreProductName.append((similar_category_deals[i]["title"] as? String)!)
                                }
                            
                                if similar_category_deals[i]["price_reference_text"] is NSNull
                                   {
                                    self.moreProductPrice.append("")
                                   }
                                else
                                   {
                                    self.moreProductPrice.append((similar_category_deals[i]["price_reference_text"] as? String)!)
                                }
                                
                                if similar_category_deals[i]["user_feedid"] is NSNull
                                   {
                                    self.moreProductId.append(0)
                                   }
                                else
                                   {
                                    self.moreProductId.append((similar_category_deals[i]["user_feedid"] as? Int)!)
                                }
                                
                                if let imagedir = similar_category_deals[i]["variant_image_url"] as? AnyObject
                                {
                                   if let imgdata = imagedir["image"] as? [AnyObject]
                                   {
                                       if imgdata.count > 0
                                       {
                                    self.moreProductImage.append(imgdata[0] as! String)
                                           
                                       }
                                   }
                               }
                            }
                               
                               }
                           
                        debugPrint("id", self.similarProductId, self.moreProductId)
                         self.productDetailsTable.beginUpdates()
                        self.productDetailsTable.reloadRows(at: [IndexPath(row: self.data.count + 6, section: 0)], with: .automatic)
                       self.productDetailsTable.reloadRows(at: [IndexPath(row: self.data.count + 7, section: 0)], with: .automatic)
                       self.productDetailsTable.endUpdates()
                       }
                       guard dict1 != nil else {
                           self.displayErrorAndLogOut()
                           //                        debugPrint ("Dict is nil Products")
                           return
                       }
                   }
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   
               }
           }
   //    }
           
       }
       
       func displayErrorAndLogOut() {
           let alert = SCLAlertView()
           let errorColor : UInt = 0xf8ac59
           if Util.shared.checkNetworkTapped() {
               alert.showError("Uh Oh!", subTitle: "Your session has expired! Please log in again.", closeButtonTitle: "Okay", colorStyle: errorColor)
           } else {
               alert.showError("Uh Oh!", subTitle: "You have lost connectivity to the internet. Please check your connection and log in again.", closeButtonTitle: "Okay", colorStyle:errorColor)//, colorTextButton: errorColor)
           }
           
           
           
       }

       func fetchDetails()
       {
               CustomLoader.instance.showLoaderView()
               let id = native.string(forKey: "FavProID")
               debugPrint("product id:", id)
               let b2burl = native.string(forKey: "b2burl")!
               var url = "\(b2burl)/users/get_deal_data/?user_feedid=\(id!)"
               
               var a = URLRequest(url: NSURL(string: url) as! URL)
           let token = self.native.string(forKey: "Token")!
           if token != ""
           {
               a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
           }
           else{

               a.allHTTPHeaderFields = ["Content-Type": "application/json"]
           }
           
           debugPrint("aproduct", a, token)
               Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                   //                                debugPrint(response)
                   switch response.result
                   {
                   case .failure(let error):
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       //                debugPrint(error)
                       if let err = error as? URLError, err.code == .notConnectedToInternet {
                           // Your device does not have internet connection!
                           debugPrint("Your device does not have internet connection!")
                           self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                           debugPrint(err)
                       }
                       else if error._code == NSURLErrorTimedOut {
                           debugPrint("Request timeout!")
                           self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                       }else {
                           // other failures
                           self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                       }
                       
                   
                   case .success:
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                   debugPrint("response product details: \(response)")
                   if let result1 = response.result.value
                   {
                       let result = response.result
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let dict1 = result.value as? Dictionary<String, AnyObject>{
                           if let invalidToken = dict1["detail"]{
                               if invalidToken as! String  == "Invalid token."
                               { // Create the alert controller
                                   self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                   appDelegate.window?.rootViewController = redViewController
                               }
                           }
                           else if dict1["status"] as! String == "success"
                           {
                               UIApplication.shared.endIgnoringInteractionEvents()
                           if dict1["deal_data"] is NSNull
                           {
                               
                           }
                          else if let innerdict1 = dict1["deal_data"] as? [AnyObject]
                              {
                                          
                               for i in 0..<innerdict1.count {
                              
                               debugPrint("deal details product1", innerdict1)
                             if innerdict1.count > 0
                             {
                                
                            if innerdict1[i]["marketplaceid"] is NSNull
                             {
                                 
                             }
                             else if let marketplaceid = innerdict1[i]["marketplaceid"]! as? Int
                            {
                               
                                    self.marketplaceid = marketplaceid
                                 }
                            if innerdict1[i]["marketplace"] is NSNull
                             {
                                 
                             }
                             else if let marketplaceid = innerdict1[i]["marketplace"]! as? String
                            {
                               
                                    self.productMarketplace = marketplaceid
                                 }
                            if innerdict1[i]["label"] is NSNull
                             {
                                 
                             }
                             else if let label = innerdict1[i]["label"]! as? String
                            {
                               
                                    self.featureTag = label
                                 }
                            if innerdict1[i]["feed_uploaded"] is NSNull
                                 {
                                     
                                 }
                                 else if let feed_uploaded = innerdict1[i]["feed_uploaded"]! as? String
                                {
                                   
                                        self.uploadedTime = feed_uploaded
                                     }
                            if innerdict1[i]["user_feed_status_id"] is NSNull
                             {
                                 
                             }
                             else if let user_feed_status_id = innerdict1[i]["user_feed_status_id"]! as? Int
                            {
                               
                                    self.user_feed_status_id = user_feed_status_id
                                 }
                            if innerdict1[i]["brand"] is NSNull
                             {
                                 
                             }
                             else if let brand = innerdict1[i]["brand"]! as? String
                            {
                               
                                    self.productBrand = brand
                                 }
                            if innerdict1[i]["user_feedid"] is NSNull
                             {
                                 
                             }
                             else if let user_feedid = innerdict1[i]["user_feedid"]! as? Int
                            {
                               
                                    self.user_feedid = user_feedid
                                 }
                            if innerdict1[i]["sub_categoryid"] is NSNull
                             {
                                 
                             }
                             else if let sub_categoryid = innerdict1[i]["sub_categoryid"]! as? [AnyObject]
                            {
                                if sub_categoryid.count > 0
                                {
                                    if let data = sub_categoryid[0] as? Int
                                    {
                                        self.categoryid = data
                                    self.sub_categoryid = data
                                    }
                                 }
                                }
                            if innerdict1[i]["sub_category_name"] is NSNull
                             {
                                 
                             }
                             else if let sub_category_name = innerdict1[i]["sub_category_name"]! as? String
                            {
                                self.categoryText = sub_category_name
                                }
                              if innerdict1[i]["title"] is NSNull
                              {
                                  
                              }
                              else if let name = innerdict1[i]["title"]! as? String
                             {
                                self.productTitle = name
                                     self.ProductName = name
                                  }
                            if innerdict1[i]["feed_text"] is NSNull
                             {
                                 
                             }
                             else if let description = innerdict1[i]["feed_text"]! as? String
                            {
                                self.productDesc = description
                                    self.productDescription = description
                                 }
                            var data = [[AnyObject]]()
                        if innerdict1[i]["comment_count"] is NSNull
                        {
                            
                        }
                        else if let count = innerdict1[i]["comment_count"] as? Int
                        {
                            self.comment_count = count
                             
                        }
                                
                        if innerdict1[i]["comment_data"] is NSNull
                        {
                            
                        }
                        else if let commentdata = innerdict1[i]["comment_data"] as? [AnyObject]
                        {
                                       
                         for var i in 0..<commentdata.count
                       {
                           self.owner.append("Sender")
                           self.data.append((commentdata[i] as? AnyObject)!)
                           if commentdata[i]["reply_comment_data"] is NSNull
                               {
                                   
                           }
                           else if let replyComment
                                = commentdata[i]["reply_comment_data"] as? [AnyObject]
                           {
                           for var j in 0..<replyComment.count
                               {
                               self.owner.append("Receiver")
                               self.data.append((replyComment[j] as? AnyObject)!)
                                       }
                                       }
                                           
                                       }
                                        }
                                
                              if innerdict1[i]["price_reference_text"] is NSNull
                               {
                                   
                               }
                               else if let name = innerdict1[i]["price_reference_text"]! as? String
                              {
                                 
                                self.productReference = name
                                      self.priceTag = name
                                   }
                              if innerdict1[i]["product_url"] is NSNull
                               {
                                   
                               }
                               else if let name = innerdict1[i]["product_url"]! as? String
                              {
                                 
                                      self.product_share_link = name
                                   }
                              if let likedata = innerdict1[i]["like_data"]
                              {
                                  if innerdict1[i]["like_data"] is NSNull
                                          {
                                              self.isFeedID_liked = false
                                          }
                                          else
                                          { self.like_users.append(innerdict1[i]["like_data"] as! AnyObject)
                                              
                                          }
                              }
                              else
                              {
                                 self.isFeedID_liked = false
                              }
                            if let bookmark_data = innerdict1[i]["bookmark_data"]
                            {
                                if innerdict1[i]["bookmark_data"] is NSNull
                                        {
                                            self.isFeedID_bookmark = false
                                        }
                                        else
                                        { self.bookmark_users.append(innerdict1[i]["bookmark_data"] as! AnyObject)
                                            
                                        }
                            }
                            else
                            {
                               self.isFeedID_bookmark = false
                            }
                              
                              if let likedata = innerdict1[i]["like_count"]
                              {
                              if innerdict1[i]["like_count"] is NSNull
                                  {
                                      self.likeCount = 0
                              }
                                  else
                                  {
                                      self.likeCount = (innerdict1[i]["like_count"] as? Int)!
                              }
                              }
                              else
                              {
                                  self.likeCount = 0
                              }
                              
                              if self.native.object(forKey: "userid") != nil
                              {
                                  let userid = self.native.string(forKey: "userid")!
                                  if self.like_users.count > i
                                  {
                                  if userid != ""
                                  {
                                      var data = self.like_users[i] as! AnyObject
                                      if data.contains(Int(userid))
                                      {
                                           self.isFeedID_liked = true
                                          
                                      }
                                      else
                                      {  self.isFeedID_liked = false
                                         
                                      }
                                      
                                  }
                                  else
                                  {
                                     self.isFeedID_liked = true
                                  }
                                  }
                                  if self.bookmark_users.count > i
                                  {
                                  if userid != ""
                                  {
                                      var data = self.bookmark_users[i] as! AnyObject
                                      if data.contains(Int(userid))
                                      {
                                           self.isFeedID_bookmark = true
                                          
                                      }
                                      else
                                      {  self.isFeedID_bookmark = false
                                         
                                      }
                                      
                                  }
                                  else
                                  {
                                     self.isFeedID_bookmark = true
                                  }
                                  }
                                  // bookamrk
                                
                                  
                                  
                              }
                                if self.isFeedID_bookmark == true
                                {
                                    self.bookmark.image = UIImage(named: "bookmarkfill")
                                     
                                    self.bookmark.tintColor = self.textcolor
                                  }
                                else
                                {
                                        self.bookmark.image = UIImage(named: "bookmark")
                                    
                                    self.bookmark.tintColor = self.lightBlackColor
                                }
                                if self.isFeedID_liked == true
                                {
                                    
                                    self.likeImage.image = UIImage(named: "liked")
                                }
                                else
                                {
                                    
                                    self.likeImage.image = UIImage(named: "like")
                                }
                                if innerdict1[i]["userid"] is NSNull
                                {}
                                else if let userid = innerdict1[i]["userid"] as? Int
                                {
                                    self.userid = userid
                                }
                              
                                if let imagedir = innerdict1[i]["media_url"] as? AnyObject{
                                            if let imagedata = imagedir["image"] as? [AnyObject]
                                           {
                                               debugPrint("image is not empty", imagedata)
                                               if imagedata.count > 0
                                               { self.ProductImg.append((imagedata[0] as? String)!)
                                               }
                                            else
                                            {
                                                self.ProductImg.append("")
                                                
                                                }
                                            }
                                           else if let videodata = imagedir["image"] as? String
                                           { self.ProductImg.append((imagedir["image"] as? String)!)
                                           }
                                           else
                                           {
                                               debugPrint("outer image is empty")
                                               self.ProductImg.append("")
                                           }
                                    
                                          if imagedir["mp4"] is NSNull
                                          {
                                              self.ProductVideo = ""
                                          }
                                          else{
                                              if let videodata = imagedir["mp4"] as? [AnyObject]
                                          {             if videodata.count > 0
                                              {
                                                  self.ProductVideo = videodata[0] as! String
                                              }
                                          }
                                    }      } } }
                              
                          }
                           }
                        
                        if self.ProductName != ""
                        {
                            self.footerView.isHidden = false
                            self.UnavailableMsgContainer.isHidden = true
                            self.productDetailsTable.isHidden = false
                            self.productDetailsTable.reloadData()
                        }
                        else
                        {
                            self.footerView.isHidden = true
                            self.UnavailableMsgContainer.isHidden = false
                            self.productDetailsTable.isHidden = false
                            self.productDetailsTable.reloadData()
                            
                            self.view.makeToast("Currently Unavailable!", duration: 2.0, position: .center)
                            
                        }
                        self.getSimilarProduct()
                       }
                   }
                    
                    
                   }}
   //        }
       }
       
     
       
       @IBAction func shareButton(_ sender: Any) {
           
           
           // image to share
           let text = self.ProductName
           var productImage = "thumbnail"
           if self.ProductImg.count>0
           {
           var imageUrl = self.ProductImg[0] as! String
           imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
           
           if let url = NSURL(string: imageUrl )
           {
               if url != nil
               {
                   productImage = imageUrl
               }}}
           let image = UIImage(named: productImage)
           let myWebsite = NSURL(string:self.product_share_link)
           let shareAll = [text , image , myWebsite!] as [Any]
           let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
           activityViewController.popoverPresentationController?.sourceView = self.view
           self.present(activityViewController, animated: true, completion: nil)
       }
 
      
           func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
               if data.count > 0
               {
               return data.count + 8
               }
               else
               {
                   return 8
               }
           }
    
    func relativeDate(time: Double) -> String {
        
        let theDate = Date(timeIntervalSince1970: TimeInterval(time))
        let components = Calendar.current.dateComponents([.day, .year, .month, .weekOfYear], from: theDate, to: Date())
        if let year = components.year, year == 1{
            return "\(year) year ago"
        }
        if let year = components.year, year > 1{
            return "\(year) years ago"
        }
        if let month = components.month, month == 1{
            return "\(month) month ago"
        }
        if let month = components.month, month > 1{
            return "\(month) months ago"
        }

        if let week = components.weekOfYear, week == 1{
            return "\(week) week ago"
        }
        if let week = components.weekOfYear, week > 1{
            return "\(week) weeks ago"
        }

        if let day = components.day{
            if day > 1{
                return "\(day) days ago"
            }else{
                "Yesterday"
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: theDate)
        return strDate
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row >= 5 && indexPath.row < self.data.count + 5
        {
            debugPrint(indexPath.row, self.owner[indexPath.row - 5], self.editIndex)
        if self.owner[indexPath.row - 5] == "Sender"
        {
            self.replyStatus=true
            self.editIndex = indexPath.row - 5
            self.productDetailsTable.reloadData()
            UIView.transition(with: secondResponder, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0
                    self.secondResponder.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.commentText2.becomeFirstResponder()
                    self.secondResponder.isHidden = false
            }, completion: nil)  
        }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellSize = 0
        
        if indexPath.row == 0
        {
            cellSize = 280
        }
        else if indexPath.row == 1
        {
            cellSize = Int(UITableViewAutomaticDimension)
        }
            
        else if indexPath.row == 2
        {
            if self.productDescription as String != ""
            {
                cellSize = 45
            }
            else
            {
                cellSize = 0
            }
        }
        else if indexPath.row == 3
        {
            if self.productDescription as String != "" && self.isDescExpand == true
            {
                cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        }
        else if indexPath.row == 4
        {
              cellSize = 50
        }
        else if indexPath.row >= 5 && indexPath.row < self.data.count
        {
            if self.data.count != 0
            {
            cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        }
        else if indexPath.row == self.data.count + 6
        {
            if self.similarProductId.count > 0
            {
            cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        }
        else if indexPath.row == self.data.count + 7
        {
            if self.moreProductId.count > 0
            {
            cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        }
        else
        {
          cellSize = Int(UITableViewAutomaticDimension)
        }
       
            return CGFloat(cellSize)
    }
       
    
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           
           var ReuseCell = UITableViewCell()
        if indexPath.row == 0
        {
               let cell = tableView.dequeueReusableCell(withIdentifier: "productImage", for: indexPath) as! ProductImageCollectionTableViewCell
             
               cell.pageIndicator.currentPage = indexPath.row
               cell.ProductImageCollection.tag = indexPath.row
               cell.ProductImageCollection.reloadData()

               ReuseCell = cell
        }
        
        else if indexPath.row == 1
        {
               let cell = tableView.dequeueReusableCell(withIdentifier: "nameandpricing", for: indexPath) as! ProductNameAndPricingTableViewCell
               let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
               let textbackcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 0.2)
               let lightGrayColor = UIColor(red: 87/255, green: 87/255, blue: 87/255, alpha: 1)
               let lightGrayBackColor = UIColor(red: 87/255, green: 87/255, blue: 87/255, alpha: 0.2)
               
               
               if self.user_feed_status_id == 5
               {
                cell.featuredTag.backgroundColor = lightGrayBackColor
                cell.featuredTag.textColor = lightGrayColor
                cell.productName.attributedText = ( "\(self.ProductName) ".color(lightGrayColor).strikethrough(1)).attributedText
                
               }
               else
               {
                cell.featuredTag.backgroundColor = textbackcolor
                cell.featuredTag.textColor = textcolor
                cell.productName.text = self.ProductName
               }
               if self.uploadedTime != ""
               {
                let time  = self.relativeDate( time: Double(self.uploadedTime)!)
                cell.uploadTime.text = "\(time)"
               }
               cell.featuredTag.layer.cornerRadius = cell.featuredTag.frame.height/2
               cell.featuredTag.clipsToBounds=true
               if self.featureTag != ""
               {
                    cell.featuredTag.text = "   \(self.featureTag)   "
               }
               else
               {
                    cell.featuredTag.text = ""
               }
               if self.priceTag != ""
               {
                cell.price.attributedText = ("\(priceTag)".color(textcolor).size(20)).attributedText
               }
               
               ReuseCell = cell
        }
        else if indexPath.row == 2
        {
                let cell = tableView.dequeueReusableCell(withIdentifier: "descriptionHeader", for: indexPath) as! DescriptionHeaderTableViewCell
                        cell.showIcon.isHidden=true
                        ReuseCell = cell
        }
        else if indexPath.row == 3
        {
                let cell = tableView.dequeueReusableCell(withIdentifier: "description", for: indexPath) as! DescriptionTableViewCell
                cell.productDescription.attributedText = NSAttributedString(html: self.productDescription)
                cell.productDescription.isUserInteractionEnabled = true
               
                
                ReuseCell = cell
        }
        else if indexPath.row == 4
        {
                let cell = tableView.dequeueReusableCell(withIdentifier: "commentHeader", for: indexPath) as! commentHeaderTableViewCell
                if self.data.count == 1
                {
                    cell.totalComments.text = "\(self.comment_count) Comment"
                    
                }
                else if self.data.count > 1
                {
                    cell.totalComments.text = "\(self.comment_count) Comments"
                    
                }
                
                else
                {
                    cell.totalComments.text = "Add Comments"
                    
                }
                if self.comment_count > 3
                {
                    cell.viewAllButton.isHidden = false
                }
                else
                {
                    cell.viewAllButton.isHidden = true
                }
                cell.viewAllButton.tag = 4
                cell.viewAllButton.addTarget(self, action: #selector(comment_data(sender:)), for: .touchUpInside)
                
                ReuseCell = cell
        }
        else if indexPath.row >= 5 && indexPath.row < self.data.count + 5 && self.data.count != 0
        {
            if self.owner[indexPath.row - 5] == "Sender"
            {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
           
            let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if indexPath.row == 0
                {
                  cell.seprator.isHidden=true
                }
                else
                {
                    cell.seprator.isHidden=true
                }
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                
                if data[indexPath.row - 5]["user_id"] is NSNull
                {}
                else
                {   if Int(userid) != (data[indexPath.row - 5]["user_id"] as? Int)!
                {
                    cell.deleteButton.isHidden = true
                    cell.editButton.isHidden = true
                }
                    else
                {
                    cell.deleteButton.isHidden = false
                    cell.editButton.isHidden = true
                }
                    
                }
            }
            if data[indexPath.row - 5]["profile_pic_url"] is NSNull
            {}
            else
            {

                let imagedata = data[indexPath.row - 5]["profile_pic_url"] as? String

                   var imageUrl = (imagedata)!
                   imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                   if let url = NSURL(string: imageUrl )
                   {
                       if url != nil
                       {
                        DispatchQueue.main.async {
                            cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                       }
                   }
                }

            var like = 0
            var comment = 0
            
            
            if data[indexPath.row - 5]["comment"] is NSNull
            {
                cell.NameAndText.text = ""
            }
            else
            {
                
                cell.NameAndText.attributedText = ("\(data[indexPath.row - 5]["first_name"] as! String) \(data[indexPath.row - 5]["last_name"] as! String) ".color(lightBlackColor).size(16.0)).attributedText
               
                cell.commentText.attributedText = ( "\(data[indexPath.row - 5]["comment"] as! String)".color(lightGrayColor).size(14.0) + "  Reply".color(UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)).size(14.0)).attributedText
                if data[indexPath.row - 5]["inserted_at"] is NSNull
                {}
                else
                {
                     if let date1 = self.data[indexPath.row - 5]["inserted_at"] as? String
                        {
                        
                        let time  = self.relativeDate( time: Double(date1)!)
                            debugPrint("time", time)
                        cell.commentTime.text = "\(time)"
                    }
                }
                
                
            }
            
            
            cell.editButton.tag = indexPath.row - 5
            cell.editButton.addTarget(self, action: #selector(updateEditButton(Feed_Index:)), for: .touchUpInside)
            cell.deleteButton.tag = indexPath.row - 5
            cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
             
            ReuseCell = cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "replyCommentCell", for: indexPath) as! ReplyCommentTableViewCell
                
                 let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                 let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                 
                 if self.native.object(forKey: "userid") != nil
                 {
                     let userid = self.native.string(forKey: "userid")!
                     
                     if data[indexPath.row - 5]["user_id"] is NSNull
                     {}
                     else
                     {   if Int(userid) != (data[indexPath.row - 5]["user_id"] as? Int)!
                     {
                         cell.deleteButton.isHidden = true
                         cell.editButton.isHidden = true
                     }
                         else
                     {
                         cell.deleteButton.isHidden = false
                         cell.editButton.isHidden = true
                     }
                         
                     }
                 }
                 if data[indexPath.row - 5]["profile_pic_url"] is NSNull
                 {}
                 else
                 {

                     let imagedata = data[indexPath.row - 5]["profile_pic_url"] as? String

                        var imageUrl = (imagedata)!
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                        if let url = NSURL(string: imageUrl )
                        {
                            if url != nil
                            {
                                DispatchQueue.main.async {
                                    cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        }
                     }

                 var like = 0
                 var comment = 0
                 
                 
                 if data[indexPath.row - 5]["reply"] is NSNull
                 {
                     cell.NameAndText.text = ""
                 }
                 else
                 {
                     
                     cell.NameAndText.attributedText = ("\(data[indexPath.row - 5]["first_name"] as! String) \(data[indexPath.row - 5]["last_name"] as! String) ".color(lightBlackColor).size(16.0)).attributedText
                    debugPrint("reply comment", indexPath.row, self.owner[indexPath.row - 5], self.data[indexPath.row - 5])
//                    cell.commentText.attributedText = ( "jkcdnjdksncdjkscn".color(lightGrayColor).size(14.0)).attributedText
                    
                     cell.commentText.attributedText = ( "\(data[indexPath.row - 5]["reply"] as! String)".color(lightGrayColor).size(14.0)).attributedText
                     if data[indexPath.row - 5]["inserted_at"] is NSNull
                     {}
                     else
                     {
                          if let date1 = self.data[indexPath.row - 5]["inserted_at"] as? String
                             {
                             
                             let time  = self.relativeDate( time: Double(date1)!)
                                 debugPrint("time", time)
                             cell.commentTime.text = "\(time)"
                         }
                     }
                     
                     
                 }
                 
                 
                 cell.editButton.tag = indexPath.row - 5
                 cell.editButton.addTarget(self, action: #selector(updateEditButton(Feed_Index:)), for: .touchUpInside)
                 cell.deleteButton.tag = indexPath.row - 5
                 cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
                
                ReuseCell = cell
            }
        }
        else if indexPath.row == self.data.count + 5
        {
            
                postCell = tableView.dequeueReusableCell(withIdentifier: "postComment") as! postCommentsTableViewCell
                postCell.comment.delegate = self
                
                    postCell.comment.becomeFirstResponder()
            
            postCell.userImage.layer.cornerRadius = postCell.userImage.frame.height/2
            postCell.userImage.clipsToBounds=true
            if native.object(forKey: "profilepic") != nil
            {
                let image = native.string(forKey: "profilepic")!
                
                if image != nil{
                    let url = URL(string: image)
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            self.postCell.userImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
            }
                ReuseCell = postCell
        }
       else if indexPath.row == self.data.count + 6
        {
               let cell = tableView.dequeueReusableCell(withIdentifier: "shopproduct", for: indexPath) as! ShopProductTableViewCell
               cell.shopProductTitle.text = "Similar Deals"
               cell.ShopProductCollection.tag = indexPath.row
               cell.ShopProductCollection.reloadData()
               ReuseCell = cell
        }
        else 
        {
               let cell = tableView.dequeueReusableCell(withIdentifier: "shopproduct", for: indexPath) as! ShopProductTableViewCell
               cell.shopProductTitle.text = "More Deals from This Store"
               cell.ShopProductCollection.tag = indexPath.row
               cell.ShopProductCollection.reloadData()
               ReuseCell = cell
        }
               
          
           return ReuseCell
       }
       

   
       
       
       
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           var gridSize = 0
           debugPrint("collectionView.tag", collectionView.tag)
          if collectionView.tag == 0
          {
               gridSize = self.ProductImg.count
        }
          else if  collectionView.tag == self.data.count + 6
          {
                debugPrint("self.similarProductId.count", self.similarProductId.count , collectionView.tag)
                gridSize = self.similarProductId.count
        }
          else if  collectionView.tag == self.data.count + 7
          {
                debugPrint("self.moreProductId.count", self.moreProductId.count, collectionView.tag)
                gridSize = self.moreProductId.count
        }
           
           return gridSize
       }
    
       
      
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   //        debugPrint("collection index", collectionView.tag)
           var ReuseCell = UICollectionViewCell()
           if collectionView.tag == 0
           {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoandimageslider", for: indexPath) as! VideoAndImageSliderCollectionViewCell
                   if indexPath.row == 0 && ProductVideo != ""
                   {
                       let url = self.ProductVideo as String
                       if self.ProductVideo as String != ""
                       {
                           let videoURL = NSURL(string: url)
                           avPlayer = AVPlayer(url: videoURL! as URL)
                           avPlayer.volume = 10
                           avPlayer.isMuted = true
                           let playerController = AVPlayerViewController()
                           playerController.player = avPlayer
                           
                           self.addChildViewController(playerController)
                           
                           // Add your view Frame
                           
                           playerController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                           playerController.view.frame = cell.videoView.bounds
                           playerController.showsPlaybackControls = true
                           
                           do {
                               try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                           } catch _ {
                           }
                           do {
                               try AVAudioSession.sharedInstance().setActive(true)
                           } catch _ {
                           }
                           do {
                               try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                           } catch _ {
                           }
                           avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.none
                           
                           // Add subview in your view
                           cell.videoView.isHidden = false
                           cell.image.isHidden = true
                           cell.videoView.addSubview(playerController.view)
                           playerController.didMove(toParentViewController: self)

   //                        avPlayer.play()
                           
                           
                           
                       }}
               else
                       if self.ProductImg.count > indexPath.row && indexPath.row > -1
               {
                   cell.videoView.isHidden = true
                   cell.image.isHidden = false
//                    debugPrint("ImageUrl-NM\(self.ProductImg[indexPath.row])")
                   var imageUrl = self.ProductImg[indexPath.row] as! String
                   imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
//                debugPrint("ImageUrl-MOD\(imageUrl)")
                   
                   if let url = NSURL(string: imageUrl )
                   {
                       if url != nil
                       {
                        DispatchQueue.main.async {
                            cell.image.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                       }
                   }
               }
               
               
                           ReuseCell = cell
        }
        
         else if  collectionView.tag == self.data.count + 6
           {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similarDeals", for: indexPath) as! SimilarProductCollectionViewCell
                   debugPrint("ImageUrl-Count\(similarProductImage.count) and \(indexPath.row)")
                if similarProductImage.count > indexPath.row
                    {
                        debugPrint("ImageUrl-INIF\(similarProductImage[indexPath.row]) and \(indexPath.row)")
                        var imageUrl = similarProductImage[indexPath.row] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        debugPrint("ImageUrl-INIF\(imageUrl) and \(indexPath.row)")
                        if let url = NSURL(string: imageUrl )
                        {
                            if url != nil
                            {
                                DispatchQueue.main.async {
                                    cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        }
                    }
                    
                        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                       
                        if native.string(forKey: "Token")! == ""
                        {
                            let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                            let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                            cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                        }
                        else
                        {
                            cell.price.attributedText = ("\(self.similarProductPrice[indexPath.row])" .color(textcolor).size(11)).attributedText
                        }
                    
                        if self.similarProductName.count > indexPath.row
                        {
                            cell.productName.text = self.similarProductName[indexPath.row]
                        }
                
                ReuseCell = cell
        }
           else if  collectionView.tag == self.data.count + 7 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similarDeals", for: indexPath) as! SimilarProductCollectionViewCell
                if moreProductImage.count > indexPath.row
                    {
                        var imageUrl = moreProductImage[indexPath.row] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        
                        if let url = NSURL(string: imageUrl )
                        {
                            if url != nil
                            {
                                DispatchQueue.main.async {
                                    cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        }
                    }
                    
                        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                       
                        if native.string(forKey: "Token")! == ""
                        {
                            let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                            let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                            cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                        }
                        else
                        {
                            cell.price.attributedText = ("\(self.moreProductPrice[indexPath.row])" .color(textcolor).size(11)).attributedText
                        }
                    
                        if self.moreProductName.count > indexPath.row
                        {
                            cell.productName.text = self.moreProductName[indexPath.row]
                        }
                
                ReuseCell = cell
        }
          
           
           return ReuseCell
       }

       
       
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           var cellSize: CGSize = CGSize(width: 0.0, height: 0.0)
           let frameSize = collectionView.frame.size
           if collectionView.tag == 0
           {

           cellSize = CGSize(width: frameSize.width, height: 300)
           }
        else  if  collectionView.tag == self.data.count + 6
        {
            cellSize = CGSize(width: (frameSize.width-60)/3, height: frameSize.height)
        }
        else  if  collectionView.tag == self.data.count + 7
        {
            cellSize = CGSize(width: (frameSize.width-60)/3, height: frameSize.height)
        }

           return cellSize
       }
       
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           
           if collectionView.tag == 0 {
                   var AllImage = [URL]()
               if  ProductVideo == ""
               {
                   if indexPath.row >= 0
                   {
                   for var i in 0..<self.ProductImg.count
                   {
                   var imageUrl = self.ProductImg[i] as! String
                   imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                       if let url = URL(string: imageUrl)
                   {
                       if url != nil
                       {
                           AllImage.append(url)
                           debugPrint("allimage", AllImage)
                       }
                       }}
                   debugPrint("allimage", AllImage)
//                       let imageDownloader = AlamofireImageDownloader()
//
//                       let viewController = Optik.imageViewer(withURLs: AllImage, imageDownloader: imageDownloader)
//                       present(viewController, animated: true, completion: nil)
                   }}
               else if ProductVideo != ""
               {
                   if indexPath.row > 0
                   {
                   for var i in 1..<self.ProductImg.count
                   {
                       var imageUrl = self.ProductImg[i] as! String
                       imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                       if let url = URL(string: imageUrl)
                       {
                           if url != nil
                           {
                               AllImage.append(url)
                               debugPrint("allimage", AllImage)
                           }
                       }}
                   debugPrint("allimage", AllImage)
//                   let imageDownloader = AlamofireImageDownloader()
//                   
//                   let viewController = Optik.imageViewer(withURLs: AllImage, imageDownloader: imageDownloader)
//                   present(viewController, animated: true, completion: nil)
                   }}
           }
       else if  collectionView.tag == self.data.count + 6
        {
            
            native.set("product", forKey: "comefrom")
            native.set(similarProductId[indexPath.row], forKey: "FavProID")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "dealDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        else if  collectionView.tag == self.data.count + 7
        {
            
            native.set("product", forKey: "comefrom")
            native.set(moreProductId[indexPath.row], forKey: "FavProID")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "dealDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
           
       }
   
    
   /// delete button
    
    @objc func deleteButton(Feed_Index: UIButton)
    {
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
                if self.native.object(forKey: "userid") != nil && data.count>Feed_Index.tag
            {
                var userid = 0
                
                if self.owner[Feed_Index.tag] == "Sender"
                {
                    userid = (data[Feed_Index.tag]["user_feed_commentid"] as? Int)!
                }
                else
                {
                    userid = (data[Feed_Index.tag]["user_feed_sub_commentid"] as? Int)!
                }
            
               
            //        debugPrint("product id:", id)
                let parameters: [String:Any] = ["user_feed_commentid": userid]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/delete_feed_comment/"
            
            debugPrint("follow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    debugPrint("res;;;;;", jsonString)
                    
                            
                            if self.data.count > Feed_Index.tag
                            {
                                self.data.remove(at: Feed_Index.tag)
                                
                                if self.comment_count>0
                                {
                                    self.comment_count = self.comment_count - 1
                                }
                            }
                        if self.owner[Feed_Index.tag] == "Sender"
                        {
                        if self.comment_count == 0
                        {
                            self.navigationController?.navigationBar.topItem?.title = "Comment"
                        }
                        else
                        {
                            self.navigationController?.navigationBar.topItem?.title = "Comment (\(self.comment_count))"  }
                            }
                            DispatchQueue.main.async {
                                self.owner.remove(at: Feed_Index.tag)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.productDetailsTable.reloadData()
                            }
                    }
                }}
                }
                }}
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
       
       
       
       @objc func updateEditButton(Feed_Index: UIButton)
       {
           self.is_editing = true
           self.editIndex = Feed_Index.tag - 5
           self.view.becomeFirstResponder()
        self.commentText = (self.data[Feed_Index.tag]["comment"] as? String)!
        self.productDetailsTable.reloadRows(at: [IndexPath(row: 8, section: 0)], with: .automatic)
       }
       
    
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool
//    {
//         //cell = tbl.dequeueReusableCell(withIdentifier: "CELL") as! TableViewCell
//        self.commentText = postCell.comment.text!
//        postCell.comment.resignFirstResponder()
//        self.postComment()
//        return true
//    }
      
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
              
              self.view.endEditing(true)
              if replyStatus == false && postCell.comment.text!.count > 0
              {
              self.sendComment()
              }
              else
              {
                  
                  self.replyComment()
              }
              
              return true
              
          }
          
          
           func sendComment() {
              
              if postCell.comment.text! != ""
              {
                self.view.resignFirstResponder()
              let token = self.native.string(forKey: "Token")!
              var fav = 0
                  if token != ""
                  {
                      CustomLoader.instance.showLoaderView()
                  if self.native.object(forKey: "userid") != nil
                  {
//                      let userid = self.native.string(forKey: "userid")!
                  
                      var user_feedid = "0"
                    if self.native.object(forKey: "FavProID") != nil
                    {
                      let id = native.string(forKey: "FavProID")
                       user_feedid = id!
                    }
                  //        debugPrint("product id:", id)
                    let parameters: [String:Any] = ["user_feedid":user_feedid,"comment": postCell.comment.text!]
                  let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                  debugPrint("Successfully post")
              
                  let b2burl = native.string(forKey: "b2burl")!
                  var url = "\(b2burl)/users/add_comment_to_feed/"
                  
                  
                  debugPrint("comment product", url, parameters)
                  Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                      
                      
                      guard response.data != nil else { // can check byte count instead
                          let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                          self.present(alert, animated: true, completion: nil)
                          
                          
                          UIApplication.shared.endIgnoringInteractionEvents()
                          return
                      }
                      debugPrint("resonce comment", response)
                      
                      switch response.result
                      {
                          
                          
                      case .failure(let error):
                          //                debugPrint(error)
                          CustomLoader.instance.hideLoaderView()
                          UIApplication.shared.endIgnoringInteractionEvents()
                          if let err = error as? URLError, err.code == .notConnectedToInternet {
                              // Your device does not have internet connection!
                              debugPrint("Your device does not have internet connection!")
                              self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                              debugPrint(err)
                          }
                          else if error._code == NSURLErrorTimedOut {
                              debugPrint("Request timeout!")
                              self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                          }else {
                              // other failures
                              self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                          }
                      case .success:
                          let result = response.result
                          CustomLoader.instance.hideLoaderView()
                          UIApplication.shared.endIgnoringInteractionEvents()
                          if let dict1 = result.value as? Dictionary<String, AnyObject>{
                              if let invalidToken = dict1["detail"]{
                                  if invalidToken as! String  == "Invalid token."
                                  { // Create the alert controller
                                      self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                      let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                      let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                      let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                      appDelegate.window?.rootViewController = redViewController
                                  }
                              }
                      
                      else if let json = response.result.value {
                          let jsonString = "\(json)"
                         
                              
                                  var first_name = ""
                                  var last_name = ""
                                  var profile_pic_url = "";
                                  var user_feed_commentid = 0;
                                  var user_id = 0
                                  
                                  if dict1["response"] is NSNull
                                  {}
                                  else
                                  {
                                      if let commentData = dict1["response"] as? AnyObject
                                      {
                                          if let data = commentData["response"]
                                          {
                                          if commentData["response"] is NSNull
                                          {}
                                          else
                                          {
                                             user_feed_commentid = (commentData["response"] as? Int)!
                                          }
                                          }
                                      }
                                  }
                                  
                                  
                                  
                                  if self.native.object(forKey: "userid") != nil
                                  {
                                      user_id = Int(self.native.string(forKey: "userid")!)!
                                  }
                                  
                                  if self.native.object(forKey: "profilepic") != nil
                                  {
                                      profile_pic_url = self.native.string(forKey: "profilepic")!
                                  }
                                  if self.native.object(forKey: "firstname") != nil
                                  {
                                      first_name = self.native.string(forKey: "firstname")!
                                  }
                                  if self.native.object(forKey: "lastname") != nil
                                  {
                                      last_name = self.native.string(forKey: "lastname")!
                                  }
                                  if self.native.object(forKey: "userid") != nil
                                  {
                                      last_name = self.native.string(forKey: "userid")!
                                  }
                                  if self.native.object(forKey: "lastname") != nil
                                  {
                                      last_name = self.native.string(forKey: "lastname")!
                                  }
                              
                          var timestamp = NSDate().timeIntervalSince1970
                                  var allComment = self.data
                                       self.data.removeAll()
                                       
                                self.data.append(["comment": self.postCell.comment!.text!,
                                           "first_name": first_name,
                                           "inserted_at": "\(timestamp)",
                                           "last_name": last_name,
                                           "profile_pic_url": "\(profile_pic_url)",
                                  "user_feed_commentid": user_feed_commentid,
                                  "user_id": user_id] as AnyObject)

                                       for var i in 0..<allComment.count
                                       {
                                           self.data.append(allComment[i])
                                       }
                                 
                          
                          self.comment_count = self.comment_count + 1
                                  
      //                    self.owner.append("Receiver")
                                  let ownerData = self.owner
                                  self.owner.removeAll()
                          
                          self.owner.append("Sender")
                                  for var i in 0..<ownerData.count
                                  {
                                      self.owner.append(ownerData[i])
                                  }
                          if self.comment_count == 0
                          {
                              self.navigationController?.navigationBar.topItem?.title = "Comment"
                          }
                          else
                          {
                              self.navigationController?.navigationBar.topItem?.title = "Comment (\(self.comment_count))"                                }
                          self.postCell.comment!.text! = ""
                                  
                          DispatchQueue.main.async {
                            self.productDetailsTable.reloadData()
                          CustomLoader.instance.hideLoaderView()
                          UIApplication.shared.endIgnoringInteractionEvents()
                              self.productDetailsTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                                  }
                          
                          }
                      }}
                      }
                      }}
                  else
                  {
                      CustomLoader.instance.hideLoaderView()
                      UIApplication.shared.endIgnoringInteractionEvents()
                      let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                      let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                      let appDelegate = UIApplication.shared.delegate as! AppDelegate
                      appDelegate.window?.rootViewController = redViewController
                  }
              
              
          }
              else
              {
                  self.view.makeToast("Please type send comment first.", duration: 2.0, position: .center)
              }
          }
          
          
          func replyComment() {
                  self.hideSecondResponder()
                  if self.commentText2!.text! != ""
                  {
                    self.commentText2!.resignFirstResponder()
                    self.view.resignFirstResponder()
                  let token = self.native.string(forKey: "Token")!
                  var fav = 0
                      if token != ""
                      {
                          CustomLoader.instance.showLoaderView()
                        debugPrint(self.data.count, editIndex)
                          let parameters: [String:Any] = ["user_feed_commentid": (data[editIndex]["user_feed_commentid"] as? Int)!,"reply_text": self.commentText2!.text!]
                      let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                      debugPrint("Successfully post")
                  
                      let b2burl = native.string(forKey: "b2burl")!
                      var url = "\(b2burl)/users/reply_to_comment/"
                      
                      
                      debugPrint("comment product", url, parameters)
                      Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                          
                          
                          guard response.data != nil else { // can check byte count instead
                              let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                              alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                              self.present(alert, animated: true, completion: nil)
                              
                              
                              UIApplication.shared.endIgnoringInteractionEvents()
                              return
                          }
                          debugPrint("resonce comment", response)
                          
                          switch response.result
                          {
                              
                              
                          case .failure(let error):
                              //                debugPrint(error)
                              CustomLoader.instance.hideLoaderView()
                              UIApplication.shared.endIgnoringInteractionEvents()
                              if let err = error as? URLError, err.code == .notConnectedToInternet {
                                  // Your device does not have internet connection!
                                  debugPrint("Your device does not have internet connection!")
                                  self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                  debugPrint(err)
                              }
                              else if error._code == NSURLErrorTimedOut {
                                  debugPrint("Request timeout!")
                                  self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                              }else {
                                  // other failures
                                  self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                              }
                          case .success:
                              let result = response.result
                              CustomLoader.instance.hideLoaderView()
                              UIApplication.shared.endIgnoringInteractionEvents()
                              if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                  if let invalidToken = dict1["detail"]{
                                      if invalidToken as! String  == "Invalid token."
                                      { // Create the alert controller
                                          self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                          let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                          let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                          let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                          appDelegate.window?.rootViewController = redViewController
                                      }
                                  }
                          
                          else if let json = response.result.value {
                              let jsonString = "\(json)"
                             
                                  
                                      var first_name = ""
                                      var last_name = ""
                                      var profile_pic_url = "";
                                      var user_feed_commentid = 0;
                                      var user_id = 0
                                      
                                      if dict1["response"] is NSNull
                                      {}
                                      else
                                      {
                                          
                                      }
                                      
                                      
                               
                              self.commentText2!.text! = ""
                                      
                          DispatchQueue.main.async {
                          self.productDetailsTable.resignFirstResponder()
                          self.replyStatus=false
                          self.data.removeAll()
                          self.owner.removeAll()
                          self.fetchDetails()
                          CustomLoader.instance.hideLoaderView()
                      UIApplication.shared.endIgnoringInteractionEvents()
                                  
                                      }
                              
                              }
                              }}}}
                          
                      else
                      {
                          CustomLoader.instance.hideLoaderView()
                          UIApplication.shared.endIgnoringInteractionEvents()
                          let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                          let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                          let appDelegate = UIApplication.shared.delegate as! AppDelegate
                          appDelegate.window?.rootViewController = redViewController
                      }
                  }
                  else
                  {
                      self.view.makeToast("Please type comment first.", duration: 2.0, position: .center)
                      }
                
                  
              }
       
       
   }


