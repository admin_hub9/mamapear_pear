//
//  CustomTabBarController.swift
//  Hub9
//
//  Created by Deepak on 10/9/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

class CustomTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.delegate = self
        
        self.viewControllers = [createDummyNavControllerWithTitle("News", imageName: "icon_news", viewController: MainController()),
                                createDummyNavControllerWithTitle("Games", imageName: "icon_games", viewController: GamesController()),
                                createDummyNavControllerWithTitle("Students", imageName: "icon_students", viewController: StudentsController()),
                                createDummyNavControllerWithTitle("Ranking", imageName: "icon_rank", viewController: StatsController()),
                                createDummyNavControllerWithTitle("Info", imageName: "icon_info", viewController: InfoController())]
    }
    
    func tabBarController(tabbarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        
        self.title = viewController.title
    }
    
    // Shortcut function for the tab bar items
    private func createDummyNavControllerWithTitle(title: String, imageName: String, viewController: UIViewController) -> UINavigationController {
        
        viewController.title = title
        
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = UIImage(named: imageName)?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        return navController
    }
}
