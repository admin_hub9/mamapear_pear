//
//  UserProfileViewController.swift
//  Hub9
//
//  Created by Deepak on 3/4/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import AWSS3
import AWSCore
import MapKit
import GoogleMaps
import GooglePlaces


class UserProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var native = UserDefaults.standard
    var firstname = "Deepak"
    var lastname = "Srivastav"
    var imageurl = ""
    var timestamp = ""
    var imagePicker = UIImagePickerController()
    var notificationis = "1"
    var latitue = Double()
    var longitute = Double()
    var location_name = ""
    var languageData = [
        [
            "language_name": "Hindi",
            "languageid": 1
        ],
        [
            "language_name": "English",
            "languageid": 2
        ],
        [
            "language_name": "Simplified Chinese",
            "languageid": 3
        ]
    ]
    
    var languageID = 1
    
    
    //location
    var GoogleMapsAPIServerKey = ""
    var cityName1 = ""
    var stateName1 = ""
    var region = ""
    var is_validated = false
    var countryid = 0
    
    
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var changeImage: UIButton!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var languageContainer: UIView!
    @IBOutlet weak var languagePicker: UIPickerView!
    @IBOutlet weak var languageText: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var locationText: UITextField!
    
    
    
    
    @IBOutlet weak var saveProfile: UIBarButtonItem!
    
    @IBAction func saveProfile(_ sender: Any) {
        updateUserInfo()
    }
    
    // to display number of row for each colomn in picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //        debugPrint(pickerView)
        //        return pickerData[component].count
        var row = pickerView.selectedRow(inComponent: 0)
        debugPrint("this is the pickerView\(row)")
        return 3
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.languageID = languageData[row]["languageid"] as! Int
        
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //        return pickerData[component][row]
        
        return languageData[row]["language_name"] as! String
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == firstName {
            lastName.becomeFirstResponder()
        } else if textField == lastName {
            email.becomeFirstResponder()
        } else if textField == email {
            //            gstNumber.becomeFirstResponder()
        }
            //        else if textField == gstNumber {
            //            gstNumber.resignFirstResponder()
            //        }
        else
        {
            view.endEditing(true)
        }
        return true
    }
    
    
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        firstName.resignFirstResponder()
        lastName.resignFirstResponder()
        email.resignFirstResponder()
        phoneNumber.resignFirstResponder()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 50
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 50
            }
        }
    }
    @IBAction func selectLanguageButton(_ sender: Any) {
        UIView.transition(with: languageContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                    self.blurView.alpha = 0.6
                self.languageContainer.transform = CGAffineTransform(translationX: 0, y: 300)
                self.languageContainer.isHidden = false
        }, completion: nil)
    }
    
    @IBAction func cancelPicker(_ sender: Any) {
        UIView.transition(with: languageContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                    self.blurView.alpha = 0.6
                self.languageContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                self.languageContainer.isHidden = true
        }, completion: nil)
    }
    
    @IBAction func SaveLanguage(_ sender: Any) {
        self.languageText.text = languageData[languageID - 1]["language_name"] as! String
        UIView.transition(with: languageContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                    self.blurView.alpha = 0.6
                self.languageContainer.transform = CGAffineTransform(translationX: 0, y: 300)
                self.languageContainer.isHidden = true
        }, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: #selector(self.action(sender:)))
        
        
        CustomLoader.instance.showLoaderView()
        getUserInfo()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        if native.string(forKey: "phone") != nil
            
        {
            if self.isStringContainsOnlyNumbers(string: native.string(forKey: "phone") ?? "") {
                debugPrint("hdghjcgj", native.string(forKey: "phone")!)
                phoneNumber.text = native.string(forKey: "phone")!
            } else {
                debugPrint("hdghjcgj", "No")
            }
            phoneNumber.isEnabled = false
        }
        
        
        
        //        navigationController!.navigationBar.barTintColor = UIColor.green
        // Do any additional setup after loading the view.
    }
    
    
    
    
    @IBAction func changeImage(_ sender: Any) {
        self.view.endEditing(true)
        btnClicked()
    }
    
    func btnClicked() {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    // open camera
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    // open Gallery
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //MARK:-- ImagePicker delegate
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        if #available(iOS 11.0, *) {
            if let imgUrl = info[UIImagePickerControllerImageURL] as? URL{
                let imgName = imgUrl.lastPathComponent
                let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                let localPath = documentDirectory?.appending(imgName)
                
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                let data = UIImagePNGRepresentation(image)! as NSData
                data.write(toFile: localPath!, atomically: true)
                //let imageData = NSData(contentsOfFile: localPath!)!
                let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
                debugPrint(photoURL)
                uploadPhotos(Image: image)
                
            }
        } else {
            // Fallback on earlier versions
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    // upload Image
    func uploadPhotos(Image: UIImage)
        
    {
        let accessKey = root.s3AccessKeyId
        let secretKey = root.s3SecretAccessKey
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        //        var companyID = native.string(forKey: "shopid")!
        //        debugPrint(companyID)
        var firebaseid = native.string(forKey: "firebaseid")!
        var userid = native.string(forKey: "userid")!
        let S3BucketName = "\(root.s3Bucket)/user-profile/\(userid)"+"/image"
        
        let remoteName = String("image\(self.timestamp)") + ".jpeg"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
        
        if let data = UIImageJPEGRepresentation(Image, 0.2){
            debugPrint(data.count)
            
            do {
                debugPrint("-------", fileURL)
                try data.write(to: fileURL)
            }
            catch {}
        }
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/JPEG"
        uploadRequest.acl = .bucketOwnerRead
        
        let transferManager = AWSS3TransferManager.default()
        
        
        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
            DispatchQueue.main.async {
                
            }
            
            if let error = task.error {
                //                self!.blurView.alpha = 0
                //                self!.activity.stopAnimating()
                debugPrint("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    debugPrint("Uploaded to:\(absoluteString)")
                    //                    self?.s3count = (self?.s3count)! + 1
                    //                    if(self?.s3count == self?.selectedImage.count)
                    //                    {
                    debugPrint("uplaoding successfully")
                    //                    self!.blurView.alpha = 0
                    //                    self!.activity.stopAnimating()
                    
                    self!.imageurl = absoluteString
                    if self!.imageurl != nil
                    {
                        let imageurl = NSURL(string: self!.imageurl)
                        DispatchQueue.main.async {
                            
                        }
                        DispatchQueue.main.async {
                            self!.userImage.sd_setImage(with: URL(string: self!.imageurl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                    //                    self!.updateUserInfo()
                    
                    //                    }
                }
            }
            
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            return nil
        }
        
        
    }
    func validateGST(YourGSTNumber: String) -> Bool {
        let REGEX: String
        debugPrint("REGEX")
        REGEX = "(?<! )[-a-zA-Z' ]{2,26}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourGSTNumber)
    }
    
    
    func validateEmail(YourEMailAddress: String) -> Bool {
        let REGEX: String
        debugPrint("REGEX")
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
    }
    
    
    func validateName(YourName: String) -> Bool {
        let REGEX: String
        debugPrint("REGEX")
        REGEX = "^[a-zA-Z]+$"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourName)
    }
    func validateLastName(YourName: String) -> Bool {
        let REGEX: String
        debugPrint("REGEX")
        REGEX = "[a-zA-Z]+$"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourName)
    }
    
    //    func isValidInput(Input:String) -> Bool {
    //        let RegEx = "\\w{0,18}"
    //        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
    //        return Test.evaluate(with: Input)
    //    }
    func validation() -> Bool {
        var valid: Bool = true
        if validateEmail(YourEMailAddress: email.text!) == false{
            // change placeholder color to red color for textfield email-id
            email.text?.removeAll()
            email.attributedPlaceholder = NSAttributedString(string: "Invalid Email id", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        //        if validateGST(YourGSTNumber: gstNumber.text!) == false && gstNumber.text!.count > 0{
        //            // change placeholder color to red color for textfield email-id
        //            gstNumber.text?.removeAll()
        //            gstNumber.attributedPlaceholder = NSAttributedString(string: "Please enter 15 digit GST Number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
        //            valid = false
        //        }
        if validateName(YourName: firstName.text!) == false{
            // change placeholder color to red color for textfield email-id
            firstName.text?.removeAll()
            firstName.attributedPlaceholder = NSAttributedString(string: "Please enter correct name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if validateLastName(YourName: lastName.text!) == false{
            // change placeholder color to red color for textfield email-id
            lastName.text?.removeAll()
            lastName.attributedPlaceholder = NSAttributedString(string: "Please give valid Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        
        
        return valid
    }
    
    // send Update user info
    func updateUserInfo()
    {
        CustomLoader.instance.showLoaderView()
        if validation() == true
        {
            
            if native.object(forKey: "userid") != nil
            {
                var userid = native.string(forKey: "userid")!
                //            let userCode = native.string(forKey: "userCode")
                let parameters: [String:Any]
                var Pushtoken = ""
                debugPrint("Pushtoken", Pushtoken)
                var firebaseid = native.string(forKey: "firebaseid")!
                self.timestamp = "\(NSDate().timeIntervalSince1970 * 1000)"
                
                if imageurl == "" && userImage != nil
                {
                    imageurl = "https://s3.amazonaws.com/\(root.s3Bucket)/user-profile/\(userid)/image/image\(self.timestamp).jpeg"
                    
                }
                parameters = ["first_name":firstName.text!,"firebase_userid": firebaseid, "last_name": lastName.text!,"email":email.text!, "profile_pic_url": imageurl, "languageid": languageID,"user_gst_number": "", "latitude": self.latitue, "longitude": self.longitute, "location_name": self.location_name ]
                
                let token = self.native.string(forKey: "Token")!
                debugPrint(parameters)
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                debugPrint("Successfully post")
                let b2burl = native.string(forKey: "b2burl")!
                Alamofire.request("\(b2burl)/users/update_user_profile/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        //                    self.blurView.alpha = 0
                        //                    self.activity.stopAnimating()
                        //                    self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    if let json = response.result.value {
                        debugPrint("JSON: \(json)")
                        let jsonString = "\(json)"
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            switch response.result
                            {
                            case .failure(let error):
                                //                debugPrint(error)
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let err = error as? URLError, err.code == .notConnectedToInternet {
                                    // Your device does not have internet connection!
                                    debugPrint("Your device does not have internet connection!")
                                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                    debugPrint(err)
                                }
                                else if error._code == NSURLErrorTimedOut {
                                    debugPrint("Request timeout!")
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                                }else {
                                    // other failures
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                                }
                                
                            case .success:
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                    if let invalidToken = dict1["detail"]{
                                        if invalidToken as! String  == "Invalid token."
                                        { // Create the alert controller
                                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            appDelegate.window?.rootViewController = redViewController
                                        }
                                    }
                                    else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                                    {
                                        self.blurView.alpha = 0
                                        
                                        self.native.set(self.latitue, forKey: "latitute")
                                        self.native.set(self.longitute, forKey: "longitute")
                                        self.native.set(self.location_name, forKey: "location_name")
                                        self.native.synchronize()
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        debugPrint(dict1["status"]!)
                                        //                            debugPrint(dict1["data"]!)
                                        let appearance = SCLAlertView.SCLAppearance(
                                            showCloseButton: false
                                        )
                                        let alertView = SCLAlertView(appearance: appearance)
                                        alertView.addButton("OK") {
                                            debugPrint("Second button tapped")
                                            self.navigationController?.popViewController(animated: true)
                                            //                                self.dismiss(animated: true, completion: nil)
                                        }
                                        alertView.showSuccess(dict1["status"]! as! String, subTitle: dict1["response"]! as! String)
                                        
                                        
                                    }
                                    else
                                    {
                                        self.blurView.alpha = 0
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        debugPrint(dict1["status"] as AnyObject)
                                        debugPrint(dict1["response"] as AnyObject)
                                        let alert = UIAlertController(title: dict1["status"]! as! String, message: dict1["response"]! as! String, preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                            }
                        }}
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
            }
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }}
    
    func getUserInfo()
    {
        
        CustomLoader.instance.showLoaderView()
        //            let userCode = native.string(forKey: "userCode")
        let parameters: [String:Any]
        var Pushtoken = ""
        debugPrint("Pushtoken", Pushtoken)
        
        
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        debugPrint("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        Alamofire.request("\(b2burl)/users/get_user_profile/", encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            guard response.data != nil else { // can check byte count instead
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                //                    self.blurView.alpha = 0
                //                    self.activity.stopAnimating()
                //                    self.activityIndicator.stopAnimating()
                
                return
            }
            if let json = response.result.value {
                debugPrint("JSON: \(json)")
                let jsonString = "\(json)"
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    switch response.result
                    {
                    case .failure(let error):
                        //                debugPrint(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            debugPrint("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            debugPrint(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            debugPrint("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                            {
                                self.blurView.alpha = 0
                                CustomLoader.instance.hideLoaderView()
                                debugPrint(dict1["status"]!)
                                //                            debugPrint(dict1["data"]!)
                                if let data = dict1["response"] as? NSDictionary
                                {
                                    if data["email"] is NSNull && !(self.isValidEmail(data["email"] as! String))
                                    {}
                                    else
                                    {
                                        self.email.text = "\((data["email"] as? String)!)"
                                    }
                                    if data["latitude"] is NSNull
                                    {}
                                    else if let lat = data["latitude"] as? Double
                                    {
                                        self.latitue = lat
                                        
                                    }
                                    if data["longitude"] is NSNull
                                    {}
                                    else if let long = data["longitude"] as? Double
                                    {
                                        self.longitute = long
                                    }
                                    if data["location_name"] is NSNull
                                    {
                                        self.locationText.text = ""
                                    }
                                    else if let loc = data["location_name"] as? String
                                    {
                                        self.location_name = loc
                                        if self.location_name != ""
                                        {
                                            self.locationText.text = self.location_name
                                        }
                                        else
                                        {
                                            self.locationText.text = ""
                                        }
                                    }
                                    
                                    self.native.set(self.latitue, forKey: "latitute")
                                    self.native.set(self.longitute, forKey: "longitute")
                                    self.native.set(self.location_name, forKey: "location_name")
                                    self.native.synchronize()
                                    
                                    if data["first_name"] is NSNull
                                    {}
                                    else
                                    {
                                        self.firstName.text = (data["first_name"] as? String)!
                                    }
                                    if data["last_name"] is NSNull
                                    {}
                                    else
                                    {
                                        self.lastName.text = "\((data["last_name"] as? String)!)"
                                    }
                                    if data["phone"] is NSNull
                                    {}
                                    else
                                    {
                                        self.phoneNumber.text = "\((data["phone"] as? String)!)"
                                    }
                                    //                                if data["user_gst_number"] is NSNull
                                    //                                {}
                                    //                                else
                                    //                                {
                                    //                                self.gstNumber.text = "\((data["user_gst_number"] as? String)!)"
                                    //                                }
                                    if data["languageid"] is NSNull
                                    {}
                                    else
                                    {
                                        var langid = (data["languageid"] as? Int)!
                                        
                                        self.languageID = langid
                                        self.languageText.text = self.languageData[langid - 1]["language_name"] as! String
                                    }
                                    if data["profile_pic_url"] is NSNull
                                    {}
                                    else
                                    {
                                        var image = data["profile_pic_url"] as? String
                                        self.imageurl = image!
                                        self.userImage.image = nil
                                        if image != nil
                                        {
                                            let imageurl = NSURL(string: image!)
                                            DispatchQueue.main.async {
                                                self.userImage.sd_setImage(with: URL(string: self.imageurl), placeholderImage: UIImage(named: "thumbnail"))
                                            }
                                        }
                                    }
                                }
                                
                            }
                            else
                            {
                                self.blurView.alpha = 0
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                debugPrint(dict1["status"] as AnyObject)
                                debugPrint(dict1["response"] as AnyObject)
                                let alert = UIAlertController(title: dict1["status"]! as! String, message: dict1["response"]! as! String, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }}
                }
            }
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            return
        }
        
    }
    
    
    @IBAction func selectLocation(_ sender: Any) {
        
        self.searchAddress()
        
    }
    
    func searchAddress() {
        
        var country_code = ""
        if native.object(forKey: "country_code") != nil {
            
            country_code = native.string(forKey: "country_code")!
            
        }
        if native.object(forKey: "countryid") != nil {
            
            
            self.countryid = Int(native.string(forKey: "countryid")!)!
            
        }
        GoogleMapsAPIServerKey = "AIzaSyAOycE9VXNxb9_cvKrsKr6KT7Am9jxS2BQ"
        if GoogleMapsAPIServerKey != "" && country_code != ""
        {
            GMSServices.provideAPIKey(GoogleMapsAPIServerKey)
            GMSPlacesClient.provideAPIKey(GoogleMapsAPIServerKey)
            
            let autocompletecontroller = GMSAutocompleteViewController()
            
            autocompletecontroller.delegate = self
            
            let filter = GMSAutocompleteFilter()
            //            filter.type = .address  //suitable filter type
            filter.country = country_code  //appropriate country code
            autocompletecontroller.autocompleteFilter = filter
            
            present(autocompletecontroller, animated: true, completion: nil)
            
            // changes the color of the sugested places
            autocompletecontroller.primaryTextColor = UIColor.black
            autocompletecontroller.secondaryTextColor = UIColor.gray
            UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = UIColor.black
            
        }
        else
        {
            self.view.makeToast("Please try later!", duration: 2.0, position: .center)
        }
    }
    
    
}

extension UserProfileViewController: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        debugPrint(error.localizedDescription)
    }
    
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        debugPrint("place", place.coordinate.latitude)
        self.latitue = place.coordinate.latitude
        self.longitute = place.coordinate.longitude
        debugPrint("Place name: ", place.name)
        
        debugPrint("Place address: ", place.formattedAddress)
        var country = ""
        var pincode = ""
        var address = ""
        address = place.name!
        debugPrint("Place attributions: ", place.attributions)
        //Get address Seperated like city,country,postalcode
        
        let arrays : Array = place.addressComponents!
        for i in 0..<arrays.count {
            
            let dics : GMSAddressComponent = arrays[i]
            let str : String = dics.type
            
            if (str == "country") {
                debugPrint("Country: \(dics.name)")
                country = "\(dics.name)"
                
            }
            else if (str == "administrative_area_level_1") {
                debugPrint("State: \(dics.name)")
                
                self.stateName1 = "\(dics.name)"
                
            }
            else if (str == "administrative_area_level_2") {
                debugPrint("State: \(dics.name)")
                
                self.region = "\(dics.name)"
                
            }
            else if (str == "locality") {
                debugPrint("City: \(dics.name)")
                self.cityName1 = "\(dics.name)"
                
            }
            else if (str == "postal_code"){
                debugPrint("PostalCode:\(dics.name)")
                pincode = dics.name
            }// this is only to get postalcode of the selected nearby location
        }
        if cityName1 != "" && self.region != "" && self.stateName1 != "" && place.formattedAddress != nil
        {
            
            //address
            self.location_name = "\(cityName1), \(self.stateName1)"
            self.locationText.text = location_name
            if location_name == ""
            {
                self.locationText.text = ""
                
            }
            else
            {
                self.locationText.text = "\(location_name)"
            }
            //            self.city.text = "\(cityName1),\(self.stateName1), \(self.region)"
            var country_code = ""
            if native.object(forKey: "country_code") != nil {
                
                country_code = native.string(forKey: "country_code")!
                
            }
            if pincode != ""
            {
                //                self.pincode.isEnabled = false
                self.is_validated=true
                //                self.pincode.text = pincode
            }
            else
            {
                //                self.pincode.isEnabled = true
                self.is_validated=false
                //                self.pincode.text = ""
                //                self.view.makeToast("Please enter correct postal code!", duration: 2.0, position: .center)
            }
        }
        else
        {
            //          self.view.makeToast("Sorry this address is not servicable!", duration: 2.0, position: .center)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // To handle error
        debugPrint(error)
        
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    // Function to check valid Email
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isStringContainsOnlyNumbers(string: String) -> Bool {
        return string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    
}



