

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import FirebaseDatabase
import FirebaseCore



class User: NSObject {
    
    //MARK: Properties
    let name: String
    let userid: String
    let id: String
    var profilePic: String

    class func downloadAllUsers(exceptID: String, completion: @escaping (User) -> Swift.Void) {

//        print("user info.....contact download",exceptID)
        let db = Firestore.firestore()

        db.collection("chatRooms").whereField("otherUserId", isEqualTo:exceptID).order(by: "lastConversationTime", descending: true)
            .addSnapshotListener() { (querySnapshot, err) in
//                print("database error", err)
                guard let snapshot = querySnapshot else {
                    print("Error retreiving snapshots \(err!)")
                    return
                }
                print("snapshot", snapshot)
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){

                        guard let userinfo: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        
                        let userid = userinfo["userId"] as! [String:AnyObject]
                        let id = diff.document.documentID
                        let credentials: [String:String] = userinfo["userInfo"] as! [String: String]
                        print("snapshot", snapshot)
//                        print("credentials", credentials)
                        var name = credentials["first_name"]!
                        var lastname = credentials["last_name"]!
                        var image = credentials["profileImageUrl"] as! String
//                                                print(image)
//                        if image == ""
//                        {
//                            image = "https://s3.amazonaws.com/b2b-product-images/1.jpg"
//                        }
                        var link = URL.init(string: image)
                        URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                            if error == nil {
                                let profilePic = UIImage.init(data: data!)
                                if lastname != nil
                                {
                                    name = name + " " + lastname
                                }
                                let user = User.init(name: name, userid: userid as! String, id: id as! String, profilePic: image)
                                completion(user)
                            }
                        }).resume()
                    }
                }
        }
        db.collection("chatRooms").whereField("userId", isEqualTo:exceptID).order(by: "lastConversationTime", descending: true)
            .addSnapshotListener() { (querySnapshot, err) in

                guard let snapshot = querySnapshot else {
//                    print("Error retreiving snapshots \(err!)")
                    return
                }

                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){

                        guard let userinfo: [String:Any] = diff.document.data()
                            else {
                                return
                        }

                        let id = diff.document.documentID
                        let userid = userinfo["otherUserId"]!
//                        print("++++++", userinfo["otherUserInfo"]!)
                        let credentials: [String:String] = userinfo["otherUserInfo"] as! [String: String]
                        var name = credentials["first_name"]!
                        var lastname = credentials["last_name"]!
                        var image = credentials["profileImageUrl"]!
//                                                print("image", image)
//                        if image == ""
//                        {
//                            image = "https://s3.amazonaws.com/b2b-product-images/1.jpg"
//                        }
                        var link = URL.init(string: image)
                        URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                            if error == nil {
                                let profilePic = UIImage.init(data: data!)
                                if lastname != nil
                                {
                                    name = name + " " + lastname
                                }
                                let user = User.init(name: name, userid: userid as! String, id: id as! String, profilePic: image)
                                completion(user)
                            }
                        }).resume()
                    }
                }
        }
    }


    class func info(forUserID: String, completion: @escaping (User) -> Swift.Void) {

        let db = Firestore.firestore()

        db.collection("chatRooms").whereField("otherUserId", isEqualTo: forUserID)
            .addSnapshotListener() { (querySnapshot, err) in

                guard let snapshot = querySnapshot else {
//                    print("Error retreiving snapshots \(err!)")
                    return
                }

                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){

                        guard let userinfo: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        let id = diff.document.documentID
                        let userid = userinfo["userId"]!
                        print("0000000000000", userinfo["userInfo"]!)
                        let credentials: [String:String] = userinfo["userInfo"] as! [String: String]
                        var name = credentials["first_name"]!
                        let lastname = credentials["last_name"]!
print("credentialsprofileImageUrl", credentials["profileImageUrl"]!)
                        let image = credentials["profileImageUrl"] as! String
                        var profileImage: UIImage
                        
//                        if image != nil && image != ""
//                        {
//                            profileImage = UIImage(named: image!)
//                    }
                        let user = User.init(name: name, userid: userid as! String, id: id as! String, profilePic: image)
                                completion(user)
                    }
                }
        }
        db.collection("chatRooms").whereField("userId", isEqualTo: forUserID)
            .addSnapshotListener() { (querySnapshot, err) in

                guard let snapshot = querySnapshot else {
//                    print("Error retreiving snapshots \(err!)")
                    return
                }

                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){

                        guard let userinfo: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        let id = diff.document.documentID
                        let userid = userinfo["otherUserId"]
//                        print(userinfo["userInfo"]!, "userinfo", userinfo)
                        let credentials: [String:String] = userinfo["otherUserInfo"] as! [String: String]
//                        print("credentials", credentials)
                        var name = credentials["first_name"]!
                        let lastname = credentials["last_name"]!
                        var image = credentials["profileImageUrl"] as! String
                        var link = URL.init(string: credentials["profileImageUrl"]!)
//                        print(link)
                        if link == nil
                        {
                            link = URL.init(string: "bulkbyte")
                        }
                        URLSession.shared.dataTask(with: link!, completionHandler: { (data, response, error) in
                            if error == nil {
                                var profilePic = UIImage.init(data: data!)
//                                print(name,userid, id, profilePic)
                                if lastname != nil
                                {
                                name = name + " " + lastname
                                }
//                                print(name, userid!, id, profilePic!)
                                profilePic = profilePic!
                                let user = User.init(name: name, userid: userid as! String, id: id , profilePic: image )
                                completion(user)
                            }
                        }).resume()
                    }
                }
        }

    }

    
    
    class func checkUserVerification(completion: @escaping (Bool) -> Swift.Void) {
        Auth.auth().currentUser?.reload(completion: { (_) in
            let status = (Auth.auth().currentUser?.isEmailVerified)!
            completion(status)
        })
    }

    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    //MARK: Inits
    init(name: String, userid: String, id: String, profilePic: String) {
        self.name = name
        self.userid = userid
        self.id = id
        self.profilePic = profilePic
    }
}

