//
//  FeedViewController.swift
//  Hub9
//
//  Created by Deepak on 18/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import AVKit
import Alamofire
import AttributedTextView
import SDWebImage


var FeedViewData = FeedViewController()


class FeedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, PinterestLayoutDelegate, UIViewControllerTransitioningDelegate, UISearchBarDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    
    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    
    var refreshControl = UIRefreshControl()
    let imageCache = NSCache<AnyObject, AnyObject>()
    var returnImage:UIImage = UIImage()
    let native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var user_purchase_modeid = "1"
    
    
    
    
    var limit = 20
    var offset = 0
    var feedDetailsIndex = 0
    
    /// data
    var userImage = [String]()
    var userName = [String]()
    var uploadTime = [Double]()
    var feedVideo = [String]()
    var feedGif = [String]()
    var like = [Int]()
    var Comment = [Int]()
    
    var feed_users = [AnyObject]()
    var feedImage = [String]()
    var feed_id = [Int]()
    var feedUser_Image = [String]()
    var user_feedid = [Int]()
    var feed_text = [String]()
    var user_Comment_data = [[AnyObject]]()
    var isFeedID_liked = [Bool]()
    var shareLink = [String]()
    static var selectedIndex = 0
    static var user_feed_id = 0
    var searchText = ""
    var isSearching = false
    
    
    
    @IBOutlet var searchbar: UISearchBar!
    @IBOutlet weak var searchContainer: UIView!
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var productCollection: UICollectionView!
    
    //search view
    @IBAction func searchButton(_ sender: Any) {
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "feedSearch"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    
    
    @IBAction func addfeed(_ sender: Any) {
        UserFeedUploadViewController.is_edit = false
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "feedUpload"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    
    
    /**
     Simple Alert
     - Show alert with title and alert message and basic two actions
     */
    func showSimpleAlert() {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Post", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            debugPrint("Saved")
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "feedUpload"))! as UIViewController
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
            self.navigationController?.pushViewController(rootPage, animated: true)
        })
        
        let deleteAction = UIAlertAction(title: "Add Question", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            debugPrint("Deleted")
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "Comments"))! as UIViewController
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
            self.navigationController?.pushViewController(rootPage, animated: true)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            debugPrint("Cancelled")
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        attemptToHidKeyboard()
        searchbar.resignFirstResponder()
        self.tabBarController?.tabBar.isHidden = false
        self.avPlayer.play()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        
        super.viewWillDisappear(animated)
        attemptToHidKeyboard()
        searchbar.resignFirstResponder()
        self.avPlayer.pause()
        NotificationCenter.default.post(name: Notification.Name("avPlayerDidDismiss"), object: nil, userInfo: nil)
        
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Api Data
        refreshControl.beginRefreshing()
        //        self.updateView()
        FeedViewController.selectedIndex = 0
        FeedViewController.user_feed_id = 0
        self.clearData()
        
        
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            attemptToHidKeyboard()
        }
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            attemptToHidKeyboard()
        }
    }
    
    private func attemptToHidKeyboard() {
        self.searchbar.resignFirstResponder()
        if let cancelButton : UIButton = searchbar.value(forKey: "cancelButton") as? UIButton{
            cancelButton.isEnabled = true
        }
        self.searchbar.endEditing(true)
        self.view.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("cancel")
        searchBar.resignFirstResponder()
        attemptToHidKeyboard()
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        attemptToHidKeyboard()
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        //        debugPrint("gfghffhj")
        var heightInPoints = 250
        let image =  UIImage(named: "\(self.feedImage[indexPath.row])")
        debugPrint(image?.size.height)
        
        //last cell's width
        if(  (indexPath.row + 1) % 2 == 0){
            
        }else {
            heightInPoints = heightInPoints + 60
        }
        
        
        return CGFloat(heightInPoints)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let layout = productCollection.collectionViewLayout as? PinterestLayout
        {
            layout.delegate = self
        }
        productCollection.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
        if #available(iOS 13.0, *) {
            searchbar.searchTextField.font = UIFont(name: "Helvetica Neue", size: 12)
        } else {
            // Fallback on earlier versions
        }
        if native.object(forKey: "deeplink") != nil {
            if native.object(forKey: "deeplink") as? String == "true"
            {
                native.set("false", forKey: "deeplink")
                native.synchronize()
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
                self.navigationController?.pushViewController(editPage, animated: true)
            }
        }
        searchView.layer.cornerRadius = searchView.frame.height/2
        searchView.clipsToBounds=true
        // refresh Table
        // Add Refresh Control to Table View
        
        
        productCollection.isHidden = true
        CustomLoader.instance.showLoaderView()
        self.clearData()
        // Configure Refresh Control
        refreshControl = UIRefreshControl()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        if #available(iOS 10.0, *) {
            self.productCollection.refreshControl = self.refreshControl
        } else {
            // Fallback on earlier versions
            self.productCollection.addSubview(self.refreshControl)
        }
        native.set(0, forKey: "loggedIn")
        native.synchronize()
        FeedViewData = self
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        self.tabBarController?.tabBar.isHidden = true
        
        
        
        view.endEditing(true)
        view.resignFirstResponder()
        resignFirstResponder()
        searchbar.delegate = self
        
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func userRedirect(sender: UIButton)
    {
        native.set(self.feed_id[sender.tag], forKey: "feed_userid")
        native.synchronize()
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "UserFeedPost"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    func clearData()
    {
        
        
        feedUser_Image.removeAll()
        user_feedid.removeAll()
        feedImage.removeAll()
        feed_text.removeAll()
        
        userImage.removeAll()
        userName.removeAll()
        uploadTime.removeAll()
        feedVideo.removeAll()
        feedGif.removeAll()
        like.removeAll()
        Comment.removeAll()
        feed_users.removeAll()
        feed_id.removeAll()
        user_Comment_data.removeAll()
        isFeedID_liked.removeAll()
        shareLink.removeAll()
        FeedViewController.selectedIndex = 0
        DispatchQueue.main.async {
            self.productCollection.reloadData()
        }
        self.limit = 20
        self.offset = 0
        getRecommended_product(limit: limit, offset: offset)
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        self.view.resignFirstResponder()
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.50)
    }
    
    @objc func reload(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespaces) != "" else {
            debugPrint("nothing to search")
            self.deleteData()
            isSearching = true
            searchText = ""
            getRecommended_product(limit: self.limit, offset: self.offset)
            return
        }
        debugPrint(query)
        isSearching = false
        if query == nil || query == ""
        {
            
            isSearching = false
            view.endEditing(true)
            self.deleteData()
        }
        else if (query.count) >= 3
        {
            isSearching = true
            searchText = query
            getRecommended_product(limit: self.limit, offset: self.offset)
        }
            
        else
        {
            self.deleteData()
        }
    }
    
    
    
    func deleteData()
    {
        feedUser_Image.removeAll()
        user_feedid.removeAll()
        feedImage.removeAll()
        feed_text.removeAll()
        
        userImage.removeAll()
        userName.removeAll()
        uploadTime.removeAll()
        feedVideo.removeAll()
        feedGif.removeAll()
        like.removeAll()
        Comment.removeAll()
        feed_users.removeAll()
        feed_id.removeAll()
        user_Comment_data.removeAll()
        isFeedID_liked.removeAll()
        shareLink.removeAll()
        FeedViewController.selectedIndex = 0
        DispatchQueue.main.async {
            self.productCollection.reloadData()
        }
        self.limit = 20
        self.offset = 0
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleCells: [IndexPath] = self.productCollection.indexPathsForVisibleItems
        let lastIndexPath = IndexPath(item: (self.feed_users.count - 1), section: 0)
        if visibleCells.contains(lastIndexPath) {
            //This means you reached at last of your datasource. and here you can do load more process from server
            //            debugPrint("last index ", lastIndexPath)
            offset = offset + 10
            getRecommended_product(limit: limit, offset: offset)
        }
    }
    
    func getRecommended_product(limit: Int, offset: Int)
    {
        
        CustomLoader.instance.showLoaderView()
        let token = self.native.string(forKey: "Token")!
        //            debugPrint("token9894579", token)
        let b2burl = native.string(forKey: "b2burl")!
        //            if native.object(forKey: "userid") != nil
        //            {
        searchText = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed/?limit=\(limit)&offset=\(offset)&feed_typeid=2&search_query=\(searchText.lowercased())") as! URL)
        if token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        debugPrint("yyugdfhjb", a, token)
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            //                debugdebugPrint(response)
            debugPrint("response", response)
            switch response.result {
            case .success:
                
                if let result1 = response.result.value
                {
                    let result = response.result
                    
                    debugPrint("result feed video", response)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if let innerdict = dict1["feed_data"] as? [AnyObject]
                            {
                                if innerdict.count > 0
                                {
                                    for i in 0..<innerdict.count {
                                        
                                        if innerdict[i]["feed_share_link"] is NSNull
                                        {
                                            self.shareLink.append("pears.com")
                                        }
                                        else
                                        {
                                            self.shareLink.append(innerdict[i]["feed_share_link"] as! String)
                                        }
                                        
                                        
                                        
                                        
                                        if innerdict[i]["comment_data"] is NSNull
                                        {
                                            var data = [AnyObject]()
                                            self.user_Comment_data.append(data)
                                        }
                                        else
                                        {
                                            debugPrint(i)
                                            self.user_Comment_data.append((innerdict[i]["comment_data"] as? [AnyObject])!)
                                        }
                                        
                                        if innerdict[i]["like_data"] is NSNull
                                        {
                                            self.isFeedID_liked.append(false)
                                        }
                                        else
                                        { self.feed_users.append(innerdict[i]["like_data"] as! AnyObject)
                                            
                                        }
                                        
                                        if self.native.object(forKey: "userid") != nil
                                        {
                                            let userid = self.native.string(forKey: "userid")!
                                            if self.feed_users.count > i
                                            {
                                                if userid != ""
                                                {
                                                    var data = self.feed_users[i] as! AnyObject
                                                    if data.contains(Int(userid))
                                                    {
                                                        self.isFeedID_liked.append(true)
                                                        
                                                    }
                                                    else
                                                    {  self.isFeedID_liked.append(false)
                                                        
                                                    }
                                                    
                                                }
                                                else
                                                {
                                                    self.isFeedID_liked.append(true)
                                                }
                                            }}
                                        if let user_data =  innerdict[i]["feed_user_data"] as? AnyObject
                                        {
                                            var firstName = ""
                                            var lastName = ""
                                            
                                            if user_data["userid"] is NSNull
                                            {
                                                self.feed_id.append(0)
                                            }
                                            else
                                            {
                                                self.feed_id.append((user_data["userid"] as? Int)!)
                                            }
                                            if user_data["profile_pic_url"] is NSNull
                                            {
                                                self.feedUser_Image.append("")
                                            }
                                            else
                                            {
                                                self.feedUser_Image.append((user_data["profile_pic_url"] as? String)!)
                                            }
                                            if innerdict[i]["user_feedid"] is NSNull
                                                
                                            {
                                                self.user_feedid.append(0)
                                            }
                                            else
                                            {
                                                self.user_feedid.append((innerdict[i]["user_feedid"] as? Int)!)
                                            }
                                            
                                            if innerdict[i]["feed_user_data"] is NSNull
                                                
                                            {}
                                            else
                                            {
                                                
                                                if user_data["first_name"] is NSNull
                                                {}
                                                else
                                                {
                                                    firstName = (user_data["first_name"] as? String)!
                                                }
                                                
                                                if user_data["last_name"] is NSNull
                                                {}
                                                else
                                                {
                                                    lastName = (user_data["last_name"] as? String)!
                                                }
                                                
                                                self.userName.append("\(firstName) \(lastName)")
                                                
                                                debugPrint("---------", self.userName)
                                                
                                                if user_data["profile_pic_url"] is NSNull
                                                {
                                                    self.userImage.append("")
                                                }
                                                else
                                                {self.userImage.append((user_data["profile_pic_url"] as? String)!)
                                                    
                                                    
                                                }}
                                        }
                                        
                                        if let feed_user_data = innerdict[i]["feed_user_data"] as? AnyObject
                                        {
                                            
                                            if innerdict.count > 0
                                            {
                                                
                                                
                                                if innerdict[i]["feed_uploaded"] is NSNull
                                                    
                                                {
                                                    self.uploadTime.append(0)
                                                }
                                                else
                                                {
                                                    let date = innerdict[i]["feed_uploaded"] as! String
                                                    self.uploadTime.append(Double(date)!)
                                                }
                                                
                                                
                                                
                                                
                                                var commentCount = 0
                                                var like = 0
                                                if innerdict[i]["comment_count"] is NSNull
                                                {
                                                    commentCount = 0
                                                }
                                                else
                                                {
                                                    commentCount = (innerdict[i]["comment_count"] as? Int)!
                                                }
                                                
                                                if innerdict[i]["like_count"] is NSNull
                                                {
                                                    like = 0
                                                }
                                                else
                                                {
                                                    like = (innerdict[i]["like_count"] as? Int)!
                                                }
                                                
                                                self.like.append(like)
                                                self.Comment.append(commentCount)
                                                
                                                
                                            }
                                        }
                                        
                                        
                                        
                                        if innerdict[i]["title"] is NSNull
                                        {
                                            self.feed_text.append("")
                                        }
                                        else
                                        {
                                            self.feed_text.append((innerdict[i]["title"] as? String)!)
                                        }
                                        
                                        
                                        
                                        if innerdict[i]["media_url"] is NSNull
                                        {}
                                        else
                                        {
                                            if let data = innerdict[i]["media_url"] as? AnyObject
                                            {
                                                /// get image
                                                if let imagedata = data["image"] as? [AnyObject]
                                                {
                                                    if imagedata.count > 0
                                                    { self.feedImage.append((imagedata[0] as? String)!)
                                                    }
                                                    else
                                                    {
                                                        self.feedImage.append("")
                                                    }
                                                }
                                                else
                                                {
                                                    self.feedImage.append("")
                                                }
                                                /// get gif image
                                                if let imagedata = data["gif"] as? [AnyObject]
                                                {
                                                    
                                                    if imagedata.count > 0
                                                    { self.feedGif.append((imagedata[0] as? String)!)
                                                    }
                                                    else
                                                    {
                                                        self.feedGif.append("")
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    self.feedGif.append("")
                                                }
                                                if let imagedata = data["mp4"] as? [AnyObject]
                                                {
                                                    
                                                    if imagedata.count > 0
                                                    { self.feedVideo.append((imagedata[0] as? String)!)
                                                    }
                                                    else
                                                    {
                                                        self.feedVideo.append("")
                                                        
                                                    }
                                                }
                                                else
                                                {
                                                    self.feedVideo.append("")
                                                }
                                                
                                            }
                                            
                                            
                                        }
                                        
                                        
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        
                                    }}
                                
                            }
                        }}}
                
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            }
            
            DispatchQueue.main.async {
                self.productCollection.isHidden = false
                self.refreshControl.endRefreshing()
                self.productCollection.reloadData()
                self.refreshControl.endRefreshing()
                CustomLoader.instance.hideLoaderView()
            }
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "feedComments") {
            guard let destination = segue.destination as? FeedCommentDataViewController else
            {
                debugPrint("return data")
                return
            }
            
            destination.feedComment_screen = 0
            destination.user_feedid = self.user_feedid[FeedViewController.selectedIndex]
        }
        else if (segue.identifier == "feedDetails1") {
            guard let destination = segue.destination as? FeedDetailsViewController else
            {
                debugPrint("return data")
                return
            }
            destination.user_feedid = self.user_feedid[feedDetailsIndex]
            if self.feedVideo.count > feedDetailsIndex
            {
                
                if (self.feedVideo[feedDetailsIndex] as? String)! != ""
                {
                    destination.feedVideo = (self.feedVideo[feedDetailsIndex] as? String)!
                }
                else if (self.feedImage[feedDetailsIndex] as? String)! != ""
                {
                    destination.feedImage = (self.feedImage[feedDetailsIndex] as? String)!
                }
                else
                {
                    destination.isFeedID_liked = self.isFeedID_liked[feedDetailsIndex]
                    destination.userImage = self.userImage[feedDetailsIndex]
                    //                    destination.userName = self.userName[feedDetailsIndex]
                    //                    destination.uploadTime = self.uploadTime[feedDetailsIndex]
                    
                    destination.QuestionText = "\(self.feed_text[feedDetailsIndex] as! String)"
                    
                }
                
            }
            else
            {}
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.feed_id.count > 0
        {
            return self.feed_id.count
        }
        else {
            //            self.productCollection.showEmptyListMessage("No Product found!")
            return 0
        }
    }
    
    
    
    
    func returnImageUsingCacheWithURLString(url: NSURL) -> (UIImage) {
        
        // First check if there is an image in the cache
        if let cachedImage = imageCache.object(forKey: url) as? UIImage {
            
            return cachedImage
        }
            
        else {
            // Otherwise download image using the url location in Google Firebase
            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                
                if error != nil {
                    debugPrint(error)
                }
                else {
                    DispatchQueue.global().async {
                        
                        // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                        if let downloadedImage = UIImage(data: data!) {
                            
                            self.imageCache.setObject(downloadedImage, forKey: url)
                            self.returnImage = downloadedImage
                            
                        }
                    }
                }
            }).resume()
            return returnImage
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
        
        //            debugPrint("product count \(Product.count)")
        //            cell.product1.layer.cornerRadius = 5.0
        cell.clipsToBounds = true
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        
        cell.contentView.layer.cornerRadius = 5
        cell.contentView.layer.borderWidth = 1.0
        
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        cell.productImage.image = nil
        cell.userDetail.tag = indexPath.row
        cell.userDetail.addTarget(self, action: #selector(displayImage(imgaeIndex:)), for: .touchUpInside)
        
        if self.feed_text.count > indexPath.row
        {
            if self.feed_text[indexPath.row] != ""
            {
                cell.productName.text = self.feed_text[indexPath.row]
            }}
        if self.feedVideo[indexPath.row] as String != ""
        {
            if self.feedGif[indexPath.row] as String != ""
            {
                debugPrint("gifindex", indexPath.row, self.feedGif)
                cell.videoContainer.isHidden = true
                cell.productImage.isHidden = false
                
                // You can also set it with an URL pointing to your gif
                let url = URL(string: "\(self.feedGif[indexPath.row])")
                let loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
                if url != nil
                {
                    DispatchQueue.main.async {
                        let animatedImage = SDAnimatedImage(named: self.feedGif[indexPath.row])
                        cell.productImage.image = animatedImage
                    }
                }
            }
            else{
                let url = self.feedVideo[indexPath.row] as String
                
                let videoURL = NSURL(string: url)
                
                avPlayer = AVPlayer(url: videoURL! as URL)
                avPlayer.volume = 10
                avPlayer.isMuted = true
                let playerController = AVPlayerViewController()
                playerController.player = avPlayer
                
                self.addChildViewController(playerController)
                
                // Add your view Frame
                
                playerController.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                playerController.view.frame = cell.videoContainer.bounds
                playerController.showsPlaybackControls = false
                
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                } catch _ {
                }
                do {
                    try AVAudioSession.sharedInstance().setActive(true)
                } catch _ {
                }
                do {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                } catch _ {
                }
                //                        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.advance
                
                // Add subview in your view
                cell.videoContainer.isHidden = false
                cell.productImage.isHidden = true
                cell.videoContainer.addSubview(playerController.view)
                playerController.didMove(toParentViewController: self)
                
                //                            avPlayer.play()
                
            }
            
        }
        else if self.feedImage.count > indexPath.row
        {
            cell.videoContainer.isHidden = true
            cell.productImage.isHidden = false
            
            if self.feedImage[indexPath.row] as String != ""
            {
                var image = self.feedImage[indexPath.row] as String
                image = image.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: image)
                if url != nil{
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
                
            }
        }
        if self.feedUser_Image.count > indexPath.row
        {
            
            if self.feedUser_Image[indexPath.row] as String != ""
            {
                var image = self.feedUser_Image[indexPath.row] as String
                image = image.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: image)
                if url != nil{
                    DispatchQueue.main.async {
                        cell.userImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
                
            }
        }
        if self.userName.count > indexPath.row
        {
            cell.userName.text = self.userName[indexPath.row]
        }
        cell.likeProduct.tag = indexPath.row
        cell.likeProduct.addTarget(self, action: #selector(favourite(index:)), for: .touchUpInside)
        if self.isFeedID_liked.count>indexPath.row
        {
            if self.isFeedID_liked[indexPath.row] == true
            {
                cell.likeProduct.setImage(UIImage(named: "liked"), for: .normal)
            }
            else
            {
                cell.likeProduct.setImage(UIImage(named: "like"), for: .normal)
            }
            if self.like.count > indexPath.row
            {
                if self.like[indexPath.row] > 0
                {
                    cell.likeProduct.setTitle(" \(self.like[indexPath.row])", for: .normal)
                }
                else
                {
                    cell.likeProduct.setTitle("", for: .normal)
                }
            }
        }
        return cell
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.feedDetailsIndex = indexPath.row
        self.feed_Details(sender: indexPath.row)
    }
    
    @objc func favourite(index: UIButton) {
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
            var id = native.string(forKey: "FavProID")
            
            //        debugPrint("product id:", id)
            let parameters: [String:Any] = ["user_feedid":self.feed_id[index.tag]]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            var url = ""
            if self.isFeedID_liked[index.tag] == true
            {
                url = "\(b2burl)/users/dislike_user_feed/"
            }
            else
            {
                url = "\(b2burl)/users/like_user_feed/"
            }
            debugPrint("follow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                debugPrint(response)
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if let json = response.result.value {
                            let jsonString = "\(json)"
                            //                            debugdebugPrint("res;;;;;", jsonString)
                            if self.isFeedID_liked[index.tag] == true
                            {
                                if self.like[index.tag] > 1
                                {
                                    self.like[index.tag] = self.like[index.tag] - 1
                                }
                                else
                                {
                                    self.like[index.tag] = 0
                                }
                                self.isFeedID_liked[index.tag] = false
                            }
                            else
                            {
                                self.isFeedID_liked[index.tag] = true
                                self.like[index.tag] = self.like[index.tag] + 1
                            }
                            
                            if self.isFeedID_liked[index.tag] == true
                            {
                                self.view.makeToast("Added in your Favourite", duration: 2.0, position: .center)
                                
                            }
                            else
                            {
                                self.view.makeToast("Removed from your Favourite", duration: 2.0, position: .center)
                                
                            }
                            DispatchQueue.main.async {
                                
                                self.productCollection.reloadData()
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                            }
                            
                        }
                    }}
            }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    
    @objc func displayImage(imgaeIndex: UIButton)
    {
        FeedViewController.selectedIndex = imgaeIndex.tag
        FeedViewController.user_feed_id = self.feed_id[imgaeIndex.tag]
        native.set(self.feed_id[imgaeIndex.tag], forKey: "feed_userid")
        native.synchronize()
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "UserFeedPost"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
        
        
    }
    
    
    func feed_Details(sender: Int)
    {
        self.feedDetailsIndex = sender
        performSegue(withIdentifier: "feedDetails1", sender: self)
    }
    
    
    @objc func comment_data(sender: UIButton)
    {
        
        FeedViewController.user_feed_id = self.user_feedid[sender.tag]
        FeedViewController.selectedIndex = sender.tag
        performSegue(withIdentifier: "feedComments", sender: self)
        
        
    }
    
    
    
    
    
    /// like button
    
    @objc func LikeButton(Feed_Index: UIButton)
    {
        let token = self.native.string(forKey: "Token")!
        var fav = 0
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                
                let parameters: [String:Any] = ["user_feedid":self.user_feedid[Feed_Index.tag]]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                debugPrint("Successfully post")
                
                let b2burl = native.string(forKey: "b2burl")!
                var url = ""
                if self.isFeedID_liked.count > Feed_Index.tag
                {
                    if self.isFeedID_liked[Feed_Index.tag] == true
                    {
                        
                        url = "\(b2burl)/users/dislike_user_feed/"
                    }
                    else
                    {
                        url = "\(b2burl)/users/like_user_feed/"
                    }
                }
                debugPrint("follow product", url)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                debugPrint(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            debugPrint("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            debugPrint(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            debugPrint("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                                
                            else if let json = response.result.value {
                                let jsonString = "\(json)"
                                debugPrint("res;;;;;", jsonString)
                                
                                if self.isFeedID_liked[Feed_Index.tag] == true
                                {
                                    self.like[Feed_Index.tag] = self.like[Feed_Index.tag] - 1
                                    self.isFeedID_liked[Feed_Index.tag] = false
                                }
                                else
                                {
                                    self.like[Feed_Index.tag] = self.like[Feed_Index.tag] + 1
                                    self.isFeedID_liked[Feed_Index.tag] = true
                                }
                                
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                DispatchQueue.main.async {
                                    self.productCollection.reloadItems(at: [IndexPath(row: Feed_Index.tag, section: 0)])
                                    
                                }}
                        }}
                }
            }
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
        
    }
    
    
    @objc func shareButton(sender: UIButton) {
        
        
        // image to share
        let text = "Feed Product"
        var productImage = "thumbnail"
        
        let image = UIImage(named: productImage)
        let myWebsite = NSURL(string:self.shareLink[sender.tag])
        let shareAll = [text , image , myWebsite!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    
    
}



