//
//  FeedCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 18/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class FeedCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var userIcon: RoundedImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var uploadingTime: UILabel!
    
    
    
    @IBOutlet weak var videoContainer: UIView!
    
    
    ///bottom label & button
    
    @IBOutlet weak var likeandComment: UILabel!
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
}
