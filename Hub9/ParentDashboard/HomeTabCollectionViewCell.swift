//
//  HomeTabCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 18/02/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class HomeTabCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var textName: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    
}
