//
//  MainDashboardViewController.swift
//  Hub9
//
//  Created by Deepak on 18/02/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire



var MainDash = MainDashboardViewController()
class MainDashboardViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var native = UserDefaults.standard
//    public static var tabText = ["Feed","Home", "Featured"]
    public static var tabID = [0,0,0]
    var selectedTab = 1
    
    
    @IBOutlet weak var searchContainer: UIView!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var tabContainer: UIView!
    @IBOutlet weak var tabCollection: UICollectionView!
    
    
    @IBOutlet weak var viewContainer: UIView!
    
    
    /// feed view
    
    @IBOutlet weak var feedContainer: UIView!
    
    ///home view
    
    @IBOutlet weak var homeContainer: UIView!
    
    ///featured view
    
    @IBOutlet weak var featuredContainer: UIView!
    
    
    ///collection view
    
    @IBOutlet weak var collectionContainer: UIView!
    
    
    
    @IBAction func searchClicked(_ sender: Any) {
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchSugg"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
 
    @objc func loadList(notification: NSNotification) {
      self.tabCollection.reloadData()
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///to select default index 0(home) for tab
        MainDash = self
        searchView.layer.cornerRadius = searchView.frame.height/2
        searchView.clipsToBounds=true
         self.tabCollection.reloadData()
        onSelectTab(index: 1)
        NotificationCenter.default.addObserver(self, selector: #selector(loadList(notification:)), name: NSNotification.Name(rawValue: "load"), object: nil)
            // Turn off all segments been fixed/equal width.
            // The width of each segment would be based on the text length and font size.
           
            
            if native.object(forKey: "deeplink") != nil {
                if native.object(forKey: "deeplink") as? String == "true"
                {
                    native.set("false", forKey: "deeplink")
                    native.synchronize()
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
                self.navigationController?.pushViewController(editPage, animated: true)
                }
            }
        navigationController?.isNavigationBarHidden = true
        native.set(0, forKey: "loggedIn")
        native.synchronize()
        // Do any additional setup after loading the view.
    }
    
    
    func onSelectTab(index: Int)
    {
        self.feedContainer.isHidden = true
        self.homeContainer.isHidden = true
        self.featuredContainer.isHidden = true
        self.collectionContainer.isHidden = true
        self.selectedTab = index
        if index == 0
        {
            self.feedContainer.isHidden = false
        }
        else if index == 1
        {
            self.homeContainer.isHidden = false
        }
        else if index == 2
        {
            self.featuredContainer.isHidden = false
        }
        else
        {
            self.collectionContainer.isHidden = false
            self.native.set(MainDashboardViewController.tabID[index], forKey: "product_collectionid")
            self.native.synchronize()
            NotificationCenter.default.post(name: NSNotification.Name("loadCollectionData"), object: nil)
        }
        
        
        self.tabCollection.reloadData()
        self.tabCollection.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredVertically, animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.navigationController?.setNavigationBarHidden(true, animated: true)
           if #available(iOS 13.0, *) {
               let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                statusBar.backgroundColor = UIColor.white
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                   // get the navigation bar from the current navigation controller if there is one
                   
               UINavigationBar.appearance().barTintColor = UIColor.white
                   }
           else { UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.white
               UINavigationBar.appearance().barTintColor = UIColor.white
                   UIApplication.shared.statusBarStyle = .default
                   }
           self.tabBarController?.tabBar.isHidden = false

          
       }
      
       override func viewWillDisappear(_ animated: Bool) {
          
           self.navigationController?.setNavigationBarHidden(true, animated: true)
           let NavBackColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
          if #available(iOS 13.0, *) {
               let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
                statusBar.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                   // get the navigation bar from the current navigation controller if there is one
                   
               UINavigationBar.appearance().barTintColor = NavBackColor
                   }
           else {
    UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
                    UINavigationBar.appearance().barTintColor = NavBackColor
                   UIApplication.shared.statusBarStyle = .default
                   }
           UIApplication.shared.isStatusBarHidden = false //Set if hidden
       }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
//        return MainDashboardViewController.tabText.count
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.onSelectTab(index: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeashboardTab", for: indexPath) as! HomeTabCollectionViewCell
//        cell.textName.text = MainDashboardViewController.self.tabText[indexPath.row]
        
        if selectedTab == indexPath.row
        {
            cell.textName.textColor = UIColor.init(red: 247/250, green: 82/250, blue: 85/250, alpha: 1)
            cell.bottomView.backgroundColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        }
        else
        {
            cell.textName.textColor = UIColor.init(red: 51/250, green: 51/250, blue: 51/250, alpha: 1)
            cell.bottomView.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    


}
