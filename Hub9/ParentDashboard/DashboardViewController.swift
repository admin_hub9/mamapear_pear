//
//  DashboardViewController.swift
//  Hub9
//
//  Created by Deepak on 11/22/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import AttributedTextView
import AudioToolbox
/// segment import
import ScrollableSegmentedControl



var dashboardCategory = DashboardViewController()

class DashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UIGestureRecognizerDelegate, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate, UIScrollViewDelegate {
    
    var native = UserDefaults.standard
    var refreshControl = UIRefreshControl()
    
    var bannerData = [AnyObject]()
    var categoryData = [AnyObject]()
    var trendingPro = [AnyObject]()
    var featured_shops = [AnyObject]()
    var shopProduct = [AnyObject]()
    var Category_data = [AnyObject]()
    var product_top_category_data = [AnyObject]()
    var selectedtableindex = 0
    static var selectedcategoryindex = 0
    var shoptext = ""
    var activeBannerIndex = 0
    var user_purchase_modeid = "1"
    var Currency = ""
    
    var DataType = [["Dicttype": String(), "dict": [AnyObject]()]]
    
    
    
    var payment_orderid = ""
    var gateway_paymentid = ""
    var orderid = ""
    var insurance = false
    var totalCost = ""
    
    var amount = 0.0
    var address_lkpid = ""
    var paymentmode = 1
    var ship_insurance = 0.0
    var shippingPrice = 0.0
    var payment_id = ""
    
    var limit = 10
    var offset = 0
    var isDataLoading:Bool=false
    
    
    //
    var shoplocation = 0
    var shopIndex = false
    var recommendedIndex = false
    var recommendedLocation = 0
    
    
    
    @IBOutlet weak var dashboardTable: UITableView!
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var searchView: UIView!
    
    
    
    @IBAction func searchClicked(_ sender: Any) {
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchSugg"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    @IBAction func oprnCart(_ sender: Any) {
        BBProductDetails.senddataAddtocart = true
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "buying"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Api Data
        refreshControl.beginRefreshing()
        //        self.updateView()
        bannerData.removeAll()
        categoryData.removeAll()
        trendingPro.removeAll()
        shopProduct.removeAll()
        Category_data.removeAll()
        product_top_category_data.removeAll()
        selectedtableindex = 0
        DashboardViewController.selectedcategoryindex = 0
        shoptext = ""
        activeBannerIndex = 0
        
        var DataType = [["Dicttype": String(), "dict": [AnyObject]()]]
        
        parseData()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        
        super.viewWillDisappear(animated)
        
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getAllKey()
        searchView.layer.cornerRadius = searchView.frame.height/2
        searchView.clipsToBounds=true
        dashboardTable.contentInset = UIEdgeInsets(top: -1, left: 0, bottom: 0, right: 0)
        // change some colors
        let lightRed = UIColor.init(red: 153/250, green: 153/250, blue: 153/250, alpha: 0.5)
        let red = UIColor.init(red: 247/250, green: 82/250, blue: 85/250, alpha: 1)
        
        
        // Turn off all segments been fixed/equal width.
        // The width of each segment would be based on the text length and font size.
        
        
        if native.object(forKey: "deeplink") != nil {
            if native.object(forKey: "deeplink") as? String == "true"
            {
                native.set("false", forKey: "deeplink")
                native.synchronize()
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
                self.navigationController?.pushViewController(editPage, animated: true)
            }
        }
        //        UINavigationBar.appearance().barTintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        
        //        razorpay = Razorpay.initWithKey(razorpayTestKey, andDelegate: self)
        // refresh Table
        // Add Refresh Control to Table View
        dashboardTable.tableFooterView = UIView()
        
        // Configure Refresh Control
        refreshControl = UIRefreshControl()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.red
        if #available(iOS 10.0, *) {
            self.dashboardTable.refreshControl = self.refreshControl
        } else {
            // Fallback on earlier versions
            self.dashboardTable.addSubview(self.refreshControl)
        }
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        
        var color = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
        //                searchbar.isHidden = false
        
        //        searchBar.backgroundImage = UIImage()
        //        searchBar.barTintColor = UIColor.clear
        //        topview.backgroundColor = color
        
        dashboardCategory = self
        //        dashboardTable.isHidden = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        // Do any additional setup after loading the view.
        //        sendDeviceToken()
        CustomLoader.instance.showLoaderView()
        parseData()
        if let version: String = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            guard let intVersion = Int(version) else { return }
            
            if UserDefaults.standard.integer(forKey: "lastVersion") < intVersion {
                debugPrint("need to show popup")
            } else {
                debugPrint("Don't need to show popup")
            }
            
            UserDefaults.standard.set(intVersion, forKey: "lastVersion")
        }
        
    }
    
    
    private func alertUserWithHandler(title: String?, message: String, upgradeHandler: ((UIAlertAction) -> Void)? ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: upgradeHandler))
        self.present(alert, animated: true, completion: nil)
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    
    // The upgrade handler takes the user to the AppStore.
    func upgradeHandler(action:UIAlertAction) {
        UIApplication.shared.openURL(NSURL(string:"https://testflight.apple.com/join/FzZegRY6") as! URL)
    }
    //Calls this function when the tap is recognized.
    override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    var lastContentOffset: CGFloat = 0
    
    
    @IBAction func searchButton(_ sender: Any) {
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchSugg"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var dataType = self.DataType[indexPath.row]["Dicttype"] as! String
        if dataType == "banner"
        {
            debugPrint("")
        }
        else if dataType == "category"
        {
            
            
        }
        else if dataType == "RecommendedProduct"
        {
            if let data = self.DataType[indexPath.row]["dict"] as? NSDictionary
            {
                native.set(data["productid"] as! Int, forKey: "FavProID")
                native.set(data["title"], forKey: "search")
                native.set("fav", forKey: "comefrom")
                native.synchronize()
                //        currntProID = self.ProductID[indexPath.row] as! Int
                
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
                self.navigationController?.pushViewController(editPage, animated: true)
            }
        }
        else if dataType == "categoryBanner"
        {
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            
            
            if let category_name = data["category_name"] as? String
            {
                
                
                if let image = data["mobile_url"] as? String
                {
                    //                debugPrint("cat mobile_url", image)
                    if image != nil
                    {
                        
                        native.set(image, forKey: "bannerImage")
                        native.synchronize()
                    }
                    else
                    {
                        native.set("", forKey: "bannerImage")
                        native.synchronize()
                    }
                    
                    
                }
                else
                {
                    native.set("", forKey: "bannerImage")
                    native.synchronize()
                }
                native.set(category_name, forKey: "search")
                native.set("parent_category_elastic", forKey: "searchType")
                native.synchronize()
                selectdashboardSubCategoryViewController.subcategoryid = (data["parent_categoryid"] as? Int)!
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "product"))! as UIViewController
                self.navigationController?.pushViewController(editPage, animated: true)
            }
            
            
        }
            
        else if dataType == "trendingPro"
        {
            var data = self.DataType[indexPath.row]["dict"] as! [AnyObject]
            native.set(data[indexPath.row]["productid"] as! Int, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as! UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
            
        }
        else if dataType == "shop"
        {
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            var id = (data["shopid"] as? Int)!
            native.set(id, forKey: "entershopid")
            native.synchronize()
            //            debugPrint("product id:", id)
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shopDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellSize = 0
        var dataType = self.DataType[indexPath.row]["Dicttype"] as! String
        if dataType == "banner"
        {
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            if data.count == 0
                
            {
                cellSize = 0
            }
            else
            {
                cellSize = 250
            }
        }
        else if dataType == "category"
        {
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            if data.count > 0
            {
                cellSize = 90
            }
            else
            {
                cellSize = 0
            }
        }
        else if dataType == "categoryBanner"
        {
            cellSize = 307
        }
        else if dataType == "featured_shops"
        {
            if self.featured_shops.count > 4
            {
                cellSize = 90
            }
            else
            {
                cellSize = 0
                
            }
        }
        else if dataType == "trendingPro"
        {
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            cellSize = 160 * data.count
            
        }
        else if dataType == "shop"
        {
            cellSize = 220
        }
        else if dataType == "RecommendedProduct"
        {
            cellSize = 160
        }
        return CGFloat(cellSize)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.DataType.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReuseCell = UITableViewCell()
        var dataType = self.DataType[indexPath.row]["Dicttype"] as! String
        if dataType == "banner"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashBannerTable", for: indexPath) as! DashboardbannerTableViewCell
            cell.scrollView.auk.removeAll()
            // Make the images fill entire page
            cell.scrollView.auk.settings.contentMode = .scaleAspectFill
            
            // Set background color of page indicator
            cell.scrollView.auk.settings.pageControl.backgroundColor = UIColor.gray.withAlphaComponent(0.0)
            cell.scrollView.auk.settings.pageControl.pageIndicatorTintColor = UIColor.gray
            cell.scrollView.auk.settings.pageControl.currentPageIndicatorTintColor = UIColor.red
            
            // Show placeholder image while remote image is being downloaded.
            var data = self.DataType[0]["dict"] as! [AnyObject]
            for var i in 0..<data.count
            {
                //            debugPrint("Banner data", data)
                if data.count > 0
                {
                    if let banner = data[i]["banner_url"] as? [AnyObject]
                    {
                        //                    debugPrint(banner)
                        var imageUrl = (banner[1]["mobile_banner"] as? String)!
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        let url = NSURL(string: imageUrl)
                        if url != nil{
                            // Show an image AFTER specifying the settings
                            cell.scrollView.auk.show(url: imageUrl)
                        }}
                }
            }
            
            cell.scrollView.auk.settings.pageControl.marginToScrollViewBottom = -10
            cell.scrollView.auk.settings.placeholderImage = UIImage(named: "thumbnail")
            cell.scrollView.auk.startAutoScroll(delaySeconds: 3)
            
            ReuseCell = cell
        }
            
            
        else if dataType == "category"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashCategoryTable", for: indexPath) as! DashboardCategoryTableViewCell
            cell.categoryCollection.tag = indexPath.row
            cell.categoryCollection.reloadData()
            
            ReuseCell = cell
            
        }
        else if dataType == "featured_shops"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "featured_shops", for: indexPath) as! FeaturedShopTableViewCell
            cell.featuredShopCollection.tag = indexPath.row
            cell.featuredShopCollection.reloadData()
            
            ReuseCell = cell
            
        }
        else if dataType == "RecommendedProduct"
        {
            
            if recommendedIndex == false
            {
                recommendedLocation = indexPath.row
                recommendedIndex = true
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "recommendedProduct", for: indexPath) as! RecommendedProductTableViewCell
            
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            //            debugPrint("recommandedProduct", data)
            if data["title"] is NSNull
            {}
            else
            {
                cell.productName.text = data["title"] as! String
            }
            if data["product_variant_data"] is NSNull
            {}
            else
            {
                let price = data["product_variant_data"] as! NSDictionary
                var product_price_tag = ""
                if data["product_price_tag"] is NSNull
                {}
                else
                {
                    product_price_tag = data["product_price_tag"] as! String
                }
                if price != nil
                {
                    let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                    let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                    let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                    if price["deal_price"] is NSNull{}
                    else{
                        var actualprice = 0.0
                        var retailsPrice = 0.0
                        
                        if self.user_purchase_modeid == "1"
                        {
                            if price["deal_price"] != nil
                            {
                                if price["deal_price"] is NSNull
                                {}
                                else{
                                    if let price = price["deal_price"] as? NSNumber!
                                    {
                                        if (price != nil){
                                            actualprice = Double(price!)
                                        }
                                        
                                        
                                    }}
                                
                            } }
                        else
                        {
                            if price["bulk_price"] == nil
                            {
                                actualprice = Double((price["selling_price"] as? NSNumber)!)
                            }
                            else
                            {
                                if price["bulk_price"] is NSNull
                                {}
                                else
                                {
                                    actualprice = Double((price["bulk_price"] as? NSNumber)!)
                                }}}
                        
                        if price["retail_price"] is NSNull
                        {}
                        else
                        {
                            retailsPrice = Double(Float((price["retail_price"] as? NSNumber)!))
                        }
                        if self.user_purchase_modeid == "1"
                        {
                            cell.priceSaveTag.isHidden = false
                        }
                        else
                        {
                            cell.priceSaveTag.isHidden = true
                        }
                        if price["percentage_off"] is NSNull
                        {}
                        else
                        {
                            cell.priceSaveTag.text = "Save \(Double(Float((price["percentage_off"] as? NSNumber)!)))% vs retail"
                        }
                        
                        
                        if native.string(forKey: "Token")! == ""
                        {
                            cell.price.attributedText = ("Sign In ".color(textcolor).size(11) + "to Unlock Wholesale Price".color(lightBlackColor).size(11)).attributedText
                        }
                        else
                        {
                            if let currency = price["currency_symbol"] as? String
                            {
                                self.Currency = currency
                            }
                            
                            cell.price.attributedText = ("\(self.Currency)\(String(format: "%.2f",actualprice))\(product_price_tag) ".color(textcolor) +  " \(self.Currency)\(String(format: "%.2f",retailsPrice))".color(graycolor).strikethrough(1).size(11)).attributedText
                        }
                    }
                }
            }
            if data["feature_name"] is NSNull
            {
                cell.productTag.text = ""
            }
            else
            {
                if data["feature_name"] as! String != "Default"
                {
                    cell.productTag.text = " \(data["feature_name"] as! String) "
                    cell.productTag.isHidden = false
                }
                else
                {
                    cell.productTag.isHidden = true
                }
            }
            if let data = data["image_url"] as? AnyObject
            {
                if let image = data["200"] as? [AnyObject]
                {
                    if image.count > 0
                    {
                        var imageUrl = image[image.count - 1] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        var url = NSURL(string:imageUrl as! String )
                        
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                    
                else if let image = data["200"] as? [AnyObject]
                {
                    if image.count > 0
                    {
                        var imageUrl = image[0] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        var url = NSURL(string:imageUrl as! String )
                        
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
            }
            
            ReuseCell = cell
            
        }
        else if dataType == "categoryBanner"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCategorBannerTableViewCell", for: indexPath) as! DashboardCategorBannerTableViewCell
            //shadwo
            //            cell.clipsToBounds = true
            //            cell.contentBackground.layer.cornerRadius = 5
            //            cell.contentBackground.layer.borderWidth = 1.0
            //            cell.contentBackground.layer.borderColor = UIColor.clear.cgColor
            //            cell.contentBackground.layer.masksToBounds = true
            //            cell.contentBackground.layer.shadowColor = UIColor.lightGray.cgColor
            //            cell.contentBackground.layer.shadowOffset = CGSize(width: 1, height: 2.0)
            
            cell.contentBackground.layer.shadowColor = UIColor.gray.cgColor
            cell.contentBackground.layer.shadowOffset = CGSize(width: 0, height: 1)
            cell.contentBackground.layer.shadowOpacity = 1
            cell.contentBackground.layer.shadowRadius = 1.0
            cell.contentBackground.layer.cornerRadius = 3.0
            
            cell.contentBackground.clipsToBounds = false
            cell.contentBackground.layer.masksToBounds = false
            cell.clipsToBounds = true
            cell.layer.masksToBounds = true
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            
            if let image = data["mobile_url"] as? String
            {
                //                debugPrint("mobile_url", image)
                if image != nil
                {
                    let url = NSURL(string:image as! String )
                    if url != nil{
                        DispatchQueue.main.async {
                            cell.categoryImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
            }
            
            cell.categoryProduct.tag = indexPath.row
            cell.categoryProduct.reloadData()
            
            ReuseCell = cell
        }
        else if dataType == "trendingPro"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashProductTable", for: indexPath) as! DashboardProductTableViewCell
            cell.productCollection.tag = indexPath.row
            cell.productCollection.reloadData()
            
            ReuseCell = cell
            
        }
        else if dataType == "shop"
        {
            if shopIndex == false
            {
                shoplocation = indexPath.row
                shopIndex = true
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardShopTable", for: indexPath) as! DashboardShopTableViewCell
            var data = self.DataType[indexPath.row]["dict"] as! AnyObject
            if let image = data["logo_url"] as? String
            {
                if image != nil
                {
                    var imageUrl = image
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    //                        debugPrint("shop_logo", imageUrl)
                    let url = NSURL(string: imageUrl)
                    if url != nil{
                        DispatchQueue.main.async {
                            cell.shopLogo.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
            }
            if let address = data["location"] as? String
            {
                cell.shopAddress.text = address
            }
            //            debugPrint(index)
            
            if let name = data["shop_name"] as? String
            {
                cell.shopTitle.text = name
            }
            
            
            let greencolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
            cell.enterShop.layer.cornerRadius = cell.enterShop.frame.height / 2
            cell.enterShop.layer.borderWidth = 0.8
            cell.enterShop.layer.borderColor = greencolor.cgColor
            cell.enterShop.clipsToBounds = true
            
            cell.shopCollection.tag = indexPath.row
            cell.shopCollection.reloadData()
            
            ReuseCell = cell
            
        }
        
        return ReuseCell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        debugPrint("collectionView.tag", collectionView.tag)
        var gridSize = 0
        var dataType = self.DataType[collectionView.tag]["Dicttype"] as! String
        if dataType == "banner"
        {
            gridSize = 4
        }
        else if dataType == "category"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            
            gridSize = data.count
        }
        else if dataType == "categoryBanner"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            
            if data["product_data"] is NSNull
            {
                gridSize = 0
            }
            else
            {
                var productcount = data["product_data"] as! [AnyObject]
                
                //            debugPrint("76546789096754547",data, data.count)
                gridSize = productcount.count
            }
        }
            
        else if dataType == "featured_shops"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            
            gridSize = data.count
            debugPrint("featured_shops", gridSize)
        }
        else if dataType == "trendingPro"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            gridSize = data.count
            
        }
        else if dataType == "shop"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            //            debugPrint("-===-=-=-=-==", collectionView.tag, data.count, data)
            if data["shop_product"] is NSNull
            {
                gridSize = 0
            }
            else
            {
                var productcount = data["shop_product"] as! [AnyObject]
                gridSize = productcount.count
            }
        }
        
        return gridSize
    }
    
    //this method is for the size of items
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize
        var dataType = self.DataType[collectionView.tag]["Dicttype"] as! String
        if dataType == "category"
        {
            let width = (collectionView.frame.width-30)/5
            
            return CGSize(width: width, height: collectionView.frame.height)
        }
            
        else if dataType == "categoryBanner"
        {
            let width = collectionView.frame.width/3
            
            return CGSize(width: width, height: collectionView.frame.height)
        }
        else if dataType == "featured_shops"
        {
            let width = (collectionView.frame.width-30)/5
            
            return CGSize(width: width, height: collectionView.frame.height)
        }
        else if dataType == "trendingPro"
        {
            let width = collectionView.frame.width
            
            return CGSize(width: width, height: 160)
        }
        else if dataType == "shop"
        {
            let width = (collectionView.frame.width-60)/3
            
            return CGSize(width: width, height: collectionView.frame.height)
        }
        else
        {
            return CGSize(width: 0.0, height: 0.0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        debugPrint("index", indexPath.row,  collectionView.tag, self.product_top_category_data.count + 4)
        var ReuseCell = UICollectionViewCell()
        var dataType = self.DataType[collectionView.tag]["Dicttype"] as! String
        if dataType == "banner"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Banner", for: indexPath) as! DashboardBannerCollectionViewCell
            
            
            ReuseCell = cell
        }
        else if dataType == "category"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashCategory", for: indexPath) as! DashboardCategoryCollectionViewCell
            
            var data = self.DataType[collectionView.tag]["dict"] as! [AnyObject]
            //            debugPrint("data", data)
            cell.categoryName.text = data[indexPath.row]["collection_name"] as? String
            cell.categoryImage.layer.masksToBounds = true
            cell.categoryImage.layer.cornerRadius = cell.categoryImage.frame.size.width / 2
            cell.categoryImage.clipsToBounds = true
            let image = data[indexPath.row]["collection_name"] as? String
            //            debugPrint("Banner data1", data[indexPath.row]["icon_image_url"] as! [AnyObject])
            if data[indexPath.row]["icon_image_url"] is NSNull
            {}
            else
            {
                let banner_data = data[indexPath.row]["icon_image_url"] as! [AnyObject]
                if banner_data.count>0
                {
                    var imageUrl = banner_data[0]["mobile_icon"] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    let url = NSURL(string: imageUrl)
                    if url != nil{
                        DispatchQueue.main.async {
                            cell.categoryImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                    
                }
            }
            //            if (data[indexPath.row]["product_collectionid"] as? Int)! ==  6
            //            {
            //                cell.categoryImage.image = UIImage(named: "men")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  2
            //            {
            //                cell.categoryImage.image = UIImage(named: "women")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  7
            //            {
            //                cell.categoryImage.image = UIImage(named: "Home & Living")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  4
            //            {
            //                cell.categoryImage.image = UIImage(named: "Electronics")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  6
            //            {
            //                cell.categoryImage.image = UIImage(named: "Beauty & Health")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  9
            //            {
            //                cell.categoryImage.image = UIImage(named: "Maternity & Baby")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  7
            //            {
            //                cell.categoryImage.image = UIImage(named: "Jewellery & Accessories")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  5
            //            {
            //                cell.categoryImage.image = UIImage(named: "Packeging")
            //            }
            //            else if (data[indexPath.row]["product_collectionid"] as? Int)! ==  3
            //            {
            //                cell.categoryImage.image = UIImage(named: "toys&baby")
            //            }
            //            else
            //            {
            //                cell.categoryImage.image = UIImage(named: "Home & Kitchen Appliances")
            //            }
            
            
            
            ReuseCell = cell
        }
        else if dataType == "categoryBanner"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashShop", for: indexPath) as! DashboardShopCollectionViewCell
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            if let shopproduct = data["product_data"] as? [AnyObject]
            {
                if shopproduct[indexPath.row]["title"] is NSNull
                {}
                else{
                    cell.productName.text = shopproduct[indexPath.row]["title"] as? String
                }
                cell.Producttag.layer.cornerRadius = 5
                cell.Producttag.clipsToBounds = true
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if shopproduct[indexPath.row]["deal_price"] is NSNull{}
                else{
                    var actualprice = 0.0
                    var retailsPrice = 0.0
                    
                    if self.user_purchase_modeid == "1"
                    {
                        if shopproduct[indexPath.row]["deal_price"] != nil
                        {
                            if shopproduct[indexPath.row]["deal_price"] is NSNull
                            {}
                            else{
                                if let price = shopproduct[indexPath.row]["deal_price"] as? String!
                                {
                                    if (price != nil){
                                        actualprice = Double(Float(price!)!)
                                    }
                                    
                                    
                                }}
                            
                        } }
                    else
                    {
                        if shopproduct[indexPath.row]["bulk_price"] is NSNull
                        {}
                        else
                        {
                            actualprice = Double(Float((shopproduct[indexPath.row]["bulk_price"] as? String)!)!)
                        }}
                    
                    if shopproduct[indexPath.row]["retail_price"] is NSNull
                    {}
                    else
                    {
                        retailsPrice = Double(Float((shopproduct[indexPath.row]["retail_price"] as? String)!)!)
                    }
                    
                    
                    if native.string(forKey: "Token")! == ""
                    {
                        cell.price.attributedText = ("Sign In ".color(textcolor).size(11) + "to Unlock Wholesale Price".color(lightBlackColor).size(11)).attributedText
                    }
                    else
                    {
                        if let currency = shopproduct[indexPath.row]["currency_symbol"] as? String
                        {
                            self.Currency = currency
                        }
                        var product_price_tag = ""
                        if shopproduct[indexPath.row]["product_price_tag"] is NSNull
                        {}
                        else
                        {
                            product_price_tag = (shopproduct[indexPath.row]["product_price_tag"] as? String)!
                        }
                        cell.price.attributedText = ("\(self.Currency)\(String(format: "%.2f",actualprice))\(product_price_tag) ".color(textcolor) +  " \(self.Currency)\(String(format: "%.2f",retailsPrice))".color(graycolor).strikethrough(1).size(11)).attributedText
                    }
                }
                
                if shopproduct[indexPath.row]["feature_name"] is NSNull
                {
                    cell.Producttag.text = ""
                }
                else
                {
                    if shopproduct[indexPath.row]["feature_name"] as! String != "Default"
                    {
                        cell.Producttag.text = " \(shopproduct[indexPath.row]["feature_name"] as! String) "
                        cell.Producttag.isHidden = false
                    }
                    else
                    {
                        cell.Producttag.isHidden = true
                    }
                }
                if let data = shopproduct[indexPath.row]["image_url"] as? AnyObject
                {
                    if let image = data["200"] as? [AnyObject]
                    {
                        if image.count > 0
                        {
                            var imageUrl = image[0] as! String
                            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                            let url = NSURL(string: imageUrl)
                            if url != nil{
                                DispatchQueue.main.async {
                                    cell.shopProductImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        }
                    }
                    //                    else if let image = data["200"] as? [AnyObject]
                    //                    {
                    //                        if image.count > 0
                    //                        {
                    //                            var imageUrl = image[0] as! String
                    //                            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    //                            let url = NSURL(string: imageUrl)
                    //                            if url != nil{
                    //                                cell.shopProductImage.af_setImage(withURL: url as! URL, placeholderImage: UIImage(named: "thumbnail"))
                    //                            }
                    //                        }
                    //                    }
                }
            }
            ReuseCell = cell
        }
            
        else if dataType == "featured_shops"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featured_shops", for: indexPath) as! DashboardCategoryCollectionViewCell
            
            var data = self.DataType[collectionView.tag]["dict"] as! [AnyObject]
            //            debugPrint("data", data)
            cell.categoryName.text = data[indexPath.row]["shop_name"] as? String
            cell.categoryImage.layer.masksToBounds = true
            cell.categoryImage.layer.cornerRadius = cell.categoryImage.frame.size.width / 2
            cell.categoryImage.clipsToBounds = true
            
            
            if data[indexPath.row]["logo_url"] is NSNull
            {}
            else
            {
                let banner_data = data[indexPath.row]["logo_url"] as! String
                if banner_data != nil
                {
                    var imageUrl = banner_data
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    let url = NSURL(string: imageUrl)
                    if url != nil{
                        DispatchQueue.main.async {
                            cell.categoryImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                    
                }
            }
            
            ReuseCell = cell
        }
        else if dataType == "trendingPro"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashProduct", for: indexPath) as! DashboardProductCollectionViewCell
            var data = self.DataType[collectionView.tag]["dict"] as! [AnyObject]
            cell.clipsToBounds = true
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            cell.productName.text = data[indexPath.row]["title"] as? String
            //                        cell.price.text = tre
            cell.productTag.layer.cornerRadius = 5
            cell.productTag.clipsToBounds = true
            if data[indexPath.row]["product_variant_data"] is NSNull
            {}
            else
            {
                let price = data[indexPath.row]["product_variant_data"] as! [AnyObject]
                var product_price_tag = ""
                if data[indexPath.row]["product_price_tag"] is NSNull
                {}
                else
                {
                    product_price_tag = data[indexPath.row]["product_price_tag"] as! String
                }
                
                if price != nil
                {
                    let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                    let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                    let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                    if price[0]["bprice"] is NSNull{}
                    else{
                        var actualprice = 0.0
                        var retailsPrice = 0.0
                        if self.user_purchase_modeid == "1"
                        {
                            if price[0]["deal_price"] != nil
                            {
                                if price[0]["deal_price"] is NSNull
                                {}
                                else{
                                    if let price = price[0]["deal_price"] as? String!
                                    {
                                        if (price != nil){
                                            actualprice = Double(Float(price!)!)
                                        }
                                        
                                        
                                    }}
                                
                            } }
                        else
                        {
                            if price[0]["bprice"] is NSNull
                            {}
                            else
                            {
                                actualprice = Double(Float((price[0]["bprice"] as? String)!)!)
                            }}
                        
                        if price[0]["retail_price"] is NSNull
                        {}
                        else
                        {
                            retailsPrice = Double(Float((price[0]["retail_price"] as? String)!)!)
                        }
                        if self.user_purchase_modeid == "1"
                        {
                            cell.productSaveTag.isHidden = false
                        }
                        else
                        {
                            cell.productSaveTag.isHidden = true
                        }
                        if price[0]["percentage_off"] is NSNull
                        {}
                        else
                        {
                            cell.productSaveTag.text = "Save \((price[0]["percentage_off"] as? NSNumber)!)% vs retail"
                        }
                        
                        //            cell.featuredTag.layer.borderColor = redColor.cgColor
                        
                        
                        if native.string(forKey: "Token")! == ""
                        {
                            cell.price.attributedText = ("Sign In ".color(textcolor).size(11) + "to Unlock Wholesale Price".color(lightBlackColor).size(11)).attributedText
                        }
                        else
                        {
                            if let currency = price[0]["currency_symbol"] as? String
                            {
                                self.Currency = currency
                            }
                            cell.price.attributedText = ("\(self.Currency)\(String(format: "%.2f", actualprice))\(product_price_tag) ".color(textcolor) + " \(self.Currency)\(String(format: "%.2f",retailsPrice))".color(graycolor).strikethrough(1).size(11)).attributedText
                        }
                    }
                }
            }
            if data[indexPath.row]["feature_name"] is NSNull
            {
                cell.productTag.text = ""
            }
            else
            {
                if data[indexPath.row]["feature_name"] as! String != "Default"
                {
                    cell.productTag.text = " \(data[indexPath.row]["feature_name"] as! String) "
                    cell.productTag.isHidden = false
                }
                else
                {
                    cell.productTag.isHidden = true
                }
            }
            if let data = data[indexPath.row]["image_url"] as? AnyObject
            {
                if let image = data["200"] as? [AnyObject]
                {
                    if image.count > 0
                    {
                        var imageUrl = image[0] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        var url = NSURL(string:imageUrl as! String )
                        
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                    
                else if let image = data["200"] as? [AnyObject]
                {
                    if image.count > 0
                    {
                        var imageUrl = image[0] as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        var url = NSURL(string:imageUrl as! String )
                        
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
            }
            
            ReuseCell = cell
        }
        else if dataType == "shop"
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashShop", for: indexPath) as! DashboardShopCollectionViewCell
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            if let shopproduct = data["shop_product"] as? [AnyObject]
            {
                //                debugPrint(indexPath.row)
                if indexPath.row < shopproduct.count
                {
                    cell.productName.text = shopproduct[indexPath.row]["title"] as? String
                    
                    
                    if shopproduct[indexPath.row]["product_variant_data"] is NSNull
                    {}
                    else
                    {
                        let price = shopproduct[indexPath.row]["product_variant_data"] as! AnyObject
                        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                        let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                        if price != nil
                        {
                            var product_price_tag = ""
                            var Price = 0.0
                            var retailPrice = 0.0
                            
                            if shopproduct[indexPath.row]["product_price_tag"] is NSNull
                            {}
                            else
                            {
                                product_price_tag = shopproduct[indexPath.row]["product_price_tag"] as! String
                            }
                            if self.user_purchase_modeid == "1"
                            {
                                if price["deal_price"] != nil
                                {
                                    if price["deal_price"] is NSNull
                                    {}
                                    else{
                                        if let price = price["deal_price"] as? String!
                                        {
                                            if (price != nil){
                                                Price = Double(Float(price!)!)
                                            }
                                            
                                            
                                        }}
                                    
                                } }
                            else
                            {
                                if price["bprice"] is NSNull
                                {}
                                else
                                {
                                    Price = Double(Float((price["bprice"] as? String)!)!)
                                }}
                            if price["retail_price"] is NSNull
                            {}
                            else
                            {
                                retailPrice = Double(Float((price["retail_price"] as? String)!)!)
                            }
                            if native.string(forKey: "Token")! == ""
                            {
                                cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                            }
                            else
                            {
                                if let currency = price["currency_symbol"] as? String
                                {
                                    self.Currency = currency
                                }
                                
                                cell.price.attributedText = ("\(self.Currency)\(String(format: "%.2f", Price))\(product_price_tag) ".color(textcolor).size(11) + " \(self.Currency)\(String(format: "%.2f", retailPrice))".color(graycolor).strikethrough(1).size(9)).attributedText
                            }
                        }
                    }
                    cell.Producttag.layer.cornerRadius = 5
                    cell.Producttag.clipsToBounds = true
                    if shopproduct[indexPath.row]["feature_name"] is NSNull
                    {
                        cell.Producttag.text = ""
                    }
                    else
                    {
                        if shopproduct[indexPath.row]["feature_name"] as! String != "Default"
                        {
                            cell.Producttag.text = " \(shopproduct[indexPath.row]["feature_name"] as! String) "
                            cell.Producttag.isHidden = false
                        }
                        else
                        {
                            cell.Producttag.isHidden = true
                        }
                    }
                    if let data = shopproduct[indexPath.row]["image_url"] as? AnyObject
                    {
                        if let image = data["200"] as? [AnyObject]
                        {
                            //                        debugPrint("200 images")
                            if image.count > 0
                            {
                                var imageUrl = image[0] as! String
                                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                                let url = NSURL(string: imageUrl)
                                if url != nil{
                                    DispatchQueue.main.async {
                                        cell.shopProductImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                }
                            }
                        }
                            
                        else if let image = data["200"] as? [AnyObject]
                        {
                            debugPrint("main image")
                            if image.count > 0
                            {
                                var imageUrl = image[0] as! String
                                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                                let url = NSURL(string: imageUrl)
                                if url != nil{
                                    DispatchQueue.main.async {
                                        cell.shopProductImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ReuseCell = cell
        }
        return ReuseCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var dataType = self.DataType[collectionView.tag]["Dicttype"] as! String
        if dataType == "banner"
        {
            
            
        }
        else if dataType == "category"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! [AnyObject]
            //            debugPrint("data", data)
            var categoryName = data[indexPath.row]["collection_name"] as? String
            var product_collectionid = (data[indexPath.row]["product_collectionid"] as? Int)!
            if data[indexPath.row]["icon_image_url"] is NSNull
            {
                native.set("", forKey: "bannerImage")
            }
            else
            {
                if let productImage = data[indexPath.row]["icon_image_url"] as? [AnyObject]
                {
                    
                    if productImage.count>1
                    {
                        if let image = productImage[1]["product_mobile_banner"] as? String
                        {
                            native.set("\(image)", forKey: "bannerImage")
                        }
                    }
                }
            }
            native.set(categoryName, forKey: "search")
            native.set("icon_category_elastic", forKey: "searchType")
            native.set(product_collectionid, forKey: "product_collectionid")
            native.synchronize()
            selectdashboardSubCategoryViewController.subcategoryid = (data[indexPath.row]["product_collectionid"] as? Int)!
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "iconCategory"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
            
        }
        else if dataType == "featured_shops"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! [AnyObject]
            var id = (data[indexPath.row]["shopid"] as? Int)!
            native.set(id, forKey: "entershopid")
            native.synchronize()
            //            debugPrint("product id:", id)
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shopDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        else if dataType == "categoryBanner"
        {
            
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            //            debugPrint("========", data, data["product_data"])
            
            if let shopproduct = data["product_data"] as? [AnyObject]
            {
                
                native.set(shopproduct[indexPath.row]["productid"]! as! Int, forKey: "FavProID")
                native.set("fav", forKey: "comefrom")
                native.synchronize()
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
                
                self.navigationController?.pushViewController(editPage, animated: true)
            }
        }
        else if dataType == "trendingPro"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! [AnyObject]
            native.set(data[indexPath.row]["productid"] as! Int, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        else if dataType == "shop"
        {
            var data = self.DataType[collectionView.tag]["dict"] as! AnyObject
            if let shopproduct = data["shop_product"] as? [AnyObject]
            {
                
                native.set(shopproduct[indexPath.row]["productid"]! as! Int, forKey: "FavProID")
                native.set("fav", forKey: "comefrom")
                native.synchronize()
                //        currntProID = self.ProductID[indexPath.row] as! Int
                
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
                
                self.navigationController?.pushViewController(editPage, animated: true)
            }
        }
        
    }
    
    // fetch dashboard data
    func parseData()
    {
        
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        //        if Token != ""
        //        {
        
        let b2burl = native.string(forKey: "b2burl")!
        var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_home_data/?version=1.0&device=ios") as! URL)
        if Token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(Token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        debugPrint(a, Token)
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            //                                debugdebugPrint(response)
            switch (response.result) {
            case .success:
                //                    debugPrint("response: \(response)")
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as! String == "success"{
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let innerdict1 = dict1["data"] as? AnyObject
                        {
                            
                            if innerdict1.count>0
                            {
                                self.DataType.removeAll()
                                debugPrint("innerdict", innerdict1)
                                if innerdict1["collection_banner_data"] is NSNull
                                {}
                                else
                                {
                                    if let bannerdata = innerdict1["collection_banner_data"] as? [AnyObject]
                                    {
                                        self.bannerData = bannerdata
                                    }
                                    
                                }
                                
                                if innerdict1["collection_icon_data"] is NSNull
                                {}
                                else
                                {
                                    
                                    if let data = innerdict1["collection_icon_data"] as? [AnyObject]
                                    {
                                        self.categoryData = data
                                    }}
                                if innerdict1["product_top_category_data"] is NSNull
                                {}
                                else
                                {
                                    self.product_top_category_data = innerdict1["product_top_category_data"] as! [AnyObject]
                                }
                                self.DataType.append(["Dicttype" : "banner", "dict": self.bannerData])
                                //                                self.DataType.append(["Dicttype" : "category", "dict": self.categoryData])
                                
                                
                                
                                
                                for i in 0..<self.product_top_category_data.count
                                {
                                    self.DataType.append(["Dicttype" : "categoryBanner", "dict": self.product_top_category_data[i]])
                                }
                                if innerdict1["product_data"] is NSNull
                                {}
                                else
                                {
                                    self.trendingPro = innerdict1["product_data"]  as! [AnyObject]
                                }
                                if innerdict1["featured_shops"] is NSNull
                                {}
                                else
                                {
                                    self.featured_shops = innerdict1["featured_shops"]  as! [AnyObject]
                                }
                                if innerdict1["shop_data"] is NSNull
                                {}
                                else
                                {
                                    self.shopProduct = innerdict1["shop_data"]  as! [AnyObject]
                                }
                                if self.trendingPro.count >= 0 && self.shopProduct.count > 0
                                {
                                    var Count = self.trendingPro.count / self.shopProduct.count
                                    var productDict = [AnyObject]()
                                    var start = 0
                                    var listCount = Count
                                    self.DataType.append(["Dicttype" : "featured_shops", "dict": self.featured_shops])
                                    for i in 0..<self.shopProduct.count
                                    {
                                        productDict.removeAll()
                                        for j in start..<listCount
                                        {
                                            productDict.append(self.trendingPro[j])
                                        }
                                        
                                        self.DataType.append(["Dicttype" : "shop", "dict": (self.shopProduct[i])])
                                        self.DataType.append(["Dicttype" : "trendingPro", "dict": productDict])
                                        
                                        if listCount < self.trendingPro.count - 1
                                        {
                                            
                                            start = start + Count
                                            listCount = listCount + Count
                                            
                                        }
                                        else
                                        {
                                            break
                                        } }
                                }
                            }
                        }
                    }
                    //                        self.getRecommended_product(limit: self.limit, offset: self.offset)
                    //                        MainDashboardViewController.tabText.removeAll()
                    //                        MainDashboardViewController.tabID.removeAll()
                    //
                    //                       MainDashboardViewController.tabText.append("Feed")
                    //                        MainDashboardViewController.tabText.append("Deal")
                    //                        MainDashboardViewController.tabText.append("Featured")
                    //
                    //                       MainDashboardViewController.tabID.append(0)
                    //                        MainDashboardViewController.tabID.append(0)
                    //                        MainDashboardViewController.tabID.append(0)
                    //                        for i in 0..<self.categoryData.count
                    //                        {
                    //                           
                    //                        MainDashboardViewController.tabText.append(self.categoryData[i]["collection_name"] as! String)
                    //                            MainDashboardViewController.tabID.append((self.categoryData[i]["product_collectionid"] as? Int)!)
                    //                        }
                    NotificationCenter.default.post(name: NSNotification.Name("load"), object: nil)
                    self.refreshControl.endRefreshing()
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    //                        self.dashboardTable.isHidden = false
                    self.dashboardTable.reloadData()
                    self.refreshControl.endRefreshing()
                }
                //
                break
            case .failure(let error):
                //                debugPrint(error)
                self.refreshControl.endRefreshing()
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .bottom)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
            }
        }
        
    }
    //    }
    
    
    
    ////get all key from api
    func getAllKey()
    {
        let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            if native.object(forKey: "countryid") != nil
            {
                let countryid = native.string(forKey: "countryid")!
                let b2burl = native.string(forKey: "b2burl")!
                
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                var url = "\(b2burl)/users/get_key/"
                debugPrint("url", url, token)
                
                Alamofire.request(url, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    //                                debugdebugPrint(response)
                    //                                debugdebugPrint(response)
                    debugPrint("response API key: \(response)")
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        switch response.result
                        {
                        case .failure(let error):
                            //                debugPrint(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                debugPrint("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                debugPrint(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                debugPrint("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                    // STATUS AS RESPONCE
                                else if dict1["response"] as! String == "success"
                                {
                                    if dict1["place_api_key"] is NSNull
                                    {}
                                    else
                                    {
                                        self.native.set(dict1["place_api_key"] as? String, forKey: "place_api_key")
                                        self.native.synchronize()
                                    }
                                    if dict1["key"] is NSNull
                                    {}
                                    else
                                    {
                                        self.native.set(dict1["key"] as! String, forKey: "RazorPay")
                                        self.native.synchronize()
                                    }
                                    if dict1["stripe_key"] is NSNull
                                    {}
                                    else
                                    {
                                        self.native.set(dict1["stripe_key"] as! String, forKey: "stripe_key")
                                        self.native.synchronize()
                                    }
                                    
                                }
                                else if dict1["response"] as! String == "failure"
                                {
                                    
                                    
                                }
                            }
                            
                        }}
                    
                }
            }
        }
    }
    
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        
        if ((dashboardTable.contentOffset.y + dashboardTable.frame.size.height) >= dashboardTable.contentSize.height)
        {
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: dashboardTable.bounds.width, height: CGFloat(44))
            
            debugPrint(isDataLoading)
            if !isDataLoading{
                isDataLoading = true
                
                debugPrint(self.DataType.count, self.offset)
                if self.DataType.count >= self.offset
                {
                    self.offset=self.offset+10
                    
                    
                    //                    getRecommended_product(limit: limit, offset: offset)
                    self.dashboardTable.tableFooterView = spinner
                    self.dashboardTable.tableFooterView?.isHidden = false
                    //                loadCallLogData(offset: self.offset, limit: self.limit)
                }}}}
    
    
    
    
    
}




