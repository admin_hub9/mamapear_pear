//
//  CommentTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 30/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
   
        @IBOutlet weak var userImage: RoundedImageView!
        @IBOutlet weak var NameAndText: UILabel!
        
        @IBOutlet weak var editButton: UIButton!
        
        @IBOutlet weak var commentText: UILabel!
        
        @IBOutlet weak var commentTime: UILabel!
        
        @IBOutlet weak var deleteButton: UIButton!
        
    @IBOutlet weak var seprator: UIView!
    
    @IBOutlet weak var userProfile: UIButton!
    
    
        override func awakeFromNib() {
            super.awakeFromNib()
            self.selectionStyle = .none
            // Initialization code
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }

    }
