//
//  FeedCommentDataViewController.swift
//  Hub9
//
//  Created by Deepak on 30/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AttributedTextView

class FeedCommentDataViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate {
    
    var native = UserDefaults.standard
    var data = [AnyObject]()
    var owner = [String]()
    var replyStatus = false
    
    ///0 for feed controller
    ///1 for user feed details
    ///2 for My feed
    /// 3 for search redirect
    var feedComment_screen = 0
    var is_editing = false
    var editIndex = 0
    var user_feedid = 0
    var limit = 20
    var offset = 0
    var comment_count = 0
    
    
    
    
    @IBOutlet weak var comment: UITextField!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var commentTable: UITableView!
    
    @IBOutlet weak var myPic: UIImageView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        comment.resignFirstResponder()
        self.tabBarController?.tabBar.isHidden = false
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.bottomConstraint.constant = 0
        self.getFeedComment(limit: self.limit, offset: self.offset)
        self.commentTable.tableFooterView = UIView()
        self.commentTable.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
            comment.delegate = self
       
        self.myPic.layer.cornerRadius = self.myPic.frame.height/2
        self.myPic.clipsToBounds=true
        if native.object(forKey: "profilepic") != nil
        {
            let image = native.string(forKey: "profilepic")!
            
            if image != nil{
                let url = URL(string: image)
                if url != nil
                {
                    DispatchQueue.main.async {
                        self.myPic.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                   }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        comment.resignFirstResponder()
        self.is_editing = false
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.bottomConstraint.constant == 0 {
                self.bottomConstraint.constant = keyboardSize.height + 20
                
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.bottomConstraint.constant != 0 {
            self.bottomConstraint.constant = 0
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         
            return UITableViewAutomaticDimension
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if data.count > 0
            {
            return data.count
            }
            else
            {
                return 0
            }
        }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.owner[indexPath.row] == "Sender"
        {
            self.replyStatus=true
            self.editIndex = indexPath.row
            self.comment.becomeFirstResponder()
        }
    }
    
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            var reuseCell = UITableViewCell()
            if self.owner[indexPath.row] == "Sender"
            {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
           
            let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if indexPath.row == 0
                {
                  cell.seprator.isHidden=true
                }
                else
                {
                    cell.seprator.isHidden=false
                }
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                
                if data[indexPath.row]["user_id"] is NSNull
                {}
                else
                {   if Int(userid) != (data[indexPath.row]["user_id"] as? Int)!
                {
                    cell.deleteButton.isHidden = true
                    cell.editButton.isHidden = true
                }
                    else
                {
                    cell.deleteButton.isHidden = false
                    cell.editButton.isHidden = true
                }
                    
                }
            }
            if data[indexPath.row]["profile_pic_url"] is NSNull
            {}
            else
            {

                let imagedata = data[indexPath.row]["profile_pic_url"] as? String

                   var imageUrl = (imagedata)!
                   imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                   if let url = NSURL(string: imageUrl )
                   {
                       if url != nil
                       {
                        DispatchQueue.main.async {
                           cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                       }
                       }
                   }
                }

            var like = 0
            var comment = 0
            
            
            if data[indexPath.row]["comment"] is NSNull
            {
                cell.NameAndText.text = ""
            }
            else
            {
                
                cell.NameAndText.attributedText = ("\(data[indexPath.row]["first_name"] as! String) \(data[indexPath.row]["last_name"] as! String) ".color(lightBlackColor).size(16.0)).attributedText
                print("comment data", data)
                cell.commentText.attributedText = ( "\(data[indexPath.row]["comment"] as! String)".color(lightGrayColor).size(14.0)).attributedText
                if data[indexPath.row]["inserted_at"] is NSNull
                {}
                else
                {
                     if let date1 = self.data[indexPath.row]["inserted_at"] as? String
                        {
                        
                        let time  = self.relativeDate( time: Double(date1)!)
                            print("time", time)
                        cell.commentTime.text = "\(time)"
                    }
                }
                
                
            }
            
            
            cell.editButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(updateEditButton(Feed_Index:)), for: .touchUpInside)
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
             
            reuseCell = cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "replyCommentCell", for: indexPath) as! ReplyCommentTableViewCell
                
                 let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                 let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                 
                 if self.native.object(forKey: "userid") != nil
                 {
                     let userid = self.native.string(forKey: "userid")!
                     print("bjhfb 74", userid, self.native.string(forKey: "userid")!)
                     
                     if data[indexPath.row]["user_id"] is NSNull
                     {}
                     else
                     {   if Int(userid) != (data[indexPath.row]["user_id"] as? Int)!
                     {
                         cell.deleteButton.isHidden = true
                         cell.editButton.isHidden = true
                     }
                         else
                     {
                         cell.deleteButton.isHidden = false
                         cell.editButton.isHidden = true
                     }
                         
                     }
                 }
                 if data[indexPath.row]["profile_pic_url"] is NSNull
                 {}
                 else
                 {

                     let imagedata = data[indexPath.row]["profile_pic_url"] as? String

                        var imageUrl = (imagedata)!
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                        if let url = NSURL(string: imageUrl )
                        {
                            if url != nil
                            {
                                DispatchQueue.main.async {
                                    cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        }
                     }

                 var like = 0
                 var comment = 0
                 
                 
                 if data[indexPath.row]["reply"] is NSNull
                 {
                     cell.NameAndText.text = ""
                 }
                 else
                 {
                     
                     cell.NameAndText.attributedText = ("\(data[indexPath.row]["first_name"] as! String) \(data[indexPath.row]["last_name"] as! String) ".color(lightBlackColor).size(16.0)).attributedText
                    print("reply comment", indexPath.row, self.owner[indexPath.row], self.data[indexPath.row])
                     cell.commentText.attributedText = ( "\(data[indexPath.row]["reply"] as! String)".color(lightGrayColor).size(14.0)).attributedText
                     if data[indexPath.row]["inserted_at"] is NSNull
                     {}
                     else
                     {
                          if let date1 = self.data[indexPath.row]["inserted_at"] as? String
                             {
                             
                             let time  = self.relativeDate( time: Double(date1)!)
                                 print("time", time)
                             cell.commentTime.text = "\(time)"
                         }
                     }
                     
                     
                 }
                 
                 
                 cell.editButton.tag = indexPath.row
//                 cell.editButton.addTarget(self, action: #selector(updateEditButton(Feed_Index:)), for: .touchUpInside)
                 cell.deleteButton.tag = indexPath.row
                 cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
                
                reuseCell = cell
            }
            
            return reuseCell
            
        }
    
    /// delete button
    
    @objc func deleteButton(Feed_Index: UIButton)
    {
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
                if self.native.object(forKey: "userid") != nil && data.count>Feed_Index.tag
            {
                var userid = 0
                
                if self.owner[Feed_Index.tag] == "Sender"
                {
                    userid = (data[Feed_Index.tag]["user_feed_commentid"] as? Int)!
                }
                else
                {
                    userid = (data[Feed_Index.tag]["user_feed_sub_commentid"] as? Int)!
                }
            
               
            //        print("product id:", id)
                let parameters: [String:Any] = ["user_feed_commentid": userid]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/delete_feed_comment/"
            
            print("follow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    
                            
                            if self.data.count > Feed_Index.tag
                            {
                                self.data.remove(at: Feed_Index.tag)
                                
                                if self.comment_count>0
                                {
                                    self.comment_count = self.comment_count - 1
                                }
                            }
                        if self.owner[Feed_Index.tag] == "Sender"
                        {
                        if self.comment_count == 0
                        {
                            self.navigationController?.navigationBar.topItem?.title = "Comment"
                        }
                        else
                        {
                            self.navigationController?.navigationBar.topItem?.title = "Comment (\(self.comment_count))"  }
                            }
                            DispatchQueue.main.async {
                                self.owner.remove(at: Feed_Index.tag)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        self.commentTable.reloadData()
                            }
                    }
                }}
                }
                }}
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
    
    
    
    @objc func updateEditButton(Feed_Index: UIButton)
    {
        self.is_editing = true
        self.editIndex = Feed_Index.tag
        self.comment.becomeFirstResponder()
        self.comment.text = (data[Feed_Index.tag]["comment"] as? String)!
    }
    
    /// like button
    
    func editButton(editIndex: Int)
    {
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
            
               
            //        print("product id:", id)
                let parameters: [String:Any] = ["user_feed_commentid":(data[editIndex]["user_feed_commentid"] as? Int)!, "comment": self.comment.text!]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/edit_feed_comment/"
            
            print("edit comment", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    
                            var first_name = ""
                                    var last_name = ""
                                    var profile_pic_url = "";
                                    var user_feed_commentid = 0;
                                    var user_id = 0
                                    
                                    if (dict1["status"] as? String)! == "success"
                                    {
                                     self.is_editing = false
                                    if self.native.object(forKey: "userid") != nil
                                    {
                                        user_id = Int(self.native.string(forKey: "userid")!)!
                                    }
                                    
                                    if self.native.object(forKey: "profilepic") != nil
                                    {
                                        profile_pic_url = self.native.string(forKey: "profilepic")!
                                    }
                                    if self.native.object(forKey: "firstname") != nil
                                    {
                                        first_name = self.native.string(forKey: "firstname")!
                                    }
                                    if self.native.object(forKey: "lastname") != nil
                                    {
                                        last_name = self.native.string(forKey: "lastname")!
                                    }
                                    if self.native.object(forKey: "userid") != nil
                                    {
                                        last_name = self.native.string(forKey: "userid")!
                                    }
                                    if self.native.object(forKey: "lastname") != nil
                                    {
                                        last_name = self.native.string(forKey: "lastname")!
                                    }
                                
                            var timestamp = NSDate().timeIntervalSince1970
                            var commentID = (self.data[editIndex]["user_feed_commentid"] as? Int)!
                            if self.data.count > editIndex
                            {
                                self.data.remove(at: editIndex)
                                self.data.insert(["comment": self.comment!.text!,
                                         "first_name": first_name,
                                         "inserted_at": "\(timestamp)",
                                         "last_name": last_name,
                                         "profile_pic_url": "\(profile_pic_url)",
                                    "user_feed_commentid": commentID,
                                "user_id": user_id] as AnyObject, at: editIndex)
                                self.comment.text = ""
                                
                            }
                            }
                        DispatchQueue.main.async {
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        self.commentTable.reloadData()
                            }
                    }
                }}
                }
                }}
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        if replyStatus == false && self.comment.text!.count > 0
        {
        self.sendComment()
        }
        else
        {
            
            self.replyComment()
        }
        
        return true
        
    }
    
    
     func sendComment() {
        
        if comment.text != ""
        {
            self.comment.resignFirstResponder()
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
            
                var user_feedid = 0
                if feedComment_screen == 0
                {
                    user_feedid = FeedViewController.user_feed_id
                }
                else if feedComment_screen == 1
                {
                    user_feedid = UserFeedPostViewController.user_feed_id
                }
                else if feedComment_screen == 2
                {
                    user_feedid = MyFeedPostViewController.user_feed_id
                }
            //        print("product id:", id)
                let parameters: [String:Any] = ["user_feedid":user_feedid,"comment": comment.text!]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/add_comment_to_feed/"
            
            
            print("comment product", url, parameters)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                print("resonce comment", response)
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                   
                        
                            var first_name = ""
                            var last_name = ""
                            var profile_pic_url = "";
                            var user_feed_commentid = 0;
                            var user_id = 0
                            
                            if dict1["response"] is NSNull
                            {}
                            else
                            {
                                if let commentData = dict1["response"] as? AnyObject
                                {
                                    if let data = commentData["response"]
                                    {
                                    if commentData["response"] is NSNull
                                    {}
                                    else
                                    {
                                       user_feed_commentid = (commentData["response"] as? Int)!
                                    }
                                    }
                                }
                            }
                            
                            
                            
                            if self.native.object(forKey: "userid") != nil
                            {
                                user_id = Int(self.native.string(forKey: "userid")!)!
                            }
                            
                            if self.native.object(forKey: "profilepic") != nil
                            {
                                profile_pic_url = self.native.string(forKey: "profilepic")!
                            }
                            if self.native.object(forKey: "firstname") != nil
                            {
                                first_name = self.native.string(forKey: "firstname")!
                            }
                            if self.native.object(forKey: "lastname") != nil
                            {
                                last_name = self.native.string(forKey: "lastname")!
                            }
                            if self.native.object(forKey: "userid") != nil
                            {
                                last_name = self.native.string(forKey: "userid")!
                            }
                            if self.native.object(forKey: "lastname") != nil
                            {
                                last_name = self.native.string(forKey: "lastname")!
                            }
                        
                    var timestamp = NSDate().timeIntervalSince1970
                            var allComment = self.data
                                 self.data.removeAll()
                                 
                            self.data.append(["comment": self.comment!.text!,
                                     "first_name": first_name,
                                     "inserted_at": "\(timestamp)",
                                     "last_name": last_name,
                                     "profile_pic_url": "\(profile_pic_url)",
                            "user_feed_commentid": user_feed_commentid,
                            "user_id": user_id] as AnyObject)

                                 for var i in 0..<allComment.count
                                 {
                                     self.data.append(allComment[i])
                                 }
                           
                    
                    self.comment_count = self.comment_count + 1
                            
//                    self.owner.append("Receiver")
                            let ownerData = self.owner
                            self.owner.removeAll()
                    
                    self.owner.append("Sender")
                            for var i in 0..<ownerData.count
                            {
                                self.owner.append(ownerData[i])
                            }
                    if self.comment_count == 0
                    {
                        self.navigationController?.navigationBar.topItem?.title = "Comment"
                    }
                    else
                    {
                        self.navigationController?.navigationBar.topItem?.title = "Comment (\(self.comment_count))"                                }
                    self.comment.text = ""
                            
                    DispatchQueue.main.async {
                    self.commentTable.reloadData()
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        self.commentTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                            }
                    
                    }
                }}
                }
                }}
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
        else
        {
            self.view.makeToast("Please type comment first.", duration: 2.0, position: .center)
        }
    }
    
    
    func replyComment() {
            
            if comment.text != ""
            {
                self.comment.resignFirstResponder()
            let token = self.native.string(forKey: "Token")!
            var fav = 0
                if token != ""
                {
                    CustomLoader.instance.showLoaderView()
                
                    let parameters: [String:Any] = ["user_feed_commentid": (data[editIndex]["user_feed_commentid"] as? Int)!,"reply_text": comment.text!]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
            
                let b2burl = native.string(forKey: "b2burl")!
                var url = "\(b2burl)/users/reply_to_comment/"
                
                
                print("comment product", url, parameters)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    print("resonce comment", response)
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    
                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                       
                            
                                var first_name = ""
                                var last_name = ""
                                var profile_pic_url = "";
                                var user_feed_commentid = 0;
                                var user_id = 0
                                
                                if dict1["response"] is NSNull
                                {}
                                else
                                {
                                    
                                }
                                
                                
                         
                        self.comment.text = ""
                                
                    DispatchQueue.main.async {
                    self.comment.resignFirstResponder()
                    self.replyStatus=false
                    self.data.removeAll()
                    self.owner.removeAll()
                    self.getFeedComment(limit: self.limit, offset: self.offset)
                    CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                            
                                }
                        
                        }
                        }}}}
                    
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
            }
            else
            {
                self.view.makeToast("Please type comment first.", duration: 2.0, position: .center)
                }
          
            
        }
    
    
    func getFeedComment(limit: Int, offset: Int)
            {
                
        //        CustomLoader.instance.showLoaderView()
                let token = self.native.string(forKey: "Token")!
                
                let b2burl = native.string(forKey: "b2burl")!
                if native.object(forKey: "userid") != nil
                {
                    var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed_data/?user_feedid=\(self.user_feedid)&limit=\(limit)&offset=\(offset)") as! URL)
                if token != ""
                {
                    a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
                }
                else{
                    
                    a.allHTTPHeaderFields = ["Content-Type": "application/json"]
                }
                print("feed comments", a, token)
                Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    debugPrint(response)
                    var url =
                    print("feed url recommended", a, token)
                    
                    
                switch response.result {
                case .success:
                    
                    
                    if let result1 = response.result.value
                    {
                        let result = response.result
                       
                       print("result feed video", response)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            else if dict1["status"] as! String == "success"
                            {
                                if dict1["feed_data"] is NSNull
                                {}
                                else
                                {
                                if let innerdict = dict1["feed_data"] as? AnyObject
                                {
                                    if innerdict.count > 0
                                       {
                           
                               var data = [[AnyObject]]()
                                
                                        
                            if innerdict["comment_count"] is NSNull
                            { self.navigationController?.navigationBar.topItem?.title = "Comment"
                            }
                            else if let count = innerdict["comment_count"] as? Int
                            {
                                if count == 0
                                {
                                    self.navigationController?.navigationBar.topItem?.title = "Comment"
                                }
                                else
                                {
                                    self.navigationController?.navigationBar.topItem?.title = "Comment (\(count))"                                }
                                self.comment_count = count
                                        }
                                if innerdict["comment_data"] is NSNull
                                {
                                    
                                }
                                else if let commentdata = innerdict["comment_data"] as? [AnyObject]
                                {
                                   
                    for var i in 0..<commentdata.count
                    {
                        self.owner.append("Sender")
                        self.data.append((commentdata[i] as? AnyObject)!)
                        if commentdata[i]["reply_comment_data"] is NSNull
                            {
                                
                        }
                            else if let replyComment
                                 = commentdata[i]["reply_comment_data"] as? [AnyObject]
                            {
                            for var j in 0..<replyComment.count
                                {
                                self.owner.append("Receiver")
                                self.data.append((replyComment[j] as? AnyObject)!)
                                        }
                                        }
                                        
                                    }
                                    }
                                        
                                    }
                                
                                    }
                                 CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                                        
                                    
                                }
                                }}}
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
            }
                    
                    DispatchQueue.main.async {
                    self.commentTable.isHidden = false
    //                self.refreshControl.endRefreshing()
                    self.commentTable.reloadData()
                    }
                    }
                    
                    DispatchQueue.main.async {
                        self.commentTable.isHidden = false
    //                    self.refreshControl.endRefreshing()
                        self.commentTable.reloadData()
    //                    self.refreshControl.endRefreshing()
                        CustomLoader.instance.hideLoaderView()
                    }
                    UIApplication.shared.endIgnoringInteractionEvents()
                       
                    
                }
                else
                {
    //                self.refreshControl.endRefreshing()
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            }
    
    func relativeDate(time: Double) -> String {
        
        let theDate = Date(timeIntervalSince1970: TimeInterval(time))
        let components = Calendar.current.dateComponents([.day, .year, .month, .weekOfYear], from: theDate, to: Date())
        if let year = components.year, year == 1{
            return "\(year) year ago"
        }
        if let year = components.year, year > 1{
            return "\(year) years ago"
        }
        if let month = components.month, month == 1{
            return "\(month) month ago"
        }
        if let month = components.month, month > 1{
            return "\(month) months ago"
        }

        if let week = components.weekOfYear, week == 1{
            return "\(week) week ago"
        }
        if let week = components.weekOfYear, week > 1{
            return "\(week) weeks ago"
        }

        if let day = components.day{
            if day > 1{
                return "\(day) days ago"
            }else{
                "Yesterday"
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: theDate)
        return strDate
    }

}
