//
//  LoginSuccessViewController.swift
//  Hub9
//
//  Created by Deepak on 6/18/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class LoginSuccessViewController: UIViewController {

    @IBOutlet var signInSuccess: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        ///next button
        signInSuccess.layer.cornerRadius = signInSuccess.layer.frame.size.height/2
        signInSuccess.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 238/255.0, green: 90/255.0, blue: 95/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 241/255.0, green: 146/255.0, blue: 152/255.0, alpha: 1.00).cgColor
        
        signInSuccess.applyendGradient(colors: [loginstartColor, loginendColor])

        // Do any additional setup after loading the view.
    }

}
