//
//  Util.swift
//  Hub9
//
//  Created by Deepak on 2/12/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import Foundation
import Alamofire

class Util:NSObject {
    
    // #MARK: Singleton Instance
    static var shared = Util()
    // #MARK: User Default instance
    let native = UserDefaults.standard
    // #MARK: AppDelegate Instance
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // #MARK: fn(Serialize JSON)
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // #MARK: fn(notification defaults)
    //        (local persistant storage)
    func setNotificationDefaults() {
        native.set(false, forKey: "notificationsEnabled")
        native.set(true, forKey: "soundEnabled")
        native.set(true, forKey: "vibrationEnabled")
        native.set(24, forKey: "currentInterval")
        native.synchronize()
    }
    
    // #MARK: fn(Log Out)
    func logUserOut() {
        
        // Logging out via the sellerhub API
        if self.native.object(forKey: "Token") != nil {
            let header = ["Content-Type":"application/json", "Authorization": "Token \(self.native.object(forKey: "Token")!)"]
            Alamofire.request("https://app1.hub9.io/api/logout_user/", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { (response) in
                debugPrint(response)
                print("response: \(response)")
                if let json = response.result.value {
                    print ("JSON: \(json)")
                    let jsonString = "\(json)"
                    let dict = self.convertToDictionary(text: jsonString)
                    if dict == nil  {
                        print("Dict is nil")
                    }
                    if "\(dict?["detail"])" == "Invalid token." {
//                        self.appDelegate.toggleLeftDrawer(self, animated: true)
                    }
                }
                if response.result.isFailure {
//                    self.appDelegate.toggleLeftDrawer(self, animated: true)
                }
                
                let sessionManager = Alamofire.SessionManager.default
                sessionManager.session.getAllTasks(completionHandler: { tasks in
                    tasks.forEach{$0.cancel()}
                })
            })
        }
        
//        self.native.set(true, forKey: "first_time")
//        self.native.removeObject(forKey: "Token")
//        self.native.removeObject(forKey: "logged_in")
//        self.native.removeObject(forKey: "loggedInUser")
//        self.native.removeObject(forKey: "csrf_token")
//        self.native.removeObject(forKey: "dashboard")
//        self.native.synchronize()
        let cookieJar = HTTPCookieStorage.shared
        for cookie in cookieJar.cookies! {
            cookieJar.deleteCookie(cookie)
        }
        print("local variables erased")
        
    }
    
    // #MARK: fn(Internet connectivity)
    func checkNetworkTapped()->Bool {
        
        var check : Bool = false
        
        let reachabilityManager = NetworkReachabilityManager()
        reachabilityManager!.listener = { status in
            
            switch status {
                
            case .notReachable:
                print("The network is not reachable")
//                self.onInternetDisconnection()
                check = false
                
            case .unknown :
                print("It is unknown whether the network is reachable")
//                self.onInternetDisconnection() // not sure what to do for this case
                check = false
                
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
//                self.onInternetConnection()
                check = true
            case .reachable(.wwan):
                print("The network is reachable over the WWAN connection")
//                self.onInternetConnection()
                check = true
                
            }
        }
        
        return check
    }
   
    
}
