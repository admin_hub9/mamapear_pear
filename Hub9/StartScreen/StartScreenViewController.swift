//
//  StartScreenViewController.swift
//  Hub9
//
//  Created by Deepak on 05/08/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class StartScreenViewController: UIViewController, UIPageViewControllerDataSource,
UIPageViewControllerDelegate, tutorialPageTurnerDelegate {
    
    // #MARK: Helper Variables
    var turn: UIPageViewController!
    let pages = ["tutorialOne","tutorialTwo","tutorialThird", "tutorialFourth", "tutorialFifth"]
    var i: Int!
    
    // #MARK: Common Outlet for tutorial
    @IBOutlet var pageLight: UIPageControl!
    
    // #MARK: View Function
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let take = storyboard?.instantiateViewController(withIdentifier: "pageView") {
            
            self.addChildViewController(take)
            self.view.addSubview(take.view)
            take.view.addSubview(pageLight)
            take.view.bringSubview(toFront: pageLight)
            
            turn = take as! UIPageViewController
            turn.dataSource = self
            turn.delegate = self
            
            turn.setViewControllers([viewControllerAtIndex(0)!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
            turn.didMove(toParentViewController: self)
            
        }
        
    }
    
    // #MARK: Page Control protocol functions
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(completed) {
            
            if let enlarge = pageViewController.viewControllers?.last {
                if let index = pages.index(of: enlarge.restorationIdentifier!) {
                    pageLight.currentPage = index
                }
            }
            
        }
    }
    
    
    // required for protocol
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if let index = pages.index(of: viewController.restorationIdentifier!) {
            
            // pageLight.currentPage = index
            
            if index < pages.count - 1 {
                
                let view = storyboard?.instantiateViewController(withIdentifier: pages[index+1])
                return view
                
            }
            
        }
        
        //pageLight.currentPage = pages.count - 1
        return nil
    }
    
    
    
    // required for protocol
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if let index = pages.index(of: viewController.restorationIdentifier!) {
            
            //pageLight.currentPage = index
            
            if index > 0{
                
                let view = storyboard?.instantiateViewController(withIdentifier: pages[index-1])
                
                return view
                
            }
            
        }
        
        //pageLight.currentPage = 0
        return nil
    }
    
    // #MARK: Custom Protocol Function to move to next page after save
    func saveActionCompleted() {
        
        turn.setViewControllers([viewControllerAtIndex(1)!], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        pageLight.currentPage = 1
        turn.didMove(toParentViewController: self)
        
    }
    
    // #MARK: Helper Function to return Index
    func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: pages[index])
        
        if index == 0 {
            let InputViewController = vc as! MamaScreenViewController
            InputViewController.delegate = self
        }
        
        return vc
        
    }
    
}


