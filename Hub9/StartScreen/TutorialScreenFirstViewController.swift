//
//  TutorialScreenFirstViewController.swift
//  Hub9
//
//  Created by Deepak on 06/08/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class TutorialScreenFirstViewController: UIViewController {

    @IBOutlet weak var Container: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Container.layer.shadowPath = UIBezierPath(rect: Container.bounds).cgPath
        Container.layer.shadowRadius = 5
        Container.layer.shadowOffset = .zero
        Container.layer.shadowOpacity = 1
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
