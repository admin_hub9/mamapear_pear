//
//  TutorialScreenFourthViewController.swift
//  Hub9
//
//  Created by Deepak on 06/08/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire



class TutorialScreenFourthViewController: UIViewController {
    
    var native = UserDefaults.standard
    var alamoFireManager : SessionManager?
    var window: UIWindow?
    
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var Container: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueButton.layer.cornerRadius = continueButton.frame.height/2
        continueButton.clipsToBounds=true
        continueButton.layer.borderColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1).cgColor
        continueButton.layer.borderWidth = 1.0
        
        // shadwo
        Container.layer.shadowPath = UIBezierPath(rect: Container.bounds).cgPath
        Container.layer.shadowRadius = 5
        Container.layer.shadowOffset = .zero
        Container.layer.shadowOpacity = 1
        // Do any additional setup after loading the view.
    }
    
    @IBAction func SkiptoRoot(_ sender: Any) {
        skipLogin()
    }
    
    func skipLogin() {
            CustomLoader.instance.showLoaderView()
            var userid = "0"
            var passcode = "0"
            var countrycode = "0"
            var userCode = "0"
        if native.object(forKey: "userid") != nil
        {
            userid = native.string(forKey: "userid")!
        }
        if native.object(forKey: "country_code") != nil
        {
            passcode = native.string(forKey: "country_code")!
        }
        if native.object(forKey: "countryid") != nil
        {
            countrycode = native.string(forKey: "countryid")!
        }
        if native.object(forKey: "usertype") != nil
        {
            userCode = native.string(forKey: "usertype")!
        }
        
        let type = MamaScreenViewController()
        var usertyptid = type.userTypeID
        let parameters: [String:Any] = ["userid":userid ,"countryid":countrycode, "account_typeid": userCode,"user_typeid": usertyptid]
            let header = ["Content-Type": "application/json"]
            let b2burl = native.string(forKey: "b2burl")!
            print("Successfully post", parameters)
            
            Alamofire.request("\(b2burl)/users/update_user_country/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                        
                        
                        guard response.data != nil else { // can check byte count instead
                            let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        
                        if let json = response.result.value {
                            let jsonString = "\(json)"
                            let result = response.result
                            switch response.result
                            {
                            case .failure(let error):
                                //                print(error)
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let err = error as? URLError, err.code == .notConnectedToInternet {
                                    // Your device does not have internet connection!
                                    print("Your device does not have internet connection!")
                                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                                    print(err)
                                }
                                else if error._code == NSURLErrorTimedOut {
                                    print("Request timeout!")
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                                }else {
                                    // other failures
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                                }
                                
                            case .success:
                            print(result)
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                
                                if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                                {   CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    if  root.sentFromLogin == true
                                    {
                                        self.ThirdPartyLogin(email: root.emailid, password: root.pass, firstname: root.username, lastname: root.lastname)
                                    }
                                    else if loginView.sentFromLogin == true
                                    {
                                        self.ThirdPartyLogin(email: loginView.emailid, password: loginView.pass, firstname: loginView.username, lastname: loginView.lastname)
                                    }
                                    else
                                    {
                                        self.loginUser()
                                    }
        
                                }
                                else
                                {
                                    CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                     self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .top)
                                    
                                }
                            }
                            
                            
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        
                        }
                    }
    }
    
    
    
    //remove special character from string
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    func loginUser()
        {
            
            var username1 = self.native.string(forKey: "username")!
            var password1 = self.native.string(forKey: "password")!
            let countrycode = native.string(forKey: "country_code")!
            var Pushtoken = ""
            let b2burl = native.string(forKey: "b2burl")!
            if native.string(forKey: "B2BTokenForSNS") != nil
            {
                Pushtoken = native.string(forKey: "B2BTokenForSNS")!
            }
            let deviceId = UIDevice.current.identifierForVendor?.uuidString
            print(deviceId!)
            var devicename = UIDevice.current.name
            devicename = removeSpecialCharsFromString(text: devicename)
            let deviceinfo:[String:Any] = ["DeviceName": devicename, "DeviceModel": UIDevice.current.modelName, "DeviceVersion": UIDevice.current.systemVersion]
            let parameters: [String:Any] = [ "username":username1, "password":password1, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_token": Pushtoken, "country_code": countrycode]
            CustomLoader.instance.showLoaderView()
            let header = ["Content-Type": "application/json"]
            print("Successfully post---", parameters)
            //
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 10
            configuration.timeoutIntervalForResource = 10
            alamoFireManager = Alamofire.SessionManager(configuration: configuration)
            //
            alamoFireManager!.request("\(b2burl)/users/login_user/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                print(response)
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    //                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
    //                print(result)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        
                        if dict1["status"] as! String  == "success" || dict1["status"] as! String  == "Success"
                        {   CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.native.set(dict1["token"]!, forKey: "Token")
                             let data = dict1["user_data"] as! NSDictionary
                            if data["shopid"] is NSNull
                            {
                                self.native.set("", forKey: "shopid")
                            }
                            else
                            {
                            self.native.set(data["shopid"], forKey: "shopid")
                            }
                            if data["first_name"] is NSNull
                            {
                                self.native.set("", forKey: "firstname")
                            }
                            else
                            {
                                self.native.set(data["first_name"]! as? String, forKey: "firstname")
                            }
                            if data["last_name"] is NSNull
                            {
                                self.native.set(" ", forKey: "lastname")
                            }
                            else
                            {
                                self.native.set(data["last_name"]! as? String, forKey: "lastname")
                            }
                            if data["support_firebaseid"] is NSNull
                            {
                                self.native.set("", forKey: "support_firebaseid")
                            }
                            else
                            {
                                self.native.set(data["support_firebaseid"], forKey: "support_firebaseid")
                            }
                            
                            if data["support_profile_pic_url"] is NSNull
                            {
                                self.native.set("", forKey: "support_profile_pic_url")
                            }
                            else
                            {
                                self.native.set(data["support_profile_pic_url"], forKey: "support_profile_pic_url")
                            }
                            if data["support_name"] is NSNull
                            {
                                self.native.set("", forKey: "support_name")
                            }
                            else
                            {
                                self.native.set(data["support_name"], forKey: "support_name")
                            }
                            if data["country_code"] is NSNull
                            {
                                self.native.set("", forKey: "country_code")
                            }
                            else
                            {
                                self.native.set(data["country_code"], forKey: "country_code")
                            }
                            if data["firebase_userid"] is NSNull
                            {
                                self.native.set("", forKey: "firebaseid")
                            }
                            else
                            {
                            self.native.set(data["firebase_userid"], forKey: "firebaseid")
                            }
                            if data["userid"] is NSNull
                            {
                                self.native.set("", forKey: "userid")
                            }
                            else
                            {
                            self.native.set(data["userid"], forKey: "userid")
                            }
                            if data["notification_active"] is NSNull
                            {
                                self.native.set("1", forKey: "notification")
                            }
                            else
                            {
                            self.native.set(data["notification_active"], forKey: "notification")
                            }
                            if data["profile_pic_url"] is NSNull
                            {
                                self.native.set("", forKey: "profilepic")
                            }
                            else
                            {
                                self.native.set(data["profile_pic_url"]! as? String, forKey: "profilepic")
                            }
                          
                            self.native.synchronize()
                            var type = self.native.string(forKey: "changetype")!
                            if type == "1"
                            {
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                            else
                            {
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                            
                            
                        }
                    }
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                    
                    }
                }
                
            }
        }
        
        func ThirdPartyLogin(email:String,password:String,firstname:String,lastname:String)
        {
            let b2burl = native.string(forKey: "b2burl")!
            var Pushtoken = ""
            if native.string(forKey: "B2BTokenForSNS") != nil
            {
                Pushtoken = native.string(forKey: "B2BTokenForSNS")!
            }
            let deviceId = UIDevice.current.identifierForVendor?.uuidString
            print(deviceId!)
            var devicename = UIDevice.current.name
            devicename = removeSpecialCharsFromString(text: devicename)
            let deviceinfo:[String:Any] = ["DeviceName": devicename, "DeviceModel": UIDevice.current.modelName, "DeviceVersion": UIDevice.current.systemVersion]
            let parameters: [String:Any] = [ "email":email, "password":password, "first_name":firstname, "last_name":lastname, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_token": Pushtoken]
    //        username = email
    //        self.lastname = lastname
    //        emailid = email
    //        pass = password
            CustomLoader.instance.showLoaderView()
            let header = ["Content-Type": "application/json"]
            print("Successfully post", parameters)
            
            Alamofire.request("\(b2burl)/users/social_signup_data/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    
                    
                    return
                }
                
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let json = response.result.value {
                        print("JSON social: \(json)")
                        var errorval = 0
                        let result = response.result
                        
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            
                            if dict1["status"] as! String  == "SUCCESS" || dict1["status"] as! String  == "success"
                            {   print("success")
                                CustomLoader.instance.hideLoaderView()
                                print("ereff", dict1["response"]  as! String)
                                if dict1["response"]  as! String == "User created successfully"
                                {   self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                if (((dict1["token"]as AnyObject) as? String) != nil)
                                {
                                    //                            self.blurView.alpha = 0
                                    CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                    let dict2 = (dict1["token"] as? String)!
                                    print((dict1["token"] as? String) as Any )
                                    
                                    self.native.set((dict1["token"] as? String)!, forKey: "Token")
                                    let userData = dict1["user_data"] as! NSDictionary
                                    //                            print(userData["firebase_userid"] as? String as Any)
                                    if userData["firebase_userid"] is NSNull
                                    {
                                        errorval = 1
                                    }
                                    else
                                    {
                                        self.native.set(userData["firebase_userid"] as? String, forKey: "firebaseid")
                                        print("firebase", userData["firebase_userid"] as? String)
                                    }
                                    if userData["country_code"] is NSNull
                                    {
                                        self.native.set("", forKey: "country_code")
                                    }
                                    else
                                    {
                                        self.native.set(userData["country_code"], forKey: "country_code")
                                    }
                                    if userData["business_enabled"] is NSNull
                                    {
                                        self.native.set("0", forKey: "business_enabled")
                                    }
                                    else
                                    {
                                        self.native.set(userData["business_enabled"] as? Int, forKey: "business_enabled")
    //                                    print("business_enabled", userData["business_enabled"] as? Int)
                                    }
                                    if userData["account_typeid"]! is NSNull
                                    {
                                        errorval = 1
                                    }
                                    else
                                    {
                                        self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                        
                                        print("account type", (userData["account_typeid"]! as? Int)!)}
                                    if userData["userid"]! is NSNull && userData["shopid"]! is NSNull &&
                                        userData["companyid"]! is NSNull
                                    {
                                        errorval = 1
                                        
                                    }
                                    if errorval == 0
                                    {
                                        print("90909", userData["is_user_updated"] as! Int)
                                        if userData["is_user_updated"] as! Int == 1
                                        {
                                            self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                            self.native.set(userData["userid"]! as? Int, forKey: "userid")
                                            self.native.set(userData["shopid"]! as? String, forKey: "shopid")
                                            if userData["notification_active"] is NSNull
                                            {
                                                self.native.set("0", forKey: "notification")
                                            }
                                            else
                                            {
                                                self.native.set(userData["notification_active"], forKey: "notification")
                                            }
                                            if userData["user_purchase_modeid"] is NSNull
                                            {
                                                self.native.set("1", forKey: "user_purchase_modeid")
                                            }
                                            else
                                            {
                                                self.native.set(userData["user_purchase_modeid"], forKey: "user_purchase_modeid")
                                            }
                                            if userData["phone"] is NSNull
                                                                      {
                                                                      if userData["email"] is NSNull
                                                                      {
                                                                      self.native.set("", forKey: "phone")
                                                                      }
                                                                      else
                                                                      {
                                                                         self.native.set(userData["email"], forKey: "phone")
                                                                      }
                                                                      } else {
                                                                       self.native.set(userData["phone"], forKey: "phone")
                                                                      }
                                            //                            self.native.set(userData["companyid"]! as? String, forKey: "companyid")
                                            if userData["first_name"] is NSNull
                                            {
                                                self.native.set("", forKey: "firstname")
                                            }
                                            else
                                            {
                                                self.native.set(userData["first_name"]! as? String, forKey: "firstname")
                                            }
                                            if userData["last_name"] is NSNull
                                            {
                                                self.native.set(" ", forKey: "lastname")
                                            }
                                            else
                                            {
                                                self.native.set(userData["last_name"]! as? String, forKey: "lastname")
                                            }
                                            if userData["profile_pic_url"] is NSNull
                                            {
                                                self.native.set("", forKey: "profilepic")
                                            }
                                            else
                                            {
                                                self.native.set(userData["profile_pic_url"]! as? String, forKey: "profilepic")
                                            }
                                            self.native.synchronize()
                                            let usertype = self.native.string(forKey: "usertype")!
                                            if usertype == "3" || usertype == "1"
                                            {
                                                self.native.set(1, forKey: "changetype")
                                                print("login by local id........")
                                            }
                                            else
                                            {
                                                self.native.set(2, forKey: "changetype")
                                                self.native.synchronize()
                                            }
                                            if userData["countryid"]! is NSNull
                                            {
                                                self.native.set("0", forKey: "countryid")
                                            }
                                            else
                                            {
                                                self.native.set(userData["countryid"]!, forKey: "countryid")
                                            }
                                            self.native.synchronize()
                                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            appDelegate.window?.rootViewController = redViewController
                                        }
                        else if userData["is_user_updated"] as! Int == 0
                                {
                                            self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                            self.native.set(userData["firebase_userid"] as? String, forKey: "firebaseid")
                                            self.native.set(userData["userid"]! as? String, forKey: "userid")
                                            if userData["notification_active"] is NSNull
                                            {
                                                self.native.set("0", forKey: "notification")
                                            }
                                            else
                                            {
                                                self.native.set(userData["notification_active"], forKey: "notification")
                                            }
                                            if userData["support_firebaseid"] is NSNull
                                            {
                                                self.native.set("", forKey: "support_firebaseid")
                                            }
                                            else
                                            {
                                                self.native.set(userData["support_firebaseid"], forKey: "support_firebaseid")
                                            }
                                            
                                            if userData["support_profile_pic_url"] is NSNull
                                            {
                                                self.native.set("", forKey: "support_profile_pic_url")
                                            }
                                            else
                                            {
                                                self.native.set(userData["support_profile_pic_url"], forKey: "support_profile_pic_url")
                                            }
                                            if userData["support_name"] is NSNull
                                            {
                                                self.native.set("", forKey: "support_name")
                                            }
                                            else
                                            {
                                                self.native.set(userData["support_name"], forKey: "support_name")
                                            }
                                            
                                             if userData["phone"] is NSNull
                                                                      {
                                                                      if userData["email"] is NSNull
                                                                      {
                                                                      self.native.set("", forKey: "phone")
                                                                      }
                                                                      else
                                                                      {
                                                                         self.native.set(userData["email"], forKey: "phone")
                                                                      }
                                                                      } else {
                                                                       self.native.set(userData["phone"], forKey: "phone")
                                                                      }
                                            let usertype = self.native.string(forKey: "usertype")!
                                            if usertype == "3" || usertype == "1"
                                            {
                                                self.native.set(1, forKey: "changetype")
                                                self.native.synchronize()
                                            }
                                            else
                                            {
                                                self.native.set(2, forKey: "changetype")
                                                self.native.synchronize()
                                            }
                                            self.native.synchronize()
                                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            appDelegate.window?.rootViewController = redViewController
                                        }
                                    }
                                        
                                    else
                                    {
                                        
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                        self.present(rootPage, animated: false, completion: nil)
                                        
                                        let innerdict = dict1["user_data"] as! NSDictionary
                                        print(innerdict["userid"]!)
                                        if innerdict["firebaseid"]! != nil
                                        {
                                            print(innerdict["firebaseid"] as? String)
                                            self.native.set(innerdict["firebaseid"] as? String, forKey: "firebaseid")
                                            self.native.set(innerdict["userid"]!, forKey: "userid")
                                        }
                                        self.native.synchronize()
                                        //                        print(innerdict)
                                    }
                                }
                            }
                                
                            else if dict1["status"] as! String != "failure"
                            {
    //                            print("hdiuhjhb", dict1["response"]  as! String)
                                //                        self.blurView.alpha = 0
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                if dict1["response"] as! String == "OTP verification pending"
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                    
                                else if dict1["response"]  as! String == "User created successfully"
                                {   self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else if dict1["response"] as! String == "Contact detail missing."
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else{
                                    
                                    self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .bottom)
                                    
                                }
                            }
                            else if dict1["status"] as! String == "failure"
                            {
                                if dict1["response"] as! String == "OTP verification pending"
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else if dict1["response"]  as! String == "Contact detail missing."
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else
                                {
                                    self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .bottom)
                                }
                            }
                            
                            
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                            
                        }
                    }
                    
                }
            }
        }

}
