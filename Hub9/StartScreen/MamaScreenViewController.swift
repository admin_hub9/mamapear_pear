//
//  MamaScreenViewController.swift
//  Hub9
//
//  Created by Deepak on 05/08/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit


// #MARK: Custom Protocol
    // To send notification on save
    protocol tutorialPageTurnerDelegate {
//        func saveActionCompleted()
    }


class MamaScreenViewController: UIViewController {

    
 var delegate: tutorialPageTurnerDelegate?
    
    var userTypeID = 1
    
    
    @IBOutlet weak var mamToText: UILabel!
    @IBOutlet weak var mamaText: UILabel!
    @IBOutlet weak var Container: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        MamaScreen.delegate = self
        Container.layer.shadowPath = UIBezierPath(rect: Container.bounds).cgPath
        Container.layer.shadowRadius = 0
        Container.layer.shadowOffset = .zero
        Container.layer.shadowOpacity = 1

        // Do any additional setup after loading the view.
    }
    

   
    @IBAction func mamaTO(_ sender: Any) {
        
        self.userTypeID = 1
        mamToText.textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        mamaText.textColor = UIColor.lightGray
    }
    
    
    @IBAction func Mama(_ sender: Any) {
        
        self.userTypeID = 2
        mamaText.textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        mamToText.textColor = UIColor.lightGray
    }
    

}
