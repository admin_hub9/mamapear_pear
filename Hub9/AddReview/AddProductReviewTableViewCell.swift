//
//  AddProductReviewTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 25/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class AddProductReviewTableViewCell: UITableViewCell {

    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var unite: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
