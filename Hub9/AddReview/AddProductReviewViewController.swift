//
//  AddProductReviewViewController.swift
//  Hub9
//
//  Created by Deepak on 25/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire


var productReview = AddProductReviewViewController()

class AddProductReviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var native = UserDefaults.standard
    var order_itemid = 0
    
    
    @IBOutlet weak var productReviewTable: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        productReview = self
        productReviewTable.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        if touch?.view?.tag != 20 {
            print("inside view")
            dismiss(animated: true, completion: nil)
        }
        else
        {
            
            print("outside view")
        }
        
        
    }

    @IBAction func clodeView(_ sender: Any) {
    
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("345345", indexPath.row)
        order_itemid = (orderDetails.orderItem[indexPath.row]["order_itemid"] as? Int)!
        
        self.dismiss(animated: true, completion: nil)
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "OrderRating"))! as UIViewController

        orderDetails.navigationController?.pushViewController(editPage, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addProductReview", for: indexPath) as! AddProductReviewTableViewCell
        if orderDetails.orderItem.count > 0
        {
            if let productimage = orderDetails.orderItem[indexPath.row]["item_image_url"] as? AnyObject
        {
            let imagedata = productimage["200"] as! [AnyObject]
            print("imagedata", imagedata[0])
            if imagedata.count > 0
            {
               var imageUrl = (imagedata[0] as? String)!
               imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
               
               if let url = NSURL(string: imageUrl )
               {
                   if url != nil
                   {
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                   }
               }
            }
            }
            if orderDetails.orderItem[indexPath.row]["quantity"] is NSNull
        {
            cell.unite.text = ""
        }
        else
        {
            cell.unite.text = "X \((orderDetails.orderItem[indexPath.row]["quantity"] as? NSNumber)!)"
            }
        var productColor = ""
        var productSize = ""
            if let color = orderDetails.orderItem[indexPath.row]["color_ui"] as? String
        {
            productColor = color
            }
            if let size = orderDetails.orderItem[indexPath.row]["size_ui"] as? String
            {
                productSize = size
            }
        cell.price.text = "\(productColor)/\(productSize)"
            cell.price.text = "\(orderDetails.currency)\(String(format:"%.2f", (Double((orderDetails.orderItem[indexPath.row]["price"] as? NSNumber)!))))"
            cell.productName.text = "\((orderDetails.orderItem[indexPath.row]["title"] as? String)!)"
        
        }
        
    
        
        return cell
        
    }

   

}
