//
//  AddSellVariantController.swift
//  Hub9
//
//  Created by Deepak on 6/6/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
import iOSDropDown
import Alamofire

class AddSellVariantController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource {
    
    
    
    // to display number of row for each colomn in picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        print(pickerView)
//        return pickerData[component].count
        var row = pickerView.selectedRow(inComponent: 0)
        print("this is the pickerView\(row)")
        
        if component == 0 {
            return 1
        }
            
        else {
            return seconds.count
        }

    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        time1 = pickerData [component][pickerView.selectedRow(inComponent: 0)] + ""
//            time2 = pickerData [component][pickerView.selectedRow(inComponent: 1)] + ""
//        print("time \(time1)", time2)
//        time2 = pickerData [component][pickerView.selectedRow(inComponent: 1)] + ""
//        seconds[pickerView.selectedRow(inComponent: 1)]
        time1 = seconds[pickerView.selectedRow(inComponent: 1)]
        print("value", minutes, seconds[pickerView.selectedRow(inComponent: 1)])
        
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return pickerData[component][row]
        if component == 0 {
            return String(minutes)
        } else {
            
            return String(seconds[row])
        }
    }
    
    var imagePicker: UIImagePickerController!
    var selectedImage = [UIImage]()
    var pickerData = [[String]]()
    var time1 = 0
    var minutes = 1
    var recievedString: String = ""
    var selectedunits = [String]()
    var selctedprice = [Int]()
    var numberOfVariant = 0
    var seconds = Array(1...100)
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedImage.count == 0 {
            return 1
        }
        
        return selectedImage.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addproduct", for: indexPath) as! AddProductCell
        if indexPath.row != 0
        {
            cell.images.layer.cornerRadius = 8
            cell.images.image = selectedImage[indexPath.row - 1]
            cell.images.clipsToBounds = true
            //        cell.images.image = UIImage(named: "car")
            cell.photos.isHidden = true
            cell.camera.isHidden = true
            cell.cancel.isHidden = false
            cell.clickPhotos.isHidden = true
            let button = UIButton(frame: CGRect())
            button.layer.cornerRadius = button.layer.bounds.size.width / 2
            button.layer.masksToBounds = true
            //            cell.cancel.layer.cornerRadius = 0.5 * cancel.bounds.size.width
            cell.cancel.layer.masksToBounds = true
        }else{
            cell.images.layer.cornerRadius = 8
            cell.images.clipsToBounds = true
            cell.photos.isHidden = false
            cell.camera.isHidden = false
            cell.cancel.isHidden = true
            cell.clickPhotos.isHidden = false
            
            
        }
        return cell
    }
    
    func uploadPhotos()
    {
        let accessKey = "AKIAJYCW54FQC6MMHIDQ"
        let secretKey = "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"
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let S3BucketName = "android-image-bucket"
        let remoteName = "test.png"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
        //        let image = UIImage(named: "car")
        let data = UIImageJPEGRepresentation(selectedImage[0], 0.9)
        do {
            try data?.write(to: fileURL)
        }
        catch {}
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/jpeg"
        uploadRequest.acl = .bucketOwnerRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
            DispatchQueue.main.async {
                
            }
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    print("Uploaded to:\(absoluteString)")
                }
            }
            
            return nil
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImage.remove(at: indexPath.row - 1)
        productCollection.reloadData()
    }
    @IBAction func clickPhotos(_ sender: Any) {
        let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert: UIAlertAction) in
            print("Take Photo Code")
            if(UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.allowsEditing = true
                myPickerController.sourceType = .camera
                self.present(myPickerController, animated: true, completion: nil)
            }
            else
            {
                let actionController: UIAlertController = UIAlertController(title: "Camera is not available",message: "", preferredStyle: .alert)
                let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void  in
                    //Just dismiss the action sheet
                }
                actionController.addAction(cancelAction)
                self.present(actionController, animated: true, completion: nil)
            }}
        
        let gallerySelect = UIAlertAction(title: "Choose from Photos", style: .default) { (alert: UIAlertAction) in
            print("Gallery Select Code")
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        //            // Uncomment here and below at actionMenu to add a save photo action to the action list
        
        //            let savePhoto = UIAlertAction(title: "Save Photo", style: .Default, handler: { (alert: UIAlertAction) in
        //                print("Save photo code")
        //            })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
            print("Cancelled")
        }
        
        actionMenu.addAction(takePhoto)
        actionMenu.addAction(gallerySelect)
        //          actionMenu.addAction(savePhoto)
        actionMenu.addAction(cancelAction)
        
        self.present(actionMenu, animated: true, completion: nil)
    }
    //#MARK: Image picker protocol functions
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if info[UIImagePickerControllerOriginalImage] as? UIImage != nil {
            imagePicker.dismiss(animated: true, completion: nil)
            //            profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            print("gallery Images:", info[UIImagePickerControllerOriginalImage] as? UIImage)
            let data = UIImagePNGRepresentation((info[UIImagePickerControllerOriginalImage] as? UIImage)!)
            //            native.set(data, forKey: "DP")
            //            native.synchronize()
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                selectedImage.insert(image, at: 0)
                self.productCollection.reloadData()
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                selectedImage.insert(image, at: 0)
                self.productCollection.reloadData()
            }
            else
            {
                print("Something went wrong")
            }
            
        }
        self.dismiss(animated: true, completion: nil)
        
        
        //        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        //        profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfVariant
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell()
        if tableView == self.variantTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addproductTable", for: indexPath) as! AddProductTableCell
            cell.units.text = selectedunits[indexPath.row]
            cell.cost.text = String(selctedprice[indexPath.row])
            cell.cellView.layer.cornerRadius = cell.cellView.layer.frame.height/2
            cell.cellView.clipsToBounds = true
            cellToReturn = cell
        }
//            else if tableView == self.allVariantTable {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "allVariantCell", for: indexPath) as! AllVariantViewCell
//            cell.VariantProductName.text = selectedunits[indexPath.row]
//            cell.VariantSkuName.text = String(selctedprice[indexPath.row])
//            cell.cellShadwo.layer.cornerRadius = cell.cellShadwo.layer.frame.height/2
//            cell.cellShadwo.clipsToBounds = true
//            cellToReturn = cell
//        }
        
        return cellToReturn
    }
    
    
    @IBAction func cancelVariant(_ sender: Any) {
        
        UIView.transition(with: addVariantContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
//                                self.blueView.alpha = 0
                self.addVariantContainer.transform = CGAffineTransform(translationX: 0, y: 480)
                self.addVariantContainer.isHidden = true
        }, completion: nil)
    }
    @IBAction func addVariant(_ sender: Any) {
        Parentvalidated()
        if (pName.text?.count != 0 && pBrand.text?.count != 0 && pSku.text?.count != 0 && pHSNCode.text?.count != 0 && pSupply.text?.count != 0 && PDescription.text?.count != 0
            && pCategory.text?.count != 0
            && pCategory2.text?.count != 0
            && pCategory3.text?.count != 0)
        {
        UIView.transition(with: addVariantContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
//                                self.blueView.alpha = 0.6
                self.addVariantContainer.transform = CGAffineTransform(translationX: 0, y: -480)
                self.addVariantContainer.isHidden = false
        }, completion: nil)
        }
    }
    
    @IBAction func doneButton(_ sender: Any) {
        
        
        
        if (productName.text?.count != 0 && brandName.text?.count != 0 && sku.text?.count != 0 && hsnCode.text?.count != 0 && suppyAbility.text?.count != 0 && lenght.text?.count != 0 && width.text?.count != 0 && height.text?.count != 0 && weight.text?.count != 0)
        {
        let parameters: [String: Any] = [
            
            "parent_product_data": [
                "name": productName.text,
                "sku": sku.text,
                "brand": brandName.text,
                "sub_categoryid": 2,
                "description": "Awesome Product",
                "dynamic_discount_range" : [["index": 0, "low": 0, "high": 10, "price" : 200],["index": 0, "low": 11, "high" : 21, "price" : 180]],
                "variant_data" : [["name" : "Pyjama", "sku":"PJ-a2-m", "min_order_qty" : "20", "supply_ability_daily" : 10, "description" : "Awesome Product", "stock": 1000, "dimention":["weight": weight.text,"height": height.text, "length" : lenght.text, "breadth" : width.text], "dynamic_discount_range" : [["index":"0", "low": 0, "high" : "10", "price" : "200"], ["index":"0", "low":11, "high" : "21", "price" : "180"]] ]]]]
        let token = "8eac752292f55cf0196927adb7a7cfc7b8ffafdb"
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
        Alamofire.request("http://demo.hub9.io:9001/api/create_listing/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                //            debugPrint(response)
                            print("response: \(response)")
                switch(response.result) {
                    
                case .success(_):
                    if let data = response.result.value{
                        print(response.result.value)
                        self.allVariantTable.reloadData()
                        UIView.transition(with: self.addVariantContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                            {
                                //                                self.blueView.alpha = 0.6
                                self.addVariantContainer.transform = CGAffineTransform(translationX: 0, y: 480)
                                self.addVariantContainer.isHidden = true
                        }, completion: nil)
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error)
                    break
                    
                }
            guard response.data != nil else { // can check byte count instead
                    //                        self.displayAlert("Uh Oh", message: "There seem to be server issues! Please try again later")
//                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
            }
       
        }
    
}
    func validated() -> Bool {
        var valid: Bool = true
        if ((hsnCode.text?.isEmpty) != nil){
            // change placeholder color to red color for textfield email-id
            hsnCode.attributedPlaceholder = NSAttributedString(string: " HSN Code", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if ((sku.text?.isEmpty) != nil){
            // change placeholder color to red for textfield username
            
            sku.attributedPlaceholder = NSAttributedString(string:"SKU",attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if ((productName.text?.isEmpty) != nil){
            // change placeholder color to red for textfield password
            productName.attributedPlaceholder = NSAttributedString(string:"Name",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
            valid = false
        }
        if ((brandName.text?.isEmpty) != nil){
            // change placeholder color to red for textfield mobile no.
            brandName.attributedPlaceholder = NSAttributedString(string:"Brand Name",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
            valid = false
        }
        return valid
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        blueView.alpha = 0
        addVariantButton.layer.cornerRadius = addVariantButton.layer.frame.height/2
        addVariantButton.clipsToBounds = true
        unitContainer.isHidden = true
        addVariantContainer.isHidden = true
        
//        pickerData = [["1"],["1", "2", "3", "4","5"]]
        categories1.optionArray = ["Credit card", "Debit card", "Master card", "Maestro card", "Visa card"]
        //Its Id Values and its optional
        categories1.optionIds = [1,23,54,22]
        categories1.didSelect{(selectedText , index ,id) in
            print( "Selected String: \(selectedText) \n index: \(index)")
        }
        categories2.optionArray = ["Credit card", "Debit card", "Master card", "Maestro card", "Visa card"]
        //Its Id Values and its optional
        categories2.optionIds = [1,23,54,22]
        categories2.didSelect{(selectedText , index ,id) in
            print( "Selected String: \(selectedText) \n index: \(index)")
            self.categories3.select((Any).self)
            
        }
        categories3.optionArray = ["Credit card", "Debit card", "Master card", "Maestro card", "Visa card"]
        //Its Id Values and its optional
        categories3.optionIds = [1,23,54,22]
        categories3.didSelect{(selectedText , index ,id) in
            print( "Selected String: \(selectedText) \n index: \(index)")
        }
        pCategory.optionArray = ["Credit card", "Debit card", "Master card", "Maestro card", "Visa card"]
        //Its Id Values and its optional
        pCategory.optionIds = [1,23,54,22]
        pCategory.didSelect{(selectedText , index ,id) in
            print( "Selected String: \(selectedText) \n index: \(index)")
        }
        pCategory2.optionArray = ["Credit card", "Debit card", "Master card", "Maestro card", "Visa card"]
        //Its Id Values and its optional
        pCategory2.optionIds = [1,23,54,22]
        pCategory2.didSelect{(selectedText , index ,id) in
            print( "Selected String: \(selectedText) \n index: \(index)")
            self.categories3.select((Any).self)
            
        }
        pCategory3.optionArray = ["Credit card", "Debit card", "Master card", "Maestro card", "Visa card"]
        //Its Id Values and its optional
        pCategory3.optionIds = [1,23,54,22]
        pCategory3.didSelect{(selectedText , index ,id) in
            print( "Selected String: \(selectedText) \n index: \(index)")
        }
    }
    
    @IBAction func Cancel(_ sender: Any) {
        unitContainer.isHidden = true
    }
    @IBAction func unitSelected(_ sender: Any) {
        
        unitContainer.isHidden = true
        selectedunits.insert(String(String(minutes) + " - " + String(time1) + " Units"), at: numberOfVariant)
        
        minutes = time1 + 1
        if minutes <= 100
        {
        seconds = Array(minutes...100)
        }
        else
        {
            seconds = Array(minutes...minutes + 100)
            print("Invalid number")
        }
    
        unitPicker.reloadAllComponents()
        
    }


    @IBAction func insertPVariantButton(_ sender: Any) {
        if (pName.text?.count != 0 && pBrand.text?.count != 0 && pSku.text?.count != 0 && pHSNCode.text?.count != 0 && pSupply.text?.count != 0 && PDescription.text?.count != 0
            && pCategory.text?.count != 0
            && pCategory2.text?.count != 0
            && pCategory3.text?.count != 0)
        {
        if(pPrice.text?.count != nil){
            selctedprice.insert(Int(pPrice.text!)!, at: numberOfVariant)
        }
        numberOfVariant = numberOfVariant + 1
        PVariantTable.reloadData()
         pPrice.text = ""
        }
    }
    @IBAction func pUnitsButton(_ sender: Any) {
        unitContainer.isHidden = false
    }
    @IBAction func insertVariant(_ sender: Any) {
        
        if(discount.text?.count != nil){
            selctedprice.insert(Int(discount.text!)!, at: numberOfVariant)
        }
        numberOfVariant = numberOfVariant + 1
        variantTable.reloadData()
    }
    @IBAction func uniteButton(_ sender: Any) {
        unitContainer.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Parentvalidated() -> Bool {
        var valid: Bool = true
        if ((pHSNCode.text?.isEmpty) != nil){
            // change placeholder color to red color for textfield email-id
            pHSNCode.attributedPlaceholder = NSAttributedString(string: " HSN Code", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if ((pSku.text?.isEmpty) != nil){
            // change placeholder color to red for textfield username
            
            pSku.attributedPlaceholder = NSAttributedString(string:"SKU",attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if ((pName.text?.isEmpty) != nil){
            // change placeholder color to red for textfield password
            pName.attributedPlaceholder = NSAttributedString(string:"Name",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
            valid = false
        }
        if ((pBrand.text?.isEmpty) != nil){
            // change placeholder color to red for textfield mobile no.
            pBrand.attributedPlaceholder = NSAttributedString(string:"Brand Name",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
            valid = false
        }
        if ((pSupply.text?.isEmpty) != nil){
            // change placeholder color to red for textfield mobile no.
            pSupply.attributedPlaceholder = NSAttributedString(string:"Supply Ability",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
            valid = false
        }
        if ((PDescription.text?.isEmpty) != nil){
            // change placeholder color to red for textfield mobile no.
            PDescription.attributedPlaceholder = NSAttributedString(string:"Description",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
            valid = false
        }
        return valid
    }
    
    
}


