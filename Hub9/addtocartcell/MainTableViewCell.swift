//
//  MainTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 4/18/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productMoqAlert: UILabel!
    @IBOutlet weak var insideTable: UITableView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension MainTableViewCell
{
    func  setTableViewDataSourceDelegate
        <D: UITableViewDelegate & UITableViewDataSource>
        (_ dataSourceDelegate: D, forRow row: Int){
        insideTable.delegate = dataSourceDelegate
        insideTable.dataSource = dataSourceDelegate
        insideTable.reloadData()
    }
}
