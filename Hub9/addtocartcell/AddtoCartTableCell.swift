//
//  AddtoCartTableCell.swift
//  Hub9
//
//  Created by Deepak on 6/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class AddtoCartTableCell: UITableViewCell {
    
    
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var delete: UIButton!
    @IBOutlet weak var totalUnit: UILabel!
    @IBOutlet var total: UILabel!
    @IBOutlet weak var qtyPlaceholder: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var quantity: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
