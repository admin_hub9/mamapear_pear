//
//  PaymentViewController.swift
//  Hub9
//
//  Created by Deepak on 6/1/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

//import UIKit
//import iOSDropDown
//
//
//class PaymentViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
//
//
//    var count = 0
//    var native = UserDefaults.standard
//    var selected_payment = 1
//    var data = [String:Any]()
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var count1 = 0
//        if count > 0
//        {
//            count1 = 10
//        }
//        else{
//            let noItemLabel = UILabel() //no need to set frame.
//            noItemLabel.textAlignment = .center
//            noItemLabel.textColor = .lightGray
//            noItemLabel.text = "No Address Found"
//            addressTable.backgroundView = noItemLabel
//
//        }
//        return count1
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "addresscell", for: indexPath) as! AddressTableViewCell
//        return cell
//    }
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        print(indexPath.row)
//        if indexPath.row != -1
//        {
//            blurView.alpha = 0
//            addressView.isHidden = true
//        }
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 90
//
//    }
//
//
//
//    @IBOutlet var addressTable: UITableView!
//    @IBOutlet var addressView: UIView!
//    @IBOutlet var blurView: UIVisualEffectView!
//    @IBOutlet var paymentOption: DropDown!
//    @IBOutlet var landmark: UITextField!
//    @IBOutlet var name: UITextField!
//    @IBOutlet var mobile: UITextField!
//    @IBOutlet var phone: UITextField!
//    @IBOutlet var country: UITextField!
//    @IBOutlet var state: UITextField!
//    @IBOutlet var city: UITextField!
//    @IBOutlet var pincode: UITextField!
//    @IBOutlet var address: UITextField!
//
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        name.resignFirstResponder()
//        mobile.resignFirstResponder()
//        phone.resignFirstResponder()
//        country.resignFirstResponder()
//        state.resignFirstResponder()
//        city.resignFirstResponder()
//        pincode.resignFirstResponder()
//        address.resignFirstResponder()
//        landmark.resignFirstResponder()
//
//
//    }
//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= 120
//            }
//        }
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y != 0{
//                self.view.frame.origin.y += 120
//            }
//        }
//    }
//
//
//
//
//
//    //validation
//    // check phone number
//    func validate(pincode: String) -> Bool {
//        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
//        let inputString = pincode.components(separatedBy: charcterSet)
//        let filtered = inputString.joined(separator: "")
//        return  pincode == filtered
//    }
//    // check pincode
//    func isValidInput(Input:String) -> Bool {
//        let RegEx = "\\A\\w{3,18}\\z"
//        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
//        return Test.evaluate(with: Input)
//    }
//    // check string
//    func validateString(pincode: String) -> Bool {
//        let charcterSet  = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
//        let inputString = pincode.components(separatedBy: charcterSet)
//        let filtered = inputString.joined(separator: "")
//        return  pincode == filtered
//    }
//
//    func validation() -> Bool {
//        var valid: Bool = true
//        if (name.text!.count) < 2 || validateString(pincode: name.text!) == false{
//            // change placeholder color to red color for textfield email-id
//            name.text?.removeAll()
//            name.attributedPlaceholder = NSAttributedString(string: " Enter correct name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        if (mobile.text!.count) < 8 || validate(pincode: mobile.text!) == false || (mobile.text!.count) > 14 {
//            // change placeholder color to red color for textfield email-id
//            mobile.text?.removeAll()
//            mobile.attributedPlaceholder = NSAttributedString(string: " Enter Mobile Number", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        if (country.text!.count) < 2 || validateString(pincode: country.text!) == false{
//            // change placeholder color to red color for textfield email-id
//            country.text?.removeAll()
//            country.attributedPlaceholder = NSAttributedString(string: " Enter correct Country name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        if (state.text!.count) < 2 || validateString(pincode: state.text!) == false{
//            // change placeholder color to red color for textfield email-id
//            state.text?.removeAll()
//            state.attributedPlaceholder = NSAttributedString(string: " Enter correct State name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        if (city.text!.count) < 2 || validateString(pincode: city.text!) == false{
//            // change placeholder color to red color for textfield email-id
//            city.text?.removeAll()
//            city.attributedPlaceholder = NSAttributedString(string: " Enter correct City name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        if validate(pincode: pincode.text!) == false || pincode.text?.count != 6{
//            // change placeholder color to red color for textfield email-id
//            pincode.text?.removeAll()
//            pincode.attributedPlaceholder = NSAttributedString(string: " Enter correct Pincode", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        if (phone.text?.count)! > 0
//        {
//        if (phone.text!.count) < 8 || validate(pincode: phone.text!) == false || (phone.text!.count) > 14{
//            // change placeholder color to red color for textfield email-id
//            phone.text?.removeAll()
//            phone.attributedPlaceholder = NSAttributedString(string: " Enter Phone Number", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        }
//        if (landmark.text!.count) < 2 || (landmark.text!.count) > 150 {
//            // change placeholder color to red color for textfield email-id
//            landmark.text?.removeAll()
//            landmark.attributedPlaceholder = NSAttributedString(string: "Please Enter correct Landmark", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        if (address.text!.count) < 2 || (address.text!.count) > 150 {
//            // change placeholder color to red color for textfield email-id
//            address.text?.removeAll()
//            address.attributedPlaceholder = NSAttributedString(string: "Please Enter correct Address", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
//            valid = false
//        }
//        return valid
//    }
//
//
//
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(CompleteProfileController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(CompleteProfileController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//        blurView.alpha = 0
//        addressView.isHidden = true
//        addressTable.allowsMultipleSelection = false
//        var payment_mode = native.object(forKey: "Payment_mode") as? [String] ?? [String]()
//        var payment_lkid = native.object(forKey: "Payment_lkid") as? [Int] ?? [Int]()
//
//            paymentOption.optionArray = payment_mode
//        // The list of array to display. Can be changed dynamically
//
//        //Its Id Values and its optional
//        paymentOption.optionIds = payment_lkid
//        print(paymentOption.optionArray, paymentOption.optionIds)
//        paymentOption.didSelect{(selectedText , index ,id) in
//            print( "Selected String: \(selectedText) \n index: \(index)")
//            self.selected_payment = self.paymentOption.optionIds![index] as! Int
//        }
//
//        // Do any additional setup after loading the view.
//    }
//
//
//
////    @IBAction func completePayment(_ sender: Any) {
////        if validation() == true
////        {
////
////            data = ["contact_number1":mobile.text!, "contact_number2":phone.text!,"country":country.text!,"city":city.text!,"state":state.text!,"pincode":pincode.text!,"address1":landmark.text!,"address2":address.text!,"payment_mode_lkpid":selected_payment,"amount":18000 ,"remarks": ""] as [String : Any]
//////            print(data)
////           let val = ReviewPaymentViewController()
////            val.Payment_data(data: data)
////
////        value1.Review()
////        }
////    }
//    func return_data() ->[String:Any]
//    {
//        return data
//    }
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//}
