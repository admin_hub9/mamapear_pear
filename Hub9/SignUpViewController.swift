//
//  SignUpViewController.swift
//  Hub9
//
//  Created by Deepak on 6/7/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import CountryList
import CoreLocation
import MRCountryPicker



var signUpView = SignUpViewController()
class SignUpViewController: UIViewController, UITextFieldDelegate, CountryListDelegate, UITextViewDelegate, CLLocationManagerDelegate, MRCountryPickerDelegate {
    let nextSelectedButton = UIColor(red: 0/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1.00)
    let native = UserDefaults.standard
    var userCode = 1
    var countryList = CountryList()
    var passcode = 1
    var countrycode = 231
    var countryString = "US"
    var iconClick = false
    var sendfromSignup = false
    var alamoFireManager : SessionManager?
    var socialLogin = false
    var emailsignup = false
    
    //location
    let locationManager = CLLocationManager()
    let geoCoder = CLGeocoder()
    var window: UIWindow?
    
    
    @IBOutlet var phoneNumber: UITextField!
    @IBOutlet var buyerButton: UIButton!
    @IBOutlet var sellerButton: UIButton!
    @IBOutlet var nextbuttonSignup: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var passwordicon: UIButton!
    
    @IBOutlet weak var skipLogin: UIButton!
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var countryPicker: MRCountryPicker!
    
    
    
    ///country code
    func selectedCountry(country: Country) {
        self.countryString = country.countryCode
        
        selectCountryCode.setTitle("\(countryString) + \(country.phoneExtension)", for: .normal)
        selectCountryCode.setTitleColor(UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1), for: .normal)
        passcode = Int(country.phoneExtension)!
        country_lkpid(countryCode: country.name!)
        
    }
    
    
    @IBAction func skipLogin(_ sender: Any) {
            CustomLoader.instance.showLoaderView()
            let userid = native.string(forKey: "userid")!
            native.set(passcode, forKey: "country_code")
            native.set(countrycode, forKey: "countryid")
            native.set(userCode, forKey: "usertype")
            native.synchronize()
            print(userid)
            
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "InstructionScreen"))! as UIViewController
            self.present(rootPage, animated: false, completion: nil)
                
    }
    
    
    @IBAction func termsConsitions(_ sender: Any) {
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "termsAndCondition"))! as UIViewController
        self.present(rootPage, animated: false, completion: nil)
    }
    
    //country lkpid
    func country_lkpid(countryCode: String)
    {
        if let urlPath = Bundle.main.url(forResource: "country", withExtension: "json") {
            
            do {
                let jsonData = try Data(contentsOf: urlPath, options: .mappedIfSafe)
                
                if let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: AnyObject] {
                    
                    if let personArray = jsonDict["response"] as? [[String: AnyObject]] {
//                         print("responce", personArray)
                        for personDict in personArray {
                            
                            if countryCode == personDict["country_name"] as! String
                                {
//                                    print(personDict["country_lkpid"]!)
                                    countrycode = Int((personDict["country_lkpid"] as! NSNumber ).floatValue)
                                    var phonecode = Int(personDict["phone_code"] as! String)
                                    selectCountryCode.setTitle("\(countryString) + \(phonecode!)", for: .normal)
                                    selectCountryCode.setTitleColor(UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1), for: .normal)
                                    passcode = phonecode!
                                    break
                                }
                        }
                    }
                }
            }
                
            catch let jsonError {
                print(jsonError)
            }
        }
    }
    
    @IBOutlet var selectCountryCode: UIButton!
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func selectcode(_ sender: Any) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    
    
    
  /////keybpard hide and show
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        phoneNumber.resignFirstResponder()
        password.resignFirstResponder()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y == 0{
                
                self.view.frame.origin.y -= 90
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y != 0{
                
                self.view.frame.origin.y += 90
            }

        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        print(textField.text)
        
        if (textField.text?.count)! > 0
        {
         if  validate(phoneNumber:  textField.text!) == true
        {
            self.emailsignup = false
            selectCountryCode.isHidden = false
            let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: selectCountryCode.frame.width + 5, height: 26))
            leftView.backgroundColor = .clear
            
            phoneNumber.leftView = leftView
            phoneNumber.leftViewMode = .always
            var frame: CGRect = phoneNumber.frame
            frame.size.height = 40
            phoneNumber.frame = frame
            phoneNumber.layer.masksToBounds = true
            
            
        }
        else
        {
            self.emailsignup = true
            selectCountryCode.isHidden = true
            let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
            leftView.backgroundColor = .clear
            
            phoneNumber.leftView = leftView
            phoneNumber.leftViewMode = .always
            var frame: CGRect = phoneNumber.frame
            frame.size.height = 40
            phoneNumber.frame = frame
            phoneNumber.layer.masksToBounds = true
        }
        }
        else
        {
            self.emailsignup = true
            selectCountryCode.isHidden = true
            let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
            leftView.backgroundColor = .clear
            
            phoneNumber.leftView = leftView
            phoneNumber.leftViewMode = .always
            var frame: CGRect = phoneNumber.frame
            frame.size.height = 40
            phoneNumber.frame = frame
            phoneNumber.layer.masksToBounds = true
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = CharacterSet.whitespaces
        if let _ = string.rangeOfCharacter(from: whitespaceSet) {
            return false
        } else {
            return true
        }
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        password.text = password.text?.replacingOccurrences(of: " ", with: "")
//            return true
//
//    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.first else { return }

        geoCoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
            guard let currentLocPlacemark = placemarks?.first else { return }
            print(currentLocPlacemark.country ?? "No country found")
            print(currentLocPlacemark.isoCountryCode ?? "No country code found")
            
            if currentLocPlacemark.country! != nil
            {
            self.country_lkpid(countryCode: currentLocPlacemark.country!)
            }
        }
        
    }
    
    // a picker item was selected
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
//        blurView.alpha = 0.0
//        countryPicker.isHidden=true
        print("country details", name, countryCode, phoneCode, flag)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        blurView.alpha = 0.0
        countryPicker.isHidden=true
        // set country by its code
        countryPicker.setCountry("SI")
        // optionally set custom locale; defaults to system's locale
        countryPicker.setLocale("sl_SI")
        // set country by its name
        countryPicker.setCountryByName("Canada")
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.startMonitoringSignificantLocationChanges()
        }
        if (loginView.socialLogin == true) || ( root.socialLogin == true)
        {
            self.skipLogin.isHidden=false
        }
        else
        {
            self.skipLogin.isHidden=true
        }
        
        if loginView.sentFromLogin == true || root.sentFromLogin == true
        {
            password.isHidden = true
            passwordicon.isHidden = true
        }
        else
        {
            password.isHidden = false
            passwordicon.isHidden = false
        }
        selectCountryCode.setTitle("+ \(passcode)", for: .normal)
        selectCountryCode.layer.cornerRadius = selectCountryCode.layer.frame.size.height/2
        selectCountryCode.layer.borderWidth = 1
        selectCountryCode.layer.borderColor = UIColor.gray.cgColor
        selectCountryCode.clipsToBounds = true
//        selectCountryCode.isHidden = true
        phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        countryList.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        phoneNumber.delegate = self
        BuyerButton((Any).self)
        
        ///next button
        nextbuttonSignup.layer.cornerRadius = nextbuttonSignup.layer.frame.size.height/2
        nextbuttonSignup.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 239/255.0, green: 130/255.0, blue: 136/255.0, alpha: 1.00).cgColor
       
//        nextbuttonSignup.applyendGradient(colors: [loginstartColor, loginendColor])
        ///textfields
        let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
        leftView.backgroundColor = .clear
        
        phoneNumber.leftView = leftView
        phoneNumber.leftViewMode = .always
        var frame: CGRect = phoneNumber.frame
        frame.size.height = 40
        phoneNumber.frame = frame
        phoneNumber.layer.cornerRadius = phoneNumber.layer.frame.height/2
        phoneNumber.layer.borderWidth = 1
        phoneNumber.layer.borderColor = UIColor.lightGray.cgColor
        phoneNumber.placeholder = "Enter Email or Phone Number"
        phoneNumber.layer.masksToBounds = true
        // password
        let passwordleftView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 26))
        passwordleftView.backgroundColor = .clear
        let passwordrightView = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 26))
        passwordrightView.backgroundColor = .clear
        password.rightView = passwordrightView
        password.rightViewMode = .always
        password.leftView = passwordleftView
        password.leftViewMode = .always
        var passwordframe: CGRect = password.frame
        passwordframe.size.height = 40
        password.frame = passwordframe
        password.layer.cornerRadius = password.layer.frame.height/2
        password.layer.borderWidth = 1
        password.layer.borderColor = UIColor.lightGray.cgColor
        password.layer.masksToBounds = true
        self.emailsignup = true
        self.selectCountryCode.isHidden = true
     }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /// change password visibility
    @IBAction func showandhidePassword(_ sender: Any) {
        if(iconClick == true) {
            password.isSecureTextEntry = false
            passwordicon.setImage(UIImage(named: "showpassword"), for: .normal)
        }
        else
        {
            password.isSecureTextEntry = true
            passwordicon.setImage(UIImage(named: "hidepassword"), for: .normal)
        }
        
        let tempString = password.text
        password.text = nil
        password.text = tempString
        iconClick = !iconClick
    }
    
    
    // account type buyer
    @IBAction func BuyerButton(_ sender: Any) {
        userCode = 1
        self.native.set(1, forKey: "userCode")
        self.native.synchronize()
        buyerButton.layer.cornerRadius = buyerButton.layer.frame.size.height/2
        buyerButton.layer.masksToBounds = true
        buyerButton.layer.borderColor = nextSelectedButton.cgColor
        buyerButton.layer.borderWidth = 1.5
        buyerButton.setTitleColor(nextSelectedButton, for: .normal)
        sellerButton.layer.cornerRadius = sellerButton.layer.frame.size.height/2
        sellerButton.layer.masksToBounds = true
        sellerButton.layer.borderColor = UIColor.lightGray.cgColor
        sellerButton.layer.borderWidth = 1
        sellerButton.setTitleColor(UIColor.gray, for: .normal)
        //
        
        
    }
    //account type seller
    @IBAction func sellerButoon(_ sender: Any) {
        userCode = 2
        self.native.set(2, forKey: "userCode")
        self.native.synchronize()
        sellerButton.layer.cornerRadius = buyerButton.layer.frame.size.height/2
        sellerButton.layer.masksToBounds = true
        sellerButton.layer.borderColor = nextSelectedButton.cgColor
        sellerButton.layer.borderWidth = 1.5
        sellerButton.setTitleColor(nextSelectedButton, for: .normal)
        
        //
        buyerButton.layer.cornerRadius = sellerButton.layer.frame.size.height/2
        buyerButton.layer.masksToBounds = true
        buyerButton.layer.borderColor = UIColor.lightGray.cgColor
        buyerButton.layer.borderWidth = 1
        buyerButton.setTitleColor(UIColor.gray, for: .normal)
    }
    func validate(phoneNumber: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phoneNumber == filtered
    }
    // validation
    func validation() -> Bool {
        var valid: Bool = true
        if ((phoneNumber.text?.count)! < 10) || ((phoneNumber.text?.count)! > 13) && self.emailsignup == false {
            // change placeholder color to red color for textfield email-id
            phoneNumber.text?.removeAll()
            phoneNumber.attributedPlaceholder = NSAttributedString(string: "invalid phone number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if self.emailsignup == true && ((phoneNumber.text?.count)! < 6)  {
            // change placeholder color to red color for textfield email-id
            phoneNumber.text?.removeAll()
            phoneNumber.attributedPlaceholder = NSAttributedString(string: "invalid username", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if validate(phoneNumber: phoneNumber.text!) == false && self.emailsignup == false
        {
            phoneNumber.text?.removeAll()
            phoneNumber.attributedPlaceholder = NSAttributedString(string: "invalid number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        return valid
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    //send otp fro user
    func sendUserType()
        {
    //        CustomLoader.instance.showLoaderView()
            native.set(userCode, forKey: "changetype")
            let userid = native.string(forKey: "userid")!
            native.set(phoneNumber.text!, forKey: "username")
             native.set(phoneNumber.text!, forKey: "phone")
            native.set(password.text!, forKey: "password")
            native.set(passcode, forKey: "country_code")
            native.set(countrycode, forKey: "countryid")
            native.set(userCode, forKey: "usertype")
            native.set(passcode, forKey: "passcode")
            native.synchronize()
            let b2burl = native.string(forKey: "b2burl")!
            let parameters: [String:Any]
                
            if self.emailsignup == true
            {
                self.native.set("Email", forKey: "lgoinType")
                self.native.synchronize()
                parameters = ["username": phoneNumber.text!,"country_code":String(passcode), "account_typeid":userCode,"userid": userid, "countryid": countrycode, "signin_type": "Email"]
            }
            else
            {
                self.native.set("Phone", forKey: "lgoinType")
                self.native.synchronize()
               parameters = ["phone": phoneNumber.text!, "country_code":String(passcode), "account_typeid":userCode,"userid": userid, "countryid": countrycode, "signin_type": "Phone"]
            }
            let header = ["Content-Type": "application/json"]
            print("Successfully post", parameters)
            
            Alamofire.request("\(b2burl)/users/sendOtpVerification/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                print(response)
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let result = response.result
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        CustomLoader.instance.hideLoaderView()
                         UIApplication.shared.endIgnoringInteractionEvents()
                        switch response.result
                        {
                           
                        case .failure(let error):
                            //                print(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                print("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                                print(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                print("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                        else if (dict1["status"]as AnyObject) as! String  == "success"
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                            self.native.set(1, forKey: "otp_res")
                            self.native.synchronize()
                            self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "otpController"))! as UIViewController
                                self.present(editPage, animated: false, completion: nil)
                            
                            
                        }
                        else
                        {
                            
                            if dict1["response"] as! String == "OTP verification pending" &&
                                dict1["status"] as! String == "failure"
                            {
                                self.native.set(dict1["userid"], forKey: "userid")
                                self.native.synchronize()
                                self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                                    let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "otpController"))! as UIViewController
                                    self.present(editPage, animated: false, completion: nil)
                            }
                            else
                            {
                                
                                self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .top)
                            }
                            
                        }
                        }}
                    }
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                
                
            }
        }
        }
    
    func displayErrorAndLogOut() {
        
        
        if Util.shared.checkNetworkTapped() {
            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
            
        } else {
            self.view.makeToast("You have lost connectivity to the internet. Please self.check your connection and log in again.", duration: 2.0, position: .top)
            
        }
    }
    
    
    // submit userdata
    @IBAction func nextButtonAction(_ sender: Any) {
        if validation() == true
        {
            if loginView.sentFromLogin == true || root.sentFromLogin == true
            {
                sendUserType()
            }
            
                CustomLoader.instance.showLoaderView()
                
                let b2burl = native.string(forKey: "b2burl")!
                let parameters: [String:Any]
                    
                    if self.emailsignup == true
                    {
                       parameters = ["username": phoneNumber.text!,"country_code":passcode, "password":password.text!, "countryid": countrycode, "signup_type":"Email"]
                    }
                else
                    {
                    parameters = ["phone": phoneNumber.text!,"country_code":passcode, "password":password.text!, "countryid": countrycode, "signup_type":"Phone"]
                }
                    
               
                
                native.set(passcode, forKey: "country_code")
                native.set(countrycode, forKey: "countryid")
                native.set(userCode, forKey: "usertype")
                native.synchronize()
                let header = ["Content-Type": "application/json"]
                print("Successfully post", parameters)
                self.socialLogin=false
                Alamofire.request("\(b2burl)/users/signup_user/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        self.displayErrorAndLogOut()
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    switch response.result
                    {
                    case .failure(let error):
            
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                        }
                    case .success:
                    if let json = response.result.value {
                        print("JSON: \(json)")
                        print("In 1 \(response.result)")
                        
                        let jsonString = "\(json)"
                        let result = response.result
                        
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            
                            switch response.result
                            {
                            case .failure(let error):
                              print("In 2 \(response.result)")
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let err = error as? URLError, err.code == .notConnectedToInternet {
                                    // Your device does not have internet connection!
                                    print("Your device does not have internet connection!")
                                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                                    print(err)
                                }
                                else if error._code == NSURLErrorTimedOut {
                                    print("Request timeout!")
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                                }else {
                                    // other failures
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                                }
                            case .success:
                                print("In 3 \(response.result)")
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                    if let invalidToken = dict1["detail"]{
                                        if invalidToken as! String  == "Invalid token."
                                        { // Create the alert controller
                                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            appDelegate.window?.rootViewController = redViewController
                                        }
                                    }
                               else if let val = dict1["isOtpVerified"]
                                    {
                                        if (dict1["isOtpVerified"] as AnyObject) as! Int == 0 && (dict1["status"]as AnyObject) as! String  == "failure"
                                    {
                                        self.native.set(dict1["userid"], forKey: "userid")
                                        self.native.synchronize()
                                        self.sendUserType()
                                    }
                                    }
                            else if let val = dict1["isOtpVerified"]
                            {
                                if (dict1["isOtpVerified"] as AnyObject) as! Int == 1 && (dict1["status"]as AnyObject) as! String  == "failure"
                            {
                                self.native.set(dict1["userid"], forKey: "userid")
                                self.native.synchronize()
                                self.loginUser()
                            }
                            }
                            else if (dict1["status"]as AnyObject) as! String  == "success" && (dict1["status"]as AnyObject) as! String  == "User created successfully"
                            {
                                
                                self.native.set(dict1["userid"], forKey: "userid")
                                self.native.synchronize()
                                self.sendUserType()
                            }
                                       
                            else if dict1["response"] as! String == "OTP verification pending" &&
                                    dict1["status"] as! String == "failure"
                                {
                                    self.native.set(dict1["userid"], forKey: "userid")
                                    self.native.synchronize()
                                    self.sendUserType()
                                }
                                
                               else if dict1["response"] as! String == "User already exist" &&
                                    dict1["status"] as! String == "failure"
                                {
                                    self.loginUser()
                                }
                                else if ((dict1["response"] as! String == "Incorrect password") || (dict1["response"] as! String == "Phone number already exist")) &&
                                    dict1["status"] as! String == "failure"
                                {
                                    
                                    self.view.makeToast("Phone Number already exist", duration: 2.0, position: .top)
                                }
                                else if (dict1["status"]as AnyObject) as! String  == "success"
                                {
                                    self.native.set(1, forKey: "otp_res")
                                    self.native.set(dict1["userid"], forKey: "userid")
                                    self.native.synchronize()
                                    self.sendUserType()
                                }
                                else
                                {
                                
                                self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                    
                                    }}
                        }
                        return
                        
    //                }
    //
    //                }
                }
                }
                }
            }}
        }
    //// skip otp when socail signup hit
    func ThirdPartyLogin(email:String,password:String,firstname:String,lastname:String)
        {
            let b2burl = native.string(forKey: "b2burl")!
            var Pushtoken = ""
            if native.string(forKey: "B2BTokenForSNS") != nil
            {
                Pushtoken = native.string(forKey: "B2BTokenForSNS")!
            }
            let deviceId = UIDevice.current.identifierForVendor?.uuidString
            print(deviceId!)
            var devicename = UIDevice.current.name
            devicename = removeSpecialCharsFromString(text: devicename)
            let deviceinfo:[String:Any] = ["DeviceName": devicename, "DeviceModel": UIDevice.current.modelName, "DeviceVersion": UIDevice.current.systemVersion]
            let parameters: [String:Any] = [ "email":email, "password":password, "first_name":firstname, "last_name":lastname, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_token": Pushtoken]
    //        username = email
    //        self.lastname = lastname
    //        emailid = email
    //        pass = password
            CustomLoader.instance.showLoaderView()
            let header = ["Content-Type": "application/json"]
            print("Successfully post", parameters)
            
            Alamofire.request("\(b2burl)/users/social_signup_data/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                    
                    
                    return
                }
                
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                    }
                    
                case .success:
                    if let json = response.result.value {
                        print("JSON social: \(json)")
                        var errorval = 0
                        let result = response.result
                        
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            
                            if dict1["status"] as! String  == "SUCCESS" || dict1["status"] as! String  == "success"
                            {   print("success")
                                CustomLoader.instance.hideLoaderView()
                                print("ereff", dict1["response"]  as! String)
                                if dict1["response"]  as! String == "User created successfully"
                                {   self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                if (((dict1["token"]as AnyObject) as? String) != nil)
                                {
                                    //                            self.blurView.alpha = 0
                                    CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                    let dict2 = (dict1["token"] as? String)!
                                    print((dict1["token"] as? String) as Any )
                                    
                                    self.native.set((dict1["token"] as? String)!, forKey: "Token")
                                    let userData = dict1["user_data"] as! NSDictionary
                                    //                            print(userData["firebase_userid"] as? String as Any)
                                    if userData["firebase_userid"] is NSNull
                                    {
                                        errorval = 1
                                    }
                                    else if let firebaseid = userData["firebase_userid"] as? String
                                    {
                                        self.native.set(firebaseid, forKey: "firebaseid")
                                        
                                    }
                                    if userData["userid"] is NSNull
                                    {
                                        self.native.set("0", forKey: "userid")
                                    }
                                    else if let id = userData["userid"]! as? Int
                                    {
                                    self.native.set(id, forKey: "userid")
                                    }
                                    if userData["country_code"] is NSNull
                                    {
                                        self.native.set("", forKey: "country_code")
                                    }
                                    else
                                    {
                                        self.native.set(userData["country_code"], forKey: "country_code")
                                    }
                                    if userData["business_enabled"] is NSNull
                                    {
                                        self.native.set("0", forKey: "business_enabled")
                                    }
                                    else
                                    {
                                        self.native.set(userData["business_enabled"] as? Int, forKey: "business_enabled")
                                        print("business_enabled", userData["business_enabled"] as? Int)
                                    }
                                    if userData["account_typeid"]! is NSNull
                                    {
                                        errorval = 1
                                    }
                                    else
                                    {
                                    self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                        
                                       
                                        
                                    }
                                    if userData["userid"]! is NSNull && userData["shopid"]! is NSNull &&
                                        userData["companyid"]! is NSNull
                                    {
                                        errorval = 1
                                        
                                    }
                                    if errorval == 0
                                    {
                                        print("90909", userData["is_user_updated"] as! Int)
                                        if userData["is_user_updated"] as! Int == 0 && userData["is_shop_address"] as! Int == 0
                                        {
                                            self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                            self.native.set(userData["userid"]! as? Int, forKey: "userid")
                                            self.native.set(userData["shopid"]! as? String, forKey: "shopid")
                                            if userData["notification_active"] is NSNull
                                            {
                                                self.native.set("0", forKey: "notification")
                                            }
                                            else
                                            {
                                                self.native.set(userData["notification_active"], forKey: "notification")
                                            }
                                            if userData["user_purchase_modeid"] is NSNull
                                            {
                                                self.native.set("1", forKey: "user_purchase_modeid")
                                            }
                                            else
                                            {
                                                self.native.set(userData["user_purchase_modeid"], forKey: "user_purchase_modeid")
                                            }
                                            if userData["phone"] is NSNull
                                            {
                                            self.native.set("", forKey: "phone")
                                            }
                                            else
                                            {
                                            self.native.set(userData["phone"], forKey: "phone")
                                            }
                                            //                            self.native.set(userData["companyid"]! as? String, forKey: "companyid")
                                           if userData["first_name"] is NSNull
                                           {
                                            self.native.set("", forKey: "firstname")
                                           }
                                            else
                                           {
                                            self.native.set(userData["first_name"]! as? String, forKey: "firstname")
                                            }
                                            if let lastname = userData["last_name"]
                                            {
                                                self.native.set(userData["last_name"]! as? String, forKey: "lastname")
                                            }
                                            if userData["profile_pic_url"] is NSNull
                                            {
                                                self.native.set("", forKey: "profilepic")
                                            }
                                            else
                                            {
                                                self.native.set(userData["profile_pic_url"]! as? String, forKey: "profilepic")
                                            }
                                            let usertype = self.native.string(forKey: "usertype")!
                                            if usertype == "3" || usertype == "1"
                                            {
                                                self.native.set(1, forKey: "changetype")
                                                print("login by local id........")
                                            }
                                            else
                                            {
                                                self.native.set(2, forKey: "changetype")
                                                self.native.synchronize()
                                            }
                                            if userData["countryid"]! is NSNull
                                            {
                                                self.native.set("0", forKey: "countryid")
                                            }
                                            else
                                            {
                                                self.native.set(userData["countryid"]!, forKey: "countryid")
                                            }
                                            self.native.synchronize()
                                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "userIntent") as! UIViewController
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            appDelegate.window?.rootViewController = redViewController
                                        }
                                        else
                                        {
                                            self.native.set((userData["account_typeid"]!), forKey: "usertype")
                                            self.native.set(userData["firebase_userid"] as? String, forKey: "firebaseid")
                                            self.native.set(userData["userid"]! as? String, forKey: "userid")
                                            if userData["notification_active"] is NSNull
                                            {
                                                self.native.set("0", forKey: "notification")
                                            }
                                            else
                                            {
                                                self.native.set(userData["notification_active"], forKey: "notification")
                                            }
                                            if userData["support_firebaseid"] is NSNull
                                            {
                                                self.native.set("", forKey: "support_firebaseid")
                                            }
                                            else
                                            {
                                                self.native.set(userData["support_firebaseid"], forKey: "support_firebaseid")
                                            }
                                            
                                            if userData["support_profile_pic_url"] is NSNull
                                            {
                                                self.native.set("", forKey: "support_profile_pic_url")
                                            }
                                            else
                                            {
                                                self.native.set(userData["support_profile_pic_url"], forKey: "support_profile_pic_url")
                                            }
                                            if userData["support_name"] is NSNull
                                            {
                                                self.native.set("", forKey: "support_name")
                                            }
                                            else
                                            {
                                                self.native.set(userData["support_name"], forKey: "support_name")
                                            }
                                            
                                            if userData["phone"] is NSNull
                                            {
                                            self.native.set("", forKey: "phone")
                                            }
                                            else
                                            {
                                            self.native.set(userData["phone"], forKey: "phone")
                                            }
                                            let usertype = self.native.string(forKey: "usertype")!
                                            if usertype == "3" || usertype == "1"
                                            {
                                                self.native.set(1, forKey: "changetype")
                                                self.native.synchronize()
                                            }
                                            else
                                            {
                                                self.native.set(2, forKey: "changetype")
                                                self.native.synchronize()
                                            }
                                            self.native.synchronize()
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let connectPage : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "userIntent") as UIViewController
                                            self.window = UIWindow(frame: UIScreen.main.bounds)
                                            self.window?.rootViewController = connectPage
                                            self.window?.makeKeyAndVisible()
                                        }
                                    }
                                        
                                    else
                                    {
                                        
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                        self.present(rootPage, animated: false, completion: nil)
                                        
                                        let innerdict = dict1["user_data"] as! NSDictionary
                                        
                                        if innerdict["firebaseid"]! != nil
                                        {
                                             self.native.set(innerdict["firebaseid"] as? String, forKey: "firebaseid")
                                            self.native.set(innerdict["userid"]!, forKey: "userid")
                                        }
                                        self.native.synchronize()
                                        //                        print(innerdict)
                                    }
                                }
                            }
                                
                            else if dict1["status"] as! String != "failure"
                            {
                                print("hdiuhjhb", dict1["response"]  as! String)
                                //                        self.blurView.alpha = 0
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                if dict1["response"] as! String == "OTP verification pending"
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                    
                                else if dict1["response"]  as! String == "User created successfully"
                                {   self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else if dict1["response"] as! String == "Contact detail missing."
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else{
                                    
                                    self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                                    
                                }
                            }
                            else if dict1["status"] as! String == "failure"
                            {
                                if dict1["response"] as! String == "OTP verification pending"
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else if dict1["response"]  as! String == "Contact detail missing."
                                {
                                    self.native.set(dict1["userid"]! as? Int, forKey: "userid")
                                    self.native.synchronize()
                                    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                }
                                else
                                {
                                    self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .top)
                                }
                            }
                            
                            
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                            
                        }
                    }
                    
                }
            }
        }
    
    
   /// direct login if user exist already
    
    func loginUser()
    {   if (phoneNumber.text?.count)! > 0 && (password.text?.count)! > 0
    {
        var Pushtoken = ""
        if native.string(forKey: "B2BTokenForSNS") != nil
        {
            Pushtoken = native.string(forKey: "B2BTokenForSNS")!
        }
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        print(deviceId!)
        //    let country_code = native.string(forKey: "country_code")!
        var devicename = UIDevice.current.name
        devicename = removeSpecialCharsFromString(text: devicename)
        let deviceinfo:[String:Any] = ["DeviceName": devicename, "DeviceModel": UIDevice.current.modelName, "DeviceVersion": UIDevice.current.systemVersion]
        let parameters: [String:Any]
            
        if self.emailsignup == true
        {
            parameters = [ "username":phoneNumber.text!, "password":password.text!, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_notification_token": Pushtoken, "country_code": passcode, "signin_type": "Email"]
        }
        else
        {
            parameters = [ "phone":phoneNumber.text!, "password":password.text!, "device_info":deviceinfo , "deviceid":deviceId!, "device_channelid": "1", "device_notification_token": Pushtoken, "country_code": passcode, "signin_type": "Phone"]
        }
        native.set(countrycode, forKey: "countryid")
        native.synchronize()
        let header = ["Content-Type": "application/json"]
        print("Successfully post---", parameters)
        //
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        //
        let b2burl = native.string(forKey: "b2burl")!
        alamoFireManager!.request("\(b2burl)/users/login_user/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print(response)
            guard response.data != nil else { // can check byte count instead
                
                self.view.makeToast("There seem to be server issues! Please try again later", duration: 2.0, position: .top)
                
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            switch (response.result) {
                
            case .success:
                //Success....
                
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .top)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                    else if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        //                print(dict1["token"]!)
                        if (dict1["status"]as AnyObject) as! String  == "SUCCESS" || (dict1["status"]as AnyObject) as! String  == "success"
                        {   print("success")
                            //                        self.blurView.alpha = 0
                            CustomLoader.instance.hideLoaderView()
                            
                            
                            self.native.set(dict1["token"], forKey: "Token")
                            self.native.synchronize()
                            let innerdict = dict1["user_data"] as AnyObject
                            if innerdict["account_typeid"] is NSNull
                            {}
                            else
                            {
                            self.native.set((innerdict["account_typeid"]!), forKey: "usertype")
                            }
                            if innerdict["country_code"] is NSNull
                            {
                                self.native.set("", forKey: "country_code")
                            }
                            else
                            {
                                self.native.set(innerdict["country_code"], forKey: "country_code")
                            }
                            self.native.set(innerdict["firebase_userid"]! as? String, forKey: "firebaseid")
                            self.native.set(innerdict["userid"]!, forKey: "userid")
                            if innerdict["notification_active"] is NSNull
                            {
                                self.native.set("0", forKey: "notification")
                            }
                            else
                            {
                                self.native.set(innerdict["notification_active"], forKey: "notification")
                            }
                            if innerdict["business_enabled"] is NSNull
                            {
                                self.native.set("0", forKey: "business_enabled")
                            }
                            else
                            {
                                self.native.set(innerdict["business_enabled"] as? Int, forKey: "business_enabled")
                                
                            }
                            if innerdict["user_purchase_modeid"] is NSNull
                            {
                                self.native.set("1", forKey: "user_purchase_modeid")
                            }
                            else
                            {
                                self.native.set(innerdict["user_purchase_modeid"], forKey: "user_purchase_modeid")
                            }
                            
                            if innerdict["phone"] is NSNull
                           {
                           if innerdict["email"] is NSNull
                           {
                           self.native.set("", forKey: "phone")
                           }
                           else
                           {
                              self.native.set(innerdict["email"], forKey: "phone")
                           }
                           }
                           else
                           {
                              self.native.set(innerdict["phone"], forKey: "phone")
                           }
                            
                            self.native.set(innerdict["countryid"]!, forKey: "countryid")
                            if innerdict["support_firebaseid"] is NSNull
                            {
                                self.native.set("", forKey: "support_firebaseid")
                            }
                            else
                            {
                                self.native.set(innerdict["support_firebaseid"], forKey: "support_firebaseid")
                            }
                            
                            if innerdict["support_profile_pic_url"] is NSNull
                            {
                                self.native.set("", forKey: "support_profile_pic_url")
                            }
                            else
                            {
                                self.native.set(innerdict["support_profile_pic_url"], forKey: "support_profile_pic_url")
                            }
                            if innerdict["support_name"] is NSNull
                            {
                                self.native.set("", forKey: "support_name")
                            }
                            else
                            {
                                self.native.set(innerdict["support_name"], forKey: "support_name")
                            }
                            if innerdict["first_name"] is NSNull
                            {
                                self.native.set("", forKey: "firstname")
                            }
                            else
                            {
                                self.native.set(innerdict["first_name"]! as? String, forKey: "firstname")
                            }
                            if innerdict["last_name"] is NSNull
                            {
                                self.native.set(" ", forKey: "lastname")
                            }
                            else
                            {
                                self.native.set(innerdict["last_name"]! as? String, forKey: "lastname")
                            }
                            if innerdict["is_user_address"] is NSNull
                            {}
                            else
                            {
                                if (innerdict["is_user_address"] as? Int)! == 0
                                {
                                    self.native.set("false", forKey: "useraddress")
                                }
                                else
                                {
                                    self.native.set("true", forKey: "useraddress")
                                }
                            }
                            if innerdict["profile_pic_url"] is NSNull
                            {
                                self.native.set("", forKey: "profilepic")
                            }
                            else
                            {
                                self.native.set(innerdict["profile_pic_url"]! as? String, forKey: "profilepic")
                            }
                            self.native.synchronize()
                            print(innerdict)
                            print("completeprofile",innerdict["is_user_updated"] as! Int )
                            //                        if innerdict["is_user_updated"] as! Int == 1
                            //                        {
                            let usertype = self.native.string(forKey: "usertype")!
                            if usertype == "3" || usertype == "1"
                            {
                                self.native.set(1, forKey: "changetype")
                                self.native.synchronize()
                            }
                            else
                            {
                                self.native.set(2, forKey: "changetype")
                                self.native.synchronize()
                            }
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                            //                        }
                            //                        else
                            //                        {
                            //
                            //                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "completeprofile"))! as UIViewController
                            //                            self.present(rootPage, animated: false, completion: nil)
                            //                        }
                        }
                        else
                        {
                            //                        self.blurView.alpha = 0
                            CustomLoader.instance.hideLoaderView()
                            print(dict1["status"] as AnyObject)
                            print("responce",dict1["response"] as AnyObject)
                            let userdata = dict1["userid"] as AnyObject
                            print(userdata)
                            if (dict1["response"] as AnyObject) as! String == "OTP verification pending"
                            {   self.native.set(dict1["userid"]! as AnyObject, forKey: "userid")
                                self.native.synchronize()
                                
                                let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                                self.present(rootPage, animated: false, completion: nil)
                            }
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .top)
                            
                            }}
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                        
                        
                        
                    }
                }
                print("Success")
                
                break
            case .failure(let error):
                //                print(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .top)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .top)
                }
                
                break
            }
            
            
            
        }
    }
    else
    {
        CustomLoader.instance.hideLoaderView()
         self.view.makeToast("Invalid Email ID or Password", duration: 2.0, position: .top)
       
        
        }
        CustomLoader.instance.hideLoaderView()
    }
    
    //remove special character from string
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
}


