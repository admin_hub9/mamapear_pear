//
//  orderDetailViewController.swift
//  Hub9
//
//  Created by Deepak on 12/18/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

//
//  OrderDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire


class OrderDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var orderid = "5657575675523"
    
    
    
    @IBOutlet weak var orderStatusView: UIView!
    @IBOutlet weak var orderdetailsTable: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedCell:UITableViewCell = orderdetailsTable.cellForRow(at: indexPath)!
        selectedCell.selectionStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell()
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsstatus", for: indexPath) as! orderDetailsStatusTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsshippingurl", for: indexPath) as! orderDetailsShiipingUrlTableViewCell
            
            cellToReturn = cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderDetailsShipping", for: indexPath) as! orderDetailsShippingTableViewCell
            cell.shippingAddress.text = "G1- 4rth floor, kirti nagar, new delhi"
            cellToReturn = cell
        }
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsfirstempty", for: indexPath) as! orderDetailsFirstEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsshopname", for: indexPath) as! orderDetailsShopNameTableViewCell
            cell.shopname.text = "Masha"
            cellToReturn = cell
        }
        else if indexPath.row == 5
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsproductdetails", for: indexPath) as! orderDetailsProductDetailsTableViewCell
            cell.productPrice.text = "$ 242.0"
            cell.actualPrice.text = "$ 243.4"
            cell.priceDetails.text = "$ 242.0"
            cell.taxDetails.text = "$ 1.4"
            cell.productName.text = "Masha No Logo Lining Shirt Black Color"
            cell.productReturn.layer.borderColor = UIColor.lightGray.cgColor
            cell.productReturn.layer.borderWidth = 1
            cell.productReturn.layer.cornerRadius = 3
            cell.productReturn.layer.masksToBounds = true
            cellToReturn = cell
        }
        else if indexPath.row == 6
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailssecondempty", for: indexPath) as! orderDetailsSecondEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == 7
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailstotalcost", for: indexPath) as! orderDetailsTotalCostTableViewCell
            cell.totalCost.text = "$ 243.4"
            
            cellToReturn = cell
        }
        else if indexPath.row == 8
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsthirdempty", for: indexPath) as! orderDetailsThirdEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == 9
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailspayment", for: indexPath) as! orderDetailsPaymentTableViewCell
            cell.orderId.text = "OrderId:  8745675364532"
            cell.paymentdate.text = "Payment:  2018-10-05 05:21:01"
            cell.orderconfdate.text = "Confirm:  2018-10-6 14:32:58"
            cell.copyOrderId.layer.borderColor = UIColor.lightGray.cgColor
            cell.copyOrderId.layer.borderWidth = 1
            cell.copyOrderId.layer.cornerRadius = 3
            cell.copyOrderId.addTarget(self, action: #selector(buttonViewLinkAction(sender:)), for: .touchUpInside)
            
            
            cell.copyOrderId.layer.masksToBounds = true
            cellToReturn = cell
        }
        return cellToReturn
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0
        {
            return 60
        }
        else if indexPath.row == 1
        {
            return 50
        }
        else if indexPath.row == 2
        {
            return 50
        }
        else if indexPath.row == 3
        {
            return 7
        }
        else if indexPath.row == 4
        {
            return 50
        }
        else if indexPath.row == 5
        {
            return 190
        }
        else if indexPath.row == 6
        {
            return 7
        }
        else if indexPath.row == 7
        {
            return 50
        }
        else if indexPath.row == 8
        {
            return 7
        }
        else if indexPath.row == 9
        {
            return 150
        }
        return 0
    }
    /// cpoing order id
    @objc func buttonViewLinkAction(sender:UIButton!) {
        print("Button tapped")
        UIPasteboard.general.string = orderid // or use  sender.titleLabel.text
        
    }
    
}
