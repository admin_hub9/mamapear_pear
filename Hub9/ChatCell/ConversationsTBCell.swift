//
//  ConversationsTBCell.swift
//  Hub9
//
//  Created by Deepak on 3/18/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ConversationsTBCell: UITableViewCell {
    
    @IBOutlet weak var profilePic: RoundedImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var unreadDot: UIImageView!
    
    func clearCellData()  {
        self.nameLabel.font = UIFont(name:"AvenirNext-Regular", size: 17.0)
        self.messageLabel.font = UIFont(name:"AvenirNext-Regular", size: 14.0)
        self.timeLabel.font = UIFont(name:"AvenirNext-Regular", size: 13.0)
        self.messageLabel.textColor = UIColor.rbg(r: 111, g: 113, b: 121)
        self.unreadDot.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.profilePic.layer.borderWidth = 2
        self.unreadDot.layer.cornerRadius = self.unreadDot.frame.height/2
        self.unreadDot.clipsToBounds=true
        self.profilePic.layer.borderColor = UIColor.white.cgColor
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
