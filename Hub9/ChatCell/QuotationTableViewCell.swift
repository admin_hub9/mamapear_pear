//
//  QuotationTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 4/8/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class QuotationTableViewCell: UITableViewCell {

    
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var quotationCollection: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
