//
//  quotationcollectionCell.swift
//  Hub9
//
//  Created by Deepak on 3/18/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class quotationcollectionCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productQuantity: UILabel!
    @IBOutlet weak var productValue: UILabel!
    @IBOutlet weak var price: UILabel!
    
}
