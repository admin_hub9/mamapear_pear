//
//  CreateShopDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 12/10/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SCLAlertView



var shopdetails = CreateShopDetailsViewController()

class CreateShopDetailsViewController: UIViewController, CLLocationManagerDelegate, UITextViewDelegate, UITextFieldDelegate  {

    var locationManager: CLLocationManager!
    var native = UserDefaults.standard
    var selectedCitylkpd = -1
    
    
    @IBOutlet weak var shopname: UITextField!
    @IBOutlet weak var websitename: UITextField!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var shoplocation: UIButton!
    @IBOutlet weak var soprurlAlert: UILabel!
    @IBOutlet weak var shopnamecheck: UILabel!
    @IBOutlet weak var shopurlcheck: UILabel!
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        shopname.resignFirstResponder()
        websitename.resignFirstResponder()
    }
    
    @IBAction func getCurrentLocation(_ sender: Any) {
        determineMyCurrentLocation()
    }
    func displayErrorAndLogOut() {
        let alert = SCLAlertView()
        let errorColor: UInt = 0xf8ac59

        if Util.shared.checkNetworkTapped() {
            alert.showError("Uh Oh!", subTitle: "Your session has expired! Please log in again.", closeButtonTitle: "Okay", colorStyle: errorColor)
        } else {
            alert.showError("Uh Oh!", subTitle: "You have lost connectivity to the internet. Please check your connection and log in again.", closeButtonTitle: "Okay", colorStyle: errorColor)
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shopdetails = self
        
        shoplocation.layer.cornerRadius = 5
        let buttoncolor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1)
        shoplocation.layer.borderColor = buttoncolor.cgColor
        shoplocation.layer.borderWidth = 0.5
        shoplocation.layer.masksToBounds = true
        websitename.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        shopname.addTarget(self, action: #selector(shopname(_:)), for: .editingChanged)
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if (websitename.text?.count)! > 4
        {
        checkShopURL()
        }
    }
    @objc func shopname(_ textField: UITextField) {
        websitename.text = shopname.text
        if (websitename.text?.count)! > 4
        {
            checkShopURL()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        getCity_lkpd(lng: "\(userLocation.coordinate.longitude)", lat: "\(userLocation.coordinate.latitude)")
        locationManager.stopUpdatingLocation()
//        let geocoder = CLGeocoder()
//        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
//            if (error != nil){
//                print("error in reverseGeocode")
//            }
//            let placemark = placemarks! as [CLPlacemark]
//            self.displayLocationInfo(placemark: placemark[0])
//        }
    }
    
//    func displayLocationInfo(placemark: CLPlacemark) {
//        if placemark != nil {
//            //stop updating location to save battery life
//            locationManager.stopUpdatingLocation()
//            print((placemark.locality != nil) ? placemark.locality as Any : "")
//            print((placemark.postalCode != nil) ? placemark.postalCode as Any : "")
//            print((placemark.administrativeArea != nil) ? placemark.administrativeArea as Any : "")
//            print((placemark.country != nil) ? placemark.country as Any : "")
//            let location =  "  " + placemark.subLocality! + ", " + placemark.locality!
//            print(location)
//            shoplocation.setTitle(location, for: .normal)
//        }
//    }
    
    @IBAction func showLocationButton(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "selectShopAddress") as! UIViewController
//        self.tabBarController?.selectedIndex = 1
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
        showLocationButton((Any).self)
    }
    
    
    
    
    
    func checkShopURL()
    {
        
        if let token = self.native.string(forKey: "Token") as? String
//        if token != nil
        {
            var validateUrl =  "https://www.tradey.in/users/check_shop_url/?shop_url=\(websitename.text!)"
            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            print("/////", validateUrl)
            var a = URLRequest(url: NSURL(string:validateUrl)! as URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                //                                debugPrint(response)
                print("response shop: \(response)")
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        //                        print(response.result.value)
                        let res = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = res.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            else if dict1["status"] as! String == "success"
                            {
                                self.shopurlcheck.setIcon(image: UIImage(named: "validtext")!, with: "valid shop url")
                                self.shopurlcheck.textColor = UIColor.green
                            }
                            else
                            {
                                self.shopurlcheck.setIcon(image: UIImage(named: "invalidtext")!, with: "invalid shop url")
                                self.shopurlcheck.textColor = UIColor.red
                            }
                        }
                        }
                    
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    
                }
            }}
        else
        {
            displayErrorAndLogOut()
        }
    }
    //get city lookup id
    func getCity_lkpd(lng: String, lat: String)
    {
        let parameters: [String:Any] = ["lng": lng, "lat": lat]
        
        let token = self.native.string(forKey: "Token")!
        print(parameters)
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post")
        
        Alamofire.request("https://www.tradey.in/users/place_lang_lat/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                //                    self.blurView.alpha = 0
                //                    self.activity.stopAnimating()
                //                    self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            if let json = response.result.value {
                print("JSON: \(json)")
                let jsonString = "\(json)"
                let result = response.result
                
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
               
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                    {
                        //                            self.blurView.alpha = 0
                        //                            self.activity.stopAnimating()
                        print(dict1["status"]!)
                        print(dict1["data"]!)
                        self.selectedCitylkpd = dict1["data"]!["cityid"] as! Int
                        let cityname = dict1["data"]!["city_name"]!
                        let state = dict1["data"]!["state_name"]!
                        let location = "  \(cityname!), \(state!) "
                        print(location)
                        self.shoplocation.setTitle(location, for: .normal)
                }
                    else
                    {
                        //                            self.blurView.alpha = 0
                        //                            self.activity.stopAnimating()
                        print(dict1["status"] as AnyObject)
                        print(dict1["response"] as AnyObject)
                        let alert = UIAlertController(title: dict1["status"]! as! String, message: dict1["response"]! as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                        }}
            }}
        }
    }
    
    @IBAction func updateNext(_ sender: Any) {
        updateUserInfo()
    }
    
    
    // send Update user info
    func updateUserInfo()
    {
        if selectedCitylkpd != -1 && (shopname.text?.count)! > 4
        {
            let userCode = native.string(forKey: "userCode")
            let parameters: [String:Any]
            var Pushtoken = ""
            if native.string(forKey: "B2BTokenForSNS") != nil
            {
                Pushtoken = native.string(forKey: "B2BTokenForSNS")!
            }
            let deviceId = UIDevice.current.identifierForVendor?.uuidString
            print(deviceId!)
            let deviceinfo:[String:Any] = ["DeviceName": UIDevice.current.name, "DeviceModel": UIDevice.current.modelName, "DeviceVersion": UIDevice.current.systemVersion]
            print("Pushtoken", Pushtoken)
            parameters = ["shop_name":shopname.text!,"landmarkid":2, "shop_url": websitename.text!, "cityid": selectedCitylkpd, "device_info":deviceinfo , "deviceid":deviceId! , "device_channelid":"1","device_token": Pushtoken]
        
            let token = self.native.string(forKey: "Token")!
            print(parameters)
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            Alamofire.request("\(b2burl)/users/update_user_info/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    //                    self.blurView.alpha = 0
                    //                    self.activity.stopAnimating()
                    //                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonString = "\(json)"
                    let result = response.result
                    print(result)
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        
                        switch response.result
                        {
                        case .failure(let error):
                            //                print(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                print("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                print(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                print("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                        else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                        {
//                            self.blurView.alpha = 0
//                            self.activity.stopAnimating()
                            print(dict1["status"]!)
                            print(dict1["data"]!)
                            screen.nextButton()
                            
                        }
                        else
                        {
//                            self.blurView.alpha = 0
//                            self.activity.stopAnimating()
                            print(dict1["status"] as AnyObject)
                            print(dict1["response"] as AnyObject)
                            let alert = UIAlertController(title: dict1["status"]! as! String, message: dict1["response"]! as! String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                                }}
                    }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Please Select Valid Addrees First!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}




extension UILabel {
    func setIcon(image: UIImage, with text: String) {
        let attachment = NSTextAttachment()
        attachment.image = image
        
        attachment.bounds = CGRect(x: 0, y: -2, width: 12, height: 12)
        //        attachment.image.
        
        let attachmentStr = NSAttributedString(attachment: attachment)
        
        let mutableAttributedString = NSMutableAttributedString()
       
        
        let textString = NSAttributedString(string: text + " ", attributes: [.font: self.font])
        mutableAttributedString.append(textString)
        mutableAttributedString.append(attachmentStr)
        
        self.attributedText = mutableAttributedString
    }
}
