//
//  ViewController.swift
//  Hub9
//
//  Created by Deepak on 5/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
//import UserNotifications
import Alamofire
//import AWSCore
import NotificationBannerSwift

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate{
    var type = ""
    let native = UserDefaults.standard
    var Product = [AnyObject]()
    var ProductName = [AnyObject]()
    var location = [AnyObject]()
    var CompnayName = [AnyObject]()
    var ProductPrice = [AnyObject]()
    var ProductMOQ = [AnyObject]()
    var ProductImg = [AnyObject]()
    var ProductID = [AnyObject]()
    var Img = [AnyObject]()
    var filteredName = [AnyObject]()
    var filteredImg = [AnyObject]()
    var filterName = [AnyObject]()
    var filterPrice = [AnyObject]()
    var filterMOQ = [AnyObject]()
    var filterImg = [AnyObject]()
    let jsonObject: NSMutableDictionary = NSMutableDictionary()
    var count = 0
    var filterdata = [String]()
    var isSearching = false
    var filterclick = 0
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var innerdict = [AnyObject]()
    var trendingPro = [AnyObject]()
    var userRelatedPro = [AnyObject]()
    var userrelatedProImg = [AnyObject]()
    var favProName = [AnyObject]()
    var favProID = [AnyObject]()
    var Pfilterdata = ["Categories","Brands","Stauts","Stocks"]
    
    var bannerImage = ["image", "image (1)", "image (2)", "image (3)"]
    
    
    
    
    
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var innerviewupdateContainer: UIView!
    @IBOutlet var upgradeContainer: UIView!
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var update: UIButton!
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var optionCollection: UICollectionView!
    @IBOutlet weak var favouritProduct: UICollectionView!
    @IBOutlet var shop: UIButton!
    @IBOutlet var product: UIButton!
    @IBOutlet var bannerCollection: UICollectionView!
    @IBOutlet var selectView: UIView!
    @IBOutlet var trendingProduct: UICollectionView!
    @IBOutlet weak var myscrollView: UIScrollView!
    
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var topview: UIView!
    
    
    
    
    @IBAction func cancelUpdateButton(_ sender: Any) {
        UIView.transition(with: upgradeContainer, duration: 0.4, options: .transitionFlipFromTop, animations:
            {
                self.blurView.alpha = 0
                self.upgradeContainer.isHidden = true
        }, completion: nil)
        self.navigationController?.navigationBar.alpha = 1
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.alpha = 1
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
    }
    
    @IBAction func selectType(_ sender: Any) {
        
        self.selectView.isHidden = false
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchSugg"))! as UIViewController
        self.present(editPage, animated: false, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchSugg"))! as UIViewController
        self.present(editPage, animated: false, completion: nil)
    }
    
    
    func sendDeviceToken()
    {
        
        let token = self.native.string(forKey: "Token")!
        if (UserDefaults.standard.value(forKey: "B2BTokenForSNS") != nil)
        {
            if (UserDefaults.standard.value(forKey: "B2BTokenForSNS") as! String).count == 0
            {
                //value is ""
            }
            else
            {
                //having some value
                
                let Pushtoken = native.string(forKey: "B2BTokenForSNS")!
                print("Pushtoken", Pushtoken)
                let parameters: [String:Any] = ["device_channel_lkpid":"1","device_token": Pushtoken]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
                var url = "https://demo.hub9.io/api/update_device_token/"
                
                Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    print("responses: \(response)")
                    let result = response.result
                    
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                let alertController = UIAlertController(title: "Alert", message: "Invalid Token Found", preferredStyle: .alert)
                                
                                // Create the actions
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    NSLog("OK Pressed")
                                    let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "root"))! as UIViewController
                                    
                                    self.present(editPage, animated: false, completion: nil)
                                }
                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                                    UIAlertAction in
                                    NSLog("Cancel Pressed")
                                }
                                
                                // Add the actions
                                alertController.addAction(okAction)
                                alertController.addAction(cancelAction)
                                
                                // Present the controller
                                self.present(alertController, animated: true, completion: nil)
                            }
                            
                        }}
                }}}
    }
    
    func parseData()
    {
        let Token = self.native.string(forKey: "Token")!
        print("token", Token)
        if Token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_home_data/?version=1.0&device=ios") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                switch (response.result) {
                case .success:
                    //do json stuff
                    print("response: \(response)")
                    let result = response.result
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else{
                            if let innerdict1 = dict1["data"] as? [AnyObject]
                                //                            print("---------",innerdict1)
                                
                                //                            print("innerdict", innerdict1[0]["trending_products"] as! [AnyObject])
                            { self.trendingPro = innerdict1[0]["trending_products"]  as! [AnyObject]
                                for var i in 0..<self.trendingPro.count
                                {
                                    self.ProductID.insert(self.trendingPro[i]["productid"] as! AnyObject, at: i)
                                    self.CompnayName.insert(self.trendingPro[i]["name"] as! AnyObject, at: i)
                                    self.location.insert(self.trendingPro[i]["city_name"] as! AnyObject, at: i)
                                    self.ProductName.insert(self.trendingPro[i]["product_name"] as! AnyObject, at: i)
                                    self.ProductPrice.insert(self.trendingPro[i]["price"] as! AnyObject, at: i)
                                    self.ProductImg.insert(self.trendingPro[i]["image_url"] as! AnyObject, at: i)
                                    
                                    //                        let image = self.ProductImg[i] as! [AnyObject]
                                    //                        print("images1", image[0])
                                }
                                
                                self.native.set("1", forKey: "appversion")
                                self.native.set(innerdict1[0]["update_build"]!, forKey: "appversion")
                                self.native.synchronize()
                                self.userRelatedPro = innerdict1[0]["user_related_products"] as! [AnyObject]
                                for var i in 0..<self.userRelatedPro.count
                                {   self.favProID.insert(self.userRelatedPro[i]["productid"] as! AnyObject, at: i)
                                    self.favProName.insert(self.userRelatedPro[i]["product_name"] as! AnyObject, at: i)
                                    self.userrelatedProImg.insert(self.userRelatedPro[i]["image_url"] as! AnyObject, at: i)
                                    //                        let image = self.userrelatedProImg[i] as! [AnyObject]
                                    //                        print("images1", image[0])
                                }
                            }
                        }
                        self.trendingProduct.reloadData()
                        self.favouritProduct.reloadData()
                        self.bannerCollection.reloadData()
                        self.startTimer()
                    }
                    //
                    break
                case .failure(let error):
                    if error._code == NSURLErrorTimedOut {
                        //timeout here
                    }
                    //                    print("\n\nAuth request failed with error:\n \(error)")
                    let alert = UIAlertController(title: "Alert", message: "request timed out", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    break
                }
            }}
    }
    
    
    var lastContentOffset: CGFloat = 0
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < myscrollView.contentOffset.y) {
            // moved to top
            //            print("move Up\(myscrollView.contentOffset.y)")
            //            print("alpha\(Float(myscrollView.contentOffset.y / 166))")
            navigationController?.navigationBar.alpha = myscrollView.contentOffset.y / 166
            if Float(myscrollView.contentOffset.y / 166) >= 0 {
                var newcolor = 46.0 + myscrollView.contentOffset.y
                var color = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1)
                //                searchbar.isHidden = false
                searchbar.backgroundImage = UIImage()
                searchbar.barTintColor = UIColor.clear
                topview.backgroundColor = color
                
            }
            
        } else if (self.lastContentOffset > myscrollView.contentOffset.y) {
            // moved to bottom
            //            print("alpha\(Float(myscrollView.contentOffset.y / 166))")
            //            print("move down\(myscrollView.contentOffset.y)")
            self.topview.backgroundColor = UIColor.clear
            //            searchbar.searchBarStyle = .minimal
            searchbar.backgroundImage = UIImage()
            //            searchbar.isHidden = true
            searchbar.barTintColor = UIColor.clear
            
        } else {
            
            // didn't move
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.endEditing(true)
        //        let banner = NotificationBanner(title: "alert", subtitle: "updated", style: .danger)
        //
        //        banner.show()
        //        let banner = StatusBarNotificationBanner(title: "Success", style: .success)
        //        banner.show()
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
        //            banner.dismiss()
        //            // Your code with delay
        //        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        let banner = NotificationBanner(title: "alert", subtitle: "updated", style: .success)
        //        banner.show()
        native.set(0, forKey: "loggedIn")
        native.synchronize()
        myscrollView.updateContentView()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //        readLocalJsonFile()
        //        sendDeviceToken()
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            print("varsion ", version)
        }
        //        Crashlytics.sharedInstance().crash()
        parseData()
        //        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in})
        //        clickedNotify()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        //
        //        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //        tap.cancelsTouchesInView = false
        //
        //        view.addGestureRecognizer(tap)
        //        productSelected((Any).self)
        //        searchbar.delegate = self
        bannerCollection.reloadData()
        optionCollection.reloadData()
        optionCollection.delegate = self
        optionCollection.dataSource = self
        favouritProduct.reloadData()
        favouritProduct.delegate = self
        favouritProduct.dataSource = self
        navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //Calls this function when the tap is recognized.
    override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        
        if collectionView == optionCollection{
            return 11
        }
        else if collectionView == bannerCollection{
            self.pageController.numberOfPages = bannerImage.count
            return 4
        }
        else if collectionView == trendingProduct{
            return ProductImg.count
        }
        else if collectionView == favouritProduct{
            return userrelatedProImg.count
        }
        return 5
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = bannerCollection.contentOffset
        visibleRect.size = bannerCollection.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = bannerCollection.indexPathForItem(at: visiblePoint) else { return }
        
        print("visible index is", indexPath.row)
        self.pageController.currentPage = indexPath.row
    }
    //
    /**
     Scroll to Next Cell
     */
    @objc func scrollAutomatically(_ timer1: Timer) {
        
        if let coll  = bannerCollection {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < bannerImage.count - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    //                    print("pppp", indexPath1!.row)
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    self.pageController.currentPage = indexPath1!.row
                    
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                    self.pageController.currentPage = indexPath1!.row
                    
                }
                
            }
        }
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        
        let timer =  Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = UICollectionViewCell()
        if collectionView == bannerCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "banner", for: indexPath) as! HomeBannerCollectionViewCell
            
            cell.imagebanner.image = UIImage(named: bannerImage[indexPath.row])
            
            return cell
        }
        else if collectionView == favouritProduct {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favourite", for: indexPath) as! FavouriteProductViewCell
            cell.product.layer.cornerRadius = 8.0
            cell.clipsToBounds = true
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            cell.contentView.layer.cornerRadius = 5
            cell.contentView.layer.borderWidth = 1.0
            
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
            cell.name.text = self.favProName[indexPath.row] as! String
            if self.userrelatedProImg.count > indexPath.row
            {
                if let data = self.userrelatedProImg[indexPath.row] as? [AnyObject]
                {
                    
                    var image = self.userrelatedProImg[indexPath.row] as! [AnyObject]
                    //            print("images1", image.count)
                    if image.count > 0
                    {
                        let url = NSURL(string:image[image.count - 1] as! String)
                        DispatchQueue.main.async {
                            cell.product.sd_setImage(with: URL(string: image[image.count - 1] as! String), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                    
                }
            }
            
            return cell
        }
        else if collectionView == trendingProduct
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
            
            //            print("product count \(Product.count)")
            trendingProduct.frame.size = trendingProduct.contentSize
            cell.clipsToBounds = true
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            cell.contentView.layer.cornerRadius = 5
            cell.contentView.layer.borderWidth = 1.0
            
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 3.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
            //                cell.name1.text = self.ProductName[indexPath.row] as! String
            //                cell.sold1.text = self.CompnayName[indexPath.row] as! String
            //                cell.moq1.text = self.location[indexPath.row] as? String
            //
            //                cell.price1.text = String(self.ProductPrice[indexPath.row] as! Int)
            if self.ProductImg.count >= indexPath.row
            {
                print("=====", self.ProductImg[indexPath.row], indexPath.row)
                var count1 = self.ProductImg[indexPath.row] as! [AnyObject]
                if count1.count > 0
                {
                    var image = self.ProductImg[indexPath.row] as! [AnyObject]
                    let url = NSURL(string:image[image.count - 1] as! String )
                    //                cell.product1.af_setImage(withURL: url as! URL, placeholderImage: UIImage(named: "thumbnail"))
                }
                
            }
            //                cell.name1.text = (self.ProductName[indexPath.row] as? String)?.capitalized
            let price = self.ProductPrice[indexPath.row ]
            //            cell.price1.text = String(describing: price)
            //print("mrp", self.Product[indexPath.row ]["mrp"])
            //cell.moq1.text = self.ProductMOQ[indexPath.row] as? String
            
            return cell
        }
        else if collectionView == optionCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "optioncell", for: indexPath) as! OptionViewCell
            
            switch (indexPath as NSIndexPath).row {
            case 0:
                cell.name.text = "Electronic"
                cell.image.image = UIImage(named: "Electronic")
                
            case 1:
                cell.name.text = "Furniture"
                cell.image.image = UIImage(named: "furniture")
            case 2:
                cell.name.text = "Tools"
                cell.image.image = UIImage(named: "tools")
            case 3:
                cell.name.text = "Beauty"
                cell.image.image = UIImage(named: "beauty")
            case 4:
                cell.name.text = "Car"
                cell.image.image = UIImage(named: "car")
            case 5:
                cell.name.text = "Bike"
                cell.image.image = UIImage(named: "bike")
            case 6:
                cell.name.text = "Mobile"
                cell.image.image = UIImage(named: "mobile")
            case 7:
                cell.name.text = "Shoes"
                cell.image.image = UIImage(named: "shoes")
            case 8:
                cell.name.text = "Fashion"
                cell.image.image = UIImage(named: "fashion")
            case 9:
                cell.name.text = "Sports"
                cell.image.image = UIImage(named: "sports")
            case 10:
                cell.name.text = "Houses"
                cell.image.image = UIImage(named: "Houses")
            default:
                break
            }
            return cell
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == trendingProduct
        {
            if ProductID[indexPath.row] is NSNull {
                // do something with null JSON value here
                //else code execute
                let alert = UIAlertController(title: "Alert", message: "Data Invalid Can't Proceed", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
            }
            else {
                //if code not execute
                native.set(ProductID[indexPath.row] as! Int, forKey: "FavProID")
                native.synchronize()
            }
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            self.present(editPage, animated: false, completion: nil)
        }
        if collectionView == favouritProduct
        {   if favProID[indexPath.row] is NSNull {
            // do something with null JSON value here
            //else code execute
            let alert = UIAlertController(title: "Alert", message: "Data Invalid Can't Proceed", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        else {
            //if code not execute
            native.set(favProID[indexPath.row] as! Int, forKey: "FavProID")
            native.synchronize()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            
            self.present(editPage, animated: false, completion: nil)
            }
            
            
        }
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    
}

