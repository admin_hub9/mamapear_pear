//
//  BuyNowViewController.swift
//  Hub9
//  Created by Deepak on 6/1/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import StepIndicator

import Alamofire


var value1 = BuyNowViewController()

var RazorPay = BuyNowViewController()

class BuyNowViewController: UIViewController {
    
    
    var native = UserDefaults.standard
    
   
    
    
    var window: UIWindow?
    var paymentmode = 1
    static var price = 0
    var paymentSuccess = false
 
    @IBOutlet var SuccessCard: UIView!
    @IBOutlet weak var collectAddress: UIView!
    @IBOutlet var ReviewContainer: UIView!
//    @IBOutlet weak var blurView: UIVisualEffectView!
//    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    
    
    @IBAction func bachButton(_ sender: Any) {
        print("back.....")
//        ReviewPayment.RazorpayClicked = false
//        self.dismiss(animated: true, completion: nil)
        if BBProductDetails.order_typeid != 2 || paymentSuccess == true
        {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
        }
        else
        {
            /**
             Simple Alert with Distructive button
             */
            
            //Unpaid order will be closed soon. Please submit payment
            //
            let alertController = UIAlertController(title: "", message: "Are you sure that you want to quit paying?", preferredStyle: .alert)
            
            let cancel = UIAlertAction(title: "Continue" , style: .default) { (_ action) in
                        //code here…
                   
                    }
            let ok = UIAlertAction(title: "Quit" , style: .default) { (_ action) in
                 //code here…
                         let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                         let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.window?.rootViewController = redViewController
            }
            ok.setValue(UIColor.black, forKey: "titleTextColor")
            cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
            alertController.addAction(cancel)
            alertController.addAction(ok)
            alertController.view.tintColor = .yellow
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if BBProductDetails.order_typeid != 2
        {
            self.navigationItem.title = "Checkout"
        }
        else
        {
            self.navigationItem.title = "Checkout"
        }
        Details()
        value1 = self
        // Do any additional setup after loading the view.
        
    }
    func Details()
    {
        ReviewContainer.isHidden = true
        SuccessCard.isHidden = true
        collectAddress.isHidden = false
       
    }
   
    func Payments()
    {
        collectAddress.isHidden = true
        ReviewContainer.isHidden = false
        SuccessCard.isHidden = true
       
    }
    func Review()
    {
        ReviewPayment.amount = Double(PaymentDetails.Amount)
        ReviewPayment.paymentmode = PaymentAddress.selectedPayementlkpid
        let data = PaymentAddress.selectedAddress as! AnyObject
        let useraddress = ""
        var userlandmark = ""
        var pincode = ""
        var country = ""
        if let data = data["address1"] as? String
        {
            userlandmark = data
        }
        if let data = data["landmark"] as? String
        {
            userlandmark = data
        }
        if let data = data["pincode"] as? String
        {
            pincode = data
        }
        if let data = data["country_name"] as? String
        {
            country = data
        }
        let address = "\(useraddress)  \(userlandmark) \(pincode) \(country)"
        ReviewPayment.address_lkpid = "\(data["user_addressid"] as! Int)"
        collectAddress.isHidden = true
        ReviewContainer.isHidden = false
        SuccessCard.isHidden = true
        
        
    }
    func Success()
    {
        collectAddress.isHidden = true
        ReviewContainer.isHidden = true
        SuccessCard.isHidden = false
        
        
    }
  
    
    
    

    
    
}
