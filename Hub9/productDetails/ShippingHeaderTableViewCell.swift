//
//  ShippingHeaderTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/18/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShippingHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var ShippingHeader: UILabel!
    @IBOutlet weak var arrowIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
