//
//  SameShopCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 2/26/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class SameShopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var price: UILabel!
    
}
