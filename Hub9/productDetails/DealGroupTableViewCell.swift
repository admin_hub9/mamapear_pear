//
//  DealGroupTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/23/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DealGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var dealCollection: UICollectionView!
    
    @IBOutlet weak var dealAlertButton: UIButton!
    @IBOutlet weak var userInterested: UILabel!
    @IBOutlet weak var viewAll: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
