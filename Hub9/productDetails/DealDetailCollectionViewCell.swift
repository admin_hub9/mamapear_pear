//
//  DealDetailCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/23/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DealDetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var topMessage: UILabel!
    @IBOutlet weak var timeRemaining: UILabel!
    @IBOutlet weak var labelText: UILabel!
    
    var localEndTime: TimeInterval = 0 //set value from model
    var timer:Timer?
    @objc func timerIsRunning(timer: Timer){
        let diff = localEndTime - Date().timeIntervalSince1970
        if diff  > 0 {
            self.showProgress()
            return
        }
        self.stopProgress()
        
        
    }

    func showProgress(){
        let endDate = Date(timeIntervalSince1970: localEndTime / 1000)
        let nowDate = Date()
        
        var components = Calendar.current.dateComponents(Set([.day, .hour, .minute, .second, .nanosecond]), from: endDate, to: nowDate)
        // *** Get Individual components from date ***
        self.timeRemaining?.text = "Ends in: \(String(format:"%02i:%02i:%02i:%02i", (24*(2-components.day!))+(23-components.hour!) , 59 - components.minute!, 60 - components.second!, Int(Double(components.nanosecond!)/100000000 + 0.5)))"
        
        
    }
    
    func stopProgress(){
        self.timeRemaining?.text = String(format:"%02i:%02i:%02i", 0, 0, 0)
        self.timer?.invalidate()
        self.timer = nil
    }
    
    func startTimerProgress() {
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.timerIsRunning(timer:)), userInfo: nil, repeats: true)
        self.timer?.fire()
    }
    
   
    
}
