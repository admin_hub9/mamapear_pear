//
//  ShopProductTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 2/26/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShopProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shopProductTitle: UILabel!
    @IBOutlet weak var ShopProductCollection: UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
