//
//  ShippingBillingTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 4/16/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShippingBillingTableViewCell: UITableViewCell {

    @IBOutlet weak var bullettext: UILabel!
    @IBOutlet weak var expandImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
