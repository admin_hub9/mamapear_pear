//
//  ShopNameTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/6/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Cosmos

class ShopNameTableViewCell: UITableViewCell {

    @IBOutlet weak var shopLogo: RoundedImageView!
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var shopState: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var shopLocationIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
