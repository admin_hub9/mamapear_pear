//
//  ProductNameAndPricingTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 2/26/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ProductNameAndPricingTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var featuredTag: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var signIn: UIButton!
    
    @IBOutlet weak var priceMatch: UILabel!
    @IBOutlet weak var saveRetail: UILabel!
    @IBOutlet weak var uploadTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
