//
//  DynamicDiscountRageCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 2/26/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DynamicDiscountRageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var range: UILabel!
    @IBOutlet weak var retailPrice: UILabel!
    @IBOutlet weak var bulkPrice: UILabel!
    
}
