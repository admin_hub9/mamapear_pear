//
//  ProductImageCollectionTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 2/26/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Auk

class ProductImageCollectionTableViewCell: UITableViewCell {
    

    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet weak var ProductImageCollection: UICollectionView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    

}
