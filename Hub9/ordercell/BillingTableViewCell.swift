//
//  BillingTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/6/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class BillingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var Shipping: UILabel!
    @IBOutlet weak var tax: UILabel!
    
    @IBOutlet weak var shippingInsurance: UILabel!
    //Billing text
    @IBOutlet weak var shippingChargeText: UILabel!
    @IBOutlet weak var taxText: UILabel!
    @IBOutlet weak var codText: UILabel!
    @IBOutlet weak var shippingInsuranceText: UILabel!
    
    
    //constant
    @IBOutlet weak var shippingChargeTopConstant: NSLayoutConstraint!
    @IBOutlet weak var taxtopConstant: NSLayoutConstraint!
    @IBOutlet weak var shippingTopConstant: NSLayoutConstraint!
    @IBOutlet weak var codtaxConstant: NSLayoutConstraint!
    
    
    @IBOutlet weak var cod: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
