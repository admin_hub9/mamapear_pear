//
//  AddReviewTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 25/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class AddReviewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
