//
//  orderDetailsShiipingUrlTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class orderDetailsShiipingUrlTableViewCell: UITableViewCell {

    @IBOutlet weak var shippingurlIcon: UIImageView!
    @IBOutlet weak var shippingurl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
