//
//  orderDetailsShippingTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class orderDetailsShippingTableViewCell: UITableViewCell {

    

    @IBOutlet weak var locationIcon: UIImageView!
    @IBOutlet weak var shippingAddress: UILabel!
    
    @IBOutlet weak var buyerName: UILabel!
    @IBOutlet weak var cityState: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
