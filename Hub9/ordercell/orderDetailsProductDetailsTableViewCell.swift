//
//  orderDetailsProductDetailsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class orderDetailsProductDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productReturn: UIButton!
    @IBOutlet weak var productType: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var priceDetails: UILabel!
    @IBOutlet weak var taxDetails: UILabel!
    @IBOutlet weak var actualPrice: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
