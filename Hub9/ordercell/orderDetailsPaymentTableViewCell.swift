//
//  orderDetailsPaymentTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class orderDetailsPaymentTableViewCell: UITableViewCell {

    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var trackingId: UILabel!
    @IBOutlet weak var paymentdate: UILabel!
    @IBOutlet weak var orderconfdate: UILabel!
    @IBOutlet weak var copyOrderId: UIButton!
    @IBOutlet weak var chat: UIButton!
    @IBOutlet weak var call: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
