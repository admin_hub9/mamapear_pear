//
//  QuotationDetailsStatusDetailsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class QuotationDetailsStatusDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var quotationid: UILabel!
    @IBOutlet weak var createdat: UILabel!
    @IBOutlet weak var approvedat: UILabel!
    @IBOutlet weak var copyid: UIButton!
    @IBOutlet weak var approved: UIButton!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var reject: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
