//
//  QuotationDetailsProductDetailsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class QuotationDetailsProductDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productprice: UILabel!
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var producttype: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var actualprice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
