//
//  QuotationActionTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 4/6/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class QuotationActionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var Approved: UIButton!
    @IBOutlet weak var Reject: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
