//
//  CuponsViewController.swift
//  Hub9
//
//  Created by Deepak on 8/20/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire


class CuponsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var native = UserDefaults.standard
    var CuponData = [AnyObject]()
    var order_typeid = 3
    
    @IBOutlet weak var CuponTable: UITableView!
    @IBOutlet weak var cuponHeader: UILabel!
    
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
             statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
             UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CuponTable.tableFooterView = UIView()
        getCuponData()
        if BBProductDetails.order_typeid != 2
        {
            self.order_typeid = PaymentDetails.order_typeid
        }
        else
        {
            self.order_typeid = updateCart.order_typeid
        }
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.CuponData.count <= 0
        {
            self.CuponTable.showEmptyListMessage("No Cupon found!")
            self.cuponHeader.isHidden = true
            return 0
        }
        else
        {
            self.cuponHeader.isHidden = false
            self.CuponTable.showEmptyListMessage("")
        return self.CuponData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cuponsCell", for: indexPath) as! CuponsTableViewCell
        let cupondata = self.CuponData[indexPath.row] as! AnyObject
        cell.cuponCode.text = " \(cupondata["promo_text"] as! String)  "
        cell.cuponDescription.text = cupondata["promo_description"] as! String
        print("price", cupondata["discount_percent"] as! NSNumber, Int((cupondata["user_promo_codeid"] as? NSNumber)!))
        cell.offerDetail.text = "Get \((Double(cupondata["discount_percent"] as! NSNumber))*100) Off"
        cell.ApplyButton.tag = indexPath.row
        
            cell.ApplyButton.addTarget(self, action: #selector(getCuponData(user_promo_codeid:)), for: .touchUpInside)
        let greenColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
        if BBProductDetails.order_typeid != 2
        {
        if Double(cupondata["promo_mcv"] as! NSNumber) < PaymentDetails.Amount
        {
            cell.cuponAlert.isHidden = true
            cell.ApplyButton.isEnabled = true
            cell.ApplyButton.setTitleColor(greenColor, for: .normal)
        }
        else
        {
            cell.cuponAlert.isHidden = false
            cell.ApplyButton.isEnabled = false
            cell.ApplyButton.setTitleColor(graycolor, for: .normal)
            cell.cuponAlert.text = "*Applicable on orders above \(cupondata["promo_mcv"] as! NSNumber)"
        }
        }
        else
        {
            if Double(cupondata["promo_mcv"] as! NSNumber) < updateCart.Amount
            {
                cell.cuponAlert.isHidden = true
                cell.ApplyButton.isEnabled = true
                cell.ApplyButton.setTitleColor(greenColor, for: .normal)
            }
            else
            {
                cell.cuponAlert.isHidden = false
                cell.ApplyButton.isEnabled = false
                cell.ApplyButton.setTitleColor(graycolor, for: .normal)
                cell.cuponAlert.text = "*Applicable on orders above \(cupondata["promo_mcv"] as! NSNumber)"
            }
        }
        cell.cuponCode.layer.cornerRadius = cell.cuponCode.frame.height/2
        
        cell.cuponCode.clipsToBounds = true
        return cell
    }
    
    @objc func getCuponData(user_promo_codeid: UIButton)
    {
        
        var Token = self.native.string(forKey: "Token")!
        print(Token, user_promo_codeid)
        let cupondata = self.CuponData[user_promo_codeid.tag] as! AnyObject
         var promo_codeid = Int((cupondata["user_promo_codeid"] as? NSNumber)!)
        PaymentAddress.cuponcode = cupondata["promo_text"] as! String
        PaymentAddress.user_promo_codeid = (cupondata["user_promo_codeid"] as? Int)!
        let parameters: [String:Any] = ["order_typeid":self.order_typeid, "user_promo_codeid":promo_codeid]
        let header = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
        if Token != nil
        {
            
            let b2burl = native.string(forKey: "b2burl")!
            
            var url =  "\(b2burl)/order/user_apply_promo_code/"
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                //                                debugPrint(response)
                print("response applied cupons: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    
                
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        
                            print("cupon Applied")
                            if dict1["status"] as! String == "success"
                            {
                             let data = dict1["response"] as! NSDictionary
                             if data["is_applied"] as! NSNumber == 1
                             {
                                print("cupon Applied true")
                            
                           PaymentAddress.cuponCodeApplied = true
                           PaymentAddress.cuponDiscount = Double(data["discount_value"] as! NSNumber)
                                
                                }
                                self.navigationController?.popViewController(animated: true)
                            }                    }}
                
                
            }
            
        }}
    
    
    ////get all Cupons
    func getCuponData()
    { var Token = self.native.string(forKey: "Token")!
        print(Token)
        if Token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_promo_code/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    
                
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        if let innerdict = dict1["response"] as? [AnyObject]
                        {
                            if innerdict.count > 0
                            {
                                
                                self.CuponData = innerdict
                            }
                        }
                    }}
                
                self.CuponTable.reloadData()
            }
            
        }}

}
