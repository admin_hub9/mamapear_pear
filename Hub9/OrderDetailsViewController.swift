//
//  OrderDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import Razorpay
import ZendeskSDK
import ZendeskCoreSDK


var orderDetails = OrderDetailsViewController()
class OrderDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate {
    
    
        private var razorpay:RazorpayCheckout?
        var native = UserDefaults.standard
        var trackingid = ""
        var address = ""
        var userName = ""
        var userNumber = ""
        var orderItem = [AnyObject]()
        var order_seller_detail = [AnyObject]()
        var payment_amount = 0.0
        var orderid = ""
        var order_date = ""
        var payment_date = ""
        var payment_orderid = ""
        var currency = ""
        var shopFirebaseId = ""
        var shopName = ""
        var ShopLogo = ""
        var orderChat = false
        var payment_mode = ""
        var cityState = ""
        var buyerName = ""
    
    ///charges
        var subTotal = 0.0
        var shipping = 0.0
        var quantity = ""
        var tax = 0.0
        var shippingInsurance = 0.0
        var cod = 0.0
        var tracking_url = ""
    

    @IBOutlet weak var orderdetailsTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        orderDetails = self
        razorpay = RazorpayCheckout.initWithKey("rzp_test_FYCQAsmKTFF8FR", andDelegate: self)
        self.tabBarController?.tabBar.isHidden = true
        CustomLoader.instance.showLoaderView()
        orderdetailsTable.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        parsePackedData()
        // Do any additional setup after loading the view.
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedCell:UITableViewCell = orderdetailsTable.cellForRow(at: indexPath)!
        selectedCell.selectionStyle = .none
        if indexPath.row == 1 && tracking_url != ""
        {
            
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "orderTracking"))! as UIViewController
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
        if indexPath.row == (5 + orderItem.count + 6)
            
        {
            orderChat = true
            contactSeller()
        }
        else if indexPath.row >= 5 && indexPath.row < (5 + orderItem.count)
        {
            let index = indexPath.row - 5
            if self.orderItem.count > index
            {
                if let productid = self.orderItem[index]["productid"] as? NSNumber
                {
                    native.set(productid, forKey: "FavProID")
                    native.set("fav", forKey: "comefrom")
                    native.synchronize()
                    let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
                    
                    self.navigationController?.pushViewController(editPage, animated: true)
                }}
        }
        else if indexPath.row == (7 + orderItem.count + 6)
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "addReview") as! AddProductReviewViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
        }
        
    }
    
    

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var cellCount = 0
        
        if native.object(forKey: "status") != nil
        {
            
            if let status = native.string(forKey: "status")
            {
              if status == "Delivered"
              {
                cellCount = 16 + orderItem.count
                }
                else
              {
               cellCount = 12 + orderItem.count
                }
            }}
        return cellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell()
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsstatus", for: indexPath) as! orderDetailsStatusTableViewCell
            if native.object(forKey: "status") != nil
            {
                
                if let status = native.string(forKey: "status")
                {
                    cell.status.text = "\(status)"
                   
                    ///
                    if status == "Unpaid"
                    {
                        cell.statusIcon.image = UIImage(named: "statusUnpaid")
                    }
                    else if status == "Pending"
                    {
                        cell.statusIcon.image = UIImage(named: "statusPending")
                    }
                    else if status == "Packed"
                    {
                        cell.statusIcon.image = UIImage(named: "statusPacked")
                    }
                    else if status == "Shipped"
                    {
                        cell.statusIcon.image = UIImage(named: "statusShipped")
                    }
                    else if status == "Delivered"
                    {
                        cell.statusIcon.image = UIImage(named: "statusDelivered")
                    }
                    else if status == "Return"
                    {
                        cell.statusIcon.image = UIImage(named: "statusReturn")
                    }
                    else
                    {
                        cell.statusIcon.image = UIImage(named: "statusCancelled")
                    }
                    
                    //
                    if status != "Cancelled"
                    {
                        cell.status.textColor = UIColor.white
                        cell.viewColor.backgroundColor = UIColor(red:247/255, green:82/255, blue:85/255, alpha: 1)
                    }
                    else
                    {
                        cell.status.textColor = UIColor(red:51/255, green:51/255, blue:51/255, alpha: 1)
                        cell.viewColor.backgroundColor = UIColor.lightGray
                    }
                }
            }
            cellToReturn = cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsshippingurl", for: indexPath) as! orderDetailsShiipingUrlTableViewCell
            cell.shippingurl.attributedText = NSAttributedString(string: "\(trackingid)", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
            
            cellToReturn = cell
        }
        else if indexPath.row == 2
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderDetailsShipping", for: indexPath) as! orderDetailsShippingTableViewCell
            cell.buyerName.text = "\(userName) \(userNumber)"
        cell.shippingAddress.text = "\(address)"
        cell.cityState.text = "\(self.cityState)"
        cellToReturn = cell
        }
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsfirstempty", for: indexPath) as! orderDetailsFirstEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsshopname", for: indexPath) as! orderDetailsShopNameTableViewCell
            if order_seller_detail.count > 0
            {
            cell.shopname.text = "\(order_seller_detail[0]["shop_name"] as! String)"
                var shopimage = ""
            if order_seller_detail[0]["logo_url"] is NSNull
            {}
                else
            {
            shopimage = order_seller_detail[0]["logo_url"] as! String
                }
            let url = NSURL(string: shopimage)
            if url != nil{
                DispatchQueue.main.async {
                    cell.shopIcon.sd_setImage(with: URL(string: shopimage), placeholderImage: UIImage(named: "thumbnail"))
                }
            }
            }
            cellToReturn = cell
        }
        else if indexPath.row >= 5 && indexPath.row < (5 + orderItem.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsproductdetails", for: indexPath) as! orderDetailsProductDetailsTableViewCell
            let index = indexPath.row - 5
            if self.orderItem.count > 0
            {
            if let productimage = self.orderItem[index]["item_image_url"] as? AnyObject
            {
                let imagedata = productimage["200"] as! [AnyObject]
                print("imagedata", imagedata[0])
                if imagedata.count > 0
                {
                   var imageUrl = (imagedata[0] as? String)!
                   imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                   
                   if let url = NSURL(string: imageUrl )
                   {
                       if url != nil
                       {
                        DispatchQueue.main.async {
                            cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                       }
                   }
                }
                }
            if self.orderItem[index]["quantity"] is NSNull
            {
                cell.quantity.text = ""
            }
            else
            {
                cell.quantity.text = "X \((self.orderItem[index]["quantity"] as? NSNumber)!)"
                }
            var productColor = ""
            var productSize = ""
            if let color = self.orderItem[index]["color_ui"] as? String
            {
                productColor = color
                }
                if let size = self.orderItem[index]["size_ui"] as? String
                {
                    productSize = size
                }
            cell.productType.text = "\(productColor)/\(productSize)"
                cell.productPrice.text = "\(self.currency)\(String(format:"%.2f", (Double((self.orderItem[index]["price"] as? NSNumber)!))))"
            cell.productName.text = "\((self.orderItem[index]["title"] as? String)!)"
            cell.productReturn.layer.borderColor = UIColor.lightGray.cgColor
            cell.productReturn.layer.borderWidth = 1
            cell.productReturn.layer.cornerRadius = 3
            cell.productReturn.layer.masksToBounds = true
            }
            cellToReturn = cell
        }
        else if indexPath.row == (5 + orderItem.count + 1)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailssecondempty", for: indexPath) as! orderDetailsSecondEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row ==  (5 + orderItem.count + 2)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BillingCell", for: indexPath) as! BillingTableViewCell
            
            cell.subTotal.text = "\(self.currency)\(String(format: "%.2f", (self.payment_amount - self.tax)))"
            
            if self.shipping == 0.0
            {
                cell.Shipping.text = "Free Shipping"
            }
            else
            {
            cell.Shipping.text = "\(self.currency)\(String(format: "%.2f", self.shipping))"
            }
            if self.tax > 0
            {
            cell.taxText.isHidden=false
            cell.tax.isHidden=false
                cell.taxtopConstant.constant=11
            cell.tax.text = "\(self.currency)\(String(format: "%.2f", self.tax))"
            }
            else
            {
                cell.taxText.isHidden=true
                cell.tax.isHidden=true
                cell.taxtopConstant.constant = -23
            }
            if self.shippingInsurance == 0.0
            {
                cell.shippingInsuranceText.isHidden=true
                cell.shippingTopConstant.constant = -23
                cell.shippingInsurance.isHidden=true
                
            }
            else
            {
                cell.shippingInsuranceText.isHidden=false
            cell.shippingTopConstant.constant=11
            cell.shippingInsurance.isHidden=false
            cell.shippingInsurance.text = "\(self.currency)\(String(format: "%.2f", self.shippingInsurance))"
            }
            cell.cod.text = "\(self.currency)\(String(format: "%.2f", self.cod))"
            if self.cod != 0
            {
                cell.codtaxConstant.constant = 11
                cell.codText.isHidden=false
                cell.cod.isHidden=false
            }
            else
            {
                cell.codtaxConstant.constant = -23
                cell.codText.isHidden=true
                cell.cod.isHidden=true
            }
            cellToReturn = cell
        }
        else if indexPath.row ==  (5 + orderItem.count + 3)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailstotalcost", for: indexPath) as! orderDetailsTotalCostTableViewCell
            cell.totalCost.text = "\(self.currency)\(String(format:"%.2f", (self.payment_amount+self.shippingInsurance+self.cod+self.shipping)))"
           
            cellToReturn = cell
        }
        else if indexPath.row == (5 + orderItem.count + 4)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailsthirdempty", for: indexPath) as! orderDetailsThirdEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == (5 + orderItem.count + 5)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailspayment", for: indexPath) as! orderDetailsPaymentTableViewCell
            cell.orderId.text = "OrderID: \(self.orderid)"
            cell.paymentdate.text = "Payment Date: \(self.payment_date)"
            cell.orderconfdate.text = "Payment Mode: \(self.payment_mode)"
            cell.copyOrderId.layer.borderColor = UIColor.lightGray.cgColor
            cell.copyOrderId.layer.borderWidth = 1
            cell.copyOrderId.layer.cornerRadius = 3
            cell.copyOrderId.addTarget(self, action: #selector(buttonViewLinkAction(sender:)), for: .touchUpInside)
            
           
            cell.copyOrderId.layer.masksToBounds = true
            cellToReturn = cell
        }
        else if indexPath.row == (5 + orderItem.count + 6)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatTableViewCell
        cellToReturn = cell
        }
        else if indexPath.row == (6 + orderItem.count + 6)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "fourthEmpty", for: indexPath) as! FourthEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == (7 + orderItem.count + 6)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addReview", for: indexPath) as! AddReviewTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == (8 + orderItem.count + 6)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "fifthEmpty", for: indexPath) as! FifthEmptyTableViewCell
            cellToReturn = cell
        }
        else if indexPath.row == (9 + orderItem.count + 6)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "shareFeed", for: indexPath) as! ShareFeedTableViewCell
            cellToReturn = cell
        }
        return cellToReturn
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        if indexPath.row == 0
        {
            return 60
        }
        else if indexPath.row == 1
        {
            if trackingid == ""
            {
                return 0
            }
            else
            {
            return 50
            }
        }
        else if indexPath.row == 2
        {
            if self.address == "NA"
            {
                return 0
            }
            else
            {
            return UITableViewAutomaticDimension
            }
        }
        else if indexPath.row == 3
        {
            return 7
        }
        else if indexPath.row == 4
        {
            return 40
        }
        else if indexPath.row < (5 + orderItem.count) || indexPath.row == 5
        {
            return 125
        }
        else if indexPath.row == (5 + orderItem.count + 1)
        {
            return 7
        }
        else if indexPath.row == (5 + orderItem.count + 2)
        {
            return UITableViewAutomaticDimension
        }
        else if indexPath.row == (5 + orderItem.count + 3)
        {
            return 50
        }
        else if indexPath.row == (5 + orderItem.count + 4)
        {
            return 7
        }
        else if indexPath.row == (5 + orderItem.count + 5)
        {
            return 80
        }
        else if indexPath.row == (5 + orderItem.count + 6)
        {
            return 56
        }
        else if indexPath.row == (6 + orderItem.count + 6)
        {
            return 7
        }
        else if indexPath.row == (7 + orderItem.count + 6)
        {
            return 50
        }
        else if indexPath.row == (8 + orderItem.count + 6)
        {
            return 7
        }
        else if indexPath.row == (9 + orderItem.count + 6)
        {
            return 50
        }
        else
        {
            return 50
        }
        return 0
    }
/// cpoing order id
    @objc func buttonViewLinkAction(sender:UIButton!) {
        print("Button tapped")
        self.view.makeToast("Orderid Copied", duration: 2.0, position: .bottom)
        UIPasteboard.general.string = orderid // or use  sender.titleLabel.text
        
    }

    
    // parse packed data
    func parsePackedData()
    {
        let Token = self.native.string(forKey: "Token")!
        print("token", Token)
        if Token != nil
        {
            let orderId = native.string(forKey: "orderid")!
            let status = native.string(forKey: "status")!
            self.orderid = orderId
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/order/buyer_order_detail/?orderid=\(orderId)&param_status_ui=\(status)"
            
            
            print("Packed", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response packed: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                    else if dict1["status"] as! String != "failure"
                    {
                        CustomLoader.instance.hideLoaderView()
                        if dict1["response"] is NSNull
                        {}
                        else
                        {
                            if let innerdict = dict1["response"] as? AnyObject
                            {
                                if innerdict["buyer_address"] is NSNull
                                {
                                    self.address = "NA"
                                }
                                else
                                {
                                    self.address = innerdict["buyer_address"] as! String
                                }
                                
                               
                                if innerdict["city"] is NSNull
                                {
                                    
                                }
                                else
                                {
                                    self.cityState = " \(innerdict["city"] as! String)"
                                }
                                if innerdict["state"] is NSNull
                                {
                                    
                                }
                                else
                                {
                                    self.cityState = "\(self.cityState), \(innerdict["state"] as! String)"
                                }
                                
                                if innerdict["buyer_pincode"] is NSNull
                                {
                                    
                                }
                                else
                                {
                                    self.cityState = "\(self.cityState), \(innerdict["buyer_pincode"] as! NSString)"
                                }
                                if innerdict["buyer_phone"] is NSNull
                                {}
                                else
                                {
                                    self.userNumber = "\(innerdict["buyer_phone"] as! String)"
                                }
                                if innerdict["consignee_name"] is NSNull
                                {}
                                else
                                {
                                    self.userName = "\(innerdict["consignee_name"] as! String)"
                                }
                                if innerdict["currency_symbol"] is NSNull
                                {
                                    self.currency = "NA"
                                }
                                else
                                {
                                    self.currency = innerdict["currency_symbol"] as! String
                                }
                                if innerdict["ship_insurance"] is NSNull
                                {
                                    
                                }
                                else
                                {
                                    self.shippingInsurance = Double(innerdict["ship_insurance"] as! NSNumber)
                                }
                                if innerdict["shipping_charge"] is NSNull
                                {
                                    
                                }
                                else
                                {
                                    self.shipping = Double(innerdict["shipping_charge"] as! NSNumber)
                                }
                                if innerdict["cod_charge"] is NSNull
                                {
                                    
                                }
                                else
                                {
                                    self.cod = Double(innerdict["cod_charge"] as! NSNumber)
                                }
                                
                                
                                if let itemdata = innerdict["order_item_detail"] as? [AnyObject]
                                {
                                    self.orderItem = itemdata
                                    
                                }
                                if self.orderItem.count>0
                                {
                                    for var i in 0..<self.orderItem.count
                                    {
                                    if self.orderItem[i]["product_tax_value"] is NSNull
                                    {}
                                    else
                                    {
                                        self.tax =  self.tax + (Double(self.orderItem[i]["product_tax_value"] as! NSNumber)*Double(self.orderItem[i]["quantity"] as! Int))
                                    }
                                    }
                                }
                                if let shopdetail = innerdict["order_seller_detail"] as? [AnyObject]
                                {
                                    self.order_seller_detail = shopdetail
                                    if shopdetail[0]["seller_firebaseid"] is NSNull
                                    {}
                                    else
                                    {
                                        self.shopFirebaseId = shopdetail[0]["seller_firebaseid"] as! String
                                    }
                                    if shopdetail[0]["logo_url"] is NSNull
                                    {}
                                    else
                                    {
                                        self.ShopLogo = shopdetail[0]["logo_url"] as! String
                                    }
                                    if shopdetail[0]["shop_name"] is NSNull
                                    {}
                                    else
                                    {
                                        self.shopName = shopdetail[0]["shop_name"] as! String
                                    }
                                }
                                
                                if self.orderItem.count > 0
                                {
                                    if self.orderItem[0]["trackingid"] is NSNull
                                    {}
                                    else
                                    {
                                        self.trackingid = self.orderItem[0]["trackingid"] as! String
                                    }
                                    if self.orderItem[0]["tracking_url"] is NSNull
                                    {}
                                    else
                                    {
                                        self.tracking_url = self.orderItem[0]["tracking_url"] as! String
                                    }
                                }
                                if innerdict["payment_amount"] is NSNull
                                {
                                    
                                }
                                else
                                {
                                    self.payment_amount = Double((innerdict["payment_amount"] as? NSNumber)!)
                                }
                                if innerdict["orderid"] is NSNull
                                {
                                    self.orderid = "--"
                                }
                                else
                                {
                                    self.orderid = String(innerdict["orderid"] as! Int)
                                }
                                if innerdict["order_date"] is NSNull
                                {
                                    self.order_date = "--"
                                }
                                else
                                {
                                    self.order_date = innerdict["order_date"] as! String
                                }
                                if innerdict["payment_date"] is NSNull
                                {
                                    self.payment_date = "--"
                                }
                                else
                                {
                                    self.payment_date = innerdict["payment_date"] as! String
                                }
                                if innerdict["payment_orderid"] is NSNull
                                {
                                    self.payment_orderid = "--"
                                }
                                else
                                {
                                    self.payment_orderid = innerdict["payment_orderid"] as! String
                                }
                                if innerdict["payment_mode"] is NSNull
                                {
                                    self.payment_mode = "--"
                                }
                                else
                                {
                                    self.payment_mode = innerdict["payment_mode"] as! String
                                }
                                
                            }
                        }
                        
                        
                        
                    }
                    else
                    {
                        UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        DispatchQueue.main.async {
                            
                            self.orderdetailsTable.isHidden = true
                            
                            
                        }
                        UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                            alertView.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        alertView.showInfo(dict1["status"]! as! String, subTitle: dict1["response"]! as! String, colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
                    }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                DispatchQueue.main.async {
                    
                    
                    self.orderdetailsTable.isHidden = false
                    self.orderdetailsTable.reloadData()
                    
                    
                }
            }
        }
    }
    
    func contactSeller()
    {
        var firstName = ""
         var lastName = ""
         var userID = ""
         
         if native.object(forKey: "userid") != nil
         {
             userID = native.string(forKey: "userid")!
         }
         if native.object(forKey: "firstname") != nil
         {
             firstName = native.string(forKey: "firstname")!
         }
         if native.object(forKey: "lastname") != nil
         {
             lastName = native.string(forKey: "lastname")!
         }
         print("Zendesk Support")
         let identity = Identity.createAnonymous(name: "\(firstName) \(lastName): \(userID)", email: "")
         Zendesk.instance?.setIdentity(identity)
         Support.initialize(withZendesk: Zendesk.instance)
         let hcConfig = HelpCenterUiConfiguration()
         hcConfig.groupType = .category
         let helpCenter = HelpCenterUi.buildHelpCenterOverview(withConfigs: [hcConfig])
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
             self.tabBarController?.tabBar.isHidden = true
         self.navigationController!.pushViewController(helpCenter, animated: true)
    }
    
//    func contactSeller()
//    {
//        let token = self.native.string(forKey: "Token")!
//        if token != nil
//        {
//
//
//            if self.shopFirebaseId != nil && self.shopName != nil && self.ShopLogo != nil
//
//            {
//
//                CustomLoader.instance.showLoaderView()
//                if shopFirebaseId == native.string(forKey: "firebaseid")! as! String
//                {
//                    let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your own shop", preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                }
//                else
//                {
//
//                    //            topview.isHidden = true
//                    BBProductDetails.quotation = false
//                    native.set("1", forKey: "currentuserid")
//                    native.synchronize()
//                    var chatroom = CheckChatRoomViewController()
//
//                    chatroom.chekChatRoom(shopid: shopFirebaseId, shopname: shopName, shopProfilePic: ShopLogo)
////                    self.navigationController?.navigationBar.isHidden = false;
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
//                        // Your code with delay
//                        CustomLoader.instance.hideLoaderView()
//                        UIApplication.shared.endIgnoringInteractionEvents()
//
//                        self.performSegue(withIdentifier: "orderSeller", sender: self)
//                    }
//                }
//            }
//        }
//        else
//        {
//
//        }
//    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    func payNowClicked() {
            
            let options: [String:Any] = [
                "amount" : "1000", //mandatory in paise like:- 1000 paise ==  10 rs
                "description": "purchase description",
                "image": "ss",
                "name": "Swift Series",
                "prefill": [
                "contact": "9797979797",
                "email": "foo@bar.com"
                ],
                "theme": [
                    "color": "#F37254"
                ]
            ]
           razorpay?.open(options)
        }
    }

    extension OrderDetailsViewController: RazorpayPaymentCompletionProtocol {
        func onPaymentSuccess(_ payment_id: String) {
            let alert = UIAlertController(title: "Paid", message: "Payment Success", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
        
        func onPaymentError(_ code: Int32, description str: String) {
            let alert = UIAlertController(title: "Error", message: "\(code)\n\(str)", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }

