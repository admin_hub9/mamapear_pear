//
//  selectDashboardSubCategoryTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/13/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class selectDashboardSubCategoryTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var subCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
