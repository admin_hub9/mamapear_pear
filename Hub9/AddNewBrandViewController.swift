//
//  AddNewBrandViewController.swift
//  Hub9
//
//  Created by Deepak on 7/31/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire

class AddNewBrandViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    
    var data = [String]()
    var brandID = [Int]()
    var native = UserDefaults.standard
    var filteredData: [String]!
    var indexNumber:NSInteger = -1
    @IBOutlet var addSearchBar: UISearchBar!
    @IBOutlet var AddTableView: UITableView!
    @IBOutlet var addBrandView: UIView!
    @IBOutlet var blurView: UIVisualEffectView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SelectBrand()
        AddTableView.isHidden = true
        filteredData = data
        // Do any additional setup after loading the view.
    }
    func SelectBrand()
    { let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_brand/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response shop: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    let innerdict = dict1["data"] as! [[String: Any]]
                    
                    for var i in 0..<innerdict.count
                    {
                        print("brand",innerdict[i]["brand_lkpid"]!, innerdict[i]["brand_name"]!)
                        self.data.append(innerdict[i]["brand_name"]! as! String)
                        self.brandID.append(innerdict[i]["brand_lkpid"]! as! Int)
                    }
                    
                }
                print("data", self.data)
                self.filteredData = self.data
                self.AddTableView.isHidden = false
                self.AddTableView.reloadData()
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func BrandAddedButton(_ sender: Any) {
        
                print(addSearchBar.text)
                if data.contains(addSearchBar.text!)
                {
                    let alert = UIAlertController(title: "Alert", message: "Brand Already Exist", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                }
                else
        
                {

                    if addSearchBar.text != ""
                    {
                    data.append(addSearchBar.text!)
                    filteredData = data
                        native.set(addSearchBar.text!, forKey: "brand")
                        native.set("0", forKey: "brandID")
                        native.synchronize()
                        addSearchBar.resignFirstResponder()
                        self.dismiss(animated: true, completion: nil)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert", message: "Please type your brand name first", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
//                    tableView.reloadData()
                    
                }
        
            }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == AddTableView
        {
            AddTableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            if indexPath.row == indexNumber
            {
                indexNumber = -1
            }
            else{
            indexNumber = indexPath.row
                native.set(filteredData[indexPath.row], forKey: "brand")
                native.synchronize()
                self.dismiss(animated: true, completion: nil)
                
            }
            print(indexPath.row)
            AddTableView.reloadData()
        }
    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if tableView == AddTableView
//        {
////            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
//            indexNumber = -1
//            print(indexPath.row)
//            AddTableView.reloadData()
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReturnCell = UITableViewCell()
        if tableView == AddTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "addbrand", for: indexPath) as! BrandTableViewCell
            cell.brandName?.text = filteredData[indexPath.row]
            if indexNumber == indexPath.row{
                cell.accessoryType = .checkmark
            }else{
                cell.accessoryType = .none
            }
            ReturnCell = cell
        }
       return ReturnCell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return filteredData.count
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData = searchText.isEmpty ? data : data.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        
        if searchBar == self.addSearchBar{
            print("second")
            AddTableView.reloadData()
        }
        
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
         if searchBar == self.addSearchBar{
            print("second")
            self.addSearchBar.showsCancelButton = false
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        AddTableView.reloadData()
    }

}
