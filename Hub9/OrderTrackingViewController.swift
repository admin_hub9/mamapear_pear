//
//  OrderTrackingViewController.swift
//  Hub9
//
//  Created by Deepak on 04/12/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import WebKit

class OrderTrackingViewController: UIViewController, WKNavigationDelegate {

var webView: WKWebView!

override func viewDidLoad() {
    super.viewDidLoad()
    // 1
    setToolBar()
    self.tabBarController?.tabBar.isHidden = true
    CustomLoader.instance.showLoaderView()
    let seconds = 4.0
    
    let url = URL(string: "\(orderDetails.tracking_url)")!
    webView.load(URLRequest(url: url))
    
    // 2
    let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
    toolbarItems = [refresh]
    navigationController?.isToolbarHidden = false
   DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
       CustomLoader.instance.hideLoaderView()
       UIApplication.shared.endIgnoringInteractionEvents()
       // Put your code which should be executed with a delay here
   }
    // Do any additional setup after loading the view.
}

    fileprivate func setToolBar() {
        let screenWidth = self.view.bounds.width
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(goBack))
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 44))
        toolBar.isTranslucent = false
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        toolBar.items = [backButton]
        webView.addSubview(toolBar)
    // Constraints
        toolBar.bottomAnchor.constraint(equalTo: webView.bottomAnchor, constant: 0).isActive = true
        toolBar.leadingAnchor.constraint(equalTo: webView.leadingAnchor, constant: 0).isActive = true
        toolBar.trailingAnchor.constraint(equalTo: webView.trailingAnchor, constant: 0).isActive = true
      }
    
    @objc private func goBack() {
      if webView.canGoBack {
        webView.goBack()
      } else {
        self.dismiss(animated: true, completion: nil)
      }
    }
    
func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    title = webView.title
}

override func loadView() {
    webView = WKWebView()
    webView.navigationDelegate = self
    view = webView
}

/*
// MARK: - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destination.
    // Pass the selected object to the new view controller.
}
*/

}
