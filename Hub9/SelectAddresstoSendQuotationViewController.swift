//
//  SelectAddresstoSendQuotationViewController.swift
//  Hub9
//
//  Created by Deepak on 4/6/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView

class SelectAddresstoSendQuotationViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var selectshippingindex = -1
    var native = UserDefaults.standard
    var selectAddress = [AnyObject]()
    var user_addressid = 0

    @IBOutlet weak var AddressCollection: UICollectionView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.blurView.alpha = 1
        CustomLoader.instance.showLoaderView()
        getAddressData()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancel(_ sender: Any) {
        
       
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func selectAddress(_ sender: Any) {
        if user_addressid != 0
        {
         quotationDetails.user_addressid = self.user_addressid
         quotationDetails.ApproveQuotation()
        self.dismiss(animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Please Select Valid Address First", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
            selectshippingindex = indexPath.row
            if selectAddress[indexPath.row]["user_addressid"] is NSNull
            {}
            else
            {
            user_addressid = selectAddress[indexPath.row]["user_addressid"] as! Int
            }
            AddressCollection.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if selectAddress.count == 0
        {
            
        }
        return selectAddress.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentaddresscell", for: indexPath) as! PaymentAddressCollectionViewCell
        cell.contentView.layer.cornerRadius = 2.0
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.backgroundColor = UIColor.white.cgColor
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        cell.username.text = self.selectAddress[indexPath.row]["consignee_name"] as? String
        cell.address.text = self.selectAddress[indexPath.row]["address1"] as? String
        //        print(indexPath.row, "saved address")
        var cityname = ""
        var statename = ""
        var pincode = ""
        if let data = self.selectAddress[indexPath.row]["city_name"] as? String
        {
            cityname = data
        }
        if let data = self.selectAddress[indexPath.row]["state_name"] as? String
        {
            statename = data
        }
        if let data = self.selectAddress[indexPath.row]["pincode"] as? String
        {
            pincode = data
        }
        
        cell.citystatepincode.text = "\(cityname) \(statename) \(pincode)"
        if let data = self.selectAddress[indexPath.row]["country_name"] as? String
        {
            cell.country.text = data
        }
        
            if selectshippingindex == indexPath.row
            {
                cell.mark.image = UIImage(named: "checkmark")
//                cell.edit.isHidden = false
//                cell.edit.tag = indexPath.row
                
            }
            else
            {
                cell.mark.image = UIImage(named: "Uncheckmark")
//                cell.edit.isHidden = true
            }
        
        return cell
    }

    
    ////get all address
    func getAddressData()
    { var Token = self.native.string(forKey: "Token")!
        print(Token)
        if Token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/saved_address/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if let innerdict = dict1["data"] as? [AnyObject]
                    {
                    if innerdict.count > 0
                    {
                        self.selectAddress = innerdict
                        
                    }
                            }}}
                }
                print(self.selectAddress)
                self.blurView.alpha = 0
                CustomLoader.instance.hideLoaderView()
                self.AddressCollection.reloadData()
               
            }
            
        }}

}
