//
//  UpdateVariantCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/2/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class UpdateVariantCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var variantName: UILabel!
    @IBOutlet weak var variantPrice: UILabel!
    @IBOutlet weak var QuantityView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
   
    @IBOutlet weak var quantity: UIButton!
    
    @IBOutlet weak var totalPrice: UILabel!
    
    @IBOutlet weak var outofstock: UILabel!
    @IBOutlet weak var notifyMe: UIButton!
    
}
