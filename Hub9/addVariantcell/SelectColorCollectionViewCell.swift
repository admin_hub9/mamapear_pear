//
//  SelectColorCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/2/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class SelectColorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ColorName: UILabel!
    @IBOutlet weak var indicator: UIView!
    
}
