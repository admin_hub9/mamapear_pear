//
//  ShopListingViewController.swift
//  Hub9
//
//  Created by Deepak on 6/22/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
let parent = ShopListingViewController()

class ShopListingViewController: UIViewController {
    
     var native = UserDefaults.standard
    var countdownTimer: Timer!
    var totalTime = 60

    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var update: UIButton!
    @IBOutlet var cancelUpdate: UIButton!
    @IBOutlet var upgradeContainer: UIView!
    @IBOutlet var innerviewupdateContainer: UIView!
    @IBOutlet var headerButtonView: UIView!
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var myFeed: UIView!
    @IBOutlet var nameview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let appversion = native.string(forKey: "appversion")
//        print("appversion", appversion!)
//        if appversion! == "1"
//        {
//            print("hello")
//            innerviewupdateContainer.layer.cornerRadius = 8
//            innerviewupdateContainer.clipsToBounds = true
//            self.navigationController?.navigationBar.alpha = 0.75
//            self.tabBarController?.tabBar.isUserInteractionEnabled = false
//            self.tabBarController?.tabBar.alpha = 0.75
//            self.navigationController?.navigationBar.isUserInteractionEnabled = false
//            UIView.transition(with: upgradeContainer, duration: 0.4, options: .transitionFlipFromBottom, animations:
//                {
//                    self.blurView.alpha = 1
//                    self.upgradeContainer.isHidden = false
//            }, completion: nil)
//        }
//
        let usertype = native.string(forKey: "usertype")!
        
        if usertype == "3"
        {
            if native.object(forKey: "changetype") as! Int == 1
            {
                segmentControl.isHidden = true
                myFeed.isHidden = true
                nameview.frame.size.height += 50.0
                UIView.transition(with: nameview, duration: 0.4, options: .beginFromCurrentState, animations:
                    {
                        //                                self.blueView.alpha = 0
                        self.nameview.transform = CGAffineTransform(translationX: 0, y: -50)
                        self.headerButtonView.isHidden = true
                }, completion: nil)
                nameview.isHidden = false
            }
            else
            {
//             switchButton((Any).self)
            }
        }
        else if usertype  == "1"
        {
            segmentControl.isHidden = true
            myFeed.isHidden = true
            nameview.frame.size.height += 50.0
            UIView.transition(with: nameview, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0
                                    self.nameview.transform = CGAffineTransform(translationX: 0, y: -50)
                    self.headerButtonView.isHidden = true
            }, completion: nil)
            nameview.isHidden = false
        }
        else{
//        switchButton((Any).self)
        }
        // Do any additional setup after loading the view.
    }
    func startTimer() {
//        sendAgain.isEnabled = false
//        sendAgain.tintColor = UIColor.lightGray
//        countDownLabel.isHidden = false
        
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        //    sendOtp()
        
    }
    
    @objc func updateTime() {
        
//        countDownLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
            print("......",totalTime)
        } else {
            print("over")
            endTimer()
//            countDownLabel.isHidden = true
//            sendAgain.isEnabled = true
//            sendAgain.tintColor = UIColor.black
        }
    }
    func endTimer() {
        countdownTimer.invalidate()
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 50
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 50
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func cancelUpdateButton(_ sender: Any) {
        UIView.transition(with: upgradeContainer, duration: 0.4, options: .transitionFlipFromTop, animations:
            {
                self.blurView.alpha = 0
                self.upgradeContainer.isHidden = true
        }, completion: nil)
        self.navigationController?.navigationBar.alpha = 1
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.alpha = 1
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
    }
    
    
    
   

}
