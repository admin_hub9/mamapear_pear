//
//  PaymentAddressViewController.swift
//  Hub9
//
//  Created by Deepak on 10/18/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import RSSelectionMenu
import Razorpay
import AttributedTextView
import Stripe


var PaymentAddress = PaymentAddressViewController()

class PaymentAddressViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, RazorpayPaymentCompletionProtocol, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    
    //// store stripe data
    ///
    var paymentCardTextField = STPPaymentCardTextField()
    /// to refresh address from add address
    var addAddressClick = false
    
    var selectshippingindex = -1
    var selectbillingindex = 0
    var addresscount = 0
    var native = UserDefaults.standard
    var productData = [AnyObject]()
    var productName = [String]()
    var selectAddress = [AnyObject]()
    var selectedAddress = NSDictionary()
    var selectedPayementlkpid = -1
    var selectedPaymenttype = ""
    var simpleSelectedArray = [String]()
    var checkmarkicon = true
    var insurance = true
    var useWalletCash = false
    var isCod = false
    
    
    ///charges var
    var shippingCost = 0.0
    var codCost = 0.0
    var cod_charge = 0.0
    var subTotalCost = 0.0
    var TotalCost = 0.0
    var Tax = 0.0
    var currency = ""
    var shipInsurance = 0.0
    var TotalPrice = 0.0
    var order_typeid = 3
    var group_ownerid = ""
    var group_orderid = ""
    
    var iscodApplicable = false
    var cuponCodeApplied=false
    var user_promo_codeid = 0
    var cuponcode = ""
    var cuponDiscount = 0.0
    var weight = 0
    var is_locker_code = false
    
    var amount = 0.0
    var address_lkpid = ""
    var paymentmode = 1
    var RazorPaypayment_orderid = ""
    var RazorPayorderid = ""
    var RazorPayamount = ""
    
    var payment_id = ""
    var paymentMethod = [AnyObject]()
    
    private var razorpay: RazorpayCheckout!
    // rzp_test_VopucrFFo1wvVi
    // rzp_live_MndzBgEajSb0UH
    var razorpayTestKey = "rzp_test_VopucrFFo1wvVi"
//    var payment_orderid = ""
    var gateway_paymentid = ""
    var orderid = ""
    var selectedPaymentMethod = ""
    
    
    var stripeToken = ""
    var Stripe_payment_orderid = ""
    var Stripe_payment_id = ""
    var Stripe_amount = ""
    var Stripe_orderid = ""
    
    
    var pickupLocation = false
    var expressShipping = false
    var standerdShipping = false
    var expShipMethod = NSDictionary()
    var stdShipMethod = NSDictionary()
    var pickupLocationData = [AnyObject]()
    var expressCharge = 0.0
    var standerdCharge = 0.0
    var lockerApplied = false
    var pickupApplied = false
    
    
    
    @IBOutlet weak var shippingaddress: UICollectionView!
    
    @IBOutlet weak var reviewButton: UIButton!
    
    
    @IBOutlet weak var cashViewContainer: UIView!
    
    @IBOutlet weak var productCollection: UICollectionView!
    
    @IBOutlet weak var taxTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var shippingInsuranceContraint: NSLayoutConstraint!
    @IBOutlet weak var codTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var totalTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var FinalView: NSLayoutConstraint!
    @IBOutlet weak var ReviewScreenConstraint: NSLayoutConstraint!
    
    
    
//    @IBOutlet weak var codDisableView: UIVisualEffectView!
//    @IBOutlet weak var codButton: UIButton!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var shippingCharge: UILabel!
    @IBOutlet weak var codeCharge: UILabel!
    @IBOutlet weak var codText: UILabel!
    @IBOutlet weak var shiipingInsuranceText: UILabel!
    @IBOutlet weak var insuranceCharge: UILabel!
    @IBOutlet weak var insuranceIcon: UIImageView!
    @IBOutlet weak var Total: UILabel!
    @IBOutlet weak var TaxPrice: UILabel!
    
    @IBOutlet weak var taxTitle: UILabel!
    
    @IBOutlet weak var normalContainer: UIView!
    
    @IBOutlet weak var shipinfoText: UILabel!
    
    @IBOutlet weak var CuponContainer: UIView!
    @IBOutlet weak var AppliedCuponContainer: UIView!
    
    @IBOutlet weak var methodType: UILabel!
    @IBOutlet weak var lockerCodeText: UILabel!
    
    @IBOutlet weak var changeAddressLable: UIButton!
    
    @IBOutlet weak var shopImage: UIImageView!
    @IBOutlet weak var shopTitle: UILabel!
    
    // coupon label
    
    @IBOutlet weak var couponText: UILabel!
    
    @IBOutlet weak var couponBal: UILabel!
    
    // wallet Views
    
    @IBOutlet weak var walletCheckBoxIcon: UIImageView!
    
    // Wallet Click
    
    @IBAction func walletIconClickListner(_ sender: Any) {
        if self.useWalletCash == true
         {
             self.useWalletCash=false
//             self.shiipingInsuranceText.isHidden = true
//             self.insuranceCharge.isHidden = true
//             self.codTopConstraint.constant = -23
             self.walletCheckBoxIcon.image = UIImage(named: "Uncheckmark")
         }
         else
         {
//             self.codTopConstraint.constant = 8
             self.walletCheckBoxIcon.image = UIImage(named: "checkmark")
//             self.shiipingInsuranceText.isHidden = false
//             self.insuranceCharge.isHidden = false
             self.useWalletCash=true
         }
        calculatePrice()
        
    }
    
    
    @IBAction func getCuponCode(_ sender: Any) {
        
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "cuponView"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    
    @IBAction func PaymentMethod(_ sender: Any) {
        if self.paymentCardTextField.cardNumber != nil && self.selectedPayementlkpid == 5
        {
            self.openStripe()
        }
        else
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "selectPaymentMethod") as! BBPaymentViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func RemoveCuponButton(_ sender: Any) {
        cuponCodeApplied=false
        self.cuponDiscount = 0.0
        CuponContainer.isHidden = false
        AppliedCuponContainer.isHidden = true
        calculatePrice()
    }
    
    
    
    
   @objc func openShippingAddress()
   {
    
    let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "shippingAddress"))! as! ShippingAddressViewController
    self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
    self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    
    func calculatePrice()
    {
        self.methodType.text = selectedPaymentMethod
        if let codData = native.string(forKey: "is_cod_applicable") as? String
        {
        if codData == "0"
        {
            self.iscodApplicable = false
        }
        else
        {
            self.iscodApplicable = true
            }}
        print("ordertypeid", BBProductDetails.order_typeid)
        self.group_ownerid = BBProductDetails.group_ownerid
        self.group_orderid = BBProductDetails.group_orderid
        if BBProductDetails.order_typeid != 2
        {
            self.order_typeid = PaymentDetails.order_typeid
            self.currency = "\(PaymentDetails.currency)"
            if self.cuponDiscount > 0
            {
                let textcolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                var cost = PaymentDetails.Amount - PaymentDetails.product_tax_value
                self.subTotalCost = cost - self.cuponDiscount
                self.couponText.isHidden = false
                self.couponBal.isHidden = false
                self.couponText.text = "Coupon Discount"
                self.totalTopConstraint.constant = 8
                self.couponBal.text = "-\(self.currency)\(String(format:"%.2f", self.cuponDiscount))"
                self.subTotal.attributedText = ("\(self.currency)\(String(format:"%.2f", cost)) " .color(textcolor).size(15) ).attributedText
            }
            else
            {
                self.couponBal.text = ""
                self.couponText.text = ""
                self.totalTopConstraint.constant = -23
                self.couponText.isHidden = true
                self.couponBal.isHidden = true
                self.subTotalCost = (PaymentDetails.Amount - PaymentDetails.product_tax_value)
                self.subTotal.text = "\(self.currency)\(String(format:"%.2f", self.subTotalCost))"
            }
            
            
            
            //calculate shiipingcost
            
            if self.shippingCost == 0
            {
              self.shippingCharge.text = "Free Shipping"
            }
            else
            {
                self.shippingCharge.text = "\(self.currency)\(String(format:"%.2f", self.shippingCost))"
            }
            //calculate shiiping insurance charge
            if self.insurance==false
            {
                self.shipInsurance = 0
            }
            else
            {
            self.shipInsurance = PaymentDetails.ship_insurance
            }
            if self.shipInsurance == 0
            {
                self.codTopConstraint.constant = -23
                self.insuranceCharge.text = "Free"
            }
            else
            {
                self.codTopConstraint.constant = 8
                self.insuranceCharge.text = "\(self.currency)\(String(format:"%.2f", self.shipInsurance))"
            }
            self.codCost = PaymentDetails.cod_charge
            if self.codCost == 0
            {
                self.FinalView.constant = -23
                self.codeCharge.text = "FREE"
            }
            else
            {
                self.FinalView.constant = 8
                self.codeCharge.text = "\(self.currency)\(self.codCost)"
            }
            
            self.Tax = PaymentDetails.product_tax_value
            if self.Tax == 0
           {
            self.TaxPrice.isHidden = true
            self.taxTitle.isHidden = true
            self.shippingInsuranceContraint.constant = -23
            self.TaxPrice.text = "Free"
           }
            else
           {
            self.shippingInsuranceContraint.constant = 8
            self.TaxPrice.isHidden = false
            self.taxTitle.isHidden = false
            self.TaxPrice.text = "\(self.currency)\(String(format: "%.2f", PaymentDetails.product_tax_value))"
            }
            if isCod == true && self.insurance == true
            {
                self.TotalCost = self.subTotalCost + self.shippingCost + self.Tax + self.shipInsurance + self.codCost
                self.Total.text = "\(self.currency)\(String(format:"%.2f", self.TotalCost))"
            }
            else  if isCod == true && self.insurance == false
            {
                self.TotalCost = (self.subTotalCost + self.shippingCost + self.Tax + self.codCost)
                self.Total.text = "\(PaymentDetails.currency)\(String(format:"%.2f", self.TotalCost))"
            }
            else  if isCod == false && self.insurance == true
            {
                self.TotalCost = (self.subTotalCost + self.shippingCost + self.Tax + self.shipInsurance)
                self.Total.text = "\(PaymentDetails.currency)\(String(format:"%.2f", self.TotalCost))"
            }
            else if isCod == false && self.insurance == false
            {
                self.TotalCost = (self.subTotalCost + self.shippingCost + self.Tax)
                self.Total.text = "\(self.currency)\(String(format:"%.2f", self.TotalCost))"
            }
            
            
        }
        else
        {
            self.order_typeid = updateCart.order_typeid
            self.currency = "\(updateCart.currency)"
            if self.cuponDiscount > 0
            {
                let textcolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                self.couponBal.text = ""
                self.couponText.text = ""
                self.totalTopConstraint.constant = 8
                self.couponText.isHidden = false
                self.couponBal.isHidden = false
                var cost = updateCart.Amount - updateCart.product_tax_value
                self.subTotalCost = cost - self.cuponDiscount
                self.subTotal.attributedText = ("\(self.currency)\(String(format:"%.2f",subTotalCost))" .color(textcolor).size(15) + "\(self.currency)\(String(format:"%.2f", cost)) ".color(lightGrayColor).strikethrough(1).size(15)).attributedText
            }
            else
            {
            self.couponBal.text = ""
            self.couponText.text = ""
            self.totalTopConstraint.constant = -23
            self.couponText.isHidden = true
            self.couponBal.isHidden = true
            self.subTotalCost = (updateCart.Amount - updateCart.product_tax_value)
            self.subTotal.text = "\(self.currency)\(String(format:"%.2f", self.subTotalCost))"
            }
            
            //calculate shiipingcost
            
            if self.shippingCost == 0
            {
                self.shippingCharge.text = "Free Shipping"
            }
            else
            {
                self.shippingCharge.text = "\(self.currency)\(String(format:"%.2f", self.shippingCost))"
            }
            //calculate shiiping insurance charge
            if self.insurance==false
            {
                self.shipInsurance = 0
            }
            else
            {
                self.shipInsurance = updateCart.ship_insurance
            }
            if self.shipInsurance == 0
            {
                self.codTopConstraint.constant = -23
                self.insuranceCharge.text = "Free"
            }
            else
            {
                self.codTopConstraint.constant = 8
                self.insuranceCharge.text = "\(self.currency)\(String(format:"%.2f", self.shipInsurance))"
            }
            self.codCost = updateCart.cod_charge
            if self.codCost == 0
            {
                self.FinalView.constant = -23
                self.codeCharge.text = "Free"
            }
            else
            {
                self.FinalView.constant = 8
                self.codeCharge.text = "\(self.currency)\(String(format:"%.2f", self.codCost))"
            }
            
            self.Tax = updateCart.product_tax_value
            if self.Tax == 0
            {
                self.shippingInsuranceContraint.constant = -23
                self.TaxPrice.isHidden = true
                self.taxTitle.isHidden = true
                self.TaxPrice.text = "Free"
            }
            else
            {
                self.shippingInsuranceContraint.constant = 8
                self.TaxPrice.isHidden = false
                self.taxTitle.isHidden = false
                self.TaxPrice.text = "\(self.currency)\(String(format:"%.2f", updateCart.product_tax_value))"
            }
            if isCod == true && insurance == true
            {
                self.TotalCost = self.subTotalCost + self.shippingCost + self.Tax + self.shipInsurance + self.codCost
                self.Total.text = "\(self.currency)\(String(format:"%.2f", self.TotalCost))"
            }
            else  if isCod == true && insurance == false
            {
                self.TotalCost = (self.subTotalCost + self.shippingCost + self.Tax + self.codCost)
                self.Total.text = "\(self.currency)\(String(format:"%.2f", self.TotalCost))"
            }
            else  if isCod == false && insurance == true
            {
                self.TotalCost = (self.subTotalCost + self.shippingCost + self.Tax + self.shipInsurance)
                self.Total.text = "\(self.currency)\(String(format:"%.2f", self.TotalCost))"
            }
            else if isCod == false && insurance == false
            {
                self.TotalCost = (self.subTotalCost + self.shippingCost + self.Tax)
                self.Total.text = "\(self.currency)\(String(format:"%.2f", self.TotalCost))"
            }
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkPaymentDetails()
        
        if addAddressClick == true
        {
            addAddressClick=false
            getAddressData()
        }
            let lockercode = UITapGestureRecognizer(target: self, action: #selector(lockerAlert))
            lockerCodeText.isUserInteractionEnabled = true
            lockerCodeText.addGestureRecognizer(lockercode)
        
        
        if cuponCodeApplied == false{
            CuponContainer.isHidden = false
            AppliedCuponContainer.isHidden = true
        }
        else
        {
            CuponContainer.isHidden = true
            AppliedCuponContainer.isHidden = false
            self.calculatePrice()
            let alertController = UIAlertController(title: "Coupon Applied Successfully", message: "You save \(self.currency)\(String(format:"%.2f", self.cuponDiscount)).", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                
            }
            // Add the actions
            alertController.addAction(okAction)
            //                            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
//        BBProductDetails.order_typeid = 3
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(PaymentDetails.bb_coin)
           print(PaymentDetails.wallet_value)
        getAddressData()
        reviewButton.backgroundColor = UIColor.lightGray
        checkPaymentMathod()
        if BBProductDetails.order_typeid != 2
        {
        if let data = PaymentDetails.productData as? [AnyObject]
        {
            for var i in 0..<data.count
            {
                if let variantData = data[i]["variant_data"] as? [AnyObject]
                {
                for var j in 0..<variantData.count
                {
                    if data[i]["title"] is NSNull
                    {
                        self.productName.append("")
                    }
                    else
                    {
                    self.productName.append(data[i]["title"] as! String)
                    }
                    if variantData[j] is NSNull
                    {}
                    else
                    {
                        self.productData.append(variantData[j] as! AnyObject)
                    }
//                    print("data product", i, data[i]["title"] as! String)
//                    print("data product", i, variantData[j])
                    
                    } }} }
        }
            else
            {
                if let data = updateCart.cartProduct as? [AnyObject]
                {
                    for var i in 0..<data.count
                    {
                        if let variantData = data[i]["variant_data"] as? [AnyObject]
                        {
                        for var j in 0..<variantData.count
                        {
                            if data[i]["title"] is NSNull
                            {
                                self.productName.append("")
                            }
                            else
                            {
                                self.productName.append(data[i]["title"] as! String)
                            }
                            if variantData[j] is NSNull
                            {}
                            else
                            {
                                self.productData.append(variantData[j] as! AnyObject)
                            }
                        } } }}
        }
        if BBProductDetails.group_ownerid != "" && BBProductDetails.order_typeid == 2
        {
            UIView.transition(with: self.cashViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                    self.blurView.alpha = 0.6
                    self.cashViewContainer.transform = CGAffineTransform(translationX: 0, y: -123)
                    self.shippingaddress.isHidden = true
            }, completion: nil)
        }
        else
        {
        UIView.transition(with: self.cashViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                    self.blurView.alpha = 0.6
                self.cashViewContainer.transform = CGAffineTransform(translationX: 0, y: -143)
                self.shippingaddress.isHidden = true
        }, completion: nil)
        }
        razorpay = RazorpayCheckout.initWithKey(razorpayTestKey, andDelegate: self)
        
        let priceM = UITapGestureRecognizer(target: self, action: #selector(ShipInfoButton))
        self.shipinfoText.isUserInteractionEnabled = true
        self.shipinfoText.addGestureRecognizer(priceM)
        if BBProductDetails.group_ownerid != "" && BBProductDetails.order_typeid == 2
        {
            self.lockerCodeText.isHidden = false
            self.ReviewScreenConstraint.constant = 8
        }
        else
        {
            self.ReviewScreenConstraint.constant = -23
            self.lockerCodeText.isHidden = true
        }
        checkmarkicon = false
        PaymentAddress = self
        reviewButton.layer.cornerRadius = reviewButton.layer.frame.size.height/2
        reviewButton.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 239/255.0, green: 130/255.0, blue: 136/255.0, alpha: 1.00).cgColor
       
        
        // Do any additional setup after loading the view.
    }
    
    
    func checkPaymentMathod()
    {
        if BBProductDetails.order_typeid != 2
        {
            self.paymentMethod = PaymentDetails.paymentMethod
            
        }
        else
        {
            self.paymentMethod = updateCart.paymentMethod
        }
//        for var i in 0..<paymentMethod.count
//        {
//
//             if paymentMethod[i]["payment_mode"] as! String == "Stripe"{
//                self.selectedPayementlkpid = 5
//                self.paymentMethod.removeAll()
//                self.paymentMethod.append(["Credit/Debit/UPI"] as AnyObject)
//                break
//            }
//        }
        paymenttype()
    }
    
    func calculateProductWeight()
    {
        weight = 0
        if BBProductDetails.order_typeid == 2
        {
            let data1 = updateCart.cartProduct as! [AnyObject]
            print("cart data", updateCart.cartProduct)
            if data1.count > 0
            {
                if data1[0]["shop_logo"] is NSNull
                {}
                else
                {
                    var imageUrl = data1[0]["shop_logo"] as! String
                    
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    let url = NSURL(string: imageUrl)
                    if url != nil{
                        DispatchQueue.main.async {
                            self.shopImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
                if data1[0]["shop_name"] is NSNull
                {}
                else
                {
                    self.shopTitle.text = (data1[0]["shop_name"] as? String)!
                }
            }
            for var i in 0..<data1.count
            {
                if let data = data1[i]["variant_data"] as? [AnyObject]
                {
                    for var j in 0..<data.count
                    {
                        if data[j]["product_weight"] is NSNull
                        {}
                        else
                        {
                            weight = weight + (Int(data[j]["product_weight"] as! NSNumber)*Int(data[j]["quantity"] as! NSNumber))
                            print("weight", weight)
                        }
                    }
                }
            }
            
//            if self.selectAddress.count > self.selectshippingindex
//            {
//                var pincode = Int(self.selectAddress[selectshippingindex]["pincode"] as! String)
//                print("uyuygyu:';''", pincode, weight)
//                checkPincodeServicable(pincode: pincode!, shopid: Int(data1[0]["shopid"] as! NSNumber), weight: weight)
//            }
        }
        else
        {
            let data2 = PaymentDetails.productData as! [AnyObject]
            print("product details data", PaymentDetails.productData)
            if data2.count > 0
            {
            if data2[0]["shop_logo"] is NSNull
            {}
            else
            {
                var imageUrl = data2[0]["shop_logo"] as! String
                
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: imageUrl)
                if url != nil{
                    DispatchQueue.main.async {
                        self.shopImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
                }
                if data2[0]["shop_name"] is NSNull
                {}
                else
                {
                    self.shopTitle.text = (data2[0]["shop_name"] as? String)!
                }
            }
            for var i in 0..<data2.count
            {
                if let data = data2[i]["variant_data"] as? [AnyObject]
                {
                    for var j in 0..<data.count
                    {
                        if data[j]["product_weight"] is NSNull
                        {}
                        else
                        {
                    weight = weight + (Int(data[j]["product_weight"] as! NSNumber)*Int(data[j]["quantity"] as! NSNumber))
                            print("weight", weight)
                        }
                    }
                }
            }
            
            if self.selectAddress.count > self.selectshippingindex && self.selectshippingindex > 0
            {
                var pincode = Int((self.selectAddress[selectshippingindex]["pincode"] as? String)!)
                var userCountryid = Int((self.selectAddress[selectshippingindex]["countryid"] as? NSNumber)!)
                var shopPincode = 0
                var shopCountryID = 0
                if data2[0]["pincode"] is NSNull
                {}
                else
                {
                    if let pincode = data2[0]["pincode"] as? NSNumber
                    {
                        shopPincode = Int(pincode)
                    }
                }
                if data2[0]["countryid"] is NSNull
                {}
                else
                {
                    if let countryid = data2[0]["countryid"] as? NSNumber
                    {
                        shopCountryID = Int(countryid)
                    }
                }
                print("uyuygyu:';''", pincode, weight, shopCountryID, shopPincode)
                checkPincodeServicable(userPincode: pincode!, shopid: Int((data2[0]["shopid"] as? NSNumber)!), weight: weight, shpoPincode: shopPincode, shop_countryid: shopCountryID, user_countryid: userCountryid)
            }
            
        }
    }
    
    
    func checkPaymentDetails()
    {
        if selectedPayementlkpid != -1 && address_lkpid != ""
        {
            reviewButton.setTitle("Place Order", for: .normal)
            reviewButton.backgroundColor =  UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        }
        else
        {
            reviewButton.setTitle("Continue", for: .normal)
            reviewButton.backgroundColor =  UIColor.lightGray
        }
    }
    
    @IBAction func ReviewOrder(_ sender: Any) {
        if address_lkpid != ""
        {
        if selectedPayementlkpid == -1
        {
            self.view.makeToast("Please select Payment method first!", duration: 2.0, position: .center)
        }
        else if selectedPayementlkpid == 5 && stripeToken == ""
        {
            openStripe()
        }
        else
        {
        
        if selectAddress.count > 0
        {
            parseData()
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Please add Some Address", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.view.makeToast("Please select address first!", duration: 2.0, position: .center)
        }
    }
    
    
    @objc func openStripe()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "stripePayment") as! UIViewController
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        present(pvc, animated: true, completion: nil)
    }
    
    
    
    @objc func ShipInfoButton(_ sender: Any) {
        native.set("3", forKey: "AlertInfo")
        native.synchronize()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "alertInfo") as! InformationAlertViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
    }
    
    
    
    @IBAction func addNewAddress(_ sender: Any) {
        addAddressClick = true
        if self.selectAddress.count>0
        {
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "displayAddress"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)
        }
        else
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "addnewaddress"))! as UIViewController
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        
        if collectionView == self.productCollection
        {
            return self.productData.count
        }
        else
        {
            count = selectAddress.count
        }
        
        return count
    }
    
    
    func openShippingCharge(addressIndex: Int)
    {
        
        if selectAddress.count>addressIndex
        {
            selectshippingindex = addressIndex
            self.address_lkpid = String(Int((self.selectAddress[addressIndex]["user_addressid"] as? NSNumber)!))
            print("herjb address", self.selectAddress[addressIndex]["user_addressid"] as? NSNumber!, self.address_lkpid)
            var data1 = [AnyObject]()
            if BBProductDetails.order_typeid == 2
            {
                data1 = updateCart.cartProduct as! [AnyObject]
            }
            else
            {
                data1 = PaymentDetails.productData as! [AnyObject]
            }
            var pincode = Int((self.selectAddress[selectshippingindex]["pincode"] as? String)!)
            
            var shopPincode = 0
            var shopCountryID = 0
            if data1.count>0
            {
                if data1[0]["pincode"] is NSNull
                {}
                else
                {
                    if let pincode = data1[0]["pincode"] as? String
                    {
                        shopPincode = Int(pincode)!
                    }
                }
                if data1[0]["countryid"] is NSNull
                {}
                else
                {
                    if let countryid = data1[0]["countryid"] as? String
                    {
                        shopCountryID = Int(countryid)!
                    }
                }
                print("uyuygyu:';''", pincode, weight)
                if var userCountryid = self.selectAddress[selectshippingindex]["countryid"] as? NSNumber
                {
                    checkPincodeServicable(userPincode: pincode!, shopid: Int((data1[0]["shopid"] as? NSNumber)!), weight: weight, shpoPincode: shopPincode, shop_countryid: shopCountryID, user_countryid: Int(userCountryid))
                    
                    print("uyuygyu:';''", pincode, weight, data1[0]["shopid"] as? NSNumber)
                }}
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == shippingaddress
        {
            if lockerApplied == false && pickupApplied == false
            {
                self.openShippingCharge(addressIndex: indexPath.row)
                
            }
            else if pickupApplied == true && lockerApplied == false
            {
                if indexPath.row != 0
                {
                    
                    let alertController = UIAlertController(title: "", message: "Are you want to remove pickup location?", preferredStyle: .alert)
                                       
                                       let cancel = UIAlertAction(title: "No" , style: .default) { (_ action) in
                                                   //code here…
                                              
                                               }
                                       let ok = UIAlertAction(title: "Yes" , style: .default) { (_ action) in
                                            //code here…
                                                     self.pickupApplied = false
                                                     self.lockerApplied = false
                                                     self.selectAddress.remove(at: 0)
                                                     self.selectshippingindex = -1
                                                     self.selectbillingindex = -1
                                                     self.shippingCost=0.0
                                                     self.address_lkpid=""
                                                     PaymentAddress.calculatePrice()
                                                     self.shippingaddress.reloadData()
                                                     
                                                     self.openShippingCharge(addressIndex: indexPath.row - 1)
                                       }
                                       ok.setValue(UIColor.black, forKey: "titleTextColor")
                                       cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
                                       alertController.addAction(cancel)
                                       alertController.addAction(ok)
                                       alertController.view.tintColor = .yellow
                                       self.present(alertController, animated: true, completion: nil)
                    
                    
                
                }
            }
            else if lockerApplied == true && pickupApplied == false
            {
                if indexPath.row != 0
                {
                    let alertController = UIAlertController(title: "", message: "Are you want to remove locker discount?", preferredStyle: .alert)
                    
                    let cancel = UIAlertAction(title: "No" , style: .default) { (_ action) in
                                //code here…
                           
                            }
                    let ok = UIAlertAction(title: "Yes" , style: .default) { (_ action) in
                         //code here…
                                  self.lockerCodeText.text = "Get Free Shipping with Locker Code"
                                                             self.is_locker_code = false
                                                             self.changeAddressLable.isHidden = false
                                                             self.pickupApplied = false
                                                             self.lockerApplied = false
                                                             self.selectAddress.remove(at: 0)
                                                             self.selectshippingindex = -1
                                                             self.selectbillingindex = -1
                                                             self.shippingCost=0.0
                                                             self.address_lkpid=""
                                                             PaymentAddress.calculatePrice()
                                                             self.shippingaddress.reloadData()
                                                             self.openShippingCharge(addressIndex: indexPath.row - 1)
                    }
                    ok.setValue(UIColor.black, forKey: "titleTextColor")
                    cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
                    alertController.addAction(cancel)
                    alertController.addAction(ok)
                    alertController.view.tintColor = .yellow
                    self.present(alertController, animated: true, completion: nil)
                    
                
                }
            }
            checkPaymentDetails()
            shippingaddress.reloadData()
            
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var ReuseCell = UICollectionViewCell()
        if collectionView == productCollection
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cartCollectionCell", for: indexPath) as! CartProductCollectionViewCell
            cell.clipsToBounds = true
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            cell.contentView.layer.cornerRadius = 5
            cell.contentView.layer.borderWidth = 1.0
            
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
                cell.productName.text = self.productName[indexPath.row] as! String
            if let data = self.productData[indexPath.row] as? AnyObject
            {
                if let images = data["product_image_url"] as? AnyObject
                {
                    let imagedata = images["200"] as! [AnyObject]
                    if imagedata.count > 0
                    {
                        
                        var imageUrl = imagedata[0] as! String
                        
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                        let url = NSURL(string: imageUrl)
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                        
                    }}
                if data["selling_price"] is NSNull
                {}
                else
                {
                    cell.price.text = "\((data["currency_symbol"] as! String)) \((data["selling_price"] as! NSString))"
                    
                }
                if data["quantity"] is NSNull
                {}
                else
                {
                    cell.quantity.text = "Qty: \(data["quantity"] as! NSNumber) pcs"
                }
                var color_ui = ""
                var size_ui = ""
                if data["color_ui"] is NSNull
                {}
                else
                {
                    color_ui = "\(data["color_ui"] as! String)"
                }
                if data["size_ui"] is NSNull
                {}
                else
                {
                    size_ui = "\(data["size_ui"] as! String)"
                }
                if (color_ui != "") && (size_ui != "")
                {
                cell.materialType.text="\(color_ui)/\(size_ui)"
                }
                if (color_ui != "") && (size_ui == "")
                {
                cell.materialType.text="\(color_ui)"
                }
                else
                {
                cell.materialType.text="\(size_ui)"
                }
                }
         
            ReuseCell = cell
        
            
        }
        else
        {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "paymentaddresscell", for: indexPath) as! PaymentAddressCollectionViewCell
        cell.layer.masksToBounds = false
            if selectshippingindex == indexPath.row
                {
                    cell.mark.image = UIImage(named: "checkmark")
    
                }
                else
                {
                    cell.mark.image = UIImage(named: "Uncheckmark")
                }
            
                    if lockerApplied == true && indexPath.row == 0
                    {
                    var result = ""
                    if self.selectAddress[indexPath.row]["phone"] is NSNull
                    {}
                    else
                    {
                        let mobileNumer = "\((self.selectAddress[indexPath.row]["phone"] as? String)!)"
                        guard mobileNumer.count > 5 else {
                            fatalError("The phone number is not complete")
                        }
                        
                        let intLetters = mobileNumer.prefix(3)
                        let endLetters = mobileNumer.suffix(2)
                        
                        let stars = String(repeating: "*", count: mobileNumer.count - 5)
                        
                        result = intLetters + stars + endLetters
                    }
                        if self.selectAddress[indexPath.row]["address_name"] is NSNull
                        {}
                        else
                        {
                            var name = self.selectAddress[indexPath.row]["address_name"] as? String
                            name = name!.starred()
                            
                            cell.username.text = "\(name!), \(result)"
                        }
                    }
                   
                    else if pickupApplied == true && indexPath.row == 0
                    {
                        cell.addressTag.text = "pickup"
                        var name = ""
                        if self.selectAddress[indexPath.row]["address_name"] is NSNull
                        {}
                        else
                        {
                            name = (self.selectAddress[indexPath.row]["address_name"] as? String)!
                        }
                        var result = ""
                        
                        if self.selectAddress[indexPath.row]["phone"] is NSNull
                        {}
                        else
                        {
                            let mobileNumer = "\((self.selectAddress[indexPath.row]["phone"] as? String)!)"
                            
                            result = mobileNumer
                        }
                        
                        cell.username.text = "\(name), \(result)"
                        }
                    else if pickupApplied == true && indexPath.row != 0
                    {
                        cell.addressTag.text = ""
                        var name = ""
                        if self.selectAddress[indexPath.row]["consignee_name"] is NSNull
                        {
                            if self.selectAddress[indexPath.row]["address_name"] is NSNull
                            {}
                            else
                            {
                                cell.username.text = self.selectAddress[indexPath.row]["address_name"] as? String
                            }
                        }
                        else
                        {
                            name = (self.selectAddress[indexPath.row]["consignee_name"] as? String)!
                        }
                        var result = ""
                        
                        if self.selectAddress[indexPath.row]["contact_number"] is NSNull
                        {}
                        else
                        {
                            let mobileNumer = "\((self.selectAddress[indexPath.row]["contact_number"] as? String)!)"
                            
                            result = mobileNumer
                        }
                        
                        cell.username.text = "\(name), \(result)"
                }
                    
                    else
                    {
                    var name = ""
                        cell.addressTag.text = ""
                        if self.selectAddress[indexPath.row]["consignee_name"] is NSNull
                        {
                            if self.selectAddress[indexPath.row]["address_name"] is NSNull
                            {}
                            else
                            {
                                cell.username.text = self.selectAddress[indexPath.row]["address_name"] as? String
                            }
                        }
                        else
                        {
                        name = (self.selectAddress[indexPath.row]["consignee_name"] as? String)!
                        }
                        var result = ""
                        
                        if self.selectAddress[indexPath.row]["contact_number"] is NSNull
                        {}
                        else
                        {
                            let mobileNumer = "\((self.selectAddress[indexPath.row]["contact_number"] as? String)!)"
                            
                            result = mobileNumer
                        }
                    
                    cell.username.text = "\(name), \(result)"
                    
                    }
                    var cityname = ""
                    var statename = ""
                    var address = ""
                    var pincode = ""
            
                    if let data = self.selectAddress[indexPath.row]["address2"] as? String
                    {
                        address = data
                    }
                    if let data = self.selectAddress[indexPath.row]["address1"] as? String
                    {
                        if address != ""
                        {
                        address = "\(data), \(address)"
                        }
                        else
                        {
                            address = data
                        }
                    }
            
                    if let data = self.selectAddress[indexPath.row]["city"] as? String
                    {
                        cityname = data
                    }
                    if let data = self.selectAddress[indexPath.row]["state"] as? String
                    {
                        statename = data
                    }
                    if let data = self.selectAddress[indexPath.row]["pincode"] as? String
                    {
                        pincode = data
                    }
            
                    cell.address.text = "\(address)"
                    cell.citystatepincode.text = "\(cityname) \(statename) \(pincode)"
                    if self.lockerApplied == true && indexPath.row == 0
                    {
                        cell.addressTag.isHidden = false
                        cell.addressTag.text = " Locker "
                    }
                    else if self.pickupApplied == true && indexPath.row == 0
                    {
                        cell.addressTag.isHidden = false
                        cell.addressTag.text = " Pickup "
                    }
                    else
                    {
                      cell.addressTag.isHidden = true
                    }
        
            ReuseCell = cell
        }
        return ReuseCell
    }

            
    
    
    ////get all address
    func getAddressData()
    { var Token = self.native.string(forKey: "Token")!
        print(Token)
        if Token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/saved_address/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                    let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                        switch response.result
                        {
                        case .failure(let error):
                            //                print(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                print("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                print(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                print("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                        else if let innerdict = dict1["data"] as? [AnyObject]
                        {
                            
                            if innerdict.count > 0
                            {
                                
                                self.selectAddress = innerdict
                                
                                
                                UIView.transition(with: self.cashViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                                    {
                                        //                    self.blurView.alpha = 0.6
                                        self.cashViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                                        self.shippingaddress.isHidden = false
                                }, completion: nil)
                                
                            }
//                            else
//                            {
//
//                                if BBProductDetails.group_ownerid != "" && BBProductDetails.order_typeid == 2
//                                {
//                                    UIView.transition(with: self.cashViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
//                                        {
//                                            //                    self.blurView.alpha = 0.6
//                                            self.cashViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
//                                            self.shippingaddress.isHidden = true
//                                    }, completion: nil)
//                                }}
                            
                            if self.selectAddress.count <= 0
                            
                                {
                                    UIView.transition(with: self.cashViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                                        {
                                            //                    self.blurView.alpha = 0.6
                                            self.cashViewContainer.transform = CGAffineTransform(translationX: 0, y: -93)
                                            self.shippingaddress.isHidden = true
                                    }, completion: nil)
                                
                            }
                        }
                            }}}
                print(self.selectAddress)
                self.calculatePrice()
                self.calculateProductWeight()
                self.shippingaddress.reloadData()
                
            }
            
        }}
    
 func CardPayment() {
    
        self.selectedPayementlkpid = 1
         self.selectedPaymenttype = "Cash on Delivery"

        self.codText.isHidden = false
        self.codeCharge.isHidden = false
            isCod = true
            calculatePrice()
    
    }

   func paymenttype() {
    
        if self.selectedPayementlkpid == 4
        {
        self.selectedPaymenttype = "Credit/Debit/UPI"
        
        self.codText.isHidden = true
        self.codeCharge.isHidden = true
        self.codeCharge.text = ""
        isCod = false
        calculatePrice()
        }
        else
        {
            self.selectedPaymenttype = "Stripe"
            
            self.codText.isHidden = true
            self.codeCharge.isHidden = true
            self.codeCharge.text = ""
            self.FinalView.constant = -23
            isCod = false
            calculatePrice()
    }

         }
    
    @IBAction func InsuranceButton(_ sender: Any) {
        
        if self.insurance == true
        {
            self.insurance=false
            self.shiipingInsuranceText.isHidden = true
            self.insuranceCharge.isHidden = true
            self.codTopConstraint.constant = -23
            self.insuranceIcon.image = UIImage(named: "Uncheckmark")
        }
        else
        {
            self.codTopConstraint.constant = 8
            self.insuranceIcon.image = UIImage(named: "checkmark")
            self.shiipingInsuranceText.isHidden = false
            self.insuranceCharge.isHidden = false
            self.insurance=true
        }
       calculatePrice()
    }
    
    
 ///create order
    
    public func parseData()
    {
        
        if address_lkpid != ""
        {
        
        if PaymentAddress.selectedPayementlkpid == 1 || PaymentAddress.selectedPayementlkpid == 4 || PaymentAddress.selectedPayementlkpid == 5
        {
        if PaymentAddress.selectedPayementlkpid == 4 || PaymentAddress.selectedPayementlkpid == 5
        {
            self.codCost = 0
        }
//        else if PaymentAddress.selectedPayementlkpid == 1
//        {
//            self.codCost = PaymentDetails.cod_charge
//        }
        
        let address_data: [String:Any] = ["addressid": address_lkpid]
        
        let billing_address_data: [String:Any] = ["addressid": address_lkpid]
        var parameters: [String:Any] = [String:Any]()
        print("BBProductDetails.group_ownerid", PaymentAddress.group_ownerid, PaymentAddress.order_typeid)
        if PaymentAddress.order_typeid != 2 && PaymentAddress.group_ownerid == ""
        {
            parameters = ["address_data":address_data,"payment_modeid":selectedPayementlkpid,"order_typeid":PaymentAddress.order_typeid, "amount":String(format:"%.2f", self.subTotalCost),"cod_charge": self.codCost, "ship_insurance": self.shipInsurance, "shipping_charge": self.shippingCost, "remarks":"thanks", "billing_address_data":billing_address_data, "user_promo_codeid": user_promo_codeid, "is_promo_code": cuponCodeApplied]
        }
        else if PaymentAddress.order_typeid == 2 && PaymentAddress.group_ownerid == ""
        {
            print("5765765")
            parameters = ["address_data":address_data,"payment_modeid":selectedPayementlkpid,"order_typeid":PaymentAddress.order_typeid, "amount":String(format:"%.2f", self.subTotalCost),"cod_charge": self.codCost, "ship_insurance": self.shipInsurance, "shipping_charge": self.shippingCost, "remarks":"thanks", "billing_address_data":billing_address_data, "user_promo_codeid": user_promo_codeid, "is_promo_code": cuponCodeApplied]
        }
        else if PaymentAddress.group_ownerid != "" && BBProductDetails.order_typeid == 2
        {
            print("g78667hgdf")
            parameters = ["address_data":address_data,"payment_modeid":selectedPayementlkpid,"order_typeid":PaymentAddress.order_typeid, "amount":String(format:"%.2f", self.subTotalCost) ,"cod_charge": self.codCost, "ship_insurance": self.shipInsurance, "shipping_charge": self.shippingCost, "remarks":"thanks", "billing_address_data":billing_address_data, "group_ownerid":PaymentAddress.group_ownerid, "group_orderid":PaymentAddress.group_orderid,"is_locker_code":self.is_locker_code,"user_promo_codeid": user_promo_codeid, "is_promo_code": cuponCodeApplied]
            //
        }
        
        print("rrrrr",parameters)
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post", parameters)
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/create_order/"
        
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            print("response shop: \(response)")
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as? String == "success" && dict1["response"] as? String == "Order Created Successfully."{
                        
                        if self.selectedPayementlkpid == 4
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            print("paymentmode", self.paymentmode)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.RazorPaypayment_orderid = (dict1["payment_order_id"] as? String)!
                            self.RazorPayorderid = "\((dict1["order_id"] as? Int)!)"
                            self.payment_id = "\(dict1["payment_id"] as! NSNumber)"
                            self.RazorPayamount = "\(self.amount)"
                            
                            self.initPayment((Any).self)
                            //
                        }
                        else if self.selectedPayementlkpid == 5
                        {
                            // payment_orderid = amount according to yadav
                            self.Stripe_payment_orderid = "\((dict1["payment_order_id"] as? NSNumber)!)"
                            self.Stripe_orderid = "\((dict1["order_id"] as? Int)!)"
                            self.Stripe_payment_id = "\(dict1["payment_id"] as! NSNumber)"
                            self.Stripe_amount = "\(self.TotalCost)"
                            self.stripeGeneratePayment()
                            
                        }
                        else
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
//                            self.RazorPaypayment_orderid = (dict1["payment_order_id"] as? String)!
//                            self.RazorPayorderid = "\((dict1["order_id"] as? Int)!)"
//                            self.payment_id = "\(dict1["payment_id"] as! NSNumber)"
//                            self.RazorPayamount = "\(self.amount)"
                            value1.paymentSuccess = true
                            value1.Success()
                        }
                    }
                    else{
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        let alert = UIAlertController(title: "Alert", message: "failed to create orders! please try after sometime", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }}
        }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.view.makeToast("Please select payment method first!", duration: 2.0, position: .center)
        }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.view.makeToast("Please select address first", duration: 2.0, position: .center)
        }
        
    }
    
    
    ////////payment option razorpay
    @objc func initPayment(_ sender: Any)
    {
        orderid = RazorPayorderid
        gateway_paymentid = RazorPaypayment_orderid
        //        amount = Double(RazorPayamount)!
        showPaymentForm()
    }
    
    
    
    internal func showPaymentForm(){
        var options: [String:Any] = [
            "name" : "BulkByte", //mandatory in paise
            "description": "purchase description",
            "order_id": self.RazorPaypayment_orderid
        ]
        
        print(options)
        if options != nil
        {
            razorpay.open(options)
        }
    }
    
    
    func Repayment()
    {
        UIApplication.shared.beginIgnoringInteractionEvents()
        let pvc = storyboard?.instantiateViewController(withIdentifier: "Repayment") as! RepaymentAlertViewController
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        present(pvc, animated: true, completion: nil)
        
        
    }
    
    
    public func onPaymentError(_ code: Int32, description str: String){
        print("error in payment", code, description, str)
        UIApplication.shared.beginIgnoringInteractionEvents()
        Repayment()
        
    }
    
    public func onPaymentSuccess(_ payment_id: String){
        //        CustomLoader.instance.showLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        gateway_paymentid = payment_id
        generatePayment()
        
    }
    
    func generatePayment()
    {
        var parameters: [String:Any] = [String:Any]()
        if self.stripeToken == ""
        {
        parameters = ["gateway_paymentid":gateway_paymentid, "payment_orderid":RazorPaypayment_orderid,"payment_id":payment_id, "amount": amount, "orderid": orderid]
        }
        else
        {
            parameters = ["gateway_paymentid":self.stripeToken, "payment_orderid":self.Stripe_payment_orderid,"payment_id":Stripe_payment_id, "amount": Stripe_amount, "orderid": Stripe_orderid]
        }
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully generate payment", parameters)
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/generate_payment/"
        print(parameters, url)
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            switch response.result {
            case .success(let data as [String:Any]):
                // Yeah! Hand response
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                        print("res;;; pincode", jsonString)
                        if (dict1["status"] as? String)?.lowercased() == "success"
                        {
                            value1.Success()
                            self.dismiss(animated: true, completion: nil)
                        }
                        else if dict1["status"] as? String == "failure"
                        {
                         self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .center)
                        }
                    }
                }
                if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    
                }
            case .failure(let error):
                //                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            default:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                print("default")
            }
            print(" generate payment", response)
            
        }
        
    }
    
    
    // check pincode servicable
    func checkPincodeServicable(userPincode: Int, shopid: Int, weight: Int, shpoPincode: Int, shop_countryid: Int, user_countryid: Int )
    {
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
            
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/order/get_shipping_charge/?destination_pincode=\(userPincode)&weight=\(weight)&shopid=\(shopid)&shop_countryid=\(shop_countryid)&buyer_countryid=\(user_countryid)&shop_pincode=\(shpoPincode)"
            print("follow product", url)
            Alamofire.request(url , encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                print("shipping charge", response)
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if let json = response.result.value {
                            let jsonString = "\(json)"
                            print("res;;; pincode", jsonString)
                            if dict1["status"] as? String == "success" && dict1["is_servicable"] as? Bool ?? true
                            {
                                self.shippingCost = Double(dict1["shipping_charge"] as! NSNumber)
                                
                                if dict1["express_data"] is NSNull
                                {
                                    self.expressShipping = false
                                }
                                else
                                {
                                    if let expShipping = dict1["express_data"] as? NSDictionary
                                    {
                                        
                                        if (expShipping["processing_from"] is NSNull) || (expShipping["processing_to"] is NSNull)
                                        {
                                            print("87645687468", expShipping["processing_from"]!)
                                            self.expressShipping = false
                                            
                                        }
                                        else
                                        {
                                            
                                            self.expShipMethod = expShipping
                                            self.expressShipping = true
                                            if let expPrice = expShipping["express_shipping_charge"] as? NSNumber
                                            {
                                                self.expressCharge = Double(expPrice)
                                            }
                                        }
                                        
                                    }
                                   
                                }
                                if dict1["pickup_location_data"] is NSNull
                                {
                                    self.pickupLocation = false
                                }
                                else
                                {
                                    if let pickLoc = dict1["pickup_location_data"] as? [AnyObject]
                                    {
                                        self.pickupLocationData = pickLoc
                                    }
                                    
                                    self.pickupLocation = true
                                }
                                
                                
                                if dict1["standard_data"] is NSNull
                                {
                                    self.standerdShipping = false
                                }
                                else
                                {if let stdShipping = dict1["standard_data"] as? NSDictionary
                                {
                                    print("87645687468", stdShipping["processing_from"]!)
                                    if (stdShipping["processing_from"] is NSNull) || (stdShipping["processing_to"] is NSNull)
                                    {
                                        print("87645687468", stdShipping["processing_from"]!)
                                        self.standerdShipping = false
                                        
                                    }
                                    else
                                    {
                                        self.standerdShipping = true
                                        self.stdShipMethod = stdShipping
                                        if let stdPrice = stdShipping["shipping_charge"] as? NSNumber
                                        {
                                            self.standerdCharge = Double(stdPrice)
                                        }
                                    }
                                    
                                    }
                                }
                                
                                print("shipping", self.expressShipping, self.standerdShipping, self.pickupLocation)
                                if self.standerdShipping == true && self.expressShipping == true || self.pickupLocation == true
                                {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let pvc = storyboard.instantiateViewController(withIdentifier: "ShippingMethod") as! ShippingOtionsViewController
                                    
                                    pvc.modalPresentationStyle = .custom
                                    pvc.transitioningDelegate = self
                                    
                                    self.present(pvc, animated: true, completion: nil)
                                    
                                }
                                self.calculatePrice()
                            }
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                        }
                    }}
            }
        }
        else
        {
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    
    //locker code alert
    @objc func lockerAlert()
    {
        if lockerApplied == false && pickupApplied == false
        {
        if self.lockerCodeText.text != "Remove locker code"
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "lockerAlert") as! LockerAlertViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
        }
        else
        {
            let alertController = UIAlertController(title: "", message: "Are you sure that you want to remove locker code?", preferredStyle: .alert)
            
            let cancel = UIAlertAction(title: "No" , style: .default) { (_ action) in
                        //code here…
                   
                    }
            let ok = UIAlertAction(title: "Yes" , style: .default) { (_ action) in
                 //code here…
                 self.lockerCodeText.text = "Get Free Shipping with Locker Code"
                 self.is_locker_code = false
                 self.changeAddressLable.isHidden = false
                 self.pickupApplied = false
                 self.lockerApplied = false
                 self.selectAddress.remove(at: 0)
                 self.selectshippingindex = -1
                 self.selectbillingindex = -1
                 self.shippingCost=0.0
                 self.address_lkpid=""
                 PaymentAddress.calculatePrice()
                 self.shippingaddress.reloadData()
            }
            ok.setValue(UIColor.black, forKey: "titleTextColor")
            cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
            alertController.addAction(cancel)
            alertController.addAction(ok)
            alertController.view.tintColor = .yellow
            self.present(alertController, animated: true, completion: nil)
            
        }
        }
        else if pickupApplied == true && self.lockerApplied == false
        {
            let alertController = UIAlertController(title: "", message: "Are you want to remove pickup location?", preferredStyle: .alert)
            
            let cancel = UIAlertAction(title: "No" , style: .default) { (_ action) in
                        //code here…
                   
                    }
            let ok = UIAlertAction(title: "Yes" , style: .default) { (_ action) in
                 //code here…
                 self.lockerCodeText.text = "Get Free Shipping with Locker Code"
                 self.is_locker_code = false
                 self.changeAddressLable.isHidden = false
                 self.pickupApplied = false
                 self.lockerApplied = false
                 self.selectAddress.remove(at: 0)
                 self.selectshippingindex = -1
                 self.selectbillingindex = -1
                 self.shippingCost=0.0
                 self.address_lkpid=""
                 PaymentAddress.calculatePrice()
                 self.shippingaddress.reloadData()
            }
            ok.setValue(UIColor.black, forKey: "titleTextColor")
            cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
            alertController.addAction(cancel)
            alertController.addAction(ok)
            alertController.view.tintColor = .yellow
            self.present(alertController, animated: true, completion: nil)
            
        }
        else if lockerApplied == true && pickupApplied == false
        {
            let alertController = UIAlertController(title: "", message: "Are you want to remove locker discount?", preferredStyle: .alert)
            
            let cancel = UIAlertAction(title: "No" , style: .default) { (_ action) in
                        //code here…
                   
                    }
            let ok = UIAlertAction(title: "Yes" , style: .default) { (_ action) in
                 //code here…
                self.lockerCodeText.text = "Get Free Shipping with Locker Code"
                 self.is_locker_code = false
                 self.changeAddressLable.isHidden = false
                 self.pickupApplied = false
                 self.lockerApplied = false
                 self.selectAddress.remove(at: 0)
                 self.selectshippingindex = -1
                 self.selectbillingindex = -1
                 self.shippingCost=0.0
                 self.address_lkpid=""
                 PaymentAddress.calculatePrice()
                 self.shippingaddress.reloadData()
            }
            ok.setValue(UIColor.black, forKey: "titleTextColor")
            cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
            alertController.addAction(cancel)
            alertController.addAction(ok)
            alertController.view.tintColor = .yellow
            self.present(alertController, animated: true, completion: nil)
            
            
        }
        
        shippingaddress.reloadData()
        
    }
    
    
    func sendLockerCode(lockerCode: String)
    {
        CustomLoader.instance.showLoaderView()
        let parameters: [String:Any] = ["user_locker_code":lockerCode]
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/apply_user_deal_lockercode/"
        print(parameters, url)
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            switch response.result {
            case .success(let data as [String:Any]):
                // Yeah! Hand response
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("locker res", jsonString)
                    if dict1["status"] as? String == "success"
                    {
                    if let lockerAddress = dict1["address_data"] as? NSDictionary
                    {
                        
                        if lockerAddress.count>0
                        {
                            self.view.makeToast("Locker code applied successfully", duration: 2.0, position: .center)
                            self.lockerCodeText.text = "Remove locker code"
                            self.lockerApplied=true
                            self.pickupApplied=false
                            var lockerDisscount = 0.0
                                let address = PaymentAddress.selectAddress as! AnyObject
                            
                            
                                PaymentAddress.selectAddress.removeAll()
                                PaymentAddress.selectAddress.append(lockerAddress)
                                for var i in 0..<address.count
                                {
                                    PaymentAddress.selectAddress.append(address[i])
                                }
                            if let shippingcost = dict1["shipping_off_value"] as? NSNumber
                            {
                                print("7673478345", shippingcost)
                                if self.selectAddress.count > 0
                                {
                                    if self.selectAddress[0]["shipping_charge"] is NSNull
                                    {}
                                    else
                                    {
                                        lockerDisscount = Double(Float((self.selectAddress[0]["shipping_charge"] as? NSNumber)!))
                                    }
                                }
                                if lockerDisscount > Double(shippingcost)
                                {
                                    self.shippingCost = lockerDisscount - Double(shippingcost)}
                                else
                                {
                                    self.shippingCost = 0.0
                                }
                                self.calculatePrice()
                            }
                            PaymentAddress.selectshippingindex = 0
                            PaymentAddress.selectbillingindex = 0
                                
                                print("address added", PaymentAddress.selectAddress)
                                if let data = dict1["address_data"] as? NSDictionary
                                {
                                    self.address_lkpid = "\(data["user_addressid"] as! NSNumber)"
                                }
                                PaymentAddress.lockerApplied = true
                                self.changeAddressLable.isHidden = true
                                PaymentAddress.pickupApplied = false
                                if let shippingcost = dict1["shipping_off_value"] as? NSNumber
                                {
                                    if self.shippingCost > Double(shippingcost)
                                    {
                                        self.shippingCost = self.shippingCost - Double(shippingcost)}
                                    else
                                    {
                                        self.shippingCost = 0.0
                                    }
                                    self.calculatePrice()
                                }
                            
                                PaymentAddress.shippingaddress.reloadData()
                                PaymentAddress.calculatePrice()
                            
                        }
                    }
                    }
                    else if dict1["status"] as? String == "failure"
                    {   self.view.makeToast(dict1["message"] as? String, duration: 2.0, position: .center)
                        
                    }
                    }
                    
                }
            case .failure(let error):
                //                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            default:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                print("default")
            }
            print("res", response)
            
        }
        }
        else
        {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    
    
    //masking mid char
    func hideMidChars(_ value: String) -> String {
        return String(value.enumerated().map { index, char in
            return [0, 1, value.count - 1, value.count - 2].contains(index) ? char : "*"
        })
    }
    
    
    
    //// Stripe payment generate
    func stripeGeneratePayment()
    {
        var parameters: [String:Any] = [String:Any]()
        if PaymentAddress.stripeToken == ""
        {
            parameters = ["gateway_paymentid":PaymentAddress.gateway_paymentid, "payment_orderid":PaymentAddress.RazorPaypayment_orderid,"payment_id":PaymentAddress.payment_id, "amount": PaymentAddress.amount, "orderid": PaymentAddress.orderid]
        }
        else
        {
            parameters = ["gateway_paymentid":PaymentAddress.stripeToken, "payment_orderid":PaymentAddress.Stripe_payment_orderid,"payment_id":PaymentAddress.Stripe_payment_id, "amount": PaymentAddress.Stripe_amount, "orderid": PaymentAddress.Stripe_orderid]
        }
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully generate payment", parameters)
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/generate_payment/"
        print(parameters, url)
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            switch response.result {
            case .success(let data as [String:Any]):
                // Yeah! Hand response
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                        print("res;;; pincode", jsonString)
                        if (dict1["status"] as? String)?.lowercased() == "success"
                        {
                            value1.Success()
                            self.dismiss(animated: true, completion: nil)
                            
                        }
                        else if dict1["status"] as? String == "failure"
                        {
                            self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .center)
                        }
                    }
                }
                if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    
                }
            case .failure(let error):
                //                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            default:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                print("default")
            }
            print(" generate payment", response)
            
        }
        
    }
    
}


extension String {
    /// Replace all characters except the first by a star.
    func starredWord() -> String {
        return String(characters.prefix(2) + characters.dropFirst().map { _ in "*" })
    }
    
    /// Star every "word" in a string.
    func starred() -> String {
        var result = ""
        self.enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) {
            (s, range, enclosingRange, _) in
            result +=
                // Append the substring preceeding the word ...
                self[enclosingRange.lowerBound..<range.lowerBound]
                // ... the starred word ...
                + (s?.starredWord() ?? "")
                // ... and the substring following the word.
                + self[range.upperBound..<enclosingRange.upperBound]
        }
        return result
    }
}
