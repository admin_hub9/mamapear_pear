

import Foundation
import UIKit
import Firebase
import FirebaseAuth

class Conversation {
    
    //MARK: Properties
    let user: User
    var lastMessage: Message
    public static var native = UserDefaults.standard
    
    
    //MARK: Methods
    
    public class func showConversations(completion: @escaping ([Conversation]) -> Swift.Void) {
        
    
         if let currentUserID = native.string(forKey: "firebaseid") as? String
        
         {
            let db = Firestore.firestore()
            var fromID = ""
            var roomid = [String]()
            db.collection("chatRooms").whereField("otherUserId", isEqualTo: currentUserID)
                .addSnapshotListener() { (querySnapshot, err) in
                    guard let snapshot = querySnapshot else {
//                        print("Error retreiving snapshots \(err!)")
                        return
                    }
                    
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){
                        
                        guard let receivedMessage: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                            
                    let id = receivedMessage["userId"]!
//                            print("id ---------", diff.document.documentID as String)
                    roomid.append(diff.document.documentID as String)
                    fromID = receivedMessage["userId"]! as! String
                     
                     var count = -1
                    
                   var conversations = [Conversation]()
                        var data: NSMutableArray = []
                    User.info(forUserID: currentUserID as! String, completion: { (user) in
                        let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, quotationdata: data)
                        let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
                        conversations.append(conversation)
                        count = count + 1
//                        conversation.lastMessage.downloadLastMessage(forLocation: fromID, roomid:user.id, completion: {
//                            completion(conversations)
//
//                        })
//                                print("conversation text", conversation.lastMessage)
                    })
                    
                    }
                if (diff.type == .modified)
                        {
                            
                            guard let receivedMessage: [String:Any] = diff.document.data()
                                else {
                                    return
                            }
                            
                            let id = receivedMessage["userId"]!
                            //                            print("id ---------", diff.document.documentID as String)
                            roomid.append(diff.document.documentID as String)
                            fromID = receivedMessage["userId"]! as! String
                            
                            var count = -1
                            
                            var conversations = [Conversation]()
                            var data: NSMutableArray = []
                            User.info(forUserID: currentUserID as! String, completion: { (user) in
                                let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, quotationdata: data)
                                let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
                                conversations.append(conversation)
                                count = count + 1
//                                conversation.lastMessage.downloadLastMessage(forLocation: fromID,roomid:user.id, completion: {
//                                    completion(conversations)
//                                })
                                //                                print("conversation text", conversation.lastMessage)
                            })
                    }
                    
                    }
        }
        db.collection("chatRooms").whereField("userId", isEqualTo: currentUserID)
            .addSnapshotListener() { (querySnapshot, err) in
                guard let snapshot = querySnapshot else {
//                    print("Error retreiving snapshots \(err!)")
                    return
                }
                
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){
                        
                        guard let receivedMessage: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        
                        let id = receivedMessage["otherUserId"]!
                        //                            print("id ---------", diff.document.documentID as String)
                        roomid.append(diff.document.documentID as String)
                        fromID = receivedMessage["otherUserId"]! as! String
                        
                        var count = -1
                        
                        var conversations = [Conversation]()
                        var data: NSMutableArray = []
                        User.info(forUserID: currentUserID as! String, completion: { (user) in
                            let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, quotationdata: data)
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
                            conversations.append(conversation)
                            count = count + 1
                            conversation.lastMessage.downloadLastMessage(forLocation: fromID, roomid:user.id, completion: {
                                completion(conversations)
                                
                            })
                            //                                print("conversation text", conversation.lastMessage)
                        })
                        
                    }
                    if (diff.type == .modified)
                    {
                        
                        guard let receivedMessage: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        
                        let id = receivedMessage["otherUserId"]!
                        //                            print("id ---------", diff.document.documentID as String)
                        roomid.append(diff.document.documentID as String)
                        fromID = receivedMessage["otherUserId"]! as! String
                        
                        var count = -1
                        
                        var conversations = [Conversation]()
                        var data: NSMutableArray = []
                        User.info(forUserID: currentUserID as! String, completion: { (user) in
                            let emptyMessage = Message.init(type: .text, content: "loading", owner: .sender, timestamp: 0, isRead: true, quotationdata: data)
                            let conversation = Conversation.init(user: user, lastMessage: emptyMessage)
                            conversations.append(conversation)
                            count = count + 1
                            conversation.lastMessage.downloadLastMessage(forLocation: fromID,roomid:user.id, completion: {
                                completion(conversations)
                            })
                            //                                print("conversation text", conversation.lastMessage)
                        })
                    }
                    
                }}}
        
    }
    
    //MARK: Inits
    init(user: User, lastMessage: Message) {
        self.user = user
        self.lastMessage = lastMessage
    }
}
