//
//  CreateQuotationViewController.swift
//  Hub9
//
//  Created by Deepak on 10/12/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView

class CreateQuotationViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    var native = UserDefaults.standard
    var variantData = [AnyObject]()
    var selectedColorIndex = 0
    var color = [String]()
    var colorID = [Int]()
    var innerVariantData = [[AnyObject]]()
    var variantImage = [String]()
    var variantid = [[Int]]()
    var quantity = [[Int]]()
    var Price = [[Int]]()
    var RealPrice = [[Int]]()
    var Size_ui = [[String]]()
    var variant_data: [[String: Any]] = []
    var chat_Variant_data: [[String: Any]] = []
    //    var checkVariantStock = [["variantid": Int(), "moq": Int(), "stock": Int()]]
    
    
    
    var followshop = 1
    var productid = ""
    var number = false
    
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var MOQ: UILabel!
    @IBOutlet weak var VariantCollection: UICollectionView!
    @IBOutlet weak var ProductCollection: UICollectionView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    
    
    
    @IBAction func CancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        dismiss(animated: true, completion: nil)
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 50
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 50
            }
        }
    }
    @IBAction func Cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.blurView.alpha = 1
        self.activity.startAnimating()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        variantData = BBProductDetails.ProductVariantData
        productname.text = BBProductDetails.ProductName
        if BBProductDetails.ProductImg.count > 0
        {
            let image = BBProductDetails.ProductImg[0] as! String
            let url = NSURL(string: image)
            if url != nil
            {
                DispatchQueue.main.async {
                    self.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                }
            }
        }
        
        if BBProductDetails.quotation == true
        {
            fetchVariant()
        }
        //
        // Do any additional setup after loading the view.
    }
    
    func fetchVariant()
    {
        innerVariantData.removeAll()
        color.removeAll()
        colorID.removeAll()
        variantid.removeAll()
        quantity.removeAll()
        Price.removeAll()
        RealPrice.removeAll()
        Size_ui.removeAll()
        if let product_variant_data = variantData as? [AnyObject]
        {
            
            for i in 0..<product_variant_data.count
            {
                debugPrint("color is:", variantData[i]["color_ui"] as! String)
                color.append(variantData[i]["color_ui"] as! String)
                colorID.append(variantData[i]["colorid"] as! Int)
                if let product_variant_size_data = product_variant_data[i]["product_variant_size_data"] as? [AnyObject]
                {
                    
                    
                    debugPrint("-09p", product_variant_size_data)
                    innerVariantData.insert(product_variant_size_data, at: i)
                    var addzero = [Int]()
                    var price = [Int]()
                    var varid = [Int]()
                    var productRealPrice = [Int]()
                    var size = [String]()
                    for var j in 0..<innerVariantData[i].count
                    {
                        addzero.append(0)
                        if product_variant_size_data[j]["bulk_price"] is NSNull
                        {
                            price.append(product_variant_size_data[j]["selling_price"] as! Int)
                        }
                        else
                        {
                            price.append(Int(product_variant_size_data[j]["bulk_price"] as! NSNumber))
                        }
                        
                        debugPrint(product_variant_size_data[j]["product_variantid"] as! Int,"variantid")
                        varid.append(product_variant_size_data[j]["product_variantid"] as! Int)
                        if product_variant_size_data[j]["bulk_price"] is NSNull
                        {
                            productRealPrice.append(product_variant_size_data[j]["selling_price"] as! Int)
                        }
                        else
                        {
                            productRealPrice.append(Int(product_variant_size_data[j]["bulk_price"] as! NSNumber))
                        }
                        size.append(product_variant_size_data[j]["size_ui"] as! String)
                    }
                    self.variantid.insert(varid, at: i)
                    self.quantity.insert(addzero, at: i)
                    self.Price.insert(price, at: i)
                    self.RealPrice.insert(productRealPrice, at: i)
                    self.Size_ui.insert(size, at: i)
                    
                }
            }
            self.blurView.alpha = 0
            self.activity.stopAnimating()
            self.VariantCollection.reloadData()
            self.ProductCollection.reloadData()
        }
    }
    
    
    @IBAction func sendQuotation(_ sender: Any) {
        //        if (quantity.text?.count)! > 0 && (price.text?.count)! > 0
        //        {
        self.blurView.alpha = 1
        self.activity.startAnimating()
        debugPrint("quantity", quantity)
        variant_data.removeAll()
        chat_Variant_data.removeAll()
        for i in 0..<quantity.count
        {
            
            debugPrint(quantity[i])
            for j in 0..<quantity[i].count
            {
                if quantity[i][j] != 0 && RealPrice[i][j] > 0
                {
                    variant_data.append(["product_variantid":variantid[i][j],"quantity":quantity[i][j],"requested_price":RealPrice[i][j]])
                    
                    
                    chat_Variant_data.append(["productid":variantid[i][j],
                                              "quantity":quantity[i][j],
                                              "requested_price":RealPrice[i][j],
                                              "real_price":Price[i][j],"image_url":BBProductDetails.ProductImg[i],"text_message":"Accept my quote","colorid":colorID[i],"color_ui":color[i],"url_productid":variantid[i][j],"size_ui":Size_ui[i][j],"product_name":BBProductDetails.ProductName])
                }
            }
        }
        
        debugPrint(variant_data)
        debugPrint(self.chat_Variant_data)
        sendQuot()
    }
    
    func sendQuot()
    {
        
        if variant_data.count > 0
        {
            
            self.blurView.alpha = 1
            self.activity.startAnimating()
            let userid = native.string(forKey: "userid")
            let token = self.native.string(forKey: "Token")!
            debugPrint("userid:::", userid)
            let number = native.string(forKey: "number")
            let userCode = self.native.string(forKey: "userCode")
            let parameters: [String:Any] = ["parent_product_data": ["productid": BBProductDetails.productid, "remark": "", "variant_data": variant_data]]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post",parameters )
            let b2burl = native.string(forKey: "b2burl")!
            Alamofire.request("\(b2burl)/quotation/create_quotation/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                debugPrint(response)
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    //                self.blurView.alpha = 0
                    //                self.activity.stopAnimating()
                    //                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                if let json = response.result.value {
                    debugPrint("JSON: \(json)")
                    let jsonString = "\(json)"
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        
                        switch response.result
                        {
                        case .failure(let error):
                            //                debugPrint(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                debugPrint("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                debugPrint(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                debugPrint("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                else if (dict1["response"]as AnyObject) as! String  == "success"
                                {
                                    self.blurView.alpha = 0
                                    self.activity.stopAnimating()
                                    let data = ["Question": ChatMessage.botQuestion, "BotAnswers": ChatMessage.botAnswers] as [String : Any]
                                    ChatMessage.composeMessage(type: .botJson, content: ChatMessage.botjsonContent)
                                    //                        ChatMessage.composeMessage(type: .text, content: ChatMessage.botAnswers[ChatMessage.botAnswers.count - 1] as! String)
                                    ChatMessage.composeMessage(type: .quotationJson, content: self.chat_Variant_data)
                                    ChatMessage.composeMessage(type: .text, content: "Please Accept My quotation")
                                    let appearance = SCLAlertView.SCLAppearance(
                                        showCloseButton: false
                                    )
                                    let alertView = SCLAlertView(appearance: appearance)
                                    alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                                        alertView.dismiss(animated: true, completion: nil)
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alertView.showInfo("Success", subTitle: "Quotation sent Successfully", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
                                    
                                }
                                else
                                {
                                    self.blurView.alpha = 0
                                    self.activity.stopAnimating()
                                    let appearance = SCLAlertView.SCLAppearance(
                                        showCloseButton: false
                                    )
                                    let alertView = SCLAlertView(appearance: appearance)
                                    alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                                        alertView.dismiss(animated: true, completion: nil)
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alertView.showInfo(dict1["status"]! as! String, subTitle: dict1["response"]! as! String, colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
                                    
                                }
                            }}}
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                    
                    
                    
                }
                
                
            }
        }
        else{
            self.blurView.alpha = 0
            self.activity.stopAnimating()
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showInfo("Alert", subTitle: "Please add quantity first ", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
            
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if collectionView == VariantCollection
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "color", for: indexPath) as! ProductColorCollectionViewCell
            
            self.productImage.image = nil
            debugPrint( BBProductDetails.ProductImg.count,  BBProductDetails.ProductImg)
            if BBProductDetails.ProductImg.count > indexPath.row
            {
                debugPrint(BBProductDetails.ProductImg[indexPath.row] as! String)
                let image = BBProductDetails.ProductImg[indexPath.row] as! String
                let url = NSURL(string: image)
                if url != nil{
                    DispatchQueue.main.async {
                        self.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            
            cell.color.textColor = UIColor.red
            
            selectedColorIndex = indexPath.row
        }
        if collectionView == ProductCollection
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "quotaioncell", for: indexPath) as! CreateQuotationCollectionViewCell
            
            debugPrint("textindex", indexPath.row)
            
            
        }
        VariantCollection.reloadData()
        ProductCollection.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == VariantCollection
        {
            debugPrint("color.count", self.color.count)
            return self.color.count
        }
        else if innerVariantData.count > 0
        {
            return innerVariantData[selectedColorIndex].count
        }
        else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cellToReturn = UICollectionViewCell()
        if collectionView == VariantCollection
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "color", for: indexPath) as! ProductColorCollectionViewCell
            
            cell.color.text = color[indexPath.row] as! String
            if selectedColorIndex == indexPath.row
            {
                cell.color.textColor = UIColor.red
                cell.Indicator.isHidden = false
            }
            else
            {
                cell.color.textColor = UIColor.black
                cell.Indicator.isHidden = true
            }
            cellToReturn = cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "quotaioncell", for: indexPath) as! CreateQuotationCollectionViewCell
            let data = innerVariantData[selectedColorIndex] as! [AnyObject]
            //            debugPrint("ioioioioio", data[indexPath.row]["currency_symbol"] as! Int)
            cell.productName.text = "\(data[indexPath.row]["material_ui"] as! String)/\(data[indexPath.row]["size_ui"] as! String)"
            debugPrint(self.quantity[selectedColorIndex])
            cell.quantitysegment.setTitle("\(self.quantity[selectedColorIndex][indexPath.row])", forSegmentAt: 1)
            let sortedViews = cell.quantitysegment.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
            cell.quantitysegment.selectedSegmentIndex = UISegmentedControlNoSegment
            cell.quantitysegment.tag = indexPath.row
            cell.quantitysegment.addTarget(self, action: #selector(updateUnit(sender:)), for: .valueChanged)
            
            cell.pricesegment.setTitle("\(self.RealPrice[selectedColorIndex][indexPath.row])", forSegmentAt: 1)
            let sortedViewsprice = cell.pricesegment.subviews.sorted( by: { $0.frame.origin.x < $1.frame.origin.x } )
            cell.pricesegment.selectedSegmentIndex = UISegmentedControlNoSegment
            cell.pricesegment.tag = indexPath.row
            cell.pricesegment.addTarget(self, action: #selector(updatePrice(sender:)), for: .valueChanged)
            
            cellToReturn = cell
        }
        return cellToReturn
    }
    
    
    
    @objc func updateUnit(sender: UISegmentedControl)
    {
        
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            let val = self.quantity[selectedColorIndex][sender.tag] as! Int
            if val > 0
            {
                quantity[selectedColorIndex][sender.tag] = val - 1
            }
            else
            {
                ProductCollection.reloadData()
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                    alertView.dismiss(animated: true, completion: nil)
                }
                alertView.showInfo("Alert", subTitle: "Number can not less than Zero", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
            }
            ProductCollection.reloadData()
        case 1:
            updateBulkQuantity(currentIndex: sender.tag)
            ProductCollection.reloadData()
            
            
        case 2:
            let val = self.quantity[selectedColorIndex][sender.tag] as! Int
            if val < 50000
            {
                self.quantity[selectedColorIndex][sender.tag] = val + 1
            }
            else
            {
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                    alertView.dismiss(animated: true, completion: nil)
                }
                alertView.showInfo("Alert", subTitle: "Quantity can't greater than 50000", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
            }
            
            ProductCollection.reloadData()
        default:
            break
        }
        
    }
    
    @objc func updatePrice(sender: UISegmentedControl)
    {
        switch sender.selectedSegmentIndex
        {
        case 0:
            
            //            if (0 < self.Price[selectedColorIndex][sender.tag])
            //            {
            let val = self.RealPrice[selectedColorIndex][sender.tag] as! Int
            if   val > 1
            {
                RealPrice[selectedColorIndex][sender.tag] = val - 1
                ProductCollection.reloadData()
            }
                
            else
            {
                ProductCollection.reloadData()
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                    alertView.dismiss(animated: true, completion: nil)
                }
                alertView.showInfo("Alert", subTitle: "Number can not be Zero", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
            }
            
            
            
        //            }
        case 1:
            updateBulkPrice(currentIndex: sender.tag)
            ProductCollection.reloadData()
            
            
        case 2:
            let val = self.RealPrice[selectedColorIndex][sender.tag] as! Int
            if (self.Price[selectedColorIndex][sender.tag] as! Int) > val
            {
                self.RealPrice[self.selectedColorIndex][sender.tag] = val + 1
            }
            else
            {
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                    alertView.dismiss(animated: true, completion: nil)
                }
                alertView.showInfo("Alert", subTitle: "Price can't be greater than Actual Price(\(self.Price[self.selectedColorIndex][sender.tag] as! Int)) and zero", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
            }
            ProductCollection.reloadData()
        default:
            break
        }
        
    }
    
    
    @objc func updateBulkQuantity(currentIndex: Int)
    {
        
        
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert = SCLAlertView(appearance: appearance)
        let txt = alert.addTextField("Enter Quantity")
        alert.addButton("Save", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            //            self.inventryBlur.alpha = 0
            debugPrint("Text value: \(txt.text!)")
            //            let check = self.quantity[self.selectedColorIndex][currentIndex] as! NSDictionary
            if self.checknumber(number: txt.text!) == true && txt.text! != ""
            {
                let val = Int(txt.text!)!
                if val < 50001
                {
                    self.quantity[self.selectedColorIndex][currentIndex] = Int(txt.text!)!
                }
                else
                {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                        alertView.dismiss(animated: true, completion: nil)
                    }
                    alertView.showInfo("Alert", subTitle: "Quantity can't greater than 50000", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
                }
                self.ProductCollection.reloadData()
            }
            else
            {
                alert.dismiss(animated: true, completion: nil)
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                    alertView.dismiss(animated: true, completion: nil)
                }
                alertView.showInfo("Alert", subTitle: "Please Enter valid number", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
            }
            //                self.inventryBlur.alpha = 0
            
            
        }
        alert.addButton("Cancel", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            //            self.inventryBlur.alpha = 0
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit("Update Quantity", subTitle: "Please enter valid Quantity value", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
    }
    
    
    
    @objc func updateBulkPrice(currentIndex: Int)
    {
        
        
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert = SCLAlertView(appearance: appearance)
        let txt = alert.addTextField("Enter Price")
        alert.addButton("Save", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            //            self.inventryBlur.alpha = 0
            debugPrint("Text value: \(txt.text!)")
            //            let check = self.quantity[self.selectedColorIndex][currentIndex] as! NSDictionary
            if self.checknumber(number: txt.text!) == true && txt.text! != ""
            {
                let val = Int(txt.text!)!
                if (self.Price[self.selectedColorIndex][currentIndex] as! Int) > val && val > 0
                {
                    self.RealPrice[self.selectedColorIndex][currentIndex] = val
                }
                else
                {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                        alertView.dismiss(animated: true, completion: nil)
                    }
                    alertView.showInfo("Alert", subTitle: "Price can't be greater than Actual Price(\(self.Price[self.selectedColorIndex][currentIndex] as! Int)) and zero", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
                }
                
                self.ProductCollection.reloadData()
            }
            else
            {
                alert.dismiss(animated: true, completion: nil)
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                    alertView.dismiss(animated: true, completion: nil)
                }
                alertView.showInfo("Alert", subTitle: "Please Enter valid number", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
            }
            //                self.inventryBlur.alpha = 0
            
            
        }
        alert.addButton("Cancel", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            //            self.inventryBlur.alpha = 0
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit("Update Price", subTitle: "Please enter valid Price", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
    }
    
    
    
    
    
    func checknumber(number: String) -> Bool
    {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: number)) else {
            return false
        }
        return true
    }
}
