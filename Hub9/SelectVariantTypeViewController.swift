////
////  SelectVariantTypeViewController.swift
////  Hub9
////
////  Created by Deepak on 7/19/18.
////  Copyright © 2018 Deepak. All rights reserved.
////
//
//import UIKit
//import Alamofire
//
//class SelectVariantTypeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
//    
//    var colorvalue = [Int]()
//    var color = ["Red", "Green", "Yellow", "Black", "Cyan", "Blue", "White","Orange"]
//    var selectedColor = [String]()
//    var selectedSize = [String]()
//    var selectedMaterial = [String]()
//    var size = ["S", "M", "L", "XL"]
//    var material = ["Fabric","Plastic","Wood","Fiber"]
//    var native = UserDefaults.standard
//    var variantType = 0
//    
//    
//    @IBAction func cancelButton(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
//    }
//    @IBAction func doneButton(_ sender: Any) {
//        if variantType == 0
//        {
//        UserDefaults.standard.set(selectedColor, forKey: "color")
//        native.synchronize()
//        }
//        else if variantType == 1
//        {
//            UserDefaults.standard.set(selectedSize, forKey: "size")
//            native.synchronize()
//        }
//        else if variantType == 2
//        {
//            UserDefaults.standard.set(selectedMaterial, forKey: "material")
//            native.synchronize()
//        }
//    self.dismiss(animated: true, completion: nil)
//    }
//    @IBOutlet var variantTypeTable: UITableView!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let value = native.string(forKey: "varianttype")!
//        variantType = Int(value)!
//            SelectSize()
//            SelectColor()
//            SelectMaterial()
//        
//        // Do any additional setup after loading the view.
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//        {
//         ///color
//        if variantType == 0
//        {
//        if let index = selectedColor.index(of: color[indexPath.row] as! String) {
//            print("found duplicate")
//        }
//        else{
//            selectedColor.append(color[indexPath.row])
//            }
//            print(selectedColor)
//        }
//            
//            ///size
//            else if variantType == 1
//        {
//            if let index = selectedSize.index(of: size[indexPath.row] as! String) {
//                print("found duplicate")
//            }
//            else{
//                selectedSize.append(size[indexPath.row])
//            }
//            print(selectedSize)
//            }
//        else if variantType == 2
//        {
//            if let index = selectedMaterial.index(of: material[indexPath.row] as! String) {
//                print("found duplicate")
//            }
//            else{
//                selectedMaterial.append(material[indexPath.row])
//            }
//            print(selectedMaterial)
//            }
//        
//    }
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        if variantType == 0
//        {
//        if let index = selectedColor.index(of: color[indexPath.row] as! String) {
//            
//            
//            selectedColor.remove(at: index)
//            
//            print("found duplicate")
//            print(selectedColor)
//        }
//        }
//        else if variantType == 1
//        {
//            if let index = selectedSize.index(of: size[indexPath.row] as! String) {
//                selectedSize.remove(at: index)
//                print("found duplicate")
//                print(selectedSize)
//        }
//    }
//        else if variantType == 2
//        {
//            if let index = selectedMaterial.index(of: material[indexPath.row] as! String) {
//                selectedMaterial.remove(at: index)
//                print("found duplicate")
//                print(selectedMaterial)
//            }
//        }
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 50
//    }
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        var ReturnCell = UITableViewCell()
//        if tableView == variantTypeTable
//        {
//            if variantType == 0
//            { return color.count
//            }
//            else if variantType == 1
//            {
//                return size.count
//            }
//            else if variantType == 2
//            {
//                return material.count
//            }
//        }
//        
//        return color.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var ReturnCell = UITableViewCell()
//        if tableView == variantTypeTable
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "addvariant", for: indexPath) as! AddVariantTableViewCell
//            if variantType == 0
//            {
//            cell.type.text = color[indexPath.row]
//            }
//            else if variantType == 1
//            {
//                cell.type.text = size[indexPath.row]
//            }
//            else if variantType == 2
//            {
//                cell.type.text = material[indexPath.row]
//            }
//            ReturnCell = cell
//        }
//        return ReturnCell
//    }
//    
//    func SelectColor()
//    { let token = self.native.string(forKey: "Token")!
//        if token != nil
//        {
//            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_color/") as! URL)
//            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
//            
//            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
//                //                                debugPrint(response)
//                print("response shop: \(response)")
//                let result = response.result
//                if let dict1 = result.value as? Dictionary<String, AnyObject>{
//                    let innerdict = dict1["data"] as! NSDictionary
//                    
//                    print("inner dict", innerdict)
//                    let data = innerdict
//                    print(" json", data)
//                    
//                    
//                    
//                }
//            }
//        }
//    }
//    func SelectSize()
//    { let token = self.native.string(forKey: "Token")!
//        let category = self.native.string(forKey: "subcategoryID")!
//        print(category,"category hgh id is...")
//        if token != nil && category != nil
//        {
//            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_size/ ?categoryid=\(category)") as! URL)
//            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
//            
//            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
//                //                                debugPrint(response)
//                print("response shop: \(response)")
//                let result = response.result
//                if let dict1 = result.value as? Dictionary<String, AnyObject>{
//                    let innerdict = dict1["data"] as! NSDictionary
//                    
//                    print("inner dict", innerdict)
//                    let data = innerdict
//                    print(" json", data)
//                    
//                    
//                    
//                }
//            }
//        }
//    }
//    func SelectMaterial()
//    { let token = self.native.string(forKey: "Token")!
//        let category = self.native.string(forKey: "subcategoryID")!
//        if token != nil && category != nil
//        {
//            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_material/ ?categoryid=\(category)") as! URL)
//            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
//            
//            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
//                //                                debugPrint(response)
//                print("response shop: \(response)")
//                let result = response.result
//                if let dict1 = result.value as? Dictionary<String, AnyObject>{
//                    let innerdict = dict1["data"] as! NSDictionary
//                    
//                    print("inner dict", innerdict)
//                    let data = innerdict
//                    print(" json", data)
//                    
//                    
//                    
//                }
//            }
//        }
//    }
//
//}
