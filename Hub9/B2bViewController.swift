//
//  B2bViewController.swift
//  Hub9
//
//  Created by Deepak on 11/22/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Firebase


class B2bViewController: UITabBarController, UITabBarControllerDelegate {
    
    
    var native = UserDefaults.standard
    var window = UIWindow.self
    var arrayOfImageNameForSelectedState = [""]
    var arrayOfImageNameForUnselectedState = [""]
    let tabBarCnt = UITabBarController()
    var selectIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let items = tabBar.items {
            items.enumerated().forEach { if $1 == item { print("your index is: \($0)") } }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if selectIndex == 3
        {
            BBProductDetails.senddataAddtocart = false
        }
        else
        {
            BBProductDetails.senddataAddtocart = true
        }
        let unselectedcolor = UIColor(red: 51/255, green: 51/255, blue: 52/255, alpha: 1)
        let selectedcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        UITabBar.appearance().tintColor = selectedcolor
        UITabBar.appearance().backgroundColor = UIColor.white
        UITabBar.appearance().unselectedItemTintColor = unselectedcolor
//        if let data = UserDefaults.standard.object(forKey: "changetype")
//        {
            native.set("2", forKey: "changetype")
            native.synchronize()
            let usertype1 = native.string(forKey: "changetype")!
            print(usertype1)
            if usertype1 == "2" && usertype1 == "3"
            {
                native.set("1", forKey:  "changetype")
                native.synchronize()
            }
            let usertype = native.string(forKey: "changetype")!
            
                let Itemindex0 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeData") as! HomeViewController
                Itemindex0.title = "Deal"
                Itemindex0.tabBarItem.image = UIImage(named: "Home")
                Itemindex0.tabBarItem.selectedImage = UIImage(named: "HomeFilled")
                Itemindex0.tabBarItem.isEnabled = true
                //                self.viewControllers?.insert(Itemindex0, at:0)
                /////
                //                self.viewControllers?.remove(at: 1)
                let Itemindex1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "feedpage") as! FeedViewController
                BBProductDetails.senddataAddtocart = false
                Itemindex1.title = "Review"
                Itemindex1.tabBarItem.image = UIImage(named: "review")
                Itemindex1.tabBarItem.selectedImage = UIImage(named: "ReviewFilled")
                Itemindex1.tabBarItem.isEnabled = true
                Itemindex1.navigationController?.isNavigationBarHidden = false
                //                self.viewControllers?.insert(Itemindex1, at:1)
                ////
                //                self.viewControllers?.remove(at: 2)
                //back to root view controller from navigation
                let Itemindex3 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "favouritepage") as! FavouriteViewController
                Itemindex3.title = "Saved"
//                if #available(iOS 13.0, *) {
//                    Itemindex3.tabBarItem.image = UIImage(systemName: "bookmark")
//                    Itemindex3.tabBarItem.selectedImage = UIImage(systemName: "bookmark.fill")
//                } else {
                    // Fallback on earlier versions
                    Itemindex3.tabBarItem.image = UIImage(named: "tabBookmark")
                    Itemindex3.tabBarItem.selectedImage = UIImage(named: "tabBookmarkFill")
//                }
                Itemindex3.tabBarItem.isEnabled = true
                
                let Itemindex2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Home") as! DashboardViewController
                Itemindex2.title = "Shop"
                Itemindex2.tabBarItem.image = UIImage(named: "cart")
                Itemindex2.tabBarItem.selectedImage = UIImage(named: "cartFilled")
                Itemindex2.tabBarItem.isEnabled = true
                //                self.viewControllers?.insert(Itemindex3, at:3)
                ////
                //                self.viewControllers?.remove(at: 4)
                
                let Itemindex4 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "more") as! SettingViewController
                Itemindex4.title = "Me"
                Itemindex4.tabBarItem.image = UIImage(named: "more")
                Itemindex4.tabBarItem.selectedImage = UIImage(named: "moreFilled")
                Itemindex4.tabBarItem.isEnabled = true//                self.viewControllers?.insert(Itemindex4, at:4)
                let controllerArray = [Itemindex0, Itemindex2, Itemindex1, Itemindex3, Itemindex4]
                tabBarCnt.viewControllers = controllerArray.map{ UINavigationController.init(rootViewController: $0)}
                tabBarCnt.tabBar.tintColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                tabBarCnt.selectedIndex = 2

                self.view.addSubview(tabBarCnt.view)
            
            
//        }
        
    }


    
    
   
}
