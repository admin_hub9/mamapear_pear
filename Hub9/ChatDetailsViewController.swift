//
//  ChatDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 10/10/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class ChatDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var array = ["Hello"]
    
    
    @IBOutlet weak var message: UITextField!
    @IBOutlet weak var sendMessage: UIButton!
    @IBOutlet weak var ChatTable: UITableView!
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let usercell = tableView.dequeueReusableCell(withIdentifier: "userchat", for: indexPath) as! UserChatTableViewCell
        let hieght = getTextViewHeight(text: array[indexPath.row], font: UIFont.systemFont(ofSize: 14))
        print(hieght)
        usercell.senderText.numberOfLines = Int(hieght / 30)
    
            usercell.usertext.text = array[indexPath.row]
        usercell.usertext.lineBreakMode = NSLineBreakMode.byWordWrapping
        let font = UIFont(name: "Helvetica", size: 14.0)
        usercell.usertext.font = font
        
        usercell.usertext.sizeToFit()
//            usercell.usertext.adjustsFontSizeToFitWidth = true
        
            usercell.senderText.isHidden = true
        
            return usercell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        print(getTextViewHeight(text: array[indexPath.row], font: UIFont.systemFont(ofSize: 14)))
        return getTextViewHeight(text: array[indexPath.row], font: UIFont.systemFont(ofSize: 14))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ChatTable.reloadData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendText(_ sender: Any) {
        
        if (message.text?.count)! > 0
        {
        array.append(message.text!)
            message.text = ""
        ChatTable.reloadData()
        }
    }
    

    func getTextViewHeight(text: String, font: UIFont) -> CGFloat {
        let textSize: CGSize = text.size(withAttributes: [NSAttributedStringKey.font: font])
        var height: CGFloat = (textSize.width / UIScreen.main.bounds.width) * font.pointSize
        
        var lineBreaks: CGFloat = 0
        if text.contains("\n") {
            for char in text.characters {
                if char == "\n" {
                    lineBreaks += (font.pointSize + 12)
                }
            }
        }
        
        height += lineBreaks
        return height + 60
    }

}
