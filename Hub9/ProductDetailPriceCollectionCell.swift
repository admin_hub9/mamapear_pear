//
//  ProductDetailPriceCollectionCell.swift
//  Hub9
//
//  Created by Deepak on 6/16/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class ProductDetailPriceCollectionCell: UICollectionViewCell {
    
    @IBOutlet var unit: UILabel!
    @IBOutlet var price: UILabel!
}
