//
//  BBRazorPayViewController.swift
//  Hub9
//
//  Created by Deepak on 3/29/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Razorpay
import Alamofire




class BBRazorPayViewController: UIViewController, RazorpayPaymentCompletionProtocol {
    
    
    
    var native = UserDefaults.standard

    private var razorpay: Razorpay!
    var razorpayTestKey = "rzp_test_8Ex2t6j1vfXYDy"
    var payment_orderid = ""
    var gateway_paymentid = ""
    var amount = ""
    var orderid = ""
    var paymentmode = 4
    static var price = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        razorpay = Razorpay.initWithKey(razorpayTestKey, andDelegate: self)
        orderid = ReviewPayment.RazorPayorderid
        gateway_paymentid = ReviewPayment.RazorPaypayment_orderid
        amount = ReviewPayment.RazorPayamount
        showPaymentForm()
        // Do any additional setup after loading the view.
    }
    

     func showPaymentForm(){
        let options: [String:Any] = [
            "name" : "BulkByte", //mandatory in paise
            "description": "purchase description",
            "order_id": "order_C65S0UL3GFQbt1"
            
        ]
        razorpay.open(options)
    }
    
    
    public func onPaymentError(_ code: Int32, description str: String){
        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    public func onPaymentSuccess(_ payment_id: String){
        
        gateway_paymentid = payment_id
        generatePayment()
        
        
    }
    
    
    
    func generatePayment()
    {
        
        
        let parameters: [String:Any] = ["gateway_paymentid":gateway_paymentid, "payment_orderid":payment_orderid, "amount": amount, "orderid": orderid]
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/generate_payment/"
        print(parameters, url)
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            if let json = response.result.value {
                let jsonString = "\(json)"
                print("res;;;;;", jsonString)
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        
    }
   

}
