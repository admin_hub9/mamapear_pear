//
//
//import UIKit
//
//
//class LandingVC: UIViewController {
//    
//    //MARK: Properties
//    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        get {
//            return UIInterfaceOrientationMask.portrait
//        }
//    }
//
//    //MARK: Push to relevant ViewController
//    func pushTo(viewController: ViewControllerType)  {
//        switch viewController {
//        case .conversations:
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Navigation") as! NavVC
//            self.present(vc, animated: false, completion: nil)
////        
//        }
//    }
//    
//    //MARK: Check if user is signed in or not
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        if let userInformation = UserDefaults.standard.dictionary(forKey: "userInformation") {
//            let email = userInformation["email"] as! String
//            let password = userInformation["password"] as! String
//            User.loginUser(withEmail: email, password: password, completion: { [weak weakSelf = self] (status) in
//                DispatchQueue.main.async {
//                    if status == true {
//                        weakSelf?.pushTo(viewController: .conversations)
//                    }
//                    weakSelf = nil
//                }
//            })
//        }
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//    }
//}
