//
//  dealAlertViewController.swift
//  Hub9
//
//  Created by Deepak on 8/6/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class dealAlertViewController: UIViewController {

    @IBOutlet weak var dealAlertContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        dealAlertContainer.layer.cornerRadius = 5
        dealAlertContainer.clipsToBounds = true
        dealAlertContainer.tag = 99
        // Do any additional setup after loading the view.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        if touch?.view?.tag == 99 {
            print("inside view")
            dismiss(animated: true, completion: nil)
        }
        else
        {
            
            print("outside view")
        }
        
        
    }
    
    @IBAction func gotitButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
