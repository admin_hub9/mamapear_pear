//
//  CompleteProfileController.swift
//  Hub9
//
//  Created by Deepak on 6/15/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire

class CompleteProfileController: UIViewController, UITextFieldDelegate {
   
    let native = UserDefaults.standard
    
    
    @IBOutlet var firstName: UITextField!
    
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var activity: UIActivityIndicatorView!
    @IBOutlet var countryName: UITextField!
    @IBOutlet var city: UITextField!
    @IBOutlet var pinCode: UITextField!
    @IBOutlet var state: UITextField!
    @IBOutlet var address2: UITextField!
    @IBOutlet var address1: UITextField!
    @IBOutlet var companyName: UITextField!
    @IBOutlet var shopName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var buyerView: UIView!
    @IBOutlet var shopUrl: UITextField!
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        firstName.resignFirstResponder()
        lastName.resignFirstResponder()
        shopName.resignFirstResponder()
        companyName.resignFirstResponder()
        state.resignFirstResponder()
        countryName.resignFirstResponder()
        address1.resignFirstResponder()
        address2.resignFirstResponder()
        pinCode.resignFirstResponder()
        city.resignFirstResponder()
        shopUrl.resignFirstResponder()
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 120
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 120
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(CompleteProfileController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CompleteProfileController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        blurView.alpha = 0
        firstName.delegate = self
        lastName.delegate = self
        countryName.delegate = self
        companyName.delegate = self
        address1.delegate = self
        address2.delegate = self
        shopName.delegate = self
        state.delegate = self
        pinCode.delegate = self
        city.delegate = self
        shopUrl.delegate = self
        activity.stopAnimating()
        let userCode = native.string(forKey: "userCode")
        if userCode == "1"
        {
            
            UIView.transition(with: buyerView, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0
                    self.buyerView.transform = CGAffineTransform(translationX: 0, y: -96)
                    self.shopName.isHidden = true
                    self.companyName.isHidden = true
                    self.shopUrl.isHidden = true
            }, completion: nil)
            
        }
        ///next button
        nextButton.layer.cornerRadius = nextButton.layer.frame.size.height/2
        nextButton.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 238/255.0, green: 90/255.0, blue: 95/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 241/255.0, green: 146/255.0, blue: 152/255.0, alpha: 1.00).cgColor
        
        nextButton.applyendGradient(colors: [loginstartColor, loginendColor])

        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func Nextbutton(_ sender: Any) {
        createProfile()
        
    }
    func isValidInput(Input:String) -> Bool {
        let RegEx = "\\A\\w{3,18}\\z"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    func validate(pincode: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = pincode.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  pincode == filtered
    }
    func validateStringandnumber(pincode: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
        let inputString = pincode.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  pincode == filtered
    }
    func validateString(pincode: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
        let inputString = pincode.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  pincode == filtered
    }
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.openURL(url as URL)
            }
        }
        return false
    }
    
    func validation() -> Bool {
        var valid: Bool = true
        let userCode = native.string(forKey: "userCode")
        if userCode != "1"
        {
            if (shopName.text!.count) < 3 || (shopName.text!.count) > 30{
                // change placeholder color to red color for textfield email-id
                shopName.text?.removeAll()
                shopName.attributedPlaceholder = NSAttributedString(string: " Enter correct Shop Name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                valid = false
            }
            if (companyName.text!.count) < 3 || (companyName.text!.count) > 30{
                // change placeholder color to red color for textfield email-id
                companyName.text?.removeAll()
                companyName.attributedPlaceholder = NSAttributedString(string: " Enter correct Compnay Name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                valid = false
            }
            if (shopUrl.text!.count) <= 3 && validateStringandnumber(pincode: shopUrl.text!) == false
            {
                // change placeholder color to red color for textfield email-id
                shopUrl.text?.removeAll()
                shopUrl.attributedPlaceholder = NSAttributedString(string: " Enter correct Shop URL", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
                valid = false
            }
        }
        
        if validateString(pincode: firstName.text!) == false || firstName.text?.count == 0{
            // change placeholder color to red color for textfield email-id
            firstName.text?.removeAll()
            firstName.attributedPlaceholder = NSAttributedString(string: " Enter correct name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if validateString(pincode: lastName.text!) == false{
            // change placeholder color to red color for textfield email-id
            lastName.text?.removeAll()
            lastName.attributedPlaceholder = NSAttributedString(string: " Enter correct name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        
        if (address1.text!.count) < 2 || (address1.text!.count) > 50{
            // change placeholder color to red color for textfield email-id
            address1.text?.removeAll()
            address1.attributedPlaceholder = NSAttributedString(string: " Enter correct Address", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if (address2.text!.count) < 2 || (address2.text!.count) > 50{
            // change placeholder color to red color for textfield email-id
            address2.text?.removeAll()
            address2.attributedPlaceholder = NSAttributedString(string: " Enter correct Address", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if validate(pincode: pinCode.text!) == false || pinCode.text?.count != 6{
            // change placeholder color to red color for textfield email-id
            pinCode.text?.removeAll()
            pinCode.attributedPlaceholder = NSAttributedString(string: " Enter correct Pincode", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if (state.text!.count) < 2 || validateString(pincode: state.text!) == false{
            // change placeholder color to red color for textfield email-id
            state.text?.removeAll()
            state.attributedPlaceholder = NSAttributedString(string: " Enter correct state", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if  validateString(pincode: city.text!) == false || (city.text!.count) < 2 {
            // change placeholder color to red color for textfield email-id
            city.text?.removeAll()
            city.attributedPlaceholder = NSAttributedString(string: " Enter correct city", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        if (countryName.text!.count) < 2 || validateString(pincode: countryName.text!) == false{
            // change placeholder color to red color for textfield email-id
            countryName.text?.removeAll()
            countryName.attributedPlaceholder = NSAttributedString(string: " Enter correct country name", attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
            valid = false
        }
        return valid
    }
    

}
