//
//  OrderReviewViewController.swift
//  Hub9
//
//  Created by Deepak on 31/03/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
import Alamofire
import Cosmos

class OrderReviewViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {

    var native = UserDefaults.standard
    
    var imagePicker: UIImagePickerController!
     var selectedImage = [UIImage]()
    var parentsku = ""
    var check = 1
    
    
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var rating: CosmosView!
    
    @IBOutlet weak var comment_text: UITextView!
    @IBOutlet weak var ReviewImage: UICollectionView!
    
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        comment_text.resignFirstResponder()
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y == 0{
                
                self.view.frame.origin.y -= 120
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += 120
            }

        }
    }
    
    @IBAction func uploadComment(_ sender: Any) {
        if rating.rating > 0 && (selectedImage.count > 0 || comment_text.text.count > 0)
        {
            commentData()
        }
        else
        {
            self.view.makeToast("please click on rating bar first!", duration: 2.0, position: .center)
        }
    }
    
    func commentData()
    {
        let token = self.native.string(forKey: "Token")!
            var fav = 0
                if token != ""
                {
                    CustomLoader.instance.showLoaderView()
                if self.native.object(forKey: "userid") != nil
                {
                    let userid = self.native.string(forKey: "userid")!
                var imageurl = [String]()
                for var i in 0..<self.selectedImage.count
                {
                    imageurl.append("\(userid)/\(productReview.order_itemid)/image\([i])/.jpeg")
                }
                //        print("product id:", id)
                    let parameters: [String:Any] = ["user_feedid":userid, "order_itemid": productReview.order_itemid, "review_data": ["rating": rating.rating, "review_text": comment_text.text, "image_url": imageurl]]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
            
                let b2burl = native.string(forKey: "b2burl")!
                var url = "\(b2burl)/users/post_review/"
               
                print("follow product", url, parameters)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    
                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                        print("res;;;;;", jsonString)
                        
                               
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                                
                        self.navigationController?.popViewController(animated: true)
                        
                        }
                    }}
                    }
                    }}
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
            
            
        }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            comment_text.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        selectedImage.append(UIImage(named:"car")!)
        ReviewImage.reloadData()
        // Do any additional setup after loading the view.
    }
    
    public func uploadPhotos()
        {
//            print("uploading", selectedImage, sku)
            for var i in 0..<selectedImage.count
        {
            let accessKey = "AKIAJYCW54FQC6MMHIDQ"
            let secretKey = "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"
            
            let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
            let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
            
            AWSServiceManager.default().defaultServiceConfiguration = configuration
            
            let S3BucketName = "b2b-user-review-image"
            let remoteName = "image\(i).png"
            let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
            //                let image = UIImage(named: "car")
            if let data = UIImageJPEGRepresentation(selectedImage[i], 0.2) {
                print(data.count)
            
            do {
                try data.write(to: fileURL)
            }
            catch {}
            }
            let uploadRequest = AWSS3TransferManagerUploadRequest()!
            uploadRequest.body = fileURL
            uploadRequest.key = remoteName
            uploadRequest.bucket = S3BucketName
            uploadRequest.contentType = "image/jpeg"
            uploadRequest.acl = .bucketOwnerRead
            
            let transferManager = AWSS3TransferManager.default()
            
            transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
                DispatchQueue.main.async {
                    
                }
                
                if let error = task.error {
                    print("Upload failed with error: (\(error.localizedDescription))")
                }
                
                if task.result != nil {
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                    if let absoluteString = publicURL?.absoluteString {
                        print("Uploaded to:\(absoluteString)")
                    }
                }
                
                return nil
            }
            }
        }
        
        
        
        
        @IBAction func clickPhotos(sender: UIButton) {
            check = 0
            let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert: UIAlertAction) in
                print("Take Photo Code")
                if(UIImagePickerController.isSourceTypeAvailable(.camera))
                {
                    let myPickerController = UIImagePickerController()
                    myPickerController.delegate = self
                    myPickerController.allowsEditing = true
                    myPickerController.sourceType = .camera
                    self.present(myPickerController, animated: true, completion: nil)
                }
                else
                {
                    let actionController: UIAlertController = UIAlertController(title: "Camera is not available",message: "", preferredStyle: .alert)
                    let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) {
                        action -> Void  in
                        //Just dismiss the action sheet
                    }
                    actionController.addAction(cancelAction)
                    self.present(actionController, animated: true, completion: nil)
                }}
            
            let gallerySelect = UIAlertAction(title: "Choose from Photos", style: .default) { (alert: UIAlertAction) in
                print("Gallery Select Code")
                self.imagePicker = UIImagePickerController()
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
            //            // Uncomment here and below at actionMenu to add a save photo action to the action list
            
            //            let savePhoto = UIAlertAction(title: "Save Photo", style: .Default, handler: { (alert: UIAlertAction) in
            //                print("Save photo code")
            //            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
                print("Cancelled")
            }
            
            actionMenu.addAction(takePhoto)
            actionMenu.addAction(gallerySelect)
            //          actionMenu.addAction(savePhoto)
            actionMenu.addAction(cancelAction)
            
            self.present(actionMenu, animated: true, completion: nil)
        }
        //#MARK: Image picker protocol functions
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            
            if info[UIImagePickerControllerOriginalImage] as? UIImage != nil {
    //            imagePicker.dismiss(animated: true, completion: nil)
                //            profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
                print("gallery Images:", info[UIImagePickerControllerOriginalImage] as? UIImage)
                let data = (info[UIImagePickerControllerOriginalImage] as? UIImage)
                
                if let image = info[UIImagePickerControllerEditedImage] as? UIImage
                {
                    selectedImage.append(image)
                }
                else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
                {
                    
                    selectedImage.append(image)
                    
                   print("selectedImage", selectedImage, selectedImage.count)
                }
                else
                {
                    print("Something went wrong")
                }
                
            }
            self.dismiss(animated: true, completion: nil)
            ReviewImage.reloadData()
           
            
        }
    func delete_variantImages(end: Int)
    {
        print(selectedImage, end)
     selectedImage.remove(at: end)
    }

}

extension OrderReviewViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    // collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return selectedImage.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addproduct", for: indexPath) as! AddProductCell
        print("index is :", cell.tag)
        if indexPath.row > 0
        {
            
            cell.images.layer.cornerRadius = 8
            cell.cancel.layer.cornerRadius = cell.cancel.layer.frame.size.height/2
            cell.cancel.clipsToBounds = true
            cell.images.image = selectedImage[indexPath.row]
            cell.images.clipsToBounds = true
            //        cell.images.image = UIImage(named: "car")
            cell.photos.isHidden = true
            cell.camera.isHidden = true
            cell.cancel.isHidden = false
            cell.clickPhotos.isHidden = true
            let button = UIButton(frame: CGRect())
            button.layer.cornerRadius = button.layer.bounds.size.width / 2
            button.layer.masksToBounds = true
            //cell.cancel.layer.cornerRadius = 0.5 * cancel.bounds.selectedSize.width
            cell.cancel.layer.masksToBounds = true
            
        }else{
            cell.images.layer.cornerRadius = 8
            cell.images.clipsToBounds = true
            cell.images.image = nil
            cell.photos.isHidden = false
            cell.camera.isHidden = false
            cell.cancel.isHidden = true
            cell.clickPhotos.isHidden = false
            cell.clickPhotos.tag = collectionView.tag
            cell.clickPhotos.addTarget(self, action: #selector(clickPhotos(sender:)), for: .touchUpInside)
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row > 0
        {
            delete_variantImages(end: indexPath.row)
            
//            print(selectedImage)
            ReviewImage.reloadData()
        }
        
    }
    
    
}

