//
//  SelectCategoryViewController.swift
//  Hub9
//
//  Created by Deepak on 12/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import  Alamofire


class SelectCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var categoryName = [String]()
    var SubCategoryName = [String]()
    var subCategoryId = [Int]()
    var native = UserDefaults.standard
    var indexNumber:NSInteger = -1
    
    

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var categoryTable: UITableView!
    
    
    @IBAction func CancelButton(_ sender: Any) {
        native.set("", forKey: "categoryname")
        native.synchronize()
        navigationController?.popViewController(animated: true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryTable.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchBar.text?.count)!  > 2
        {
            parseCategoryData()
        }
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategoryId.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        indexNumber = indexPath.row
        print(indexPath.row)
        self.native.set(categoryName[indexPath.row], forKey: "categoryname")
        self.native.set(subCategoryId[indexPath.row], forKey: "subCategoryId")
        self.native.synchronize()
        navigationController?.popViewController(animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == tableView
        {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectCategoryCell", for: indexPath) as! SelectCategoryTableViewCell
        cell.categoryName?.text = categoryName[indexPath.row]
        cell.subCategoryName?.text = SubCategoryName[indexPath.row]
        if indexNumber == indexPath.row{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
        
    }
    
    func parseCategoryData()
    {
        
        categoryName.removeAll()
        SubCategoryName.removeAll()
        subCategoryId.removeAll()
        let token = self.native.string(forKey: "Token")!
        print(token)
        if token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            var validateUrl =  "\(b2burl)/search/shop_product_category/?limit=50&offset=0&search_query=\(searchBar.text!.lowercased())"
            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            print("/////", validateUrl)
            var a = URLRequest(url: NSURL(string:validateUrl)! as URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response shop: \(response)")
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if dict1["status"] as! String == "success"
                    {
                    let innerdict = dict1["data"] as! [[String: Any]]

                    for var i in 0..<innerdict.count
                    {
                        self.categoryName.append(innerdict[i]["sub_category_name"]! as! String)
                        self.SubCategoryName.append(innerdict[i]["category_name"]! as! String)
                        self.subCategoryId.append(innerdict[i]["sub_categoryid"]! as! Int)
                    }
                    }

                    else
                    {

                    }
                        }
                    }
                }
                
                print(self.SubCategoryName)
                self.categoryTable.isHidden = false
                self.categoryTable.reloadData()
            }
            
        }
    }
}
