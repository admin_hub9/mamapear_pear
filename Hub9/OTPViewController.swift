//
//  OTPViewController.swift
//  Hub9
//
//  Created by Deepak on 6/13/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation




class OTPViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate
{
    
    let native = UserDefaults.standard
    var countdownTimer: Timer!
    var totalTime = 60
    var alamoFireManager : SessionManager?
    var window: UIWindow?
    
    //location
    var locationManager:CLLocationManager!
    var locality = ""
    var latitude = ""
    var longitude = ""
    
    
    @IBOutlet var countDownLabel: UILabel!
    @IBOutlet var verificationAlert: UILabel!
    @IBOutlet var verificationCode: UITextField!
    @IBOutlet var sendAgain: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    
   
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        verificationCode.resignFirstResponder()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y == 0{
                
                self.view.frame.origin.y -= 150
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y != 0{
                
                self.view.frame.origin.y += 150
            }

        }
    }
    
    // lat long
    //MARK: - location delegate methods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation

        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        latitude = "\(userLocation.coordinate.latitude)"
        longitude = "\(userLocation.coordinate.longitude)"

        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count>0{
                let placemark = placemarks![0]
                print(placemark.locality!)
                print(placemark.administrativeArea!)
                print(placemark.country!)

                self.locality = "\(placemark.locality!), \(placemark.administrativeArea!)"
            }
        }
        

    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled(){
            locationManager.startUpdatingLocation()
        }
        countDownLabel.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        verificationCode.delegate = self
        
        ///next button
        let number = native.string(forKey: "phone")
        if native.object(forKey: "lgoinType") != nil
        {
            if native.string(forKey: "lgoinType") == "Email"
            {
                 verificationAlert.text = "Enter One Time Password (OTP) that has been sent to your email \(number!)"
            }
            else
            {
              verificationAlert.text = "Enter One Time Password (OTP) that has been sent to your number \(number!)"
            }
        }
        
        nextButton.layer.cornerRadius = nextButton.layer.frame.size.height/2
        nextButton.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 239/255.0, green: 130/255.0, blue: 136/255.0, alpha: 1.00).cgColor
        
//        nextButton.applyendGradient(colors: [loginstartColor, loginendColor])
        ///textfields
        let leftView = UILabel(frame: CGRect(x: 10, y: 0, width: 7, height: 26))
        leftView.backgroundColor = .clear
        
        verificationCode.leftView = leftView
        verificationCode.leftViewMode = .always
        var frame: CGRect = verificationCode.frame
        frame.size.height = 40
        verificationCode.frame = frame
        verificationCode.layer.cornerRadius = verificationCode.layer.frame.height/2
        verificationCode.layer.borderWidth = 1
        verificationCode.layer.borderColor = UIColor.lightGray.cgColor
        verificationCode.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }
    
    
    func setUserLocation(lat: Double, long: Double, locationName: String)
            {
                let token = self.native.string(forKey: "Token")!
                
                    if token != ""
                    {
                    CustomLoader.instance.showLoaderView()
                    
                    let parameters: [String:Any] = ["latitude":lat, "longitude":long, "location_name": locationName]
                    let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                    let b2burl = native.string(forKey: "b2burl")!
                    print("Successfully post update location", parameters)
                    
                    Alamofire.request("\(b2burl)/users/set_user_location/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                        
                        
                        guard response.data != nil else { // can check byte count instead
                            let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        
                        if let json = response.result.value {
                            let jsonString = "\(json)"
                            let result = response.result
                            switch response.result
                            {
                            case .failure(let error):
                                //                print(error)
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let err = error as? URLError, err.code == .notConnectedToInternet {
                                    // Your device does not have internet connection!
                                    print("Your device does not have internet connection!")
                                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                    print(err)
                                }
                                else if error._code == NSURLErrorTimedOut {
                                    print("Request timeout!")
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                                }else {
                                    // other failures
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                                }
                                
                            case .success:
                            print(result)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                
                                if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                                {   CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                }
                                else
                                {
                                    CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    print(dict1["status"] as AnyObject)
                                    print(dict1["response"] as AnyObject)
                                    self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                                    
                                }
                            }
                            
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        
                        }}
                    }}
    
    @IBAction func termsCondition(_ sender: Any) {
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "termsAndCondition"))! as UIViewController
        self.present(rootPage, animated: false, completion: nil)
    }
    
    
    @IBAction func sendAgainOTP(_ sender: Any) {
        
        sendOtp()
        
    }
    func startTimer() {
        sendAgain.isEnabled = false
        sendAgain.tintColor = UIColor.lightGray
        countDownLabel.isHidden = false
        
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
//    sendOtp()
        
    }
    
    @objc func updateTime() {
        
        countDownLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
            print("......",totalTime)
        } else {
            print("over")
            endTimer()
            countDownLabel.isHidden = true
            sendAgain.isEnabled = true
            sendAgain.tintColor = UIColor.black
        }
    }
    func endTimer() {
        countdownTimer.invalidate()
    }

    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    func sendOtp()
    {
        startTimer()
        self.totalTime = 30
        CustomLoader.instance.showLoaderView()
        let userid = native.string(forKey: "userid")!
        print("userid:::", userid)
        let passcode = native.string(forKey: "passcode")!
        let number = native.string(forKey: "phone")
        let countrycode = native.string(forKey: "countryid")!
        let userCode = self.native.string(forKey: "userCode")
        let parameters: [String:Any]
        if native.object(forKey: "lgoinType") != nil
        {
            if native.string(forKey: "lgoinType") == "Email"
            {
             parameters  = ["username": number!,"country_code":passcode, "userid":userid, "account_typeid":userCode!, "countryid": countrycode, "signin_type": "Email"]
            }
            else
            {
                parameters = ["username": number!,"country_code":passcode, "userid":userid, "account_typeid":userCode!, "countryid": countrycode, "signin_type": "Phone"]
            }
        }
        else{
            
//            print("username", number!,"country_code", passcode, "userid", userid, "account_typeid", userCode!, "countryid", countrycode, "signin_type", "Phone")
            
          parameters = ["username": number!,"country_code":passcode, "userid":userid, "account_typeid":userCode!, "countryid": countrycode, "signin_type": "Phone"]
        }
           
        let header = ["Content-Type": "application/json"]
        let b2burl = native.string(forKey: "b2burl")!
        print("Successfully post", parameters)
        
        Alamofire.request("\(b2burl)/users/sendOtpVerification/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            print("\(b2burl)/users/sendOtpVerification/")
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            if let json = response.result.value {
                print("JSON: \(json)")
                let jsonString = "\(json)"
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    
                    if (dict1["status"]as AnyObject) as! String  == "Success"
                    {   CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.native.set(1, forKey: "otp_res")
                        self.native.synchronize()
                       
                        self.view.makeToast("OTP has been sent on your mobile number", duration: 2.0, position: .bottom)
                        
                        
                    }
                    else
                    {
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                        
                    }
                }
                
                
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
                
                }
                
            }
            
            
        }
    }
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func validation() -> Bool {
        var valid: Bool = true
        if ((verificationCode.text!.count) < 4){
            // change placeholder color to red color for textfield email-id
            verificationCode.text?.removeAll()
            verificationCode.attributedPlaceholder = NSAttributedString(string: " Please enter One Time Password (OTP).", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        
        return valid
    }
    
    ///convert json string to dict
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    ///parse api data
    @IBAction func nextButton(_ sender: Any) {
//            validation()
        if verificationCode.text?.count != 0
        {   CustomLoader.instance.showLoaderView()
            let userid = native.string(forKey: "userid")!
            print(userid)
            let parameters: [String:Any] = ["userid":userid ,"client_otp":verificationCode.text!]
            let header = ["Content-Type": "application/json"]
            let b2burl = native.string(forKey: "b2burl")!
            print("Successfully post", parameters)
            
            Alamofire.request("\(b2burl)/users/verifyOtp/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                if let json = response.result.value {
                    let jsonString = "\(json)"
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                    print(result)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        
                        if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                        {   CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.native.set(1, forKey: "OTP_response")
                            self.native.synchronize()
                            if (self.latitude != "" && self.longitude != "" && self.locality != "")
                        {
                            self.setUserLocation(lat: Double(self.latitude)!, long: Double(self.longitude)!, locationName: self.locality)
                        }
                            
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "InstructionScreen"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        else
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            print(dict1["status"] as AnyObject)
                            print(dict1["response"] as AnyObject)
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                            
                        }
                    }
                    
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                }
            }
            
        }
    }
    
    
}


