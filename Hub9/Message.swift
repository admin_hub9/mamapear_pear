


import Foundation
import UIKit
import Firebase

class Message {
    
    //MARK: Properties
    var owner: MessageOwner
    var type: MessageType
    var content: Any
    var timestamp: Int
    var isRead: Bool
    var image: UIImage?
    var quotation: NSMutableArray
    private var toID: String?
    private var fromID: String?
    var native = UserDefaults.standard
    
    //MARK: Methods
    class func downloadAllMessages(userID: String, chatRoomId: String, completion: @escaping (Message) -> Swift.Void) {
        let currentUserID = userID
        let db = Firestore.firestore()
//        print("printing messages....", userID)
       db.collection("messages").whereField("chatRoomId", isEqualTo: chatRoomId).addSnapshotListener() { (querySnapshot, err) in
            guard let snapshot = querySnapshot else {
//                print("Error retreiving snapshots \(err!)")
                return
            }
            
            snapshot.documentChanges.forEach { diff in
                if (diff.type == .added){
                    
                    guard let receivedMessage: [String:Any] = diff.document.data()
                        else {
                            return
                    }
//                    print("received Message", receivedMessage)
                    let messageId = receivedMessage["chatRoomId"] as? String
//                    if  messageId == "wOB9Gq4zIjcINlnOZaoK"
//                    {
                        let messageType = receivedMessage["type"] as! String
                        var content = ""
                        var fromID = ""
                        var timestamp = 0
                    var quotationtype: NSMutableArray = []
                    
                   
                        var type = MessageType.text
//                        print("type ", MessageType.text, receivedMessage)
                        switch messageType {
                        case "text":
                            type = .text
                            content = receivedMessage["content"] as! String
                            fromID = receivedMessage["sender"] as! String
                            timestamp = receivedMessage["messageTime"] as! Int
                        case "image":
                            type = .image
                            content = receivedMessage["content"] as! String
                            fromID = receivedMessage["sender"] as! String
                            timestamp = receivedMessage["messageTime"] as! Int
                        case "botJson":
                            type = .botJson
                            let array = receivedMessage["content"] as! NSMutableArray
                            print("bot array", array)
                            quotationtype.addObjects(from: receivedMessage["content"]  as! [Any])
//                            print("botresult", receivedMessage["content"] as! NSMutableArray)
                            content = (receivedMessage["content"] as! NSMutableArray).componentsJoined(by: "-")
                            fromID = receivedMessage["sender"] as! String
                            timestamp = receivedMessage["messageTime"] as! Int
                            
                        case "quotationJson":
                            
                            type = .quotationJson
                            let array = receivedMessage["content"] as! NSMutableArray
                            quotationtype.addObjects(from: receivedMessage["content"]  as! [Any])
//                            let data = array[0] as! [String:Any]
                            content = (receivedMessage["content"] as! NSMutableArray).componentsJoined(by: "-")
                            fromID = receivedMessage["sender"] as! String
                            timestamp = receivedMessage["messageTime"] as! Int
                        default: break
                        }
                    if MessageType.text == .quotationJson
                    {
                        if fromID != currentUserID {
                            
                            let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, quotationdata: quotationtype)
                            completion(message)
                        } else {
                            let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, quotationdata: quotationtype)
                            completion(message)
                        }
                    }
                    
                    else{
                        if fromID != currentUserID {
                            
                            let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true, quotationdata: quotationtype)
                            completion(message)
                        } else {
                            let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true, quotationdata: quotationtype)
                            completion(message)
                        }
                    }
//                    }
                }
                if(diff.type == .modified) {
//                    print("modified the document in firestore")
                }
            }
        }
        
    }
    
    func downloadImage(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
        if self.type == .image {
            let imageLink = self.content as! String
            let imageURL = URL.init(string: imageLink)
            URLSession.shared.dataTask(with: imageURL!, completionHandler: { (data, response, error) in
                if error == nil {
                    self.image = UIImage.init(data: data!)
                    completion(true, indexpathRow)
                }
            }).resume()
        }
    }
    
    class func markMessagesRead(forUserID: String)  {
        if let currentUserID = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserID).child("conversations").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let data = snapshot.value as! [String: String]
                    let location = data["location"]!
                    Database.database().reference().child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
                        if snap.exists() {
                            for item in snap.children {
                                let receivedMessage = (item as! DataSnapshot).value as! [String: Any]
                                let fromID = receivedMessage["fromID"] as! String
                                if fromID != currentUserID {
                                    Database.database().reference().child("conversations").child(location).child((item as! DataSnapshot).key).child("isRead").setValue(true)
                                }
                            }
                        }
                    })
                }
            })
        }
    }
    
    
    
    
    func downloadLastMessage(forLocation: String,roomid: String, completion: @escaping () -> Swift.Void) {
        var currentUserID = forLocation

        let db = Firestore.firestore()

//        print("last user message info.....", currentUserID, roomid)
        db.collection("chatRooms").whereField("id", isEqualTo: roomid)
           .addSnapshotListener() { (querySnapshot, err) in
                guard let snapshot = querySnapshot else {
//                    print("Error retreiving snapshots \(err!)")
                    return
                }
                
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){
                        
                        guard let receivedMessage: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        
                        print("jkjkjk", receivedMessage)
                        if receivedMessage["lastConversationText"] is NSNull
                        {
                            self.content = ""
                        }
                        else
                        {
                            if let data = receivedMessage["lastConversationTime"]{
                        self.content = receivedMessage["lastConversationText"]!
                        }
                            else
                            {
                                self.content = ""
                            }}
//                        print("content ---------", self.content)
                        if let data = receivedMessage["lastConversationTime"]
                        {
                        self.timestamp = receivedMessage["lastConversationTime"] as! Int
                        }
                        
//                        let messageType = userinfo["type"] as! String
                       if let data = receivedMessage["sendBy"]
                       {
                        let fromID = receivedMessage["sendBy"] as! String
                        if receivedMessage["isRead"] as? Bool ?? false {
                        
                        self.isRead = false
                        }
                        else
                        {
                         self.isRead = true
                        }
                        var type = MessageType.text

                        if currentUserID != fromID {
                            self.owner = .receiver
                        } else {
                            self.owner = .sender
                        }
                        completion()
                        }}
                }

        }
    }
    
    class func send(message: Message, fromID:String, toID: String,id: String, completion: @escaping (Bool) -> Swift.Void)  {
        
        switch message.type {
        case .quotationJson:
            let values = ["type": "quotationJson", "content": message.content, "sender": fromID, "chatRoomId":id, "id": "pdJPfocd8fOWvEwPAcoo", "messageTime": Date().millisecondsSince1970, "isRead": false] 
            
            print("::::::::value", values)
            Message.uploadMessage(withValues: values, completion: { (status) in
                completion(status)
            })
        case .image:
                let values = ["type": "image", "content": message.content, "sender": fromID, "chatRoomId": id, "messageTime": Date().millisecondsSince1970, "id":"pdJPfocd8fOWvEwPAcoo", "isRead": false] as [String : Any]
                Message.uploadMessage(withValues: values, completion: { (status) in
                    completion(status)
                })
            
        case .text:
            let values = ["type": "text", "content": message.content, "sender": fromID, "chatRoomId": id, "messageTime": Date().millisecondsSince1970, "id":"pdJPfocd8fOWvEwPAcoo", "isRead": false]
            Message.uploadMessage(withValues: values, completion: { (status) in
                completion(status)
            })
        case .botJson:
            let values = ["type": "botJson","Question": "Question", "content": message.content, "sender": fromID, "chatRoomId":id, "messageTime":  Date().millisecondsSince1970, "id":"pdJPfocd8fOWvEwPAcoo", "isRead": false]
            Message.uploadMessage(withValues: values, completion: { (status) in
                completion(status)
            })
        }
        
    }
    
    class func uploadMessage(withValues: [String: Any], completion: @escaping (Bool) -> Swift.Void) {
        
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection("messages").addDocument(data: withValues) { err in
            if let err = err {
//                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
                let id = withValues["chatRoomId"]!
                let text = withValues["content"]!
                let sendby = withValues["sender"]!
                let messagetime = withValues["messageTime"]!
                let updateID: [String: Any] = ["lastConversationText": text, "lastConversationTime": messagetime, "sendBy": sendby ]
                let citiesRef = db.collection("chatRooms")
                citiesRef.document(id as! String).updateData(updateID)
                {err in
                    if let err = err {
//                        print("Error adding document: \(err)")
                    } else {
//                        print("Document updated with ID: \(ref!.documentID)")
                        
                        
                    }
                    
                    
                }
            }
        }
        
        completion(true)
    }
    
    //MARK: Inits
    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int, isRead: Bool, quotationdata: NSMutableArray) {
        self.type = type
        self.content = content
        self.owner = owner
        self.timestamp = timestamp
        self.isRead = isRead
        self.quotation = quotationdata
    }
}




extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
