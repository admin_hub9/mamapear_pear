//
//  InformationAlertViewController.swift
//  Hub9
//
//  Created by Deepak on 8/7/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class InformationAlertViewController: UIViewController {

    
    
    
    @IBOutlet weak var alertContainer: UIView!
    @IBOutlet weak var headerText: UILabel!
    @IBOutlet weak var alertDetails: UILabel!
    @IBOutlet weak var gotIt: UIButton!
    
    /// alert 1- priceGarantee
    /// alert 2- retailText
    /// alert 3- shippingInfo
    var native = UserDefaults.standard
    var alert = 1
    
    var priceGarantee = "Bulkbyte is dedicated to offer the best value to our customers. We price match at the time of purchase if you find the same item at a lower price from any online retailer."
    var retailText = "Bulkbyte sells bulk quality products -- the kind you’d find at your local wholesale market. The bigger the quantity and so are the savings. We calculate your savings by comparing the price per unit of a product at retail stores vs. the price per unit of that same item at Bulkbyte"
    var shippingInfo = "Shipping insurance offers premium protection and safety for your valuable items during shipping. We will reship immediately at no extra charge if it's reported as lost."
    override func viewDidLoad() {
        super.viewDidLoad()
        if native.string(forKey: "AlertInfo")! != nil
        {
        alert = Int(native.string(forKey: "AlertInfo")!)!
            print("alertdetails", alert)
        if alert == 1
        {
            headerText.text = "Price Match Gaurantee"
            alertDetails.text = self.priceGarantee
        }
        else if alert == 2
        {
            headerText.text = "How You Save at BulkByte?"
            alertDetails.text = self.retailText
        }
        else if alert == 3
        {
            headerText.text = "Shipping Insurance Policy"
            alertDetails.text = self.shippingInfo
        }
        }
        // Do any additional setup after loading the view.
        alertContainer.layer.cornerRadius = 5
        alertContainer.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        
//        dismiss(animated: true, completion: nil)
//    }

    @IBAction func GotItButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
