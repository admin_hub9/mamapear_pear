//
//  AllListingViewController.swift
//  Hub9
//
//  Created by Deepak on 6/25/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire

class AllListingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    
    let native = UserDefaults.standard
    var ProductName = [AnyObject]()
    var ProductStatus = [AnyObject]()
    var ProductMOQ = [AnyObject]()
    var ProductImage = [AnyObject]()
    var ProductSKU = [AnyObject]()
    var ProductID = [AnyObject]()
    var currntProID = -1
    var ProductImg = [AnyObject]()
    var Categoryname = [AnyObject]()
    var Categoryid = [AnyObject]()
    var status = [String]()
    var stocks = [Int]()
    var Minstocks = [Int]()
    var Maxstocks = [Int]()
    
    var Brands = [String]()
    var favouriteIndex = 0
    var selectPfiletr = "category"
    var category_data = [AnyObject]()
    var brand_data = [AnyObject]()
    var status1 = [AnyObject]()
    var stock_min = 0
    var stock_max = 0
    var limit = 15
    var offset = 0
    
    var Img = [AnyObject]()
    var Pfilterdata = ["Categories","Brands","Status","Stocks"]
    @IBOutlet var uploadButton: UIButton!
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var filterButton: UIButton!
    @IBOutlet var filterView: UIView!
    @IBOutlet var searchbar: UIView!
    @IBOutlet var listingTable: UITableView!
    @IBOutlet var searchOption: UISearchBar!
    @IBOutlet var filterContainer: UIView!
    @IBOutlet var PfilterTable: UITableView!
    @IBOutlet var cfilterTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        native.set("-1", forKey: "ProductIDforEditing")
        native.synchronize()
        Brands = ["Flipkart", "Snapdeal", "Mintra", "Amazon"]
        Minstocks = [0,2,4,6,8]
        Maxstocks = [2,4,6,8,10]
        status = ["Active","Offline"]
        searchOption.delegate = self
        listingTable.tableFooterView = UIView()
        uploadButton.layer.cornerRadius = uploadButton.layer.frame.size.height/2
        uploadButton.clipsToBounds = true
        self.PfilterTable.separatorStyle = .none
        self.PfilterTable.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        parseData()
    }
    func parseData()
    { let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/product/get_seller_listing/?limit=\(limit)&offset=\(offset)&category_data=\(category_data)&brand_data=&\(brand_data)&stock_min=\(stock_min)&stock_max=\(stock_max)&status\(status1)"
            ProductID.removeAll()
            //            debugPrint("url is", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            let alertController = UIAlertController(title: "Alert", message: "Invalid Token Found", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                NSLog("OK Pressed")
                                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "root"))! as UIViewController
                                
                                self.present(editPage, animated: false, completion: nil)
                            }
                            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                                UIAlertAction in
                                NSLog("Cancel Pressed")
                            }
                            
                            // Add the actions
                            alertController.addAction(okAction)
                            alertController.addAction(cancelAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                    else
                    {
                        if let innerdict1 = dict1["category_data"]! as? [AnyObject]
                        {for i in 0..<innerdict1.count {
                            //                        debugPrint("category name", innerdict1[i]["category_name"]! as Any)
                            //                        debugPrint("category name", innerdict1[i]["categoryid"]! as Any)
                            self.Categoryname.insert(innerdict1[i]["category_name"]! as AnyObject, at: i)
                            self.Categoryid.insert(innerdict1[i]["categoryid"]! as AnyObject, at: i)
                            }
                            let status = dict1["status_data"]! as! [AnyObject]
                            for i in 0..<status.count {
                                //                        debugPrint("status_data", status[i]["product_status_lkpid"]! as Any)
                                //                        debugPrint("status_data", status[i]["status"]! as Any)
                                //                        self.Categoryname.insert(status[i]["product_status_lkpid"]! as AnyObject, at: i)
                                //                        self.Categoryid.insert(status[i]["status"]! as AnyObject, at: i)
                            }
                            let stock = dict1["stock_data"]! as! [AnyObject]
                            
                            //                        debugPrint("stock_data", stock["max"]! as Any)
                            //                        debugPrint("stock_data", stock["min"]! as Any)
                            //                        self.Categoryname.insert(stock[i]["max"]! as AnyObject, at: i)
                            //                        self.Categoryid.insert(stock[i]["min"]! as AnyObject, at: i)
                            
                            if let innerdict = dict1["data"]{
                                self.ProductImg = innerdict as! [AnyObject]
                                for i in 0..<self.ProductImg.count {
                                    let name = self.ProductImg[i]["parent_product_name"]
                                    //                            self.filteredName.insert(name as AnyObject, at: i)
                                    let image = self.ProductImg[i]["parent_product_image_url"]!
                                    //                            debugPrint(image)
                                    let id = self.ProductImg[i]["parent_productid"]
                                    //                            debugPrint(self.ProductImg[i]["parent_product_sku"])
                                    self.ProductID.insert(id as AnyObject, at: i)
                                    self.Img.append(image as AnyObject)
                                }
                                //                        for var i in 0..<self.ProductImg.count {
                                //                            debugPrint("product image", self.Img[i]["image_url"]!)
                                //                        }
                                //                        debugPrint("product:", self.ProductID)
                                //                        debugPrint("category name", self.Categoryname, self.Categoryid)
                            }
                        }
                        if self.ProductID.count > 0
                        {
                            self.listingTable.showEmptyListMessage("")
                        }
                        else
                        {
                            self.listingTable.showEmptyListMessage("Your product list is empty ")
                        }
                        self.listingTable.reloadData()
                        self.cfilterTable.reloadData()
                        self.PfilterTable.reloadData()
                        self.PfilterTable.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
                        
                    }
                    
                }
            }
            
        }
        
    }
    ///upload button
    @IBAction func uploadProductButton(_ sender: Any) {
        let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let SingleProduct = UIAlertAction(title: "Add Single Product", style: .default) { (alert: UIAlertAction) in
            //            debugPrint("Add Single Product")
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "singleProduct"))! as UIViewController
            
            self.present(editPage, animated: false, completion: nil)
            
        }
        
        let Variant = UIAlertAction(title: "Add Variant", style: .default) { (alert: UIAlertAction) in
            //            debugPrint("Add Variant")
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "addVariant"))! as UIViewController
            
            self.present(editPage, animated: false, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
            //            debugPrint("Cancelled")
        }
        
        actionMenu.addAction(SingleProduct)
        actionMenu.addAction(Variant)
        actionMenu.addAction(cancelAction)
        
        self.present(actionMenu, animated: true, completion: nil)
    }
    
    
    //search button click
    @IBAction func searchButton(_ sender: Any) {
        //        debugPrint("search")
        self.filterView.isHidden=true
        self.searchbar.isHidden=false
        //        UIView.transition(with: searchbar, duration: 0.4, options: .beginFromCurrentState, animations:
        //            {
        //                //                                self.blueView.alpha = 0
        //                self.searchbar.transform = CGAffineTransform(translationX: 0, y: 0)
        //
        //
        //        }, completion: nil)
        
    }
    
    //filter button click
    @IBAction func filterButton(_ sender: Any) {
        UIView.transition(with: filterContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.filterContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                self.filterContainer.isHidden = false
        }, completion: nil)
    }
    @IBAction func cancelFilter(_ sender: Any) {
        UIView.transition(with: filterContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.filterContainer.transform = CGAffineTransform(translationX: 0, y: 360)
                self.filterContainer.isHidden = true
        }, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == PfilterTable
        {
            return 40
            
        }
        else if tableView == cfilterTable
        {
            return 40
        }
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == PfilterTable
        {
            return Pfilterdata.count
        }
        else if tableView == listingTable
        {
            return self.ProductImg.count
        }
        else if tableView == cfilterTable
        {
            //            debugPrint("ndsfksbf", Categoryname.count)
            //            return Categoryid.count
            if selectPfiletr.contains("category")
            {
                return Categoryid.count
            }
            else if selectPfiletr.contains("brands")
            {
                return Brands.count
            }
            else if selectPfiletr.contains("status")
            {
                return status.count
            }
            else if selectPfiletr.contains("stock")
            {
                return Minstocks.count
            }
        }
        
        return 10
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == PfilterTable
        {   debugPrint("parent index is", indexPath.row)
            if indexPath.row == 0
            {
                //                debugPrint("category")
                selectPfiletr = "category"
            }
            if indexPath.row == 1
            {
                //                debugPrint("brands")
                selectPfiletr = "brands"
            }
            if indexPath.row == 2
            {
                //                debugPrint("status")
                selectPfiletr = "status"
            }
            if indexPath.row == 3
            {
                //                debugPrint("stock")
                selectPfiletr = "stock"
            }
            cfilterTable.reloadData()
        }
        if tableView == cfilterTable
        {
            if selectPfiletr.contains("category")
            {
                category_data.insert(Categoryname[indexPath.row] as! AnyObject, at: 0)
            }
            else if selectPfiletr.contains("brands")
            {
                brand_data.insert(Brands[indexPath.row] as! AnyObject, at: 0)
            }
            else if selectPfiletr.contains("status")
            {
                status1.insert(status[indexPath.row] as! AnyObject, at: 0)
            }
            else if selectPfiletr.contains("stock")
            {
                stock_min = Minstocks[indexPath.row]
                stock_max = Maxstocks[indexPath.row]
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReturnCell = UITableViewCell()
        if tableView == listingTable
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "alllisting", for: indexPath) as! AllListingTableViewCell
            cell.editButton.addTarget(self, action: #selector(sumbitOrContinue), for: .touchUpInside)
            cell.editButton.tag = indexPath.row
            cell.editButton.isEnabled = false
            cell.name.text = self.ProductImg[indexPath.row]["parent_product_name"] as? String
            cell.moq.text = "Stock \(self.ProductImg[indexPath.row]["total_stock"] as! Int)"
            cell.sku.text = self.ProductImg[indexPath.row]["parent_product_sku"] as? String
            
            cell.stauts.text = self.ProductImg[indexPath.row]["status"] as? String
            if self.ProductImg[indexPath.row]["status"] as? String == "Draft"
            {
                cell.stauts.textColor = UIColor.red
            }
            
            if self.ProductImg[indexPath.row]["parent_product_image_url"] is NSNull
            {}
            else
            {
                
                var image1 = self.ProductImg[indexPath.row]["parent_product_image_url"] as! [AnyObject]
                
                if image1.count > 0
                {
                    let url = NSURL(string:image1[0] as! String )
                    debugPrint("=====", url!)
                    if verifyUrl(urlString: (image1[0] as! String)) == false
                    {
                        debugPrint("invalid url")
                        cell.icon.image = nil
                        
                    }
                    else{
                        DispatchQueue.main.async {
                            cell.icon.sd_setImage(with: URL(string: image1[0] as! String), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
                else{
                    cell.icon.image = UIImage(named: "thumbnail")
                }
            }
            ReturnCell = cell
        }
        else if tableView == PfilterTable
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "filter", for: indexPath) as! FilterTableViewCell
            cell.name.text = Pfilterdata[indexPath.row]
            
            ReturnCell = cell
        }
        else if tableView == cfilterTable{
            let cell = tableView.dequeueReusableCell(withIdentifier: "subfilter", for: indexPath) as! SubFilterTableViewCell
            
            
            
            if selectPfiletr.contains("category")
            {
                cell.name.text = Categoryname[indexPath.row] as? String
                cell.name.frame.size.width = 40
                if category_data.count > 0{
                    if category_data[0] as! String == Categoryname[indexPath.row] as! String{
                        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                    }}
            }
            else if selectPfiletr.contains("brands")
            {   cell.mark.image = UIImage(named: "checkmark")
                cell.name.text = Brands[indexPath.row]
                cell.name.frame.size.width = 40
                if brand_data.count > 0
                {
                    //                    debugPrint("bhc")
                    if brand_data[0] as? String == Brands[indexPath.row] as? String
                    {   tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                        //                    debugPrint("dfhb")
                        cell.mark.image = UIImage(named: "checkmark")
                        cell.setSelected(true, animated: true)
                    }
                }
            }
            else if selectPfiletr.contains("status")
            {
                cell.name.text = status[indexPath.row]
                cell.name.frame.size.width = 40
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            else if selectPfiletr.contains("stock")
            {
                cell.name.text = "\(Minstocks[indexPath.row]) - \(Maxstocks[indexPath.row])"
                cell.name.frame.size.width = 40
                tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            
            ReturnCell = cell
            
            
        }
        return ReturnCell
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastData = self.ProductID.count - 1
        if indexPath.row  == lastData {
            
            debugPrint("load more")
            
        }
        else{
            debugPrint("index", indexPath.row , lastData)
        }
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIView.transition(with: searchbar, duration: 0.6, options: .beginFromCurrentState, animations:
            {
                //  self.blueView.alpha = 0
                //                self.searchbar.transform = CGAffineTransform(translationX: 360, y: 0)
                searchBar.endEditing(true)
                self.searchbar.isHidden=true
                self.filterView.isHidden=false
        }, completion: nil)
    }
    @objc func sumbitOrContinue(sender: UIButton!) {
        let tag = sender.tag
        //        debugPrint(tag)
        native.set(ProductID[tag], forKey: "ProductIDforEditing")
        native.synchronize()
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "addVariant"))! as UIViewController
        
        self.present(editPage, animated: false, completion: nil)
    }
    @IBAction func ApplyFilter(_ sender: Any) {
        parseData()
        UIView.transition(with: filterContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.filterContainer.transform = CGAffineTransform(translationX: 0, y: 360)
                self.filterContainer.isHidden = true
        }, completion: nil)
    }
    
}
