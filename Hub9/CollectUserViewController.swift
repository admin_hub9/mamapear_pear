//
//  CollectUserViewController.swift
//  Hub9
//
//  Created by Deepak on 12/10/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit


var screen = CollectUserViewController()
class CollectUserViewController: UIViewController {
    
    
    @IBOutlet weak var pageindicator: UIPageControl!
    @IBOutlet weak var shopLocation: UIView!
    @IBOutlet weak var shopInterest: UIView!
    

    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        screen = self
        shopLocation.isHidden = true
        shopInterest.isHidden = false
        pageindicator.isHidden = true
        pageindicator.currentPage = 1
        // Do any additional setup after loading the view.
    }
    
    public func nextButton() {
        
        shopLocation.isHidden = true
        shopInterest.isHidden = false
        pageindicator.currentPage = 1
        
        
    }
    
    
  

}
