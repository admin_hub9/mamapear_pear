//
//  AddNewAddressViewController.swift
//  Hub9
//
//  Created by Deepak on 10/16/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import RSSelectionMenu
import Alamofire
import MapKit
import GoogleMaps
import GooglePlaces


var useraddress = AddNewAddressViewController()

class AddNewAddressViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var native = UserDefaults.standard
//    var locationManager: CLLocationManager!
    var selectedCitylkpd = -1
    
    var simpleSelectedArray = [String]()
    var countryName = [String]()
    var countrylkid = [String]()
    var cityName = [String]()
    var citylkid = [String]()
    var stateName = [String]()
    var statelkid = [String]()
    var statedata = [AnyObject]()
    var citydata = [AnyObject]()
    var selectedcountryName = ""
    var selectedcountrylkid = 0
    var selectedcityName = ""
    var selectedcitylkid = 0
    var selectedstateName = ""
    var selectedstatelkid = 0
    var selectedaddresslkpid = 0
    var selectedaddresstype = ""
    var isbillingon = false
    var isshippingon = false
    var user_AddressId = 0
    var countryid = 0
    var cityName1 = ""
    var stateName1 = ""
    var region = ""
    var is_validated = false
    
    
    
    var grayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 0.2).cgColor
    var redColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 0.20).cgColor
    var redtitleColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1.0)
    
    
    @IBOutlet weak var searchPlaceholder: UILabel!
    @IBOutlet weak var searchlocationTitle: UILabel!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var locationButton: UIButton!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var city: UITextField!
    
    @IBOutlet weak var addressScrollView: UIScrollView!
    @IBOutlet weak var landmark: UITextField!
    
    
    
    @IBOutlet weak var pincode: UITextField!
    @IBOutlet weak var contact: UITextField!
    
    @IBOutlet weak var home: UIButton!
    @IBOutlet weak var work: UIButton!
    @IBOutlet weak var other: UIButton!
    @IBOutlet weak var addressButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var address2: UITextField!
    
    
    
    // top constraint
    
    @IBOutlet weak var AreaTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var saveButtonConstraint: NSLayoutConstraint!
    
    
    var GoogleMapsAPIServerKey = ""

   
    
    
    
    @IBAction func searchAddress(_ sender: Any) {
        
        var country_code = ""
        if native.object(forKey: "country_code") != nil {
            
            country_code = native.string(forKey: "country_code")!
            
        }
        if native.object(forKey: "countryid") != nil {
           
            self.countryid = Int(native.string(forKey: "countryid")!)!
            
        }
        
        
        print("country_code", country_code)
        GoogleMapsAPIServerKey = "AIzaSyAOycE9VXNxb9_cvKrsKr6KT7Am9jxS2BQ"
        if GoogleMapsAPIServerKey != "" && country_code != ""
        {
            GMSServices.provideAPIKey(GoogleMapsAPIServerKey)
            GMSPlacesClient.provideAPIKey(GoogleMapsAPIServerKey)
            
            let autocompletecontroller = GMSAutocompleteViewController()
            
            autocompletecontroller.delegate = self
            
            let filter = GMSAutocompleteFilter()
//            filter.type = .address  //suitable filter type
            filter.country = country_code  //appropriate country code
            autocompletecontroller.autocompleteFilter = filter
            
            present(autocompletecontroller, animated: true, completion: nil)
            
            // changes the color of the sugested places
            autocompletecontroller.primaryTextColor = UIColor.black
            autocompletecontroller.secondaryTextColor = UIColor.gray
            UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = UIColor.black

        }
        else
        {
           self.view.makeToast("Please try later!", duration: 2.0, position: .center)
        }
    }
   

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.delegate = self
        landmark.delegate = self
        contact.delegate = self
        pincode.delegate = self
        address.delegate = self
        address2.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
            
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        self.saveButtonConstraint.constant = 20
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")

        view.addGestureRecognizer(tap)
        var country_code = ""
        if native.object(forKey: "country_code") != nil {
            
            country_code = native.string(forKey: "country_code")!
            
        }
        print("country_code", country_code)
        if country_code == "US"
        {
            landmark.isHidden=true
            AreaTopConstraint.constant = -45
        }
        else
        {
            
            landmark.isHidden=false
            AreaTopConstraint.constant = 8
        }
    
        searchView.layer.cornerRadius = 8
        searchView.clipsToBounds = true
        home.layer.cornerRadius = home.frame.size.height/2
        home.clipsToBounds = true
        work.layer.cornerRadius = work.frame.size.height/2
        work.clipsToBounds = true
        other.layer.cornerRadius = other.frame.size.height/2
        other.clipsToBounds = true
        addressButton.layer.cornerRadius = addressButton.frame.size.height/2
        addressButton.clipsToBounds = true
        
        
        if DisplayAddress.addressSender != "getAddress"
        {
//            self.setBillingAddressContainer.isHidden = false
            countryid = 0
            cityName1 = ""
            stateName1 = ""
            region = ""
            self.navigationItem.title = "Edit Address"
            getAddresstoEdit()
        }
        else
        {
            disableAddressType()
            self.navigationItem.title = "Add Address"
//            self.setBillingAddressContainer.isHidden = true
        }
        
//           determineMyCurrentLocation()
      
        // Do any additional setup after loading the view.
    }
    
    func getAddresstoEdit()
    {
        let index = DisplayAddress.selectedindex
        print(DisplayAddress.selectAddress[index])
        if DisplayAddress.selectAddress[index]["consignee_name"] is NSNull
        {}
        else
        {
        name.text = "\((DisplayAddress.selectAddress[index]["consignee_name"] as? String)!)"
        }
        
        if DisplayAddress.selectAddress[index]["city"] is NSNull
        {}
        else
        {
            self.cityName1 = "\((DisplayAddress.selectAddress[index]["city"] as? String)!)"
            
        }
        
        if DisplayAddress.selectAddress[index]["state"] is NSNull
        {}
        else
        {
            self.stateName1 = "\((DisplayAddress.selectAddress[index]["state"] as? String)!)"
            
        }
        if DisplayAddress.selectAddress[index]["region"] is NSNull
        {}
        else
        {
            self.region =   "\((DisplayAddress.selectAddress[index]["region"] as? String)!)"
            
        }
        
        self.city.text = "\(self.cityName1), \(self.region), \(self.stateName1)  "
        if DisplayAddress.selectAddress[index]["pincode"] is NSNull
        {}
        else
        {
                // dict["totalfup"] isn't a String
                pincode.text = "\((DisplayAddress.selectAddress[index]["pincode"] as! String))"
            
            
        }
        if DisplayAddress.selectAddress[index]["address1"] is NSNull
        {
            address.text = ""
            self.locationName.isHidden = true
            self.searchlocationTitle.isHidden = true
            self.searchPlaceholder.isHidden = false
        }
        else
        {
          self.locationName.isHidden = false
          self.searchlocationTitle.isHidden = false
          self.searchPlaceholder.isHidden = true
            locationName.text = "\(DisplayAddress.selectAddress[index]["address1"] as! String)"

          address.text = "\(DisplayAddress.selectAddress[index]["address1"] as! String)"
        }
        if DisplayAddress.selectAddress[index]["user_landmark"] is NSNull
        {
            landmark.text = ""
        }
        else
        {
          landmark.text = "\(DisplayAddress.selectAddress[index]["user_landmark"] as! String)"
        }
        if DisplayAddress.selectAddress[index]["address3"] is NSNull
        {}
        else
        {
          city.text = "\(DisplayAddress.selectAddress[index]["address3"] as! String)"
        }
        if DisplayAddress.selectAddress[index]["address2"] is NSNull
        {
            
            address2.text = ""
        }
        else{
          address2.text = "\(DisplayAddress.selectAddress[index]["address2"] as! String)"
        }
        if DisplayAddress.selectAddress[index]["contact_number"] is NSNull
        {}
        else
        {
            contact.text = "\(DisplayAddress.selectAddress[index]["contact_number"] as! String)"
        }
        if DisplayAddress.selectAddress[index]["is_shipping_default"] is NSNull
        {}
        else
        {
            if DisplayAddress.selectAddress[index]["is_shipping_default"] as! Bool == true
            {
//                shippingAddress.isOn = true
                isshippingon = true
            }
            else
            {
//                shippingAddress.isOn = false
                isshippingon=false
            }
        }
        if DisplayAddress.selectAddress[index]["address_name"] is NSNull && DisplayAddress.selectAddress[index]["address_typeid"] is NSNull
        {}
        else
        {
          selectedaddresslkpid = DisplayAddress.selectAddress[index]["address_typeid"] as! Int
            selectedaddresstype = "\(DisplayAddress.selectAddress[index]["address_name"] as! String)"
            if selectedaddresslkpid == 2
            {
                self.SelectHome((Any).self)
            }
            else if selectedaddresslkpid == 3
            {
               self.selectWork((Any).self)
            }
            else if selectedaddresslkpid == 5
            {
                 self.selectOther((Any).self)
            }
            else
            {
                
            }
        }
        
    }
    
    func disableAddressType()
    {
        user_AddressId = 0
        home.layer.backgroundColor = grayColor
        work.layer.backgroundColor = grayColor
        other.layer.backgroundColor = grayColor
        home.setTitleColor(UIColor.white, for: .normal)
        work.setTitleColor(UIColor.white, for: .normal)
        other.setTitleColor(UIColor.white, for: .normal)
    }
    
    @IBAction func SelectHome(_ sender: Any) {
        disableAddressType()
        selectedaddresstype = "Home"
        selectedaddresslkpid = 2
        home.layer.backgroundColor = redColor
        home.setTitleColor(redtitleColor, for: .normal)
    }
    
    @IBAction func selectWork(_ sender: Any) {
        disableAddressType()
        selectedaddresstype = "Work"
        selectedaddresslkpid = 3
        work.layer.backgroundColor = redColor
        work.setTitleColor(redtitleColor, for: .normal)
    }
    
    @IBAction func selectOther(_ sender: Any) {
        disableAddressType()
        selectedaddresstype = "Other"
        selectedaddresslkpid = 5
        other.layer.backgroundColor = redColor
        other.setTitleColor(redtitleColor, for: .normal)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
     
     @objc func keyboardWillShow(notification: NSNotification) {
         if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
             if self.saveButtonConstraint.constant <= 20 {
                 
                 self.saveButtonConstraint.constant = keyboardSize.height+20
             }
         }
     }

     @objc func keyboardWillHide(notification: NSNotification) {
         if self.saveButtonConstraint.constant > 20 {
             self.saveButtonConstraint.constant = 20
         }
     }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        name.resignFirstResponder()
        landmark.resignFirstResponder()
        contact.resignFirstResponder()
        pincode.resignFirstResponder()
        address.resignFirstResponder()
         
    }
    
    //Calls this function when the tap is recognized.
    @objc override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
  
    
    // check phone number
    func validate(pincode: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = pincode.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  pincode == filtered
    }
    // check pincode
    func isValidInput(Input:String) -> Bool {
        let RegEx = "\\A\\w{3,18}\\z"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    // check string
    func validateString(pincode: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
        let inputString = pincode.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  pincode == filtered
    }
    
    func validation() -> Bool {
        var valid: Bool = true
        if (name.text!.count) < 1 || validateString(pincode: name.text!) == false || name.text?.contains("  ") == true{
            // change placeholder color to red color for textfield email-id
            self.view.makeToast("Please enter your full name!", duration: 2.0, position: .center)
//            name.text?.removeAll()
//            name.attributedPlaceholder = NSAttributedString(string: " Enter correct name",  attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
         if (contact.text!.count) < 7 || validate(pincode: contact.text!) == false || (contact.text!.count) > 13{
            // change placeholder color to red color for textfield email-id
            self.view.makeToast("Please enter your valid number!", duration: 2.0, position: .center)
//            contact.text?.removeAll()
//            contact.attributedPlaceholder = NSAttributedString(string: " Enter correct number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if  validate(pincode: pincode.text!) == false || pincode.text == "" || pincode.text!.count < 4 || pincode.text!.count > 10{
            // change placeholder color to red color for textfield email-id
            self.view.makeToast("Please enter correct postal code!", duration: 2.0, position: .center)
//            pincode.text?.removeAll()
//            pincode.attributedPlaceholder = NSAttributedString(string: " Enter correct Pincode", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }

        if (address.text!.count) < 3 || (address.text!.count) > 150 || address.text?.contains("  ") == true {
            // change placeholder color to red color for textfield email-id
            self.view.makeToast("Please enter your correct address!", duration: 2.0, position: .center)
//            address.text?.removeAll()
//            address.attributedPlaceholder = NSAttributedString(string: "Please Enter correct Address", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if (locationName.text!.count) < 3 || (locationName.text!.count) > 150 || locationName.text?.contains("  ") == true {
            // change placeholder color to red color for textfield email-id
            locationName.text?.removeAll()
            self.view.makeToast("Please assign a Tag first !", duration: 2.0, position: .center)
            valid = false
        }
        if selectedaddresslkpid == 0 {
            // change placeholder color to red color for textfield email-id
            let bottomOffset = CGPoint(x: 0, y: (addressScrollView.contentSize.height - addressScrollView.bounds.height + addressScrollView.adjustedContentInset.bottom)/2)
            addressScrollView.setContentOffset(bottomOffset, animated: true)
            self.view.makeToast("Please select address type first!", duration: 2.0, position: .center)
            valid = false
        }
        
        return valid
    }
    
    
    
    @IBAction func SaveAddress(_ sender: Any) {
    
        if validation() == true
        {
            if selectedaddresslkpid != 0
            {
        var Token = native.string(forKey: "Token")!
        if Token != nil
        {
            CustomLoader.instance.showLoaderView()
            let header = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            print("Successfully post")
            let address_data: [String: Any]
            let b2burl = native.string(forKey: "b2burl")!
            var url = ""
            if DisplayAddress.addressSender == "getAddress"
            {
                
                
                
                
                
                address_data = [
                
                "contact_number": contact.text!,
                "address1": address.text!,
                "address2": address2.text!,
                "address3": city.text!,
                "countryid": self.countryid,
                    "city": self.cityName1,
                    "state": self.stateName1,
                    "region": self.region,
                    "is_validated": self.is_validated,
                "consignee_name":name.text!,
                "cityid": 0,
                "landmark": landmark.text!,
                "address_typeid": selectedaddresslkpid,
                "pincode": pincode.text!,
                "address_name":selectedaddresstype,
                "is_billing_default": isbillingon,
                "is_shipping_default": isshippingon
                ]

                url = "\(b2burl)/users/add_address/"
            }
            else
            {
                let index = DisplayAddress.selectedindex
                var userAddressId = DisplayAddress.selectAddress[index]["user_addressid"] as! Int
                address_data = [
                    "contact_number": contact.text!,
                    "address1": address.text!,
                    "address2": address2.text!,
                    "address3": city.text!,
                    "countryid": self.countryid,
                        "city": self.cityName1,
                        "state": self.stateName1,
                        "region": self.region,
                        "is_validated": self.is_validated,
                    "consignee_name":name.text!,
                    "landmark": landmark.text!,
                    "cityid": selectedcitylkid,
                    "address_typeid": selectedaddresslkpid,
                    "pincode": pincode.text!,
                    "user_addressid": userAddressId,
                    "address_name":selectedaddresstype,
                    "is_billing_default": isbillingon,
                    "is_shipping_default": isshippingon
                ]
             url = "\(b2burl)/users/edit_address/"
            }
            let parameters: [String:Any] = ["address_data": address_data]
            print(url, parameters)
            Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
             
            print("response shop: \(response)")
            let result = response.result
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if let invalidToken = dict1["status"]{
                        if invalidToken as! String  == "success"
                        {
                            self.navigationController?.popViewController(animated: true)
                        }
                        else if dict1["status"] as! String  == "failure"
                        {
                        self.view.makeToast(dict1["data"] as! String, duration: 2.0, position: .center)
                        }
                        else
                        {
                          self.view.makeToast("Please try agian later!", duration: 2.0, position: .center)
                        }
                    }
                }

            }}
            }
            else
            {
                // create the alert
                self.view.makeToast("Please select address type first", duration: 2.0, position: .center)

            }
        }
    }
    
    
    
    
 
    
}


extension UILabel {
    func set(image: UIImage, with text: String) {
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: 0, width: 10, height: 10)
        let attachmentStr = NSAttributedString(attachment: attachment)
        
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentStr)
        
        let textString = NSAttributedString(string: text, attributes: [.font: self.font])
        mutableAttributedString.append(textString)
        
        self.attributedText = mutableAttributedString
    }
}


extension AddNewAddressViewController: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("place", place)
        print("Place name: ", place.name)
        
        print("Place address: ", place.formattedAddress)
        var country = ""
        var pincode = ""
        var address = ""
        address = place.name!
        print("Place attributions: ", place.attributions)
         //Get address Seperated like city,country,postalcode
        
           let arrays : Array = place.addressComponents!
           for i in 0..<arrays.count {

               let dics : GMSAddressComponent = arrays[i]
               let str : String = dics.type
            
               if (str == "country") {
                   print("Country: \(dics.name)")
                country = "\(dics.name)"
                
               }
                else if (str == "administrative_area_level_1") {
                 print("State: \(dics.name)")
                 
                self.stateName1 = "\(dics.name)"
                 
                }
               else if (str == "administrative_area_level_2") {
                print("State: \(dics.name)")
                
                self.region = "\(dics.name)"
                
               }
               else if (str == "locality") {
                print("City: \(dics.name)")
                self.cityName1 = "\(dics.name)"
                
               }
               else if (str == "postal_code"){
                   print("PostalCode:\(dics.name)")
                   pincode = dics.name
                }// this is only to get postalcode of the selected nearby location
           }
        if cityName1 != "" && self.region != "" && self.stateName1 != "" && place.formattedAddress != nil
        {
          
          
            self.city.text = "\(cityName1),\(self.stateName1), \(self.region)"
            var country_code = ""
            if native.object(forKey: "country_code") != nil {
                
                country_code = native.string(forKey: "country_code")!
                
            }
//            if country_code == "US"
//            {
            self.address.text = address
//            }
//            else
//            {
//               self.address.text = ""
//            }
          searchlocationTitle.isHidden = false
          searchPlaceholder.isHidden = true
          locationName.isHidden = false
          locationName.text = address
          selectedcitylkid = 0
            if pincode != ""
            {
                self.pincode.isEnabled = false
                self.is_validated=true
                self.pincode.text = pincode
            }
            else
            {
                self.pincode.isEnabled = true
                self.is_validated=false
                self.pincode.text = ""
                self.view.makeToast("Please enter correct postal code!", duration: 2.0, position: .center)
            }
        }
        else
        {
          self.view.makeToast("Sorry this address is not servicable!", duration: 2.0, position: .center)
        }
        
        self.dismiss(animated: true, completion: nil)
    }


    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // To handle error
        print(error)

    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        self.dismiss(animated: true, completion: nil)
    }

    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }


}
