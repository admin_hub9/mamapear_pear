//
//  ReviewSearchViewController.swift
//  Hub9
//
//  Created by Deepak on 13/07/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire



var ReviewSuggestion = ReviewSearchViewController()

class ReviewSearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    
    
    var native = UserDefaults.standard
    //search limit
    var limit = 10
    var Product = [String]()
    var filterdItemsArray = [String]()
    var shop : NSArray = []
    var isSearching = false
    static var searchText = ""
    
    @IBOutlet var searchbar: UISearchBar!
    @IBOutlet var searchTable: UITableView!
    
    @IBAction func cancel(_ sender: Any) {
        searchbar.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        searchbar.becomeFirstResponder()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        searchbar.resignFirstResponder()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let change = native.string(forKey: "searchchange")
        searchbar.becomeFirstResponder()
        if change == "0"
        {
            searchbar.text = ""
            isSearching = true
            searchTable.isHidden = false
            searchTable.reloadData()
        }
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///shoe keyborad
        self.tabBarController?.tabBar.isHidden = true
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.darkGray
        
        searchTable.isHidden = true
        searchTable.tableFooterView = UIView()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchbar.resignFirstResponder()
        searchBarViewController.searchText = searchbar.text!
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchReview"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "suggetion", for: indexPath) as! suggestionTableViewCell
        
        
        cell.name.text = searchbar.text
        cell.category.text = "Keyword"
        
        return cell
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("cancel")
        
        searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.30)
    }
    
    @objc func reload(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespaces) != "" else {
            debugPrint("nothing to search")
            
            searchTable.reloadData()
            return
        }
        
        debugPrint(query)
        isSearching = false
        if  query != ""
        {
            
            self.Product.removeAll()
            self.Product.append(query)
            searchTable.isHidden = false
            //                isSearching = true
            searchTable.reloadData()
        }
        
    }
    
    
    
    //// search bar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if (searchBar.text?.count)! >= 3
        {
            searchBarViewController.searchText = searchBar.text!
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchReview"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
            
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    //fetch suggestion data
    //    func parseData(text: String)
    //    {
    //         searchTable.isHidden = true
    ////        let Token = self.native.string(forKey: "Token")!
    ////        if Token != nil
    ////        {
    //            let b2burl = native.string(forKey: "b2burl")!
    //            debugPrint(b2burl)
    //            debugPrint(text, searchBarViewController.type)
    ////            debugPrint(Token)
    //            var validateUrl =  "\(b2burl)/search/suggestion/?search_type=\(searchBarViewController.type)&search_query=\(text.lowercased())&limit=\(limit)&offset=0"
    //
    //            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    //            debugPrint("========",validateUrl)
    //            var a = URLRequest(url: NSURL(string:validateUrl)! as URL)
    //            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
    //
    //            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
    //                //                                debugdebugPrint(response)
    //                debugPrint("responseSerach: \(response)")
    //                let result = response.result
    //                CustomLoader.instance.hideLoaderView()
    //                UIApplication.shared.endIgnoringInteractionEvents()
    //                CustomLoader.instance.hideLoaderView()
    //                UIApplication.shared.endIgnoringInteractionEvents()
    //                if let dict1 = result.value as? Dictionary<String, AnyObject>{
    //                    if let invalidToken = dict1["detail"]{
    //                        if invalidToken as! String  == "Invalid token."
    //                        { // Create the alert controller
    //                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
    //                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    //                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
    //                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //                            appDelegate.window?.rootViewController = redViewController
    //                        }
    //                    }
    //
    //                    else if dict1["status"] as! String == "failure"
    //                    {
    ////                        self.ActivityIndicator.stopAnimating()
    ////                        self.blurView.alpha = 0
    ////
    //                        self.blurView.alpha = 0
    //                        self.activity.stopAnimating()
    //                        let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertController.Style.alert)
    //                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
    //                        self.present(alert, animated: true, completion: nil)
    //                    }
    //
    //                    else{
    //                        if searchBarViewController.type == "product_elastic"
    //                        {
    //                        if let data = dict1["data"] as? [String]
    //                        {
    //
    //                            func filterContentForSearchText(searchText: String) {
    //                                self.filterdItemsArray = data.filter { item in
    //                                    return item.lowercased().contains(searchText.lowercased())
    //                                }
    //                            }
    //
    //                            filterContentForSearchText(searchText: (self.searchbar.text?.lowercased())!)
    //                            debugPrint(self.filterdItemsArray)
    //                            if self.filterdItemsArray != nil
    //                            {
    //                                self.Product = self.filterdItemsArray
    //                            }
    //                            }
    //                        }
    //                        else if searchBarViewController.type == "shop_elastic"{
    //                            if let innerdict = dict1["data"] as? NSArray{
    //
    //                                self.shop = dict1["data"] as! NSArray
    //                            }
    //                        }
    //
    //                            self.blurView.alpha = 0
    //                            self.activity.stopAnimating()
    //                            self.searchTable.isHidden = false
    //                            self.searchTable.reloadData()
    //
    //                    }
    //                }}
    //        }
    
    
    
    //    }
    
    
}
