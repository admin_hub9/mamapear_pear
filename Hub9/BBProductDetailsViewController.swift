//
//  BBProductDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 2/26/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import AVKit
import  Foundation
import Alamofire
import SCLAlertView
import Cosmos
import AttributedTextView
import BadgeSwift
import Toast_Swift
import Optik
//import ReadMoreTextView

var BBProductDetails = BBProductDetailsViewController()

class BBProductDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    var native = UserDefaults.standard
    var t1:Timer?
    
    var ProductData = NSDictionary()
    var ProductImg = [AnyObject]()
    var ProductMainImage = [AnyObject]()
     var DataType = [["Dicttype": String(), "dict": [AnyObject]()]]
    var ProductVideo = ""
    var ProductName = ""
    var feature_name = ""
    var shopName = ""
    var ShopLogo = ""
    var shopaddress = ""
    var shopRating = ""
    var ProductPrice = 0.0
    var ProductMOQ = ""
    var retailPrice = 0.0
    var sellingPrice = 0.0
    var groupPrice = 0.0
    var bulkPrice = 0.0
    var productSaveTag = ""
    var CurrencySymbol = ""
    var product_price_tag = "pc"
    var addtoCartTag = "pcs"
    var fav = 0
    var productid = 0
    var shopid = 0
     var quotation = false
    var senddataAddtocart = false
    var sub_categoryid = 0
    var productDescription = ""
    var dynamic_pricing  = [AnyObject]()
    var shippingDetails = [String]()
    var shop_product = [AnyObject]()
    var shop_productName=[String]()
    var ProductDeal = [AnyObject]()
    var SimilarProduct = [AnyObject]()
    var Similar_productName=[String]()
    var Similar_Product_Currency = ""
    var shopCurrency = ""
    var isDescExpand = true
    var isShipExpand = false
    var shipping_profile_info = ""
    var user_purchase_modeid = "1"
    var dealTime = ""
    var product_share_link = "https://www.bulkbyte.com"
    
    var ProductVariantData = [AnyObject]()
    //    var ProductImg = [AnyObject]()
    var VariantID = [AnyObject]()
    var blackcolor = UIColor(red:51/255, green:51/255, blue:51/255, alpha: 1).cgColor
    var appGcolor = UIColor(red:111/255, green:204/255, blue:193/255, alpha: 1).cgColor
    var lastContentOffset: CGFloat = 0
    var selectedCell = -1
    var shopFirebaseId = ""
    var shopprofile = ""
    var dealcell = 10
    var index = 0
    var groupUserName = ""
    var order_typeid = 3
    var group_orderid = ""
    var group_ownerid = ""
    var is_base_product = false
    var listing_status = false
    
    
    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()    
    @IBOutlet weak var productDetailsTable: UITableView!

    @IBOutlet weak var addtocart: UIButton!
    @IBOutlet weak var faveIcon: UIImageView!
    @IBOutlet weak var ContactSeller: UIButton!
    @IBOutlet weak var buyNow: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var BuyNowPriceLabel: UILabel!
    @IBOutlet weak var BuyNowTextLabel: UILabel!
    @IBOutlet weak var AddtoCartPriceLabel: UILabel!
    @IBOutlet weak var AddtoCartTextLabel: UILabel!
    @IBOutlet weak var shopLocationIcon: UIImageView!
    
    
    @IBOutlet weak var signinContainer: UIView!
    @IBOutlet weak var addtocartContainer: UIView!
    @IBOutlet weak var buynowContainer: UIView!
    
    @IBOutlet weak var signButtonText: UILabel!
    @IBOutlet weak var UnavailableMsgContainer: UIView!
    @IBOutlet weak var unavailableText: UILabel!
    @IBOutlet weak var blurEffect: UIVisualEffectView!
    
    
    
    @IBAction func backButton(_ sender: Any) {
        senddataAddtocart = false
        self.quotation = false
       
        self.navigationController?.popViewController(animated: true)
    }
    
    var btnBarBadge : MJBadgeBarButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        CustomLoader.instance.showLoaderView()
        fetchDetails()
        let token = self.native.string(forKey: "Token")!
        if token == ""
        {
            self.addtocartContainer.isHidden = true
            self.buynowContainer.isHidden = true
            self.signinContainer.isHidden = false
            let lightBlack = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
            self.signButtonText.attributedText = ("Sign In ".color(lightBlack).size(14) + "to Unlock Wholesale Price".color(lightBlack).size(14)).attributedText
        }
        else
        {
            self.addtocartContainer.isHidden = false
            self.buynowContainer.isHidden = false
            self.signinContainer.isHidden = true
        }
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        if user_purchase_modeid == "1"
        {
            BuyNowTextLabel.text = "Group Buy"
            AddtoCartTextLabel.text = "Bulk Buy"
            
        }
        else
        {
            BuyNowTextLabel.text = "Buy Now"
            AddtoCartTextLabel.text = "Add to Cart"
        }
        footerView.isHidden = true
        
        //        self.favProduct.isHidden = true
        BBProductDetails = self
        productDetailsTable.isHidden = true
        productDetailsTable.tableFooterView = UIView()
        
        let loginstartColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 239/255.0, green: 130/255.0, blue: 136/255.0, alpha: 1.00).cgColor
        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        
        let origImage = UIImage(named: "cart");
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        
        
        ContactSeller.layer.cornerRadius = ContactSeller.frame.height / 2
        ContactSeller.clipsToBounds = true
        ///fav navigation button
        self.faveIcon.image = UIImage(named: "follow")
        
        ///badges
        var barButton : MJBadgeBarButton!
        let customButton = UIButton(type: UIButton.ButtonType.custom)
        customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
        customButton.addTarget(self, action: #selector(self.onBagdeButtonClick), for: .touchUpInside)
        customButton.setImage(UIImage(named: "cart"), for: .normal)
        customButton.tintColor = textcolor
        self.btnBarBadge = MJBadgeBarButton()
        self.btnBarBadge.setup(customButton: customButton)
        
        self.btnBarBadge.shouldHideBadgeAtZero = true
        self.btnBarBadge.shouldAnimateBadge = true
        
        self.btnBarBadge.badgeValue = "0"
        self.btnBarBadge.badgeBGColor = textcolor
        self.btnBarBadge.badgeOriginX = 20.0
        self.btnBarBadge.badgeOriginY = -4
        
//        self.navigationController?.navigationBar.tintColor = textcolor
        self.navigationItem.rightBarButtonItem = self.btnBarBadge
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.senddataAddtocart = false
        self.tabBarController?.tabBar.isHidden = true
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        self.tabBarController?.tabBar.isHidden = false
//        senddataAddtocart = false
        t1?.invalidate()
        t1 = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getCartData()
            }

    
    
    @IBAction func favourite(_ sender: Any) {
//        self.blurView.alpha = 1
//        self.activity.startAnimating()
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
        var id = native.string(forKey: "FavProID")
        
        //        print("product id:", id)
        let parameters: [String:Any] = ["productid":productid, "shopid":shopid]
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        var url = ""
        if( fav == 1)
        {
            "users/get_fav_product"
            url = "\(b2burl)/users/unfollow_product/"
        }
        else
        {
            url = "\(b2burl)/users/follow_product/"
        }
        print("follow product", url)
        Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            switch response.result
            {
                
                
            case .failure(let error):
                //                print(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
            case .success:
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
            
            else if let json = response.result.value {
                let jsonString = "\(json)"
                print("res;;;;;", jsonString)
                self.getfav_product()
                
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
                }
            }}
            }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    
    func getfav_product()
    {
        var id = native.string(forKey: "FavProID")!
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
        CustomLoader.instance.showLoaderView()
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
       
         var url = "\(b2burl)/users/get_fav_product/?productid=\(productid)"
       print("url", url, token)
        
        Alamofire.request(url, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            switch response.result {
            case .success:
                print("Validation Successful")
            
            if let result1 = response.result.value
            {
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                       else if dict1["status"] as! String == "success"
                        {
                            if let data = dict1["data"] as? Int
                            {
//                                print("oppoipoi", data)
                                self.fav = data
                                self.faveIcon.image = nil
                                if self.fav == 1
                                {
                                    
                                    let origImage = UIImage(named: "FavMarked")?.withRenderingMode(.alwaysTemplate)
                                    self.faveIcon.image = origImage
                                    self.faveIcon.tintColor = .red
                                }
                                else
                                {
                                    
                                    self.faveIcon.image = UIImage(named: "follow")
                                }
                            }
                        }
                        
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
            
                }
            }
        
            case .failure(let error):
//                print(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            }
        }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
            
    }
    
    @IBAction func contactSeller(_ sender: Any) {
        CustomLoader.instance.showLoaderView()
        self.ContactSeller.isEnabled = false

        UIApplication.shared.beginIgnoringInteractionEvents()
        if native.string(forKey: "Token")! != ""
        {
        if native.object(forKey: "firebaseid") != nil {
       if shopFirebaseId == native.string(forKey: "firebaseid")!
       {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.ContactSeller.isEnabled = true
            let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your own shop", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        else
        {
            CustomLoader.instance.showLoaderView()
            quotation = true
            native.set("1", forKey: "currentuserid")
            native.synchronize()
            var chatroom = CheckChatRoomViewController()
            chatroom.chekChatRoom(shopid: shopFirebaseId, shopname: shopName, shopProfilePic: ShopLogo)
//            self.navigationController?.setNavigationBarHidden(true, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                // Your code with delay
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.ContactSeller.isEnabled = true
                UIApplication.shared.endIgnoringInteractionEvents()

                self.performSegue(withIdentifier: "segueproduct", sender: self)
            }
        }
        }else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.ContactSeller.isEnabled = true
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }

        }else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.ContactSeller.isEnabled = true
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
    }
    
    @objc func openGroupView()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "gourpDealPopup") as! ViewAllGroupDealViewController
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
    }
    
    @objc func dealAlert()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "dealalert") as! dealAlertViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
    }
    
    @IBAction func addTocart(_ sender: Any) {
        if user_purchase_modeid != "1"
        {
            order_typeid = 3
        }
        else
        {
            order_typeid = 1
        }
        if native.string(forKey: "Token")! != nil
        {
        self.senddataAddtocart = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "updateVariant") as! UpdateProductVariantViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
        }
        else
        {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
    }
    
    
    
    @IBAction func BuyNow(_ sender: Any) {
        if user_purchase_modeid != "1"
        {
            order_typeid = 3
        }
        else
        {
            order_typeid = 2
        }
        if native.string(forKey: "Token")! != ""
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "updateVariant") as! UpdateProductVariantViewController

        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self

        present(pvc, animated: true, completion: nil)
        }
        else
        {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
        
    }
    
    
    
    
    @objc func onBagdeButtonClick() {
        senddataAddtocart = true
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "buying"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    func getProductsDeal()
    {
        var id = native.string(forKey: "FavProID")!
        
        let header = ["Content-Type": "application/json"]
        print("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        
        
        var a = URLRequest(url: NSURL(string: "\(b2burl)/product/get_product_deal/?productid=\(productid)")! as URL)
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            print("responses deal: \(response)")
            switch response.result {
            case .success:
    
            if let result1 = response.result.value
            {
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as! String == "success"
                    {
                        if let data = dict1["response"] as? [AnyObject]
                        {
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.ProductDeal = data
                            print("oppoipoi", data)
                           
                        }
                    
                        let token = self.native.string(forKey: "Token")!
                        if token == ""
                        {
                            self.BuyNowPriceLabel.isHidden = true
                            self.AddtoCartPriceLabel.isHidden = true
                        }
                        else
                        {
                    if self.user_purchase_modeid == "1"
                    {
                        self.BuyNowPriceLabel.isHidden = false
                        self.AddtoCartPriceLabel.isHidden = false
                        self.BuyNowPriceLabel.text = "\(self.CurrencySymbol)\(self.groupPrice)/\(self.addtoCartTag)"
                        let textcolor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                        
                        self.AddtoCartPriceLabel.attributedText = ("\(self.CurrencySymbol)\(String(format:"%.2f", ((Double(self.sellingPrice))*(Double(self.ProductMOQ)!))))/" .color(textcolor) + "\(self.ProductMOQ)\(self.addtoCartTag)s ".color(textcolor)).attributedText
                            
                        
                    }
                    else{
                        self.BuyNowPriceLabel.isHidden = true
                        self.AddtoCartPriceLabel.isHidden = true
                        self.BuyNowPriceLabel.text = "\(self.CurrencySymbol) \(self.bulkPrice)"
                        self.AddtoCartPriceLabel.text
                            = "\(self.CurrencySymbol) \(self.bulkPrice)"
                        }
                        
                    }
                        self.productDetailsTable.beginUpdates()
                        self.productDetailsTable.reloadRows(at: [IndexPath(row: 6, section: 0)], with: .automatic)
                        self.productDetailsTable.endUpdates()
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                    
                }
                
            }
            
            case .failure(let error):
                //                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
            
            }
            
        }
            
            
    }
    
    
    func getCartData()
    {
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        let Token = self.native.string(forKey: "Token")!
        print("token", Token)
        if Token != ""
        {
//            self.blurView.alpha = 1
//            self.activity.startAnimating()
            let b2burl = native.string(forKey: "b2burl")!
            var user_purchase_modeid = ""
            if native.object(forKey: "user_purchase_modeid") != nil {
                user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
            }
            var a = URLRequest(url: NSURL(string: "\(b2burl)/order/get_cart/?purchase_modeid=\(user_purchase_modeid)") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            print("a-a-a-a-a", a)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                
                switch response.result
                {
                case .failure(let error):
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    //                print(error)
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                print("responses cart: \(response)")
                let result = response.result
                
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                    else if dict1["status"] as! String == "success"
                    {
                    if dict1["data"] is NSNull{}
                    else
                    {
                    if let data = dict1["data"] as? [AnyObject]
                    {
                        if data.count>0
                        {
                        if let cartcount = data[0]["variant_data"] as? [AnyObject]
                        {
                            print(data.count)
                            self.btnBarBadge.badgeValue = "\(data.count)"
                        }  }
                    }
                    }
                    }
                }
            }}
        }
    }

    func getSimilarProduct()
    {
        
            let b2burl = native.string(forKey: "b2burl")!
            var a = URLRequest(url: NSURL(string: "\(b2burl)/product/get_similar_product/?shopid=\(shopid)&categoryid=\(sub_categoryid)") as! URL)
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        
            print("a-a-a-a-a", a)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                
                case .success:
                print("responses similar product: \(response)")
                let result = response.result
                
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as! String == "success"
                    {
                        if let responce = dict1["response"] as? AnyObject
                        {
                        if let data = responce["similar_shop_product"] as? [AnyObject]
                        {
                            for i in 0..<data.count
                            {
                                if data[i]["title"] is NSNull
                                {
                                    self.shop_productName.append("")
                                }
                                else
                                {
                                    let name = data[i]["title"] as! String
                                    self.shop_productName.append(name)
                                }
                                if data[i]["currency_symbol"] is NSNull{}
                                else
                                {
                                    self.shopCurrency = data[i]["currency_symbol"] as! String
                                }
                                
//                                let product_variant_data = data[i]["product_variant_data"] as! [AnyObject]
                                if data.count > 0
                                {
                                    
                                    let shopdata = data[i]["similar_shop_product_variant_data"] as! [AnyObject]
                                    if shopdata.count > 0
                                    {
                                        self.shop_product.append(shopdata[0] as AnyObject)
                                    }
                                }
                                
                            }
                            }
                         if let data = responce["similar_product"] as? [AnyObject]
                        {
                            for i in 0..<data.count
                            {
                                if data[i]["title"] is NSNull
                                {
                                    self.Similar_productName.append("")
                                }
                                else
                                {
                                    let name = data[i]["title"] as! String
                                    self.Similar_productName.append(name)
                                }
                                if data[i]["currency_symbol"] is NSNull{}
                                else
                                {
                                    self.Similar_Product_Currency = data[i]["currency_symbol"] as! String
                                }
                                if data.count > 0
                                {
                                    
                                    let shopdata = data[i]["similar_shop_product_variant_data"] as! [AnyObject]
                                    if shopdata.count > 0
                                    {
                                        self.SimilarProduct.append(shopdata[0] as AnyObject)
                                    }
                                }
                                
                            }
                            }
                        }
                        
                      self.productDetailsTable.beginUpdates()
                    self.productDetailsTable.reloadRows(at: [IndexPath(row: 9, section: 0)], with: .automatic)
                    self.productDetailsTable.reloadRows(at: [IndexPath(row: 10, section: 0)], with: .automatic)
                    self.productDetailsTable.endUpdates()
                    }
                    guard dict1 != nil else {
                        self.displayErrorAndLogOut()
                        //                        print ("Dict is nil Products")
                        return
                    }
                }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            }
        }
//    }
        
    }
    
    func displayErrorAndLogOut() {
        let alert = SCLAlertView()
        let errorColor : UInt = 0xf8ac59
        if Util.shared.checkNetworkTapped() {
            alert.showError("Uh Oh!", subTitle: "Your session has expired! Please log in again.", closeButtonTitle: "Okay", colorStyle: errorColor)
        } else {
            alert.showError("Uh Oh!", subTitle: "You have lost connectivity to the internet. Please check your connection and log in again.", closeButtonTitle: "Okay", colorStyle:errorColor)//, colorTextButton: errorColor)
        }
        
        
        
    }

    func fetchDetails()
    {
            CustomLoader.instance.showLoaderView()
            let id = native.string(forKey: "FavProID")
            print("product id:", id)
            let b2burl = native.string(forKey: "b2burl")!
            var url = ""
            let check = native.string(forKey: "comefrom")
            if check == "fav"
            {
            url = "\(b2burl)/product/get_listing_detail/?parent_productid=\(id!)"
            }
            else
            {
            url = "\(b2burl)/product/product_detail/?product_variantid=\(id!)"
            }
            var a = URLRequest(url: NSURL(string: url) as! URL)
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{

            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        
//        print("aproduct", a, token)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                                                debugPrint(response)
                switch response.result
                {
                case .failure(let error):
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    //                print(error)
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                print("response product details: \(response)")
                if let result1 = response.result.value
                {
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            UIApplication.shared.endIgnoringInteractionEvents()
                        if dict1["data"] is NSNull
                        {
                            
                        }
                       else if let data = dict1["data"] as? NSDictionary
                       {
                        if data.count>0
                        {
                        self.ProductData = dict1["data"] as! NSDictionary
                        
                        if self.ProductData["description"] is NSNull
                        {}
                        else
                        {
                            self.productDescription = (self.ProductData["description"] as! String)
                        }
                        if self.ProductData["feature_name"] is NSNull
                        {}
                        else
                        {
                            
                            self.feature_name = (self.ProductData["feature_name"] as! String)
                        }
                            if self.ProductData["product_price_tag"] is NSNull
                            {}
                            else
                            {
                                
                                self.product_price_tag = (self.ProductData["product_price_tag"] as! String)
                                print("self.product_price_tag", self.product_price_tag)
                                if self.product_price_tag == "/pc"
                                {
                                    self.addtoCartTag = "pc"
                                }
                                else if self.product_price_tag == "/set"
                                {
                                    self.addtoCartTag = "set"
                                }
                                else if self.product_price_tag == "/pack"
                                {
                                    
                                    self.addtoCartTag = "pck"
                                }
                            }
                        var processingfrom = 0
                        var processingto = 0
                            if self.ProductData["listing_status"] as? Bool ?? true {
                                
                                self.listing_status = true
                            }
                            else
                            {
                                self.listing_status = false
                            }
                            if self.ProductData["is_base_product"] as? Bool ?? true {
                                print("inside is_base_product")
                                self.is_base_product = true
                            }
                            else
                            {
                                print("outside is_base_product")
                                self.is_base_product = false
                            }
                            if self.ProductData["min_order_qty"] is NSNull
                            {}
                            else
                            {
                                self.ProductMOQ = "\((self.ProductData["min_order_qty"] as? Int)!)"
                            }
                        if self.ProductData["processing_from"] is NSNull
                        {}
                        else
                        {
                            processingfrom = (self.ProductData["processing_from"] as? Int)!
                        }
                        if self.ProductData["processing_to"] is NSNull
                        {}
                        else
                        {
                            processingto = (self.ProductData["processing_to"] as? Int)!
                        }
                        if processingto != 0 && processingfrom != 0
                        {
                            var shippingdata = "Estimated delivery \(processingfrom) - \(processingto) days"
                            self.shippingDetails.append(shippingdata)
                        }
                        
                        if self.ProductData["return_days"] is NSNull
                        {}
                        else
                        {
                             var returnData = ""
                            if (self.ProductData["return_days"] as? Int)! == 0
                            {
                                if self.shippingDetails.count == 0
                                {
                                returnData = "\u{2022} Non returnable"
                                }
                                else
                                {
                                    returnData = "Non returnable"
                                }
                            }
                            else
                            {
                                if self.shippingDetails.count == 0
                                {
                                    returnData = "\u{2022} \((self.ProductData["return_days"] as? Int)!) Days easy Return"
                                }
                                else
                                {
                                    returnData = "\((self.ProductData["return_days"] as? Int)!) Days easy return"
                                }
                                
                            }
                            self.shippingDetails.append(returnData)
                        }
                        if self.ProductData["shipping_profile_info"] is NSNull
                        {}
                        else
                        {
                            if self.ProductData["shipping_profile_info"] is NSNull
                            {}
                            else
                            {
                            let profiledata = (self.ProductData["shipping_profile_info"] as? String)!
                            self.shipping_profile_info = profiledata
                            }
                        }
                        
                        let name = self.ProductData["title"]! as! String
//                        print("-------", name)
                        self.ProductName = name
                        
//                        self.productName.text = name
                        
                        if let shopname = data["shop_name"] as? String
                        {
                        self.shopName = shopname
                        }
                        if let shopfirebaseid = data["firebase_userid"] as? String
                        {
                            self.shopFirebaseId = shopfirebaseid
                        }
                        if data["sub_categoryid"] is NSNull
                        {}
                        else
                        {
                            self.sub_categoryid = data["categoryid"] as! Int
                        }
                        if data["shop_location"] is NSNull{}
                        else
                        {
                        let statename = (data["shop_location"] as? String)!
                        self.shopaddress = statename
                        }
//                        if data["shop_rating"] is NSNull{}
//                        else
//                        {
//                            let rating = (data["shop_rating"] as? NSNumber)!
//                            self.shopRating = "\(rating)"
//                        }
                        self.shopName = data["shop_name"] as! String
                        if data["shop_logo_url"] is NSNull
                        {}
                        else
                        {
                        self.ShopLogo = data["shop_logo_url"] as! String
                        }
                        self.productid = data["productid"] as! Int
                        self.shopid = data["shopid"] as! Int
                        if let product_variant_data = data["product_variant_data"] as? [AnyObject]
                        {   self.ProductVariantData = (data["product_variant_data"] as? [AnyObject])!
                            for i in 0..<product_variant_data.count
                            {
                                if let product_variant_size_data = product_variant_data[i]["product_variant_size_data"] as? [AnyObject]
                                {
                                    if product_variant_size_data.count > 0
                                    {
                                        
                                        if let imagedir = product_variant_size_data[0]["variant_image_url"] as? AnyObject
                                        {
                                            
                                            if let imgdata = imagedir["800"] as? [AnyObject]
                                            {
                                                if imgdata.count > 0
                                                {
                                                    self.ProductImg.append(imgdata[0] as! AnyObject)
                                                    
                                                }
                                            }
                                        }
                                    }}}
                            if let imagedir = data["image_url"] as? AnyObject
                            {

                                if let imgdata = imagedir["800"] as? [AnyObject]
                                {
                                    for var i in 0..<imgdata.count
                                    {
                                        if imgdata.count > 0
                                        {
                                            if i < 5
                                            {self.ProductImg.append(imgdata[i] as! AnyObject)
                                            }
                                            else
                                            {
                                                break
                                            }
                                        } } }
                                if let videodata = imagedir["mp4"] as? [AnyObject]
                                {
//                                    print("mp4video", self.ProductVideo, videodata)
                                    if videodata.count > 0
                                    {
                                        self.ProductVideo = videodata[0] as! String
                                    }
                                }
                            }
                        if let product_variant_size_data = product_variant_data[0]["product_variant_size_data"] as? [AnyObject]
                        {
                        if product_variant_size_data[0]["dynamic_pricing"] is NSNull
                        {}
                        else if let data = product_variant_size_data[0]["dynamic_pricing"]
                        {
                           self.dynamic_pricing = (product_variant_size_data[0]["dynamic_pricing"] as? [AnyObject])!
                            print("self.dynamic_pricing", self.dynamic_pricing)
                        }
                                
                            if product_variant_size_data[0]["selling_price"] is NSNull
                            {}
                                else
                            {
                            let price = product_variant_size_data[0]["selling_price"] as! NSNumber
                            var currency = product_variant_size_data[0]["currency_symbol"] as! String
//                            let myInteger: Int = currency
//                            if let myUnicodeScalar = UnicodeScalar(myInteger) {
//                                let myString = String(myUnicodeScalar)
                                self.CurrencySymbol = currency
//                                            self.price.text = "\(myString) \(price)"
//                            }
                         
                                if product_variant_size_data[0]["percentage_off"] is NSNull
                                {}
                                else
                                {
                                    self.productSaveTag = "\((product_variant_size_data[0]["percentage_off"] as? NSNumber)!)"
                                }
                        if product_variant_size_data[0]["product_share_link"] is NSNull
                        {}
                        else
                        {
                            self.product_share_link = "\((product_variant_size_data[0]["product_share_link"] as? NSString)!)"
                        }
                        
                        if product_variant_size_data[0]["retail_price"] is NSNull
                            {}
                        else
                            {
                                self.retailPrice = Double((product_variant_size_data[0]["retail_price"] as? NSNumber)!)
                            }
                        
                        if self.user_purchase_modeid == "1"
                        {
                            if product_variant_size_data[0]["deal_price"] != nil
                            {
                            if product_variant_size_data[0]["deal_price"] is NSNull
                                {}
                            else{
                                if let price = product_variant_size_data[0]["deal_price"] as? NSNumber!
                                    {
                                        if (price != nil){
                                            self.groupPrice = Double(price!)
                                        }
                                        else
                                        {
                                            if product_variant_size_data[0]["group_price"] is NSNull
                                            {}
                                            else
                                            { if let price = product_variant_size_data[0]["group_price"] as? NSNumber!
                                                {
                                                    self.groupPrice = Double(price!) }}}}}
                                if product_variant_size_data[0]["selling_price"] is NSNull
                                {}
                                else
                                {
                                    self.sellingPrice = Double((product_variant_size_data[0]["selling_price"] as? NSNumber)!)
                                }} }
                                else
                            {
                                if product_variant_size_data[0]["bulk_price"] is NSNull
                                {}
                                else
                                {
                                    self.bulkPrice = Double((product_variant_size_data[0]["bulk_price"] as? NSNumber)!)
                            }}
                                self.VariantID.append((product_variant_size_data[0]["product_variantid"] as? AnyObject)!)
                        }
                    }
                        }
                        }
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if self.ProductData.count > 0
                        {
                        self.getfav_product()
                        if self.user_purchase_modeid == "1"
                        {
                            self.BuyNowPriceLabel.isHidden = false
                            self.AddtoCartPriceLabel.isHidden = false
                        self.getProductsDeal()
                        }
                        else{
                            self.BuyNowPriceLabel.isHidden = true
                            self.AddtoCartPriceLabel.isHidden = true
                            self.BuyNowPriceLabel.text = "\(self.CurrencySymbol) \(self.bulkPrice)"
                            self.AddtoCartPriceLabel.text
                                = "\(self.CurrencySymbol) \(self.bulkPrice)"
                        }
                        self.getSimilarProduct()
//                        self.getCartData()
                        self.footerView.isHidden = false
                        self.productDetailsTable.isHidden = false
                        if self.productid != 0
                        {
                            let textcolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                            print("mp4video", self.ProductVideo)
                        }
                           
                        if self.listing_status == true
                        {
                            if self.user_purchase_modeid == "2" && self.is_base_product == true
                            {
                                self.footerView.isHidden = false
                                self.UnavailableMsgContainer.isHidden = true
                                self.productDetailsTable.isHidden = false
                                self.blurEffect.alpha = 0.0
                                self.productDetailsTable.reloadData()
                            }
                            else if self.user_purchase_modeid == "1"
                            {
                                self.footerView.isHidden = false
                                self.UnavailableMsgContainer.isHidden = true
                                self.productDetailsTable.isHidden = false
                                self.blurEffect.alpha = 0.0
                                self.productDetailsTable.reloadData()
                            }
                            else
                            {
                                self.footerView.isHidden = true
                                self.UnavailableMsgContainer.isHidden = false
                                self.productDetailsTable.isHidden = false
                                self.blurEffect.alpha = 0.0
                                self.productDetailsTable.reloadData()
                                print("purchagemode", self.is_base_product, self.user_purchase_modeid, self.listing_status)
                                self.view.makeToast("Currently Unavailable!", duration: 2.0, position: .center)
                                
                            }
                            }
                            else
                            {
                                self.footerView.isHidden = true
                                self.UnavailableMsgContainer.isHidden = false
                                self.productDetailsTable.isHidden = false
                                self.blurEffect.alpha = 0.0
                                self.productDetailsTable.reloadData()
                                print("purchagemode", self.is_base_product, self.user_purchase_modeid, self.listing_status)
                            self.view.makeToast("Currently Unavailable!", duration: 2.0, position: .center)
                            
                            }
                        
                        }
                        else
                        {   self.footerView.isHidden = true
                            self.UnavailableMsgContainer.isHidden = false
                            self.productDetailsTable.isHidden = false
                            self.blurEffect.alpha = 0.0
                            self.productDetailsTable.reloadData()
                            print("listing status", self.is_base_product, self.user_purchase_modeid)
                            self.view.makeToast("Currently Unavailable!", duration: 2.0, position: .center)
                            
                            }
                        
                        
                        }
                        }
                    }
                }
                }}
//        }
    }
    
  
    
    @IBAction func shareButton(_ sender: Any) {
        
        
        // image to share
        let text = self.ProductName
        var productImage = "thumbnail"
        if self.ProductImg.count>0
        {
        var imageUrl = self.ProductImg[0] as! String
        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
        
        if let url = NSURL(string: imageUrl )
        {
            if url != nil
            {
                productImage = imageUrl
            }}}
        let image = UIImage(named: productImage)
        let myWebsite = NSURL(string:self.product_share_link)
        let shareAll = [text , image , myWebsite!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    

  
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 && self.shopid != 0
        {
            var id = self.shopid
            native.set(id, forKey: "entershopid")
            native.synchronize()
            print("product id:", id)
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shopDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        
//        else if indexPath.row == 7
//        {
//            if isDescExpand == false
//            {
//                self.isDescExpand = true
//            }
//            else
//            {
//                self.isDescExpand = false
//            }
//            self.productDetailsTable.beginUpdates()
//            self.productDetailsTable.reloadRows(at: [IndexPath(row: 7, section: 0)], with: .automatic)
//            self.productDetailsTable.endUpdates()
//        }
       else if indexPath.row == 4
        {
            if isShipExpand == false
            {
                self.isShipExpand = true
            }
            else
            {
                self.isShipExpand = false
            }
            self.productDetailsTable.beginUpdates()
            self.productDetailsTable.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
            self.productDetailsTable.endUpdates()
        }
//        print(indexPath.row)
        
    }
    
    @objc func priceMatchAlert()
    {
        native.set("1", forKey: "AlertInfo")
        native.synchronize()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "alertInfo") as! InformationAlertViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
        
    }
    
    @objc func retailPriceAlert()
    {
        native.set("2", forKey: "AlertInfo")
        native.synchronize()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "alertInfo") as! InformationAlertViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
    }
    
    @objc func productDetail()
    {
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDescription"))! as UIViewController
        self.present(editPage, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // Dequeue the cell
//        let cell = tableView.dequeueReusableCell(withIdentifier: "shopname", for: indexPath) as! ShopNameTableViewCell
        // Check the player object is set (unwrap it)
        
            // Check if the player is playing
            if avPlayer.rate != 0 {
                // Pause the player
                avPlayer.pause()
            }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 12
        
//        if self.shippingDetails.count > 0
//        {
//            return 11 + (shippingDetails.count - 1)
//        }
//        else
//        {
//        return 10 + (shippingDetails.count - 1)
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellcount = (self.shippingDetails.count - 1)
        
        print(indexPath.row + self.shippingDetails.count, indexPath.row)
        var cellSize = 0
        switch indexPath.row {
        case 0:
            cellSize = 50
        case 1:
            cellSize = 280
        case 2:
            if self.dynamic_pricing.count > 0 && user_purchase_modeid == "2"
            {
                cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
            cellSize = Int(UITableViewAutomaticDimension)
            }
        case 3:
           if self.dynamic_pricing.count > 0 && user_purchase_modeid == "2"
           {
            cellSize = 75
           }
           else
           {
            cellSize = 0
            }
            
        case 4:
            
            if self.shippingDetails.count > 0
            {
                cellSize = 50
            }
            else
            {
                cellSize = 0
            }
        case 5:
            
            print("self.shippingDetails.count + 4", 5, self.shippingDetails)
            if self.shippingDetails.count > 0 && isShipExpand == true && self.shipping_profile_info != ""
            {
                cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        case 6:
            if self.ProductDeal.count > 0
            {
                cellSize = 85
            }
            else
            {
            cellSize = 0
            }
            
        case 7:
            
            print("self.shippingDetails.count + 4", 5, self.shippingDetails)
            if self.productDescription as String != ""
            {
                cellSize = 45
            }
            else
            {
                cellSize = 0
            }
        case 8:
            
            print("self.shippingDetails.count + 4", 5, self.shippingDetails)
            if self.productDescription as String != "" && self.isDescExpand == true
            {
                cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        
        case 9:
            if self.SimilarProduct.count > 0
            {
                cellSize = 220
            }
            else
            {
            cellSize = 0
            }
        case 10:
            if self.shop_product.count > 0
            {
                cellSize = 220
            }
            else
            {
                cellSize = 0
            }
        default:
            break
        }
        return CGFloat(cellSize)
    }
    
    
    @IBAction func signin(_ sender: Any) {
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {}
        else
        {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    @objc func signInButton()
    {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController

    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var ReuseCell = UITableViewCell()
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "shopname", for: indexPath) as! ShopNameTableViewCell
            if self.ShopLogo != nil
            {
            var imageUrl = self.ShopLogo
            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
            
            if let url = NSURL(string: imageUrl )
            {
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.shopLogo.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            }
            cell.shopName.text = self.shopName
            if self.shopaddress == ""
            {
                cell.shopLocationIcon.isHidden = true
            }
            else
            {
            cell.shopLocationIcon.isHidden = false
            cell.shopState.text = self.shopaddress
            }
            if self.shopRating != ""
            {
                cell.rating.isHidden = false
                cell.rating.text = self.shopRating
            }
            else
            {
                cell.rating.isHidden = true
            }
            ReuseCell = cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "productImage", for: indexPath) as! ProductImageCollectionTableViewCell
          
            cell.pageIndicator.currentPage = indexPath.row
            cell.ProductImageCollection.tag = indexPath.row
            cell.ProductImageCollection.reloadData()

            ReuseCell = cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameandpricing", for: indexPath) as! ProductNameAndPricingTableViewCell
            cell.productName.text = self.ProductName
//            cell.featuredTag.layer.borderWidth = 1
            let priceM = UITapGestureRecognizer(target: self, action: #selector(priceMatchAlert))
            cell.priceMatch.isUserInteractionEnabled = true
            cell.priceMatch.addGestureRecognizer(priceM)
            let saveR = UITapGestureRecognizer(target: self, action: #selector(retailPriceAlert))
            cell.saveRetail.isUserInteractionEnabled = true
            cell.saveRetail.addGestureRecognizer(saveR)
            cell.featuredTag.layer.cornerRadius = 3
            if self.feature_name != "Default"
            {
            cell.featuredTag.text = self.feature_name
            }
            let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
            let redColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
            let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
            let BBcolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
//            cell.featuredTag.layer.borderColor = redColor.cgColor
            cell.featuredTag.clipsToBounds = true
            cell.saveRetail.isHidden = false
            if productSaveTag != ""
            {
            cell.saveRetail.attributedText = ("Save \(productSaveTag)% vs retail ".color(lightGrayColor) + "Learn more".color(BBcolor)).attributedText
            }
            if native.string(forKey: "Token")! == ""
            {
                cell.price.font = UIFont.systemFont(ofSize: 15.0, weight: .regular)
                cell.price.attributedText = ("Sign In ".color(redColor) + "to Unlock Wholesale Price".color(lightBlackColor)).attributedText
            }
            else if self.listing_status != true
            {
                cell.price.font = UIFont.systemFont(ofSize: 15.0, weight: .regular)
                cell.price.attributedText = ("Currently Unavailable ".color(redColor)).attributedText
            }
            else if self.listing_status != false
            {
                cell.price.font = UIFont.systemFont(ofSize: 15.0, weight: .bold)
            if self.dynamic_pricing.count > 0 && user_purchase_modeid == "2"
            {
                cell.saveRetail.isHidden = true
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                
//            cell.price.attributedText = ("\(CurrencySymbol)\(String(format:"%.2f", bulkPrice))" .color(textcolor).size(20) + "\(self.product_price_tag)  ".color(textcolor).size(12) + "\(CurrencySymbol)\(String(format:"%.2f", (retailPrice as Double))) ".color(lightGrayColor).strikethrough(1).size(12)).attributedText
            cell.price.text = ""
            cell.saveRetail.text = ""
            cell.price.isHidden = true
            }
            
            else if  user_purchase_modeid == "2"
            {
                cell.saveRetail.isHidden = true
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                
                cell.price.attributedText = ("\(CurrencySymbol)\(String(format:"%.2f", bulkPrice))" .color(textcolor).size(20) + "\(self.product_price_tag)  ".color(textcolor).size(12) + "\(CurrencySymbol)\(String(format:"%.2f", (retailPrice as Double))) ".color(lightGrayColor).strikethrough(1).size(12)).attributedText
                cell.saveRetail.text = ""
                cell.price.isHidden = false
            }

            else if user_purchase_modeid == "1"
                {
                    let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                    cell.price.attributedText = ("\(CurrencySymbol)\(groupPrice)".color(textcolor).size(20) + "\(self.product_price_tag) ".color(textcolor).size(12) + "\(CurrencySymbol)\(String(format:"%.2f", retailPrice)) ".color(lightGrayColor).strikethrough(1).size(12)).attributedText
                    cell.price.isHidden = false
                }
            else
            {
                
                cell.price.attributedText = ("\(CurrencySymbol) \(bulkPrice)".color(textcolor).size(20) + "\(self.product_price_tag)  ".color(textcolor).size(12) + "\(CurrencySymbol)\(retailPrice) ".color(lightGrayColor).strikethrough(1).size(12)).attributedText
                cell.price.isHidden = false
            }
            }
            ReuseCell = cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "dynamicPrice", for: indexPath) as! DynamicPriceRangeTableViewCell
            cell.RangeCollection.tag = indexPath.row
            cell.RangeCollection.reloadData()
            ReuseCell = cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "shippingheader", for: indexPath) as! ShippingHeaderTableViewCell
            if self.shippingDetails.count>1
            {
                cell.ShippingHeader.text = "\(shippingDetails[0] as! String)  |  \(shippingDetails[1] as! String)"
            }
            else if self.shippingDetails.count>0
            {
                if shippingDetails[0] as! String == "\u{2022} Non-returnable"
                {
                    cell.arrowIcon.isHidden = true
                }
                else
                {
                cell.ShippingHeader.text = "\(shippingDetails[0] as! String)"
                    cell.arrowIcon.isHidden = false
                }
            }
            if self.isShipExpand == true
            {
                cell.arrowIcon.image = UIImage(named: "collapse")
            }
            else
            {
                cell.arrowIcon.image = UIImage(named: "Expand")
            }
            
            ReuseCell = cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "shippingBilling", for: indexPath) as! ShippingBillingTableViewCell
            print(shipping_profile_info)
            if self.shipping_profile_info != ""
            {
                cell.bullettext.attributedText =  NSAttributedString(html: self.shipping_profile_info)
            }
            ReuseCell = cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: "dealtablecell", for: indexPath) as! DealGroupTableViewCell
            if self.ProductDeal.count > 0
            {
            if self.ProductDeal[0]["interested_users"] is NSNull
            {}
            else
            {
                if self.ProductDeal[0]["interested_users"] as! NSNumber == 1
                {
                    cell.userInterested.text = "\(self.ProductDeal[0]["interested_users"] as! NSNumber) buyer interested in this product"
                }
                else
                {
            cell.userInterested.text = "\(self.ProductDeal[0]["interested_users"] as! NSNumber) buyers interested in this product"
                }
                }
                cell.dealAlertButton.addTarget(self, action: #selector(dealAlert), for: .touchUpInside)
                cell.viewAll.addTarget(self, action: #selector(openGroupView), for: .touchUpInside)
                if self.ProductDeal[0]["group_orderid"] as! NSNumber == 0
                {
                    cell.userInterested.isHidden = false
                    cell.viewAll.isHidden=true
                }
                else
                {
                    cell.userInterested.isHidden = true
                    cell.viewAll.isHidden=false
                }
            cell.dealCollection.tag = indexPath.row
            cell.dealCollection.reloadData()
            }
            
            ReuseCell = cell
        case 7:
            let cell = tableView.dequeueReusableCell(withIdentifier: "descriptionHeader", for: indexPath) as! DescriptionHeaderTableViewCell
            cell.showIcon.isHidden=true
//            if self.isDescExpand == true
//            {
//            cell.showIcon.image = UIImage(named: "collapse")
//            }
//            else
//            {
//            cell.showIcon.image = UIImage(named: "Expand")
//            }
            ReuseCell = cell
        case 8:
            let cell = tableView.dequeueReusableCell(withIdentifier: "description", for: indexPath) as! DescriptionTableViewCell
            cell.productDescription.attributedText = NSAttributedString(html: self.productDescription)
            if cell.productDescription.text!.count > 1 {
                
                let readmoreFont = UIFont(name: "Helvetica-Oblique", size: 11.0)
                var greencolor = UIColor(red:18/255, green:132/255, blue:136/255, alpha: 1)
                let readmoreFontColor = greencolor
                DispatchQueue.main.async {
                    cell.productDescription.addTrailing(with: "... ", moreText: "View More", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
                }
            }
            let tap = UITapGestureRecognizer(target: self, action: #selector(productDetail))
            cell.productDescription.isUserInteractionEnabled = true
            cell.productDescription.addGestureRecognizer(tap)

            
            let textcolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
            
            ReuseCell = cell
        
        case 9:
            let cell = tableView.dequeueReusableCell(withIdentifier: "shopproduct", for: indexPath) as! ShopProductTableViewCell
            cell.shopProductTitle.text = "Similar Product"
            cell.ShopProductCollection.tag = indexPath.row
            cell.ShopProductCollection.reloadData()
            ReuseCell = cell
        case 10:
            let cell = tableView.dequeueReusableCell(withIdentifier: "shopproduct", for: indexPath) as! ShopProductTableViewCell
            cell.shopProductTitle.text = "More Items from This shop"
            cell.ShopProductCollection.tag = indexPath.row
            cell.ShopProductCollection.reloadData()
            ReuseCell = cell
        default:
            
            break
        }
        
        return ReuseCell
    }
    

    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var gridSize = 0
        
        switch collectionView.tag {
        case 1:
            
            gridSize = self.ProductImg.count
        case 3:
            print("discount index is", 3, self.dynamic_pricing.count)
            gridSize = self.dynamic_pricing.count
        case 6:
            gridSize = self.ProductDeal.count
        case 9:
            gridSize = self.SimilarProduct.count
        case 10:
            gridSize = self.shop_product.count
        default:
            break
        }
        return gridSize
    }
    
    
//    func SetSessionPlayerOn()
//    {
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
//        } catch _ {
//        }
//        do {
//            try AVAudioSession.sharedInstance().setActive(true)
//        } catch _ {
//        }
//        do {
//            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
//        } catch _ {
//        }
//    }
//    func SetSessionPlayerOff()
//    {
//        do {
//            try AVAudioSession.sharedInstance().setActive(false)
//        } catch _ {
//        }
//    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        print("collection index", collectionView.tag)
        var ReuseCell = UICollectionViewCell()
        switch collectionView.tag {
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoandimageslider", for: indexPath) as! VideoAndImageSliderCollectionViewCell
            print("self.ProductImg", self.ProductImg)
            
           
                if indexPath.row == 0 && ProductVideo != ""
                {
                    let url = self.ProductVideo as String
                    if self.ProductVideo as String != ""
                    {
                        let videoURL = NSURL(string: url)
                        avPlayer = AVPlayer(url: videoURL! as URL)
                        avPlayer.volume = 10
                        avPlayer.isMuted = true
                        let playerController = AVPlayerViewController()
                        playerController.player = avPlayer
                        
                        self.addChildViewController(playerController)
                        
                        // Add your view Frame
                        
                        playerController.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                        playerController.view.frame = cell.videoView.bounds
                        playerController.showsPlaybackControls = true
                        
                        do {
                            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                        } catch _ {
                        }
                        do {
                            try AVAudioSession.sharedInstance().setActive(true)
                        } catch _ {
                        }
                        do {
                            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                        } catch _ {
                        }
                        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.none
                        
                        // Add subview in your view
                        cell.videoView.isHidden = false
                        cell.image.isHidden = true
                        cell.videoView.addSubview(playerController.view)
                        playerController.didMove(toParentViewController: self)

//                        avPlayer.play()
                        
                        
                        
                    }}
            else
                    if self.ProductImg.count > indexPath.row && indexPath.row > -1
            {
                cell.videoView.isHidden = true
                cell.image.isHidden = false
                var imageUrl = self.ProductImg[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                if let url = NSURL(string: imageUrl )
                {
                    if url != nil
                    {
                        DispatchQueue.main.async {
                           cell.image.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                       }
                    }
                }
            }
            
            
                        ReuseCell = cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "discountRange", for: indexPath) as! DynamicDiscountRageCollectionViewCell
            print("dynamic ragne:::::")
            for j in 0..<self.dynamic_pricing.count
            {
                if self.dynamic_pricing[j]["i"] as! Int == indexPath.row
                {
                    var low = "\((dynamic_pricing[j]["l"] as? NSNumber)!)"
                    var high = "\(Int((dynamic_pricing[j]["h"] as! NSNumber).floatValue))"
                    
                    
                    if indexPath.row < self.dynamic_pricing.count-1
                    {
                        cell.range.text = "\(low) - \(high) pcs"
                    }
                    else
                    {
                        cell.range.text = "≥ \(low) pcs"
                    }
                    cell.retailPrice.attributedText = (" \(CurrencySymbol) \(String(format:"%.2f", retailPrice)) ".lightGray.strikethrough(2)).attributedText
                    cell.bulkPrice.text = "\(CurrencySymbol) \(String(format:"%.2f", Double((self.dynamic_pricing[j]["dp"] as? NSNumber)!.floatValue)))"
                }
            }
            
            ReuseCell = cell
        case 6:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dealcollectioncell", for: indexPath) as! DealDetailCollectionViewCell
            
            cell.joinButton.layer.cornerRadius = 3
            scrollCollection(cltView: collectionView)
                if self.ProductDeal[indexPath.row]["group_orderid"] as! NSNumber == 0
                {
                    cell.joinButton.setTitle(" Start Group Buy ", for: .normal)
                    cell.topMessage.text = ""
                    cell.timeRemaining.text = ""
                    
                }
                else
                {
                
                cell.joinButton.setTitle(" Join Group Buy ", for: .normal)
                cell.topMessage.text = "Waiting for \(self.ProductDeal[indexPath.row]["waiting_user"] as! NSNumber) user "
                if self.ProductDeal[indexPath.row]["created_at"] is NSNull
                {}
                else
                {
                let time = self.ProductDeal[indexPath.row]["created_at"] as! NSNumber
                cell.localEndTime = TimeInterval(time.doubleValue)
                cell.startTimerProgress()
                    self.dealTime = "\(cell.timeRemaining)"
                }
                
            }
            cell.joinButton.clipsToBounds=true
            if self.ProductDeal[indexPath.row]["profile_pic_url"] is NSNull
            {
                cell.labelText.isHidden = false
                cell.labelText.layer.cornerRadius = cell.labelText.frame.height / 2
                cell.labelText.layer.masksToBounds = true
                if self.ProductDeal[indexPath.row]["user_name"] is NSNull
                {}
                else
                {
                var aString: String = self.ProductDeal[indexPath.row]["user_name"] as! String
                var aChar:unichar = (aString as NSString).character(at: 0)
                cell.labelText.text = "\(Character(UnicodeScalar(aChar)!))"
                }
            }
            else
            {
            var imageUrl = self.ProductDeal[indexPath.row]["profile_pic_url"] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                if let url = NSURL(string: imageUrl )
                {
                    if url != nil
                    {
                        cell.labelText.isHidden = true
                        DispatchQueue.main.async {
                           cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                       }
                    }
                    else
                    {
                        cell.labelText.isHidden = false
                        cell.labelText.layer.cornerRadius = cell.labelText.frame.height / 2
                        cell.labelText.layer.masksToBounds = true
                        cell.labelText.text = "A"
                    }
                }
                
            }
            if self.ProductDeal[indexPath.row]["user_name"] is NSNull
            {}
            else
            {
                cell.username.text = self.ProductDeal[indexPath.row]["user_name"] as! String
            }
           
            if native.object(forKey: "userid") != nil
            {
            let userid = native.string(forKey: "userid")!
            
            if self.ProductDeal[indexPath.row]["group_ownerid"] is NSNull
            {}
            else
            {
            let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
            let graycolor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1)
            if userid == "\(self.ProductDeal[indexPath.row]["group_ownerid"] as! NSNumber)"
            {
                cell.joinButton.isEnabled = false
                cell.joinButton.backgroundColor = UIColor.lightGray
            }
                else
            {
                cell.joinButton.isEnabled = true
                cell.joinButton.backgroundColor = textcolor
                }
            }
            }
            else
            {
                
            }
            cell.joinButton.tag = indexPath.row
            cell.joinButton.addTarget(self, action: #selector(openGroupAlert), for: .touchUpInside)
                
        
            ReuseCell = cell
        case 9:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sameShop", for: indexPath) as! SimilarProductCollectionViewCell
            let data = self.SimilarProduct[indexPath.row] as! NSDictionary
            
            let productImage = data["image_url"] as! AnyObject
            if productImage.count > 0
            {
                let image = productImage["200"] as! [AnyObject]
                if image.count > 0
                {
                    var imageUrl = image[0] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    
                    if let url = NSURL(string: imageUrl )
                    {
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                
                if data["deal_price"] is NSNull
                {}
                else
                {
                    let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                    let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                    var price = 0.0
                    var retailPrice = 0.0
                    if data["bulk_price"] is NSNull
                    {
                        price = Double(data["selling_price"] as! NSNumber)
                    }
                    else
                    {
                        price = Double(data["deal_price"] as! NSNumber)
                    }
                    if data["retail_price"] is NSNull
                    {
                        
                    }
                    else
                    {
                        retailPrice = Double(data["retail_price"] as! NSNumber)
                    }
                    if native.string(forKey: "Token")! == ""
                    {let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                        let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                        cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                    }
                    else
                    {
                        cell.price.attributedText = ("\(self.shopCurrency)\(String(format:"%.2f", price)) " .color(textcolor).size(11) + "\(self.shopCurrency)\(String(format:"%.2f", retailPrice))".color(graycolor).strikethrough(1).size(9)).attributedText
                    }
                
                    if self.Similar_productName.count > indexPath.row
                    {
                        cell.productName.text = self.Similar_productName[indexPath.row]
                    }
            }
               
            }
            ReuseCell = cell
        case 10:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sameShop", for: indexPath) as! SimilarProductCollectionViewCell
            let data = self.shop_product[indexPath.row] as! NSDictionary
            
            let productImage = data["image_url"] as! AnyObject
            if productImage.count > 0
            {
                let image = productImage["200"] as! [AnyObject]
                if image.count > 0
                {
                    var imageUrl = image[0] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    
                    if let url = NSURL(string: imageUrl )
                    {
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                
                if data["deal_price"] is NSNull
                {}
                else
                {
                    if data["currency_symbol"] is NSNull
                    {}
                    else
                    {
                        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                        var price = 0.0
                        var retailPrice = 0.0
                        if data["bulk_price"] is NSNull
                        {
                            price = Double(data["selling_price"] as! NSNumber)
                        }
                        else
                        {
                            price = Double(data["deal_price"] as! NSNumber)
                        }
                        if data["retail_price"] is NSNull
                        {
                            
                        }
                        else
                        {
                            retailPrice = Double(data["retail_price"] as! NSNumber)
                        }
                        if native.string(forKey: "Token")! == ""
                        {let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                            let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                            cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                        }
                        else
                        {
                            cell.price.attributedText = ("\(self.shopCurrency)\(String(format:"%.2f", price)) " .color(textcolor).size(11) + "\(self.shopCurrency)\(String(format:"%.2f", retailPrice))".color(graycolor).strikethrough(1).size(9)).attributedText
                        }
                    }}
                    if self.shop_productName.count > indexPath.row
                    {
                        cell.productName.text = self.shop_productName[indexPath.row]
                    }
                
                
            }
            ReuseCell = cell
            
        default:
            break
        }
        
        return ReuseCell
    }

    
    //autoscroll deal collection
    func scrollCollection(cltView: UICollectionView) {
        var r = self.ProductDeal.count - 1
        
        if self.ProductDeal.count>1
        {
         t1 = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
//            print(r)
            let index = IndexPath(item: r, section: 0)
            cltView.scrollToItem(at: index, at: .top, animated: true)
            
            if r != self.ProductDeal.count - 1 {
                r = r+1
            }else {
                r = 0
            }
        }
            t1!.fire()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize: CGSize = CGSize(width: 0.0, height: 0.0)
        let frameSize = collectionView.frame.size
        if collectionView.tag == 1
        {
            
        cellSize = CGSize(width: frameSize.width, height: frameSize.height)
        }
        else if collectionView.tag == 3
        {
          cellSize = CGSize(width: (frameSize.width-30)/3, height: frameSize.height)
        }
        else if collectionView.tag == 6
        {
            
            cellSize = CGSize(width: frameSize.width, height: frameSize.height)
        }
        else if collectionView.tag == 9 || collectionView.tag == 10
        {
            cellSize = CGSize(width: (frameSize.width-60)/3, height: frameSize.height)
        }
        return cellSize
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 1 {
                var AllImage = [URL]()
            if  ProductVideo == ""
            {
                if indexPath.row >= 0
                {
                for var i in 0..<self.ProductImg.count
                {
                var imageUrl = self.ProductImg[i] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    if let url = URL(string: imageUrl)
                {
                    if url != nil
                    {
                        AllImage.append(url)
                        print("allimage", AllImage)
                    }
                    }}
                print("allimage", AllImage)
//                    let imageDownloader = AlamofireImageDownloader()
//
//                    let viewController = Optik.imageViewer(withURLs: AllImage, imageDownloader: imageDownloader)
//                    present(viewController, animated: true, completion: nil)
                }}
            else if ProductVideo != ""
            {
                if indexPath.row > 0
                {
                for var i in 1..<self.ProductImg.count
                {
                    var imageUrl = self.ProductImg[i] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    if let url = URL(string: imageUrl)
                    {
                        if url != nil
                        {
                            AllImage.append(url)
                            print("allimage", AllImage)
                        }
                    }}
                print("allimage", AllImage)
//                let imageDownloader = AlamofireImageDownloader()
//                
//                let viewController = Optik.imageViewer(withURLs: AllImage, imageDownloader: imageDownloader)
//                present(viewController, animated: true, completion: nil)
                }}
        }
        if collectionView.tag == 9
        {
            let data = self.SimilarProduct[indexPath.row] as! NSDictionary
            native.set("product", forKey: "comefrom")
            native.set(data["product_variantid"] as! Int, forKey: "FavProID")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        else if collectionView.tag == 10
        {
            let data = self.shop_product[indexPath.row] as! NSDictionary
            native.set("product", forKey: "comefrom")
            native.set(data["product_variantid"] as! Int, forKey: "FavProID")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
//        else if collectionView.tag == 6
//        {
//            let userid = native.string(forKey: "userid")!
//            if userid == "\(self.ProductDeal[indexPath.row]["group_ownerid"] as! NSNumber)"
//            {  }
//            else
//            {
//                self.selectedCell = indexPath.row
//            if self.ProductDeal[indexPath.row]["group_orderid"] as! NSNumber == 0
//            {   order_typeid = 2
//                if native.string(forKey: "Token")! != ""
//                {
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let pvc = storyboard.instantiateViewController(withIdentifier: "updateVariant") as! UpdateProductVariantViewController
//                    pvc.modalPresentationStyle = .custom
//                    pvc.transitioningDelegate = self
//                    present(pvc, animated: true, completion: nil)
//                }
//                else
//                {
//                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.window?.rootViewController = redViewController
//                } }
//            else
//            {
//                self.order_typeid = 1
//                self.group_orderid = "\(self.ProductDeal[indexPath.row]["group_orderid"] as! NSNumber)"
//                self.group_ownerid = "\(self.ProductDeal[indexPath.row]["group_ownerid"] as! NSNumber)"
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let pvc = storyboard.instantiateViewController(withIdentifier: "groupDealAlert") as! GroupDealViewController
//                pvc.modalPresentationStyle = .custom
//                pvc.transitioningDelegate = self
//                present(pvc, animated: true, completion: nil)
//                }}
//        }
    }
    
    
    @objc func openGroupAlert(sender: UIButton)
    {
        self.selectedCell = sender.tag
        print("userdvo", native.object(forKey: "userid"))
        if native.object(forKey: "userid") != nil {
        let userid = native.string(forKey: "userid")!
        if userid == "\(self.ProductDeal[sender.tag]["group_ownerid"] as! NSNumber)"
        {
            
            print("ughjdc", self.ProductDeal[sender.tag]["group_ownerid"] as! NSNumber)
        }
        else
        {
            if self.ProductDeal[sender.tag]["group_orderid"] as! NSNumber == 0
            {   order_typeid = 2
                if native.string(forKey: "Token")! != ""
                {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let pvc = storyboard.instantiateViewController(withIdentifier: "updateVariant") as! UpdateProductVariantViewController
                    pvc.modalPresentationStyle = .custom
                    pvc.transitioningDelegate = self
                    present(pvc, animated: true, completion: nil)
                }
                else
                {
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                } }
            else
            {
                self.order_typeid = 1
                self.group_orderid = "\(self.ProductDeal[sender.tag]["group_orderid"] as! NSNumber)"
                self.group_ownerid = "\(self.ProductDeal[sender.tag]["group_ownerid"] as! NSNumber)"
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let pvc = storyboard.instantiateViewController(withIdentifier: "groupDealAlert") as! GroupDealViewController
                pvc.modalPresentationStyle = .custom
                pvc.transitioningDelegate = self
                present(pvc, animated: true, completion: nil)
            }}
        }
        else
        {
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    
    
    
}



extension UILabel {
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        
        answerAttributed.append(readMoreAttributed)
        
        self.attributedText = answerAttributed
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
        
    }
    
}




    // MARK: - Protocol conformance
    // MARK: ImageViewerDelegate
//    extension ViewController: ImageViewerDelegate {
//
//        func transitionImageView(for index: Int) -> UIImageView {
//            return localImagesButton.imageView!
//        }
//
//        func imageViewerDidDisplayImage(at index: Int) {
//            currentLocalImageIndex = index
//        }
//
//}
//
