//
//  ForgetPasswordNumberViewController.swift
//  Hub9
//
//  Created by Deepak on 9/6/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import CountryList



class ForgetPasswordNumberViewController:
UIViewController, CountryListDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    var native = UserDefaults.standard
    var countryList = CountryList()
    var passcode = 1
    var countrycode = 231
    
    @IBOutlet weak var selectCountryCode: UIButton!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectCountryCode.setTitle("\(passcode)", for: .normal)
        selectCountryCode.layer.cornerRadius = selectCountryCode.layer.frame.size.height/2
        selectCountryCode.layer.borderWidth = 1
        selectCountryCode.layer.borderColor = UIColor.gray.cgColor
        selectCountryCode.clipsToBounds = true
        selectCountryCode.isHidden = true
        phoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        countryList.delegate = self
        phoneNumber.becomeFirstResponder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        phoneNumber.delegate = self
        ///next button
        nextButton.layer.cornerRadius = nextButton.layer.frame.size.height/2
        nextButton.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 239/255.0, green: 130/255.0, blue: 136/255.0, alpha: 1.00).cgColor
        
        //        nextbuttonSignup.applyendGradient(colors: [loginstartColor, loginendColor])
        ///textfields
        let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
        leftView.backgroundColor = .clear
        
        phoneNumber.leftView = leftView
        phoneNumber.leftViewMode = .always
        var frame: CGRect = phoneNumber.frame
        frame.size.height = 40
        phoneNumber.frame = frame
        phoneNumber.layer.cornerRadius = phoneNumber.layer.frame.height/2
        phoneNumber.layer.borderWidth = 1
        phoneNumber.layer.borderColor = UIColor.lightGray.cgColor
        phoneNumber.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    // back button
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //selct country code
    @IBAction func selectCountryCode(_ sender: Any) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    ///country code
    func selectedCountry(country: Country) {
        print(country.name)
        print(country.flag)
        print(country.countryCode)
        print(country.phoneExtension)
        selectCountryCode.setTitle(country.phoneExtension, for: .normal)
        passcode = Int(country.phoneExtension)!
        country_lkpid(countryCode: country.name!)
        
    }
    //country lkpid
    func country_lkpid(countryCode: String)
    {
        if let urlPath = Bundle.main.url(forResource: "country", withExtension: "json") {
            
            do {
                let jsonData = try Data(contentsOf: urlPath, options: .mappedIfSafe)
                
                if let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: AnyObject] {
                    
                    if let personArray = jsonDict["response"] as? [[String: AnyObject]] {
//                        print("responce", personArray)
                        for personDict in personArray {
                            
                            if countryCode == personDict["country_name"] as! String
                            {
//                                print(personDict["country_lkpid"]!)
                                countrycode = Int((personDict["country_lkpid"] as! NSNumber ).floatValue)
                                break
                            }
                        }
                    }
                }
            }
                
            catch let jsonError {
                print(jsonError)
            }
        }
    }
    /////keybpard hide and show
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        phoneNumber.resignFirstResponder()
    
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y == 0{
                
                self.view.frame.origin.y -= 150
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y != 0{
                
                self.view.frame.origin.y += 150
            }

        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        print(textField.text)
        
        if (textField.text?.count)! > 0
        {
            if  validate(phoneNumber:  textField.text!) == true
            {
                selectCountryCode.isHidden = false
                let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: selectCountryCode.frame.width + 5, height: 26))
                leftView.backgroundColor = .clear
                
                phoneNumber.leftView = leftView
                phoneNumber.leftViewMode = .always
                var frame: CGRect = phoneNumber.frame
                frame.size.height = 40
                phoneNumber.frame = frame
                phoneNumber.layer.masksToBounds = true
                
                
            }
            else
            {
                selectCountryCode.isHidden = true
                let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
                leftView.backgroundColor = .clear
                
                phoneNumber.leftView = leftView
                phoneNumber.leftViewMode = .always
                var frame: CGRect = phoneNumber.frame
                frame.size.height = 40
                phoneNumber.frame = frame
                phoneNumber.layer.masksToBounds = true
                passcode = 91
            }
        }
        else
        {
            selectCountryCode.isHidden = true
            let leftView: UIView = UIView(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
            leftView.backgroundColor = .clear
            
            phoneNumber.leftView = leftView
            phoneNumber.leftViewMode = .always
            var frame: CGRect = phoneNumber.frame
            frame.size.height = 40
            phoneNumber.frame = frame
            phoneNumber.layer.masksToBounds = true
            passcode = 91
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = CharacterSet.whitespaces
        if let _ = string.rangeOfCharacter(from: whitespaceSet) {
            return false
        } else {
            return true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func displayErrorAndLogOut() {
        if Util.shared.checkNetworkTapped() {
            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .center)
            
        } else {
            self.view.makeToast("You have lost connectivity to the internet. Please self.check your connection and log in again.", duration: 2.0, position: .center)
            
        }
    }
    //send otp to this number
    @IBAction func submitButton(_ sender: Any) {
        
        if self.validation() == true
        {
            CustomLoader.instance.showLoaderView()
        native.set(phoneNumber.text, forKey: "phone")
        native.set(passcode, forKey: "country_code")
       
        native.synchronize()
        let b2burl = native.string(forKey: "b2burl")!
        let parameters: [String:Any] = ["username": phoneNumber.text!,"country_code":passcode]
        let header = ["Content-Type": "application/json"]
        print("Successfully post", parameters)
        
        Alamofire.request("\(b2burl)/users/request_reset_password/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            
            guard response.data != nil else { // can check byte count instead
                self.displayErrorAndLogOut()
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
                if let json = response.result.value {
                    print("JSON: \(json)")
                    let jsonString = "\(json)"
                    let result = response.result
                    
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        
                        switch response.result
                        {
                        case .failure(let error):
                            //                print(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                print("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                print(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                print("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                            else if (dict1["status"]as AnyObject) as! String  == "success"
                            {
                               
                                self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .bottom)
                                let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "resentOtp"))! as UIViewController
                                self.present(rootPage, animated: false, completion: nil)
                            }
                            else
                            {
                                 self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .bottom)
                            }
                            
                        }
                        return
                    
                }
            }
        }
            }}
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
           self.view.makeToast("please enter valid number first!", duration: 2.0, position: .center)
        }
    }
    // term of app
    @IBAction func termButton(_ sender: Any) {
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "termsAndCondition"))! as UIViewController
        self.present(rootPage, animated: false, completion: nil)
    }
    
    
    func validate(phoneNumber: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phoneNumber.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phoneNumber == filtered
    }
    // validation
    func validation() -> Bool {
        var valid: Bool = true
        if ((phoneNumber.text?.count)! < 9) || ((phoneNumber.text?.count)! > 13) {
            // change placeholder color to red color for textfield email-id
            self.view.makeToast("please enter valid number!", duration: 2.0, position: .center)
            valid = false
        }
        if validate(phoneNumber: phoneNumber.text!) == false
        {
            phoneNumber.text?.removeAll()
            phoneNumber.attributedPlaceholder = NSAttributedString(string: "invalid number", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        return valid
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
