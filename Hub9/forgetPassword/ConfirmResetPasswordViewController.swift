//
//  ConfirmResetPasswordViewController.swift
//  Hub9
//
//  Created by Deepak on 9/6/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire

class ConfirmResetPasswordViewController: UIViewController, UITextFieldDelegate {
    
    var native = UserDefaults.standard
    var confirmIconClick = false
    var newIconClick = false

    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var newPassShow: UIButton!
    @IBOutlet weak var confirmshow: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // password
        CustomLoader.instance.showLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        let passwordleftView = UILabel(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
        passwordleftView.backgroundColor = .clear
        let passwordrightView = UILabel(frame: CGRect(x: 10, y: 0, width: 50, height: 26))
        passwordrightView.backgroundColor = .clear
        newPassword.rightView = passwordrightView
        newPassword.rightViewMode = .always
        newPassword.leftView = passwordleftView
        newPassword.leftViewMode = .always
        var passwordframe: CGRect = newPassword.frame
        passwordframe.size.height = 40
        newPassword.frame = passwordframe
        newPassword.layer.cornerRadius = newPassword.layer.frame.height/2
        newPassword.layer.borderWidth = 1
        newPassword.layer.borderColor = UIColor.lightGray.cgColor
        newPassword.layer.masksToBounds = true
        //confirm password
        let confirmleftView = UILabel(frame: CGRect(x: 10, y: 0, width: 15, height: 26))
        confirmleftView.backgroundColor = .clear
        let confirmrightView = UILabel(frame: CGRect(x: 10, y: 0, width: 50, height: 26))
        confirmrightView.backgroundColor = .clear
        confirmPassword.rightView = confirmrightView
        confirmPassword.rightViewMode = .always
        confirmPassword.leftView = confirmleftView
        confirmPassword.leftViewMode = .always
        var passwordframe1: CGRect = confirmPassword.frame
        passwordframe1.size.height = 40
        confirmPassword.frame = passwordframe1
        confirmPassword.layer.cornerRadius = confirmPassword.layer.frame.height/2
        confirmPassword.layer.borderWidth = 1
        confirmPassword.layer.borderColor = UIColor.lightGray.cgColor
        confirmPassword.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /////keybpard hide and show
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        newPassword.resignFirstResponder()
        confirmPassword.resignFirstResponder()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 50
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 50
            }
        }
    }
    
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = CharacterSet.whitespaces
        if let _ = string.rangeOfCharacter(from: whitespaceSet) {
            return false
        } else {
            return true
        }
    }
    
    @IBAction func newPassShow(_ sender: Any) {
        if(newIconClick == true) {
            newPassword.isSecureTextEntry = false
            newPassShow.setImage(UIImage(named: "showpassword"), for: .normal)
        }
        else
        {
            newPassword.isSecureTextEntry = true
            newPassShow.setImage(UIImage(named: "hidepassword"), for: .normal)
        }
        
        let tempString = newPassword.text
        newPassword.text = nil
        newPassword.text = tempString
        newIconClick = !newIconClick
        
    }
    
    @IBAction func confirmPassShow(_ sender: Any) {
        if(confirmIconClick == true) {
            confirmPassword.isSecureTextEntry = false
            confirmshow.setImage(UIImage(named: "showpassword"), for: .normal)
        }
        else
        {
            confirmPassword.isSecureTextEntry = true
            confirmshow.setImage(UIImage(named: "hidepassword"), for: .normal)
        }
        
        let tempString = confirmPassword.text
        confirmPassword.text = nil
        confirmPassword.text = tempString
        confirmIconClick = !confirmIconClick
    }
    
    // submit button
    @IBAction func submitButton(_ sender: Any) {
        if (newPassword.text != confirmPassword.text)
        {
            self.view.makeToast("Passwords do not match!", duration: 2.0, position: .center)
        }
        else
        {
        if confirmPassword.text!.count<6
        {
            self.view.makeToast("Password must contain atleast 6 characters!", duration: 2.0, position: .center)
        }
        else
        {
            CustomLoader.instance.showLoaderView()
            var key = native.string(forKey: "key")!
            let parameters: [String:Any] = ["key":key, "password": newPassword.text!]
            let header = ["Content-Type": "application/json"]
            let b2burl = native.string(forKey: "b2burl")!
            print("Successfully post", parameters)
            
            Alamofire.request("\(b2burl)/users/reset_password/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                if let json = response.result.value {
                    let jsonString = "\(json)"
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        print(result)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            else if dict1["status"] as! String  == "success"
                            {
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .bottom)
                                print("87674guyguy", dict1["response"] as! String)
                                let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "login"))! as UIViewController
                                    self.present(rootPage, animated: false, completion: nil)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                            else
                            {
                                
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                                
                            }
                        }
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                }
            }
            
            }}
        
    }
}
