//
//  UserProfileTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/4/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class UserProfileTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var texttochange: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
