//
//  CartProductCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 6/7/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class CartProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var materialType: UILabel!
    
}
