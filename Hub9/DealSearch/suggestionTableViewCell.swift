//
//  suggestionTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 13/07/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class suggestionTableViewCell: UITableViewCell {

    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var category: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
