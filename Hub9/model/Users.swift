//
//  Users.swift
//  Hub9
//
//  Created by Deepak on 12/18/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import Foundation
import RealmSwift



// Realm User Object Model

class Users: Object {
    
    @objc dynamic var device = "iOS"
    @objc dynamic var firstName = ""
    @objc dynamic var lastname = ""
    @objc dynamic var firebaseid = ""
    @objc dynamic var shopid = ""
    @objc dynamic var userid = ""
    @objc dynamic var companyid = ""
    @objc dynamic var country_lkpid = ""
    @objc dynamic var accountType_lkpid = ""
    @objc dynamic var profilePic = ""
    @objc dynamic var token = ""
    @objc dynamic var isuserUpdate = ""
    @objc dynamic var isfirebase_chat = ""
    @objc dynamic var lastLoginAt = ""
    
    //primary key
    override static func primaryKey() -> String? {
        return "firebaseid"
}
}
