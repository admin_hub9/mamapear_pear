//
//  Contact.swift
//  Hub9
//
//  Created by Deepak on 12/18/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import Foundation
import RealmSwift



// Realm User Object Model

class Contact: Object {
    
    @objc dynamic var shopfirebaseid = ""
    @objc dynamic var shoplogo = ""
    @objc dynamic var chatroomid = ""
    @objc dynamic var shoname = ""
    @objc dynamic var createdAt = ""
    
    //primary key
    override static func primaryKey() -> String? {
        return "shopfirebaseid"
    }
    
}
