////
////  test.swift
////  Hub9
////
////  Created by Deepak on 11/16/18.
////  Copyright © 2018 Deepak. All rights reserved.
////
//
//
//
//
//import Foundation
//import UIKit
//import Firebase
//import FirebaseAuth
//import FirebaseFirestore
//
//class Message {
//    
//    //MARK: Properties
//    var owner: MessageOwner
//    var type: MessageType
//    var content: Any
//    var timestamp: Int
//    var isRead: Bool
//    var image: UIImage?
//    private var toID: String?
//    private var fromID: String?
//    
//    
//    //MARK: Methods
//    class func downloadAllMessages(forUserID: String, completion: @escaping (Message) -> Swift.Void) {
//        let currentUserID = "D95Ak21s3Tbk7zI5ML97dHN6wVw2"
//        
//        let db = Firestore.firestore()
//        print("calling....")
//        db.collection("messages").addSnapshotListener() { (querySnapshot, err) in
//            
//            
//            
//            guard let snapshot = querySnapshot else {
//                print("Error retreiving snapshots \(err!)")
//                return
//            }
//            
//            snapshot.documentChanges.forEach { diff in
//                if (diff.type == .added){
//                    
//                    
//                    guard let receivedMessage: [String:Any] = diff.document.data()
//                        else {
//                            return
//                    }
//                    
//                    let messageId = receivedMessage["chatRoomId"] as? String
//                    if  messageId == "pCGMAWdn74lC6LY3bmqH"
//                    {
//                        let messageType = receivedMessage["type"] as! String
//                        var content = ""
//                        var fromID = ""
//                        var timestamp = 0
//                        var type = MessageType.text
//                        //                            print("type ", MessageType.text)
//                        switch messageType {
//                        case "text":
//                            type = .text
//                            content = receivedMessage["content"] as! String
//                            fromID = receivedMessage["sender"] as! String
//                            timestamp = receivedMessage["messageTime"] as! Int
//                        case "image":
//                            type = .photo
//                            content = receivedMessage["content"] as! String
//                            fromID = receivedMessage["sender"] as! String
//                            timestamp = receivedMessage["messageTime"] as! Int
//                        case "quotationJson":
//                            type = .Quotation
//                            let array = receivedMessage["content"] as! NSMutableArray
//                            //                                print("fhgfgf",array)
//                            let data = array[0] as! [String:Any]
//                            //                                print(data["product_name"]!)
//                            
//                            //                                content = data["content"] as! String
//                            //                                 fromID = data["sender"] as! String
//                        //                                 timestamp = array["messageTime"] as! Int
//                        default: break
//                        }
//                        
//                        if fromID == currentUserID {
//                            let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true)
//                            completion(message)
//                        } else {
//                            let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true)
//                            completion(message)
//                        }
//                        
//                    }
//                }
//                if(diff.type == .modified) {
//                    print("modified the document in firestore")
//                }
//            }
//        }
//        //            Database.database().reference().child("users").child(currentUserID).child("conversations").child(forUserID).observe(.value, with: { (snapshot) in
//        //                if snapshot.exists() {
//        //                    let data = snapshot.value as! [String: String]
//        //                    let location = data["location"]!
//        //                    Database.database().reference().child("conversations").child(location).observe(.childAdded, with: { (snap) in
//        //                        if snap.exists() {
//        //                            let receivedMessage = snap.value as! [String: Any]
//        //                            let messageType = receivedMessage["type"] as! String
//        //                            var type = MessageType.text
//        //                            switch messageType {
//        //                                case "photo":
//        //                                type = .photo
//        //                                case "location":
//        //                                type = .location
//        //                            default: break
//        //                            }
//        //                            let content = receivedMessage["content"] as! String
//        //                            let fromID = receivedMessage["fromID"] as! String
//        //                            let timestamp = receivedMessage["timestamp"] as! Int
//        //                            if fromID == currentUserID {
//        //                                let message = Message.init(type: type, content: content, owner: .receiver, timestamp: timestamp, isRead: true)
//        //                                completion(message)
//        //                            } else {
//        //                                let message = Message.init(type: type, content: content, owner: .sender, timestamp: timestamp, isRead: true)
//        //                                completion(message)
//        //                            }
//        //                        }
//        //                    })
//        //                }
//        //            })
//        
//        
//    }
//    
//    func downloadImage(indexpathRow: Int, completion: @escaping (Bool, Int) -> Swift.Void)  {
//        if self.type == .photo {
//            let imageLink = self.content as! String
//            let imageURL = URL.init(string: imageLink)
//            URLSession.shared.dataTask(with: imageURL!, completionHandler: { (data, response, error) in
//                if error == nil {
//                    self.image = UIImage.init(data: data!)
//                    completion(true, indexpathRow)
//                }
//            }).resume()
//        }
//    }
//    
//    class func markMessagesRead(forUserID: String)  {
//        if let currentUserID = Auth.auth().currentUser?.uid {
//            
//            
//            //            Database.database().reference().child("users").child(currentUserID).child("conversations").child(forUserID).observeSingleEvent(of: .value, with: { (snapshot) in
//            //                if snapshot.exists() {
//            //                    let data = snapshot.value as! [String: String]
//            //                    let location = data["location"]!
//            //                    Database.database().reference().child("conversations").child(location).observeSingleEvent(of: .value, with: { (snap) in
//            //                        if snap.exists() {
//            //                            for item in snap.children {
//            //                                let receivedMessage = (item as! DataSnapshot).value as! [String: Any]
//            //                                let fromID = receivedMessage["sender"] as! String
//            //                                if fromID != currentUserID {
//            //                                    Database.database().reference().child("conversations").child(location).child((item as! DataSnapshot).key).child("isRead").setValue(true)
//            //                                }
//            //                            }
//            //                        }
//            //                    })
//            //                }
//            //            })
//        }
//    }
//    
//    func downloadLastMessage(forLocation: String, completion: @escaping () -> Swift.Void) {
//        
//        
//        let db = Firestore.firestore()
//        db.collection("messages").addSnapshotListener() { (querySnapshot, err) in
//            guard let snapshot = querySnapshot else {
//                print("Error retreiving snapshots \(err!)")
//                return
//            }
//            var count = snapshot.count
//            var newcount = 0
//            snapshot.documentChanges.forEach { diff in
//                if (diff.type == .added){
//                    //                        print("added")
//                    
//                    newcount = newcount + 1
//                    if count == newcount
//                    {
//                        let receivedMessage: [String:Any] = diff.document.data()
//                        
//                        let messageId = receivedMessage["chatRoomId"] as? String
//                        if  messageId == "pCGMAWdn74lC6LY3bmqH"
//                        {
//                            self.content = receivedMessage["content"]!
//                            self.timestamp = receivedMessage["messageTime"] as! Int
//                            let messageType = receivedMessage["type"] as! String
//                            let fromID = receivedMessage["sender"] as! String
//                            self.isRead = false
//                            var type = MessageType.text
//                            switch messageType {
//                            case "content":
//                                type = .text
//                            case "image":
//                                type = .photo
//                            case "quotationJson":
//                                type = .Quotation
//                            default: break
//                            }
//                            self.type = type
//                            let currentUserID = "D95Ak21s3Tbk7zI5ML97dHN6wVw2"
//                            if currentUserID == fromID {
//                                self.owner = .receiver
//                            } else {
//                                self.owner = .sender
//                            }
//                            completion()
//                            
//                        }
//                    }
//                }
//                if(diff.type == .modified) {
//                    print("modified the document in firestore")
//                }
//            }
//            
//        }
//        //            Database.database().reference().child("conversations").child(forLocation).observe(.value, with: { (snapshot) in
//        //                if snapshot.exists() {
//        //                    for snap in snapshot.children {
//        //                        let receivedMessage = (snap as! DataSnapshot).value as! [String: Any]
//        //                        self.content = receivedMessage["content"]!
//        //                        self.timestamp = receivedMessage["timestamp"] as! Int
//        //                        let messageType = receivedMessage["type"] as! String
//        //                        let fromID = receivedMessage["fromID"] as! String
//        //                        self.isRead = receivedMessage["isRead"] as! Bool
//        //                        var type = MessageType.text
//        //                        switch messageType {
//        //                        case "text":
//        //                            type = .text
//        //                        case "photo":
//        //                            type = .photo
//        //                        case "location":
//        //                            type = .location
//        //                        default: break
//        //                        }
//        //                        self.type = type
//        //                        if currentUserID == fromID {
//        //                            self.owner = .receiver
//        //                        } else {
//        //                            self.owner = .sender
//        //                        }
//        //                        completion()
//        //                    }
//        //                }
//        //            })
//        
//    }
//    
//    class func send(message: Message, toID: String, completion: @escaping (Bool) -> Swift.Void)  {
//        let currentUserID =  "D95Ak21s3Tbk7zI5ML97dHN6wVw2"
//        switch message.type {
//        case .Quotation:
//            let values = ["type": "location", "content": message.content, "fromID": currentUserID, "toID": "pCGMAWdn74lC6LY3bmqH", "messageTime": Date().millisecondsSince1970, "isRead": false]
//            Message.uploadMessage(withValues: values, toID: "pCGMAWdn74lC6LY3bmqH", completion: { (status) in
//                completion(status)
//            })
//        case .photo:
//            let imageData = UIImageJPEGRepresentation((message.content as! UIImage), 0.5)
//            let child = UUID().uuidString
//            
//            Storage.storage().reference().child("messagePics").child(child).putData(imageData!, metadata: nil, completion: { (metadata, error) in
//                //                    if error == nil {
//                //
//                //
//                //                        let path = metadata?.downloadURL()?.absoluteString
//                //                        let values = ["type": "image", "content": path!, "sender": currentUserID, "chatRoomId": "pCGMAWdn74lC6LY3bmqH", "messageTime": Date().millisecondsSince1970, "isRead": false] as [String : Any]
//                //                        Message.uploadMessage(withValues: values, toID: "pCGMAWdn74lC6LY3bmqH", completion: { (status) in
//                //                            completion(status)
//                //                        })
//                //                    }
//            })
//        case .text:
//            print("revdgbvgdb")
//            let values = ["type": "text", "content": message.content, "sender": currentUserID, "chatRoomId": "pCGMAWdn74lC6LY3bmqH", "messageTime": Date().millisecondsSince1970, "isRead": false]
//            
//            //                let values = ["type": "text", "content": message.content, "fromID": currentUserID, "toID": toID, "timestamp": message.timestamp, "isRead": false]
//            Message.uploadMessage(withValues: values, toID: "pCGMAWdn74lC6LY3bmqH", completion: { (status) in
//                completion(status)
//            })
//        }
//        
//    }
//    
//    class func uploadMessage(withValues: [String: Any], toID: String, completion: @escaping (Bool) -> Swift.Void) {
//        
//        
//        
//        
//        let currentUserID = "D95Ak21s3Tbk7zI5ML97dHN6wVw2"
//        let db = Firestore.firestore()
//        let citiesRef = db.collection("messages")
//        citiesRef.document().setData(withValues)
//        completion(true)
//        //            Database.database().reference().child("users").child(currentUserID).child("conversations").child(toID).observeSingleEvent(of: .value, with: { (snapshot) in
//        //                if snapshot.exists() {
//        //                    let data = snapshot.value as! [String: String]
//        //                    let location = data["location"]!
//        //                    Database.database().reference().child("conversations").child(location).childByAutoId().setValue(withValues, withCompletionBlock: { (error, _) in
//        //                        if error == nil {
//        //                            completion(true)
//        //                        } else {
//        //                            completion(false)
//        //                        }
//        //                    })
//        //                } else {
//        //                    Database.database().reference().child("conversations").childByAutoId().childByAutoId().setValue(withValues, withCompletionBlock: { (error, reference) in
//        //                        let data = ["location": reference.parent!.key]
//        //                        Database.database().reference().child("users").child(currentUserID).child("conversations").child(toID).updateChildValues(data)
//        //                        Database.database().reference().child("users").child(toID).child("conversations").child(currentUserID).updateChildValues(data)
//        //                        completion(true)
//        //                    })
//        //                }
//        //            })
//        //
//    }
//    
//    //MARK: Inits
//    init(type: MessageType, content: Any, owner: MessageOwner, timestamp: Int, isRead: Bool) {
//        self.type = type
//        self.content = content
//        self.owner = owner
//        self.timestamp = timestamp
//        self.isRead = isRead
//    }
//}
//
//
//extension Date {
//    var millisecondsSince1970:Int {
//        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
//    }
//    
//    init(milliseconds:Int) {
//        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
//    }
//}
//
