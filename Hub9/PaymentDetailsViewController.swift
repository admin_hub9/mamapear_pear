//
//  PaymentDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 6/1/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import AttributedTextView
import SDWebImage

var PaymentDetails = PaymentDetailsViewController()

class PaymentDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITabBarDelegate {
    
    var data = [[AnyObject]]()
    var Amount = 0.0
    var shopid = 0
    var MCV = 0.0
    var currentIndex = 0
    var productTitle = [String]()
    var variantID = [Int]()
    var Quantity = [Int]()
    var currency = ""
    var order_typeid = 1
    var productMOQ = true
    var min_order_qty = true
    var user_purchase_modeid = "1"
    var productData = [AnyObject]()
    var paymentMethod = [AnyObject]()
    
    ///charges data from api
    var cod_charge = 0.0
    var ship_insurance = 0.0
    var shipping_charge = 0.0
    var product_tax_value = 0.0
    var wallet_value = 0.0
    var bb_coin = 0.0
    
    
    
    
    @IBOutlet weak var TotalQuantity: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var shopIcon: UIImageView!
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var shopAddress: UILabel!
    @IBOutlet weak var shopView: UIView!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var mcvAlert: UILabel!
    
    @IBOutlet weak var PaynowContainer: UIView!
    @IBOutlet weak var shopContainer: UIView!
    @IBOutlet weak var emptyView: UIView!
    
    
    ///proceed to pay
    
    @IBAction func back(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func redirectShop(_ sender: Any) {
        var id = self.shopid
        native.set(id, forKey: "entershopid")
        native.synchronize()
        debugPrint("product id:", id)
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shopDetails"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        debugPrint("tableView.tag", tableView.tag, data.count)
        if data.count > 0
        {
            self.emptyView.isHidden = true
            self.AddtoCartTable.isHidden = false
        }
        else
        {
            self.emptyView.isHidden = false
            self.AddtoCartTable.isHidden = true
        }
        if tableView.tag == 100
        {
            debugPrint(tableView.tag,"get refreshed")
            return data.count
        }
        else if tableView.tag == 90
        {
            debugPrint(tableView.tag,"get refreshed")
            debugPrint("currentIndex", data[currentIndex].count, data.count, data[currentIndex] )
            
            return data[currentIndex].count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 100
        {
            currentIndex = indexPath.row
            debugPrint(indexPath.row,"get refreshed")
        }
        else if tableView.tag == 90
        {
            //            let PaymentData = data[currentIndex] as! [AnyObject]
            //            let data1 = PaymentData[indexPath.row] as! NSDictionary
            //            var VariantID = (data1["product_variantid"] as? Int)!
            //            native.set(VariantID, forKey: "FavProID")
            //            native.set("product", forKey: "comefrom")
            //            native.synchronize()
            //            //        currntProID = self.ProductID[indexPath.row] as! Int
            //
            //            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            //            self.navigationController?.pushViewController(editPage, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReuseCell = UITableViewCell()
        if tableView.tag == 100
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Maincart") as! MainTableViewCell
            currentIndex = indexPath.row
            //            let viewSeparatorLine = UIView(frame:CGRect(x: 0, y: cell.contentView.frame.size.height - 5.0, width: cell.contentView.frame.size.width, height: 5))
            //            let sepratorColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
            //            viewSeparatorLine.backgroundColor = sepratorColor
            //            cell.contentView.addSubview(viewSeparatorLine)
            
            let PaymentData = data[indexPath.row] as! [AnyObject]
            var moq = 0
            for i in 0..<PaymentData.count
            {
                let data1 = PaymentData[i] as! NSDictionary
                debugPrint("data1moqcheck", i, data1["quantity"]!)
                moq = moq + (data1["quantity"] as? Int)!
                
            }
            
            if self.order_typeid == 3 || self.order_typeid == 1
            {
                if moq < self.Quantity[indexPath.row]
                {
                    debugPrint("alertMOQ")
                    
                    cell.productMoqAlert.isHidden = false
                    cell.productMoqAlert.text = "MOQ of this Product is \(self.Quantity[indexPath.row]). Add atleast \(self.Quantity[indexPath.row] - moq) more to proceed"
                }
                else
                {
                    cell.productMoqAlert.isHidden = true
                }
            }
            else
            {
                cell.productMoqAlert.isHidden = true
            }
            cell.insideTable.tag = 90
            cell.insideTable.reloadData()
            return cell
        }
        else if tableView.tag == 90 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addtoCart") as! AddtoCartTableCell
            
            let PaymentData = data[currentIndex] as! [AnyObject]
            debugPrint("PaymentData", PaymentData)
            let disablecolor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 0.3)
            let grayColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 0.5)
            let blackcolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
            cell.delete.addTarget(self, action: #selector(sumbitOrContinue), for: .touchUpInside)
            let deletecolor = UIColor(displayP3Red: 18, green: 132, blue: 136, alpha: 1)
            cell.delete.tintColor = deletecolor
            //            debugPrint(PaymentData)
            let data1 = PaymentData[indexPath.row] as! NSDictionary
            //            debugPrint(data1["color_ui"])
            if data1["selling_price"] is NSNull
            {}
            else
            {
                cell.total.text = "\((data1["currency_symbol"] as? String)!) \((data1["selling_price"] as? String)!)"
            }
            cell.totalUnit.text = "\((data1["color_ui"] as? String)!) / \((data1["size_ui"] as? String)!)"
            cell.delete.tag = (data1["product_variantid"] as? Int)!
            
            let origImage = UIImage(named: "minus")
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.minusButton.setImage(tintedImage, for: .normal)
            let plus = UIImage(named: "plus")
            let tintedPlusImage = plus?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.plusButton.setImage(tintedPlusImage, for: .normal)
            
            if (data1["quantity"] as? Int) == 1            {
                cell.plusButton.tintColor = grayColor
                cell.minusButton.tintColor = disablecolor
                cell.quantity.setTitleColor(grayColor, for: .normal)
                cell.plusButton.isEnabled = true
                cell.minusButton.isEnabled = false
                cell.quantity.isEnabled = true
            }
            else if ((data1["quantity"] as? Int)!) < 1            {
                cell.plusButton.tintColor = disablecolor
                cell.minusButton.tintColor = disablecolor
                cell.quantity.setTitleColor(disablecolor, for: .normal)
                cell.plusButton.isEnabled = false
                cell.minusButton.isEnabled = false
                cell.quantity.isEnabled = false
            }
            else
            {
                cell.plusButton.tintColor = grayColor
                cell.minusButton.tintColor = grayColor
                cell.quantity.setTitleColor(blackcolor, for: .normal)
                cell.plusButton.isEnabled = true
                cell.minusButton.isEnabled = true
                cell.quantity.isEnabled = true
            }
            cell.quantity.setTitle("\(data1["quantity"]!)", for: .normal)
            cell.minusButton.tag = (data1["product_variantid"] as? Int)!
            cell.minusButton.addTarget(self, action: #selector(MinusQunatity(sender:)), for: .touchUpInside)
            cell.plusButton.tag = (data1["product_variantid"] as? Int)!
            cell.plusButton.addTarget(self, action: #selector(PlusQunatity(sender:)), for: .touchUpInside)
            cell.quantity.tag = (data1["product_variantid"] as? Int)!
            cell.quantity.addTarget(self, action: #selector(BulkUpdate(sender:)), for: .touchUpInside)
            if let images = data1["product_image_url"] as? AnyObject
            {
                let imagedata = images["200"] as! [AnyObject]
                if imagedata.count > 0
                {
                    
                    var imageUrl = imagedata[0] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    let url = NSURL(string: imageUrl)
                    if url != nil{
                        DispatchQueue.main.async {
                            cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    } }
            }
            cell.name.text = (productTitle[currentIndex] as? String)!
            
            return cell
            
        }
        else
        {
            return ReuseCell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellToReturn = UITableViewCell()
        if tableView.tag == 90
        {
            return 120
            
        }
        else {
            
            return CGFloat((120 * data[currentIndex].count) + 20)
        }
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    
    @objc func PlusQunatity(sender: UIButton)
    {
        
        
        let shopid = String(self.shopid)
        //        debugPrint("tag", sender.tag, sender.selectedSegmentIndex)
        var productVariantId = sender.tag
        var quantity = 0
        
        for var i in 0..<self.data.count
        {   let PaymentData = data[i] as! [AnyObject]
            for var j in 0..<data[i].count
            {   let data1 = PaymentData[j] as! NSDictionary
                debugPrint(data1["color_ui"])
                if data1["product_variantid"] as! Int == sender.tag
                {
                    debugPrint("found")
                    quantity = data1["quantity"] as! Int
                    break
                }
                else
                {
                }
            }
        }
        quantity = quantity + 1
        //                    CustomLoader.instance.showLoaderView()
        update_product(productVariantID: "\(productVariantId)", quantity: "\(quantity)", shopID: shopid)
        AddtoCartTable.beginUpdates()
        AddtoCartTable.endUpdates()
        self.AddtoCartTable.reloadData()
    }
    @objc func MinusQunatity(sender: UIButton)
    {
        
        
        let shopid = String(self.shopid)
        //        debugPrint("tag", sender.tag, sender.selectedSegmentIndex)
        var productVariantId = sender.tag
        var quantity = 0
        
        for var i in 0..<self.data.count
        {   let PaymentData = data[i] as! [AnyObject]
            for var j in 0..<data[i].count
            {   let data1 = PaymentData[j] as! NSDictionary
                debugPrint(data1["color_ui"])
                if data1["product_variantid"] as! Int == sender.tag
                {
                    debugPrint("found")
                    quantity = data1["quantity"] as! Int
                    break
                }
                else
                {
                }
            }
        }
        if quantity > 1
        {
            quantity = quantity - 1
            update_product(productVariantID: "\(productVariantId)", quantity: "\(quantity)", shopID: shopid)
            AddtoCartTable.beginUpdates()
            AddtoCartTable.endUpdates()
            self.AddtoCartTable.reloadData()
        }
        
    }
    @objc func BulkUpdate(sender: UIButton)
    {
        AddtoCartTable.beginUpdates()
        updateBulkQuantity(productVariantId: sender.tag)
        AddtoCartTable.endUpdates()
        self.AddtoCartTable.reloadData()
    }
    
    
    
    //    @objc func updateUnit(sender: UISegmentedControl)
    //    {
    //        debugPrint("tag", sender.tag, sender.selectedSegmentIndex)
    //        var productVariantId = sender.tag
    //        var quantity = 0
    //
    //        for var i in 0..<self.data.count
    //        {   let PaymentData = data[i] as! [AnyObject]
    //            for var j in 0..<data[i].count
    //            {   let data1 = PaymentData[j] as! NSDictionary
    //                debugPrint(data1["color_ui"])
    //                if data1["product_variantid"] as! Int == sender.tag
    //                {
    //                    debugPrint("found")
    //                    quantity = data1["quantity"] as! Int
    //                    break
    //                }
    //                else
    //                {
    ////                    CustomLoader.instance.hideLoaderView()
    ////                    UIApplication.shared.endIgnoringInteractionEvents();                    debugPrint("not found")
    //                }
    //                // new correct declaration
    //                let result = data[i]
    //            }
    //        }
    ////
    //            switch sender.selectedSegmentIndex
    //            {
    //            case 0:
    //                /// minus quantity
    //
    //                    let shopid = String(self.shopid)
    //                    quantity = quantity - 1
    ////                    CustomLoader.instance.showLoaderView()
    //                    update_product(productVariantID: "\(productVariantId)", quantity: "\(quantity)", shopID: shopid)
    //
    //                    AddtoCartTable.beginUpdates()
    //                    AddtoCartTable.endUpdates()
    //                self.AddtoCartTable.reloadData()
    //            case 1:
    //                //bulk update in quantity
    ////                value1.blurView.alpha = 1
    ////                value1.activity.startAnimating()
    //
    //                AddtoCartTable.beginUpdates()
    //                updateBulkQuantity(productVariantId: productVariantId)
    //                AddtoCartTable.endUpdates()
    //                self.AddtoCartTable.reloadData()
    //
    //
    //            case 2:
    //
    //               // plus in quantity
    ////                    value1.blurView.alpha = 1
    ////                    value1.activity.startAnimating()
    //
    //                    let shopid = String(self.shopid)
    //                    quantity = quantity + 1
    ////                    CustomLoader.instance.showLoaderView()
    //                    update_product(productVariantID: "\(productVariantId)", quantity: "\(quantity)", shopID: shopid)
    //                    AddtoCartTable.beginUpdates()
    //                    AddtoCartTable.endUpdates()
    //                self.AddtoCartTable.reloadData()
    //
    //            default:
    //                break
    //            }
    //
    //    }
    
    func update_product(productVariantID: String, quantity: String, shopID: String)
    {
        if Int(quantity)! > 0
        {
            CustomLoader.instance.showLoaderView()
            let parameters: [String:Any] = ["product_data":[["product_variantid":productVariantID, "quantity": quantity]], "shopid": shopID, "order_typeid": order_typeid]
            debugPrint("rrrrr",parameters)
            let token = self.native.string(forKey: "Token")!
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/order/update_cart/"
            
            Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if dict1["status"] as! String == "failure"
                        {
                            debugPrint("xdfghhhghb", dict1["message"])
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.view.makeToast(dict1["message"] as! String, duration: 2.0, position: .bottom)
                            self.AddtoCartTable.reloadData()
                            
                        }
                        else  if let invalidToken = dict1["status"]{
                            if invalidToken as! String  == "success"
                            {
                                //                        value1.blurView.alpha = 0
                                //                        value1.activity.stopAnimating()
                                self.parseData()
                            }
                            else
                            {
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()                    }
                        }
                    }
                }
                
            }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.view.makeToast("Number can't be zero", duration: 2.0, position: .bottom)
            self.AddtoCartTable.reloadData()
            
        }
    }
    
    
    @objc func updateBulkQuantity(productVariantId: Int)
    {
        debugPrint("update bulk price....")
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert = SCLAlertView(appearance: appearance)
        let txt = alert.addTextField("Enter Quantity")
        alert.addButton("Save", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            //            self.inventryBlur.alpha = 0
            debugPrint("Text value: \(txt.text!)")
            if (txt.text!) != ""
            {
                if (txt.text!).count <= 4
                {
                    if self.checknumber(number: txt.text!) == true &&  Int(txt.text!)! > 0
                    {
                        
                        let shopid = String(self.shopid)
                        self.update_product(productVariantID: "\(productVariantId)", quantity: txt.text!, shopID: shopid)
                        self.AddtoCartTable.beginUpdates()
                        self.AddtoCartTable.endUpdates()
                        self.AddtoCartTable.reloadData()
                    }
                    else
                    {
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        alert.dismiss(animated: true, completion: nil)
                        self.view.makeToast("Number can't be zero", duration: 2.0, position: .bottom)
                    }
                }
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    alert.dismiss(animated: true, completion: nil)
                    self.view.makeToast("Unable to add to cart for given quantity", duration: 2.0, position: .bottom)
                    
                }
            }
            else{
                self.AddtoCartTable.reloadData()
            }
        }
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        alert.addButton("Cancel", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            self.AddtoCartTable.reloadData()
            //            self.inventryBlur.alpha = 0
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit("Update Quantity", subTitle: "Please enter valid Quantity value", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
    }
    
    
    
    
    @objc func sumbitOrContinue(sender: UIButton!) {
        let tag = sender.tag
        let alertController = UIAlertController(title: "", message: "Are you sure that you want to Remove this product?", preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: "Cancel" , style: .default) { (_ action) in
            //code here…
            
        }
        let ok = UIAlertAction(title: "Remove" , style: .default) { (_ action) in
            //code here…
            self.delete_product(productid: sender.tag)
        }
        ok.setValue(UIColor.black, forKey: "titleTextColor")
        cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
        alertController.addAction(cancel)
        alertController.addAction(ok)
        alertController.view.tintColor = .yellow
        self.present(alertController, animated: true, completion: nil)
        
        AddtoCartTable.reloadData()
    }
    
    
    func delete_product(productid: Int)
    {
        CustomLoader.instance.showLoaderView()
        let parameters: [String:Any] = ["product_variantid":productid, "order_typeid": self.order_typeid]
        debugPrint("rrrrr",parameters)
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        debugPrint("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/remove_from_cart/"
        
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            let result = response.result
            debugPrint("remove from cart", response)
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if let invalidToken = dict1["status"]{
                        if invalidToken as! String  == "success"
                        {
                            
                            self.parseData()
                            if BBProductDetails.senddataAddtocart == true
                            {
                                BBProductDetails.getCartData()
                            }
                        }
                        else
                        {
                            let alert = UIAlertController(title: "", message: (dict1["response"] as? String)!, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                
            }
            
        }
    }
    
    
    
    let native = UserDefaults.standard
    
    @IBOutlet var AddtoCartTable: UITableView!
    @IBOutlet var Paynow: UIButton!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        //        self.tabBarController?.tabBar.isHidden = true
        UIApplication.shared.statusBarStyle = .default //Set Style
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        //        if #available(iOS 13.0, *) {
        //            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        //             statusBar.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
        //             UIApplication.shared.keyWindow?.addSubview(statusBar)
        //        } else {
        //             UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
        //        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default //Set Style
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        //        if #available(iOS 13.0, *) {
        //            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        //             statusBar.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
        //             UIApplication.shared.keyWindow?.addSubview(statusBar)
        //        } else {
        //             UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
        //        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if native.string(forKey: "Token")! != ""
        {
            PaymentDetails = self
            //done
            Paynow.layer.cornerRadius = Paynow.layer.frame.size.height/2
            Paynow.layer.masksToBounds = true
            Paynow.isEnabled = false
            PaynowContainer.isHidden = true
            shopContainer.isHidden = true
            //        Paynow.applyendGradient(colors: [loginstartColor, loginendColor])
            AddtoCartTable.tableFooterView = UIView()
            AddtoCartTable.isHidden = true
        }
        else
        {
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if native.string(forKey: "Token")! != ""
        {
            AddtoCartTable.isHidden = true
            //        self.currentIndex = 0
            self.data.removeAll()
            parseData()
        }
        else
        {
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    @IBAction func Paynow(_ sender: Any) {
        //        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "myCart"))! as UIViewController
        //
        //        self.present(editPage, animated: false, completion: nil)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "myCart") as! UIViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
    }
    
    
    func parseData()
    {
        self.data.removeAll()
        self.productData.removeAll()
        self.paymentMethod.removeAll()
        self.Quantity.removeAll()
        CustomLoader.instance.showLoaderView()
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != ""
        {
            self.Amount = 0
            let b2burl = native.string(forKey: "b2burl")!
            if native.object(forKey: "user_purchase_modeid") != nil {
                user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
            }
            var a = URLRequest(url: NSURL(string: "\(b2burl)/order/get_cart/?purchase_modeid=\(user_purchase_modeid)") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            debugPrint("a-a-a-a-a", a)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("responses payement: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if dict1["status"] as! String == "failure"
                        {
                            debugPrint("xdfghhhghb", dict1["message"])
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.view.makeToast(dict1["message"] as! String, duration: 2.0, position: .bottom)
                            self.AddtoCartTable.reloadData()
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if let data = dict1["data"] as? [AnyObject]
                            {
                                
                                if data.count > 0
                                {
                                    if let payment_mode_data = dict1["payment_mode_data"] as? [AnyObject]
                                    {
                                        self.paymentMethod = payment_mode_data
                                    }
                                    self.productData = data
                                    for i in 0..<data.count{
                                        if data[i]["min_order_qty"] is NSNull
                                        {
                                            
                                            
                                        }
                                        else
                                        {
                                            self.Quantity.append(data[i]["min_order_qty"] as! Int)
                                        }
                                        if data[i]["is_cod_applicable"] is NSNull
                                        {}
                                        else
                                        {
                                            var is_cod_applicable = data[i]["is_cod_applicable"] as! NSNumber
                                            self.native.set(is_cod_applicable, forKey: "is_cod_applicable")
                                            self.native.synchronize()
                                        }
                                        if data[i]["order_typeid"] is NSNull
                                        {}
                                        else
                                        {
                                            self.order_typeid = Int(data[i]["order_typeid"] as! NSNumber)
                                        }
                                        if let productname = data[i]["title"] as? String
                                        {
                                            self.productTitle.append(productname)
                                        }
                                        else
                                        {
                                            self.productTitle.append("")
                                        }
                                        
                                    }
                                    //
                                    if data[0]["cod_charge"] is NSNull
                                    {}
                                    else
                                    {
                                        self.cod_charge = Double(data[0]["cod_charge"] as! NSNumber)
                                    }
                                    
                                    if data[0]["bb_coin"] is NSNull
                                    {}
                                    else
                                    {
                                        self.bb_coin = Double(data[0]["bb_coin"] as! NSNumber)
                                    }
                                    if data[0]["wallet_value"] is NSNull
                                    {}
                                    else
                                    {
                                        self.wallet_value = (data[0]["wallet_value"] as! NSString).doubleValue
                                    }
                                    
                                    if data[0]["ship_insurance"] is NSNull
                                    {}
                                    else
                                    {
                                        self.ship_insurance = Double(data[0]["ship_insurance"] as! NSNumber)
                                    }
                                    //                                if data[0]["shipping_charge"] is NSNull
                                    //                                {}
                                    //                                else
                                    //                                {
                                    //                                    self.shipping_charge = Double(data[0]["shipping_charge"] as! NSNumber)
                                    //                                }
                                    
                                    if let shopname = data[0]["shop_name"] as? String
                                    {
                                        self.shopName.text = shopname
                                    }
                                    
                                    if data[0]["mcv"] is NSNull
                                    {}
                                    else
                                    {
                                        if let mcv = data[0]["mcv"] as? Double
                                        {
                                            self.MCV = mcv
                                        }
                                    }
                                    if let address = data[0]["shop_location"] as? String
                                    {
                                        self.shopAddress.text = address
                                    }
                                    self.shopid = (data[0]["shopid"] as? Int)!
                                    if let shopimage = data[0]["shop_logo"] as? String
                                    {
                                        
                                        let url = NSURL(string:shopimage)
                                        DispatchQueue.main.async {
                                            self.shopIcon.sd_setImage(with: URL(string: shopimage), placeholderImage: UIImage(named: "thumbnail"))
                                        }
                                    }
                                    for i in 0..<data.count
                                    {
                                        if let data = data[i]["variant_data"] as? [AnyObject]
                                        {
                                            self.data.append(data)
                                            for each in data {
                                                
                                                
                                                let temp = each as! NSDictionary
                                                
                                                if temp["selling_price"] is NSNull
                                                {}
                                                else
                                                {
                                                    self.Amount = self.Amount + (Double((temp["selling_price"] as! NSString).floatValue) * Double((temp["quantity"] as! NSNumber).floatValue) )
                                                    debugPrint("amount---")
                                                }
                                                if temp["product_tax_value"] is NSNull
                                                {}
                                                else
                                                {
                                                    self.product_tax_value = self.product_tax_value + Double(temp["product_tax_value"] as! NSNumber)
                                                }
                                                if temp["currency_symbol"] is NSNull
                                                {}
                                                else
                                                {
                                                    if let currency_symbol = temp["currency_symbol"] as? String
                                                    {
                                                        self.currency =  currency_symbol
                                                        debugPrint("amount---", self.Amount)
                                                    }
                                                }  } }}
                                    
                                } }
                        }
                    }
                    
                    
                    self.AddtoCartTable.isHidden = false
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents();
                    self.AddtoCartTable.reloadData()
                    self.productMOQ = true
                    
                    
                    if self.data.count > 0
                    {
                        var totalMoq = 0
                        for j in 0..<self.data.count
                        {
                            var moq = 0
                            let PaymentData = self.data[j] as! [AnyObject]
                            
                            for i in 0..<PaymentData.count
                                
                            {
                                let data1 = PaymentData[i] as! NSDictionary
                                moq = moq + (data1["quantity"] as? Int)!
                                debugPrint("Max quantity is :::::", moq)
                            }
                            if self.Quantity[j] <= moq
                            {
                                self.min_order_qty = true
                            }
                            else
                            {
                                self.min_order_qty = false
                                break
                            }
                            totalMoq = totalMoq + moq
                        }
                        //                    debugPrint(self.Amount)
                        self.TotalQuantity.text = "Qty: \(totalMoq)"
                        self.totalPrice.text = "Total: \(self.currency) \(String(format:"%.2f", self.Amount))"
                        var data = self.Amount
                        
                        debugPrint("self.user_purchase_modeid", self.user_purchase_modeid, self.MCV, data, self.min_order_qty)
                        
                        if self.user_purchase_modeid == "1"
                        {
                            if self.min_order_qty == true
                            {
                                self.mcvAlert.isHidden = true
                                self.Paynow.isEnabled = true
                                self.PaynowContainer.isHidden = false
                                self.shopContainer.isHidden = false
                            }
                            else
                            {
                                
                                self.mcvAlert.isHidden = true
                                self.Paynow.isEnabled = false
                                self.PaynowContainer.isHidden = false
                                self.shopContainer.isHidden = false
                                
                            }
                        }
                        else
                        {
                            if self.MCV < data && self.min_order_qty == true
                            {
                                self.mcvAlert.isHidden = true
                                self.Paynow.isEnabled = true
                                self.PaynowContainer.isHidden = false
                                self.shopContainer.isHidden = false
                            }
                            else
                            {
                                debugPrint("mcv data", self.MCV, data)
                                data = self.MCV - data
                                debugPrint("mcv data", self.MCV, data)
                                self.PaynowContainer.isHidden = false
                                self.shopContainer.isHidden = false
                                self.Paynow.isEnabled = false
                                self.mcvAlert.isHidden = false
                                self.mcvAlert.text = "Add Items worth \(self.currency) \(String(format:"%.2f", data)) more to place the order"
                            }
                            if self.min_order_qty == false
                            {
                                self.mcvAlert.isHidden = true
                                self.Paynow.isEnabled = false
                                self.PaynowContainer.isHidden = false
                                self.shopContainer.isHidden = false
                            }
                            if self.MCV > data
                            {
                                self.mcvAlert.isHidden = false
                                self.Paynow.isEnabled = false
                                self.PaynowContainer.isHidden = false
                                self.shopContainer.isHidden = false
                            }
                            
                            
                            
                        }
                        
                        
                        
                    }
                    else
                    {
                        self.shopName.text = ""
                        self.shopIcon.image = nil
                        self.shopAddress.text = ""
                        self.totalPrice.text = ""
                        self.PaynowContainer.isHidden = true
                        self.shopContainer.isHidden = true
                        self.Paynow.isEnabled = false
                        self.mcvAlert.isHidden = true
                    } }  }}
        else
        {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
    }
    
    
    
    
    func displayErrorAndLogOut() {
        let alert = SCLAlertView()
        let errorColor : UInt = 0xf8ac59
        if Util.shared.checkNetworkTapped() {
            alert.showError("Uh Oh!", subTitle: "Your session has expired! Please log in again.", closeButtonTitle: "Okay", colorStyle: errorColor)
        } else {
            alert.showError("Uh Oh!", subTitle: "You have lost connectivity to the internet. Please check your connection and log in again.", closeButtonTitle: "Okay", colorStyle:errorColor)//, colorTextButton: errorColor)
        }
        
        
        
    }
    
    
    
    func checknumber(number: String) -> Bool
    {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: number)) else {
            return false
        }
        return true
    }
    
}


