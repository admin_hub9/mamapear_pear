//
//  ShopAboutTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/27/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShopAboutTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pageindex: UIPageControl!
    @IBOutlet weak var shopBannerCollection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
