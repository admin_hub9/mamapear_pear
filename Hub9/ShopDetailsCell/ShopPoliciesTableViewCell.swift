//
//  ShopPoliciesTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/27/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShopPoliciesTableViewCell: UITableViewCell {

    @IBOutlet weak var ShopPolicies: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
