//
//  SelectBrandViewController.swift
//  Hub9
//
//  Created by Deepak on 7/31/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class SelectBrandViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    var data = ["Add New Brand"]
    var brandID = [-1]
    var native = UserDefaults.standard
    var filteredData: [String]!
    var indexNumber:NSInteger = -1
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
   
    
    func valueCheck(_ d: String) -> String {
        var result = ""
        do {
            let regex = try NSRegularExpression(pattern: "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$", options: [])
            let results = regex.matches(in: String(d), options: [], range: NSMakeRange(0, String(d).characters.count))
            if results.count > 0 {result = d}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
        }
        print("check regex",result)
        return result
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        valueCheck("12.67684.78738")
        SelectBrand()
        tableView.isHidden = true
        filteredData = data
        
    }
    override func viewDidAppear(_ animated: Bool) {
//        let brand = native.string(forKey: "brand")
//        if(brand != "")
//        {
////            searchBar.text = brand
//
//            if data.contains(brand!)
//            {
//                for var i in 0..<data.count
//                {
//                    if brand == data[i]
//                    {
//                        indexNumber = i
//                        i = data.count
//                    }
//                }
//            }
//            else
//            {
//            data.append(brand!)
//            filteredData = data
//                for var i in 0..<data.count
//                {
//                    if brand == data[i]
//                    {
//                        indexNumber = i
//                        i = data.count
//                    }
//                }
//            }
//            tableView.reloadData()
//            native.set("", forKey: "brand")
//            native.synchronize()
//        }
    }


    func SelectBrand()
    {
        
        let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            var a = URLRequest(url: NSURL(string: "\(b2burl)/product/get_listing_brand/")! as URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response shop: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    let innerdict = dict1["data"] as! [[String: Any]]
                    
                    for var i in 0..<innerdict.count
                    {
                        print("brand",innerdict[i]["brandid"]!, innerdict[i]["brand_name"]!)
                        self.data.append(innerdict[i]["brand_name"]! as! String)
                        self.brandID.append(innerdict[i]["brandid"]! as! Int)
                    }
                    
                }
                print("data", self.data)
                self.filteredData = self.data
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }
        }
    }
    
//
    @IBAction func doneButton(_ sender: Any) {
        if indexNumber != -1
        {
            
        native.set(filteredData[indexNumber], forKey: "brand")
        native.synchronize()
        }
        navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        indexNumber = indexPath.row
            print(indexPath.row)
        if indexPath.row == 0
        {
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "addBrand"))! as UIViewController
            
            self.present(editPage, animated: true, completion: nil)
        }
        else{
            print(brandID.count - 1, indexPath.row)
            if (brandID.count - 1) < indexPath.row
            {
                native.set(filteredData[indexPath.row], forKey: "brand")
                native.set("0", forKey: "brandID")
                native.synchronize()
            }
            else{
                native.set(filteredData[indexPath.row], forKey: "brand")
                native.set(brandID[indexPath.row], forKey: "brandID")
                print(brandID[indexPath.row])
                native.synchronize()
            }
        
        
            tableView.reloadData()
            navigationController?.popViewController(animated: true)
        }
        
    }
    
     func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == tableView
        {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "brand", for: indexPath) as! BrandTableViewCell
        cell.brandName?.text = filteredData[indexPath.row]
        if indexNumber == indexPath.row{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filteredData.count
    
        
    }

    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredData = searchText.isEmpty ? data : data.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })
        
        
        if searchBar == self.searchBar {
            print("first")
            tableView.reloadData()
        } 
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        if searchBar == self.searchBar {
            print("first")
            self.searchBar.showsCancelButton = true
        } 
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
}


