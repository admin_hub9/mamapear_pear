//
//  OrdersDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 9/24/18.
//  Copyright © 20018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import UIEmptyState


class OrdersDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIViewControllerTransitioningDelegate , UIScrollViewDelegate, UICollectionViewDelegateFlowLayout, UIEmptyStateDataSource, UIEmptyStateDelegate{
    
    var native = UserDefaults.standard
    private let refreshControl = UIRefreshControl()
    var user_purchase_modeid = "1"
    
    var unpaidcount = 0
    var activeStatus = 0
    var Pendingcount = 0
    var packedcount = 0
    var shippedcount = 0
    var returncount = 0
    var cancelcount = 0
    var isDataLoading:Bool=false
    var pendingpageNo:Int=0
    var packedpageNo:Int=0
    var shippedpageNo:Int=0
    var returnpageNo:Int=0
    var cancelpageNo:Int=0
    
    
    var unpaidLimit = 10
    var unpaidOffset = 0
    var pendingLimit = 10
    var pendingOffset = 0
    var packedLimit = 10
    var packedOffset = 0
    var shippedLimit = 10
    var shippedOffset = 0
    var returnLimit = 10
    var returnOffset = 0
    var cancelLimit = 10
    var cancelOffset = 0
    var deliverLimit = 10
    var deliverOffset = 0
    
    
    
    //unpaid
    var unpaidName = [String]()
    var unpaidItemid = [String]()
    var unpaidUnit = [String]()
    var unpaidPrice = [String]()
    var unpaidImage = [String]()
    var unpaidDate = [String]()
    var unpaidItem = [String]()
    
    
    //deliver
    var deliverName = [String]()
    var deliverItemId = [String]()
    var deliverUnit = [String]()
    var deliverPrice = [String]()
    var deliverImage = [String]()
    var deliverDate = [String]()
    var deliverItem = [String]()
    
    // pending
    var pendingName = [String]()
    var pendingItemid = [String]()
    var pendingUnit = [String]()
    var pendingPrice = [String]()
    var pendingImage = [String]()
    var PendingDate = [String]()
    var PendingItem = [String]()
    ///
    var packedName = [String]()
    var packedItemid = [String]()
    var packedUnit = [String]()
    var packedImage = [String]()
    var packedDate = [String]()
    var PackedItem = [String]()
    var packedPrice = [String]()
    ///
    var shippedName = [String]()
    var shippedItemid = [String]()
    var shippedUnit = [String]()
    var shippedImage = [String]()
    var shippedDate = [String]()
    var shippedItem = [String]()
    var shippedPrice = [String]()
    ///
    var returnName = [String]()
    var returnItemid = [String]()
    var returnUnit = [String]()
    var returnImage = [String]()
    var returnDate = [String]()
    var returnItem = [String]()
    var returnPrice = [String]()
    ///
    var cancelName = [String]()
    var cancelItemId = [String]()
    var cancelUnit = [String]()
    var cancelImage = [String]()
    var cancelDate = [String]()
    var cancelItem = [String]()
    var cancelPrice = [String]()
    
    ///color
    var SelectedColor = UIColor(red:247/255, green:82/255, blue:85/255, alpha: 1)
    var UnSelectedColor = UIColor(red:85/255, green:85/255, blue:85/255, alpha: 1)
    
    
    @IBOutlet var statusCollection: UICollectionView!
    @IBOutlet var statusTable: UITableView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        activeStatus = settingPage.selectedOrderStatus
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusTable.isHidden = true
        CustomLoader.instance.showLoaderView()
        // Optionally remove seperator lines from empty cells
        self.statusTable.tableFooterView = UIView(frame: CGRect.zero)
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        // Do any additional setup after loading the view.
        // refresh Table
        // Add Refresh Control to Table View
        statusTable.tableFooterView = UIView()
        if #available(iOS 10.0, *) {
            statusTable.refreshControl = refreshControl
        } else {
            statusTable.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        emptyTable()
        parseUnpaidData(limit: self.unpaidLimit, offset: self.unpaidOffset)
        parsePendingData(limit: self.pendingLimit, offset: self.pendingOffset)
        parsePackedData(limit: self.packedLimit, offset: packedOffset)
        parseShippedData(limit: self.shippedLimit, offset: self.shippedOffset)
        parseDeliveredData(limit: deliverLimit, offset: deliverOffset)
        parseReturnData(limit: self.returnLimit, offset: self.returnOffset)
        parseCancelData(limit: self.cancelLimit, offset: self.cancelOffset)
        
    }
    
    
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Api Data
        fetchApiData()
    }
    
    
    func emptyTable()
    {
        
        unpaidName.removeAll()
        unpaidItemid.removeAll()
        unpaidUnit.removeAll()
        unpaidPrice.removeAll()
        unpaidImage.removeAll()
        unpaidDate.removeAll()
        unpaidItem.removeAll()
        unpaidLimit = 10
        unpaidOffset = 0
        ///
        pendingName.removeAll()
        pendingItemid.removeAll()
        pendingUnit.removeAll()
        pendingPrice.removeAll()
        pendingImage.removeAll()
        PendingDate.removeAll()
        PendingItem.removeAll()
        pendingLimit = 10
        pendingOffset = 0
        /////
        packedName.removeAll()
        packedItemid.removeAll()
        packedUnit.removeAll()
        packedImage.removeAll()
        packedDate.removeAll()
        PackedItem.removeAll()
        packedPrice.removeAll()
        packedLimit = 10
        packedOffset = 0
        ///
        shippedName.removeAll()
        shippedItemid.removeAll()
        shippedUnit.removeAll()
        shippedImage.removeAll()
        shippedDate.removeAll()
        shippedItem.removeAll()
        shippedPrice.removeAll()
        shippedLimit = 10
        shippedOffset = 0
        ///
        returnName.removeAll()
        returnItemid.removeAll()
        returnUnit.removeAll()
        returnImage.removeAll()
        returnDate.removeAll()
        returnItem.removeAll()
        returnPrice.removeAll()
        returnLimit = 10
        returnOffset = 0
        ///
        cancelName.removeAll()
        cancelItemId.removeAll()
        cancelUnit.removeAll()
        cancelImage.removeAll()
        cancelDate.removeAll()
        cancelItem.removeAll()
        cancelPrice.removeAll()
        cancelLimit = 10
        cancelOffset = 0
        ///
        deliverName.removeAll()
        deliverItemId.removeAll()
        deliverUnit.removeAll()
        deliverImage.removeAll()
        deliverDate.removeAll()
        deliverItem.removeAll()
        deliverPrice.removeAll()
        deliverLimit = 10
        deliverOffset = 0
        
    }
    
    private func fetchApiData() {
        
        if activeStatus == 0
        {
            unpaidName.removeAll()
            unpaidItemid.removeAll()
            unpaidUnit.removeAll()
            unpaidPrice.removeAll()
            unpaidImage.removeAll()
            unpaidDate.removeAll()
            unpaidItem.removeAll()
            unpaidLimit = 10
            unpaidOffset = 0
            parseUnpaidData(limit: self.unpaidLimit, offset: self.unpaidOffset)
        }
        else if activeStatus == 1
        {
            pendingName.removeAll()
            pendingItemid.removeAll()
            pendingUnit.removeAll()
            pendingPrice.removeAll()
            pendingImage.removeAll()
            PendingDate.removeAll()
            PendingItem.removeAll()
            pendingLimit = 10
            pendingOffset = 0
            parsePendingData(limit: self.pendingLimit, offset: self.pendingOffset)
        }
            ///
        else if activeStatus == 2
        {
            packedName.removeAll()
            packedItemid.removeAll()
            packedUnit.removeAll()
            packedImage.removeAll()
            packedDate.removeAll()
            PackedItem.removeAll()
            packedPrice.removeAll()
            packedLimit = 10
            packedOffset = 0
            parsePackedData(limit: self.packedLimit, offset: self.packedOffset)
        }
            ///
        else if activeStatus == 3
        {
            shippedName.removeAll()
            shippedItemid.removeAll()
            shippedUnit.removeAll()
            shippedImage.removeAll()
            shippedDate.removeAll()
            shippedItem.removeAll()
            shippedPrice.removeAll()
            shippedLimit = 10
            shippedOffset = 0
            parseShippedData(limit: self.shippedLimit, offset: self.shippedOffset)
        }
            ///
        else if activeStatus == 4
        {
            deliverName.removeAll()
            deliverItemId.removeAll()
            deliverUnit.removeAll()
            deliverImage.removeAll()
            deliverDate.removeAll()
            deliverItem.removeAll()
            deliverPrice.removeAll()
            deliverLimit = 10
            deliverOffset = 0
            parseDeliveredData(limit: deliverLimit, offset: deliverOffset)
        }
            ///
        else if activeStatus == 5
        {
            returnName.removeAll()
            returnItemid.removeAll()
            returnUnit.removeAll()
            returnImage.removeAll()
            returnDate.removeAll()
            returnItem.removeAll()
            returnPrice.removeAll()
            returnLimit = 10
            returnOffset = 0
            parseReturnData(limit: self.returnLimit, offset: self.returnOffset)
        }
            ///
        else if activeStatus == 6
        {
            cancelName.removeAll()
            cancelItemId.removeAll()
            cancelUnit.removeAll()
            cancelImage.removeAll()
            cancelDate.removeAll()
            cancelItem.removeAll()
            cancelPrice.removeAll()
            cancelLimit = 10
            cancelOffset = 0
            parseCancelData(limit: cancelLimit, offset: cancelOffset)
        }
        
        self.updateView()
        
        
    }
    
    func updateView()
    {
        refreshControl.tintColor = UIColor(red:247/255, green:82/255, blue:85/255, alpha:1.0)
        self.statusTable.tableFooterView?.isHidden = true
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching  Data ...")
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        debugPrint("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        debugPrint("scrollViewDidEndDecelerating")
    }
    
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        debugPrint("scrollViewDidEndDragging")
        if ((statusTable.contentOffset.y + statusTable.frame.size.height) >= statusTable.contentSize.height)
        {
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: statusTable.bounds.width, height: CGFloat(44))
            
            
            if !isDataLoading{
                isDataLoading = true
                if activeStatus == 0
                {
                    debugPrint(self.unpaidItemid.count, self.unpaidOffset)
                    if self.unpaidItemid.count >= self.unpaidOffset
                    {
                        self.unpaidOffset=self.unpaidOffset+10
                        
                        
                        //                        parseUnpaidData(limit: self.unpaidLimit, offset: self.unpaidOffset)
                        self.statusTable.tableFooterView = spinner
                        self.statusTable.tableFooterView?.isHidden = false
                        //                loadCallLogData(offset: self.offset, limit: self.limit)
                    }
                }
                else if activeStatus == 1
                {
                    debugPrint(self.pendingItemid.count, self.pendingOffset)
                    if self.pendingItemid.count >= self.pendingOffset
                    {
                        self.pendingOffset=self.pendingOffset+10
                        
                        
                        parsePendingData(limit: self.pendingLimit, offset: self.pendingOffset)
                        self.statusTable.tableFooterView = spinner
                        self.statusTable.tableFooterView?.isHidden = false
                        //                loadCallLogData(offset: self.offset, limit: self.limit)
                    }
                }
                else if activeStatus == 2
                {
                    if self.packedItemid.count >= self.packedOffset
                    {
                        self.packedOffset=self.packedOffset + 10
                        
                        
                        parsePackedData(limit: self.packedLimit, offset: self.packedOffset)
                        self.statusTable.tableFooterView = spinner
                        self.statusTable.tableFooterView?.isHidden = false
                    }
                }
                else if activeStatus == 3
                {
                    if self.shippedItemid.count >= self.shippedOffset
                    {
                        self.shippedOffset=self.shippedOffset + 10
                        
                        parseShippedData(limit: self.shippedLimit, offset: self.shippedOffset)
                        self.statusTable.tableFooterView = spinner
                        self.statusTable.tableFooterView?.isHidden = false
                    }
                }
                else if activeStatus == 4
                {
                    if self.deliverItemId.count >= self.deliverOffset
                    {
                        self.deliverOffset=self.deliverOffset + 10
                        parseDeliveredData(limit: deliverLimit, offset: deliverOffset)
                        self.statusTable.tableFooterView = spinner
                        self.statusTable.tableFooterView?.isHidden = false
                    }
                }
                else if activeStatus == 5
                {
                    if self.returnItemid.count >= self.returnOffset
                    {
                        self.returnOffset=self.returnOffset + 10
                        parseReturnData(limit: self.returnLimit, offset: self.returnOffset)
                        self.statusTable.tableFooterView = spinner
                        self.statusTable.tableFooterView?.isHidden = false
                    }
                }
                else if activeStatus == 6
                {
                    if self.cancelItemId.count >= self.cancelOffset
                    {
                        self.cancelOffset=self.cancelOffset + 10
                        parseCancelData(limit: self.cancelLimit, offset: self.cancelOffset)
                        self.statusTable.tableFooterView = spinner
                        self.statusTable.tableFooterView?.isHidden = false
                    }
                }
                
                
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// UI table status
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //        debugPrint(activeStatus, self.pendingItemid.count)
        if activeStatus == 0
        {
            if unpaidItemid.count > 0
            {
                statusTable.showEmptyListMessage("")
            }
            else
            {
                statusTable.showEmptyListMessage("No Item in Unpaid")
                
            }
            return unpaidItemid.count
        }
        else if activeStatus == 1
        {
            if pendingItemid.count > 0
            {
                statusTable.showEmptyListMessage("")
            }
            else
            {
                statusTable.showEmptyListMessage("No Item in Pending")
                
            }
            return pendingItemid.count
        }
        else if activeStatus == 2
        {
            if packedItemid.count > 0
            {
                statusTable.showEmptyListMessage("")
            }
            else
            {
                statusTable.showEmptyListMessage("No Item in Packed")
                
            }
            return packedItemid.count
        }
        else if activeStatus == 3
        {
            if shippedItemid.count > 0
            {
                statusTable.showEmptyListMessage("")
            }
            else
            {
                statusTable.showEmptyListMessage("No Item in Shipping")
            }
            return shippedItemid.count
        }
        else if activeStatus == 4
        {
            if deliverItemId.count > 0
            {
                statusTable.showEmptyListMessage("")
            }
            else
            {
                statusTable.showEmptyListMessage("No Item in Delivered")
            }
            return deliverItemId.count
        }
        else if activeStatus == 5
        {
            if returnItemid.count > 0
            {
                statusTable.showEmptyListMessage("")
            }
            else
            {
                statusTable.showEmptyListMessage("No Item in Return")
            }
            return returnItemid.count
        }
        else if activeStatus == 6
        {
            if cancelItemId.count > 0
            {
                statusTable.showEmptyListMessage("")
            }
            else
            {
                statusTable.showEmptyListMessage("No Item in Cancel")
            }
            return cancelItemId.count
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if activeStatus == 0
        {
            let orderid = unpaidItemid[indexPath.row]
            native.set(orderid, forKey: "orderid")
            native.set("Unpaid", forKey: "status")
            native.synchronize()
        }
        else if activeStatus == 1
        {
            let orderid = pendingItemid[indexPath.row]
            native.set(orderid, forKey: "orderid")
            native.set("Pending", forKey: "status")
            native.synchronize()
        }
        else if activeStatus == 2
        {
            let orderid = packedItemid[indexPath.row]
            native.set(orderid, forKey: "orderid")
            native.set("Packed", forKey: "status")
            native.synchronize()
        }
        else if activeStatus == 3
        {
            let orderid = shippedItemid[indexPath.row]
            native.set(orderid, forKey: "orderid")
            native.set("Shipped", forKey: "status")
            native.synchronize()
        }
        else if activeStatus == 4
        {
            let orderid = deliverItemId[indexPath.row]
            native.set(orderid, forKey: "orderid")
            native.set("Delivered", forKey: "status")
            native.synchronize()
        }
        else if activeStatus == 5
        {
            let orderid = returnItemid[indexPath.row]
            native.set(orderid, forKey: "orderid")
            native.set("Return", forKey: "status")
            native.synchronize()
        }
        else if activeStatus == 6
        {
            let orderid = cancelItemId[indexPath.row]
            native.set(orderid, forKey: "orderid")
            native.set("Cancelled", forKey: "status")
            native.synchronize()
        }
        settingPage.selectedOrderStatus = activeStatus
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "orderdetails"))! as UIViewController
        self.navigationController?.pushViewController(rootPage, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReuseCell = UITableViewCell()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myordercell2") as! MyOrderTableViewCell2
        
        
        if activeStatus == 0 && self.unpaidItemid.count > 0
        {
            
            
            //            debugPrint("unpaidDate[indexPath.row]", indexPath.row)
            cell.date.text = unpaidDate[indexPath.row]
            cell.productname.text = unpaidName[indexPath.row]
            cell.totalunits.text = unpaidUnit[indexPath.row]
            cell.totalItem.text = unpaidPrice[indexPath.row]
            if self.unpaidImage.count > indexPath.row
            {
                var imageUrl = unpaidImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.icon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
        }
        else if activeStatus == 1 && self.pendingItemid.count > 0
        {
            
            
            //            debugPrint("PendingDate[indexPath.row]", indexPath.row)
            cell.date.text = PendingDate[indexPath.row]
            cell.productname.text = pendingName[indexPath.row]
            cell.totalunits.text = pendingUnit[indexPath.row]
            cell.totalItem.text = pendingPrice[indexPath.row]
            if self.pendingImage.count > indexPath.row
            {
                var imageUrl = pendingImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.icon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
        }
        else if activeStatus == 2 && self.packedItemid.count > 0
        {
            
            cell.date.text = packedDate[indexPath.row]
            cell.productname.text = packedName[indexPath.row]
            cell.totalunits.text = packedUnit[indexPath.row]
            cell.totalItem.text = packedPrice[indexPath.row]
            if self.packedImage.count > indexPath.row
            {
                var imageUrl = packedImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.icon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            
        }
        else if activeStatus == 3 && self.shippedItemid.count > 0
        {
            
            cell.date.text = shippedDate[indexPath.row]
            cell.productname.text = shippedName[indexPath.row]
            cell.totalunits.text = shippedUnit[indexPath.row]
            cell.totalItem.text = shippedPrice[indexPath.row]
            if self.shippedImage.count > indexPath.row
            {
                var imageUrl = shippedImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.icon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            
        }
        else if activeStatus == 4 && self.deliverItemId.count > 0
        {
            
            cell.date.text = deliverDate[indexPath.row]
            cell.productname.text = deliverName[indexPath.row]
            cell.totalunits.text = deliverUnit[indexPath.row]
            cell.totalItem.text = deliverPrice[indexPath.row]
            if self.deliverImage.count > indexPath.row
            {
                var imageUrl = deliverImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.icon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            
        }
        else if activeStatus == 5 && self.returnItemid.count > 0
        {
            
            cell.date.text = returnDate[indexPath.row]
            cell.productname.text = returnName[indexPath.row]
            cell.totalunits.text = returnUnit[indexPath.row]
            cell.totalItem.text = returnPrice[indexPath.row]
            if self.returnImage.count > indexPath.row
            {
                var imageUrl = returnImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.icon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            
        }
        else if activeStatus == 6 && self.cancelItemId.count > 0
        {
            
            cell.date.text = cancelDate[indexPath.row]
            cell.productname.text = cancelName[indexPath.row]
            cell.totalunits.text = cancelUnit[indexPath.row]
            cell.totalItem.text = cancelPrice[indexPath.row]
            if self.cancelImage.count > indexPath.row
            {
                var imageUrl = cancelImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.icon.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            
        }
        
        ReuseCell = cell
        
        
        return ReuseCell
    }
    
    
    ///status collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        activeStatus = indexPath.row
        self.statusCollection.scrollToItem(at: IndexPath(row: indexPath.row, section: 0), at: .centeredHorizontally, animated: true)
        self.statusCollection.reloadData()
        self.statusTable.tableFooterView?.isHidden = true
        self.statusTable.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize
        
        let width = (collectionView.frame.width-30)/4
        
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var ReuseCell = UICollectionViewCell()
        if collectionView == statusCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ordercell", for: indexPath) as! OrdersandQoutationCollectionViewCell
            
            if indexPath.row == 0
            {
                cell.title.text = "Unpaid"
                cell.count.text = String(self.unpaidcount)
            }
            else if indexPath.row == 1
            {
                cell.title.text = "Pending"
                cell.count.text = String(self.Pendingcount)
            }
            else if indexPath.row == 2
            {
                
                cell.title.text = "Packed"
                cell.count.text = String(self.packedcount)
            }
            else if indexPath.row == 3
            {
                
                cell.title.text = "Shipped"
                cell.count.text = String(self.shippedcount)
            }
            else if indexPath.row == 4
            {
                
                cell.title.text = "Delivered"
                cell.count.text = String(self.deliverItemId.count)
            }
            else if indexPath.row == 5
            {
                
                cell.title.text = "Return"
                cell.count.text = String(self.returncount)
            }
            else if indexPath.row == 6
            {
                
                cell.title.text = "Cancelled"
                cell.count.text = String(self.cancelcount)
            }
            if activeStatus != indexPath.row
            {
                cell.title?.font = UIFont.systemFont(ofSize: 12.0, weight: .regular)
                cell.count?.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
                cell.title.textColor = UnSelectedColor
                cell.count.textColor = UnSelectedColor
            }
            else
            {
                cell.title?.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
                cell.count?.font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
                cell.title.textColor = SelectedColor
                cell.count.textColor = SelectedColor
            }
            
            ReuseCell = cell
            
        }
        return ReuseCell
    }
    
    // parse Unpaid data
    func parseUnpaidData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Unpaid&purchase_modeid=\(user_purchase_modeid)&order_typeid=1"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Unpaid&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Pending", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response unPaid: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = (innerdict[i]["unpaid"] as? Int)!
                                    
                                    self.Pendingcount = (innerdict[i]["pending"] as? Int)!
                                    
                                    self.packedcount = innerdict[i]["packed"] as! Int
                                    
                                    self.shippedcount = innerdict[i]["shipped"] as! Int
                                    
                                    self.returncount = innerdict[i]["return"] as! Int
                                    
                                    self.cancelcount = innerdict[i]["cancelled"] as! Int
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}
                                    else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.unpaidItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["order_amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["order_amount"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["order_cod_charge"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["order_ship_insurance"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["order_shipping_charge"] as! NSString).doubleValue
                                        }
                                        self.unpaidPrice.append("\(currency)\(String(format: "%.2f",itemPrice+shippingInsurance+shipping+cod))")
                                        self.unpaidName.append(order_data[0]["product_name"] as! String)
                                        
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.unpaidImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.unpaidImage.append(image)
                                                }
                                            }
                                        }
                                        if innerdict1[i]["inserted_date"] is NSNull
                                        {}
                                        else
                                        {
                                            self.unpaidDate.append(innerdict1[i]["inserted_date"] as! String)
                                        }
                                        if order_data.count > 1
                                        {
                                            self.unpaidUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.unpaidUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                            
                        }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                debugPrint("=--=-=-=-=-=-=", self.unpaidName)
                //                        DispatchQueue.main.async {
                self.statusTable.isHidden = false
                self.statusCollection.reloadData()
                self.statusTable.reloadData()
                self.statusTable.tableFooterView?.isHidden = true
                self.refreshControl.endRefreshing()
                
                //                        }
            }
        }
        
    }
    
    // parse Delivered data
    func parseDeliveredData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Delivered&purchase_modeid=\(user_purchase_modeid)&order_typeid=1"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Delivered&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Delivered", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response unPaid: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = (innerdict[i]["unpaid"] as? Int)!
                                    
                                    self.Pendingcount = (innerdict[i]["pending"] as? Int)!
                                    self.unpaidcount = (innerdict[i]["delivered"] as? Int)!
                                    self.packedcount = innerdict[i]["packed"] as! Int
                                    
                                    self.shippedcount = innerdict[i]["shipped"] as! Int
                                    
                                    self.returncount = innerdict[i]["return"] as! Int
                                    
                                    self.cancelcount = innerdict[i]["cancelled"] as! Int
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}
                                    else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.deliverItemId.append(String(innerdict1[i]["orderid"] as! Int))
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["order_amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["order_amount"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["order_cod_charge"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["order_ship_insurance"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["order_shipping_charge"] as! NSString).doubleValue
                                        }
                                        self.deliverPrice.append("\(currency)\(String(format: "%.2f",itemPrice+shippingInsurance+shipping+cod))")
                                        self.deliverName.append(order_data[0]["product_name"] as! String)
                                        
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.deliverImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.deliverImage.append(image)
                                                }
                                            }
                                        }
                                        if innerdict1[i]["inserted_date"] is NSNull
                                        {}
                                        else
                                        {
                                            self.deliverDate.append(innerdict1[i]["inserted_date"] as! String)
                                        }
                                        if order_data.count > 1
                                        {
                                            self.deliverUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.deliverUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                            
                        }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                debugPrint("=--=-=-=-=-=-=", self.deliverName)
                //                        DispatchQueue.main.async {
                self.statusTable.isHidden = false
                self.statusCollection.reloadData()
                self.statusTable.reloadData()
                self.statusTable.tableFooterView?.isHidden = true
                self.refreshControl.endRefreshing()
                
                //                        }
            }
        }
        
    }
    
    
    // parsePending data
    func parsePendingData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Pending&purchase_modeid=\(user_purchase_modeid)&order_typeid=1"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Pending&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Pending", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response Pending: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = (innerdict[i]["unpaid"] as? Int)!
                                    
                                    self.Pendingcount = (innerdict[i]["pending"] as? Int)!
                                    
                                    self.packedcount = innerdict[i]["packed"] as! Int
                                    
                                    self.shippedcount = innerdict[i]["shipped"] as! Int
                                    
                                    self.returncount = innerdict[i]["return"] as! Int
                                    
                                    self.cancelcount = innerdict[i]["cancelled"] as! Int
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}
                                    else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.pendingItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["order_amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["order_amount"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["order_cod_charge"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["order_ship_insurance"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["order_shipping_charge"] as! NSString).doubleValue
                                        }
                                        self.pendingPrice.append("\(currency)\(String(format:"%.2f", itemPrice+shippingInsurance+shipping+cod))")
                                        self.pendingName.append(order_data[0]["product_name"] as! String)
                                        
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.pendingImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.pendingImage.append(image)
                                                }
                                            }
                                        }
                                        if innerdict1[i]["inserted_date"] is NSNull
                                        {}
                                        else
                                        {
                                            self.PendingDate.append(innerdict1[i]["inserted_date"] as! String)
                                        }
                                        if order_data.count > 1
                                        {
                                            self.pendingUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.pendingUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                            
                        }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                debugPrint("=--=-=-=-=-=-=", self.pendingName)
                //                        DispatchQueue.main.async {
                self.statusTable.isHidden = false
                self.statusCollection.reloadData()
                self.statusTable.reloadData()
                self.statusTable.tableFooterView?.isHidden = true
                self.refreshControl.endRefreshing()
                
                //                        }
            }
        }
        
    }
    
    // parse packed data
    func parsePackedData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "changetype")!
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            
            //            if usertype == "20"
            //            {
            //                url = "\(b20burl)/order/seller_orders/?limit=40&offset=0&status_ui=Packed"
            //            }
            //            else if usertype == "1"
            //            {
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Packed&purchase_modeid=\(user_purchase_modeid)&order_typeid=1"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Packed&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Packed", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response packed: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = (innerdict[i]["unpaid"] as? Int)!
                                    
                                    self.Pendingcount = (innerdict[i]["pending"] as? Int)!
                                    
                                    self.packedcount = innerdict[i]["packed"] as! Int
                                    
                                    self.shippedcount = innerdict[i]["shipped"] as! Int
                                    
                                    self.returncount = innerdict[i]["return"] as! Int
                                    
                                    self.cancelcount = innerdict[i]["cancelled"] as! Int
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.packedItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["order_amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["order_amount"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["order_cod_charge"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["order_ship_insurance"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["order_shipping_charge"] as! NSString).doubleValue
                                        }
                                        self.packedPrice.append("\(currency)\(String(format: "%.2f",itemPrice+shippingInsurance+shipping+cod))")
                                        self.packedName.append(order_data[0]["product_name"] as! String)
                                        
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.packedImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.packedImage.append(image)
                                                }
                                            }
                                        }
                                        self.packedDate.append(innerdict1[i]["inserted_date"] as! String)
                                        if order_data.count > 1
                                        {
                                            self.packedUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.packedUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                DispatchQueue.main.async {
                    
                    self.statusTable.isHidden = false
                    self.statusCollection.reloadData()
                    self.statusTable.reloadData()
                    self.statusTable.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    
                }
            }
        }
    }
    
    // parse Shipped data
    func parseShippedData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "changetype")!
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            
            //            if usertype == "20"
            //            {
            //                url = "\(b20burl)/order/seller_orders/?limit=40&offset=0&status_ui=Shipped"
            //            }
            //            else if usertype == "1"
            //            {
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Shipped&purchase_modeid=\(user_purchase_modeid)&order_typeid=1"
            }
            else{
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Shipped&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Shipped", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response shipped: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = (innerdict[i]["unpaid"] as? Int)!
                                    
                                    self.Pendingcount = (innerdict[i]["pending"] as? Int)!
                                    
                                    self.packedcount = innerdict[i]["packed"] as! Int
                                    
                                    self.shippedcount = innerdict[i]["shipped"] as! Int
                                    
                                    self.returncount = innerdict[i]["return"] as! Int
                                    
                                    self.cancelcount = innerdict[i]["cancelled"] as! Int
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.shippedItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["order_amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["order_amount"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["order_cod_charge"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["order_ship_insurance"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["order_shipping_charge"] as! NSString).doubleValue
                                        }
                                        self.shippedPrice.append("\(currency)\(String(format: "%.2f",itemPrice+shippingInsurance+shipping+cod))")
                                        self.shippedName.append(order_data[0]["product_name"] as! String)
                                        
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.shippedImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.shippedImage.append(image)
                                                }
                                            }
                                        }
                                        self.shippedDate.append(innerdict1[i]["inserted_date"] as! String)
                                        if order_data.count > 1
                                        {
                                            self.shippedUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.shippedUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                        
                    }}
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                DispatchQueue.main.async {
                    self.statusCollection.reloadData()
                    self.statusTable.reloadData()
                    self.statusTable.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    
                }
            }
        }
        
    }
    // parse Return data
    func parseReturnData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "changetype")!
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            
            //            if usertype == "20"
            //            {
            //                url = "\(b20burl)/order/seller_orders/?limit=40&offset=0&status_ui=Return"
            //            }
            //            else if usertype == "1"
            //            {
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Return&purchase_modeid=\(user_purchase_modeid)&order_typeid=1"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Return&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("return ", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response return: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = (innerdict[i]["unpaid"] as? Int)!
                                    
                                    self.Pendingcount = (innerdict[i]["pending"] as? Int)!
                                    
                                    self.packedcount = innerdict[i]["packed"] as! Int
                                    
                                    self.shippedcount = innerdict[i]["shipped"] as! Int
                                    
                                    self.returncount = innerdict[i]["return"] as! Int
                                    
                                    self.cancelcount = innerdict[i]["cancelled"] as! Int
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}
                                    else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.returnItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["order_amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["order_amount"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["order_cod_charge"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["order_ship_insurance"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["order_shipping_charge"] as! NSString).doubleValue
                                        }
                                        self.returnPrice.append("\(currency)\(String(format: "%.2f",itemPrice+shippingInsurance+shipping+cod))")
                                        self.returnName.append(order_data[0]["product_name"] as! String)
                                        
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.returnImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.returnImage.append(image)
                                                }
                                            }
                                        }
                                        self.returnDate.append(innerdict1[i]["inserted_date"] as! String)
                                        if order_data.count > 1
                                        {
                                            self.returnUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.returnUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                DispatchQueue.main.async {
                    self.statusCollection.reloadData()
                    self.statusTable.reloadData()
                    self.statusTable.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    
                }
            }
        }
    }
    // Parse Cancel Data
    func parseCancelData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        //        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "changetype")!
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            //
            //            if usertype == "20"
            //            {
            //                url = "\(b20burl)/order/seller_orders/?limit=40&offset=0&status_ui=Cancelled"
            //            }
            //            else if usertype == "1"
            //            {
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Cancelled&purchase_modeid=\(user_purchase_modeid)&order_typeid=1"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Cancelled&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("cancelled", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response cancelled: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = (innerdict[i]["unpaid"] as? Int)!
                                    
                                    self.Pendingcount = (innerdict[i]["pending"] as? Int)!
                                    
                                    self.packedcount = innerdict[i]["packed"] as! Int
                                    
                                    self.shippedcount = innerdict[i]["shipped"] as! Int
                                    
                                    self.returncount = innerdict[i]["return"] as! Int
                                    
                                    self.cancelcount = innerdict[i]["cancelled"] as! Int
                                }
                                
                                
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}
                                    else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.cancelItemId.append(String(innerdict1[i]["orderid"] as! Int))
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["order_amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["order_amount"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["order_cod_charge"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["order_ship_insurance"] as! NSString).doubleValue
                                        }
                                        if innerdict1[i]["order_shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["order_shipping_charge"] as! NSString).doubleValue
                                        }
                                        self.cancelPrice.append("\(currency)\(String(format: "%.2f",itemPrice+shippingInsurance+shipping+cod))")
                                        self.cancelName.append(order_data[0]["product_name"] as! String)
                                        
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.cancelImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.cancelImage.append(image)
                                                }
                                            }
                                        }
                                        self.cancelDate.append(innerdict1[i]["inserted_date"] as! String)
                                        if order_data.count > 1
                                        {
                                            self.cancelUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.cancelUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                        }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                DispatchQueue.main.async {
                    self.statusCollection.reloadData()
                    self.statusTable.reloadData()
                    self.statusTable.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    
                }
            }
        }
        
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    
    
    
}

