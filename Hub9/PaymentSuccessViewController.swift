//
//  PaymentSuccessViewController.swift
//  Hub9
//
//  Created by Deepak on 6/1/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView


var paymentSuccess = PaymentSuccessViewController()

class PaymentSuccessViewController: UIViewController {
    
    
    var window: UIWindow?
    var native = UserDefaults.standard
    
    

    
    @IBOutlet weak var Congo: UILabel!
    @IBOutlet weak var congomsg2: UILabel!
    @IBOutlet var muOrder: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentSuccess = self
        muOrder.layer.cornerRadius = 3
        muOrder.layer.borderWidth = 1
        let textcolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
        muOrder.layer.borderColor = textcolor.cgColor
        self.tabBarController?.delegate = self as! UITabBarControllerDelegate
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose ofaany resources that can be recreated.
    }
    
    @IBAction func myOrderButton(_ sender: Any) {
        print("hello")
        
        rootViewController()

    }
    
    func rootViewController()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let connectPage : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "rootController") as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = connectPage
        self.window?.makeKeyAndVisible()

    }
    
}
