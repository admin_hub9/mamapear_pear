//
//  CheckChatRoomViewController.swift
//  Hub9
//
//  Created by Deepak on 11/30/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Firebase
import SCLAlertView

class CheckChatRoomViewController: UIViewController, UIViewControllerTransitioningDelegate {

    var selectedUser = User?.self
    var native = UserDefaults.standard
    var isotherUserIdExist =  false
    var chatroomid = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func chekChatRoom(shopid: String, shopname: String, shopProfilePic: String)
    {
        if self.native.object(forKey: "firebaseid") != nil
        {
        let db = Firestore.firestore()
        print("user info.....", shopid, shopProfilePic)
        var currentUserId = self.native.string(forKey: "firebaseid")!
//        var currentuserfirstname = self.native.string(forKey: "firstname")!
//        var currentuserlastname = self.native.string(forKey: "lastname")!
//        var currentuserprofilepic = self.native.string(forKey: "profilepic")
        db.collection("chatRooms").whereField("otherUserId", isEqualTo: currentUserId)
            .addSnapshotListener() { (querySnapshot, err) in
                guard let snapshot = querySnapshot else {
                    print("Error retreiving snapshots \(err!)")
                    return
                }
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){
                        
                        guard let receivedMessage: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        print("otherUserId", receivedMessage["userId"]!, receivedMessage["otherUserId"]!)
                        if receivedMessage["userId"]! as! String == shopid && receivedMessage["otherUserId"]! as! String == currentUserId
                        {
                            print("exist")
                            self.native.set(shopid, forKey: "currentuserid")
                            self.native.set(shopname, forKey: "otherusername")
                            self.native.set(shopProfilePic, forKey: "otheruserpic")
                            self.native.set(receivedMessage["id"]!, forKey: "chatroomid")
                            self.native.synchronize()
                            self.isotherUserIdExist = true
                        }  }
                     }
        }
        if isotherUserIdExist == false
        {
            db.collection("chatRooms").whereField("userId", isEqualTo: currentUserId)
            .addSnapshotListener() { (querySnapshot, err) in
                guard let snapshot = querySnapshot else {
                    print("Error retreiving snapshots \(err!)")
                    return
                }
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added){
                        
                        guard let receivedMessage: [String:Any] = diff.document.data()
                            else {
                                return
                        }
                        print("UserId", receivedMessage["userId"]!, receivedMessage["otherUserId"]!)
                        if receivedMessage["userId"]! as! String == currentUserId && receivedMessage["otherUserId"]! as! String == shopid
                        {
                            print("exist..")
                            self.native.set(shopid, forKey: "currentuserid")
                            self.native.set(shopname, forKey: "otherusername")
                            self.native.set(shopProfilePic, forKey: "otheruserpic")
                            self.native.set(receivedMessage["id"]!, forKey: "chatroomid")
                            self.native.synchronize()
                            self.isotherUserIdExist = true
                        }
                    } }
                
                if self.isotherUserIdExist == true
                {
                    print("ChatRoomExist....")
                }
                else
                {
                    print(self.isotherUserIdExist)
                    // Add a new document in collection "cities"
                    let db = Firestore.firestore()
                    if self.native.object(forKey: "profilepic") != nil
                    {
                    let userProfilePic = self.native.string(forKey: "profilepic")!
                    let userName = self.native.string(forKey: "firstname")!
                    let lastName = self.native.string(forKey: "lastname")!
                    let otheruserinfo: [String: Any] = ["first_name":shopname, "last_name":"", "profileImageUrl": shopProfilePic]
                    if userProfilePic == nil || userName == nil
                    {
                        let alertView = SCLAlertView()
                        alertView.showError("Uh Oh", subTitle: "Please Update Your UserInfo First!")
                    }
                    else
                    {
                    let userinfo: [String: Any] = ["first_name":userName, "last_name":lastName, "profileImageUrl": userProfilePic]
                    print(userinfo, otheruserinfo)
                    let values: [String: Any] = ["lastConversationText": "", "lastConversationTime": Date().millisecondsSince1970, "isRead": false, "otherUserId": shopid,"id":"","sendBy": currentUserId,"userId": currentUserId,"otherUserInfo": otheruserinfo, "userInfo":userinfo, "createdAt": Date().millisecondsSince1970]
                    
                    var ref: DocumentReference? = nil
                    ref = db.collection("chatRooms").addDocument(data: values) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            print("Document added with ID: \(ref!.documentID)")
                            ref!.updateData([
                                "id": ref!.documentID
                            ]){err in
                                if let err = err {
                                    print("Error adding document: \(err)")
                                } else {
                                    print("Document updated with ID: \(ref!.documentID)")
                                    self.native.set(shopid, forKey: "currentuserid")
                                    self.native.set(shopname, forKey: "otherusername")
                                    self.native.set(shopProfilePic, forKey: "otheruserpic")
                                    self.native.set(ref!.documentID, forKey: "chatroomid")
                                    self.native.synchronize()
                                    
                                }
                            }
                        }
                        }}
                    }
                }
            }
            
        }
        else
        {
            if isotherUserIdExist == true
            {
                
                
                
            }
        }
        
        
    }
        else
        {
            let alertView = SCLAlertView()
            alertView.showError("Uh Oh", subTitle: "Contact support!")
        
        }
        
    }
    
   
    
}

