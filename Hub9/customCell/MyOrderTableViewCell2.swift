//
//  MyOrderTableViewCell2.swift
//  Hub9
//
//  Created by Deepak on 5/15/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class MyOrderTableViewCell2: UITableViewCell {
    

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var totalunits: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var totalItem: UILabel!
    @IBOutlet weak var sellerApproved: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
