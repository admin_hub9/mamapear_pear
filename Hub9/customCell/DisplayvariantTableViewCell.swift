//
//  DisplayvariantTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/21/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class DisplayvariantTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
        
        
    }
    
    
    @IBOutlet var vaiantimage: UIImageView!
    
    @IBOutlet var addphots: UIButton!
    @IBOutlet var productVariantImages: UICollectionView!
    @IBOutlet var deletecell: UIButton!
    @IBOutlet var VariantTitle: UILabel!
    @IBOutlet var price: UITextField!
    @IBOutlet var moq: UITextField!
    @IBOutlet var stock: UITextField!
    @IBOutlet var sku: UITextField!
    @IBOutlet var shadwo: UIView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
}

