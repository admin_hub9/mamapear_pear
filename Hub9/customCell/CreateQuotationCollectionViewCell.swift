//
//  CreateQuotationCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 11/23/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class CreateQuotationCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var quantitysegment: UISegmentedControl!
    @IBOutlet weak var pricesegment: UISegmentedControl!
    
    
}
