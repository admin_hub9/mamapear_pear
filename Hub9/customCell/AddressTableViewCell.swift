//
//  AddressTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 6/21/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    
    
    @IBOutlet var checkBox: UIImageView!
    @IBOutlet var address: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            address.textColor = UIColor.blue
            checkBox.image = UIImage(named: "checkmark")
        }
        else{
            address.textColor = UIColor.black
            checkBox.image = UIImage(named: "Uncheckmark")
        }

        // Configure the view for the selected state
    }

}
