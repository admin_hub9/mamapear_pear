//
//  MoreTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 11/22/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class MoreTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var content: UILabel!
    
    @IBOutlet weak var nextIcon: UIImageView!
    @IBOutlet weak var notification: UISwitch!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
