//
//  ShopAddressTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/11/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class ShopAddressTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }
    
    @IBOutlet weak var shopaddress: UILabel!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
