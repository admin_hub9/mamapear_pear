//
//  AddProductCell.swift
//  Hub9
//
//  Created by Deepak on 6/2/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class AddProductCell: UICollectionViewCell {
    
    @IBOutlet var clickPhotos: UIButton!
    @IBOutlet var photos: UILabel!
    @IBOutlet var camera: UIImageView!
    @IBOutlet var cancel: UIButton!
    @IBOutlet var images: UIImageView!
    
}
