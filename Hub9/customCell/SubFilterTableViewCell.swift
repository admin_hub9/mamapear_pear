//
//  SubFilterTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 6/25/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class SubFilterTableViewCell: UITableViewCell {

    @IBOutlet var unit: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var mark: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        if selected{
//            mark.image = UIImage(named: "checkmark")
//            
//            contentView.backgroundColor = UIColor.white
//        }
//        else{
//            mark.image = UIImage(named: "Uncheckmark")
//        }
        // Configure the view for the selected state
    }

}
