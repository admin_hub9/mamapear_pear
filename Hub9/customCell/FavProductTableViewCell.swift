//
//  FavProductTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/3/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class FavProductTableViewCell: UITableViewCell {


    @IBOutlet var productImage: UIImageView!
    @IBOutlet var productname: UILabel!
    @IBOutlet var stauts: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productTag: UILabel!
    @IBOutlet weak var productMatchText: UILabel!
    @IBOutlet weak var retailText: UILabel!
    @IBOutlet weak var matchTagIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
