//
//  AddProductTableCell.swift
//  Hub9
//
//  Created by Deepak on 6/2/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class AddProductTableCell: UITableViewCell {

    @IBOutlet var cellView: UIView!
    @IBOutlet var delete: UIButton!
    @IBOutlet var cost: UILabel!
    @IBOutlet var units: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
