//
//  FavShopTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/3/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class FavShopTableViewCell: UITableViewCell {

    @IBOutlet var shopimage: UIImageView!
    @IBOutlet var shopname: UILabel!
    @IBOutlet var shopsku: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var status: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
