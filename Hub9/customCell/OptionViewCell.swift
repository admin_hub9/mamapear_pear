//
//  OptionViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class OptionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var image: UIImageView!
}
