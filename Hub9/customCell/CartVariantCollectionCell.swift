//
//  CartVariantCollectionCell.swift
//  Hub9
//
//  Created by Deepak on 6/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class CartVariantCollectionCell: UICollectionViewCell {
    
    @IBOutlet var variantName: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var unit: UITextField!
    @IBOutlet var cancel: UIButton!

    @IBOutlet var outofstock: UILabel!
    @IBOutlet var unitlabel: UILabel!
    @IBOutlet var unitView: UIView!
    
}
