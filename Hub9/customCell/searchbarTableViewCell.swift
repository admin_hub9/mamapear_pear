//
//  searchbarTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 9/27/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class searchbarTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }
    @IBOutlet var productimage: UIImageView!
    
    @IBOutlet var name: UILabel!
    
    @IBOutlet var address: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
