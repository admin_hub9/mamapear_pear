//
//  ShopCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/9/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class ShopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var shadwo: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var about: UILabel!
}
