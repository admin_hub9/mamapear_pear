//
//  SelectCategoryTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 12/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class SelectCategoryTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var subCategoryName: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
