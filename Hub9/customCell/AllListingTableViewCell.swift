//
//  AllListingTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 6/25/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class AllListingTableViewCell: UITableViewCell {
    
    
    
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var sku: UILabel!
    @IBOutlet var moq: UILabel!
    @IBOutlet var stauts: UILabel!
    @IBOutlet var editButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        let borderColor = UIColor(red: 0.0/255, green: 200.0/255, blue: 177.0/255, alpha: 1.0)
        editButton.layer.borderWidth = 1
        editButton.layer.borderColor = borderColor.cgColor
        editButton.layer.cornerRadius = editButton.layer.frame.size.height/2
        editButton.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
         super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
