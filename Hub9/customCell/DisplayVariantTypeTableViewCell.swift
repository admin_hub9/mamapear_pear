//
//  DisplayVariantTypeTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/21/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class DisplayVariantTypeTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }
    
    @IBOutlet var name: UILabel!
    
    @IBOutlet var count: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
