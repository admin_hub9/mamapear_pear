//
//  AddVariantTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/19/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class AddVariantTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }
    @IBOutlet var type: UILabel!
    @IBOutlet var mark: UIImageView!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            mark.image = UIImage(named: "checkmark")
        }
        else{
            mark.image = UIImage(named: "Uncheckmark")
        }

        // Configure the view for the selected state
    }

}
