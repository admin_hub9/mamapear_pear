//
//  ShopTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/9/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class ShopTableViewCell: UITableViewCell {

    @IBOutlet weak var shopCollection: UICollectionView!
    
    @IBOutlet var followButton: UIButton!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var shopimage: UIImageView!
    @IBOutlet weak var enterShop: UIButton!
    @IBOutlet var shopcell: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
