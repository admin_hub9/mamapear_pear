//
//  ProductCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {
    
  
    @IBOutlet var shadwo: UIView!
    @IBOutlet weak var bookmark: UIButton!
    
    
    ///
    @IBOutlet weak var productImage: SDAnimatedImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productTag: UILabel!
    @IBOutlet weak var productSaveTag: UILabel!
    
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var marketPlaceName: UILabel!
    /// for video in dashboard
    
    @IBOutlet weak var videoContainer: UIView!
    
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var likeProduct: UIButton!
    
    @IBOutlet weak var userDetail: UIButton!
    @IBOutlet weak var featuredName: UILabel!
    @IBOutlet weak var uploadTime: UILabel!
    
    
    
    //closet view
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var priceTag: UILabel!
    
    
}
