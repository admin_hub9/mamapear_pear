//
//  SelectInterestCollectionCell.swift
//  Hub9
//
//  Created by Deepak on 6/15/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class SelectInterestCollectionCell: UICollectionViewCell {
    
    @IBOutlet var selectedInterest: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var images: UIImageView!
}
