//
//  BrandTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/31/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class BrandTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    @IBOutlet var brandName: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        if selected
//        {
//            
//        }
        // Configure the view for the selected state
    }

}
