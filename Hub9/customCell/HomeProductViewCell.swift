//
//  HomeProductViewCell.swift
//  Hub9
//
//  Created by Deepak on 5/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class HomeProductViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productcard3: UIImageView!
    @IBOutlet weak var productcard2: UIImageView!
    @IBOutlet weak var productcard1: UIImageView!
}
