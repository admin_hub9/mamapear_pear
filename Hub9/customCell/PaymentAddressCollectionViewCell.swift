//
//  PaymentAddressCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 10/18/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class PaymentAddressCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var mark: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var citystatepincode: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var edit: UIButton!
    @IBOutlet weak var addressTag: UILabel!
    
}
