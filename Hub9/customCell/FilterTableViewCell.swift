//
//  FilterTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 6/25/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    
    var SelectedColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 0.2)
    
    @IBOutlet var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            contentView.backgroundColor = SelectedColor
        }
        else
        {
            contentView.backgroundColor = UIColor.clear
        }

        // Configure the view for the selected state
    }

}
