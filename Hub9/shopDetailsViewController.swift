//
//  shopDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 5/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AttributedTextView




class shopDetailsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    var native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    
    
    var policy = ""
    var banner = [String]()
    var feature_name = [String]()
    var productImage = [AnyObject]()
    var ProductName = [String]()
    var ProductMOQ = [String]()
    var Loaction = [String]()
    var ProductPrice = [String]()
    var retailPrice = [NSNumber]()
    var shop_Product = [AnyObject]()
    var variantID = [String]()
    var brand = ""
    var Currency = ""
    var fav = 0
    var bannerIndex = 0
    var limit = 20
    var offset = 0
    
    

    @IBOutlet var productCollection: UICollectionView!
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(productImage.count)
        

        return productImage.count

    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleCells: [IndexPath] = productCollection.indexPathsForVisibleItems
        let lastIndexPath = IndexPath(item: (productImage.count - 1), section: 0)
        if visibleCells.contains(lastIndexPath) {
            //This means you reached at last of your datasource. and here you can do load more process from server
            print("last index ", lastIndexPath)
            offset = offset + 20
            parseData(limit: limit, offset: offset)
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
        cell.clipsToBounds = true
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        
        cell.contentView.layer.cornerRadius = 5
        cell.contentView.layer.borderWidth = 1.0
        
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        if let image1 = self.productImage[indexPath.row] as? [AnyObject]
        {
            if image1.count > 0
            {
                var imageUrl = image1[0] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: imageUrl)
                if url != nil{
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
        }
        cell.productName.text = (self.ProductName[indexPath.row] as? String)?.capitalized
        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
        let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        if self.shop_Product.count > indexPath.row
        {
            
            let actualprice = self.ProductPrice[indexPath.row ]
            let retailsPrice = self.retailPrice[indexPath.row ]
            var product_price_tag = ""
            if shop_Product[indexPath.row]["product_price_tag"] is NSNull
            {}
            else
            {
                product_price_tag = shop_Product[indexPath.row]["product_price_tag"] as! String
            }
            if native.string(forKey: "Token")! == ""
            {let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
            }
            else
            {
                cell.price.attributedText = ("\(self.Currency)\(String(format:"%.2f", Double(Float(actualprice)!)))".color(textcolor).size(11) + "\(product_price_tag) ".color(textcolor).size(9) + "\(self.Currency)\(String(format:"%.2f",Double(Float(retailsPrice))))".color(graycolor).strikethrough(1).size(9)).attributedText
                
            }
        }
        print("feature_name[indexPath.row]", feature_name[indexPath.row])
        if feature_name[indexPath.row] == ""
        {
            cell.productTag.text = ""
        }
        else
        {
            if feature_name[indexPath.row] as! String != "Default"
            {
                cell.productTag.text = " \(feature_name[indexPath.row] as! String) "
                cell.productTag.isHidden = false
            }
            else
            {
                cell.productTag.isHidden = true
            }
        }
        
        


        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
            var width = 0.0
            var height = 0.0
            width = Double(CGFloat(screenWidth / 2 - 15))
            height = Double(CGFloat(Double(screenHeight) / 3 + 20))
        return CGSize(width: width, height: height )
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        native.set(variantID[indexPath.row], forKey: "FavProID")
        native.set("product", forKey: "comefrom")
        native.synchronize()
        //        currntProID = self.ProductID[indexPath.row] as! Int
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomLoader.instance.showLoaderView()
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        parseData(limit: self.limit, offset: self.offset)

        // Do any additional setup after loading the view.
    }
    func parseData(limit: Int, offset: Int)
    {
        
            let shopid = native.string(forKey: "entershopid")!
            let b2burl = native.string(forKey: "b2burl")!
            let token = self.native.string(forKey: "Token")!
                
                
            var a = URLRequest(url: NSURL(string: "\(b2burl)/search/shop_products/?shopid=\(shopid)&limit=\(limit)&offset=\(offset)")! as URL)
            if token != ""
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            }
            else{
                
                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
                print("url", a)
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                                debugPrint(response)
                                                                print("response: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                    
                    
                    else if dict1["status"] as! String == "success"
                    {
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                     if let innerdict = dict1["data"] as? [AnyObject]{
                        print("inner dict shop", innerdict)
                        if innerdict.count > 0
                        {
                            self.shop_Product = innerdict
                            print((innerdict[0]["brand_name"] as? String)!)
                            let bannerimg = innerdict[0]["banner_url"] as! AnyObject
                            
                            var imageurl = bannerimg["image_url"] as? [AnyObject]
                            for i in 0..<imageurl!.count
                            {
                                self.banner.append(imageurl![i] as! String)
                            }
                            self.brand = innerdict[0]["brand_name"] as! String
                            
                            
                            for i in 0..<innerdict.count {
                                //                        print(innerdict[i]["name_json"] as! NSDictionary, "name")
//                                let data = innerdict[i]["name_json"] as! AnyObject
                                let name = innerdict[i]["title"]! as! String
                                if innerdict[i]["feature_name"] is NSNull
                                {
                                    self.feature_name.append("")
                                }
                                else
                                {
                                self.feature_name.append(innerdict[i]["feature_name"] as! String)
                                }
                                //                        print(name)
                                
                                
                                let shopname = innerdict[i]["shop_name"] as! String
                                let statename = innerdict[i]["state_name"] as? String
                                
                                if let product_variant_data = innerdict[i]["product_variant_data"] as? [AnyObject]
                                {
                                    //                                for i in 0..<product_variant_data.count
                                    //                                {
                                    if let product_variant_size_data = product_variant_data[0]["product_variant_size_data"] as? [AnyObject]
                                    {
                                        if product_variant_size_data.count > 0
                                        {
                                            self.ProductName.append(name as String)
                                            let imagedir = (product_variant_size_data[0]["variant_image_url"] as? AnyObject)!
                                            self.productImage.append((imagedir["200"] as? AnyObject)! )
                                        if product_variant_size_data[0]["currency_symbol"] is NSNull
                                            {}
                                            else
                                            {
                                                let myInteger = product_variant_size_data[0]["currency_symbol"] as! NSString
                                                self.Currency = "\(myInteger)"
                                                //                                    }
                                                
                                            }
//                                            self.ProductMOQ.append(String((product_variant_size_data[0]["min_order_qty"] as? Int)!))
                                            self.ProductPrice.append("\((product_variant_size_data[0]["selling_price"] as! NSNumber).floatValue)")
                                           self.retailPrice.append(product_variant_size_data[0]["retail_price"] as! NSNumber)
                                            self.variantID.append(String((product_variant_size_data[0]["product_variantid"] as? Int)!))
                                            self.Loaction.append((statename as? String)!)
                                           
                                        }}
                                    } }}
                        }
                    }
                }
                
                self.productCollection.reloadData()
                }}
//        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }




}
