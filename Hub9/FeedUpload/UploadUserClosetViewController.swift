//
//  UploadUserClosetViewController.swift
//  Hub9
//
//  Created by Deepak on 18/06/20.
//  Copyright © 2020 Deepak. All rights reserved.
//


   import UIKit
    import Gallery
    import Lightbox
    import AWSCore
    import AWSS3
    import Alamofire
    import Photos
    import SVProgressHUD
    import AlignedCollectionViewFlowLayout
    import Material
    import MapKit
    import GoogleMaps
    import GooglePlaces


    var UserCloset = UploadUserClosetViewController()

class UploadUserClosetViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LightboxControllerDismissalDelegate, GalleryControllerDelegate, UITextViewDelegate {

        
        var native = UserDefaults.standard
        
        ////for images and videos
        var gallery: GalleryController!
        let editor: VideoEditing = VideoEditor()
        ///fetch dynamic price disscount
        var imagePicker: UIImagePickerController!
        var selectedImage = [UIImage]()
        var videourl = [URL]()
        var is_edit = false
        var s3count = 0
        var url = ""
        var timestamp = ""
        static var categoryid = 0
        static var is_Editing = false
            
        static var conditionID = 0
        static var categoryText = "Category*"
        var addressText = "Location*"
        var conditionText = "Condition*"
        var isClosetCategoryActive = false
    
        var GoogleMapsAPIServerKey = ""
        var cityName1 = ""
        var stateName1 = ""
        var region = ""
        var is_validated = false
        var countryid = 0
        
        var lightGray = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1)
        var textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        
        
    // tag collection variable
        var Product = [AnyObject]()
        var Data = [AnyObject]()
        var icon_image_url = [String]()
        var ParentProduct = [String]()
        var ChildProduct = [[AnyObject]]()
        var selectedCell = [Int]()
        var currentParentIndexIs = 0
        var totalcount = 0
        var selectcount = 0
        var window: UIWindow?
        
        var latitude = 0.0000000000
        var longitude = 0.000000000
        var cityState = ""

        
        //condition view
        @IBOutlet weak var conditionPlaceholder: UILabel!
        @IBOutlet weak var conditionView: UIView!
        @IBOutlet weak var conditionLabel: UILabel!
        @IBOutlet weak var conditionButton: UIButton!
    
        
    
        //category view
        @IBOutlet weak var categoryPlaceholder: UILabel!
        @IBOutlet weak var categoryView: UIView!
        @IBOutlet weak var categoryLabel: UILabel!
        @IBOutlet weak var categoryButton: UIButton!
        
        
        @IBOutlet weak var feedImage: UIImageView!
        @IBOutlet weak var delete: UIButton!
        @IBOutlet weak var addImage: UIButton!
        @IBOutlet weak var closetTitle: UITextField!
    
        @IBOutlet weak var ListingPrice: UITextField!
        @IBOutlet weak var productDescription: UITextView!
        @IBOutlet weak var descriptionPlaceholder: UILabel!
        @IBOutlet weak var descriptionContainer: UIView!
        
        //address view
        @IBOutlet weak var addressPlaceholder: UILabel!
        @IBOutlet weak var addressLabel: UILabel!
        @IBOutlet weak var addressView: UIView!
        @IBOutlet weak var addressButton: UIButton!
        
        
        
        override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.tabBarController?.tabBar.isHidden = true
            if UploadUserClosetViewController.is_Editing == true
            {
                self.latitude = ClosetDetails.latitude
                self.longitude = ClosetDetails.longitude
                self.addressText = ClosetDetails.location_name
                
            self.ListingPrice.text = "\(ClosetDetails.listing_price)"
                self.closetTitle.text = "\(ClosetDetails.feed_text)"
            self.productDescription.text = "\(ClosetDetails.QuestionText)"
                UploadUserClosetViewController.categoryid = ClosetDetails.categoryid
            print("ClosetDetails.categoryText", ClosetDetails.categoryText)
                UploadUserClosetViewController.categoryText = "\(ClosetDetails.categoryText)"
            self.conditionText = "\(ClosetDetails.closet_condition)"
                UploadUserClosetViewController.conditionID = ClosetDetails.closet_conditionid
            if ClosetDetails.feedImage.count>0
            {
            var imageUrl = ClosetDetails.feedImage as! String
            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

            if let url = NSURL(string: imageUrl )
            {
                if url != nil
                {

                    DispatchQueue.main.async {
                        self.feedImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                    delete.isHidden=false
                    addImage.isHidden=true
                }
                else
                {
                    delete.isHidden=true
                    addImage.isHidden=false
                }
            }}
                else
            {
                delete.isHidden=true
                addImage.isHidden=false
                }
            }
            categoryLabel.text = UploadUserClosetViewController.categoryText
            if UploadUserClosetViewController.categoryText == "Select Category*" || UploadUserClosetViewController.categoryText == ""
            {
            self.categoryPlaceholder.isHidden = true
            self.categoryLabel.isHidden = false
            categoryLabel.textColor = lightGray
                categoryLabel.text = "Select Category*"
                categoryLabel.attributedText = ( "\(UploadUserClosetViewController.categoryText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
            }
            else
            {
            self.categoryPlaceholder.isHidden = false
            self.categoryLabel.isHidden = false
            categoryLabel.textColor = textColor
                categoryLabel.attributedText = ( "\(UploadUserClosetViewController.categoryText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
            }
            //address
            self.addressText = ClosetDetails.locationName
            if addressText == "Location*"
            {
            self.addressPlaceholder.isHidden = true
            addressLabel.textColor = lightGray
            addressLabel.attributedText = ( "\(addressText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
            }
            else
            {
            self.addressPlaceholder.isHidden = false
            addressLabel.textColor = textColor
            addressLabel.attributedText = ( "\(addressText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
            }
            
//            Condition
            conditionLabel.text = conditionText
            if conditionText == "Select Condition*"
            {
            self.conditionPlaceholder.isHidden = true
            conditionLabel.textColor = lightGray
            conditionLabel.attributedText = ( "\(conditionText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
            }
            else
            {
            self.conditionPlaceholder.isHidden = false
            conditionLabel.textColor = textColor
            conditionLabel.attributedText = ( "\(conditionText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
            }
            productDescription.text = "Please add feed text here"
           
        }
        func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.textColor == UIColor.lightGray {
                textView.text = nil
                textView.textColor = textColor
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                textView.text = "Please add feed text here"
                textView.textColor = UIColor.lightGray
            }
        }

        
        
        func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
            self.descriptionPlaceholder.isHidden = false
            return true
        }
        func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
            if self.productDescription.text.count > 0
            {
                self.descriptionPlaceholder.isHidden = false
            }
            else
            {
                self.descriptionPlaceholder.isHidden = true
            }
            self.view.endEditing(true)
            return true
        }
        
        override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        }
        
        
        // end TextView Responder
        @objc func tapDone(sender: Any) {
            self.view.endEditing(true)
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
        self.productDescription.addDoneButton(title: "Done", target: self, selector: #selector(tapDone(sender:)))
            self.descriptionPlaceholder.isHidden=true
            UserCloset = self
            
            self.parseData()
            ListingPrice.delegate = self
            
            delete.layer.cornerRadius = delete.frame.height/2
            delete.clipsToBounds=true
            let rightButtonItem = UIBarButtonItem.init(
                  title: "Post",
                  style: .done,
                  target: self,
                  action: #selector(rightButtonAction(sender:))
            )
            let leftButtonItem = UIBarButtonItem.init(
                  title: "Cancel",
                  style: .plain,
                  target: self,
                  action: #selector(leftButtonAction(sender:))
                
            )
            self.navigationItem.rightBarButtonItem = rightButtonItem
            self.navigationItem.leftBarButtonItem = leftButtonItem
            productDescription.text = "Please add feed text here"
           
            self.isLocationDisplay()
        }
        
    
    func isLocationDisplay()
    {
        
        if self.native.object(forKey: "latitute") != nil
        {
            if self.native.string(forKey: "latitute") != ""
            {
                self.latitude = Double(self.native.string(forKey: "latitute")!)!
            }
        }
        if self.native.object(forKey: "longitute") != nil
        {
            if self.native.string(forKey: "longitute") != ""
            {
                self.longitude = Double(self.native.string(forKey: "longitute")!)!
            }
        }
        if self.native.object(forKey: "location_name") != nil
        {
            if self.native.string(forKey: "location_name") != ""
            {
                self.addressText = self.native.string(forKey: "location_name")!
                self.addressLabel.text = self.addressText
                self.addressPlaceholder.isHidden = false
                addressLabel.textColor = textColor
                addressLabel.attributedText = ( "\(addressText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
            }
        }
    }
        
        @objc func rightButtonAction(sender: UIBarButtonItem)
        {
            self.uploadFeed()
        }
        @objc func leftButtonAction(sender: UIBarButtonItem)
        {
            let alertController = UIAlertController(
                title: "Are you sure that you want to cancel?",
                message: "",
                preferredStyle: UIAlertControllerStyle.alert
            )

            let cancelAction = UIAlertAction(
                title: "Yes",
                style: UIAlertActionStyle.destructive) { (action) in
                // ...
                self.navigationController?.popViewController(animated: true)
            }

            let confirmAction = UIAlertAction(
            title: "No", style: UIAlertActionStyle.default) { (action) in
                // ...
                
                
            }

            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)

            present(alertController, animated: true, completion: nil)
        }
        
        
         func uploadFeed() {
            
            if (self.selectedImage.count>0 || UserCloset.is_edit == true) && ListingPrice.text!.count != 0 && (closetTitle.text?.count)! != 0 && productDescription.text.count != 0
            {
            self.sendClosetData()
            }
            else
            {
            self.view.makeToast("Please add image/video and text first", duration: 2.0, position: .center)
            }
        }
        @IBAction func selectAddress(_ sender: Any) {
            self.searchAddress()
            
        }
    
    func searchAddress() {
            
            var country_code = ""
            if native.object(forKey: "country_code") != nil {
                
                country_code = native.string(forKey: "country_code")!
                
            }
            if native.object(forKey: "countryid") != nil {
                
               
                self.countryid = Int(native.string(forKey: "countryid")!)!
                
            }
            
            
            print("country_code", country_code)
            GoogleMapsAPIServerKey = "AIzaSyAOycE9VXNxb9_cvKrsKr6KT7Am9jxS2BQ"
            if GoogleMapsAPIServerKey != "" && country_code != ""
            {
                GMSServices.provideAPIKey(GoogleMapsAPIServerKey)
                GMSPlacesClient.provideAPIKey(GoogleMapsAPIServerKey)
                
                let autocompletecontroller = GMSAutocompleteViewController()
                
                autocompletecontroller.delegate = self
                
                let filter = GMSAutocompleteFilter()
    //            filter.type = .address  //suitable filter type
                filter.country = country_code  //appropriate country code
                autocompletecontroller.autocompleteFilter = filter
                
                present(autocompletecontroller, animated: true, completion: nil)
                
                // changes the color of the sugested places
                autocompletecontroller.primaryTextColor = UIColor.black
                autocompletecontroller.secondaryTextColor = UIColor.gray
                UIBarButtonItem.appearance(whenContainedInInstancesOf:[UISearchBar.self]).tintColor = UIColor.black

            }
            else
            {
               self.view.makeToast("Please try later!", duration: 2.0, position: .center)
            }
        }
        
        @IBAction func selectCategoryButton(_ sender: Any) {
            AddDealsView.isDealCategoryActive=false
            UserFeed.isReviewCategoryActive = false
            UserCloset.isClosetCategoryActive = true
            
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "DashboardCategory"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
        self.navigationController?.pushViewController(rootPage, animated: true)
        }
        
        @IBAction func selectCondition(_ sender: Any) {
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productCondition"))! as UIViewController
            self.navigationController?.present(editPage, animated: true, completion: nil)
        }
        
        
        func sendClosetData()
        {
            let token = self.native.string(forKey: "Token")!
            var fav = 0
                if token != ""
                {
                    CustomLoader.instance.showLoaderView()
                if self.native.object(forKey: "userid") != nil
                {
                    let userid = self.native.string(forKey: "userid")!
//                var imageurl = [String]()
                    var videourl1 = [String]()
               
                    if (self.videourl.count)<=0
                    {
//                        imageurl.append("https://s3.amazonaws.com/\(root.s3Bucket)/user-closet/"+userid+"/image/image\(self.timestamp)/.jpeg")
                    }
                    else
                    {
                        videourl1.append("https://mamapear.s3-us-west-2.amazonaws.com/user-closet/"+userid+"/video/video\(self.timestamp)/.mp4")
                    }
                
                    self.timestamp = "\(NSDate().timeIntervalSince1970 * 1000)"
                
                    var parameters: [String: Any]
                    var imageurl1 = ""
                    if UploadUserClosetViewController.is_Editing == true && self.selectedImage.count == 0
                    {
                        if ClosetDetails.feedImage.count>0
                        {
                        var imageUrl = ClosetDetails.feedImage as! String
                        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                            imageurl1 = imageUrl
                        }
                        
                    }
                    else
                    {
                       imageurl1 = "https://mamapear.s3-us-west-2.amazonaws.com/user-closet/\(userid)/image/image\(self.timestamp).jpeg"
                    }
                    if UploadUserClosetViewController.is_Editing == true
                    {
                        parameters = ["feed_typeid":"2", "image_url": [imageurl1], "video_url": "\(videourl1)", "subcategoryid": [UploadUserClosetViewController.categoryid],"brand": "Sterilizers", "latitude": latitude, "longitude":longitude, "closet_conditionid": UploadUserClosetViewController.conditionID,"listing_price": self.ListingPrice.text!, "title": self.closetTitle.text!,
                                  "location_name": self.addressText, "description":
                                    self.productDescription.text!, "user_closetid": ClosetDetails.user_feedid]
                    }
                        
                    else
                    {
                        
                        parameters = ["feed_typeid":"2", "image_url": [imageurl1], "video_url": "\(videourl1)", "subcategoryid": [UploadUserClosetViewController.categoryid],"brand": UploadUserClosetViewController.categoryText, "latitude": latitude, "longitude":longitude, "closet_conditionid": UploadUserClosetViewController.conditionID,"listing_price": self.ListingPrice.text!, "title": self.closetTitle.text!,
                                      "location_name": self.addressText, "description":
                            self.productDescription.text!]
                    }
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
            
                let b2burl = native.string(forKey: "b2burl")!
                var url = ""
                
                    if UploadUserClosetViewController.is_Editing == true
                {
                   url = "\(b2burl)/users/edit_closet/"
                }
                else
                {
                    url = "\(b2burl)/users/add_closet/"
                }
                print("follow product", url, parameters)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    print("upload feed", response)
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                                        print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    
                    else if dict1["status"] as! String == "success"
                    {
                        
                        
                               DispatchQueue.main.async {
                                   if (self.videourl.count)<=0
                                   {
                                       self.uploadPhotos()
                                   }
                                   else
                                   {
                                       self.uploadVideo(fileUrl: self.videourl[0])
                                   }
                               }
                                
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            // your code here
                            
                            self.ListingPrice.text = nil
                            self.selectedImage.removeAll()
                            self.feedImage.image = nil
                            self.delete.isHidden=true
                            self.addImage.isHidden=false
                            self.ListingPrice.resignFirstResponder()
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if UserCloset.is_edit == true
                            {
                                
                                MyFeedPostViewController.is_edit = false
                                self.view.makeToast("Your Closet update successfully.", duration: 2.0, position: .center)
                                self.navigationController?.popViewController(animated: true)
                            }
                            else
                            {
                            self.view.makeToast("Your Closet uploaded successfully.", duration: 2.0, position: .center)
                            self.navigationController?.popViewController(animated: true)
                            }
                            
                            
                            
                        }
                        
                        
                        }
                    else
                            {
                                self.view.makeToast("Please try later.", duration: 2.0, position: .center)
                            }
                    }}
                    }
                    }}
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
        }
        
        

        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            var touch: UITouch? = touches.first
            //location is relative to the current view
            // do something with the touched point
            if touch?.view?.tag != 20 {
                print("inside view")
    //            dismiss(animated: true, completion: nil)
            }
            else
            {
                
                self.ListingPrice.resignFirstResponder()
            }
            
            
        }
        
        
        func textFieldShouldReturn(textField: UITextField) -> Bool {
            
            self.view.endEditing(true)
            
            return true
            
        }
        
        @objc func keyboardWillShow(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height + 70
                }
            }
        }

        @objc func keyboardWillHide(notification: NSNotification) {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
        
        
        
        
        ///// delete image and video
        
        @IBAction func deleteButton(_ sender: Any) {
            
            self.selectedImage.removeAll()
            self.feedImage.image = nil
            self.delete.isHidden=true
            self.addImage.isHidden=false
        }
        
        //############################## gallery controller #########################

        @IBAction func addImageVideo(_ sender: Any) {
            
                gallery = GalleryController()
                gallery.delegate = self
                
                present(gallery, animated: true, completion: nil)
            }
            func buttonTouched() {
                gallery = GalleryController()
                gallery.delegate = self
                
                present(gallery, animated: true, completion: nil)
            }
            
            // MARK: - LightboxControllerDismissalDelegate
            
            func lightboxControllerWillDismiss(_ controller: LightboxController) {
                
            }
            
            
            
            // MARK: - GalleryControllerDelegate
            
            func galleryControllerDidCancel(_ controller: GalleryController) {
                controller.dismiss(animated: true, completion: nil)
                gallery = nil
            }
            
            func requestImage(for asset: PHAsset,
                              targetSize: CGSize,
                              contentMode: PHImageContentMode,
                              completionHandler: @escaping (UIImage?) -> ()) {
                let imageManager = PHImageManager()
                imageManager.requestImage(for: asset,
                                          targetSize: targetSize,
                                          contentMode: contentMode,
                                          options: nil) { (image, _) in
                                            completionHandler(image)
                }
            }
            func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
                
                self.dismiss(animated: true, completion: nil)
                gallery = nil
                
                
                editor.edit(video: video) { (editedVideo: Video?, tempPath: URL?) in
//                    print("video url is", video.asset.mediaSubtypes)
                    let asset = video.asset// your existing PHAsset
                    let targetSize = CGSize(width: 100, height: 100)
                    let contentModel = PHImageContentMode.aspectFit
                    self.requestImage(for: asset, targetSize: targetSize, contentMode: contentModel, completionHandler: { image in
                        // Do something with your image if it exists
        //                print("images", image)
                        if self.videourl.count < 1
                        {
                        self.videourl.append(tempPath!)
                            if self.selectedImage.count == 0
                            {
                                self.selectedImage.append(image!)
                            }
                            else
                            {
                                let firstimage = self.selectedImage[0]
                                self.selectedImage.remove(at: 0)
                                self.selectedImage.insert(image!, at: 0)
                                self.selectedImage.append(firstimage)
                                
                            }
                        
                        DispatchQueue.main.async {
                            if (self.videourl.count)<=0
                            {}
                            else
                            {
                                self.feedImage.image = self.selectedImage[0]
                                self.delete.isHidden=false
                                self.addImage.isHidden=true
                            }
                        }
                        }
                        else
                        {
                            let alert = UIAlertController(title: "Alert", message: "Can't Upload Multiple Videos", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    })
                    
        //            DispatchQueue.main.async {
        //                if let tempPath = tempPath {
        //                    let controller = AVPlayerViewController()
        //                    controller.player = AVPlayer(url: tempPath)
        //
        //                    self.present(controller, animated: true, completion: nil)
        //                }
        //            }
                }
                
            }
            
            func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
                self.dismiss(animated: true, completion: nil)
                print(images)
                
                //    let data = UIImageJPEGRepresentation(image!, 0.9)
                
                Image.resolve(images: images, completion: { [weak self] resolvedImages in
                    if self!.selectedImage.count + resolvedImages.count < 2
                    {
                    for var i in 0..<resolvedImages.count
                    {
                        let image = resolvedImages[i]
                        self!.selectedImage.append(image!)
                    }
                        if (self?.videourl.count)!>0
                        {}
                        else
                        {
                            self?.feedImage.image = self?.selectedImage[0]
                            self?.delete.isHidden=false
                            self?.addImage.isHidden=true
                        }
    //
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert", message: "Only one imgae can be add at a time", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self!.present(alert, animated: true, completion: nil)
                    }
                })
                gallery = nil
            }
            
            func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
                LightboxConfig.DeleteButton.enabled = true
    //            for var i in 0..<images.count
    //            {
    //                print(",,,,,", images[i].asset.description)
    //            }
                SVProgressHUD.show()
                Image.resolve(images: images, completion: { [weak self] resolvedImages in
//                    print("9999", resolvedImages)
//                    print("345", images[0].asset.mediaSubtypes)
                    SVProgressHUD.dismiss()
                    self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
                })
            }
            
            // MARK: - Helper
            
            func showLightbox(images: [UIImage]) {
                guard images.count > 0 else {
                    return
                }
                
                let lightboxImages = images.map({ LightboxImage(image: $0) })
                let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
                lightbox.dismissalDelegate = self
                
                gallery.present(lightbox, animated: true, completion: nil)
            }
            
            //############################ end Gallery Controller ##########################

        
        func uploadPhotos()
                
//            { for var i in 0..<selectedImage.count
            {
                let accessKey = root.s3AccessKeyId
                let secretKey = root.s3SecretAccessKey
                
                let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
                let configuration = AWSServiceConfiguration(region:AWSRegionType.USWest2, credentialsProvider:credentialsProvider)
                
                AWSServiceManager.default().defaultServiceConfiguration = configuration
                if native.object(forKey: "userid") != nil
                {
                var userid = native.string(forKey: "userid")!
                print(userid)
                
                    let S3BucketName = "\(root.s3Bucket)/user-closet/"+userid+"/image"
                    let remoteName =  String("image\(self.timestamp)") + ".jpeg"
                let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
        //                let image = UIImage(named: "car")
                
                if let data = UIImageJPEGRepresentation(selectedImage[0], 0.2) {
                    print(data.count)
                
                do {
                    print("-------", fileURL)
                    try data.write(to: fileURL)
                }
                catch {}
                }
                let uploadRequest = AWSS3TransferManagerUploadRequest()!
                uploadRequest.body = fileURL
                uploadRequest.key = remoteName
                uploadRequest.bucket = S3BucketName
                uploadRequest.contentType = "image/JPEG"
                uploadRequest.acl = .bucketOwnerRead
                
                let transferManager = AWSS3TransferManager.default()
                
                
                transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
                    DispatchQueue.main.async {
                        
                    }
                    
                    if let error = task.error {
                        print("Upload failed with error: (\(error.localizedDescription))")
                    }
                    
                    if task.result != nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                        if let absoluteString = publicURL?.absoluteString {
                            print("Uploaded to:\(absoluteString)")
                            self?.s3count = (self?.s3count)! + 1
                            if(self?.s3count == self?.selectedImage.count)
                            {
                                print("uplaoding successfully")
                                if self?.videourl.count == 0
                                {
                                    self!.native.set("", forKey: "categoryname")
                                    self!.native.set("", forKey: "brand")
                                    self!.native.synchronize()
                                    
                                    self!.dismiss(animated: true, completion: nil)
                                }
                                
                            }
                        }
                    }
                    
                    return nil
                }
                }
                
//                }
            }
        
        func uploadVideo(fileUrl : URL){
            
            let accessKey = root.s3AccessKeyId
            let secretKey = root.s3SecretAccessKey
            let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
            let configuration = AWSServiceConfiguration(region:AWSRegionType.EUWest1, credentialsProvider:credentialsProvider)
            AWSServiceManager.default().defaultServiceConfiguration = configuration
            let uploadingFileURL = fileUrl
            let uploadRequest = AWSS3TransferManagerUploadRequest()
            if native.object(forKey: "userid") != nil
            {
            var userid = native.string(forKey: "userid")!
            print(userid)
            let S3BucketName = "\(root.s3Bucket)/user-closet/"+userid+"/video"
                
                uploadRequest?.key = String("video\(self.timestamp)") + ".mp4"
            uploadRequest?.body = uploadingFileURL
            uploadRequest?.contentType = "video/mp4"
            
            let transferManager = AWSS3TransferManager.default()
            transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread())
            { (task) -> Any? in
                if task.error == nil
                {
                    print("uploaded to :")
                    self.native.set("", forKey: "categoryname")
                    self.native.set("", forKey: "brand")
                    self.native.synchronize()
                    self.dismiss(animated: true, completion: nil)
                    return nil
                }
                else
                {
                    print("Error: \(String(describing: task.error))")
                }
                
                if task.result != nil {
                    let url = AWSS3.default().configuration.endpoint.url
                    let publicURL = url?.appendingPathComponent((uploadRequest?.bucket)!).appendingPathComponent(uploadRequest!.key!)
                    if let absoluteString = publicURL?.absoluteString {
                        print("Uploaded to:\(absoluteString)")
                        self.s3count = (self.s3count) + 1
                        
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                }
                return nil
            }
            }
        }
        
        
        func parseData()
            {
                var Token = self.native.string(forKey: "Token")!
                print(Token)
                if Token != nil
                {
                    
                    let b2burl = native.string(forKey: "b2burl")!
                    var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_category_preference/") as! URL)
                    a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
                    
                    Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                        //                                debugPrint(response)
                        print("response: \(response)")
                        let result = response.result
                        switch response.result
                        {
                        case .failure(let error):
                            //                print(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                print("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                print(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                print("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? NSDictionary{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                
                           else  if let innerdict = dict1["data"] as? NSDictionary{
                                let Product = innerdict["category_data"] as! [NSDictionary]
                                for var i in 0..<Product.count {
                                    
                                    self.ChildProduct.append((Product[i]["category"] as? [AnyObject])!)
                                    
    //                                print("Child Product---", Product[i]["category"] as? [AnyObject])
                                    if Product[i]["icon_image_url"] is NSNull
                                    {

                                        self.icon_image_url.append("")
                                    }
                                    else
                                    {
                                        if let image = Product[i]["icon_image_url"] as? String
                                        {
                                     self.icon_image_url.insert(image , at: i)
                                        }
                                        else
                                        {
                                             self.icon_image_url.insert("" , at: i)
                                        }

                                    }
                                    self.ParentProduct.insert(Product[i]["parent_category"] as! String, at: i)
                                }
                                    if self.ChildProduct.count>0
                                    {
                                for var i in 0..<self.ChildProduct[0].count {
                                    self.Data.insert(self.ChildProduct[0][i] as AnyObject, at: i)
                                }
                                    }
                                for var i in 0..<self.ChildProduct.count
                                {
                                    self.totalcount = self.totalcount + self.ChildProduct[i].count
                                }

                            }
                                print("child",self.totalcount, self.Data)
                            if self.Data.count > 0
                            {
        //                        self.blurView.alpha = 0
        //                        self.Activity.stopAnimating()
                            }
                        }
                        }
                        
                    } }
                
            }}



extension UploadUserClosetViewController: GMSAutocompleteViewControllerDelegate{
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    

    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("place", place.coordinate.latitude)
        self.latitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        print("Place name: ", place.name)
        
        print("Place address: ", place.formattedAddress)
        var country = ""
        var pincode = ""
        var address = ""
        address = place.name!
        print("Place attributions: ", place.attributions)
         //Get address Seperated like city,country,postalcode
        
           let arrays : Array = place.addressComponents!
           for i in 0..<arrays.count {

               let dics : GMSAddressComponent = arrays[i]
               let str : String = dics.type
            
               if (str == "country") {
                   print("Country: \(dics.name)")
                country = "\(dics.name)"
                
               }
                else if (str == "administrative_area_level_1") {
                 print("State: \(dics.name)")
                 
                self.stateName1 = "\(dics.name)"
                 
                }
               else if (str == "administrative_area_level_2") {
                print("State: \(dics.name)")
                
                self.region = "\(dics.name)"
                
               }
               else if (str == "locality") {
                print("City: \(dics.name)")
                self.cityName1 = "\(dics.name)"
                
               }
               else if (str == "postal_code"){
                   print("PostalCode:\(dics.name)")
                   pincode = dics.name
                }// this is only to get postalcode of the selected nearby location
           }
        if cityName1 != "" && self.region != "" && self.stateName1 != "" && place.formattedAddress != nil
        {
          
          //address
            self.addressText = "\(cityName1), \(self.stateName1)"
          addressLabel.text = addressText
          if addressText == "Location*"
          {
          self.addressPlaceholder.isHidden = true
          addressLabel.textColor = lightGray
          addressLabel.attributedText = ( "\(addressText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
          }
          else
          {
          self.addressPlaceholder.isHidden = false
          addressLabel.textColor = textColor
          addressLabel.attributedText = ( "\(addressText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
          }
//            self.city.text = "\(cityName1),\(self.stateName1), \(self.region)"
            var country_code = ""
            if native.object(forKey: "country_code") != nil {
                
                country_code = native.string(forKey: "country_code")!
                
            }
//            if country_code == "US"
//            {
//            self.address.text = address
//            }
//            else
//            {
//               self.address.text = ""
//            }
//          searchlocationTitle.isHidden = false
//          searchPlaceholder.isHidden = true
//          locationName.isHidden = false
//          locationName.text = address
//          selectedcitylkid = 0
            if pincode != ""
            {
//                self.pincode.isEnabled = false
                self.is_validated=true
//                self.pincode.text = pincode
            }
            else
            {
//                self.pincode.isEnabled = true
                self.is_validated=false
//                self.pincode.text = ""
//                self.view.makeToast("Please enter correct postal code!", duration: 2.0, position: .center)
            }
        }
        else
        {
//          self.view.makeToast("Sorry this address is not servicable!", duration: 2.0, position: .center)
        }
        
        self.dismiss(animated: true, completion: nil)
    }


    func viewController(viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: NSError) {
        // To handle error
        print(error)

    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        
        self.dismiss(animated: true, completion: nil)
    }

    func didRequestAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }


}
