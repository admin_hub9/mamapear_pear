//
//  SelectProductCategoryViewController.swift
//  Hub9
//
//  Created by Deepak on 24/06/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit


class SelectProductCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var header = ["Used(like-new)", "Used(good)", "Used(fair)"]
    var desc: [[String]] = [ ["Excellent condition, but has previously been worn or used. No sign of wear of defects."], ["Minor signs of wear. Item is operational and works as intended"], ["Some signs of wear and tear or minor defects. item is still usable as intended. Includes items from households with animala or smoking (example: potential hair, stains, or smell)"] ]
    
    
    @IBOutlet weak var productCategory: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
            
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        
        override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.tabBarController?.tabBar.isHidden = false
    //        senddataAddtocart = false
            
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productCategory.tableFooterView = UIView()
        productCategory.reloadData()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return header.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCategory", for: indexPath) as! SelectProductCategoryTableViewCell
        
        cell.headertext.text = self.header[indexPath.row]
        cell.descriptionText.text = self.desc[indexPath.row][0]
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(self.header[indexPath.row])
        UserCloset.conditionText = self.header[indexPath.row]
        UploadUserClosetViewController.conditionID = indexPath.row
        //Condition
        UserCloset.conditionLabel.text = UserCloset.conditionText
        if UserCloset.conditionText == "Select Condition*"
        {
        UserCloset.conditionPlaceholder.isHidden = true
        UserCloset.conditionLabel.textColor = UserCloset.lightGray
        UserCloset.conditionLabel.attributedText = ( "\(UserCloset.conditionText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
        }
        else
        {
        UserCloset.conditionPlaceholder.isHidden = false
        UserCloset.conditionLabel.textColor = UserCloset.textColor
        UserCloset.conditionLabel.attributedText = ( "\(UserCloset.conditionText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
