//
//  QuestionViewController.swift
//  Hub9
//
//  Created by Deepak on 07/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AttributedTextView
import AlignedCollectionViewFlowLayout

class QuestionViewController: UIViewController, UITextViewDelegate, UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    var native = UserDefaults.standard
    
    
    // tag collection variable
           var Product = [AnyObject]()
           var Data = [AnyObject]()
           var icon_image_url = [String]()
           var ParentProduct = [String]()
           var ChildProduct = [[AnyObject]]()
           var selectedCell = [Int]()
           var currentParentIndexIs = 0
           var totalcount = 0
           var selectcount = 0
           var window: UIWindow?

           @IBOutlet var InterestSet: UICollectionView!
           @IBOutlet var ChooseOption: UICollectionView!
    
    

    @IBOutlet weak var questionText: UITextView!
    
    @IBOutlet weak var sendButton: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = true
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(false, animated: true)
    self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if MyFeedPostViewController.is_edit == true
        {
            if myFeedPost.selectedTag.count > MyFeedPostViewController.editIndex
            {
                
               self.selectedCell = myFeedPost.selectedTag[MyFeedPostViewController.editIndex]
                questionText.attributedText=("\(myFeedPost.feed_text[MyFeedPostViewController.editIndex] as! String)".size(15.0)).attributedText
            }
            
        }
        
        self.parseData()
        sendButton.layer.cornerRadius = sendButton.frame.height/2
        sendButton.clipsToBounds=true
        questionText.delegate = self
        questionText.text = "Start your question with \("What"), \("How"), \("Why"), etc. "
        questionText.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
        
        /// collection view flow layout
        let flowLayout = InterestSet?.collectionViewLayout as? AlignedCollectionViewFlowLayout
        flowLayout?.horizontalAlignment = .left
    }
    
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.textColor == UIColor.lightGray {
                textView.text = "?"
                textView.textColor = UIColor.black
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                textView.text = "Please add feed text here"
                textView.textColor = UIColor.lightGray
            }
        }

        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            var touch: UITouch? = touches.first
            //location is relative to the current view
            // do something with the touched point
            if touch?.view?.tag != 20 {
                print("inside view")
            }
            else
            {
                
                questionText.resignFirstResponder()
            }
            
            
        }
        
        
        func textFieldShouldReturn(textField: UITextField) -> Bool {
            
            self.view.endEditing(true)
           
            self.questionText.resignFirstResponder()
                
            return true
            
        }
        
        @objc func keyboardWillShow(notification: NSNotification) {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height + 70
                }
            }
        }

        @objc func keyboardWillHide(notification: NSNotification) {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
        
    
    @IBAction func sendComment(_ sender: Any) {
        
        if self.questionText.text.count > 0
        {
            self.sendFeedData()
        }
    }
    
    
       
    func sendFeedData()
    {
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                var parameters: [String: Any]
                if MyFeedPostViewController.is_edit == true
                {
                    parameters=["feed_typeid":"1", "feed_text": self.questionText.text!,"user_feedid":myFeedPost.feed_id[MyFeedPostViewController.editIndex], "tagid": self.selectedCell]
                }
                else
                {
                parameters=["feed_typeid":"1", "feed_text": self.questionText.text!, "tagid": self.selectedCell]
                }
                
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = ""
                
                if MyFeedPostViewController.is_edit == true
                {
                   url = "\(b2burl)/users/edit_user_feed/"
                }
                else
                {
                    url = "\(b2burl)/users/upload_user_feed/"
                }
                
           
            print("follow product", url, parameters)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                print("upload feed", response)
                switch response.result
                {
                    
                    
                case .failure(let error):
                                    print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if dict1["status"] as! String == "success"
                {
                    
                    
                           
                            
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        // your code here
                        
                        self.questionText.text = nil
                        
                        self.questionText.resignFirstResponder()
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if MyFeedPostViewController.is_edit == true
                        {
                            MyFeedPostViewController.is_edit = false
                            self.view.makeToast("Feed update successfully.", duration: 2.0, position: .center)
                        }
                        else
                        {
                        self.view.makeToast("Feed uploaded successfully.", duration: 2.0, position: .center)
                        }
                        
                        DispatchQueue.main.async {
                            self.selectedCell.removeAll()
                            self.ChooseOption.selectItem(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
                        }
                    }
                    
                    
                    }
                else
                        {
                            self.view.makeToast("Please try later.", duration: 2.0, position: .center)
                        }
                }}
                }
                }}
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
    }
    
    

/// tag collection view
      
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            var ReturnCell = 0
            if collectionView == InterestSet{
                ReturnCell = Data.count
            }
            else
            {
                ReturnCell = ParentProduct.count
            }
            return ReturnCell
        }
        

        func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
            if collectionView == InterestSet
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interestset", for: indexPath) as! InterestCollectionViewCell
    //            print("select", Data[indexPath.row]["id"]!)
                if let id = self.Data[indexPath.row] as? NSDictionary
                 {
                     print("id:", self.Data[indexPath.row], id["parent_categoryid"] as! Int)
                
                if selectedCell.contains(id["parent_categoryid"] as! Int) == false{
                selectedCell.append(id["parent_categoryid"] as! Int)
                    
                }
                else{
                    selectedCell.remove(at: selectedCell.index(of: id["parent_categoryid"] as! Int)!)
                    
                }
                }
                print(selectedCell)
                InterestSet.reloadItems(at: [IndexPath(item: indexPath.row, section: 0)])
            }
            if collectionView == ChooseOption{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interest", for: indexPath) as! SelectInterestCollectionCell
               currentParentIndexIs = indexPath.row
                Data.removeAll()
                for var i in 0..<self.ChildProduct[indexPath.row].count {
                    print(ChildProduct[indexPath.row] as! AnyObject)
                    self.Data.insert(self.ChildProduct[indexPath.row][i] as! AnyObject, at: i)
                    
                    
                }
                ChooseOption.reloadData()
                InterestSet.reloadData()
            }
            
            return true
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            var returnCell = UICollectionViewCell()
            if collectionView == ChooseOption
            {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interest", for: indexPath) as! SelectInterestCollectionCell
                cell.selectedInterest.isHidden = true
                
                if currentParentIndexIs == indexPath.row{
                    cell.selectedInterest.isHidden = false
                }
                else{
                    cell.selectedInterest.isHidden = true
                }
                if let imageURl = self.icon_image_url[indexPath.row] as? String
                {
                    var imageUrl = imageURl
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    
                    if let url = NSURL(string: imageUrl )
                    {
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell.images.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                print("parent name", ParentProduct[indexPath.row] as? String)
                cell.name.text = ParentProduct[indexPath.row] as? String
                
            returnCell = cell
            }
            else{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interestset", for: indexPath) as! InterestCollectionViewCell
               cell.button.setTitle("\(self.Data[indexPath.row]["category_name"] as! NSString)", for: .normal)

                cell.button.layer.borderColor = UIColor.lightGray.cgColor
                if let id = self.Data[indexPath.row] as? NSDictionary
                {
                    print("id:", self.Data[indexPath.row], id["parent_categoryid"] as! Int)
                if selectedCell.contains(id["parent_categoryid"] as! Int) == true
               {


                let redColor = UIColor(red: 117/255.0, green: 76/255.0, blue: 170/255.0, alpha: 0.20)
                let textColor = UIColor(red: 117/255.0, green: 76/255.0, blue: 170/255.0, alpha: 1.00)
                cell.button.setTitleColor(textColor, for: .normal)
                cell.button.layer.borderWidth = 0.0
                cell.button.backgroundColor = redColor

                }
               else{
                let textColor = UIColor(red: 135/255.0, green: 135/255.0, blue: 135/255.0, alpha: 1.00)
                cell.button.setTitleColor(textColor, for: .normal)
                let buttoncolor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.00)
                cell.button.layer.borderWidth = 0.5
                cell.button.backgroundColor = buttoncolor
                }
                }
                cell.button.layer.cornerRadius = cell.button.layer.frame.size.height/2

                cell.button.clipsToBounds=true
                cell.button.layer.borderColor = UIColor.lightGray.cgColor
                returnCell = cell
            }
            return returnCell
        }
    
    
    func parseData()
        {
            var Token = self.native.string(forKey: "Token")!
            print(Token)
            if Token != nil
            {
                
                let b2burl = native.string(forKey: "b2burl")!
                var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_category_preference/") as! URL)
                a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
                
                Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                    //                                debugPrint(response)
                    print("response: \(response)")
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? NSDictionary{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            
                       else  if let innerdict = dict1["data"] as? NSDictionary{
                            let Product = innerdict["category_data"] as! [NSDictionary]
                            for var i in 0..<Product.count {
                                
                                self.ChildProduct.append((Product[i]["category"] as? [AnyObject])!)
                                
//                                print("Child Product---", Product[i]["category"] as? [AnyObject])
                                if Product[i]["icon_image_url"] is NSNull
                                {

                                    self.icon_image_url.append("")
                                }
                                else
                                {
                                    if let image = Product[i]["icon_image_url"] as? String
                                    {
                                 self.icon_image_url.insert(image , at: i)
                                    }
                                    else
                                    {
                                         self.icon_image_url.insert("" , at: i)
                                    }

                                }
                                self.ParentProduct.insert(Product[i]["parent_category"] as! String, at: i)
                            }
                                if self.ChildProduct.count>0
                                {
                            for var i in 0..<self.ChildProduct[0].count {
                                self.Data.insert(self.ChildProduct[0][i] as AnyObject, at: i)
                            }
                                }
                            for var i in 0..<self.ChildProduct.count
                            {
                                self.totalcount = self.totalcount + self.ChildProduct[i].count
                            }

                        }
                            print("child",self.totalcount, self.Data)
                        if self.Data.count > 0
                        {
    //                        self.blurView.alpha = 0
    //                        self.Activity.stopAnimating()
                            self.InterestSet.reloadData()

                            self.ChooseOption.reloadData()
                        }
                    }
                    }
                    
                }
                
            }
            
        }

   
    
}


extension QuestionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var ReturnCell = CGSize(width: 0, height: 0)
        if collectionView == InterestSet
        {

            let widthSize = (self.Data[indexPath.row]["category_name"] as! NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0)])
            let width = Double(self.InterestSet.bounds.width)

            ReturnCell = CGSize(width: widthSize.width + 20, height: widthSize.height + 20)

        }
        else if collectionView == ChooseOption
        {
            ReturnCell = CGSize(width: 70, height: 80)
        }

        return ReturnCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        var ReturnCell = CGSize(width: 0, height: 0)
        if collectionView == InterestSet
        {
        ReturnCell = CGSize(width: self.view.bounds.width+20, height: 40)
        }
        else if collectionView == ChooseOption
        {
         ReturnCell = CGSize(width: 70, height: 80)
        }
        return ReturnCell
    }

}

