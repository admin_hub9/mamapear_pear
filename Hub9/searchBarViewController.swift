//
//  searchBarViewController.swift
//  Hub9
//
//  Created by Deepak on 9/27/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire


class searchBarViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    
    
    var native = UserDefaults.standard
    //search limit
    var limit = 10
    var Product = [String]()
    var filterdItemsArray = [String]()
    var shop : NSArray = []
    var isSearching = false
    static var searchText = ""
    static var type = "product_elastic"
    
    @IBOutlet var searchbar: UISearchBar!
    @IBOutlet var searchTable: UITableView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBAction func cancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        searchbar.becomeFirstResponder()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        searchbar.resignFirstResponder()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let change = native.string(forKey: "searchchange")
        searchbar.becomeFirstResponder()
        if change == "0"
        {
            searchbar.text = ""
            isSearching = true
            //            self.blurView.alpha = 0.8
            //            self.activity.startAnimating()
            searchTable.reloadData()
        }
        
        
    }
    
    @IBAction func segmentControllerAction(_ sender: Any) {
        segmentController.changeUnderlinePosition()
        Product = []
        shop = []
        if (searchbar.text?.count)! > 2
        {
            self.blurView.alpha = 0.8
            self.activity.startAnimating()
        }
        searchTable.isHidden = true
        searchTable.reloadData()
        
        switch segmentController.selectedSegmentIndex
        {
            
        case 0:
            searchBarViewController.type = "product_elastic"
            if (searchbar.text?.count)! > 2
            {
                parseData(text: searchbar.text!)
            }
        //show Product view
        case 1:
            searchBarViewController.type = "shop_elastic"
            if (searchbar.text?.count)! > 2
            {
                parseData(text: searchbar.text!)
            }
        //show Shop view
        default:
            break;
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ///shoe keyborad
        self.tabBarController?.tabBar.isHidden = true
        self.blurView.alpha = 0
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = UIColor.darkGray
        self.activity.stopAnimating()
        segmentController.selectedSegmentIndex = 0
        searchTable.isHidden = true
        searchTable.tableFooterView = UIView()
        segmentController.addUnderlineForSelectedSegment()
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        //
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    //    @objc func keyboardWillShow(notification: NSNotification) {
    //        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
    //            if self.view.frame.origin.y == 0{
    //                self.view.frame.origin.y -= 50
    //            }
    //        }
    //    }
    //
    //    @objc func keyboardWillHide(notification: NSNotification) {
    //        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
    //            if self.view.frame.origin.y != 0{
    //                self.view.frame.origin.y += 50
    //            }
    //        }
    //    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var cell = 0
        if searchBarViewController.type == "product_elastic"
        {
            cell = Product.count
            if Product.count == 0
            {
                searchTable.showEmptyListMessage("No Product Found")
            }
            else
            {
                searchTable.showEmptyListMessage("")
            }
            cell = Product.count
        }
        else if searchBarViewController.type  == "shop_elastic"{
            if shop.count == 0
            {
                searchTable.showEmptyListMessage("No Shop Found")
            }
            else
            {
                searchTable.showEmptyListMessage("")
            }
            cell = shop.count
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchbar.resignFirstResponder()
        if searchBarViewController.type == "product_elastic"
        {
            let producttext = Product as! [String]
            let text = producttext[indexPath.row] as! String
            native.set(text, forKey: "search")
            native.set("product_elastic", forKey: "searchType")
            native.synchronize()
            searchBarViewController.searchText = producttext[indexPath.row] as! String
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "product"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        else if searchBarViewController.type == "shop_elastic"
        {
            let shoptext = shop as! [AnyObject]
            let text = shoptext[indexPath.row]["shop_name"] as! String
            native.set(text, forKey: "search")
            native.set("shop", forKey: "type")
            native.synchronize()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shop"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchtable", for: indexPath) as! searchbarTableViewCell
        
        if searchBarViewController.type == "product_elastic"
        {
            let producttext = Product as! [String]
            cell.name.text = producttext[indexPath.row] as! String
        }
        else if searchBarViewController.type == "shop_elastic"{
            let shoptext = shop as! [AnyObject]
            if shoptext[indexPath.row]["shop_name"] != nil
            {
                cell.name.text = shoptext[indexPath.row]["shop_name"] as! String
            }
        }
        
        return cell
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        debugPrint("cancel")
        
        searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
        perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.50)
    }
    
    @objc func reload(_ searchBar: UISearchBar) {
        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespaces) != "" else {
            debugPrint("nothing to search")
            self.Product = []
            self.shop = []
            searchTable.reloadData()
            return
        }
        
        debugPrint(query)
        isSearching = false
        if query == nil || query == ""
        {
            
            isSearching = false
            view.endEditing(true)
            self.Product = []
            self.shop = []
            searchTable.reloadData()
        }
        else if (query.count) > 2
        {
            isSearching = true
            
            parseData(text: query)
            searchTable.reloadData()
        }
        else
        {
            self.Product = []
            self.shop = []
            searchTable.reloadData()
        }
    }
    
    
    
    //// search bar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if (searchBar.text?.count)! > 2
        {
            searchBarViewController.searchText = searchBar.text!
            
            if searchBarViewController.type == "product_elastic"
            {
                native.set(searchBar.text!, forKey: "search")
                native.set("product_elastic", forKey: "searchType")
                native.synchronize()
                searchBarViewController.searchText = searchBar.text! as! String
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "product"))! as UIViewController
                self.navigationController?.pushViewController(editPage, animated: true)
                
            }
            else if searchBarViewController.type == "shop_elastic"
            {
                
                native.set(searchBar.text!, forKey: "search")
                native.set("shop", forKey: "type")
                native.synchronize()
                let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shop"))! as UIViewController
                self.navigationController?.pushViewController(editPage, animated: true)
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    //fetch suggestion data
    func parseData(text: String)
    {
        searchTable.isHidden = true
        let Token = self.native.string(forKey: "Token")!
        //        if Token != nil
        //        {
        let b2burl = native.string(forKey: "b2burl")!
        debugPrint(b2burl)
        debugPrint(text, searchBarViewController.type)
        //            debugPrint(Token)
        var validateUrl =  "\(b2burl)/search/suggestion/?search_type=\(searchBarViewController.type)&search_query=\(text.lowercased())&limit=\(limit)&offset=0"
        
        validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        debugPrint("========",validateUrl)
        var a = URLRequest(url: NSURL(string:validateUrl)! as URL)
        if Token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(Token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            //                                debugdebugPrint(response)
            debugPrint("responseSerach: \(response)")
            let result = response.result
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                if let invalidToken = dict1["detail"]{
                    if invalidToken as! String  == "Invalid token."
                    { // Create the alert controller
                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = redViewController
                    }
                }
                    
                else if dict1["status"] as! String == "failure"
                {
                    //                        self.ActivityIndicator.stopAnimating()
                    //                        self.blurView.alpha = 0
                    //
                    self.blurView.alpha = 0
                    self.activity.stopAnimating()
                    let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                    
                else{
                    if searchBarViewController.type == "product_elastic"
                    {
                        if let data = dict1["data"] as? [String]
                        {
                            
                            func filterContentForSearchText(searchText: String) {
                                self.filterdItemsArray = data.filter { item in
                                    return item.lowercased().contains(searchText.lowercased())
                                }
                            }
                            
                            filterContentForSearchText(searchText: (self.searchbar.text?.lowercased())!)
                            debugPrint(self.filterdItemsArray)
                            if self.filterdItemsArray != nil
                            {
                                self.Product = self.filterdItemsArray
                            }
                        }
                    }
                    else if searchBarViewController.type == "shop_elastic"{
                        if let innerdict = dict1["data"] as? NSArray{
                            
                            self.shop = dict1["data"] as! NSArray
                        }
                    }
                    
                    self.blurView.alpha = 0
                    self.activity.stopAnimating()
                    self.searchTable.isHidden = false
                    self.searchTable.reloadData()
                    
                }
            }}
    }
    
    
    
    //    }
    
    
}
