//
//  AllSellVariantViewController.swift
//  Hub9
//
//  Created by Deepak on 6/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import iOSDropDown
import AWSS3
import AWSCore
import Alamofire
import RSSelectionMenu

class AllSellVariantViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource {
    var companyID = ""
    var imagePicker: UIImagePickerController!
    var selectedImage = [UIImage]()
    var pickerData = [[String]]()
    var time1 = 0
    var recievedString: String = ""
    var selectedunits = [String]()
    var Vselectedunits = [String]()
    var selctedprice = [Int]()
    var numberOfVariant = 0
    var minutes = 1
    var seconds = Array(1...100)
    var DiscountNumber = Array(1...100)
    var i = 0
    var low = 0
    var variantype = ""
    let native = UserDefaults.standard
    var variant = [Int: [String:Any]]()
    var Pvariant = [["index": Int.self, "low": String(), "high": String(), "price" : String()]]
    var Cvariant = [["index": Int.self, "low": String(), "high": String(), "price" : String()]]
    var variantType = ["Color", "Size", "Material"]
    var colorcount = ["","",""]
    var color = [String]()
    var size = [String]()
    var material = [String]()
    static var selectedcolorID = [String]()
    static var selectedsizeID = [String]()
    static var selectedmaterialID = [String]()
    var selectVariantTypeIndex = 0
    var allVariantType = [String]()
    var sku = [String]()
    var price = [String]()
    var moq = [String]()
    var stock = [String]()
    var editproductID  = "-1"
    var categoryName = [String]()
    var categoryID = [Int]()
    var subCategoryID = [Int]()
    let simpleDataArray = ["Pajama", "Nighty", "Shirt", "T-Shirt", "Jeans", "Bra"]
    var simpleSelectedArray = [String]()
    var selectedCategoryID = ""
    let Brand = ["Masha", "Hub9"]
    var selectedBrandID = ""
    var selectedBrand = [String]()
    var insertVariant = false
    var pickerType = "unit"
    var Discount = "-1"
    ///for edit listing data
    var categoryedit = ""
    var brandedit = ""
    var nameedit = ""
    var skuedit = ""
    var descriptionedit = ""
    var hsncodeedit = ""
    var supplyedit = ""
    var lengthedit = ""
    var widthedit = ""
    var heightedit = ""
    var weightedit = ""
    var editlisting = "false"
    var ParentProductID = ""
    static var variantImages = [[UIImage]]()
    static var VSKu = [String]()
    var productImagename = [String]()
    var VariantImageName = [[String]]()
    var sizeid = "0"
    var colorid = "0"
    var materialid = "0"
    var productID = [Int]()
    var editdatacount = 0
    
    
    
    
   //parent
    @IBOutlet var allVariant: UITableView!
    @IBOutlet var pCancel: UIButton!
    @IBOutlet var pPost: UIButton!
    @IBOutlet var pPrice: UITextField!
    @IBOutlet var pProductVariant: UITableView!
    @IBOutlet var length: UITextField!
    @IBOutlet var width: UITextField!
    @IBOutlet var height: UITextField!
    @IBOutlet var weight: UITextField!
    
    @IBOutlet var activity: UIActivityIndicatorView!
    
    @IBOutlet var scroolview: UIView!
    @IBOutlet var addvariantViewContainer: UIView!
    @IBOutlet var selectBrandLabel: UILabel!
    @IBOutlet var selectCategoryLabel: UILabel!
    @IBOutlet var selectBrand: UIButton!
    @IBOutlet var selectCategory: UIButton!
    @IBOutlet var pName: UITextField!
    @IBOutlet var pAdd: UIButton!
    @IBOutlet var pSku: UITextField!
    @IBOutlet var pHsnCOde: UITextField!
    @IBOutlet var pSupply: UITextField!
    @IBOutlet var AddVariant: UIButton!
    @IBOutlet var pDescription: UITextField!
    @IBOutlet var productImages: UICollectionView!
    @IBOutlet var addVariantButtonView: UIView!
    @IBOutlet var pMRP: UITextField!
    
    @IBOutlet var unitClicked: UITextField!
    @IBOutlet var imageContainer: UIView!
    @IBOutlet var MoreViewContainer: UIView!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var blurView: UIVisualEffectView!
    ///edit variant
    @IBOutlet var editVariantShadwo: UIView!
    @IBOutlet var editVariantName: UILabel!
    @IBOutlet var editvariantsku: UITextField!
    @IBOutlet var editvariantPrice: UITextField!
    
    @IBOutlet var editvariantMOQ: UITextField!
    @IBOutlet var editvariantStock: UITextField!
    
    @IBOutlet var doneEdit: UIButton!
    @IBOutlet var editvariantImages: UICollectionView!
    
    
    
    
    
    //picker View
    @IBOutlet var pickerViewContainer: UIView!
    @IBOutlet var pickerCancel: UIButton!
    @IBOutlet var pikcerDone: UIButton!
    @IBOutlet var unitPicker: UIPickerView!
    
    
    @IBAction func pCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        native.set("-1", forKey: "ProductIDforEditing")
        native.synchronize()
        editproductID = "-1"
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        native.set("-1", forKey: "ProductIDforEditing")
        native.synchronize()
        editproductID = "-1"
    }
    
    // to display number of row for each colomn in picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //        print(pickerView)
        //        return pickerData[component].count
        var row = pickerView.selectedRow(inComponent: 0)
        print("this is the pickerView\(row)")
        if pickerType == "discount"
        {
            if component == 0
            {
                return DiscountNumber.count
            }
                
            else {
                return 1
            }
        }
        else{
        
        if component == 0 {
            return 1
        }
            if component == 1
            {
                return 1
            }
        else {
            return seconds.count
        }

        }
        
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerType == "discount"
        {
            return 1
        }
        else
        {
        return 3
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerType == "discount"
        {
         print("value", DiscountNumber[pickerView.selectedRow(inComponent: 0)])
            pPrice.text = String("\(DiscountNumber[pickerView.selectedRow(inComponent: 0)])% Discount")
            Discount = String("\(DiscountNumber[pickerView.selectedRow(inComponent: 0)])")
        }
        else{
        if pickerType == "unit"
        {
        time1 = seconds[pickerView.selectedRow(inComponent: 2)]
            
        print("value", minutes, seconds[pickerView.selectedRow(inComponent: 2)])
            unitClicked.text = String("\(minutes) to \(seconds[pickerView.selectedRow(inComponent: 2)]) units")
        }
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //        return pickerData[component][row]
        if pickerType == "discount"
        {
           if component == 0
           {
            return String("\(DiscountNumber[row]) %")
            }
            else
           {
            return "%"
            }
        }
        else
        {
        if component == 0 {
            return String(minutes)
            
        }
            else if component == 1
        {
            return "to"
        }
        else {
            
            return String(seconds[row])
        }
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedImage.count > 0
        {
        return selectedImage.count + 1
        }
        else
        {
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var CellCount = UICollectionViewCell()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addproduct", for: indexPath) as! AddProductCell
        if indexPath.row > 0
        {
        
        cell.cancel.layer.cornerRadius = cell.cancel.layer.frame.size.height/2
        cell.cancel.clipsToBounds = true
        cell.images.layer.cornerRadius = 8
        cell.images.image = selectedImage[indexPath.row - 1]
        cell.images.clipsToBounds = true
        cell.camera.isHidden = true
        cell.photos.isHidden = true
        cell.clickPhotos.isHidden = true
        cell.cancel.isHidden = false
        cell.cancel.tag = indexPath.row
        cell.cancel.addTarget(self, action: #selector(RemoveImages(sender:)), for: .touchUpInside)
//            cell.clickPhotos.isHidden = true
        let button = UIButton(frame: CGRect())
        button.layer.cornerRadius = button.layer.bounds.size.width / 2
        button.layer.masksToBounds = true
        cell.cancel.layer.masksToBounds = true
        CellCount = cell
        }
        else
        {
           cell.cancel.isHidden = true
            cell.images.image = nil
            cell.images.layer.cornerRadius = 8
            cell.images.clipsToBounds = true
            cell.camera.isHidden = false
            cell.photos.isHidden = false
            cell.clickPhotos.isHidden = false
            CellCount = cell
        }
        return CellCount
    }
    func uploadAlert()
    {
        print("dfgfghgfgh...")
        self.blurView.alpha = 0
        self.activity.stopAnimating()
        self.ParentProductID = ""
        // Create the alert controller
        self.dismiss(animated: true, completion: nil)
    
    }

    func uploadPhotos()
    {   var updatecount = 0
        for var i in 0..<selectedImage.count
    {
        let accessKey = "AKIAJYCW54FQC6MMHIDQ"
        let secretKey = "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"

        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)

        AWSServiceManager.default().defaultServiceConfiguration = configuration
        print("djshvnv",self.ParentProductID)
        let S3BucketName = "b2b-product-images/images/"+String(companyID)+"/"+String(self.ParentProductID)
        
        let remoteName = pSku.text! + String(i) + ".png"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
//                let image = UIImage(named: "car")
        print(fileURL.lastPathComponent)
        print("image name..", selectedImage[i])
        if let data = UIImageJPEGRepresentation(selectedImage[i], 0.2) {
            print(data.count)
        do {
            try data.write(to: fileURL)
        }
        catch {}
        }
            print("file Url", fileURL)
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.acl = AWSS3ObjectCannedACL.publicRead
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/jpeg"
        uploadRequest.acl = .bucketOwnerRead

        let transferManager = AWSS3TransferManager.default()

        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
            DispatchQueue.main.async {
            }
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }

            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    self?.blurView.alpha = 0
                    self?.activity.stopAnimating()
                    self?.ParentProductID = ""
                    // Create the alert controller
                    self?.dismiss(animated: true, completion: nil)
                    print("Uploaded to:\(absoluteString)")
//                    print("-----",AllSellVariantViewController.variantImages.count)
//                    if AllSellVariantViewController.variantImages.count < 2
//                    {
                    
//                    }
                }
            }

            return nil
        }
        
        }
//        for var i in 0..<AllSellVariantViewController.variantImages.count
//        { for var j in 1..<AllSellVariantViewController.variantImages[i].count
//        {
//            let accessKey = "AKIAJYCW54FQC6MMHIDQ"
//            let secretKey = "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"
//
//            let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
//            let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
//            AWSServiceManager.default().defaultServiceConfiguration = configuration
//
//            let S3BucketName = "b2b-product-images/images/"+String(companyID)+"/"+String(self.ParentProductID)
//            let remoteName = "Variant" + String(i) + String(j) + ".png"
//            let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
//            //                let image = UIImage(named: "car")
//            print(fileURL.lastPathComponent)
//            let data = UIImageJPEGRepresentation(AllSellVariantViewController.variantImages[i][j], 0.3)
//            print(data)
//            do {
//                try data?.write(to: fileURL)
//            }
//            catch {}
//            print("file Url", fileURL)
//            let uploadRequest = AWSS3TransferManagerUploadRequest()!
//            uploadRequest.body = fileURL
//            uploadRequest.acl = AWSS3ObjectCannedACL.publicRead
//            uploadRequest.key = remoteName
//            uploadRequest.bucket = S3BucketName
//            uploadRequest.contentType = "image/jpeg"
//            uploadRequest.acl = .bucketOwnerRead
//
//            let transferManager = AWSS3TransferManager.default()
//
//            transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
//                DispatchQueue.main.async {
//                }
//                if let error = task.error {
//                    print("Upload failed with error: (\(error.localizedDescription))")
//                }
//
//                if task.result != nil {
//                    let url = AWSS3.default().configuration.endpoint.url
//                    let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
//                    if let absoluteString = publicURL?.absoluteString {
//                        print("Uploaded to:\(absoluteString)")
//                        updatecount = updatecount + 1
//                        print("000",self?.selectedImage.count)
//
//                            self?.uploadAlert()
//
//                            // Present the controller
//
//                    }
//
//
//                }
        
                
                
//                return nil
//            }
//        }
//        }
        
        
        
    }
    
    @objc func RemoveImages(sender: UIButton)
    {
        if sender.tag > 0
        {
            selectedImage.remove(at: sender.tag - 1)
            productImages.reloadData()
            
            i = i - 1
        }
         if selectedImage.count == 0{
            ///main view container
            UIView.transition(with: MoreViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0.6
                    self.MoreViewContainer.transform = CGAffineTransform(translationX: 0, y: -100)
                    
                    self.imageContainer.isHidden = true
            }, completion: nil)
        }
    }

    @IBAction func select_Images(_ sender: Any) {
        clickPhotos(self)
    }
    
    
    @IBAction func clickPhotos(_ sender: Any) {
    
        let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert: UIAlertAction) in
            print("Take Photo Code")
            if(UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.allowsEditing = true
                myPickerController.sourceType = .camera
                self.present(myPickerController, animated: true, completion: nil)
            }
            else
            {
                let actionController: UIAlertController = UIAlertController(title: "Camera is not available",message: "", preferredStyle: .alert)
                let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void  in
                    //Just dismiss the action sheet
                }
                actionController.addAction(cancelAction)
                self.present(actionController, animated: true, completion: nil)
            }}

        let gallerySelect = UIAlertAction(title: "Choose from Photos", style: .default) { (alert: UIAlertAction) in
            print("Gallery Select Code")
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
            print("Cancelled")
        }

        actionMenu.addAction(takePhoto)
        actionMenu.addAction(gallerySelect)
        //          actionMenu.addAction(savePhoto)
        actionMenu.addAction(cancelAction)

        self.present(actionMenu, animated: true, completion: nil)
    }
    //#MARK: Image picker protocol functions

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let yourPickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            print("-------", yourPickedImage)
        }
        if info[UIImagePickerControllerEditedImage] as? UIImage != nil {
            print("gallery Images:", info[UIImagePickerControllerOriginalImage] as? UIImage)
            let data = (info[UIImagePickerControllerOriginalImage] as? UIImage)
        
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                selectedImage.insert(image, at: selectedImage.count)
                i = i + 1
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                selectedImage.insert(image, at: selectedImage.count)
                i = i + 1
                for var i in 0..<self.selectedImage.count {
                    print("selected Image", selectedImage[i])}
            }
            else
            {
                print("Something went wrong")
            }
            self.dismiss(animated: true, completion: nil)
            productImages.reloadData()

        }


        //        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        //        profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var val = 0
        if tableView == pProductVariant
        {
            val = Pvariant.count
        }
        else
        {
            val = allVariantType.count
        }
        return val
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell() // Dummy value
        if tableView == pProductVariant {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addproductTable", for: indexPath) as! AddProductTableCell
            cell.units.text = selectedunits[indexPath.row]
            cell.cost.text = String("\(selctedprice[indexPath.row])%")
            
            if indexPath.row == Pvariant.count - 1
            {
                cell.delete.isHidden = false
                cell.delete.tag = indexPath.row
                cell.delete.addTarget(self, action: #selector(delete_Disscount(sender:)), for: .touchUpInside)
            }
            else{
                cell.delete.isHidden = true
            }
            
            cellToReturn = cell
        }
        
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "allVariantCell", for: indexPath) as! AllVariantViewCell
            if editlisting == "false"
            {
            cell.VariantProductName.text = allVariantType[indexPath.row]
            cell.VariantSkuName.text = sku[indexPath.row]
            cell.CancelVariantButton.tag = indexPath.row
            cell.CancelVariantButton.addTarget(self, action: #selector(delete_variant(sender:)), for: .touchUpInside)
                cellToReturn = cell
                
            }
            else{
                if indexPath.row < self.editdatacount
                {   let image = UIImage(named: "editlist")
                    cell.CancelVariantButton.setImage(image, for: .normal)
                    cell.VariantProductName.text = allVariantType[indexPath.row]
                    cell.VariantSkuName.text = sku[indexPath.row]
                    cell.CancelVariantButton.addTarget(self, action: #selector(edit_list), for: .touchUpInside)
                    cellToReturn = cell
                }
                else{
                cell.CancelVariantButton.isHidden = false
                cell.VariantProductName.text = allVariantType[indexPath.row]
                cell.VariantSkuName.text = sku[indexPath.row]
                cell.CancelVariantButton.tag = indexPath.row
                cell.CancelVariantButton.addTarget(self, action: #selector(delete_variant(sender:)), for: .touchUpInside)
                cellToReturn = cell
                }}
        }

        return cellToReturn
    }
    @objc func edit_list()
    {   editvariantMOQ.text = moq[0]
        editvariantsku.text = sku[0]
        editvariantPrice.text = price[0]
        editvariantStock.text = stock[0]
        editVariantName.text = variantType[0]
        editvariantsku.isEnabled = false
        editVariantShadwo.isHidden = false
    }
    @objc func delete_variant(sender: UIButton)
    {
        allVariantType.remove(at: sender.tag)
        sku.remove(at: sender.tag)
        price.remove(at: sender.tag)
        moq.remove(at: sender.tag)
        stock.remove(at: sender.tag)
        ///create default storage for array
        UserDefaults.standard.set(sku, forKey: "sku")
        native.synchronize()
        UserDefaults.standard.set(price, forKey: "price")
        native.synchronize()
        UserDefaults.standard.set(moq, forKey: "moq")
        native.synchronize()
        UserDefaults.standard.set(stock, forKey: "stock")
        native.synchronize()
        UserDefaults.standard.set(allVariantType, forKey: "title")
        native.synchronize()
        
        allVariant.reloadData()
    }
    
    @objc func delete_Disscount(sender: UIButton)
    {   print("selectedunits", Pvariant)
        
        print(Pvariant[sender.tag]["low"])
        minutes = Pvariant[sender.tag]["low"] as! Int
        seconds = Array(minutes...minutes + 100)
        unitPicker.reloadAllComponents()
        Pvariant.remove(at: sender.tag)
        selectedunits.remove(at: sender.tag)
        selctedprice.remove(at: sender.tag)
        if self.selectedunits.count == 0
        {
            self.addVariantButtonView.transform = CGAffineTransform(translationX: 0, y: -200)
            self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
        }
        else
        {
            self.addVariantButtonView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1300)
        }
        numberOfVariant = numberOfVariant - 1
        pProductVariant.reloadData()
        
        print("selectedunits", selectedunits)
        print("selectedunits", Pvariant)
        
    }

    // sku and product name
    func isValidInput(Input:String) -> Bool {
        let RegEx = "[a-zA-Z.?0-9/_-]*"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    
    //special characters
    func validateHsnCode(Input:String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "[A-Za_z0-9]]").inverted
        let inputString = Input.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  Input == filtered
    }
    //number
    func validate(Number: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = Number.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  Number == filtered
    }
    //decimal
    func valueCheck(_ d: String) -> String {
        var result = ""
        do {
            let regex = try NSRegularExpression(pattern: "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$", options: [])
            let results = regex.matches(in: String(d), options: [], range: NSMakeRange(0, String(d).characters.count))
            if results.count > 0 {result = d}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
        }
        print("check regex",result)
        return result
    }
    func Parentvalidated() -> Bool {
        var valid: Bool = true
        if ((pHsnCOde.text?.isEmpty) != true) && valueCheck(pHsnCOde.text!) == ""{
            pHsnCOde.text?.removeAll()
            // change placeholder color to red color for textfield email-id
            pHsnCOde.attributedPlaceholder = NSAttributedString(string: " HSN Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if ((pSku.text?.isEmpty) == true || isValidInput(Input: pSku.text!) == false) {
            // change placeholder color to red for textfield username
            pSku.text?.removeAll()
            pSku.attributedPlaceholder = NSAttributedString(string:" Invalid SKU",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if ((pMRP.text?.isEmpty) != true && valueCheck(pMRP.text!) == "") {
            // change placeholder color to red for textfield username
            pMRP.text?.removeAll()
            pMRP.attributedPlaceholder = NSAttributedString(string:" Invalid MRP",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if ((pName.text?.isEmpty) == true) || isValidInput(Input: pName.text!) == false{
            // change placeholder color to red for textfield password
            pName.text?.removeAll()
            pName.attributedPlaceholder = NSAttributedString(string:"Name",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((pSupply.text?.isEmpty) != true) && validate(Number: pSupply.text!) == false{
            // change placeholder color to red for textfield mobile no.
            pSupply.text?.removeAll()
            pSupply.attributedPlaceholder = NSAttributedString(string:"Supply Ability",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((pDescription.text?.isEmpty) == true) {
            pDescription.text?.removeAll()
            // change placeholder color to red for textfield mobile no.
            pDescription.attributedPlaceholder = NSAttributedString(string:"Please fill Description",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((length.text?.isEmpty) != true) && valueCheck(length.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            length.text?.removeAll()
            length.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((width.text?.isEmpty) != true) && valueCheck(width.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            width.text?.removeAll()
            width.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((height.text?.isEmpty) != true) && valueCheck(height.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            height.text?.removeAll()
            height.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((weight.text?.isEmpty) != true) && valueCheck(weight.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            weight.text?.removeAll()
            weight.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        
        //
        return valid
    }
    
    @IBAction func addVariant(_ sender: Any) {
        
        if Parentvalidated() == true
        {
        if selectedImage.count > 0
        {
            if (selectCategoryLabel.text?.count)! > 0 && (selectBrandLabel.text?.count)! > 0
            {
            
        if (pName.text?.count != 0  && pSku.text?.count != 0  && pDescription.text?.count != 0
            )
        {
//            if  isValidInput(Input: pSku.text!) == true
//        {
            AlertAddVariant()
//            }
//        else{
//            let alert = UIAlertController(title: "Alert", message: "Invalid Sku", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//            }
            
        }
        }
            else{
                let alert = UIAlertController(title: "Alert", message: "Please Select Catefory and Brand", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "Please Add Some Images", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        }
        else{
            let alert = UIAlertController(title: "Alert", message: "Please enter correct data", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        }
    public func copy_data(images:[[UIImage]], skudata:[String])
    {
        print(images, skudata)
        for var i in 0..<images.count
        {
            
            AllSellVariantViewController.variantImages.append(images[i])
            
        }
        for var i in 0..<skudata.count
        {
            
            AllSellVariantViewController.VSKu.append(skudata[i])
            
        }
        print("bjhbhd",AllSellVariantViewController.variantImages, AllSellVariantViewController.VSKu)
    }
    func copyVariantType(colorlk:[String],sizelk:[String],materiallk:[String])
    {
        print("typedata...",colorlk,sizelk,materiallk)
        AllSellVariantViewController.selectedcolorID = colorlk
        AllSellVariantViewController.selectedsizeID = sizelk
        AllSellVariantViewController.selectedmaterialID = materiallk
    }
    

    @IBAction func doneButton(_ sender: Any) {
        if selectedImage.count > 0
        {
        if sku.count > 0
        {
        if (pName.text?.count != 0 && selectBrandLabel.text != "" && pSku.text?.count != 0 && selectCategoryLabel.text != "")
        {
            print(sku.count, sku)
            
            print("bjhbhd......",AllSellVariantViewController.variantImages, AllSellVariantViewController.VSKu)
//            for var i in 0..<sku.count
//            {
//                VariantImageName.append([""])
//                if AllSellVariantViewController.variantImages[i].count > i
//                {
//                for var j in 1..<AllSellVariantViewController.variantImages[i].count
//            {
//
//                VariantImageName[i].insert("Variant" + String(i) + String(j) + ".png", at: j - 1)
//                 print(i,j,VariantImageName )
//                    }}}
            let listvale = native.string(forKey: "ProductIDforEditing")!
            print(VariantImageName )
            for var i in 0..<VariantImageName.count{
                VariantImageName.remove(at: 0)
            }
            print(VariantImageName )
            for var i in 0..<selectedImage.count
            {
                
                self.productImagename.insert(pSku.text! + String(i) + ".png", at: i)
            }
            if listvale != "-1"
            {
            
            for var i in 0..<sku.count
            {
                
                variant[i] = ["name": variantType[i],
                              "sku": sku[i],
                              "price": price[i],
                              "min_order_qty": moq[i],
                              "stock": stock[i],
                              "image_url": [],
                              "color_lkpid": self.colorid,
                              "size_lkpid": self.sizeid,
                              "material_lkpid": self.materialid,
                              "productid": self.productID[i]]
            }
            }
            else{
                for var i in 0..<sku.count
                {
                variant[i] = ["name": variantType[i],
                              "sku": sku[i],
                              "price": price[i],
                              "min_order_qty": moq[i],
                              "stock": stock[i],
                              "image_url": [],
                              "color_lkpid": "3",
                              "size_lkpid": "",
                              "material_lkpid": "",]
                }
            }

            var variant_data = [AnyObject]()
            for var i in 0..<variant.count
            {
                variant_data.insert(variant[i] as AnyObject, at: i)
                print(Pvariant.count)
            }
            var parameters: [String: Any]
            if listvale != "-1"
            {
                var hsncode1 = "null"
                var sku1 = "null"
                var  mrp1 = "null"
                var weight1 = "null"
                var height1 = "null"
                var width1 = "null"
                var lenght1 = "null"
                
                if (pHsnCOde.text?.count)! > 0
                {
                hsncode1 = pHsnCOde.text!
                }
                if (pSku.text?.count)! > 0
                {
                    sku1 = pSku.text!
                }
                if (pMRP.text?.count)! > 0
                {
                    mrp1 = pMRP.text!
                }
                if (weight.text?.count)! > 0
                {
                    weight1 = weight.text!
                }
                if (height.text?.count)! > 0
                {
                    height1 = height.text!
                }
                if (length.text?.count)! > 0
                {
                    lenght1 = length.text!
                }
                if (width.text?.count)! > 0
                {
                    width1 = width.text!
                }
                 parameters = [
                    "parent_product_data": [
                        "parent_productid": self.ParentProductID,
                        "is_single_product": false,
                        "name": pName.text!,
                        "sku": pSku.text!,
                        "mrp": mrp1,
                        "hsn_code": hsncode1,
                        //                    "brand": ["brand_lkpid": selectedBrandID, "brand": selectBrandLabel.text!],
                        "supply_ability_daily":"null",
                        "sub_categoryid": self.categoryID[0],
                        "image_url": productImagename,
                        "description": pDescription.text!,
                        "dynamic_discount_range" : Pvariant,
                        "dimension": ["weight": weight1, "height": height1, "length": lenght1, "breadth": width1],
                        "variant_data" : variant_data]]
            }
            else
            {
                 parameters = [
                    "parent_product_data": [
                        "is_single_product": false,
                        "name": pName.text!,
                        "sku": pSku.text!,
                        "mrp": pMRP.text!,
                        "hsn_code": pHsnCOde.text!,
                        "brand": ["brand_lkpid": selectedBrandID, "brand_name": selectBrandLabel.text!],
                        "supply_ability_daily":"null",
                        "sub_categoryid": self.categoryID[0],
                        "image_url": productImagename,
                        "description": pDescription.text!,
                        "dynamic_discount_range" : Pvariant,
                        "dimension": ["weight": weight.text!, "height": height.text!, "length": length.text!, "breadth": width.text!],
                        "variant_data" : variant_data]]
            }
            
            let token = self.native.string(forKey: "Token")!
            self.blurView.alpha = 1
            self.activity.startAnimating()
            print("token is ", token)
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            print("parameters", parameters)
            
            var url = "https://demo.hub9.io/api/create_listing/"
            if listvale != "-1"
            {
                url = "https://demo.hub9.io/api/edit_listing/"
            }
            
            Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in


                //            debugPrint(response)
                print("response: \(response)")
                switch(response.result) {

                case .success(_):
                    if let data = response.result.value{
                        print(response.result.value)
                        self.allVariant.reloadData()
                        //    response: SUCCESS: {
                        //    data =     {
                        //    "parent_product_data" =         {
                        //    "image_url" =             (
                        //    "https://b2b-product-images.s3.amazonaws.com/images/3/5186/testing-060.png"
                        //    );
                        //    "parent_productid" = 5186;
                        //    sku = "testing-06";
                        //    };
                        //    "variant_data" =         (
                        //    {
                        //    "image_url" =                 (
                        //    );
                        //    productid = 5206;
                        //    sku = "Amethyst-123";
                        //    }
                        //    );
                        //    };
                        //    status = success;
                        //    }
                        ////
                        if let dict1 = response.result.value as? Dictionary<String, AnyObject>{
                            if dict1["status"] as! String == "failure"
                            {
                                self.blurView.alpha = 0
                                self.activity.stopAnimating()
                                let alert = UIAlertController(title: "Alert", message: dict1["status"] as! String, preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }else
                            {
                            let innerdict = dict1["data"] as! NSDictionary

                            print("inner dict", innerdict)
                                if listvale != "-1"
                                {
                                    if dict1["status"] as! String == "success"
                                    {
//                                        self.blurView.alpha = 0
//                                        self.activity.stopAnimating()
                                        self.brandedit = ""
                                        self.categoryedit = ""
                                        
                                        self.categoryID.removeAll()
                                        self.descriptionedit = ""
                                        
                                        self.ParentProductID = ""
                                        self.nameedit = ""
                                        self.skuedit = ""
                                        self.ParentProductID = ""
                                        // Create the alert controller
                                        
                                        self.ParentProductID = String(innerdict["parent_productid"] as! Int)
                                       self.native.set("-1", forKey: "ProductIDforEditing")
                                        self.native.synchronize()
                                        let alertController = UIAlertController(title: "Success", message: "Product Succfully Updated", preferredStyle: .alert)
                                        
                                        // Create the actions
//                                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
//                                            UIAlertAction in
//                                            NSLog("OK Pressed")
//                                            self.dismiss(animated: true, completion: nil)
//                                        }
//
//                                        alertController.addAction(okAction)
//
//                                        // Present the controller
//                                        self.present(alertController, animated: true, completion: nil)
                                    }
                                }
                                else
                                {
                            let data = innerdict
                                var productDeatil = innerdict["parent_product_data"] as! NSDictionary
                                print("gjhdbc", productDeatil)
                                print("vh", productDeatil["image_url"])
                                print("gvhdsv", productDeatil["parent_productid"])
                                self.ParentProductID = String(productDeatil["parent_productid"] as! Int)
                            print(" json", data)
                            print(self.ParentProductID)
                                }}
                        self.uploadPhotos()
                        }
//                        UIView.transition(with: self.addvariantViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
//                            {
//                                //                                self.blueView.alpha = 0.6
//                                self.addVariantContainer.transform = CGAffineTransform(translationX: 0, y: 480)
//                                self.addVariantContainer.isHidden = true
//                        }, completion: nil)
                    }
                    break

                case .failure(_):
                    print(response.result.error)
                    break

                }
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)

                    //                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
            }

        }
        }
        else
        {   self.blurView.alpha = 0
            self.activity.stopAnimating()
            let alert = UIAlertController(title: "Alert", message: "Add some Variant First", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        }
            else
            {   self.blurView.alpha = 0
                self.activity.stopAnimating()
                let alert = UIAlertController(title: "Alert", message: "Please Add Atleast one Product image", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        pName.resignFirstResponder()
        pSku.resignFirstResponder()
        pDescription.resignFirstResponder()
        pHsnCOde.resignFirstResponder()
        pSupply.resignFirstResponder()
        length.resignFirstResponder()
        width.resignFirstResponder()
        height.resignFirstResponder()
        weight.resignFirstResponder()
        
        
        
    }
    @objc func selectunit(_ sender: UITapGestureRecognizer) {
        print("detected tap!")
        pickerType = "unit"
        unitPicker.reloadAllComponents()
        UIView.transition(with: pickerViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                self.blurView.alpha = 1
                self.pickerViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                self.pickerViewContainer.isHidden = false
        }, completion: nil)
    }
    @objc func tappedTextView(_ sender: UITapGestureRecognizer) {
        print("detected tap!")
        pickerType = "discount"
        unitPicker.reloadAllComponents()
        UIView.transition(with: pickerViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                self.blurView.alpha = 1
                self.pickerViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                self.pickerViewContainer.isHidden = false
        }, completion: nil)
    }
    func validateEditVariant()-> Bool {
        var valid: Bool = true
        if ((editvariantStock.text?.count)! <= 0) || validate(Number: editvariantStock.text!) == false{
            editvariantStock.text?.removeAll()
            // change placeholder color to red color for textfield email-id
            editvariantStock.attributedPlaceholder = NSAttributedString(string: " Invalid Stock", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if ((editvariantPrice.text?.count)! <= 0) || validate(Number: editvariantPrice.text!) == false{
            editvariantPrice.text?.removeAll()
            // change placeholder color to red color for textfield email-id
            editvariantPrice.attributedPlaceholder = NSAttributedString(string: " Invalid Price", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if ((editvariantMOQ.text?.count)! <= 0) || validate(Number: editvariantMOQ.text!) == false{
            editvariantMOQ.text?.removeAll()
            // change placeholder color to red color for textfield email-id
            editvariantMOQ.attributedPlaceholder = NSAttributedString(string: " Invalid MOQ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        return valid
    }
    @IBAction func finishEditVariant(_ sender: Any) {
        if validateEditVariant() == true
        {
        stock[0] = editvariantStock.text!
        price[0] = editvariantPrice.text!
        moq[0] = editvariantMOQ.text!
        
        editVariantShadwo.isHidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.blurView.alpha = 1
        self.activity.startAnimating()
        editVariantShadwo.isHidden = true
        companyID = native.string(forKey: "shopid")!
        native.set("", forKey: "brand")
        native.set("", forKey: "brandID")
        native.synchronize()
        editproductID = native.string(forKey: "ProductIDforEditing")!
        print("editproductID", editproductID)
        if editproductID != "-1"
        {
            EditData()
            editproductID = "-1"
            
        }
        else{
            readLocalJsonFile()
        
        ///create default storage for array
        UserDefaults.standard.set(sku, forKey: "sku")
        native.synchronize()
        UserDefaults.standard.set(price, forKey: "price")
        native.synchronize()
        UserDefaults.standard.set(moq, forKey: "moq")
        native.synchronize()
        UserDefaults.standard.set(stock, forKey: "stock")
        native.synchronize()
        UserDefaults.standard.set(allVariantType, forKey: "title")
        native.synchronize()
            
        }
        pCancel.layer.cornerRadius = pCancel.layer.frame.size.height/2
        pCancel.clipsToBounds = true
        pPost.layer.cornerRadius = pPost.layer.frame.size.height/2
        pPost.clipsToBounds = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        ///discount
        let Discount = UITapGestureRecognizer()
        Discount.addTarget(self, action: #selector(tappedTextView(_:)))
        pPrice.addGestureRecognizer(Discount)
        let Unit = UITapGestureRecognizer()
        Unit.addTarget(self, action: #selector(selectunit(_:)))
        unitClicked.addGestureRecognizer(Unit)
        Pvariant.removeAll()
        
        pickerViewContainer.isHidden = true
        UIView.transition(with: addVariantButtonView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0.6
                self.addVariantButtonView.transform = CGAffineTransform(translationX: 0, y: -200)

                self.addVariantButtonView.isHidden = false
        }, completion: nil)
        
        ///main view container
        UIView.transition(with: MoreViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0.6
                self.MoreViewContainer.transform = CGAffineTransform(translationX: 0, y: -100)
                
                self.imageContainer.isHidden = true
        }, completion: nil)
        self.blurView.alpha = 0
        self.activity.stopAnimating()

    }
    override func viewDidAppear(_ animated: Bool) {
        //retrive array data
        if (native.string(forKey: "createvarient")) == "true"
        {
        if let arr = UserDefaults.standard.array(forKey: "sku") as? [String]{
//            print(arr)
            for var i in 0..<arr.count
            {
            sku.append(arr[i])
                let listvale = native.string(forKey: "ProductIDforEditing")!
                if listvale != "-1"
                {
            self.productID.append(0)
                }
            }
            print("sku", sku)
        }
        if let arr = UserDefaults.standard.array(forKey: "price") as? [String]{
//            print(arr)
            for var i in 0..<arr.count
            {
                price.append(arr[i])
            }
            print("price", price)
        }
        if let arr = UserDefaults.standard.array(forKey: "moq") as? [String]{
//            print(arr)
            for var i in 0..<arr.count
            {
                moq.append(arr[i])
            }
            print("moq", moq)
        }
        if let arr = UserDefaults.standard.array(forKey: "stock") as? [String]{
//            print(arr)
            for var i in 0..<arr.count
            {
                stock.append(arr[i])
            }
            print("stock", stock)
        }
        if let arr = UserDefaults.standard.array(forKey: "title") as? [String]{
//            print(arr)
            for var i in 0..<arr.count
            {
                allVariantType.append(arr[i])
            }
            print("allVariantType", allVariantType)
        }
            native.set("false", forKey: "createvarient")
            native.synchronize()
        }
//        UserDefaults.standard.set("", forKey: "sku")
//        native.synchronize()
//        UserDefaults.standard.set("", forKey: "price")
//        native.synchronize()
//        UserDefaults.standard.set("", forKey: "moq")
//        native.synchronize()
//        UserDefaults.standard.set("", forKey: "stock")
//        native.synchronize()
//        UserDefaults.standard.set("", forKey: "title")
//        native.synchronize()
        ///main view container
        if selectedImage.count > 0
        {
        UIView.transition(with: MoreViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0.6
                self.MoreViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                if self.selectedunits.count > 0
                {
                    self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1250)
                }
                else
                {
                    self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1100)
                }
                self.imageContainer.isHidden = false
        }, completion: nil)
        }else{
            ///main view container
            UIView.transition(with: MoreViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0.6
                    self.MoreViewContainer.transform = CGAffineTransform(translationX: 0, y: -100)
                    if self.selectedunits.count > 0
                    {
                     self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1250)
                    }
                    else
                    {
                    self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 850)
                    }
                    self.imageContainer.isHidden = true
            }, completion: nil)
        }
            print("sku",sku)
            allVariant.reloadData()
        let brand = native.string(forKey: "brand")
        selectedBrandID = native.string(forKey: "brandID")!
        print("selectedBrandID", selectedBrandID)
        if brand != ""
        {
            selectBrandLabel.text = brand
        }
        native.set("", forKey: "brand")
        native.synchronize()
    }
    @IBAction func selectCategory(_ sender: Any) {
        selection()
    }

    @objc func selection()
    {
        let selectionMenu =  RSSelectionMenu(dataSource: categoryName) { (cell, object, indexPath) in
            cell.textLabel?.text = object
            
            // Change tint color (if needed)
            cell.tintColor = .blue
            

        }
        
        // set default selected items when menu present on screen.
        // Here you'll get onDidSelectRow
        selectionMenu.setNavigationBar(title: "Select Category", attributes: nil, tintColor: UIColor.black)
        selectionMenu.setSelectedItems(items: simpleSelectedArray) { (text, index, isSelected, selectedItems)   in

            // update your existing array with updated selected items, so when menu presents second time updated items will be default selected.
            self.simpleSelectedArray = selectedItems
            self.selectCategoryLabel.text = selectedItems[0]
            for var i in 0..<self.categoryName.count
            {
                if (self.categoryName[i] == self.selectCategoryLabel.text!)
                {
                print("dff45545",self.categoryID[i])
                    
                    self.selectedCategoryID = String(self.categoryID[i])
                    self.native.set(self.subCategoryID[i], forKey: "subcategoryID")
                    self.native.set(self.categoryID[i], forKey: "categoryID")
                    self.native.synchronize()
                }
            }

        }
        
        // show searchbar
        selectionMenu.showSearchBar { (searchtext) -> ([String]) in
            
            // return filtered array based on any condition
            // here let's return array where name starts with specified search text
            
            return self.categoryName.filter({ $0.lowercased().hasPrefix(searchtext.lowercased()) })
        }
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .push, from: self)

    }
    @IBAction func selectBrand(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "brand") as! SelectBrandViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    

    func EditData()
    { let Token = self.native.string(forKey: "Token")!
        
        print("token", Token)
        if Token != nil
        {   editlisting = "true"
            pPost.setTitle("Save", for: .normal)
            //            ProductID.removeAll()
            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_detail/?parent_productid=\(editproductID)")! as URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                let result = response.result
                
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    let innerdict = dict1["data"] as! NSDictionary
                    print(innerdict["brand_name"]! )
                    self.brandedit = innerdict["brand_name"] as! String
                    print(innerdict["categry_name"]! )
                    self.categoryedit = innerdict["categry_name"] as! String
                    print(innerdict["description"]! )
                    self.categoryID.insert(innerdict["sub_categoryid"] as! Int, at: 0)
                    self.descriptionedit = innerdict["description"]! as! String
                    print(innerdict["parent_productid"] as! Int)
                    self.ParentProductID = String(innerdict["parent_productid"] as! Int)
                    self.nameedit = innerdict["parent_product_name"] as! String
                    self.skuedit = innerdict["parent_product_sku"] as! String
                    var parentImage = innerdict["parent_product_image_url"] as? [AnyObject]
                    print(parentImage)
                    if (parentImage?.count)! > 0
                    {
                    for var i in 0..<(parentImage?.count)!
                   {
                    let url = URL(string:parentImage![i] as! String)
                    if let data = try? Data(contentsOf: url!)
                    {
                        let image: UIImage = UIImage(data: data)!
                        print("image...", image)
                        self.selectedImage.insert(image, at: i)
                    }

//                    self.selectedImage.insert(parentImage![i] as! UIImage, at: i)
                    }
                    }
//                    print(innerdict["variant_data"] as AnyObject)
                    if innerdict["variant_data"] is NSNull
                    {
                      print("No variants")
                    }
                    else
                    {
                    let variantData = innerdict["variant_data"] as! [[String:Any]]
                    
                    for var i in 0..<variantData.count
                    {
                        self.editdatacount = self.editdatacount + 1
                    print(variantData[i]["name"] as! AnyObject)
                    let price = variantData[i]["price"] as! Int
                    self.price.insert(String(price), at: i)
                    self.colorid = String(variantData[i]["color_lkpid"] as! Int)
                     self.sizeid = String(variantData[i]["size_lkpid"] as! Int)
                     self.materialid = String(variantData[i]["material_lkpid"] as! Int)
                    
                    print(variantData[i]["price"] as! AnyObject)
                    print(variantData[i]["sku"] as! AnyObject)
                    self.sku.insert(variantData[i]["sku"] as! String, at: i)
                    print(variantData[i]["stock"] as! AnyObject)
                    let stock = variantData[i]["stock"] as! Int
                    self.stock.insert(String(stock), at: i)
                    self.productID.insert(variantData[i]["productid"] as! Int, at: i)
                    let moq = variantData[i]["min_order_qty"] as! Int
                    self.moq.insert(String(moq), at: i)
//                    self.categoryID.insert(variantData[0]["sub_categoryid"] as! Int, at: 0)
                    self.variantType.insert(variantData[i]["name"] as! String, at: i)
                    self.allVariantType.insert(variantData[i]["name"] as! String, at: i)
                    var images = variantData[i]["image_url"] as? [AnyObject]
                    
                    print(images)
                    if (images?.count)! > 0
                    {
                        self.VariantImageName.append([""])
                        for var i in 0..<(images?.count)!
                        {
                            self.VariantImageName.insert(["variant"+String(i)], at: i)
                        }
                    }
                    else{
                        self.VariantImageName.append([""])
                        }}}
//                    self.ProductImage = images[1] as! String
//                    print("product images",self.ProductImage)
//                    print(variantData[0]["name"] as! AnyObject)
//                    self.name = (variantData[0]["name"] as! AnyObject) as! String
//                    print(variantData[0]["product_status_lkpid"] as! AnyObject)
//                    print(variantData[0]["sku"] as! AnyObject)
//                    self.sku = (variantData[0]["sku"] as! AnyObject) as! String
//                    print(variantData[0]["brand"] as! AnyObject)
//                    self.brand = (variantData[0]["brand"] as! AnyObject) as! String
                    if innerdict.count > 0
                    {
//                        self.singleProductTable.reloadData()
                        self.productImages.reloadData()
                        self.allVariant.reloadData()
//                        self.collectData()
                        self.selectBrand.isEnabled = false
                        self.selectCategory.isEnabled = false
                        self.selectBrandLabel.text = self.brandedit
                        self.selectCategoryLabel.text = self.categoryedit
                        self.pName.text = self.nameedit
                        self.pSku.text = self.skuedit
                        self.pName.isEnabled = false
                        self.pSku.isEnabled = false
                        self.pDescription.text = self.descriptionedit
                        
                        if self.selectedImage.count == 0{
                            ///main view container
                            UIView.transition(with: self.MoreViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                                {
                                    //                                self.blueView.alpha = 0.6
                                    self.MoreViewContainer.transform = CGAffineTransform(translationX: 0, y: -100)
                                    
                                    self.imageContainer.isHidden = true
                            }, completion: nil)
                        }
                        else{
                            UIView.transition(with: self.MoreViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                                {
                                    //                                self.blueView.alpha = 0.6
                                    self.MoreViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                                    self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1200)
                                    self.addVariantButtonView.isHidden = false
                                    self.imageContainer.isHidden = false
                            }, completion: nil)
                        }
                    }
                }
                
                
            }
            
        }
        
    }
    
    @IBAction func insertPVariantButton(_ sender: Any) {
        insertVariant = true
        
        if((pPrice.text?.isEmpty)! != true ){
            var price = pPrice.text!
            price = price.replacingOccurrences(of: "% Discount", with: "")
            
                Pvariant.append(["index": numberOfVariant, "low": minutes, "high": time1, "price" : price])
            print("variant product", Pvariant)
            selctedprice.append(Int(Discount)!)
            selectedunits.insert(String(String(minutes) + " - " + String(time1) + " Units"), at: numberOfVariant)
            minutes = time1 + 1
            if minutes <= 100
            {
                seconds = Array(minutes...100)
            }
            else
            {   seconds = Array(minutes...minutes + 100)
                print("Invalid number")
            }
            
            unitPicker.reloadAllComponents()
            
            
            numberOfVariant = numberOfVariant + 1
        
            pProductVariant.reloadData()
            pPrice.text = ""
            Discount = "-1"
            unitClicked.text = ""
        }
        UIView.transition(with: addVariantButtonView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                
                self.addVariantButtonView.transform = CGAffineTransform(translationX: 0, y: -100)
                self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1100)
                self.addVariantButtonView.isHidden = false
        }, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    @IBAction func pickerDataSelected(_ sender: Any) {
        pickerViewContainer.isHidden = true
        blurView.alpha = 0
        
        print(selectedunits)
        
    }
    
    @IBAction func cancelPicker(_ sender: Any) {
        pickerViewContainer.isHidden = true
        blurView.alpha = 0
    }
    
    ///category data
    func readLocalJsonFile() {
        
        if let jsonPath: String = Bundle.main.path(forResource: "category", ofType: "json"), let jsonData: Data = NSData(contentsOfFile: jsonPath) as? Data {
            do {
                let json: AnyObject = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as AnyObject
//                print(json)
                if let dict1 = json as? Dictionary<String, AnyObject>{
//                    print(dict1)
                    let innerdict = dict1["data"] as! NSDictionary
                    
//                    print("inner dict", innerdict)
                    let data = innerdict
//                    print(" json", data)
                    
                    
//                    print("data value", data["array_to_json"] as! [[String:Any]])
                    var finaldata = data["array_to_json"] as! [[String:Any]]
                    for var k in 0..<finaldata.count
                    {
//                    print("data value", finaldata[0]["category"]!)
                    var subcategory = finaldata[k]["category"] as! [[String: Any]]
//                    print("sub category data", subcategory[0]["sub_category"]!)
                    for var j in 0..<subcategory.count
                    {
                    var subcategory_data = subcategory[j]["sub_category"] as! [NSDictionary]
                    for var i in 0..<subcategory_data.count
                    {
                        print(subcategory_data[i]["sub_category_name"]!)
                        print(subcategory_data[i]["sub_categoryid"]!)
                        self.categoryName.append(subcategory_data[i]["sub_category_name"]! as! String)
                        self.categoryID.append(subcategory_data[i]["sub_categoryid"]! as! Int)
                        self.subCategoryID.append(subcategory_data[i]["categoryid"]! as! Int)
                    }
                    // do whatever you want with the content of your json file
                }
                }
                }
            } catch {
                print("Error while deserialization of jsonData")
            }
        }
        
    }
}
extension AllSellVariantViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //        texts[textField.tag] = textField.text
        
        print(textField.tag)
        print("data",textField.text)
        
        
                }
    func AlertAddVariant()
    {
        let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let Color = UIAlertAction(title: "Select Color", style: .default) { (alert: UIAlertAction) in
            print(" Color Selected")
            self.native.set(0, forKey: "selectVariantList")
            self.native.set(self.pSku.text!, forKey: "Psku")
            self.native.synchronize()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "createVariant"))! as UIViewController
            //
            self.present(editPage, animated: false, completion: nil)
            
            }
        
        let Size = UIAlertAction(title: "Select Size", style: .default) {
            (alert: UIAlertAction) in
            self.native.set(1, forKey: "selectVariantList")
            self.native.synchronize()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "createVariant"))! as UIViewController
            //
            self.present(editPage, animated: false, completion: nil)
            print(" Size Selected")
        }
        let material = UIAlertAction(title: "Select Material", style: .default) {
            (alert: UIAlertAction) in
            self.native.set(2, forKey: "selectVariantList")
            self.native.synchronize()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "createVariant"))! as UIViewController
            //
            self.present(editPage, animated: false, completion: nil)
            print(" Material Selected")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
            print("Cancelled")
        }
        
        actionMenu.addAction(Color)
        actionMenu.addAction(Size)
        actionMenu.addAction(material)
        actionMenu.addAction(cancelAction)
        
        self.present(actionMenu, animated: true, completion: nil)
    }
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        selectindexforediting = textField.tag
//        pickerViewData()
        return true
    }
    
    
}

