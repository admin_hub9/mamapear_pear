//
//  ProductMainViewController.swift
//  Hub9
//
//  Created by Deepak on 5/9/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import RangeSeekSlider
import AttributedTextView

class ProductMainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource{
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var returnImage:UIImage = UIImage()
    let native = UserDefaults.standard
    var Product = [AnyObject]()
    var ProductName = [AnyObject]()
    var shopName = [AnyObject]()
    var shopaddress = [AnyObject]()
    var ProductPrice = [Double]()
    var RetailPrice = [Double]()
    var ProductTag = [String]()
    var productSaveTag = [AnyObject]()
    var ProductMOQ = [AnyObject]()
    var ProductImg = [AnyObject]()
    var VariantID = [AnyObject]()
    var featureTag = [String]()
    var Img = [AnyObject]()
    let jsonObject: NSMutableDictionary = NSMutableDictionary()
    var type = ""
    var count = 0
    var isSearching = false
    var filterclick = 0
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var Pfilterdata = ["Categories","Brand","Sort By"]
    var limit = 20
    var offset = 0
    var cellType = 1
    var Category = [AnyObject]()
    var brand = [AnyObject]()
    var selectedCategory = [Int]()
    var selectedbrand = [Int]()
    var sort = ["asc", "desc"]
    var minPrice = 0
    var maxPrice = 5000
    var selectedSort = "asc"
    var number = false
    var Currency = ""
    var user_purchase_modeid = "1"
    
    
    
    
    
    @IBOutlet weak var productCollection: UICollectionView!
    
    @IBOutlet weak var searchText: UILabel!
    @IBOutlet var filterView: UIView!
    @IBOutlet var ParentFilter: UITableView!
    @IBOutlet var ChildFilter: UITableView!
    @IBOutlet var CancelFilter: UIButton!
    @IBOutlet var ApplyFilter: UIButton!
    @IBOutlet weak var cellStyle: UIButton!
    @IBOutlet weak var filter: UIButton!
    @IBOutlet weak var rangeSlider: RangeSeekSlider!
    
    @IBOutlet weak var collectionTopConstraint: NSLayoutConstraint!
    
    //    @IBOutlet var filterButton: UIBarButtonItem!
    @IBOutlet weak var navBarImage: UIImageView!
    
    
    @IBAction func searchButton(_ sender: Any) {
        //        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "searchSugg"))! as UIViewController
        //        self.present(editPage, animated: false, completion: nil)
    }
    
    
    
    @IBAction func BackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func CancelFilter(_ sender: Any) {
        
    }
    
    @IBAction func cellStyleButton(_ sender: Any) {
        if cellType == 0
        {
            productCollection.reloadData()
            cellType = 1
            cellStyle.setImage(UIImage(named:"GrideView"), for: .normal)
        }
        else
        {
            productCollection.reloadData()
            cellType = 0
            cellStyle.setImage(UIImage(named:"listview"), for: .normal)
            
        }
    }
    
    @IBAction func filterButton(_ sender: Any) {
        UIView.transition(with: filterView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.filterView.transform = CGAffineTransform(translationX: 0, y: 0)
                self.number = true
                self.filterView.isHidden=false
                //                self.searchbar.isHidden=false
        }, completion: nil)
    }
    
    @IBAction func cancelFilter(_ sender: Any) {
        UIView.transition(with: filterView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.filterView.transform = CGAffineTransform(translationX: 0, y: 380)
                self.number = true
                self.filterView.isHidden=true
                //                self.searchbar.isHidden=false
        }, completion: nil)
    }
    
    
    
    
    
    
    @IBAction func applyFilter(_ sender: Any) {
        native.set("product_elastic", forKey: "searchType")
        native.synchronize()
        self.offset = 0
        Product.removeAll()
        ProductName.removeAll()
        shopName.removeAll()
        shopaddress.removeAll()
        ProductPrice.removeAll()
        ProductMOQ.removeAll()
        ProductImg.removeAll()
        RetailPrice.removeAll()
        ProductTag.removeAll()
        productSaveTag.removeAll()
        VariantID.removeAll()
        Img.removeAll()
        self.parseData(limit: limit, offset: offset)
        UIView.transition(with: filterView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.filterView.transform = CGAffineTransform(translationX: 0, y: 380)
                
                self.filterView.isHidden=true
                //                self.searchbar.isHidden=false
        }, completion: nil)
    }
    
    
    
    
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if number == true
        {
            UIView.transition(with: filterView, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0
                    self.filterView.transform = CGAffineTransform(translationX: 0, y: 380)
                    
                    self.filterView.isHidden=true
                    self.number = false
                    //                self.searchbar.isHidden=false
            }, completion: nil)
        }
        else
        {
            //            dismiss(animated: true, completion: nil)
        }
        //   quantity.resignFirstResponder()
        //        changedPrice.resignFirstResponder()
        //        remarks.resignFirstResponder()
    }
    
    //Scroll view delegate method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 10{
            //scrolling down
            self.navBarImage.isHidden=true
            self.collectionTopConstraint.constant = -121
        }
        else{
            //scrolling up
            self.navBarImage.isHidden=false
            self.collectionTopConstraint.constant = 2
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        CustomLoader.instance.showLoaderView()
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        if let savedValue = native.string(forKey: "bannerImage"){
            debugPrint("Here you will get saved value")
            var image = (native.string(forKey: "bannerImage") as? String)!
            
            image = image.replacingOccurrences(of: " ", with: "%20")
            let url = NSURL(string: image)
            if url != nil{
                self.navBarImage.isHidden=false
                self.collectionTopConstraint.constant=2
                DispatchQueue.main.async {
                    self.navBarImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                }
            }
            else
            {
                self.navBarImage.isHidden=true
                self.collectionTopConstraint.constant = -121
            }
        } else {
            
            self.navBarImage.isHidden=true
            self.collectionTopConstraint.constant = -121
            debugPrint("No value in Userdefault,Either you can save value here or perform other operation")
            
        }
        
        view.endEditing(true)
        view.resignFirstResponder()
        resignFirstResponder()
        rangeSlider.delegate = self as! RangeSeekSliderDelegate
        productCollection.dataSource = self
        productCollection.delegate = self
        Product.removeAll()
        ProductName.removeAll()
        shopName.removeAll()
        shopaddress.removeAll()
        ProductPrice.removeAll()
        RetailPrice.removeAll()
        ProductTag.removeAll()
        productSaveTag.removeAll()
        ProductMOQ.removeAll()
        ProductImg.removeAll()
        VariantID.removeAll()
        if native.string(forKey: "searchType") as? String == "category_elastic"
        {
            if let searchData = native.string(forKey: "search") as? String
            {
                var search = native.string(forKey: "search")!
                searchText.text = search
            }
            selectedCategory.append(selectdashboardSubCategoryViewController.subcategoryid)
        }
        else if native.string(forKey: "searchType") as? String == "parent_category_elastic"
        {
            
            if let searchData = native.string(forKey: "search") as? String
            {
                var search = native.string(forKey: "search")!
                searchText.text = search
            }
            selectedCategory.append(selectdashboardSubCategoryViewController.subcategoryid)
        }
        else if native.string(forKey: "searchType") as? String == "product_elastic"
        {
            if (native.object(forKey: "search") != nil)
            {
                
                searchText.text = native.string(forKey: "search")!
            }
        }
        else
        {
            var search = native.string(forKey: "search")!
            searchText.text = search
        }
        parseData(limit: limit, offset: offset)
        
        productCollection.isHidden = true
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        
        self.ParentFilter.separatorStyle = .none
        self.ChildFilter.separatorStyle = .none
        self.brand.removeAll()
        self.Category.removeAll()
        self.ParentFilter.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        self.filterView.isHidden = true
        UIView.transition(with: filterView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.filterView.transform = CGAffineTransform(translationX: 0, y: 380)
                
                self.filterView.isHidden=true
                //                self.searchbar.isHidden=false
        }, completion: nil)
        // Do any additional setup after loading the view.
        self.filterData()
    }
    
    func parseData(limit: Int, offset: Int)
    {
        var text = ""
        
        if (native.object(forKey: "search") != nil)
        {
            text = native.string(forKey: "search")!
        }
        let b2burl = native.string(forKey: "b2burl")!
        
        var validateUrl = ""
        if native.string(forKey: "searchType") as? String == "product_elastic"
        {
            
            validateUrl = "\(b2burl)/search/search/?search_type=product_elastic&search_query=\(text)&limit=\(limit)&offset=\(offset)&brand=\(selectedbrand)&category=\(selectedCategory)&min_price=\(minPrice)&max_price=\(maxPrice)&sort=\(selectedSort)"
            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        }
        else if native.string(forKey: "searchType") as? String == "category_elastic"
            
        {
            
            
            validateUrl = "\(b2burl)/search/search/?search_type=category_elastic&search_query=\(text)&limit=\(limit)&offset=\(offset)&category=\(selectedCategory)"
        }
        else if native.string(forKey: "searchType") as? String == "parent_category_elastic"
            
        {
            
            
            validateUrl = "\(b2burl)/search/search/?search_type=parent_category_elastic&search_query=\(text)&limit=\(limit)&offset=\(offset)&parent_category=\(selectedCategory)"
        }
        else if native.string(forKey: "searchType") as? String == "icon_category_elastic"
            
        {
            
            
            var product_collectionid = native.string(forKey: "product_collectionid")!
            validateUrl = "\(b2burl)/product/get_product_collection_data/?product_collectionid=\(product_collectionid)&limit=\(limit)&offset=\(offset)"
        }
        
        
        
        debugPrint("/////", validateUrl)
        var urlString = validateUrl.replacingOccurrences(of: " ", with: "%20")
        var a = URLRequest(url: NSURL(string: urlString) as! URL)
        let token = self.native.string(forKey: "Token")!
        if token != "" 
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            //                                debugdebugPrint(response)
            debugPrint("response: \(response)")
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    //                        debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    //                        debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                        
                    else if dict1["status"] as! String == "failure"
                    {
                        
                        let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        //                        self.dismiss(animated: true, completion: nil)
                        self.present(alert, animated: true, completion: nil)
                    }
                        
                    else if dict1["status"] as! String == "success"
                    {
                        debugPrint("search data", dict1["data"])
                        if let innerdict = dict1["data"] as? [AnyObject]
                        {
                            if innerdict.count > 0
                            {
                                for i in 0..<innerdict.count {
                                    //                        debugPrint(innerdict[i]["name_json"] as! NSDictionary, "name")
                                    let data = innerdict[i]["name_json"] as! AnyObject
                                    let name = innerdict[i]["title"]! as! String
                                    //                        debugPrint(name)
                                    
                                    
                                    //                        let shopname = innerdict[i]["shop_name"] as? String
                                    let statename = innerdict[i]["state_name"] as? String
                                    if innerdict[i]["feature_name"] is NSNull
                                    {
                                        self.featureTag.append("")
                                    }
                                    else
                                    {
                                        self.featureTag.append(innerdict[i]["feature_name"] as! String)
                                    }
                                    if let product_variant_data = innerdict[i]["product_variant_data"] as? [AnyObject]
                                    {
                                        //                        for j in 0..<product_variant_data.count
                                        if product_variant_data.count > 0
                                        {
                                            if let product_variant_size_data = product_variant_data[0]["product_variant_size_data"] as? [AnyObject]
                                            {
                                                if product_variant_size_data.count > 0
                                                {
                                                    
                                                    if innerdict[i]["product_price_tag"] is NSNull
                                                    {
                                                        self.ProductTag.append("")
                                                    }
                                                    else
                                                    {
                                                        self.ProductTag.append((innerdict[i]["product_price_tag"] as? String)!)
                                                    }
                                                    if product_variant_size_data[0]["percentage_off"] is NSNull
                                                    {
                                                        self.productSaveTag.append("" as AnyObject)
                                                    }
                                                    else
                                                    {
                                                        self.productSaveTag.append((product_variant_size_data[0]["percentage_off"] as? AnyObject)!)
                                                    }
                                                    if product_variant_size_data[0]["currency_symbol"] is NSNull
                                                    {}
                                                    else
                                                    {
                                                        let myInteger = product_variant_size_data[0]["currency_symbol"] as! String
                                                        //                                if let myUnicodeScalar = UnicodeScalar(myInteger) {
                                                        //                                    let myString = String(myUnicodeScalar)
                                                        self.Currency = "\(myInteger)"
                                                        //                                }
                                                    }
                                                    self.ProductName.append(name as AnyObject)
                                                    let imagedir = (product_variant_size_data[0]["variant_image_url"] as? AnyObject)!
                                                    self.ProductImg.append((imagedir["200"] as? AnyObject)!)
                                                    self.ProductMOQ.append((product_variant_size_data[0]["min_order_qty"] as? AnyObject)!)
                                                    if product_variant_size_data[0]["retail_price"] is NSNull
                                                    {
                                                        self.RetailPrice.append(Double(0.0))
                                                    }
                                                    else
                                                    {
                                                        self.RetailPrice.append(Double((product_variant_size_data[0]["retail_price"] as? NSNumber)!))
                                                    }
                                                    if self.user_purchase_modeid == "1"
                                                    {
                                                        if product_variant_size_data[0]["deal_price"] != nil
                                                        {
                                                            if product_variant_size_data[0]["deal_price"] is NSNull
                                                            {
                                                                if product_variant_size_data[0]["selling_price"] is NSNull
                                                                {}
                                                                else
                                                                {
                                                                    self.ProductPrice.append(Double((product_variant_size_data[0]["selling_price"] as? NSNumber)!))
                                                                }
                                                            }
                                                            else{
                                                                if let price = product_variant_size_data[0]["deal_price"] as? NSNumber!
                                                                {
                                                                    if (price != nil){
                                                                        self.ProductPrice.append(Double(price!))                                                                }
                                                                    else
                                                                    {
                                                                        if product_variant_size_data[0]["group_price"] is NSNull
                                                                        {}
                                                                        else
                                                                        { if let price = product_variant_size_data[0]["group_price"] as? NSNumber!
                                                                        {
                                                                            self.ProductPrice.append(Double((product_variant_size_data[0]["group_price"] as? NSNumber)!))
                                                                            
                                                                            }}}}}
                                                        } }
                                                    else
                                                    {
                                                        if product_variant_size_data[0]["bulk_price"] is NSNull
                                                        {}
                                                        else
                                                        {
                                                            self.ProductPrice.append(Double((product_variant_size_data[0]["bulk_price"] as? NSNumber)!))
                                                        }}
                                                    self.VariantID.append((product_variant_size_data[0]["product_variantid"] as? AnyObject)!)
                                                    //                                self.shopName.append((shopname as? AnyObject)!)
                                                    self.shopaddress.append((statename as? AnyObject)!)
                                                    
                                                }
                                            }}
                                    }
                                }
                            }
                        }
                        //                        self.filterData()
                    }
                    
                    self.productCollection.isHidden = false
                    self.productCollection.reloadData()
                    
                }
            }}
        //        }
        
    }
    
    func filterData()
    {
        
        var text = ""
        if native.string(forKey: "searchType") as? String == "product_elastic"
        {
            text = searchBarViewController.searchText
        }
        else if native.string(forKey: "searchType") as? String == "category_elastic"
        {
            text  = "clothing"
        }
        else if native.string(forKey: "searchType") as? String == "parent_category_elastic"
        {
            text  = "clothing"
        }
        
        //            debugPrint(Token)
        debugPrint(limit, offset)
        let b2burl = native.string(forKey: "b2burl")!
        
        var validateUrl = ""
        
        validateUrl = "\(b2burl)/search/search/?search_type=product_filter_elastic&search_query=\(text)&limit=\(limit)&offset=\(offset)&brand=\(selectedbrand)&category=\(selectedCategory)&min_price=\(minPrice)&max_price=\(maxPrice)&sort=\(selectedSort)"
        validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        
        debugPrint("/////", validateUrl)
        var a = URLRequest(url: NSURL(string:validateUrl)! as URL)
        a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            //                                debugdebugPrint(response)
            debugPrint("response: \(response)")
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as! String == "failure"
                    {
                        
                        let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        //                        self.dismiss(animated: true, completion: nil)
                        self.present(alert, animated: true, completion: nil)
                    }
                        
                    else{
                        
                        let innerdict = dict1["data"] as! [AnyObject]
                        debugPrint(innerdict)
                        if innerdict.count > 1
                        {
                            if let brand = innerdict[0]["brand"] as? [AnyObject]
                            {
                                self.brand = brand
                            }
                            if let category = innerdict[1]["category"] as? [AnyObject]
                            {
                                self.Category = category
                            }
                        }
                    }
                    self.ChildFilter.reloadData()
                }
            }
        }
        //        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleCells: [IndexPath] = productCollection.indexPathsForVisibleItems
        let lastIndexPath = IndexPath(item: (ProductImg.count - 1), section: 0)
        if visibleCells.contains(lastIndexPath) {
            //This means you reached at last of your datasource. and here you can do load more process from server
            debugPrint("last index ", lastIndexPath)
            offset = offset + 10
            parseData(limit: limit, offset: offset)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = 0.0
        var height = 0.0
        if cellType == 0
        {
            width = Double(CGFloat(screenWidth / 2 - 15))
            height = Double(CGFloat(Double(screenHeight-30) / 3 ))
            
        }
        else
        {
            width = Double(CGFloat(screenWidth))
            height = Double(CGFloat(Double(screenHeight-30) / 7))
        }
        return CGSize(width: width, height: height )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.VariantID.count > 0
        {
            
            debugPrint("self.VariantID.count", self.VariantID.count)
            self.productCollection.showEmptyListMessage("")
            return self.VariantID.count
        }
        else {
            self.productCollection.showEmptyListMessage("No Product found!")
            return 0
        }
    }
    
    
    
    
    func returnImageUsingCacheWithURLString(url: NSURL) -> (UIImage) {
        
        // First check if there is an image in the cache
        if let cachedImage = imageCache.object(forKey: url) as? UIImage {
            
            return cachedImage
        }
            
        else {
            // Otherwise download image using the url location in Google Firebase
            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                
                if error != nil {
                    debugPrint(error)
                }
                else {
                    DispatchQueue.global().async {
                        
                        // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                        if let downloadedImage = UIImage(data: data!) {
                            
                            self.imageCache.setObject(downloadedImage, forKey: url)
                            self.returnImage = downloadedImage
                            
                        }
                    }
                }
            }).resume()
            return returnImage
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = UICollectionViewCell()
        if collectionView == productCollection {
            if cellType == 0
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
                
                //            debugPrint("product count \(Product.count)")
                //            cell.product1.layer.cornerRadius = 5.0
                cell.clipsToBounds = true
                cell.layer.cornerRadius = 5
                cell.layer.masksToBounds = true
                
                cell.contentView.layer.cornerRadius = 5
                cell.contentView.layer.borderWidth = 1.0
                
                cell.contentView.layer.borderColor = UIColor.clear.cgColor
                cell.contentView.layer.masksToBounds = true
                
                cell.layer.shadowColor = UIColor.lightGray.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 2.0
                cell.layer.shadowOpacity = 1.0
                cell.layer.masksToBounds = false
                cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
                cell.productImage.image = nil
                if let image1 = self.ProductImg[indexPath.row] as? [AnyObject]
                {
                    if image1.count > 0
                    {
                        var image = image1[image1.count - 1] as! String
                        image = image.replacingOccurrences(of: " ", with: "%20")
                        let url = NSURL(string: image)
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                    
                }
                cell.productName.text = (self.ProductName[indexPath.row] as? String)?.capitalized
                let price = self.ProductPrice[indexPath.row ]
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if native.string(forKey: "Token")! == ""
                {
                    cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                }
                else
                {
                    cell.price.text = String(describing: "\(self.Currency) \(price)")
                }
                cell.productTag.layer.cornerRadius = 3
                cell.productTag.clipsToBounds = true
                if self.featureTag[indexPath.row] as! String != "Default"
                {
                    cell.productTag.text = " \(self.featureTag[indexPath.row] as! String) "
                }
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "multiproductswitch", for: indexPath) as! ProductCollectionViewCell
                cell.clipsToBounds = true
                cell.productImage.image = nil
                
                
                if let image1 = self.ProductImg[indexPath.row] as? [AnyObject]
                {
                    if image1.count > 0
                    {
                        var image = image1[image1.count - 1] as! String
                        image = image.replacingOccurrences(of: " ", with: "%20")
                        let url = NSURL(string: image)
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                cell.productName.text = (self.ProductName[indexPath.row] as? String)?.capitalized
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let redColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if self.user_purchase_modeid == "1"
                {
                    cell.productSaveTag.isHidden = false
                }
                else
                {
                    cell.productSaveTag.isHidden = true
                }
                if native.string(forKey: "Token")! == ""
                {
                    cell.price.attributedText = ("Sign In ".color(textcolor).size(11) + "to Unlock Wholesale Price".color(lightBlackColor).size(11)).attributedText
                }
                else
                {
                    cell.price.attributedText = ("\(self.Currency)\(String(format:"%.2f", ProductPrice[indexPath.row]))\(self.ProductTag[indexPath.row]) " .color(textcolor).size(20).color(textcolor).size(12) + "\(self.Currency)\(String(format:"%.2f", (RetailPrice[indexPath.row]))) ".color(lightGrayColor).strikethrough(1).size(12)).attributedText
                }
                cell.productSaveTag.text = "Save \(self.productSaveTag[indexPath.row])% vs retail"
                cell.productTag.layer.cornerRadius = 3
                cell.productTag.clipsToBounds = true
                if self.featureTag[indexPath.row] as! String != "Default"
                {
                    cell.productTag.text = " \(self.featureTag[indexPath.row] as! String) "
                }
                return cell
            }
            
        }
        count = 0
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        native.set(VariantID[indexPath.row] as! Int, forKey: "FavProID")
        native.set("product", forKey: "comefrom")
        native.synchronize()
        //        currntProID = self.ProductID[indexPath.row] as! Int
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == ParentFilter
        {
            filterclick = indexPath.row
            
        }
        else if tableView == ChildFilter
        {
            if filterclick == 0
            {
                self.selectedCategory.removeAll()
                var item = self.Category[indexPath.row]["categoryid"] as! Int
                if let index = self.selectedCategory.index(of: item) {
                    self.selectedCategory.remove(at: index)
                }
                else
                {
                    self.selectedCategory.append(item)
                }
            }
            else if filterclick == 1
            {
                var item = self.brand[indexPath.row]["brandid"] as! Int
                if let index = self.selectedbrand.index(of: item) {
                    self.selectedbrand.remove(at: index)
                }
                else
                {
                    self.selectedbrand.append(item)
                }
            }
            else if filterclick == 2
            {
                debugPrint(indexPath.row)
                self.selectedSort = sort[indexPath.row]
            }
            
        }
        //        debugPrint(brand, self.selectedbrand)
        //        debugPrint(Category, self.selectedCategory)
        self.ChildFilter.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == ParentFilter
        {
            return 44.5
        }
        return 44.5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ParentFilter
        {
            return Pfilterdata.count
            
            
        }
        else if tableView == ChildFilter
        {
            
            if filterclick == 0
            {
                return self.Category.count
            }
            else if filterclick == 1
            {
                return self.brand.count
            }
            else if filterclick == 2
            {
                return self.sort.count
                
            }
        }
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReturnCell = UITableViewCell()
        if tableView == ParentFilter
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "filter", for: indexPath) as! FilterTableViewCell
            cell.name.text = Pfilterdata[indexPath.row]
            ReturnCell = cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "subfilter", for: indexPath) as! SubFilterTableViewCell
            if filterclick == 0
            {
                cell.name.text = self.Category[indexPath.row]["category_name"] as! String
                var item = self.Category[indexPath.row]["categoryid"] as! Int
                if let index = self.selectedCategory.index(of: item) {
                    cell.mark.image = UIImage(named: "checkmark")
                }
                else
                {
                    cell.mark.image = UIImage(named: "Uncheckmark")
                }
            }
            else if filterclick == 1
            {
                cell.name.text = self.brand[indexPath.row]["brand_name"] as! String
                var item = self.brand[indexPath.row]["brandid"] as! Int
                if let index = self.selectedbrand.index(of: item) {
                    cell.mark.image = UIImage(named: "checkmark")
                }
                else
                {
                    cell.mark.image = UIImage(named: "Uncheckmark")
                }
            }
            else if filterclick == 2
            {
                cell.name.text = self.sort[indexPath.row]
                if self.sort[indexPath.row] == selectedSort
                {
                    cell.mark.image = UIImage(named: "checkmark")
                }
                else
                {
                    cell.mark.image = UIImage(named: "Uncheckmark")
                }
            }
            
            cell.name.frame.size.width = 40
            ReturnCell = cell
            
            
        }
        return ReturnCell
    }
    
    
    
    
    
}



// MARK: - RangeSeekSliderDelegate
extension ProductMainViewController: RangeSeekSliderDelegate {
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === rangeSlider {
            //            debugPrint("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
            self.minPrice = Int(minValue)
            self.maxPrice = Int(maxValue)
        }
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        debugPrint("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        debugPrint("did end touches")
    }
}


