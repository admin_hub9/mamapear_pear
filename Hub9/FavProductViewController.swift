//
//  FavProductViewController.swift
//  Hub9
//
//  Created by Deepak on 7/3/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AttributedTextView

class FavProductViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let native = UserDefaults.standard
    var ProductID = [AnyObject]()
    var currntProID = -1
    var ProductImg = [AnyObject]()
    var favouriteIndex = 0
    var Img = [AnyObject]()
    var user_purchase_modeid = "1"
    
    var deletePlanetIndexPath: NSIndexPath? = nil
    
    @IBOutlet var favProductTable: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        favProductTable.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        ProductID.removeAll()
        ProductImg.removeAll()
        Img.removeAll()
        self.favProductTable.isHidden = true
        self.emptyView.isHidden = true
        CustomLoader.instance.showLoaderView()
        parseData()
    }
    
    
    
    func parseData()
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/get_fav_product/?limit=20&offset=0&all_product=true"
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            debugPrint(a)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                debugPrint("response: \(response)")
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if let innerdict = dict1["data"]{
                            self.ProductImg = innerdict as! [AnyObject]
                            for var i in 0..<self.ProductImg.count {
                                let name = self.ProductImg[i]["title"]
                                let image = self.ProductImg[i]["parent_product_image_url"]!
                                let id = self.ProductImg[i]["productid"]
                                self.ProductID.insert(id as AnyObject, at: i)
                                self.Img.insert(image as! AnyObject, at: i)
                            }
                        }
                    }
                    self.favProductTable.tableFooterView?.isHidden = true
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if self.ProductID.count>0
                    {
                        self.emptyView.isHidden = true
                        self.favProductTable.isHidden = false
                        self.favProductTable.reloadData()
                    }
                    else
                    {
                        self.favProductTable.isHidden = true
                        self.emptyView.isHidden = false
                    }
                    
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ProductImg.count > 0
        {
            favProductTable.isHidden = false
            
        }
        else
        {
            favProductTable.isHidden = true
            
        }
        
        return self.ProductImg.count
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favproducttable") as! FavProductTableViewCell
        if self.ProductImg.count>indexPath.row
        {
            let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
            let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
            cell.productname.text = self.ProductImg[indexPath.row]["title"] as? String
            let allimage = self.ProductImg[indexPath.row]["image_url"] as! AnyObject
            var currency = ""
            if self.user_purchase_modeid == "1"
            {
                cell.retailText.isHidden = false
            }
            else
            {
                cell.retailText.isHidden = true
            }
            if self.ProductImg[indexPath.row]["currency_symbol"] is NSNull
            {}
            else
            {
                currency = self.ProductImg[indexPath.row]["currency_symbol"] as! String
            }
            if self.ProductImg[indexPath.row]["feature_name"] is NSNull
            {
                cell.productTag.text = ""
            }
            else
            {
                if self.ProductImg[indexPath.row]["feature_name"] as! String != "Default"
                {
                    cell.productTag.text = " \(self.ProductImg[indexPath.row]["feature_name"] as! String) "
                    cell.productTag.isHidden = false
                }
                else
                {
                    cell.productTag.isHidden = true
                }
            }
            if self.ProductImg[indexPath.row]["product_variant_data"] is NSNull
            {}
            else
            {
                let price = self.ProductImg[indexPath.row]["product_variant_data"] as! AnyObject
                var product_price_tag = ""
                var actualprice = 0.0
                var retailsPrice = 0.0
                if price["retail_price"] is NSNull
                {}
                else
                {
                    retailsPrice = Double(Float((price["retail_price"] as? NSNumber)!))
                }
                if self.ProductImg[indexPath.row]["product_price_tag"] is NSNull
                {}
                else
                {
                    product_price_tag = self.ProductImg[indexPath.row]["product_price_tag"] as! String
                }
                if self.user_purchase_modeid == "1"
                {
                    if price["deal_price"] != nil
                    {
                        if price["deal_price"] is NSNull
                        {
                            if price["selling_price"] is NSNull
                            {}
                            else
                            {
                                actualprice = Double(Float((price["selling_price"] as? NSNumber)!))
                            }
                        }
                        else{
                            if let price = price["deal_price"] as? NSNumber!
                            {
                                if (price != nil){
                                    actualprice = Double(Float(price!))
                                }}}
                    } }
                else
                {
                    if price["bulk_price"] is NSNull
                    {}
                    else
                    {
                        actualprice = Double(Float((price["bulk_price"] as? NSNumber)!))
                    }}
                
                
                
                if native.string(forKey: "Token")! == ""
                {
                    cell.price.attributedText = ("Sign In ".color(textcolor).size(11) + "to Unlock Wholesale Price".color(lightBlackColor).size(11)).attributedText
                }
                else
                {
                    if price["percentage_off"] is NSNull
                    {}
                    else
                    {
                        cell.retailText.text = "Save \((price["percentage_off"] as? NSNumber)!)% vs retail"
                    }
                    
                    
                    cell.price.attributedText = ("\(currency)\(String(format: "%.2f",actualprice))\(product_price_tag) ".color(textcolor) +  "\(currency)\(String(format: "%.2f",retailsPrice))".color(graycolor).strikethrough(1).size(11)).attributedText
                }
                
            }
            
            let image = allimage["200"] as! [AnyObject]
            if image.count > 0 {
                var imageUrl = image[0] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: imageUrl)
                if url != nil
                {
                    
                    cell.productImage.image = nil
                    DispatchQueue.main.async {
                       cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                   }
                }
            }
        }
        return cell
        
        let cell1 : UITableViewCell = UITableViewCell()
        return cell1
    }
    func currentId()-> Int
    {
        return currntProID
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        debugPrint(self.ProductID)
        if self.ProductID.count>indexPath.row
        {
            native.set(self.ProductID[indexPath.row] as! Int, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        let lastSectionIndex = tableView.numberOfSections - 1
    //        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
    //        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
    //            // debugPrint("this is the last cell")
    //            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    //            spinner.startAnimating()
    //            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
    //
    //            self.favProductTable.tableFooterView = spinner
    //            self.favProductTable.tableFooterView?.isHidden = false
    //        }
    //    }
    
    //
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.favourite(index: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    
    func favourite(index: Int) {
        //        self.blurView.alpha = 1
        //        self.activity.startAnimating()
        
        let token = self.native.string(forKey: "Token")!
        if token != "" && self.ProductID.count > index
        {
            CustomLoader.instance.showLoaderView()
            
            var id = ProductID[index]
            //        debugPrint("product id:", id)
            let parameters: [String:Any] = ["productid":id, "shopid":0]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/unfollow_product/"
            
            debugPrint("unfollow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if let json = response.result.value {
                            let jsonString = "\(json)"
                            debugPrint("res;;;;;", jsonString)
                            self.ProductID.remove(at: index)
                            self.ProductImg.remove(at: index)
                            self.Img.remove(at: index)
                            self.favProductTable.reloadData()
                            
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                        }
                    }}
            }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
}

extension UIScrollView {
    func showEmptyListMessage(_ message:String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.bounds.size.width, height: self.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 15)
        messageLabel.sizeToFit()
        
        if let `self` = self as? UITableView {
            self.backgroundView = messageLabel
            self.separatorStyle = .singleLine
            
        } else if let `self` = self as? UICollectionView {
            self.backgroundView = messageLabel
        }
    }
}
