//
//  VideoAndImageSliderCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/22/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class VideoAndImageSliderCollectionViewCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var pageControl: UIPageControl!
}
