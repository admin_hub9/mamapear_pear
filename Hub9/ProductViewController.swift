//
//  ProductViewController.swift
//  Hub9
//
//  Created by Deepak on 5/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
   var clickcount = 0
    
    @IBOutlet var followIcon: UIBarButtonItem!
    @IBOutlet weak var switchButton: UISegmentedControl!
    @IBOutlet weak var productContainer: UIView!
    @IBOutlet weak var shopContainer: UIView!
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func followButton(_ sender: Any) {
    }
    
    @IBAction func filterClicked(_ sender: Any) {
        clickcount = clickcount + 1
        if clickcount % 2 != 0
        {
        product.hide()
        }
        else
        {
            product.unhide()
        }
    }
    
    @IBAction func SwitchButtom(_ sender: Any) {
        switch switchButton.selectedSegmentIndex {
        case 0:
            shopContainer.isHidden = true
            productContainer.isHidden = false
            
        case 1:
            shopContainer.isHidden = false
            productContainer.isHidden = true
            
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
