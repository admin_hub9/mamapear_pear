//
//  UpdateProductVariantViewController.swift
//  Hub9
//
//  Created by Deepak on 3/2/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import AttributedTextView
import Toast_Swift
import UITextField_Shake_Swift

var updateCart = UpdateProductVariantViewController()

class UpdateProductVariantViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    var native = UserDefaults.standard
    var variantData = [AnyObject]()
    var selectedColorIndex = 0
    var color = [String]()
    var innerVariantData = [[AnyObject]]()
    var variantImage = [String]()
    var quantity = [[Int]]()
    var checkVariantStock = [["variantid": Int(), "moq": Int(), "stock": Int()]]
    var max_quantity = [["variantid": Int(), "single_max": Int(), "bulk_max": Int()]]
    
    
    /// Deal variables
    var data = [[AnyObject]]()
    var Amount = 0.0
    var shopid = 0
    var MCV = 0.0
    var MOQ = [Int]()
    var currentIndex = 0
    var productTitle = [String]()
    var variantID = [Int]()
    var Quantity = [Int]()
    var currency = ""
    var order_typeid = 1
    var cartProduct = [AnyObject]()
    var paymentMethod = [AnyObject]()
    
    
    ///charges data from api
    var cod_charge = 0.0
    var ship_insurance = 0.0
    var shipping_charge = 0.0
    var product_tax_value = 0.0
    var wallet_value = 0.0
    var bb_coin = 0.0
    
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var stock: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var selectColor: UICollectionView!
    @IBOutlet weak var selectVariant: UICollectionView!
    
    @IBOutlet weak var displayVariantTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var addtocart: UIButton!
    @IBOutlet weak var totalCost: UILabel!
    @IBOutlet weak var totalquantity: UILabel!
    
    @IBAction func dismissButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addtoCartButton(_ sender: Any) {
        
        
        
        if BBProductDetails.order_typeid == 3 || BBProductDetails.order_typeid == 1
        {
            var qty = totalquantity.text?.replacingOccurrences(of: "Qty: ", with: "")
            if qty != ""
            {
                if Int(BBProductDetails.ProductMOQ)! <= Int(qty!)!
                {
                    self.addDataCart()
                }
                else
                {
                    self.view.makeToast("This item has MOQ of \(BBProductDetails.ProductMOQ) per buyer!", duration: 2.0, position: .center)
                }}
            else
            {
                self.view.makeToast("This item has MOQ of \(BBProductDetails.ProductMOQ) per buyer!", duration: 2.0, position: .center)
            }
        }
        else
        {
            self.addDataCart()
        }
        
        
        
    }
    
    // add tot cart
    func addDataCart()
    {
        CustomLoader.instance.showLoaderView()
        var data = [["product_variantid": Int(), "quantity": Int()]]
        data.removeAll()
        if let product_variant_data = variantData as? [AnyObject]
        {
            
            for i in 0..<product_variant_data.count
            {
                if let product_variant_size_data = product_variant_data[i]["product_variant_size_data"] as? [AnyObject]
                {
                    for var j in 0..<innerVariantData[i].count
                    {
                        if self.quantity[i][j] != 0
                        {
                            let variantid = innerVariantData[i][j]["product_variantid"] as! Int
                            data.append(["product_variantid": variantid, "quantity": self.quantity[i][j]])
                        }
                        
                    }
                    
                }}}
        if data.count > 0 && BBProductDetails.shopid != nil
        {
            var id = native.string(forKey: "FavProID")
            
            debugPrint("product orderid:", BBProductDetails.order_typeid)
            var order_typeid = BBProductDetails.order_typeid
            let parameters: [String:Any] = ["product_data":data, "shopid":BBProductDetails.shopid, "order_typeid": BBProductDetails.order_typeid]
            let token = self.native.string(forKey: "Token")!
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/order/add_to_cart/"
            
            
            Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                
                guard response.data != nil else { // can check byte count instead
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                if let json = response.result.value {
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                debugPrint(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            debugPrint("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            debugPrint(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            debugPrint("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .center)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                                
                            else if dict1["status"] as! String == "success"
                            {
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                if BBProductDetails.user_purchase_modeid == "1" && BBProductDetails.order_typeid == 2
                                {
                                    self.getDealData()
                                }
                                else if BBProductDetails.order_typeid == 3 ||
                                    BBProductDetails.order_typeid == 1
                                {
                                    BBProductDetails.senddataAddtocart=true
                                    self.dismiss(animated: true, completion: nil)
                                    
                                    BBProductDetails.onBagdeButtonClick()
                                    
                                }
                            }
                            else if dict1["status"] as! String == "failure"{
                                debugPrint("res", dict1)
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                self.view.makeToast(dict1["response"] as? String, duration: 2.0, position: .center)
                                
                                
                            }}
                        
                        
                    }
                    
                }
                
            }
        }
        else
        {
            UIApplication.shared.endIgnoringInteractionEvents()
            CustomLoader.instance.hideLoaderView()
            
            
            self.view.makeToast("Please select quantity", duration: 2.0, position: .center)
        }
    }
    
    
    
    // remove cart
    func delete_product()
    {
        let parameters: [String:Any] = ["parent_productid": 0, "productid": 0]
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        debugPrint("Successfully post",parameters)
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/order/remove_from_cart/"
        
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .center)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as! String == "success"
                    {
                        self.addtoCartButton((Any).self)
                    }
                    else if dict1["status"] as! String == "failure"
                    {
                        self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .center)
                    }
                }
            }
            
        }}
    
    
    func showPrice(cellIndex: Int)
    {
        
    }
    
    func pdDefaultIndex()
    {
        var moq = 0
        var stock = 0
        for var k in 0..<self.color.count
        {
            var varcount = 0
            debugPrint("color count", k)
            let data = innerVariantData[k] as! [AnyObject]
            
            if checkVariantStock.count > k
            {
                for j in 0..<data.count
                {
                    
                    let currentid = data[j]["product_variantid"] as! Int
                    
                    for var i in 0..<checkVariantStock.count
                    {
                        if currentid == checkVariantStock[i]["variantid"] as! Int
                        {
                            moq = checkVariantStock[i]["moq"] as! Int
                            stock = checkVariantStock[i]["stock"] as! Int
                            
                            if stock<moq
                            {
                                varcount = varcount + 1
                                break
                            }
                        }
                    }}}
            if varcount > 0
            {
                debugPrint("variant in stock", k, moq , stock)
                self.selectedColorIndex = k
                break
            }}
        if self.selectedColorIndex != 0
        {
            self.selectedColorIndex = 0
            self.selectColor.reloadData()
            self.selectVariant.reloadData()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateCart = self
        debugPrint("order_typeid", BBProductDetails.order_typeid)
        if BBProductDetails.order_typeid == 3 ||
            BBProductDetails.order_typeid == 1
        {
            stock.isHidden = false
            stock.text = "MOQ ≥ \(BBProductDetails.ProductMOQ)"
        }
        else
        {
            stock.isHidden = true
        }
        CustomLoader.instance.showLoaderView()
        containerView.isHidden = true
        addtocart.layer.cornerRadius = addtocart.frame.height / 2
        addtocart.clipsToBounds = true
        variantData = BBProductDetails.ProductVariantData
        productName.text = BBProductDetails.ProductName
        if BBProductDetails.ProductImg.count > 0
        {
            var image = BBProductDetails.ProductImg[0] as! String
            image = image.replacingOccurrences(of: " ", with: "%20")
            let url = NSURL(string: image)
            if url != nil
            {
                
                productImage.layer.cornerRadius = 3
                productImage.clipsToBounds = true
                DispatchQueue.main.async {
                    self.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                }
            }
        }
        //        debugPrint(variantData)
        // Do any additional setup after loading the view.
        fetchVariant()
    }
    
    
    func fetchVariant()
    {
        
        self.color.removeAll()
        self.innerVariantData.removeAll()
        self.quantity.removeAll()
        if let product_variant_data = variantData as? [AnyObject]
        {
            
            for i in 0..<product_variant_data.count
            {
                //                debugPrint("color is:", variantData[i]["color_ui"] as! String)
                if variantData[i]["color_ui"] is NSNull
                {}
                else
                {
                    color.append(variantData[i]["color_ui"] as! String)
                    if let product_variant_size_data = product_variant_data[i]["product_variant_size_data"] as? [AnyObject]
                    {
                        debugPrint("-09p", product_variant_size_data)
                        
                        innerVariantData.insert(product_variant_size_data, at: i)
                        var addzero = [Int]()
                        for var j in 0..<innerVariantData[i].count
                        {
                            addzero.append(0)
                        }
                        self.quantity.insert(addzero, at: i)
                    }
                }
            }
            
            checkStock()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        debugPrint("touch?.view?.tag", touch?.view?.tag)
        if touch?.view?.tag == 100 {
            debugPrint("inside view")
            dismiss(animated: true, completion: nil)
        }
        else
        {
            //            dismiss(animated: true, completion: nil)
            debugPrint("outside view")
        }
        
        
    }
    
    @objc func notifyMe(productVariantID: UIButton)
    {
        let parameters: [String:Any] = ["product_variantid": productVariantID.tag]
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        debugPrint("Successfully post",parameters)
        let b2burl = native.string(forKey: "b2burl")!
        var url = "\(b2burl)/users/stock_notify/"
        
        Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as! String == "success"
                    {
                        self.view.makeToast(dict1["response"] as! String, duration: 2.0, position: .center)
                        debugPrint("notify me", dict1)
                    }
                    else if dict1["status"] as! String == "failure"
                    {
                        self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .center)
                    }
                }
            }
            
        }
    }
    
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //
    //            dismiss(animated: true, completion: nil)
    //    }
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        var cellSize: CGSize = CGSize(width: 0.0, height: 0.0)
    //        let frameSize = collectionView.frame.size
    //        if collectionView == selectColor
    //        {
    //            
    //            cellSize = CGSize(width: (frameSize.width-60)/4, height: 50)
    //        }
    //        else
    //        {
    //            cellSize = CGSize(width: frameSize.width, height: (frameSize.height-15)/3)
    //        }
    //        
    //        return cellSize
    //    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == selectColor
        {
            //            debugPrint(BBProductDetails.ProductImg[indexPath.row] as! String)
            self.productImage.image = nil
            debugPrint( BBProductDetails.ProductImg.count,  BBProductDetails.ProductImg)
            if BBProductDetails.ProductImg.count > indexPath.row
            {
                debugPrint(BBProductDetails.ProductImg[indexPath.row] as! String)
                var image = BBProductDetails.ProductImg[indexPath.row] as! String
                image = image.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: image)
                if url != nil{
                    DispatchQueue.main.async {
                        self.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
                
                
            }
            selectedColorIndex = indexPath.row
            self.selectColor.reloadData()
            self.selectVariant.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == selectColor
        {
            if self.color.count<=1
            {
                self.displayVariantTopConstraint.constant = -55
                self.selectColor.isHidden=true
            }
            else
            {
                self.displayVariantTopConstraint.constant=15
                self.selectColor.isHidden=false
            }
            return self.color.count
        }
        else if innerVariantData.count > 0
        {
            
            return innerVariantData[selectedColorIndex].count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var ReuseCell = UICollectionViewCell()
        let disablecolor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 0.3)
        let grayColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 0.5)
        let blackcolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        if collectionView == selectColor
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectcolor", for: indexPath) as! SelectColorCollectionViewCell
            cell.ColorName.text = (color[indexPath.row] as? String)!
            if selectedColorIndex == indexPath.row
            {
                
                cell.ColorName.textColor = UIColor.red
                cell.indicator.isHidden = false
            }
            else
            {
                cell.ColorName.textColor = UIColor.black
                cell.indicator.isHidden = true
            }
            ReuseCell = cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "updatevariant", for: indexPath) as! UpdateVariantCollectionViewCell
            //            debugPrint("inn", innerVariantData)
            var CurrencySymbol = ""
            let data = innerVariantData[selectedColorIndex] as! [AnyObject]
            let currency = data[indexPath.row]["currency_symbol"] as! String
            //            let myInteger: Int = currency as! Int
            //            if let myUnicodeScalar = UnicodeScalar(myInteger) {
            //                let myString = String(myUnicodeScalar)
            CurrencySymbol = currency
            //                                            self.price.text = "\(myString) \(price)"
            //            }
            var moq = 0
            var stock = 0
            if checkVariantStock.count > indexPath.row
            {
                let currentid = data[indexPath.row]["product_variantid"] as! Int
                
                for var i in 0..<checkVariantStock.count
                {
                    if currentid == checkVariantStock[i]["variantid"] as! Int
                    {
                        moq = checkVariantStock[i]["moq"] as! Int
                        stock = checkVariantStock[i]["stock"] as! Int
                        break
                    }
                }
                
                let origImage = UIImage(named: "minus")
                let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.minusButton.setImage(tintedImage, for: .normal)
                let plus = UIImage(named: "plus")
                let tintedPlusImage = plus?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
                cell.plusButton.setImage(tintedPlusImage, for: .normal)
                cell.notifyMe.tag = currentid
                cell.notifyMe.addTarget(self, action: #selector(notifyMe(productVariantID:)), for: .touchUpInside)
                if stock <= moq
                {
                    cell.plusButton.tintColor = disablecolor
                    cell.minusButton.tintColor = disablecolor
                    cell.quantity.setTitleColor(disablecolor, for: .normal)
                    cell.outofstock.isHidden = false
                    cell.notifyMe.isHidden = false
                    cell.QuantityView.isHidden = true
                    cell.plusButton.isEnabled = false
                    cell.minusButton.isEnabled = false
                    cell.quantity.isEnabled = false
                }
                else
                {
                    cell.plusButton.tintColor = grayColor
                    cell.minusButton.tintColor = grayColor
                    cell.quantity.setTitleColor(blackcolor, for: .normal)
                    cell.outofstock.isHidden = true
                    cell.notifyMe.isHidden = true
                    cell.QuantityView.isHidden = false
                    cell.plusButton.isEnabled = true
                    cell.minusButton.isEnabled = true
                    cell.quantity.isEnabled = true
                }
            }
            if data[indexPath.row]["material_ui"] as! String == "Default" && data[indexPath.row]["size_ui"] as! String == "Default"
            {
                cell.variantName.text = "Default"
            }
            else if data[indexPath.row]["material_ui"] as! String == "Default"
            {
                cell.variantName.text = "\(data[indexPath.row]["size_ui"] as! String)"
            }
            else if data[indexPath.row]["material_ui"] as! String != "Default" && data[indexPath.row]["size_ui"] as! String == "Default"
            {
                cell.variantName.text = "\(data[indexPath.row]["material_ui"] as! String)"
            }
            else if data[indexPath.row]["material_ui"] as! String != "Default" && data[indexPath.row]["size_ui"] as! String != "Default"
            {
                cell.variantName.text = "\(data[indexPath.row]["size_ui"] as! String)/\(data[indexPath.row]["material_ui"] as! String)"
            }
                
            else
            {
                cell.variantName.text = "\(data[indexPath.row]["size_ui"] as! String)"
            }
            cell.quantity.tag = indexPath.row
            if self.quantity[selectedColorIndex][indexPath.row] > 0
            {
                cell.minusButton.tintColor = grayColor
                cell.minusButton.isEnabled = true
            }
            else
            {
                cell.minusButton.tintColor = disablecolor
                cell.minusButton.isEnabled = false
            }
            cell.quantity.setTitle("\(self.quantity[selectedColorIndex][indexPath.row])", for: .normal)
            
            
            cell.minusButton.tag = indexPath.row
            cell.minusButton.addTarget(self, action: #selector(MinusQunatity(sender:)), for: .touchUpInside)
            cell.plusButton.tag = indexPath.row
            cell.plusButton.addTarget(self, action: #selector(PlusQunatity(sender:)), for: .touchUpInside)
            cell.quantity.addTarget(self, action: #selector(BulkUpdate(sender:)), for: .touchUpInside)
            if BBProductDetails.order_typeid == 3
            {
                var dynamic_pricing = data[indexPath.row]["dynamic_pricing"] as! [AnyObject]
                if dynamic_pricing.count > 0
                {
                    for j in 0..<dynamic_pricing.count
                    {       var low = Int((dynamic_pricing[j]["l"] as! NSNumber).floatValue)
                        var high = Int((dynamic_pricing[j]["h"] as! NSNumber).floatValue)
                        if indexPath.row < dynamic_pricing.count-1
                        {
                            //                            debugPrint("T##items: Any...##Any", self.quantity[selectedColorIndex][indexPath.row] as! Int, high)
                            if (self.quantity[selectedColorIndex][indexPath.row] as! Int) <= high && j == 0
                            {
                                cell.variantPrice.text = "\(CurrencySymbol) \(String(format:"%.2f",Float(dynamic_pricing[j]["dp"] as! NSNumber)))/pc"
                            }
                            else if (self.quantity[selectedColorIndex][indexPath.row] as! Int) <= high && j != 0 && self.quantity[selectedColorIndex][indexPath.row] >= low
                            {
                                cell.variantPrice.text = "\(CurrencySymbol) \(String(format:"%.2f",Float(dynamic_pricing[j]["dp"] as! NSNumber)))/pc"
                            } }
                        else
                        {
                            cell.variantPrice.text = "\(CurrencySymbol) \(String(format:"%.2f",Float(dynamic_pricing[0]["dp"] as! NSNumber)))/pc"
                        } } }
                else if data[indexPath.row]["bulk_price"] is NSNull
                {
                    cell.variantPrice.text = "\(CurrencySymbol) \(String(format:"%.2f",Float(data[indexPath.row]["selling_price"] as! NSNumber)))/pc"
                }
                else
                { cell.variantPrice.text = "\(CurrencySymbol) \(String(format:"%.2f",Float(data[indexPath.row]["bulk_price"] as! NSNumber)))/pc"
                } }
            else if BBProductDetails.order_typeid == 1
            {
                cell.variantPrice.text = "\(CurrencySymbol)\(String(format:"%.2f", Float(data[indexPath.row]["selling_price"] as! NSNumber)))/pc"
            }
            else
            {
                if data[indexPath.row]["deal_price"] is NSNull
                {}
                else
                {
                    cell.variantPrice.text = "\(CurrencySymbol)\(String(format:"%.2f", Float(data[indexPath.row]["deal_price"] as! NSNumber)))/pc"
                }}
            
            if self.quantity[selectedColorIndex][indexPath.row] != 0
            {
                let quantity = self.quantity[selectedColorIndex][indexPath.row]
                var price = 0.0
                if BBProductDetails.order_typeid == 3
                {
                    if data[indexPath.row]["bulk_price"] is NSNull
                    {
                        
                    }
                    else
                    {
                        price = Double(data[indexPath.row]["bulk_price"] as! NSNumber)
                        
                    }
                }
                else if BBProductDetails.order_typeid == 1
                {
                    price = Double(data[indexPath.row]["selling_price"] as! NSNumber)
                }
                else
                {
                    if data[indexPath.row]["deal_price"] is NSNull
                    {}
                    else
                    {
                        price = Double((data[indexPath.row]["deal_price"] as! NSNumber))
                        
                    }}
                var totalprice = Double(quantity) * Double(price)
                debugPrint(totalprice)
                
                cell.totalPrice.text = "Sub Total  \(CurrencySymbol)\(String(format: "%.2f", totalprice))"
            }
            else
            {
                cell.totalPrice.text = ""
            }
            ReuseCell = cell
        }
        
        return ReuseCell
    }
    
    
    @objc func MinusQunatity(sender: UIButton)
    {
        
        debugPrint("selectedColorIndex", selectedColorIndex, sender.tag)
        let currentcell = self.innerVariantData[selectedColorIndex] as! [AnyObject]
        let currentid = currentcell[sender.tag]["product_variantid"] as! Int
        var moq = 0
        var stock = 0
        for var i in 0..<checkVariantStock.count
        {
            if currentid == checkVariantStock[i]["variantid"] as! Int
            {
                moq = checkVariantStock[i]["moq"] as! Int
                stock = checkVariantStock[i]["stock"] as! Int
            }
        }
        
        if (0 < self.quantity[selectedColorIndex][sender.tag])
        {
            let val = self.quantity[selectedColorIndex][sender.tag] as! Int
            if BBProductDetails.order_typeid == 1
            {
                if 1 < self.quantity[selectedColorIndex][sender.tag] as! Int
                {
                    quantity[selectedColorIndex][sender.tag] = val - 1
                }
                else
                {
                    quantity[selectedColorIndex][sender.tag] = 0
                }
            }
            else
            {
                
                if 1 < self.quantity[selectedColorIndex][sender.tag] as! Int
                {
                    quantity[selectedColorIndex][sender.tag] = val - 1
                }
                else
                {
                    quantity[selectedColorIndex][sender.tag] = 0
                }
            }
            if BBProductDetails.order_typeid == 3 || BBProductDetails.order_typeid == 1
            {
                var qty = totalquantity.text?.replacingOccurrences(of: "Qty: ", with: "")
                debugPrint(self.totalquantity.text, BBProductDetails.ProductMOQ, Int(qty!)!)
                
            }
            self.totalCostAndPrice(currentIndex: selectedColorIndex, variantIndex: sender.tag)
            selectVariant.reloadData()
        }
        else
        {
            selectVariant.reloadData()
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showInfo("Alert", subTitle: "Number should be in between \(moq) and \(stock)", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
        }
        
        
        
        
    }
    @objc func PlusQunatity(sender: UIButton)
    {
        let currentcell = self.innerVariantData[selectedColorIndex] as! [AnyObject]
        let currentid = currentcell[sender.tag]["product_variantid"] as! Int
        var moq = 0
        var stock = 0
        var singleStock = 0
        var bulkStock = 0
        for var i in 0..<checkVariantStock.count
        {
            if currentid == checkVariantStock[i]["variantid"] as! Int
            {
                moq = checkVariantStock[i]["moq"] as! Int
                stock = checkVariantStock[i]["stock"] as! Int
                singleStock = max_quantity[i]["single_max"] as! Int
                bulkStock = max_quantity[i]["bulk_max"] as! Int
            }
        }
        if BBProductDetails.user_purchase_modeid == "1"
        {
            if (stock > self.quantity[selectedColorIndex][sender.tag]) && (self.quantity[selectedColorIndex][sender.tag]) < singleStock
            {
                let val = self.quantity[selectedColorIndex][sender.tag] as! Int
                if val == 0 && BBProductDetails.order_typeid == 1
                {
                    quantity[selectedColorIndex][sender.tag] = moq
                }
                else if val == 0
                {
                    quantity[selectedColorIndex][sender.tag] = val + 1
                }
                else
                {
                    quantity[selectedColorIndex][sender.tag] = val + 1
                }
            }
            else
            {
                selectVariant.reloadData()
                self.view.makeToast("Number should be in between \(moq) and \(singleStock)", duration: 2.0, position: .center)
                
            }
        }
        else
        {
            if (stock > self.quantity[selectedColorIndex][sender.tag]) && (self.quantity[selectedColorIndex][sender.tag] < bulkStock)
            {
                let val = self.quantity[selectedColorIndex][sender.tag] as! Int
                if val == 0 && BBProductDetails.order_typeid == 1
                {
                    quantity[selectedColorIndex][sender.tag] = moq
                }
                else if val == 0
                {
                    quantity[selectedColorIndex][sender.tag] = moq
                }
                else
                {
                    quantity[selectedColorIndex][sender.tag] = val + 1
                }
            }
                
                
            else
            {
                selectVariant.reloadData()
                self.view.makeToast("Number should be in between \(moq) and \(bulkStock)", duration: 2.0, position: .center)
                
            }
        }
        
        if BBProductDetails.order_typeid == 3 || BBProductDetails.order_typeid == 1
        {
            var qty = totalquantity.text?.replacingOccurrences(of: "Qty: ", with: "")
            
            
        }
        self.totalCostAndPrice(currentIndex: selectedColorIndex, variantIndex: sender.tag)
        selectVariant.reloadData()
        
    }
    
    
    @objc func BulkUpdate(sender: UIButton)
    {
        updateBulkQuantity(currentIndex: sender.tag)
        selectVariant.reloadData()
    }
    
    func totalCostAndPrice(currentIndex: Int, variantIndex: Int)
    {
        var total = 0.0
        var unit = 0
        var currency = ""
        for k in 0..<innerVariantData.count
        {
            let data = innerVariantData[k] as! [AnyObject]
            for l in 0..<data.count
            {
                if data[l]["currency_symbol"]is NSNull
                {}
                else
                {
                    let currencySymbol = data[l]["currency_symbol"] as! String
                    currency = currencySymbol
                }
                if BBProductDetails.order_typeid == 3
                {
                    var dynamic_pricing = data[l]["dynamic_pricing"] as! [AnyObject]
                    if dynamic_pricing.count > 0
                    {
                        for j in 0..<dynamic_pricing.count
                        {
                            var low = Int((dynamic_pricing[j]["l"] as! NSNumber).floatValue)
                            var high = Int((dynamic_pricing[j]["h"] as! NSNumber).floatValue)
                            if variantIndex < dynamic_pricing.count-1
                            {
                                //                    debugPrint("T##items: Any...##Any", self.quantity[k][l] as! Int, high)
                                if (self.quantity[k][l] as! Int) <= high && j == 0
                                {
                                    unit = unit+quantity[k][l]
                                    total = total+Double(dynamic_pricing[j]["dp"] as! NSNumber)*Double(quantity[k][l])
                                }
                                else if (self.quantity[k][l] as! Int) <= high && j != 0 && self.quantity[k][l] >= low
                                {
                                    unit = unit+quantity[k][l]
                                    total = total+Double(dynamic_pricing[j]["dp"] as! NSNumber)*Double(quantity[k][l])
                                } }
                            else
                            {
                                unit = unit+quantity[k][l]
                                total = total+Double(dynamic_pricing[0]["dp"] as! NSNumber)*Double(quantity[k][l])
                            } }// end loop j
                        
                    }
                    else if data[variantIndex]["bulk_price"] is NSNull
                    {
                        unit = unit+quantity[k][l]
                        total = total+Double(data[l]["selling_price"] as! NSNumber)*Double(quantity[k][l])
                    }
                    else
                    {
                        unit = unit+quantity[k][l]
                        total = total+Double(data[l]["bulk_price"] as! NSNumber)*Double(quantity[k][l])
                        
                    } }
                else if BBProductDetails.order_typeid == 1
                {
                    unit = unit+quantity[k][l]
                    total = total+Double(data[l]["selling_price"] as! NSNumber)*Double(quantity[k][l])
                }
                else
                {
                    if data[variantIndex]["deal_price"] is NSNull
                    {}
                    else
                    {
                        if quantity[k][l] != 0
                        {
                            unit = unit+quantity[k][l]
                            total = total+Double(data[l]["deal_price"] as! NSNumber)*Double(quantity[k][l])
                        } }}
                if unit > 0
                {
                    self.totalCost.text = "Total: \(currency)\(String(format: "%.2f", total))"
                    
                    self.totalquantity.text = "Qty: \(unit)"
                }
                else
                {
                    self.totalCost.text = ""
                    self.totalquantity.text = ""
                }
            }
        }
        debugPrint("Int(BBProductDetails.ProductMOQ)! <= unit", Int(BBProductDetails.ProductMOQ)!, unit)
        if Int(BBProductDetails.ProductMOQ)! <= unit
        {
            self.stock.textColor = UIColor.gray
            
        }
        else if Int(BBProductDetails.ProductMOQ)! > unit
        {
            
            self.stock.textColor = UIColor.red
            self.stock.shake(duration: 0.30)
        }
        debugPrint("datatotal", unit, total)
    }
    
    
    @objc func updateBulkQuantity(currentIndex: Int)
    {
        
        let currentcell = self.innerVariantData[selectedColorIndex] as! [AnyObject]
        let currentid = currentcell[currentIndex]["product_variantid"] as! Int
        var moq = 0
        var stock = 0
        var singleStock = 0
        var bulkStock = 0
        for var i in 0..<checkVariantStock.count
        {
            if currentid == checkVariantStock[i]["variantid"] as! Int
            {
                moq = checkVariantStock[i]["moq"] as! Int
                stock = checkVariantStock[i]["stock"] as! Int
                singleStock = max_quantity[i]["single_max"] as! Int
                bulkStock = max_quantity[i]["bulk_max"] as! Int
            }
        }
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert = SCLAlertView(appearance: appearance)
        let txt = alert.addTextField("Enter Quantity")
        alert.addButton("Save", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            if BBProductDetails.user_purchase_modeid == "1"
            {
                if self.checknumber(number: txt.text!) == true && txt.text! != "" && ((txt.text!.count) < 5) && (Int(txt.text!)! <= singleStock)
                {
                    if ((stock >= Int(txt.text!)! ) && Int(txt.text!)! < singleStock && (Int(txt.text!)! >= 1) && BBProductDetails.order_typeid == 1)
                    {
                        let val = Int(txt.text!)!
                        
                        self.quantity[self.selectedColorIndex][currentIndex] = Int(txt.text!)!
                        self.totalCostAndPrice(currentIndex: self.selectedColorIndex, variantIndex: currentIndex)
                        self.selectVariant.reloadData()
                    }
                        
                    else if ((stock >= Int(txt.text!)! ) && Int(txt.text!)! <= singleStock && (Int(txt.text!)! >= 1))
                    {
                        let val = Int(txt.text!)!
                        
                        self.quantity[self.selectedColorIndex][currentIndex] = Int(txt.text!)!
                        self.totalCostAndPrice(currentIndex: self.selectedColorIndex, variantIndex: currentIndex)
                        self.selectVariant.reloadData()
                    }
                    else
                    {
                        alert.dismiss(animated: true, completion: nil)
                        self.view.makeToast("Number should be in between \(moq) and \(singleStock)", duration: 2.0, position: .center)
                    }
                }
                else
                {
                    alert.dismiss(animated: true, completion: nil)
                    self.view.makeToast("Number should be in between \(moq) and \(singleStock)", duration: 2.0, position: .center)
                }
            }
            else
            {
                if self.checknumber(number: txt.text!) == true && txt.text! != "" && ((txt.text!.count) < 5)
                {
                    if ((stock >= Int(txt.text!)! ) && Int(txt.text!)! <= bulkStock && (Int(txt.text!)! >= 1) && BBProductDetails.order_typeid == 1)
                    {
                        let val = Int(txt.text!)!
                        
                        self.quantity[self.selectedColorIndex][currentIndex] = Int(txt.text!)!
                        self.totalCostAndPrice(currentIndex: self.selectedColorIndex, variantIndex: currentIndex)
                        self.selectVariant.reloadData()
                    }
                        
                    else if ((stock >= Int(txt.text!)! ) && Int(txt.text!)! <= bulkStock && (Int(txt.text!)! >= 1))
                    {
                        let val = Int(txt.text!)!
                        
                        self.quantity[self.selectedColorIndex][currentIndex] = Int(txt.text!)!
                        self.totalCostAndPrice(currentIndex: self.selectedColorIndex, variantIndex: currentIndex)
                        self.selectVariant.reloadData()
                    }
                    else
                    {
                        alert.dismiss(animated: true, completion: nil)
                        self.view.makeToast("Number should be in between \(moq) and \(bulkStock)", duration: 2.0, position: .center)
                    }
                }
                else
                {
                    alert.dismiss(animated: true, completion: nil)
                    self.view.makeToast("Number should be in between \(moq) and \(bulkStock)", duration: 2.0, position: .center)
                }
                
                
            }
            
            //                self.inventryBlur.alpha = 0
            
            
        }
        alert.addButton("Cancel", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
            //            self.inventryBlur.alpha = 0
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit("Update Quantity", subTitle: "Please enter valid Quantity value", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
    }
    
    
    func checkStock()
    {
        var id = native.string(forKey: "FavProID")
        
        //        debugPrint("product id:", id)
        //        CustomLoader.instance.showLoaderView()
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        debugPrint("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        var productid = BBProductDetails.productid
        var url = "\(b2burl)/product/check_stock_status/?product_id=\(productid)"
        debugPrint("url", url)
        
        Alamofire.request(url, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            if let result1 = response.result.value
            {
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if dict1["status"] as! String == "success"
                        {
                            
                            self.checkVariantStock.removeAll()
                            self.max_quantity.removeAll()
                            if let data = dict1["data"] as? AnyObject
                            {
                                debugPrint("oppoipoi", data)
                                
                                if let variantstock = data["stock_status"] as? [AnyObject]
                                {
                                    for var i in 0..<variantstock.count
                                    {
                                        var moq = 0
                                        var singleQuantity = 0
                                        var bulkQuantity = 0
                                        var stock = 0
                                        debugPrint(variantstock[i]["product_variantid"] as! Int)
                                        let variantid = variantstock[i]["product_variantid"] as! Int
                                        if variantstock[i]["min_order_qty"] is NSNull
                                        {}
                                        else
                                        {
                                            moq = variantstock[i]["min_order_qty"] as! Int
                                        }
                                        if variantstock[i]["quantity"] is NSNull
                                        {}
                                        else
                                        {
                                            stock = variantstock[i]["quantity"] as! Int
                                        }
                                        if variantstock[i]["bulk_max_quantity"] is NSNull
                                        {}
                                        else
                                        {
                                            bulkQuantity = variantstock[i]["bulk_max_quantity"] as! Int
                                        }
                                        if variantstock[i]["single_max_quantity"] is NSNull
                                        {}
                                        else
                                        {
                                            singleQuantity = variantstock[i]["single_max_quantity"] as! Int
                                        }
                                        
                                        self.checkVariantStock.append(["variantid": variantid, "moq": moq, "stock": stock])
                                        self.max_quantity.append(["variantid": variantid, "single_max": singleQuantity, "bulk_max": bulkQuantity])
                                        
                                        
                                    }
                                }
                            }
                            self.pdDefaultIndex()
                        }
                        else if dict1["status"] as! String == "failure" {
                            self.view.makeToast("Please try after sometime", duration: 2.0, position: .bottom)
                        }
                    }}
                self.productImage.isHidden = false
                self.containerView.isHidden = false
                self.selectColor.reloadData()
                self.selectVariant.reloadData()
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
        
    }
    
    
    
    ///// Deal cart Data
    func Paynow() {
        BBProductDetails.senddataAddtocart = true
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "myCart") as! UIViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
    }
    
    
    func getDealData()
    {
        self.data.removeAll()
        self.MOQ.removeAll()
        CustomLoader.instance.showLoaderView()
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            self.Amount = 0
            //            value1.blurView.alpha = 1
            //            value1.activity.startAnimating()
            let b2burl = native.string(forKey: "b2burl")!
            var a = URLRequest(url: NSURL(string: "\(b2burl)/order/get_deal_cart/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            //            debugPrint("a-a-a-a-a", a)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                debugPrint("deal data", response)
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if dict1["status"] as! String == "failure" {
                            self.view.makeToast("Please try after sometime", duration: 2.0, position: .bottom)
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if let data = dict1["data"] as? [AnyObject]
                            {
                                if let payment_mode_data = dict1["payment_mode_data"] as? [AnyObject]
                                {
                                    self.paymentMethod = payment_mode_data
                                }
                                
                                if data.count > 0
                                {
                                    self.cartProduct = dict1["data"] as! [AnyObject]
                                    for i in 0..<data.count{
                                        if data[i]["order_typeid"] is NSNull
                                        {}
                                        else
                                        {
                                            self.order_typeid = Int(data[i]["order_typeid"] as! NSNumber)
                                        }
                                        
                                    }
                                    if data[0]["is_cod_applicable"] is NSNull
                                    {}
                                    else
                                    {
                                        var is_cod_applicable = data[0]["is_cod_applicable"] as! NSNumber
                                        self.native.set(is_cod_applicable, forKey: "is_cod_applicable")
                                        self.native.synchronize()
                                    }
                                    //
                                    if data[0]["cod_charge"] is NSNull
                                    {}
                                    else
                                    {
                                        self.cod_charge = Double(data[0]["cod_charge"] as! NSNumber)
                                    }
                                    
                                    if data[0]["bb_coin"] is NSNull
                                    {}
                                    else
                                    {
                                        self.bb_coin = Double(data[0]["bb_coin"] as! NSNumber)
                                    }
                                    if data[0]["wallet_value"] is NSNull
                                    {}
                                    else
                                    {
                                        self.wallet_value = (data[0]["wallet_value"] as! NSString).doubleValue
                                    }
                                    if data[0]["order_typeid"] is NSNull
                                    {}
                                    else
                                    {
                                        self.order_typeid = Int(data[0]["order_typeid"] as! NSNumber)
                                    }
                                    if data[0]["ship_insurance"] is NSNull
                                    {}
                                    else
                                    {
                                        self.ship_insurance = Double(data[0]["ship_insurance"] as! NSNumber)
                                    }
                                    //                            if data[0]["shipping_charge"] is NSNull
                                    //                            {}
                                    //                            else
                                    //                            {
                                    //                                self.shipping_charge = Double(data[0]["shipping_charge"] as! NSNumber)
                                    //                            }
                                    if let mcv = (data[0]["mcv"] as? Int)
                                    {
                                        self.MCV = Double(mcv)
                                    }
                                    
                                    
                                    for i in 0..<data.count
                                    {
                                        if let data = data[i]["variant_data"] as? [AnyObject]
                                        {
                                            self.data.append(data)
                                            for each in data {
                                                let temp = each as! NSDictionary
                                                if temp["selling_price"] is NSNull
                                                {}
                                                else
                                                {
                                                    self.Amount = self.Amount + (Double((temp["selling_price"] as! NSString).floatValue) * Double((temp["quantity"] as! NSNumber).floatValue) )
                                                }
                                                if temp["product_tax_value"] is NSNull
                                                {}
                                                else
                                                {
                                                    self.product_tax_value = self.product_tax_value + (Double(temp["product_tax_value"] as! NSNumber)*(Double(temp["quantity"] as! NSNumber)))
                                                }
                                                if temp["currency_symbol"] is NSNull
                                                {}
                                                else
                                                {
                                                    if let currency_symbol = temp["currency_symbol"] as? String
                                                    {
                                                        self.currency =  currency_symbol
                                                    }
                                                }
                                            }
                                        }}
                                    
                                }
                            }
                        }
                    }
                }
                self.Paynow()
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents();
                
            }}
        
    }
    
    func displayErrorAndLogOut() {
        let alert = SCLAlertView()
        let errorColor : UInt = 0xf8ac59
        if Util.shared.checkNetworkTapped() {
            alert.showError("Uh Oh!", subTitle: "Your session has expired! Please log in again.", closeButtonTitle: "Okay", colorStyle: errorColor)
        } else {
            alert.showError("Uh Oh!", subTitle: "You have lost connectivity to the internet. Please check your connection and log in again.", closeButtonTitle: "Okay", colorStyle:errorColor)//, colorTextButton: errorColor)
        }
    }
    
    func checknumber(number: String) -> Bool
    {
        guard CharacterSet(charactersIn: "0123456789").isSuperset(of: CharacterSet(charactersIn: number)) else {
            return false
        }
        return true
    }
    
}



extension UIView {
    func shake(duration: CFTimeInterval) {
        let translation = CAKeyframeAnimation(keyPath: "transform.translation.x");
        translation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        translation.values = [-5, 5, -5, 5, -3, 3, -2, 2, 0]
        
        let rotation = CAKeyframeAnimation(keyPath: "transform.rotation.x")
        rotation.values = [-5, 5, -5, 5, -3, 3, -2, 2, 0].map {
            ( degrees: Double) -> Double in
            let radians: Double = (M_PI * degrees) / 180.0
            return radians
        }
        
        let shakeGroup: CAAnimationGroup = CAAnimationGroup()
        shakeGroup.animations = [translation, rotation]
        shakeGroup.duration = duration
        self.layer.add(shakeGroup, forKey: "shakeIt")
    }
}
