

import UIKit
import Photos
import Firebase
import CoreLocation
import Alamofire
import AWSS3
import AWSCore


var ChatMessage = ChatVC()

class ChatVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIViewControllerTransitioningDelegate {
    
    
    
    var botAnswers = [String]()
    var botQuestion = ""
    var expressionid = [Int]()
    var native = UserDefaults.standard
    var variantData = [AnyObject]()
    var selectedColorIndex = 0
    var color = [String]()
    var innerVariantData = [[AnyObject]]()
    var variantImage = [String]()
    var quantity = [[Int]]()
    var checkVariantStock = [["variantid": Int(), "moq": Int(), "stock": Int()]]
    var botjsonContent = [AnyObject]()
    var botResponce = [AnyObject]()
    var botclicked = false
    
    //MARK: Properties
    @IBOutlet var inputBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    //bottom button
    @IBOutlet weak var message: UIButton!
    @IBOutlet weak var gallery: UIButton!
    @IBOutlet weak var camera: UIButton!
    @IBOutlet weak var sentbutton: UIButton!
    
    
    
    override var inputAccessoryView: UIView? {
        get {
            self.inputBar.frame.size.height = self.barHeight
            self.inputBar.clipsToBounds = true
            return self.inputBar
        }
    }
    override var canBecomeFirstResponder: Bool{
        return true
    }
    let locationManager = CLLocationManager()
    var items = [Message]()
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    var currentUser: User?
    var canSendLocation = true
    var botIndex = -1
    var quotationIndex = -1
    
    


    //MARK: Methods
    func customization() {
        self.imagePicker.delegate = self
        self.tableView.estimatedRowHeight = self.barHeight
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset.bottom = self.barHeight
        self.tableView.scrollIndicatorInsets.bottom = self.barHeight
        var username = ""
//        native.set("1", forKey: "currentuserid")
//        native.synchronize()
        if BBProductDetails.quotation == true
        {
            print(self.items.count, "self.items.count")
            botclicked = true
        }else
        {
            botclicked = false
        }
        if BBProductDetails.quotation == true || orderDetails.orderChat == true || quotationDetails.quotationChat == true || settingPage.settingChat == true || updateUserData.phyzioChat == true || myFeedPost.phyzioChat == true ||
            FeedDetails.phyzioChat == true
        {
            print("native.string(forKey:)", native.string(forKey: "otherusername"))
            if native.string(forKey: "otherusername") != nil
            {
            username = native.string(forKey: "otherusername")!
            }
            else
            {
                self.view.makeToast("Please try again later!", duration: 2.0, position: .center)
            }
            
        }
        else
        {
            if self.currentUser?.userid != nil
            {
            username = (self.currentUser?.name)!
            }
        }
        self.navigationItem.title = username
//        self.navigationItem.setHidesBackButton(true, animated: false)
//        let icon = UIImage.init(named: "chatBack")?.withRenderingMode(.alwaysOriginal)
//        let backButton = UIBarButtonItem.init(image: icon!, style: .plain, target: self, action: #selector(self.dismissSelf))
////        self.navigationController?.navigationBar.tintColor = UIColor.red
//        self.navigationItem.leftBarButtonItem = backButton
        
    }
    
    //Downloads messages
    func fetchData() {
        var userid  = ""
        var chatroomid = ""
        if BBProductDetails.quotation == true
        {
            print(self.items.count, "self.items.count")
            botclicked = true
        }else
        {
            botclicked = false
        }
        if BBProductDetails.quotation == true || orderDetails.orderChat == true || quotationDetails.quotationChat == true || settingPage.settingChat == true || updateUserData.phyzioChat == true || myFeedPost.phyzioChat == true || FeedDetails.phyzioChat == true
        {
            if native.string(forKey: "otherusername") != nil
            {
            userid = native.string(forKey: "currentuserid")!
            chatroomid = native.string(forKey: "chatroomid")!
               
            }
            else
            {
                self.view.makeToast("Please try again later!", duration: 2.0, position: .center)
            }
        }
        else
        {
            userid = self.currentUser!.userid
            chatroomid = self.currentUser!.id
        }
        
        Message.downloadAllMessages(userID: userid, chatRoomId: chatroomid, completion: {[weak weakSelf = self] (message) in
            
            weakSelf?.items.append(message)
           
            weakSelf?.items.sort{ $0.timestamp < $1.timestamp }
            DispatchQueue.main.async {
                if let state = weakSelf?.items.isEmpty, state == false {
                    weakSelf?.tableView.reloadData()
                    weakSelf?.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                }
            }
        })
        Message.markMessagesRead(forUserID: userid)
    }
    
    //Hides current viewcontroller
    @objc func dismissSelf() {
        if let navController = self.navigationController {
            var userid  = ""
            var chatroomid = ""
            if BBProductDetails.quotation == true || orderDetails.orderChat == true || quotationDetails.quotationChat == true || settingPage.settingChat == true || updateUserData.phyzioChat == true || myFeedPost.phyzioChat == true || FeedDetails.phyzioChat == true
            {
                if native.string(forKey: "otherusername") != nil
                {
                    userid = native.string(forKey: "currentuserid")!
                    chatroomid = native.string(forKey: "chatroomid")!
                    Message.markMessagesRead(forUserID: userid)
                    
                }
                else
                {
                    self.view.makeToast("Please try again later!", duration: 2.0, position: .center)
                }
                
            }
            else
            {
                if self.currentUser?.userid != nil
                {
                userid = self.currentUser!.userid
                chatroomid = self.currentUser!.id
                if userid != ""
                {
                Message.markMessagesRead(forUserID: userid)
                }
                }
            }
            BBProductDetails.quotation = false
            orderDetails.orderChat = false
            quotationDetails.quotationChat = false
            settingPage.settingChat = false
            updateUserData.phyzioChat = false
            myFeedPost.phyzioChat = false
            FeedDetails.phyzioChat = false
            myFeedPost.dismiss(animated: true, completion: nil)
        }
    }
    
    func composeBotAnswer(type: MessageType, content: Any)  {
        //        print("content", content)
        let data: NSMutableArray = []
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, quotationdata: data)
        var userid  = ""
        var chatroomid = ""
        
        if BBProductDetails.quotation == true || orderDetails.orderChat == true || quotationDetails.quotationChat == true || settingPage.settingChat == true || updateUserData.phyzioChat == true || myFeedPost.phyzioChat == true || FeedDetails.phyzioChat == true
        {
            
            if native.string(forKey: "otherusername") != nil
            {
                userid = native.string(forKey: "currentuserid")!
                chatroomid = native.string(forKey: "chatroomid")!
                
            }
            else
            {
                self.view.makeToast("Please try again later!", duration: 2.0, position: .center)
            }
            
        }
        else
        {
            userid = self.currentUser!.userid
            chatroomid = self.currentUser!.id
        }
        
        Message.send(message: message, fromID: userid, toID: native.string(forKey: "firebaseid")!,id: chatroomid, completion: {(_) in
        })
        
        //        self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
    }
    
    
    func composeMessage(type: MessageType, content: Any)  {
//        print("content", content)
        let data: NSMutableArray = []
        let message = Message.init(type: type, content: content, owner: .sender, timestamp: Int(Date().timeIntervalSince1970), isRead: false, quotationdata: data)
        var userid  = ""
        var chatroomid = ""
        
        if BBProductDetails.quotation == true || orderDetails.orderChat == true || quotationDetails.quotationChat == true || settingPage.settingChat == true || updateUserData.phyzioChat == true || myFeedPost.phyzioChat == true || FeedDetails.phyzioChat == true
        {
            print("chatroom id", native.string(forKey: "chatroomid")!)
            if native.string(forKey: "otherusername") != nil
            {
                userid = native.string(forKey: "currentuserid")!
                chatroomid = native.string(forKey: "chatroomid")!
                
            }
            else
            {
                self.view.makeToast("Please try again later!", duration: 2.0, position: .center)
            }
            
        }
        else
        {
            userid = self.currentUser!.userid
            chatroomid = self.currentUser!.id
        }
        Message.send(message: message, fromID: native.string(forKey: "firebaseid")!, toID: userid,id: chatroomid, completion: {(_) in
        })
        
//        self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
    }
    
    func checkLocationPermission() -> Bool {
        var state = false
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            state = true
        case .authorizedAlways:
            state = true
        default: break
        }
        return state
    }
    
    func animateExtraButtons(toHide: Bool)  {
        switch toHide {
        case true:
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
            self.bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    
    
    @IBAction func showMessage(_ sender: Any) {
       self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func selectGallery(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == .authorized || status == .notDetermined) {
            self.imagePicker.sourceType = .savedPhotosAlbum;
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func selectCamera(_ sender: Any) {
        
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.imagePicker.cameraDevice = .front
                self.imagePicker.delegate = self
                present(self.imagePicker, animated: true)
                
            }
            else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        
    }

    
    @IBAction func showOptions(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
    
    func validateEmptyText(textView textView: UITextField) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
                // this will be reached if the text is nil (unlikely)
                // or if the text only contains white spaces
                // or no text at all
                return false
        }
        
        return true
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputTextField.text {
            if text.characters.count > 0 {
                if validateEmptyText(textView: self.inputTextField) {
                    // do something
                    self.composeMessage(type: .text, content: self.inputTextField.text!)
                    self.inputTextField.text = ""
                } else {
                    // do something else
                    self.view.makeToast("Please enter some text", duration: 2.0, position: .top)
                }
                if text.isEmpty
                {
                    
                }
                
                
                
            }
        }
    }
    
    //MARK: NotificationCenter handlers
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.tableView.contentInset.bottom = height
            self.tableView.scrollIndicatorInsets.bottom = height
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       if botclicked == true
        {
            print("BBProductDetails.quotation outside", BBProductDetails.quotation)
        
            return self.items.count + 1
        }
        else
        {
            print("BBProductDetails.quotation inside", BBProductDetails.quotation)
            return self.items.count
        }
//
        
    }
    
//    func getBotData()
//    {
//        let token = self.native.string(forKey: "Token")!
//        if token != nil
//        {
//            let b2burl = native.string(forKey: "b2burl")!
////            var validateUrl =  "\(b2burl)/shop/get_bot_talk/?shopid=\(BBProductDetails.shopid)"
//             var validateUrl =  "\(b2burl)/shop/get_bot_talk/?shopid=\(BBProductDetails.shopid)"
//            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
////            print("/////", validateUrl)
//            var a = URLRequest(url: NSURL(string:validateUrl)! as URL)
//            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
//
//            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
//                //                                debugPrint(response)
//                //                                debugPrint(response)
////                print("response shop: \(response)")
//                let result = response.result
//                if let dict1 = result.value as? Dictionary<String, AnyObject>{
//                    if let invalidToken = dict1["detail"]{
//                        if invalidToken as! String  == "Invalid token."
//                        { // Create the alert controller
//                            let alertController = UIAlertController(title: "", message: "Your Seesion Expired Please Login again to continue", preferredStyle: .alert)
//
//                            // Create the actions
//                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                                UIAlertAction in
//                                NSLog("OK Pressed")
//                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
//                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                                appDelegate.window?.rootViewController = redViewController
//                            }
//                            // Add the actions
//                            alertController.addAction(okAction)
//                            //                            alertController.addAction(cancelAction)
//
//                            // Present the controller
//                            self.present(alertController, animated: true, completion: nil)
//                        }
//
//                    }
//                    else if dict1["status"] as! String == "failure"
//                    {
//
//                    }
//                    else if dict1["status"] as! String == "success"
//                    {
//                        print("botjson", dict1["response"] as? AnyObject)
//                        if let responces = dict1["response"] as? NSDictionary
//                        {
//                            if let bottalk = responces["bot_talk"] as? NSDictionary
//                            {
////                                print("9090 bottalk ", bottalk["parent_expression"] as? [AnyObject])
////                                if BBProductDetails.quotation == true
////                                {
//                                    var parent_expressionid = 0
//                                    if bottalk["parent_expression"] is NSNull
//                                    {}
//                                    else{
//                                    let data = (bottalk["parent_expression"] as? [AnyObject])!
//                                    for i in 0..<data.count
//                                    {
//                                    if data[i]["type"] as! String == "product"
//                                    {
//
//
//                                        self.botAnswers.append(data[0]["parent_expression"] as! String)
//                                        parent_expressionid = 1
//                                    }
//
//                                    }
//                                    if bottalk["expression"] is NSNull
//                                    {}
//                                    else
//                                    {
//                                    let data = (bottalk["expression"] as? [AnyObject])!
//                                        for j in 0..<data.count
//                                        {
//                                            let botData = data[j] as! NSDictionary
////                                            print("data[j]", botData["parent_expressionid"] as! NSNumber, botData["expressionid"] as! NSNumber)
//                                        if botData["parent_expressionid"] as! NSNumber == 1
//                                        {
//                                            self.botjsonContent.append(data[j])
//                                            var user_purchase_modeid = "1"
//                                            if self.native.object(forKey: "user_purchase_modeid") != nil {
//                                                user_purchase_modeid = self.native.string(forKey: "user_purchase_modeid")!
//                                            }
//                                           if data[j]["expression"] as! String != "Send a quotation."
//                                           {self.botAnswers.append(data[j]["expression"] as! String)
//                                            self.expressionid.append(Int(botData["expressionid"] as! NSNumber))
//                                            }
//                                        }
//
//                                        }}
//
//                                        if bottalk["response"] is NSNull
//                                        {}
//                                        else
//                                        {
//                                            if let data = bottalk["response"] as? [AnyObject]
//                                            {
//                                                print("bot answer", data)
//                                                self.botResponce = data
//                                            }
//
//
//                                        }
//                                    }
//
//                            }
//                        }
//
//                    }
//
//                }
//
//                self.tableView.reloadData()
//            }
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        

        var rowheight = 0
        if indexPath.row == self.items.count
        {
            if self.botAnswers.count > 0
            {
            rowheight = ((self.botAnswers.count + 1) * 30)
            }
            else
            {
                rowheight = 0
            }
        }
        
        else if self.items[indexPath.row].type == .botJson
        {
            
//            print("botjson", self.items[indexPath.row])
//            rowheight = ((self.botAnswers.count + 1) * 30)
            rowheight = 0
            
        }
          
        else if self.items[indexPath.row].type == .quotationJson && self.items[indexPath.row].type != .botJson
        {
            rowheight = 150
        }
        else if self.items[indexPath.row].type != .quotationJson && self.items[indexPath.row].type != .botJson
        {
        
        switch self.items[indexPath.row].owner {
        case .receiver:
             if self.items[indexPath.row].type == .image
            {
                rowheight = 200
            }
            else
            {
                rowheight = Int(UITableViewAutomaticDimension)
            }

        case .sender:
            
            if self.items[indexPath.row].type == .image
            {
                rowheight = 200
            }
            else
            {
                rowheight = Int(UITableViewAutomaticDimension)
            }
            }
        }
        return CGFloat(rowheight)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var ReuseCell = UITableViewCell()
        
        if indexPath.row == self.items.count
        {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "bottable", for: indexPath) as! BotTable
            
            cell.botCollection.tag = indexPath.row
            botIndex = indexPath.row
            cell.botCollection.reloadData()
            ReuseCell = cell
        }
        
//        print("message type table", self.items[indexPath.row].type)
       else if self.items[indexPath.row].type == .botJson
        {
//            botIndex = indexPath.row
//            let cell = self.tableView.dequeueReusableCell(withIdentifier: "bottable", for: indexPath) as! BotTable
//
//            cell.botCollection.tag = indexPath.row
//            cell.botCollection.reloadData()
//            ReuseCell = cell
            
        }
        else if self.items[indexPath.row].type == .quotationJson && self.items[indexPath.row].type != .botJson
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationview", for: indexPath) as! QuotationTableViewCell
            quotationIndex = indexPath.row
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self.items[indexPath.row].timestamp)/1000)  //UTC time  //YOUR currentTimeInMiliseconds METHOD
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "HH:mm"
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short
            
            
            let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
//            print("Local Time", strDateSelect) //Local time
            
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone(name: "UTC") as! TimeZone
            dateFormatter2.dateFormat = "HH:mm"
            
            let date3 = dateFormatter.date(from: strDateSelect)
//            print("DATE",date3)
            
            cell.messageTime.text = dateFormatter.string(from: date3!)
            cell.quotationCollection.tag = indexPath.row
            cell.quotationCollection.reloadData()
            ReuseCell = cell
        }
        else if self.items[indexPath.row].type != .botJson && self.items[indexPath.row].type != .quotationJson
        {
//            print("ind/ex is", indexPath.row, self.items[indexPath.row].owner)
        switch self.items[indexPath.row].owner  {
        case .receiver:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            cell.clearCellData()
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self.items[indexPath.row].timestamp)/1000)  //UTC time  //YOUR currentTimeInMiliseconds METHOD
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "HH:mm"
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short
            
            
            let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
//            print("Local Time", strDateSelect) //Local time
            
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone(name: "UTC") as! TimeZone
            dateFormatter2.dateFormat = "HH:mm"
            
            let date3 = dateFormatter.date(from: strDateSelect)
//            print("DATE",date3)
            
            cell.messageTime.text = dateFormatter.string(from: date3!)
            switch self.items[indexPath.row].type {
            case .text:
                let greenColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                cell.message.isHidden = false
                cell.messageBackground.backgroundColor = greenColor
                cell.message.text = (self.items[indexPath.row].content as! String)
                cell.message.textColor = UIColor.white
            case .image:
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.backgroundColor = UIColor.gray
                    cell.messageBackground.image = UIImage.init(named: "loading")
                    self.items[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    })
                }



            default: break
            }
            ReuseCell = cell
        case .sender:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self.items[indexPath.row].timestamp)/1000)  //UTC time  //YOUR currentTimeInMiliseconds METHOD
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "HH:mm"
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.short
            
            
            let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
//            print("Local Time", strDateSelect) //Local time
            
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone(name: "UTC") as! TimeZone
            dateFormatter2.dateFormat = "HH:mm"
            
            let date3 = dateFormatter.date(from: strDateSelect)
//            print("DATE",date3)
            
            cell.messageTime.text = dateFormatter.string(from: date3!)
            if BBProductDetails.quotation == true || orderDetails.orderChat == true || quotationDetails.quotationChat == true || settingPage.settingChat == true || updateUserData.phyzioChat == true || myFeedPost.phyzioChat == true ||
                FeedDetails.phyzioChat == true
            {
                
                var image = "https://b2b-shop-favicon.s3.amazonaws.com/63/masha_favicon.jpeg"
                if native.string(forKey: "otheruserpic") != nil
                {
                    image = native.string(forKey: "otheruserpic")!
                }
                if image != nil
                {
                    let url = NSURL(string:image as! String )
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            cell.profilePic.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                        }
                        DispatchQueue.main.async {
                            cell.profilePic.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
                
                
                }
            else
            {
                print("profile pic ", self.currentUser?.profilePic)
                if ((self.currentUser?.profilePic) != nil)
                {
                let image = self.currentUser?.profilePic as! String
                if image != nil
                {
                    let url = NSURL(string:image as! String )
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            cell.profilePic.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
                
                }
            }
            
            
            cell.profilePic.isHidden = false
                let dateTimeStamp1 = NSDate(timeIntervalSince1970:Double(self.items[indexPath.row].timestamp)/1000)  //UTC time  //YOUR currentTimeInMiliseconds METHOD
                let dateFormatter1 = DateFormatter()
                dateFormatter1.timeZone = NSTimeZone.local
                dateFormatter1.dateFormat = "HH:mm"
                dateFormatter1.dateStyle = DateFormatter.Style.short
                dateFormatter1.timeStyle = DateFormatter.Style.short
                
                
                let strDateSelect1 = dateFormatter.string(from: dateTimeStamp1 as Date)
//                print("Local Time", strDateSelect1) //Local time
            
                
                let dateFormatter21 = DateFormatter()
                dateFormatter21.timeZone = NSTimeZone(name: "UTC") as! TimeZone
                dateFormatter2.dateFormat = "HH:mm"
                
                let date31 = dateFormatter.date(from: strDateSelect1)
//                print("DATE",date31)
            
                cell.messageTime.text = dateFormatter.string(from: date31!)
            
            switch self.items[indexPath.row].type {
            case .text:
                let greenColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                cell.message.isHidden = false
                cell.messageBackground.backgroundColor = UIColor.white
                cell.messageBackground.isHidden = false
                cell.message.text = self.items[indexPath.row].content as? String
                
                
            case .image:
                
                if let image = self.items[indexPath.row].image {
                    cell.messageBackground.image = image
                    cell.message.isHidden = true
                } else {
                    cell.messageBackground.backgroundColor = UIColor.gray
                    cell.messageBackground.image = UIImage.init(named: "loading")
                    self.items[indexPath.row].downloadImage(indexpathRow: indexPath.row, completion: { (state, index) in
                        if state == true {
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    })
                }
            

            default: break
                
            }
            ReuseCell = cell

        }
        }
        
        return ReuseCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.inputTextField.resignFirstResponder()
//        if indexPath.row < self.items.count
//        {
//        switch self.items[indexPath.row].type {
//        case .image:
////            print("images")
//            if let photo = self.items[indexPath.row].image {
//                let info = ["viewType" : ShowExtraView.preview, "pic": photo] as [String : Any]
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showExtraView"), object: nil, userInfo: info)
//                self.inputAccessoryView?.isHidden = true
//            }
//
//        default: break
//        }
//    }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
//            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
//            ac.addAction(UIAlertAction(title: "OK", style: .default))
//            present(ac, animated: true)
        }
    }
    
   
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        print("working...")
        if #available(iOS 11.0, *) {
            
            if let imgUrl = info[UIImagePickerControllerImageURL] as? URL{
                let imgName = imgUrl.lastPathComponent
                print("working...", imgName)
                let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                let localPath = documentDirectory?.appending(imgName)
                
                let image = info[UIImagePickerControllerOriginalImage] as? UIImage
                UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                let data = UIImagePNGRepresentation(image!)! as NSData
                data.write(toFile: localPath!, atomically: true)
                //let imageData = NSData(contentsOfFile: localPath!)!
                let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
                print("photoURL", photoURL, photoURL.lastPathComponent)
                uploadPhotos(Image: image!, ImageURL: photoURL.lastPathComponent)
                
            }
            else if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                // imageViewPic.contentMode = .scaleToFill
//                imageViewPic.image = pickedImage
                print("photoURL", pickedImage)
                uploadPhotos(Image: pickedImage, ImageURL: ".jpeg")
            }
            
        } else {
            print("image not found")
            // Fallback on earlier versions
        }
        self.imagePicker.dismiss(animated: true)
//        present(ChatVC, animated: true)
//        self.dismiss(animated: true, completion: nil)
        
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker.dismiss(animated: true)
    }
    
    
    // upload Image
    func uploadPhotos(Image: UIImage, ImageURL: String)
        
    {
        let accessKey = "AKIAJYCW54FQC6MMHIDQ"
        let secretKey = "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"
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
//        var companyID = native.string(forKey: "shopid")!
       
        var firebaseid = native.string(forKey: "firebaseid")
        var userid  = ""
        var chatroomid = ""
//        print(native.string(forKey: "currentuserid"))
        if native.string(forKey: "currentuserid") != "2"{
            userid = native.string(forKey: "currentuserid")!
            chatroomid = native.string(forKey: "chatroomid")!
        }
        else
        {
            userid = self.currentUser!.userid
            chatroomid = self.currentUser!.id
        }
//         print(companyID, self.currentUser?.userid, userid)
        let S3BucketName = "b2b-chat-docs/uploads/"+(userid)+"/"+"\(firebaseid!)"+"/"+"\(Date().millisecondsSince1970)"
        
        let remoteName = ImageURL
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
        
        if let data = UIImageJPEGRepresentation(Image, 0.2) {
            print(data.count)
        
        do {
            print("-------", fileURL)
            try data.write(to: fileURL)
        }
        catch {}
        }
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/JPEG"
        uploadRequest.acl = .bucketOwnerRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
            DispatchQueue.main.async {
                
            }
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
//                    print("Uploaded to:\(absoluteString)")
//                    self?.s3count = (self?.s3count)! + 1
//                    if(self?.s3count == self?.selectedImage.count)
//                    {
                        print("uplaoding successfully")
                    self!.composeMessage(type: .image, content: absoluteString)
                        
//                    }
                }
            }
            
            return nil
        }
    
        
    }
    
    

    
    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let textcolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        self.navigationController?.navigationBar.tintColor = textcolor
        self.inputBar.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
        var userid  = ""
        var chatroomid = ""
        if BBProductDetails.quotation == true || orderDetails.orderChat == true || quotationDetails.quotationChat == true || settingPage.settingChat == true || updateUserData.phyzioChat == true || myFeedPost.phyzioChat == true ||
            FeedDetails.phyzioChat == true
        {
            if native.string(forKey: "otherusername") != nil
            {
                userid = native.string(forKey: "currentuserid")!
                chatroomid = native.string(forKey: "chatroomid")!
                Message.markMessagesRead(forUserID: userid)
                
            }
            else
            {
                self.view.makeToast("Please try again later!", duration: 2.0, position: .center)
            }
            
        }
        else
        {
            if self.currentUser?.userid != nil
            {
            userid = self.currentUser!.userid
            chatroomid = self.currentUser!.id
            if userid != ""
            {
            Message.markMessagesRead(forUserID: userid)
            }
            }
        }
    }
    
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.inputTextField.resignFirstResponder()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard),
                                               name: NSNotification.Name(rawValue: UIKeyboardFrameEndUserInfoKey), object: nil)
        
        
        
        self.dismissSelf()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChatMessage = self
        let grayColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        message.tintColor = grayColor
        gallery.tintColor = grayColor
        camera.tintColor = grayColor
        sentbutton.tintColor = .gray
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        self.customization()
        self.fetchData()
        if BBProductDetails.quotation == true
        {
//        self.getBotData()
        }
    }
    
    ///colection cell
    
    
    
    
    
    // Implement  bot view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var cellnumber = 0
//        print(collectionView.tag, "collectionview index")
        
        if collectionView.tag == self.items.count
        {
            cellnumber = self.botAnswers.count
        }
        else if self.items[collectionView.tag].type == .botJson && collectionView.tag == botIndex
        {
            var str = self.items[collectionView.tag].quotation as! NSMutableArray
            print("bot details", str)
           print("botcount", self.items[collectionView.tag])
           cellnumber = self.botAnswers.count
        }
        else if self.items[collectionView.tag].type == .quotationJson
            {
                 var str = self.items[collectionView.tag].quotation as! NSMutableArray

                cellnumber = str.count
            }
        
       
        
       return cellnumber
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var ReuseCell = UICollectionViewCell()
//        print("bot", self.items[collectionView.tag].type, collectionView.tag)
        if collectionView.tag == self.items.count
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "botdata", for: indexPath) as! botDataCollectionViewCell
            
//            print(self.botAnswers[indexPath.row])
            let blackcolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
            let greencolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
            
            if indexPath.row == 0
            {
                cell.line.isHidden = true
                cell.botAns.textColor = greencolor
                cell.botAns.text = botAnswers[indexPath.row]
            }
            else if indexPath.row == 1
            {
                cell.line.isHidden = false
                cell.botAns.textColor = blackcolor
                cell.botAns.text = botAnswers[indexPath.row]
            }
            else
            {
            cell.line.isHidden = true
            cell.botAns.textColor = blackcolor
            cell.botAns.text = botAnswers[indexPath.row]
            }
            ReuseCell = cell        }
        
        else if self.items[collectionView.tag].type == .botJson && collectionView.tag == botIndex
        {
//            print(":::::::::bot initialized::::::::::::::")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "botdata", for: indexPath) as! botDataCollectionViewCell

            let blackcolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
            
            let greencolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
            cell.botAns.text = botAnswers[indexPath.row]
            if indexPath.row == 0
            {
                cell.line.isHidden = true
                cell.botAns.textColor = UIColor.gray
                cell.botAns.text = botAnswers[indexPath.row]
            }
            else if indexPath.row == 1
            {
                cell.line.isHidden = false
                cell.botAns.textColor = UIColor.gray
                cell.botAns.text = botAnswers[indexPath.row]
            }
            else
            {
                cell.line.isHidden = true
                cell.botAns.textColor = UIColor.gray
                cell.botAns.text = botAnswers[indexPath.row]
            }
            ReuseCell = cell
        }
        else if self.items[collectionView.tag].type == .quotationJson 
            {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "quotationCollectionCell", for: indexPath) as! quotationcollectionCell
//                print("bot initialized \(indexPath.row)")
                if self.items.count > indexPath.row
                {
                var str = self.items[collectionView.tag].quotation as! NSMutableArray
                
               
                let data = str[indexPath.row] as! [String:Any]
                    cell.clipsToBounds = true
                    cell.layer.cornerRadius = 5
                    cell.layer.masksToBounds = true
                    
                    cell.contentView.layer.cornerRadius = 5
                    cell.contentView.layer.borderWidth = 1.0
                    
                    cell.contentView.layer.borderColor = UIColor.clear.cgColor
                    cell.contentView.layer.masksToBounds = true
                    
                    cell.layer.shadowColor = UIColor.lightGray.cgColor
                    cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                    cell.layer.shadowRadius = 2.0
                    cell.layer.shadowOpacity = 1.0
                    cell.layer.masksToBounds = false
                    cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
                cell.productName.text = (data["product_name"] as? String)!
                if data["image_url"] is NSNull
                {}
                else if data["image_url"] != nil
                {
                    let url = NSURL(string:data["image_url"] as! String )
                    if url != nil{
                        DispatchQueue.main.async {
                            cell.productImage.sd_setImage(with: URL(string: data["image_url"] as! String), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }

                }
                    if data["real_price"] is NSNull
                    {}
                    else
                    {
                    
                        if let totalfup = (data["real_price"] as? NSString)?.doubleValue {
                            // here, totalfup is a Double
                            cell.productValue.text = "Real Price: \(Int((data["real_price"] as! NSString).doubleValue))"
                        }
                        else {
                            // dict["totalfup"] isn't a String
                            cell.productValue.text = "Real Price: \(Int((data["real_price"] as! NSNumber).floatValue))"
                        }
                    
                    }
                    if data["quantity"] is NSNull
                    {}
                    else
                    {
                        if let totalfup = (data["quantity"] as? NSString)?.doubleValue {
                            // here, totalfup is a Double
                            cell.productQuantity.text = "Proposed Qty:  \((data["quantity"] as! NSString).doubleValue)"
                        }
                        else {
                            // dict["totalfup"] isn't a String
                            cell.productQuantity.text = "Proposed Qty:  \((data["quantity"] as? Any)!)"
                        }

                    
                    }
                    if data["requested_price"] is NSNull
                    {}
                    else
                    {
                        if let totalfup = (data["requested_price"] as? NSString)?.doubleValue {
                            // here, totalfup is a Double
                            cell.price.text = "Proposed Price:  \((data["requested_price"] as? NSString)!.doubleValue)"
                        }
                        else {
                            // dict["totalfup"] isn't a String
                            cell.price.text = "Proposed Price:  \(((data["requested_price"] as? Any)! as AnyObject).floatValue)"
                        }

                    
                    }
                }
            ReuseCell = cell
            }
        

        return ReuseCell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        print("collection click.....")
        var user_purchase_modeid = "1"
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        if collectionView.tag == self.items.count
        {
        
        if self.botAnswers[indexPath.row] == "Send a quotation."
        {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "createquotation") as! CreateQuotationViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
        }
        else
        {
            
            
            let data = ["Question": self.botQuestion, "BotAnswers": self.botAnswers] as [String : Any]
//            ChatMessage.composeMessage(type: .botJson, content: self.botjsonContent)
            
            
            var text = "Hey, we have our own manufacturing facility and every signle product has been strictly QC'd before packing."
            
            if self.botResponce.count > 0
            {
                print("botResponce", botResponce)
                for var i in 0..<self.botResponce.count
                {
                    print("responce0", self.botResponce[i])
                    let data = self.botResponce[i] as! NSDictionary
                    print(data["expressionid"] as! Int, self.expressionid[indexPath.row - 1])
                   if (data["expressionid"] as! Int) == self.expressionid[indexPath.row - 1]
                   {
                    print("botanswer", data["response"] as! String, self.botAnswers[0] as! String)
                    text = data["response"] as! String
                    self.composeBotAnswer(type: .text, content: self.botAnswers[0] as! String)
                    self.composeMessage(type: .text, content: self.botAnswers[indexPath.row] as! String)
                    self.composeBotAnswer(type: .text, content: text)
                    }
                }
            }
           
            }
        }
        if BBProductDetails.quotation == true
        {
            print(self.items.count, "self.items.count")
            botclicked = false
        }else
        {
            botclicked = false
        }
        self.tableView.reloadData()
    }
}

extension String{
    
    mutating func getJSONFormatedString(){
        
        self = self.replacingOccurrences(of: "(", with: "[")
        self = self.replacingOccurrences(of: ")", with: "]")
        self = self.replacingOccurrences(of: "\n ", with: "\n \"")
        self = self.replacingOccurrences(of: ",", with: "\",")
        self = self.replacingOccurrences(of: "\"\"", with: "\"")
        self = self.replacingOccurrences(of: "\",\"", with: "\",\n \"")
        self = self.replacingOccurrences(of: "[\"", with: "[\n \"")
        self = self.replacingOccurrences(of: "\"]", with: "\"\"\n]")
        self = self.replacingOccurrences(of: "\"\"", with: "\"")
    }
}
