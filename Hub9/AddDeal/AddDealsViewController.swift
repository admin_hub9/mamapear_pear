//
//  AddDealsViewController.swift
//  Hub9
//
//  Created by Deepak on 25/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Gallery
import Lightbox
import AWSCore
import AWSS3
import Alamofire
import Photos
import SVProgressHUD



var AddDealsView = AddDealsViewController()

class AddDealsViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LightboxControllerDismissalDelegate, GalleryControllerDelegate, UITextViewDelegate {

    /// A constant to layout the textFields.
    fileprivate let constant: CGFloat = 32
    
    var native = UserDefaults.standard
    
    ////for images and videos
    var gallery: GalleryController!
    let editor: VideoEditing = VideoEditor()
    ///fetch dynamic price disscount
    var imagePicker: UIImagePickerController!
    var selectedImage = [UIImage]()
    var videourl = [URL]()
    
    var s3count = 0
    var url = ""
    var timestamp = ""
    static var isediting = false
    var categoryid = 0
    var isDealCategoryActive = false
    var categoryText = "Select Category"
    
    var lightGray = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1)
    var textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    
    
    
    var productLink1 = ""
    var productTitle1 = ""
    var productMarketplace1 = ""
    var productBrand1 = ""
    var productReference1 = ""
    var productDesc1 = ""
    var categoryid1 = 0
    static var user_feedid = 0
//    var categoryText1 = ""
    
    
    @IBOutlet weak var delete: UIButton!
    @IBOutlet weak var dealImage: UIImageView!
    
    @IBOutlet weak var uploadButton: UIButton!
    
    
    @IBOutlet weak var categoryPlaceholder: UILabel!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryButton: UIButton!
    
    
    
    //textview
    @IBOutlet weak var productLink: UITextField!
    @IBOutlet weak var productTitle: UITextField!
    @IBOutlet weak var productMarketplace: UITextField!
    @IBOutlet weak var productBrand: UITextField!
    @IBOutlet weak var productReference: UITextField!
    @IBOutlet weak var productDescription: UITextView!
    @IBOutlet weak var descriptionPlaceholder: UILabel!
    
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.descriptionPlaceholder.isHidden = false
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if self.productDescription.text.count > 0
        {
            self.descriptionPlaceholder.isHidden = false
        }
        else
        {
            self.descriptionPlaceholder.isHidden = true
        }
        self.view.endEditing(true)
        return true
    }
    
    
    func validation() -> Bool {
        var valid: Bool = true
        
        if (productLink.text?.count)! < 1
        {
            productLink.text?.removeAll()
            productLink.attributedPlaceholder = NSAttributedString(string: "please enter a valid detail first", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if (productTitle.text?.count)! < 1
        {
            productTitle.text?.removeAll()
            productTitle.attributedPlaceholder = NSAttributedString(string: "please enter a valid detail first", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if (productMarketplace.text?.count)! < 1
        {
            productMarketplace.text?.removeAll()
            productMarketplace.attributedPlaceholder = NSAttributedString(string: "please enter a valid detail first", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if (productReference.text?.count)! < 1
        {
            productReference.text?.removeAll()
            productReference.attributedPlaceholder = NSAttributedString(string: "please enter a valid detail first", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if (productDescription.text?.count)! < 1
        {
            productDescription.text?.removeAll()
            productDescription.text = ""
            valid = false
        }
        return valid
    }
    
    @IBAction func selectCategoryButton(_ sender: Any) {
        UserFeed.isReviewCategoryActive=false
        self.isDealCategoryActive = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "DashboardCategory"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        
        if AddDealsViewController.isediting == true
        {
        self.productLink.text = "\(dealProductDetails.productLink)"
        self.productTitle.text = "\(dealProductDetails.productTitle)"
        self.productMarketplace.text = "\(dealProductDetails.productMarketplace)"
        self.productBrand.text = "\(dealProductDetails.productBrand)"
        self.productReference.text = "\(dealProductDetails.productReference)"
        self.productDescription.text = "\(dealProductDetails.productDesc)"
        self.categoryid = dealProductDetails.categoryid
        self.categoryText = "\(dealProductDetails.categoryText)"
            self.categoryid = dealProductDetails.categoryid
            self.categoryText = "\(dealProductDetails.categoryText)"
        
        if dealProductDetails.ProductImg.count>0
        {
        var imageUrl = dealProductDetails.ProductImg[0] as! String
        imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
        
        if let url = NSURL(string: imageUrl )
        {
            if url != nil
            {
                DispatchQueue.main.async {
                    self.dealImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                }
                self.delete.isHidden=true
                self.uploadButton.isHidden=false
            }
            else
            {
                self.delete.isHidden=false
                self.uploadButton.isHidden=true
            }
            }}
            
        }
        categoryLabel.text = categoryText
        if categoryText == "Select Category" || categoryText == ""
        {
            self.categoryText = "Select Category"
        self.categoryPlaceholder.isHidden = true
        categoryLabel.textColor = lightGray
        categoryLabel.attributedText = ( "\(categoryText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
        }
        else
        {
            self.categoryPlaceholder.isHidden = false
            categoryLabel.textColor = textColor
            categoryLabel.attributedText = ( "\(categoryText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
        }
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//    AddDealsViewController.isediting = false
    self.navigationController?.setNavigationBarHidden(false, animated: true)
    self.tabBarController?.tabBar.isHidden = false
    }
    // end TextView Responder
    @objc func tapDone(sender: Any) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryText = "Select Category"
        self.productDescription.addDoneButton(title: "Done", target: self, selector: #selector(tapDone(sender:)))
        if categoryText == "Select Category"
        {
        self.categoryPlaceholder.isHidden = true
        categoryLabel.textColor = lightGray
        categoryLabel.attributedText = ( "\(categoryText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
        }
        else
        {
            self.categoryPlaceholder.isHidden = false
            categoryLabel.textColor = textColor
            categoryLabel.attributedText = ( "\(categoryText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
        }
        self.descriptionPlaceholder.isHidden=true
        AddDealsView = self
        productDescription.text = "Please add some description text here"
        delete.isHidden=true
        uploadButton.isHidden=false
        productDescription.delegate = self
        uploadButton.layer.cornerRadius = uploadButton.frame.height/2
        uploadButton.clipsToBounds=true
        delete.layer.cornerRadius = delete.frame.height/2
        delete.clipsToBounds=true
        categoryLabel.text = categoryText
        categoryLabel.textColor = lightGray
        productDescription.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        productLink.delegate = self
        productTitle.delegate = self
        productMarketplace.delegate = self
        productBrand.delegate = self
        productReference.delegate = self
        productDescription.delegate = self
        let rightButtonItem = UIBarButtonItem.init(
              title: "Post",
              style: .done,
              target: self,
              action: #selector(rightButtonAction(sender:))
        )
        let leftButtonItem = UIBarButtonItem.init(
              title: "Cancel",
              style: .plain,
              target: self,
              action: #selector(leftButtonAction(sender:))
            
        )
        self.navigationItem.rightBarButtonItem = rightButtonItem
        self.navigationItem.leftBarButtonItem = leftButtonItem
        
    }
    @objc func rightButtonAction(sender: UIBarButtonItem)
    {
        self.uploadFeed()
    }
    @objc func leftButtonAction(sender: UIBarButtonItem)
    {
        let alertController = UIAlertController(
            title: "Are you sure that you want to cancel?",
            message: "",
            preferredStyle: UIAlertControllerStyle.alert
        )

        let cancelAction = UIAlertAction(
            title: "Yes",
            style: UIAlertActionStyle.destructive) { (action) in
            // ...
            self.navigationController?.popViewController(animated: true)
        }

        let confirmAction = UIAlertAction(
        title: "No", style: UIAlertActionStyle.default) { (action) in
            // ...
            
            
        }

        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)

        present(alertController, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         self.productTitle.resignFirstResponder()
         self.productReference.resignFirstResponder()
         self.productDescription.resignFirstResponder()
         self.productBrand.resignFirstResponder()
         self.productMarketplace.resignFirstResponder()
         self.productLink.resignFirstResponder()
         return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            if view.frame.origin.y == 0{
                
                self.view.frame.origin.y -= 170
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += 170
            }

        }
    }
    
     func uploadFeed() {
        if categoryid == 0
            {
              self.view.makeToast("Please select category first first", duration: 2.0, position: .center)
        }
        else if self.dealImage != nil && validation() == true
        {
        self.uploadDeal()
        }
        else
        {
           self.view.makeToast("Please add image and product detail first", duration: 2.0, position: .center)
        }
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.textColor == UIColor.lightGray {
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            if textView.text.isEmpty {
                textView.text = "Please add feed text here"
                textView.textColor = UIColor.lightGray
            }
        }

        
        
        
        ///// delete image and video
        
        @IBAction func deleteButton(_ sender: Any) {
            
            self.selectedImage.removeAll()
            self.dealImage.image = nil
            self.delete.isHidden=true
            self.uploadButton.isHidden=false
        }
        
        //############################## gallery controller #########################

        @IBAction func uploadButtonVideo(_ sender: Any) {
            
                gallery = GalleryController()
                gallery.delegate = self
                
                present(gallery, animated: true, completion: nil)
            }
            func buttonTouched() {
                gallery = GalleryController()
                gallery.delegate = self
                
                present(gallery, animated: true, completion: nil)
            }
            
            // MARK: - LightboxControllerDismissalDelegate
            
            func lightboxControllerWillDismiss(_ controller: LightboxController) {
                
            }
            
            
            
            // MARK: - GalleryControllerDelegate
            
            func galleryControllerDidCancel(_ controller: GalleryController) {
                controller.dismiss(animated: true, completion: nil)
                gallery = nil
            }
            
            func requestImage(for asset: PHAsset,
                              targetSize: CGSize,
                              contentMode: PHImageContentMode,
                              completionHandler: @escaping (UIImage?) -> ()) {
                let imageManager = PHImageManager()
                imageManager.requestImage(for: asset,
                                          targetSize: targetSize,
                                          contentMode: contentMode,
                                          options: nil) { (image, _) in
                                            completionHandler(image)
                }
            }
            func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
                
                self.dismiss(animated: true, completion: nil)
                gallery = nil
                
                let alert = UIAlertController(title: "Alert", message: "Can't Upload Video", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                //editor.edit(video: video) { (editedVideo: Video?, tempPath: URL?) in
//                    print("video url is", video.asset.mediaSubtypes)
//                    let asset = video.asset// your existing PHAsset
//                    let targetSize = CGSize(width: 100, height: 100)
//                    let contentModel = PHImageContentMode.aspectFit
//                    self.requestImage(for: asset, targetSize: targetSize, contentMode: contentModel, completionHandler: { image in
//                        // Do something with your image if it exists
//        //                print("images", image)
//                        if self.videourl.count < 1
//                        {
//                        self.videourl.append(tempPath!)
//                            if self.selectedImage.count == 0
//                            {
//                                self.selectedImage.append(image!)
//                            }
//                            else
//                            {
//                                let firstimage = self.selectedImage[0]
//                                self.selectedImage.remove(at: 0)
//                                self.selectedImage.insert(image!, at: 0)
//                                self.selectedImage.append(firstimage)
//
//                            }
//
//                        DispatchQueue.main.async {
//                            if (self.videourl.count)<=0
//                            {}
//                            else
//                            {
//                                self.dealImage.image = self.selectedImage[0]
//                                self.delete.isHidden=false
//                                self.uploadButton.isHidden=true
//                            }
//                        }
//                        }
//                        else
//                        {
//                            let alert = UIAlertController(title: "Alert", message: "Can't Upload Multiple Videos", preferredStyle: UIAlertController.Style.alert)
//                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
//                            self.present(alert, animated: true, completion: nil)
//                        }
//
//                    })
//
//        //            DispatchQueue.main.async {
//        //                if let tempPath = tempPath {
//        //                    let controller = AVPlayerViewController()
//        //                    controller.player = AVPlayer(url: tempPath)
//        //
//        //                    self.present(controller, animated: true, completion: nil)
//        //                }
//        //            }
//                }
                
            }
            
            func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
                self.dismiss(animated: true, completion: nil)
                print(images)
                
                //    let data = UIImageJPEGRepresentation(image!, 0.9)
                
                Image.resolve(images: images, completion: { [weak self] resolvedImages in
                    if self!.selectedImage.count + resolvedImages.count < 2
                    {
                    for var i in 0..<resolvedImages.count
                    {
                        let image = resolvedImages[i]
                        self!.selectedImage.append(image!)
                    }
                        if (self?.videourl.count)!>0
                        {}
                        else
                        {
                            self?.dealImage.image = self?.selectedImage[0]
                            self?.delete.isHidden=false
                            self?.uploadButton.isHidden=true
                        }
    //
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Alert", message: "Only one imgae can be add at a time", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self!.present(alert, animated: true, completion: nil)
                    }
                })
                gallery = nil
            }
            
            func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
                LightboxConfig.DeleteButton.enabled = true
    //            for var i in 0..<images.count
    //            {
    //                print(",,,,,", images[i].asset.description)
    //            }
                SVProgressHUD.show()
                Image.resolve(images: images, completion: { [weak self] resolvedImages in
                    
                    SVProgressHUD.dismiss()
                    self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
                })
            }
            
            // MARK: - Helper
            
            func showLightbox(images: [UIImage]) {
                guard images.count > 0 else {
                    return
                }
                
                let lightboxImages = images.map({ LightboxImage(image: $0) })
                let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
                lightbox.dismissalDelegate = self
                
                gallery.present(lightbox, animated: true, completion: nil)
            }
            
            //############################ end Gallery Controller ##########################

        
        func uploadPhotos()
                
    {
        print(" inner upload")
        if selectedImage.count > 0
            {
                print(" inner upload is greater than zero")
                let accessKey = root.s3AccessKeyId
                let secretKey = root.s3SecretAccessKey
                
            if native.object(forKey: "userid") != nil
            {
                print(" inner upload is valid user")
                let userid = native.string(forKey: "userid")!
                
                let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
                let configuration = AWSServiceConfiguration(region:AWSRegionType.USWest2, credentialsProvider:credentialsProvider)
                
                AWSServiceManager.default().defaultServiceConfiguration = configuration
                
                let S3BucketName = "\(root.s3Bucket)/user-deals/"+userid+"/Image"
                let remoteName =  String("\(self.timestamp)_update") + ".jpeg"
                
                
                let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
                
                let data = UIImageJPEGRepresentation(selectedImage[0], 0.2)
                do {
                    try data?.write(to: fileURL)
                }
                catch {}
                
                let uploadRequest = AWSS3TransferManagerUploadRequest()!
                uploadRequest.body = fileURL
                uploadRequest.key = remoteName
                uploadRequest.bucket = S3BucketName
                uploadRequest.contentType = "image/jpeg"
                uploadRequest.acl = .publicRead
                
                let transferManager = AWSS3TransferManager.default()
                
                transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
                    DispatchQueue.main.async {
//                        self?.uploadButton.isHidden = false
//                        self?.activityIndicator.stopAnimating()
                    }
                    
                    if let error = task.error {
                        print("Upload failed with error: (\(error.localizedDescription))")
                    }
                    
                    if task.result != nil {
                        let url = AWSS3.default().configuration.endpoint.url
                        let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                        if let absoluteString = publicURL?.absoluteString {
                            print("Uploaded to:\(absoluteString)")
                            if self?.videourl.count == 0
                            {
                                self!.native.set("", forKey: "categoryname")
                                self!.native.set("", forKey: "brand")
                                self!.native.synchronize()

                                self!.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                    
                    return nil
                }
                
                }
                
                }
            }
  
    
    
   func uploadDeal()
   {
    let token = self.native.string(forKey: "Token")!
    var fav = 0
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
        var timestamp = "\(NSDate().timeIntervalSince1970 * 1000)"
                self.timestamp = timestamp
                let parameters: [String:Any]
                    
                    
                 if AddDealsViewController.isediting == false
                 {
                   parameters = ["image_url":["https://mamapear.s3-us-west-2.amazonaws.com/user-deals/\(userid)/Image/\(timestamp)_update.jpeg"], "feed_text": "\(self.productDescription.text!)", "feed_typeid": 1, "tagid":[categoryid], "marketplace":"\(self.productMarketplace.text!)", "brand":"\(productBrand.text!)", "product_url":"\(self.productLink.text!)", "price_reference":"\(self.productReference.text!)", "title":"\(self.productTitle.text!)"]
                }
                else
                 {
                    parameters = ["image_url":["https://mamapear.s3-us-west-2.amazonaws.com/user-deals/\(userid)/Image/\(timestamp)_update.jpeg"], "feed_text": "\(self.productDescription.text!)", "feed_typeid": 1, "tagid":[categoryid], "marketplace":"\(self.productMarketplace.text!)", "brand":"\(productBrand.text!)", "product_url":"\(self.productLink.text!)", "price_reference":"\(self.productReference.text!)", "title":"\(self.productTitle.text!)", "user_feedid": AddDealsViewController.user_feedid]
                }
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post")
    
        let b2burl = native.string(forKey: "b2burl")!
        var url = ""
            
            if AddDealsViewController.isediting == false
            {
             url = "\(b2burl)/users/upload_user_feed/"
                }
                else
            {
                url = "\(b2burl)/users/edit_user_feed/"
                }
        print("add deal product", url, parameters)
        Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            print(response)
            switch response.result
            {
                
                
            case .failure(let error):
                //                print(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    print("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    print(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    print("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
            case .success:
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
            
            else if let json = response.result.value {
                let jsonString = "\(json)"
                print("res;;;;;", jsonString)
                if dict1["status"] as! String == "success"
                {
                    if self.isEditing == false
                    {
                        print("call upload photo")
                    self.uploadPhotos()
                    }
                    self.productLink.text = ""
                    self.productTitle.text = ""
                    self.productMarketplace.text = ""
                    self.productBrand.text = ""
                    self.productReference.text = ""
                    self.productDescription.text = ""
                    self.categoryid = 0
                    self.categoryText = "Select Category"
                    if self.categoryText == "Select Category"
                    {
                    self.categoryPlaceholder.isHidden = true
                        self.categoryLabel.textColor = self.lightGray
                        self.categoryLabel.attributedText = ( "\(self.categoryText)".fontName("HelveticaNeue-Regular").size(9)).attributedText
                    }
                    else
                    {
                        self.categoryPlaceholder.isHidden = false
                        self.categoryLabel.textColor = self.textColor
                        self.categoryLabel.attributedText = ( "\(self.categoryText)".fontName("HelveticaNeue-Regular").size(15)).attributedText
                    }
                    self.categoryLabel.textColor = self.lightGray
                    self.view.makeToast("Your deal added successfully", duration: 2.0, position: .center)
//                    self.dismiss(animated: true, completion: nil)
//                      self.navigationController?.popViewController(animated: true)
                        }
                        
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                        }
            }}
            }
            }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    
    }
    
    }

    
