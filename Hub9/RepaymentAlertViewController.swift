//
//  RepaymentAlertViewController.swift
//  Hub9
//
//  Created by Deepak on 7/23/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class RepaymentAlertViewController: UIViewController {
    @IBOutlet weak var AlertContainer: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.endIgnoringInteractionEvents()
        AlertContainer.layer.cornerRadius = 8
        AlertContainer.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelAlert(_ sender: Any) {
        UIApplication.shared.endIgnoringInteractionEvents()
        self.dismiss(animated: true, completion: nil)
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
        
    }

    
    @IBAction func RetryPayment(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        PaymentAddress.showPaymentForm()
    }
    
}
