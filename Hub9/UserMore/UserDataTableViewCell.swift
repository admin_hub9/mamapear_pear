//
//  UserDataTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/25/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class UserDataTableViewCell: UITableViewCell {

    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var username: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
