//
//  MedealsCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/25/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class MedealsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var endIn: UILabel!
    @IBOutlet weak var waitingUsers: UILabel!
    
    var localEndTime: TimeInterval = 0 //set value from model
    var timer:Timer?
    
    
    @objc func timerIsRunning(timer: Timer){
        let diff = localEndTime - Date().timeIntervalSince1970
        if diff  > 0 {
            self.showProgress()
            return
        }
        self.stopProgress()
        
        
    }
    
    func showProgress(){
        let endDate = Date(timeIntervalSince1970: localEndTime / 1000)
        let nowDate = Date()
        
        var components = Calendar.current.dateComponents(Set([.day, .hour, .minute, .second, .nanosecond]), from: endDate, to: nowDate)
        // *** Get Individual components from date ***
        self.endIn?.text = "Ends in: \(String(format:"%02i:%02i:%02i:%02i", (24*(2-components.day!))+(23-components.hour!) , 59 - components.minute!, 60 - components.second!, Int(Double(components.nanosecond!)/100000000 + 0.5)))"
        
        
    }
    
    func stopProgress(){
        self.endIn?.text = String(format:"%02i:%02i:%02i", 0, 0, 0)
        self.timer?.invalidate()
        self.timer = nil
    }
    
    func startTimerProgress() {
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.timerIsRunning(timer:)), userInfo: nil, repeats: true)
        self.timer?.fire()
    }
    
    
    
}
