//
//  UserNavTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/25/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class UserNavTableViewCell: UITableViewCell {

    @IBOutlet weak var navBack: UIView!
    @IBOutlet weak var favProductCounts: UILabel!
    @IBOutlet weak var favShopCount: UILabel!
    @IBOutlet weak var viewedCount: UILabel!
    @IBOutlet weak var productButton: UIButton!
    @IBOutlet weak var shopButton: UIButton!
    @IBOutlet weak var ViewedItenButton: UIButton!
    
    // closet
    @IBOutlet weak var closetCount: UILabel!
    @IBOutlet weak var closetButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
