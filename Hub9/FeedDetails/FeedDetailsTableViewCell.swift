//
//  FeedDetailsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 27/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class FeedDetailsTableViewCell: UITableViewCell {
    
    
    

    
    @IBOutlet weak var viewController: UIView!
    @IBOutlet weak var videoController: UIView!
    @IBOutlet weak var imageController: UIImageView!
    @IBOutlet weak var tagName: UILabel!
    @IBOutlet weak var textData: UILabel!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
