//
//  UserDetailTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 04/06/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class UserDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var uploadTime: UILabel!

    // follow button
    @IBOutlet weak var followContainer: UIView!
    @IBOutlet weak var followText: UILabel!
    @IBOutlet weak var followButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
