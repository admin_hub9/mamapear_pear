//
//  FeedDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 27/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AVKit

var FeedDetails = FeedDetailsViewController()

class FeedDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    
    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    var commentText = ""
    var native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenHeight = 0
    var phyzioChat = false
    
    /// data
    var feedImage = ""
    var feedVideo = ""
    var QuestionText  = ""
    var like = 0
    var likeCount = 0
    var Comment = false
    var data = [AnyObject]()
    var owner = [String]()
    var replyStatus = false
    var postCell = postCommentsTableViewCell()
    var comment_count = 0
    
    var user_feedid = 0
    var ReviewUserid = 0
    var limit = 20
    var offset = 0
    var isFeedID_liked = false
    var shareLink = ""
    var userImage = ""
    var userFirbaseID = ""
    
    var userName = ""
    var uploadTime = 0.0
    var titletext = ""
    var tagText = ""
    var isFollowing = false
    
    var is_editing = false
    var editIndex = 0
    
    ////edit option data
    var categoryid = 0
    var categoryText = ""
    var feed_share_link = ""
    
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var shopButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedDetailsTable: UITableView!
    
    /// bottom button
    @IBOutlet weak var likeButton: UIButton!
    
    
    //second responder
    @IBOutlet weak var myPic2: UIImageView!
    @IBOutlet weak var commentText2: UITextField!
    @IBOutlet weak var secondResponder: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        
        
        self.avPlayer.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        
        super.viewWillDisappear(animated)
        self.avPlayer.pause()
        NotificationCenter.default.post(name: Notification.Name("avPlayerDidDismiss"), object: nil, userInfo: nil)
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        print("touch?.view?.tag", touch?.view?.tag)
        if touch?.view?.tag == 32 {
            print("inside view")
            self.hideSecondResponder()
            self.commentText2.resignFirstResponder()
            self.feedDetailsTable.reloadData()
        }
        else
        {
            //            dismiss(animated: true, completion: nil)
            print("outside view")
        }
        
        self.is_editing = false
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= 0
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            self.hideSecondResponder()
        }
    }
    
    
    func hideSecondResponder()
    {
        UIView.transition(with: secondResponder, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.secondResponder.transform = CGAffineTransform(translationX: 0, y: 800)
                self.secondResponder.isHidden = true
                self.feedDetailsTable.reloadData()
        }, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FeedDetails = self
        self.buttonConstraint.constant = 0
        self.commentText2.delegate = self
        UIView.transition(with: secondResponder, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0
                self.secondResponder.transform = CGAffineTransform(translationX: 0, y: 800)
                self.secondResponder.isHidden = true
        }, completion: nil)
        self.feedDetailsTable.tableFooterView = UIView()
        self.feedDetailsTable.isHidden = true
        //        self.feedDetailsTable.reloadData()
        //
        self.getRecommended_product(limit: self.limit, offset: self.offset)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
    }
    
    func relativeDate(time: Double) -> String {
        
        let theDate = Date(timeIntervalSince1970: TimeInterval(time))
        let components = Calendar.current.dateComponents([.day, .year, .month, .weekOfYear], from: theDate, to: Date())
        if let year = components.year, year == 1{
            return "\(year) year ago"
        }
        if let year = components.year, year > 1{
            return "\(year) years ago"
        }
        if let month = components.month, month == 1{
            return "\(month) month ago"
        }
        if let month = components.month, month > 1{
            return "\(month) months ago"
        }
        
        if let week = components.weekOfYear, week == 1{
            return "\(week) week ago"
        }
        if let week = components.weekOfYear, week > 1{
            return "\(week) weeks ago"
        }
        
        if let day = components.day{
            if day > 1{
                return "\(day) days ago"
            }else{
                "Yesterday"
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: theDate)
        return strDate
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2
        {
            native.set(self.ReviewUserid, forKey: "feed_userid")
            native.synchronize()
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "UserFeedPost"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        else if indexPath.row > 3 && indexPath.row < self.data.count + 4
        {
            print(indexPath.row, self.owner[indexPath.row - 4], self.editIndex)
            if self.owner[indexPath.row - 4] == "Sender"
            {
                self.replyStatus=true
                self.editIndex = indexPath.row - 4
                self.feedDetailsTable.reloadData()
                UIView.transition(with: secondResponder, duration: 0.4, options: .beginFromCurrentState, animations:
                    {
                        //                                self.blueView.alpha = 0
                        self.secondResponder.transform = CGAffineTransform(translationX: 0, y: 0)
                        self.commentText2.becomeFirstResponder()
                        self.secondResponder.isHidden = false
                }, completion: nil)
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellSize = 0
        
        if indexPath.row == 0
        {
            cellSize = 280
        }
        else if indexPath.row == 1
        {
            cellSize = Int(UITableViewAutomaticDimension)
        }
            
        else if indexPath.row == 2
        {
            
            cellSize = 60
        }
        else if indexPath.row == 3
        {
            
            cellSize = 50
            
        }
            
        else if indexPath.row >= 4 && indexPath.row < self.data.count + 5
        {
            cellSize = Int(UITableViewAutomaticDimension)
        }
        else if indexPath.row == self.data.count + 5
        {
            cellSize = 0
        }
        else
        {
            cellSize = 0
        }
        
        return CGFloat(cellSize)
    }
    
    
    /// number of row in table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 6 + self.data.count
        
    }
    
    // table view cell
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var reuseCell = UITableViewCell()
        let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
        var ReuseCell = UITableViewCell()
        if indexPath.row == 0
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "feedDetails", for: indexPath) as! FeedDetailsTableViewCell
            cell2.videoController.isHidden=true
            cell2.imageController.isHidden=true
            //                        cell2.userConatiner.isHidden=true
            cell2.tagName.layer.cornerRadius = cell2.tagName.frame.height/2
            
            cell2.tagName.clipsToBounds = true
            
            if self.tagText != ""
            {
                cell2.tagName.text = "  \(self.tagText)  "
            }
            else
            {
                cell2.tagName.text = ""
            }
            
            if self.titletext != ""
            {
                print("self.titletext", self.titletext)
                cell2.textData.attributedText = ( "\(self.titletext)".fontName("HelveticaNeue-Regular")).attributedText
            }
            if self.feedVideo  != ""
            {
                
                
                cell2.videoController.isHidden=false
                cell2.imageController.isHidden=true
                
                //                            cell2.videoController.constant = 300.0
                let url = self.feedVideo as! String
                
                let videoURL = NSURL(string: url)
                avPlayer = AVPlayer(url: videoURL! as URL)
                avPlayer.volume = 10
                avPlayer.isMuted = true
                let playerController = AVPlayerViewController()
                playerController.player = avPlayer
                
                self.addChildViewController(playerController)
                
                // Add your view Frame
                
                //                    playerController.videoGravity = AVLayerVideoGravity.resizeAspect
                playerController.view.frame = cell2.videoController.bounds
                playerController.showsPlaybackControls = false
                playerController.videoGravity = AVLayerVideoGravity.resize.rawValue
                
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                } catch _ {
                }
                do {
                    try AVAudioSession.sharedInstance().setActive(true)
                } catch _ {
                }
                do {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                } catch _ {
                }
                //                                        avPlayer.automaticallyWaitsToMinimizeStalling = false
                //                            avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.pause
                
                // Add subview in your view
                cell2.videoController.isHidden = false
                
                cell2.videoController.addSubview(playerController.view)
                playerController.didMove(toParentViewController: self)
                
                avPlayer.play()
                NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem, queue: .main) { [weak self] _ in
                    self!.avPlayer.seek(to: kCMTimeZero)
                    self?.avPlayer.play()
                }
            }
                
                
            else if (self.feedImage as? String)! != ""
            {
                
                cell2.videoController.isHidden=true
                cell2.imageController.isHidden=false
                
                
                let imagedata = self.feedImage
                
                var imageUrl = (imagedata)
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                if let url = NSURL(string: imageUrl )
                {
                    print("url", url)
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            cell2.imageController.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        } }
                } }
            
            
            ReuseCell = cell2
        }
            
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "description", for: indexPath) as! ReviewDescriptionTableViewCell
            cell.descriptionText.attributedText = ( "\(self.QuestionText)".fontName("HelveticaNeue-Regular")).attributedText
            ReuseCell = cell
        }
        else if indexPath.row == 2
        {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "userDetail", for: indexPath) as! UserDetailTableViewCell
            
            if self.userImage != ""
            {
                
                
                let imagedata = self.userImage
                
                var imageUrl = (imagedata)
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                if let url = NSURL(string: imageUrl )
                {
                    print("url", url)
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            cell2.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }}
                }}
            if native.object(forKey: "userid") != nil
            {
                if  native.object(forKey: "userid")! != nil
                {
                    
                    if self.ReviewUserid != Int(native.string(forKey: "userid")!)
                    {
                        cell2.followContainer.isHidden = false
                    }
                    else
                    {
                        cell2.followContainer.isHidden = true
                    }
                }}
            cell2.userName.text = self.userName
            cell2.followContainer.layer.cornerRadius = cell2.followContainer.frame.height/2
            cell2.followContainer.layer.borderWidth = 0.5
            if self.isFollowing == false
            {
                cell2.followContainer.layer.borderColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1).cgColor
                cell2.followContainer.backgroundColor = UIColor.white
                cell2.followText.textColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                cell2.followText.text = "Follow"
            }
            else
            {
                cell2.followContainer.layer.borderColor = UIColor.white.cgColor
                cell2.followContainer.backgroundColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                cell2.followText.textColor = UIColor.white
                cell2.followText.text = "Following"
            }
            cell2.followButton.tag = 0
            cell2.followButton.addTarget(self, action: #selector(userFollowButton(sender:)), for: .touchUpInside)
            if uploadTime != 0.0
            {
                
                let time  = self.relativeDate(time: uploadTime)
                
                cell2.uploadTime.text = "Posted: \(time)"
            }
            
            ReuseCell = cell2
            
        }
        else if indexPath.row == 3
        {
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentHeader", for: indexPath) as! commentHeaderTableViewCell
            if self.comment_count == 1
            {
                cell.totalComments.text = "\(self.comment_count) Comment"
                
            }
            else if self.comment_count > 1
            {
                cell.totalComments.text = "\(self.comment_count) Comments"
                
            }
            else
            {
                cell.totalComments.text = "Add Comments"
            }
            if self.comment_count > 3
            {
                cell.viewAllButton.isHidden = false
            }
            else
            {
                
                cell.viewAllButton.isHidden = true
            }
            cell.viewAllButton.tag = 3
            cell.viewAllButton.addTarget(self, action: #selector(comment_data(sender:)), for: .touchUpInside)
            
            ReuseCell = cell
        }
        else if indexPath.row >= 4 && indexPath.row < self.data.count + 4
        {
            if self.owner[indexPath.row - 4] == "Sender"
            {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
                
                let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if indexPath.row == 0
                {
                    cell.seprator.isHidden=true
                }
                else
                {
                    cell.seprator.isHidden=true
                }
                if self.native.object(forKey: "userid") != nil
                {
                    let userid = self.native.string(forKey: "userid")!
                    
                    if data[indexPath.row - 4]["user_id"] is NSNull
                    {}
                    else
                    {   if Int(userid) != (data[indexPath.row - 4]["user_id"] as? Int)!
                    {
                        cell.deleteButton.isHidden = true
                        cell.editButton.isHidden = true
                    }
                    else
                    {
                        cell.deleteButton.isHidden = false
                        cell.editButton.isHidden = true
                        }
                        
                    }
                }
                if data[indexPath.row - 4]["profile_pic_url"] is NSNull
                {}
                else
                {
                    
                    let imagedata = data[indexPath.row - 4]["profile_pic_url"] as? String
                    
                    var imageUrl = (imagedata)!
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    
                    if let url = NSURL(string: imageUrl )
                    {
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                
                var like = 0
                var comment = 0
                
                
                if data[indexPath.row - 4]["comment"] is NSNull
                {
                    cell.NameAndText.text = ""
                }
                else
                {
                    
                    cell.NameAndText.attributedText = ("\(data[indexPath.row - 4]["first_name"] as! String) \(data[indexPath.row - 4]["last_name"] as! String) ".color(lightBlackColor).size(16.0)).attributedText
                    
                    cell.commentText.attributedText = ( "\(data[indexPath.row - 4]["comment"] as! String)".color(lightGrayColor).size(14.0)).attributedText
                    
                    if data[indexPath.row - 4]["inserted_at"] is NSNull
                    {}
                    else
                    {
                        if let date1 = self.data[indexPath.row - 4]["inserted_at"] as? String
                        {
                            
                            let time  = self.relativeDate( time: Double(date1)!)
                            print("time", time)
                            cell.commentTime.text = "\(time)"
                        }
                    }
                    
                    
                }
                
                
                cell.editButton.tag = indexPath.row - 4
                cell.editButton.addTarget(self, action: #selector(updateEditButton(Feed_Index:)), for: .touchUpInside)
                cell.deleteButton.tag = indexPath.row - 4
                cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
                
                ReuseCell = cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "replyCommentCell", for: indexPath) as! ReplyCommentTableViewCell
                
                let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                
                if self.native.object(forKey: "userid") != nil
                {
                    let userid = self.native.string(forKey: "userid")!
                    
                    if data[indexPath.row - 4]["user_id"] is NSNull
                    {}
                    else
                    {   if Int(userid) != (data[indexPath.row - 4]["user_id"] as? Int)!
                    {
                        cell.deleteButton.isHidden = true
                        cell.editButton.isHidden = true
                    }
                    else
                    {
                        cell.deleteButton.isHidden = false
                        cell.editButton.isHidden = true
                        }
                        
                    }
                }
                if data[indexPath.row - 4]["profile_pic_url"] is NSNull
                {}
                else
                {
                    
                    let imagedata = data[indexPath.row - 4]["profile_pic_url"] as? String
                    
                    var imageUrl = (imagedata)!
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    
                    if let url = NSURL(string: imageUrl )
                    {
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                
                var like = 0
                var comment = 0
                
                
                if data[indexPath.row - 4]["reply"] is NSNull
                {
                    cell.NameAndText.text = ""
                }
                else
                {
                    
                    cell.NameAndText.attributedText = ("\(data[indexPath.row - 4]["first_name"] as! String) \(data[indexPath.row - 4]["last_name"] as! String) ".color(lightBlackColor).size(16.0)).attributedText
                    print("reply comment", indexPath.row, self.owner[indexPath.row - 4], self.data[indexPath.row - 4])
                    
                    cell.commentText.attributedText = ( "\(data[indexPath.row - 4]["reply"] as! String)".color(lightGrayColor).size(14.0)).attributedText
                    if data[indexPath.row - 4]["inserted_at"] is NSNull
                    {}
                    else
                    {
                        if let date1 = self.data[indexPath.row - 4]["inserted_at"] as? String
                        {
                            
                            let time  = self.relativeDate( time: Double(date1)!)
                            print("time", time)
                            cell.commentTime.text = "\(time)"
                        }
                    }
                    
                    
                }
                
                
                cell.editButton.tag = indexPath.row - 4
                cell.editButton.addTarget(self, action: #selector(updateEditButton(Feed_Index:)), for: .touchUpInside)
                cell.deleteButton.tag = indexPath.row - 4
                cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
                
                ReuseCell = cell
            }
        }
        else if indexPath.row == self.data.count + 5
        {
            
            
            postCell = tableView.dequeueReusableCell(withIdentifier: "postComment") as! postCommentsTableViewCell
            
            postCell.comment.delegate = self
            postCell.comment.resignFirstResponder()
            postCell.userImage.layer.cornerRadius = postCell.userImage.frame.height/2
            postCell.userImage.clipsToBounds=true
            if native.object(forKey: "profilepic") != nil
            {
                let image = native.string(forKey: "profilepic")!
                
                if image != nil{
                    let url = URL(string: image)
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            self.postCell.userImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
            }
            ReuseCell = postCell
        }
        
        return ReuseCell
        
    }
    
    
    @objc func updateEditButton(Feed_Index: UIButton)
    {
        self.is_editing = true
        self.editIndex = Feed_Index.tag
        self.view.becomeFirstResponder()
        self.commentText = (self.data[Feed_Index.tag]["comment"] as? String)!
        self.feedDetailsTable.reloadRows(at: [IndexPath(row: self.data.count + 4, section: 0)], with: .automatic)
    }
    
    /// delete button
    
    @objc func deleteButton(Feed_Index: UIButton)
    {
        let token = self.native.string(forKey: "Token")!
        var fav = 0
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil && data.count>Feed_Index.tag
            {
                var userid = 0
                
                if self.owner[Feed_Index.tag] == "Sender"
                {
                    userid = (data[Feed_Index.tag]["user_feed_commentid"] as? Int)!
                }
                else
                {
                    userid = (data[Feed_Index.tag]["user_feed_sub_commentid"] as? Int)!
                }
                
                
                //        print("product id:", id)
                let parameters: [String:Any] = ["user_feed_commentid": userid]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
                
                let b2burl = native.string(forKey: "b2burl")!
                var url = "\(b2burl)/users/delete_feed_comment/"
                
                print("follow product", url)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                                
                            else if let json = response.result.value {
                                let jsonString = "\(json)"
                                print("res;;;;;", jsonString)
                                
                                
                                if self.data.count > Feed_Index.tag
                                {
                                    self.data.remove(at: Feed_Index.tag)
                                    
                                    if self.comment_count>0
                                    {
                                        self.comment_count = self.comment_count - 1
                                    }
                                }
                                if self.owner[Feed_Index.tag] == "Sender"
                                {
                                    if self.comment_count == 0
                                    {
                                        self.navigationController?.navigationBar.topItem?.title = "Comment"
                                    }
                                    else
                                    {
                                        self.navigationController?.navigationBar.topItem?.title = "Comment (\(self.comment_count))"  }
                                }
                                DispatchQueue.main.async {
                                    self.owner.remove(at: Feed_Index.tag)
                                    CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    self.feedDetailsTable.reloadData()
                                }
                            }
                        }}
                }
            }}
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
        
    }
    
    @objc func comment_data(sender: UIButton)
    {
        FeedViewController.user_feed_id = self.user_feedid
        
        performSegue(withIdentifier: "feedDetailsComment", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "feedDetailsComment") {
            guard let destination = segue.destination as? FeedCommentDataViewController else
            {
                print("return data")
                return
            }
            destination.user_feedid = self.user_feedid
            
        }}
    
    //    @objc func displayImage(imgaeIndex: UIButton)
    //        {
    //            native.set(self.comment_data[imgaeIndex.tag]["user_id"], forKey: "feed_userid")
    //            native.synchronize()
    //
    //            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "UserFeedPost"))! as UIViewController
    //
    //            self.navigationController?.pushViewController(editPage, animated: true)
    //
    //
    //        }
    
    @objc func userFollowButton(sender: UIButton) {
        let token = self.native.string(forKey: "Token")!
        if native.object(forKey: "feed_userid") != nil
        {
            if self.ReviewUserid != 0
            {
                CustomLoader.instance.showLoaderView()
                
                let b2burl = native.string(forKey: "b2burl")!
                var url = ""
                var fav = ""
                if( self.isFollowing == true)
                {
                    fav = "userid_to_unfollow"
                    url = "\(b2burl)/users/unfollow_user/"
                }
                else
                {
                    fav = "userid_to_follow"
                    url = "\(b2burl)/users/follow_user/"
                }
                //        print("product id:", id)
                let parameters: [String:Any] = ["\(fav)": "\(self.ReviewUserid)"]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
                
                
                print("follow user", url, parameters)
                Alamofire.request(url ,method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                                
                            else if let json = response.result.value {
                                let jsonString = "\(json)"
                                print("res;;;;;", jsonString)
                                if dict1["status"] as! String == "success"
                                {
                                    
                                    
                                    if( self.isFollowing == true)
                                    {
                                        self.isFollowing = false
                                    }
                                    else
                                    {
                                        self.isFollowing = true
                                    }
                                }
                                self.feedDetailsTable.reloadData()
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                            }
                        }}
                }
            }
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
    }
    
    /// like button
    
    @IBAction func like_button(_ sender: Any) {
        
        let token = self.native.string(forKey: "Token")!
        var fav = 0
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                
                let parameters: [String:Any] = ["user_feedid":self.user_feedid]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
                
                let b2burl = native.string(forKey: "b2burl")!
                var url = ""
                
                if self.isFeedID_liked == true
                {
                    
                    url = "\(b2burl)/users/dislike_user_feed/"
                }
                else
                {
                    url = "\(b2burl)/users/like_user_feed/"
                }
                
                print("follow product", url)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                                
                            else if let json = response.result.value {
                                let jsonString = "\(json)"
                                print("res;;;;;", jsonString)
                                
                                if self.isFeedID_liked == true
                                {
                                    if self.likeCount > 1
                                    {
                                        self.likeCount = self.likeCount - 1
                                    }
                                    else
                                    {
                                        self.likeCount = 0
                                    }
                                    self.isFeedID_liked = false
                                }
                                else
                                {
                                    self.isFeedID_liked = true
                                    self.likeCount = self.likeCount + 1
                                }
                                
                                if self.isFeedID_liked == true
                                {  self.likeButton.setImage(UIImage(named: "liked"), for: .normal)
                                }
                                else
                                {
                                    self.likeButton.setImage(UIImage(named: "like"), for: .normal)
                                    
                                }
                                if self.likeCount > 0
                                {
                                    self.likeButton.setTitle("\(self.likeCount)", for: .normal)
                                }
                                
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                DispatchQueue.main.async {
                                    self.feedDetailsTable.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                                    
                                }}
                        }}
                }
            }}
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
        
    }
    //
    
    
    
    @IBAction func menuOption(_ sender: Any) {
        if self.native.object(forKey: "userid") != nil
        {
            let userid = self.native.string(forKey: "userid")!
            if userid != ""
            {
                if Int(userid) == self.ReviewUserid
                {
                    self.myOption()
                }
                else
                {
                    self.otherUserOption()
                }
            }}
        
    }
    
    func myOption()
    {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Delete Review", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.deleteButton()
            
        })
        
        let editAction = UIAlertAction(title: "Edit Review", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("edit")
            self.editButton()
        })
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        optionMenu.addAction(saveAction)
        optionMenu.addAction(editAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func editButton()
    {
        
        UserFeedUploadViewController.is_edit = true
        UserFeedUploadViewController.user_feedid = self.user_feedid
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "feedUpload"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
        
    }
    
    /// delete button
    
    @objc func deleteButton()
    {
        print("Successfully delete post")
        let token = self.native.string(forKey: "Token")!
        var fav = 0
        if token != ""
        {
            CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                
                
                //        print("product id:", id)
                let parameters: [String:Any] = ["user_feedid":self.user_feedid]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
                
                let b2burl = native.string(forKey: "b2burl")!
                var url = "\(b2burl)/users/delete_user_feed/"
                
                print("follow product", url)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                                
                            else if let json = response.result.value {
                                let jsonString = "\(json)"
                                print("res;;;;;", jsonString)
                                
                                if (dict1["status"] as? String)! == "success"
                                {
                                    self.view.makeToast("Your review has been successfully deleted", duration: 2.0, position: .top)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                }
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                
                            }
                        }}
                }
            }}
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        
        
    }
    
    func otherUserOption()
    {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let dealReport = UIAlertAction(title: "Report Review", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("delete")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        optionMenu.addAction(dealReport)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func share_button(_ sender: Any) {
        
        // image to share
        let text = "Feed Product"
        var productImage = "thumbnail"
        
        let image = UIImage(named: productImage)
        let myWebsite = NSURL(string:self.shareLink)
        let shareAll = [text , image , myWebsite!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    
    
    func getRecommended_product(limit: Int, offset: Int)
    {
        
        //        CustomLoader.instance.showLoaderView()
        let token = self.native.string(forKey: "Token")!
        print("token9894579", token)
        let b2burl = native.string(forKey: "b2burl")!
        if native.object(forKey: "userid") != nil
        {
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed_data/?user_feedid=\(self.user_feedid)&limit=\(limit)&offset=\(offset)") as! URL)
            if token != ""
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            }
            else{
                
                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
            print("yyugdfhjb", a, token)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                debugPrint(response)
                var url =
                    print("feed url recommended", a, token)
                
                
                switch response.result {
                case .success:
                    
                    
                    if let result1 = response.result.value
                    {
                        let result = response.result
                        
                        print("result feed video", response)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            else if dict1["status"] as! String == "success"
                            {
                                if dict1["feed_data"] is NSNull
                                {}
                                else
                                {
                                    if let innerdict = dict1["feed_data"] as? AnyObject
                                    {
                                        
                                        var feed_users = [AnyObject]()
                                        if innerdict.count > 0
                                        {
                                            
                                            var data = [[AnyObject]]()
                                            
                                            if innerdict["feed_text"] is NSNull
                                            {
                                                
                                            }
                                            else
                                            {
                                                self.QuestionText = (innerdict["feed_text"] as? String)!
                                            }
                                            
                                            if innerdict["feed_share_link"] is NSNull
                                            {
                                                
                                            }
                                            else
                                            {
                                                self.shareLink = (innerdict["feed_share_link"] as? String)!
                                            }
                                            if innerdict["comment_count"] is NSNull
                                            {
                                                
                                            }
                                            else if let count = innerdict["comment_count"] as? Int
                                            {
                                                self.comment_count = count
                                                
                                            }
                                            if innerdict["comment_data"] is NSNull
                                            {
                                                
                                            }
                                            else if let commentdata = innerdict["comment_data"] as? [AnyObject]
                                            {
                                                
                                                for var i in 0..<commentdata.count
                                                {
                                                    self.owner.append("Sender")
                                                    self.data.append((commentdata[i] as? AnyObject)!)
                                                    if commentdata[i]["reply_comment_data"] is NSNull
                                                    {
                                                        
                                                    }
                                                    else if let replyComment
                                                        = commentdata[i]["reply_comment_data"] as? [AnyObject]
                                                    {
                                                        for var j in 0..<replyComment.count
                                                        {
                                                            self.owner.append("Receiver")
                                                            self.data.append((replyComment[j] as? AnyObject)!)
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                            if innerdict["like_count"] is NSNull
                                            {}
                                            else if let count = innerdict["like_count"] as? Int
                                            {
                                                self.likeCount = count
                                            }
                                            
                                            if innerdict["like_data"] is NSNull
                                            {
                                                self.isFeedID_liked=false
                                            }
                                            else
                                            { feed_users.append(innerdict["like_data"] as! AnyObject)
                                                
                                            }
                                            
                                            if self.native.object(forKey: "userid") != nil
                                            {
                                                let userid = self.native.string(forKey: "userid")!
                                                if feed_users.count > 0
                                                {
                                                    if userid != ""
                                                    {
                                                        var data = feed_users[0] as! AnyObject
                                                        if data.contains(Int(userid))
                                                        {
                                                            self.isFeedID_liked  = true
                                                            print("isFeedID_liked", "true")
                                                        }
                                                        else
                                                        {  self.isFeedID_liked=false
                                                            
                                                        }
                                                        
                                                    }
                                                    else
                                                    {
                                                        self.isFeedID_liked=true
                                                    }
                                                }}
                                            if self.isFeedID_liked == true
                                            {  self.likeButton.setImage(UIImage(named: "liked"), for: .normal)
                                            }
                                            else
                                            {
                                                self.likeButton.setImage(UIImage(named: "like"), for: .normal)
                                                
                                            }
                                            if self.likeCount > 0
                                            {
                                                self.likeButton.setTitle("\(self.likeCount)", for: .normal)
                                            }
                                            
                                            
                                            if innerdict["product_url"] is NSNull
                                            {}
                                            else if let feed_share_link = innerdict["product_url"] as? String
                                            {
                                                self.feed_share_link = feed_share_link
                                            }
                                            if innerdict["feed_tag_data"] is NSNull
                                            {}
                                            else if let feed_user_data = innerdict["feed_tag_data"] as? [AnyObject]
                                            {
                                                if feed_user_data[0]["tagid"] is NSNull
                                                {}
                                                else if let id = feed_user_data[0]["tagid"] as? Int
                                                {
                                                    self.categoryid = id
                                                }
                                                
                                                if feed_user_data[0]["tag"] is NSNull
                                                {}
                                                else if let idtext = feed_user_data[0]["tag"] as? String
                                                {
                                                    self.categoryText = idtext
                                                }
                                            }
                                            if innerdict["feed_user_data"] is NSNull
                                            {}
                                            else if let feed_user_data = innerdict["feed_user_data"] as? AnyObject
                                            {
                                                if feed_user_data["is_following"] is NSNull
                                                {}
                                                else if let is_following = feed_user_data["is_following"] as? Int
                                                {
                                                    if is_following == 0
                                                    {
                                                        self.isFollowing = false
                                                    }
                                                    else
                                                    {
                                                        self.isFollowing = true
                                                        
                                                    }
                                                    
                                                    
                                                }
                                                
                                                if feed_user_data["userid"] is NSNull
                                                {}
                                                else if let userid = feed_user_data["userid"] as? Int
                                                {
                                                    self.ReviewUserid = userid
                                                }
                                                if feed_user_data["profile_pic_url"] is NSNull
                                                {}
                                                else if let profile_pic_url = feed_user_data["profile_pic_url"] as? String
                                                {
                                                    self.userImage = profile_pic_url
                                                }
                                                if feed_user_data["firebase_userid"] is NSNull
                                                {}
                                                else if let firebase_userid = feed_user_data["firebase_userid"] as? String
                                                {
                                                    self.userFirbaseID = firebase_userid
                                                }
                                                
                                                var firstname = ""
                                                var lastname = ""
                                                if feed_user_data["first_name"] is NSNull
                                                {}
                                                else if let name = feed_user_data["first_name"] as? String
                                                {
                                                    firstname = name
                                                }
                                                if feed_user_data["last_name"] is NSNull
                                                {}
                                                else if let last = feed_user_data["last_name"] as? String
                                                {
                                                    lastname = last
                                                }
                                                self.userName = "\(firstname) \(lastname)"
                                                
                                            }
                                            if innerdict["feed_uploaded"] is NSNull
                                            {}
                                            else if let uploadtime =  innerdict["feed_uploaded"] as? String
                                            {
                                                self.uploadTime = (uploadtime as! NSString).doubleValue
                                            }
                                            if innerdict["label"] is NSNull
                                            {
                                                
                                            }
                                            else
                                            {
                                                self.tagText = (innerdict["label"] as? String)!
                                            }
                                            if innerdict["title"] is NSNull
                                            {}
                                            else if let title =  innerdict["title"] as? String
                                            {
                                                self.titletext = title
                                            }
                                            if innerdict["media_url"] is NSNull
                                            {}
                                            else
                                            {
                                                if let data = innerdict["media_url"] as? AnyObject
                                                {
                                                    if let imagedata = data["image"] as? [AnyObject]
                                                    {
                                                        
                                                        if imagedata.count > 0
                                                        { self.feedImage = (imagedata[0] as? String)!
                                                        }
                                                        
                                                    }
                                                    else if let videodata = data["image"] as? String
                                                    { self.feedImage = (data["image"] as? String)!
                                                    }
                                                    
                                                }
                                                if let data = innerdict["media_url"] as? AnyObject
                                                {
                                                    if let imagedata = data["mp4"] as? [AnyObject]
                                                    {
                                                        if imagedata.count > 0
                                                        { self.feedVideo = (imagedata[0] as? String)!
                                                        }
                                                        
                                                    }
                                                    else if let videodata = data["mp4"] as? String
                                                    { self.feedVideo = (data["mp4"] as? String)!
                                                    }
                                                }
                                                
                                            }
                                            
                                        }
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        
                                        
                                        
                                    }
                                }
                            }}}
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                }
                if self.feed_share_link != ""
                {
                    self.buttonConstraint.constant = 200
                }
                else
                {
                    self.buttonConstraint.constant = 0
                }
                DispatchQueue.main.async {
                    self.feedDetailsTable.isHidden = false
                    //                self.refreshControl.endRefreshing()
                    self.feedDetailsTable.reloadData()
                }
            }
            
            DispatchQueue.main.async {
                self.feedDetailsTable.isHidden = false
                //                    self.refreshControl.endRefreshing()
                self.feedDetailsTable.reloadData()
                //                    self.refreshControl.endRefreshing()
                CustomLoader.instance.hideLoaderView()
            }
            UIApplication.shared.endIgnoringInteractionEvents()
            
            
        }
        else
        {
            //                self.refreshControl.endRefreshing()
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
        }
        
        
    }
    
    
    
    //    /// post comment
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    //    {
    //         //cell = tbl.dequeueReusableCell(withIdentifier: "CELL") as! TableViewCell
    //
    //
    //       print( postCell.returnTextOfTextField() )
    //        print(postCell.comment.text)
    //        self.commentText = postCell.comment.text!
    //        postCell.comment.resignFirstResponder()
    //        self.postComment()
    //        return true
    //    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        if replyStatus == false && postCell.comment.text!.count > 0
        {
            self.commentText = postCell.comment.text!
            self.sendComment()
        }
        else
        {
            
            self.replyComment()
        }
        
        return true
        
    }
    
    func sendComment() {
        
        if postCell.comment.text! != ""
        {
            self.view.resignFirstResponder()
            
            let token = self.native.string(forKey: "Token")!
            var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
                if self.native.object(forKey: "userid") != nil
                {
                    let userid = self.native.string(forKey: "userid")!
                    
                    var user_feedid = "0"
                    if self.native.object(forKey: "FavProID") != nil
                    {
                        let id = native.string(forKey: "FavProID")
                        user_feedid = id!
                    }
                    //        print("product id:", id)
                    let parameters: [String:Any] = ["user_feedid":user_feedid,"comment": postCell.comment.text!]
                    let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                    print("Successfully post")
                    
                    let b2burl = native.string(forKey: "b2burl")!
                    var url = "\(b2burl)/users/add_comment_to_feed/"
                    
                    
                    print("comment product", url, parameters)
                    Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                        
                        
                        guard response.data != nil else { // can check byte count instead
                            let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                            UIApplication.shared.endIgnoringInteractionEvents()
                            return
                        }
                        print("resonce comment", response)
                        
                        switch response.result
                        {
                            
                            
                        case .failure(let error):
                            //                print(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                print("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                print(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                print("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                        case .success:
                            let result = response.result
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                    
                                else if let json = response.result.value {
                                    let jsonString = "\(json)"
                                    
                                    
                                    var first_name = ""
                                    var last_name = ""
                                    var profile_pic_url = "";
                                    var user_feed_commentid = 0;
                                    var user_id = 0
                                    
                                    if dict1["response"] is NSNull
                                    {}
                                    else
                                    {
                                        if let commentData = dict1["response"] as? AnyObject
                                        {
                                            if let data = commentData["response"]
                                            {
                                                if commentData["response"] is NSNull
                                                {}
                                                else
                                                {
                                                    user_feed_commentid = (commentData["response"] as? Int)!
                                                }
                                            }
                                        }
                                    }
                                    
                                    
                                    
                                    if self.native.object(forKey: "userid") != nil
                                    {
                                        user_id = Int(self.native.string(forKey: "userid")!)!
                                    }
                                    
                                    if self.native.object(forKey: "profilepic") != nil
                                    {
                                        profile_pic_url = self.native.string(forKey: "profilepic")!
                                    }
                                    if self.native.object(forKey: "firstname") != nil
                                    {
                                        first_name = self.native.string(forKey: "firstname")!
                                    }
                                    if self.native.object(forKey: "lastname") != nil
                                    {
                                        last_name = self.native.string(forKey: "lastname")!
                                    }
                                    if self.native.object(forKey: "userid") != nil
                                    {
                                        last_name = self.native.string(forKey: "userid")!
                                    }
                                    if self.native.object(forKey: "lastname") != nil
                                    {
                                        last_name = self.native.string(forKey: "lastname")!
                                    }
                                    
                                    var timestamp = NSDate().timeIntervalSince1970
                                    var allComment = self.data
                                    self.data.removeAll()
                                    
                                    self.data.append(["comment": self.postCell.comment!.text!,
                                                      "first_name": first_name,
                                                      "inserted_at": "\(timestamp)",
                                        "last_name": last_name,
                                        "profile_pic_url": "\(profile_pic_url)",
                                        "user_feed_commentid": user_feed_commentid,
                                        "user_id": user_id] as AnyObject)
                                    
                                    for var i in 0..<allComment.count
                                    {
                                        self.data.append(allComment[i])
                                    }
                                    
                                    
                                    self.comment_count = self.comment_count + 1
                                    
                                    //                    self.owner.append("Receiver")
                                    let ownerData = self.owner
                                    self.owner.removeAll()
                                    
                                    self.owner.append("Sender")
                                    for var i in 0..<ownerData.count
                                    {
                                        self.owner.append(ownerData[i])
                                    }
                                    if self.comment_count == 0
                                    {
                                        self.navigationController?.navigationBar.topItem?.title = "Comment"
                                    }
                                    else
                                    {
                                        self.navigationController?.navigationBar.topItem?.title = "Comment (\(self.comment_count))"                                }
                                    self.postCell.comment!.text! = ""
                                    
                                    DispatchQueue.main.async {
                                        self.feedDetailsTable.reloadData()
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        self.feedDetailsTable.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                                    }
                                    
                                }
                            }}
                    }
                }}
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
            
            
        }
        else
        {
            self.view.makeToast("Please type comment first.", duration: 2.0, position: .center)
        }
    }
    
    func replyComment() {
        self.hideSecondResponder()
        if self.commentText2!.text! != ""
        {
            self.postCell.comment!.resignFirstResponder()
            self.view.resignFirstResponder()
            let token = self.native.string(forKey: "Token")!
            var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
                print(self.data.count, editIndex)
                let parameters: [String:Any] = ["user_feed_commentid": (data[editIndex]["user_feed_commentid"] as? Int)!,"reply_text": self.postCell.comment!.text!]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
                
                let b2burl = native.string(forKey: "b2burl")!
                var url = "\(b2burl)/users/reply_to_comment/"
                
                
                print("comment product", url, parameters)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    print("resonce comment", response)
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                                
                            else if let json = response.result.value {
                                let jsonString = "\(json)"
                                
                                
                                var first_name = ""
                                var last_name = ""
                                var profile_pic_url = "";
                                var user_feed_commentid = 0;
                                var user_id = 0
                                
                                if dict1["response"] is NSNull
                                {}
                                else
                                {
                                    
                                }
                                
                                
                                
                                self.commentText2!.text! = ""
                                
                                DispatchQueue.main.async {
                                    self.feedDetailsTable.resignFirstResponder()
                                    self.replyStatus=false
                                    self.data.removeAll()
                                    self.owner.removeAll()
                                    self.getRecommended_product(limit: self.limit, offset: self.offset)
                                    CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    
                                }
                                
                            }
                        }}}}
                
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
        else
        {
            self.view.makeToast("Please type comment first.", duration: 2.0, position: .center)
        }
        
        
    }
    
    @IBAction func contactUser(_ sender: Any) {
        
        let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            CustomLoader.instance.showLoaderView()
            self.phyzioChat = true
            if self.userFirbaseID != "" && self.userName != ""
            {if  native.object(forKey: "firebaseid") != nil
            {
                if self.userFirbaseID == native.string(forKey: "firebaseid")!
                {
                    UIApplication.shared.endIgnoringInteractionEvents()
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your profile", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    native.set("1", forKey: "currentuserid")
                    native.synchronize()
                    var chatroom = CheckChatRoomViewController()
                    
                    chatroom.chekChatRoom(shopid: userFirbaseID, shopname: userName, shopProfilePic: userImage)
                    //
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                        // Your code with delay
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                        self.performSegue(withIdentifier: "ReviewChat", sender: self)
                    }
                }
            }
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                }
                
            }
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
}
