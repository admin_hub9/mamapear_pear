//
//  ReviewDescriptionTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 02/06/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class ReviewDescriptionTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var descriptionText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
