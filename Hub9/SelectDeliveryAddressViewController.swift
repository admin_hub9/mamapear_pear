////
////  SelectDeliveryAddressViewController.swift
////  Hub9
////
////  Created by Deepak on 10/16/18.
////  Copyright © 2018 Deepak. All rights reserved.
////
//
//import UIKit
//import Alamofire
//
//
//class SelectDeliveryAddressViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
//    
//    var selectindex = 0
//    var addresscount = 0
//    var native = UserDefaults.standard
//    var selectAddress = [AnyObject]()
//    
//    @IBOutlet weak var AddressTable: UITableView!
//    
//    override func viewDidAppear(_ animated: Bool) {
//        getAddressData()
//    }
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//    }
//    
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row < selectAddress.count
//        {
//        selectindex = indexPath.row
//        AddressTable.reloadData()
//        }
//        else if indexPath.row  == selectAddress.count
//        {
//            
//            print("--------")
//            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "addnewaddress"))! as UIViewController
//            
//            self.present(editPage, animated: false, completion: nil)
////            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
////            let vc = storyboard.instantiateViewController(withIdentifier: "addnewaddress") as UIViewController!
////            navigationController?.popToRootViewController(animated: true)
//            
//           
//           
//            
//        }
//    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if selectindex == indexPath.row
//        {
//            return 160
//        }
//        else if selectAddress.count == indexPath.row
//        {
//            return 50
//        }
//        else
//        {
//            return 100
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return selectAddress.count + 1
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        var cellToReturn = UITableViewCell()
//        if indexPath.row < selectAddress.count  {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "selectaddress", for: indexPath) as! DeliveryaddressTableViewCell
//            cell.username.text = self.selectAddress[indexPath.row]["consignee_name"] as? String
//            cell.address.text = self.selectAddress[indexPath.row]["address1"] as? String
//            cell.citystatepin.text = String(((self.selectAddress[indexPath.row]["city_name"] as? String)!) + " " + ((self.selectAddress[indexPath.row]["state_name"] as? String)!) + " " + ((self.selectAddress[indexPath.row]["pincode"] as? String)!))
//            cell.country.text = self.selectAddress[indexPath.row]["country_name"] as? String
//            
//            if selectindex == indexPath.row
//            {
//                cell.selectedaddress.image = UIImage(named: "checkmark")
//                cell.selectedAddress.isHidden = false
//                cell.editbutton.isHidden = false
//                cell.selectedAddress.tag = indexPath.row
//                cell.editbutton.tag = indexPath.row
//                cell.selectedAddress.addTarget(self, action: #selector(sendthisAddress(sender:)), for: .touchUpInside)
//            }
//            else
//            {
//                cell.selectedaddress.image = UIImage(named: "Uncheckmark")
//                cell.selectedAddress.isHidden = true
//                cell.editbutton.isHidden = true
//            }
//            
//            cellToReturn = cell
//        }
//        else   {
//            
//            let cell = tableView.dequeueReusableCell(withIdentifier: "addnewaddress", for: indexPath) as! DeliveryaddressTableViewCell
//            
//            cellToReturn = cell
//        }
//    return cellToReturn
//    }
//    
//    @objc func sendthisAddress(sender: UIButton)
//    {
//        
//        var quotationitemid = native.string(forKey: "quotationitemid")!
//        if quotationitemid != ""
//        {
//            Buyer_Approved_order(itemid: quotationitemid, index: sender.tag)
//        }
//        
//    }
//    
//    
//    
//    ////get all address
//    func getAddressData()
//    { var Token = self.native.string(forKey: "Token")!
//        print(Token)
//        if Token != nil
//        {
//            
//            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/saved_address/") as! URL)
//            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
//            
//            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
//                //                                debugPrint(response)
//                print("response: \(response)")
//                let result = response.result
//                if let dict1 = result.value as? Dictionary<String, AnyObject>{
//                    let innerdict = dict1["data"] as? [AnyObject]
//                    if innerdict!.count > 0
//                        {
//                            self.selectAddress = innerdict!
//                        
//                    }
//                    else
//                    {
//                        let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//                        let vc = storyboard.instantiateViewController(withIdentifier: "addnewaddress") as UIViewController!
//                        self.navigationController?.pushViewController(vc!, animated: true)
//                    }
//                }
//                print(self.selectAddress)
//                self.AddressTable.reloadData()
//            }
//            
//        }}
//    
//
//    func Buyer_Approved_order(itemid:String ,index: Int)
//    {
//        
//            var Token = native.string(forKey: "Token")!
//            if Token != nil
//            {
//                let header = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
//                print("Successfully post")
//                let address = self.selectAddress[index]["addressid"]!
//                let address_lkpid:[String:Any]=["addresspid": address!]
//                let parameters: [String:Any] = ["quotation_itemid": itemid, "quotation_item_status_lkpid": 4,"payment_mode_lkpid":1, "address_data": address_lkpid]
//                print( parameters)
//                
//                Alamofire.request("https://demo.hub9.io/api/buyer_edit_quotation/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
//                    //                                debugPrint(response)
//                    print("response shop: \(response)")
//                    let result = response.result
//                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
//                        if let invalidToken = dict1["detail"]{
//                            if invalidToken as! String  == "Invalid token."
//                            { // Create the alert controller
//                                let alertController = UIAlertController(title: "Alert", message: "Invalid Token Found", preferredStyle: .alert)
//                                
//                                // Create the actions
//                                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
//                                    UIAlertAction in
//                                    NSLog("OK Pressed")
//                                    let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "root"))! as UIViewController
//                                    
//                                    self.present(editPage, animated: false, completion: nil)
//                                }
//                                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
//                                    UIAlertAction in
//                                    NSLog("Cancel Pressed")
//                                }
//                                
//                                // Add the actions
//                                alertController.addAction(okAction)
//                                alertController.addAction(cancelAction)
//                                
//                                // Present the controller
//                                self.present(alertController, animated: true, completion: nil)
//                            }
//                            
//                        }
//                        else if let status = dict1["status"]{
//                            if status as! String  == "success"
//                            {
//                                self.dismiss(animated: true, completion: nil)
//                            }
//                            else
//                            {
//                                let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertControllerStyle.alert)
//                                alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
//                                self.present(alert, animated: true, completion: nil)
//                            }
//                        }
//                        
//                    }
//                    
//                    
//                }}
//        
//    }
//}
