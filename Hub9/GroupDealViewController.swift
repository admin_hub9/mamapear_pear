//
//  GroupDealViewController.swift
//  Hub9
//
//  Created by Deepak on 5/24/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

var GroupDealView = GroupDealViewController()

class GroupDealViewController: UIViewController, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    var native = UserDefaults.standard
    
    @IBOutlet weak var alertContainer: UIView!
    @IBOutlet weak var timer: UILabel!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var otherImg: RoundedImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        GroupDealView = self
        joinButton.layer.cornerRadius = 5
        joinButton.clipsToBounds = true
        alertContainer.layer.cornerRadius = 8
        alertContainer.clipsToBounds = true
        var otherUserImg = ""
        if BBProductDetails.selectedCell >= 0
        {
            
            if BBProductDetails.ProductDeal[BBProductDetails.selectedCell]["user_name"] is NSNull
            {}
            else
            {
                print(BBProductDetails.ProductDeal[BBProductDetails.selectedCell]["user_name"] as! String)
                groupName.text = "Join \(BBProductDetails.ProductDeal[BBProductDetails.selectedCell]["user_name"] as! String) Group"
                if BBProductDetails.ProductDeal[BBProductDetails.selectedCell]["profile_pic_url"] is NSNull
                {}
                else{
                otherUserImg = BBProductDetails.ProductDeal[BBProductDetails.selectedCell]["profile_pic_url"] as! String
                if otherUserImg != nil{
                    let url = URL(string: otherUserImg)
                    
                    if url != nil
                    {
                        
                        print("bhecsvhjhjdbjurl", url)
                        DispatchQueue.main.async {
                            self.otherImg.sd_setImage(with: URL(string: otherUserImg), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
                }
            }
            
        let image = native.string(forKey: "profilepic")!
            print("image", image)
        if image != nil{
            let url = URL(string: image)
            
//            let boderColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
            
            if url != nil
            {
                DispatchQueue.main.async {
                    self.userImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                }
            }
        }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func joinClick(_ sender: Any) {
        BBProductDetails.order_typeid = 2
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "updateVariant") as! UpdateProductVariantViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
    }
    
    @IBAction func cancelAlert(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
}
