//
//  ProductCollectionViewController.swift
//  Hub9
//
//  Created by Deepak on 5/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import Cosmos
import AttributedTextView

var product = ProductCollectionViewController()
class ProductCollectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    
    var native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    
    
    var policy = ""
    var banner = [String]()
    var feature_name = [String]()
    var productImage = [AnyObject]()
    
    var ProductName = [String]()
    var ProductMOQ = [String]()
    var Loaction = [String]()
    var ProductPrice = [NSNumber]()
    var retailPrice = [NSNumber]()
    var shop_Product = [AnyObject]()
    var Currency = ""
    var variantID = [String]()
    var brand = ""
    var fav = 0
    var bannerIndex = 0
    
    
    
    
    @IBOutlet weak var shopImage: RoundedImageView!
    @IBOutlet weak var shopname: UILabel!
    @IBOutlet weak var shopaddress: UILabel!
    @IBOutlet weak var rating: CosmosView!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var shopProduct: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var ratingNumber: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var segmentView: UIView!
    
    
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func segmentTab(_ sender: Any) {
        segmentControl.changeUnderline()
        switch segmentControl.selectedSegmentIndex
        {
        case 0:
            var indexPath = NSIndexPath(row: 0, section: 0)
            self.shopProduct.scrollToRow(at: indexPath as IndexPath,
                                         at: UITableView.ScrollPosition.none, animated: true)
            
        //show Product view
        case 1:
            var indexPath = NSIndexPath(row: 2, section: 0)
            self.shopProduct.scrollToRow(at: indexPath as IndexPath,
                                         at: UITableView.ScrollPosition.none, animated: true)
        //show Shop view
        case 2:
            var indexPath = NSIndexPath(row: 3, section: 0)
            self.shopProduct.scrollToRow(at: indexPath as IndexPath,
                                         at: UITableView.ScrollPosition.none, animated: true)
        //show Shop view
        default:
            break;
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomLoader.instance.showLoaderView()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        
        getfav_product()
        self.shopProduct.isHidden=true
        self.headerView.isHidden=true
        self.segmentView.isHidden=true
        segmentControl.addUnderlineForSelectSegment()
        segmentControl.backgroundColor = UIColor.lightGray
        segmentControl.removeBorder()
        shopProduct.estimatedRowHeight = 68.0
        shopProduct.rowHeight = UITableViewAutomaticDimension
        shopProduct.tableFooterView = UIView()
        product = self
        
        parseData()
        
        
        let greencolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
        followButton.layer.cornerRadius = followButton.frame.height / 2
        followButton.layer.borderWidth = 0.8
        followButton.layer.borderColor = greencolor.cgColor
        followButton.clipsToBounds = true
        
        
    }
    
    func parseData()
    {
        let token = self.native.string(forKey: "Token")!
        
        let shopid = native.string(forKey: "entershopid")!
        let b2burl = native.string(forKey: "b2burl")!
        var a = URLRequest(url: NSURL(string: "\(b2burl)/search/shop_products/?shopid=\(shopid)&limit=\(10)&offset=\(0)")! as URL)
        if token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        debugPrint("url", a)
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            //                                                debugdebugPrint(response)
            debugPrint("response: \(response)")
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if let innerdict = dict1["data"] as? [AnyObject]{
                        debugPrint("inner dict shop", innerdict)
                        
                        if innerdict.count > 0
                        {
                            self.shop_Product = innerdict
                            
                            let bannerimg = innerdict[0]["banner_url"] as! AnyObject
                            if var imageurl = bannerimg["mobile_image_url"] as? [AnyObject]
                            {
                                for i in 0..<imageurl.count
                                {
                                    self.banner.append(imageurl[i] as! String)
                                }}
                            self.brand = innerdict[0]["brand_name"] as! String
                            if innerdict[0]["shop_logo_url"] is NSNull
                            {}
                            else
                            {
                                var shoplogo = (innerdict[0]["shop_logo_url"] as? String)!
                                if shoplogo != nil
                                {
                                    let url = NSURL(string:shoplogo as! String )
                                    if url != nil{
                                        DispatchQueue.main.async {
                                            self.shopImage.sd_setImage(with: URL(string: shoplogo), placeholderImage: UIImage(named: "thumbnail"))
                                        }
                                    }
                                }
                            }
                            self.shopname.text = (innerdict[0]["shop_name"] as? String)!
                            if innerdict[0]["shop_location"] is NSNull{
                                
                            }
                            else
                            {
                                self.shopaddress.text = (innerdict[0]["shop_location"] as? String)!
                            }
                            if innerdict[0]["shop_ratingc"] is NSNull{
                                self.rating.isHidden = true
                            }
                            else
                            {
                                self.rating.isHidden = false
                                if let data = innerdict[0]["shop_ratingc"] as? NSNumber
                                {
                                    self.ratingNumber.text = "\((innerdict[0]["shop_ratingc"] as? NSNumber)!)"
                                }
                            }
                            
                            for i in 0..<innerdict.count {
                                //                        debugPrint(innerdict[i]["name_json"] as! NSDictionary, "name")
                                let data = innerdict[i]["name_json"] as! AnyObject
                                let name = innerdict[i]["title"] as! String
                                if innerdict[i]["feature_name"] is NSNull
                                {
                                    self.feature_name.append("")
                                }
                                else
                                {
                                    self.feature_name.append(innerdict[i]["feature_name"] as! String)
                                }
                                
                                let shopname = innerdict[i]["shop_name"] as! String
                                var statename = ""
                                if innerdict[i]["state_name"] is NSNull
                                {}
                                else
                                {
                                    statename = (innerdict[i]["state_name"] as? String)!
                                }
                                
                                if let product_variant_data = innerdict[i]["product_variant_data"] as? [AnyObject]
                                {
                                    //                                for i in 0..<product_variant_data.count
                                    //                                {
                                    if let product_variant_size_data = product_variant_data[0]["product_variant_size_data"] as? [AnyObject]
                                    {
                                        if product_variant_size_data.count > 0
                                        {
                                            self.ProductName.append(name as String)
                                            
                                            let imagedir = (product_variant_size_data[0]["variant_image_url"] as? AnyObject)!
                                            
                                            self.productImage.append((imagedir["200"] as? AnyObject)! )
                                            //                                            self.ProductMOQ.append(String((product_variant_size_data[0]["min_order_qty"] as? Int)!))
                                            if product_variant_size_data[0]["currency_symbol"] is NSNull
                                            {}
                                            else
                                            {
                                                let myInteger = product_variant_size_data[0]["currency_symbol"] as! NSString
                                                //                                    if let myUnicodeScalar = UnicodeScalar(myInteger) {
                                                //                                        let myString = String(myUnicodeScalar)
                                                self.Currency = "\(myInteger)"
                                                //                                    }
                                                
                                            }
                                            self.ProductPrice.append(product_variant_size_data[0]["deal_price"] as! NSNumber)
                                            self.retailPrice.append(product_variant_size_data[0]["retail_price"] as! NSNumber)
                                            self.variantID.append(String((product_variant_size_data[0]["product_variantid"] as? Int)!))
                                            if statename != ""
                                            {
                                                self.Loaction.append(statename)
                                            }
                                            
                                        }
                                    }
                                    //                                }
                                }
                            }
                        }
                    }
                }}
            self.parseDescription()
            self.shopProduct.isHidden = false
            self.headerView.isHidden=false
            self.segmentView.isHidden=false
            self.shopProduct.reloadData()
        }
        //        }
        
    }
    
    func getfav_product()
    {
        let shopid = native.string(forKey: "entershopid")!
        
        //        debugPrint("product id:", id)
        
        let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            let header = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            debugPrint("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            
            var url = "\(b2burl)/users/get_fav_shop/?shopid=\(shopid)"
            debugPrint("url", url)
            
            Alamofire.request(url, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                if let result1 = response.result.value
                {
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                debugPrint(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            debugPrint("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            debugPrint(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            debugPrint("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                            else if dict1["status"] as! String == "success"
                            {
                                if let data = dict1["data"] as? Int
                                {
                                    debugPrint("oppoipoi", data)
                                    self.fav = data
                                    
                                    if self.fav == 0
                                    {
                                        self.followButton.setTitle("Follow", for: .normal)
                                        
                                    }
                                    else
                                    {
                                        self.followButton.setTitle("Unfollow", for: .normal)
                                    }
                                }
                            }
                            
                            
                            
                        }
                    }}
            }
        }
    }
    
    @IBAction func followButton(_ sender: Any) {
        CustomLoader.instance.showLoaderView()
        let shopid = native.string(forKey: "entershopid")!
        let parameters: [String:Any] = ["shopid":shopid]
        //        debugPrint("userid:", native.string(forKey: "userid")!)
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            let header = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            debugPrint("Successfully post")
            var url = ""
            let b2burl = native.string(forKey: "b2burl")!
            if( self.fav == 1)
            {
                url = "\(b2burl)/users/unfollow_shop/"
            }
            else
            {
                url = "\(b2burl)/users/follow_shop/"
            }
            debugPrint("active url", url)
            Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                guard response.data != nil else { // can check byte count instead
                    //                self.blurView.alpha = 0
                    //                self.ActivityGravity.stopAnimating()
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if let json = response.result.value {
                            let jsonString = "\(json)"
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            debugPrint("jsonString", jsonString)
                            if self.fav == 1
                            {
                                self.fav = 0
                                self.followButton.setTitle("Follow", for: .normal)                            }
                            else
                            {
                                self.fav = 1
                                self.followButton.setTitle("Unfollow", for: .normal)
                            }
                            
                        }
                    }
                    
                }}
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    func parseDescription()
    {
        CustomLoader.instance.showLoaderView()
        let token = self.native.string(forKey: "Token")!
        
        let shopid = native.string(forKey: "entershopid")!
        let b2burl = native.string(forKey: "b2burl")!
        var a = URLRequest(url: NSURL(string: "\(b2burl)/shop/shop_privacy_policy/?shopid=\(shopid)&limit=10&offset=0")! as URL)
        if token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            //                                                debugdebugPrint(response)
            //                debugPrint("response privacy: \(response)")
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    else if dict1["status"] as! String  == "success"
                    {
                        if let innerdict = dict1["data"] as? AnyObject{
                            //                        debugPrint("inner dict", innerdict)
                            if innerdict["privacy_policy"] is NSNull
                            {}
                            else
                            {
                                self.policy = innerdict["privacy_policy"] as! String
                            }
                            
                            
                        }
                    }}}
            let indexPath = NSIndexPath(row: 3, section: 0)
            self.shopProduct.reloadData()
            
            self.view.layoutIfNeeded()
            
            
        }
        
        
        //        }
        
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = self.shopProduct.contentOffset
        visibleRect.size = self.shopProduct.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = self.shopProduct.indexPathForRow(at: visiblePoint) else { return }
        
        debugPrint(indexPath.row)
        if indexPath.row == 0 
        {
            self.segmentControl.selectedSegmentIndex = 0
        }
        else if indexPath.row == 2
        {
            self.segmentControl.selectedSegmentIndex = 1
        }
        else if indexPath.row == 3
        {
            self.segmentControl.selectedSegmentIndex = 2
        }
        self.segmentControl.changeUnderline()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        var visibleRect = CGRect()
        
        visibleRect.origin = self.shopProduct.contentOffset
        visibleRect.size = self.shopProduct.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = self.shopProduct.indexPathForRow(at: visiblePoint) else { return }
        
        if indexPath.row == 0 || indexPath.row == 1
        {
            self.segmentControl.selectedSegmentIndex = 0
        }
        if indexPath.row == 2
        {
            self.segmentControl.selectedSegmentIndex = 1
        }
        else if indexPath.row == 3
        {
            self.segmentControl.selectedSegmentIndex = 2
        }
        self.segmentControl.changeUnderline()
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            var number = 0
            
            
            if self.productImage.count < 10
            {
                
                if self.productImage.count % 2 == 0
                {
                    number = self.productImage.count / 2
                }
                else
                {
                    number = (self.productImage.count / 2 ) + 1
                }
                
                
                return (CGFloat(280 * number ))
                
            }
            else
            {
                return (CGFloat(280 * 5 ))
            }
        }
        else if indexPath.row == 1
        {
            if self.productImage.count < 10
            {
                return 0
            }else
            {
                return 50
            }
        }
        else if indexPath.row == 2
        {
            return 200
        }
        else if indexPath.row == 3
        {
            if self.policy != ""
            {
                return UITableViewAutomaticDimension
            }
            else
            {
                return 0
            }
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1
        {
            CustomLoader.instance.showLoaderView()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shopProducts"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReuseCell = UITableViewCell()
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "shopTopProduct", for: indexPath) as! ShopTopProductsTableViewCell
            cell.shopProductCollection.tag = indexPath.row
            cell.shopProductCollection.reloadData()
            
            ReuseCell = cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "viewmoretable", for: indexPath) as! ViewMoreTableViewCell
            ReuseCell = cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "shopAbout", for: indexPath) as! ShopAboutTableViewCell
            cell.shopBannerCollection.tag = indexPath.row
            cell.shopBannerCollection.reloadData()
            cell.pageindex.numberOfPages = self.banner.count
            cell.pageindex.currentPage = bannerIndex
            ReuseCell = cell
        }
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "shopPoilicies", for: indexPath) as! ShopPoliciesTableViewCell
            var contentHTML =  policy
            debugPrint("description", contentHTML)
            contentHTML = contentHTML.replacingOccurrences(of: "<br><br>", with: "<br>")
            contentHTML = contentHTML.replacingOccurrences(of: "</li><br>", with: "</li>")
            contentHTML = contentHTML.replacingOccurrences(of: "</li><br></ul>", with: "</li></ul>")
            cell.ShopPolicies.attributedText = NSAttributedString(html: contentHTML)
            //            attributedText = NSAttributedString(html: "\(policy)")
            ReuseCell = cell
        }
        return ReuseCell
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0
        {
            if productImage.count <= 10
            {
                return productImage.count
            }
            else
            {
                return 10
            }
        }
        else if collectionView.tag == 2
        {
            return self.banner.count
        }
        else
        {
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0
        {
            var width = 0.0
            var height = 0.0
            width = Double(CGFloat(screenWidth / 2 - 15))
            
            return CGSize(width: width, height: 260)
        }
        else if collectionView.tag == 2
        {
            var width = 0.0
            var height = 0.0
            width = Double(CGFloat(screenWidth - 10))
            height = Double(CGFloat(screenHeight - 10))
            return CGSize(width: width, height: height )
        }
        else
        {
            return CGSize(width: 0, height: 0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var ReuseCell = UICollectionViewCell()
        
        if collectionView.tag == 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
            cell.clipsToBounds = true
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            cell.contentView.layer.cornerRadius = 5
            cell.contentView.layer.borderWidth = 1.0
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            cell.layer.shadowColor = UIColor.lightGray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
            //            debugPrint("self.productImage[indexPath.row]", self.productImage[indexPath.row])
            if let image1 = self.productImage[indexPath.row] as? [AnyObject]
            {
                if image1.count > 0
                {
                    var imageUrl = image1[0] as! String
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                    let url = NSURL(string: imageUrl)
                    if url != nil{
                        DispatchQueue.main.async {
                            cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
            }
            cell.productName.text = (self.ProductName[indexPath.row] as? String)?.capitalized
            
            if feature_name[indexPath.row] == ""
            {
                cell.productTag.text = ""
            }
            else
            {
                if feature_name[indexPath.row] as! String != "Default"
                {
                    cell.productTag.text = " \(feature_name[indexPath.row] as! String) "
                    cell.productTag.isHidden = false
                }
                else
                {
                    cell.productTag.isHidden = true
                }
            }
            let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
            let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
            if self.ProductPrice.count > indexPath.row
            {
                
                let actualprice = self.ProductPrice[indexPath.row ]
                let retailsPrice = self.retailPrice[indexPath.row ]
                var product_price_tag = ""
                if shop_Product[indexPath.row]["product_price_tag"] is NSNull
                {}
                else
                {
                    product_price_tag = shop_Product[indexPath.row]["product_price_tag"] as! String
                }
                if native.string(forKey: "Token")! == ""
                {let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                    let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                    let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                    cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                }
                else
                {
                    
                    cell.price.attributedText = ("\(self.Currency)\(String(format:"%.2f", Double(actualprice)))".color(textcolor).size(11) + "\(product_price_tag) ".color(textcolor).size(9) + "\(self.Currency)\(String(format:"%.2f",Double(retailsPrice)))".color(graycolor).strikethrough(1).size(9)).attributedText
                }
            }
            
            
            ReuseCell = cell
        }
        else if collectionView.tag == 2
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Banner", for: indexPath) as! DashboardBannerCollectionViewCell
            cell.clipsToBounds = true
            let image = banner[indexPath.row] as! String
            if image != nil
            {
                var imageUrl = image as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string:imageUrl )
                if url != nil{
                    DispatchQueue.main.async {
                        cell.bannerImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            
            ReuseCell = cell
        }
        return ReuseCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0
        {
            native.set(variantID[indexPath.row], forKey: "FavProID")
            native.set("product", forKey: "comefrom")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            CustomLoader.instance.showLoaderView()
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
    }
    
    
    
    
    //check url
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    
}




extension NSAttributedString {
    internal convenience init?(html: String) {
        guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
            // not sure which is more reliable: String.Encoding.utf16 or String.Encoding.unicode
            return nil
        }
        guard let attributedString = try? NSMutableAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else {
            return nil
        }
        self.init(attributedString: attributedString)
    }
}



extension UISegmentedControl{
    func removeUnderline(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
        
        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for: .normal)
        self.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1.0)], for: .selected)
    }
    
    func addUnderlineForSelectSegment(){
        removeUnderline()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1.0)
        underline.tag = 1
        self.addSubview(underline)
    }
    
    func changeUnderline(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}

extension UIImage{
    
    class func getColoredRectImage(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}

//extension UIScrollView {
//    func updateContentViewSize() {
//        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
//    }
//}
