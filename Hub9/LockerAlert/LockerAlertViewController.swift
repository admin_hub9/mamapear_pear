//
//  LockerAlertViewController.swift
//  Hub9
//
//  Created by Deepak on 8/28/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit



class LockerAlertViewController: UIViewController, UITextFieldDelegate {
    
    
    
    @IBOutlet weak var alertContainer: UIView!
    
    @IBOutlet weak var lockerCode: UITextField!
    @IBOutlet weak var lockerButton: UIButton!
    @IBOutlet weak var textOuter: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        alertContainer.layer.cornerRadius = 8
        lockerCode.delegate = self
        alertContainer.clipsToBounds = true
        
        alertContainer.tag = 10
        
        // Do any additional setup after loading the view.
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let whitespaceSet = CharacterSet.whitespaces
        if let _ = string.rangeOfCharacter(from: whitespaceSet) {
            return false
        } else {
            return true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        lockerCode.becomeFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        var touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        if touch?.view?.tag != 10 {
            print("inside view")
            dismiss(animated: true, completion: nil)
        }
        else
        {
            
            print("outside view")
        }
        
        
    }

    @IBAction func closeAlert(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func confirmButton(_ sender: Any) {
        if lockerCode.text!.count <= 6 && lockerCode.text!.count > 4
        {
        PaymentAddress.sendLockerCode(lockerCode: lockerCode.text!)
        self.dismiss(animated: true, completion: nil)
        }
        else if lockerCode.text?.count == 0
        
        {
            self.view.makeToast("Please enter locker code first!", duration: 2.0, position: .center)
        }
        else
        {
            self.view.makeToast("please enter valid locker code!", duration: 2.0, position: .center)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
