//
//  AddProductMainViewController.swift
//  Hub9
//
//  Created by Deepak on 6/6/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class AddProductMainViewController: UIViewController {

    @IBOutlet var blueView: UIVisualEffectView!
    @IBOutlet var sellVariantsContainer: UIView!
    @IBOutlet var singleProductContainer: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenContainers()
//        blueView.alpha = 0
        singleProductButton((Any).self)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func hiddenContainers()
    {
        singleProductContainer.isHidden = true
        sellVariantsContainer.isHidden = true
    }
    func singleProductButton(_ sender: Any) {
//        blueView.alpha = 0
        hiddenContainers()
        singleProductContainer.isHidden = false
    }
     func sellVariantsButton(_ sender: Any) {
//        blueView.alpha = 0.6
        hiddenContainers()
        sellVariantsContainer.isHidden = false
    }

    


}
