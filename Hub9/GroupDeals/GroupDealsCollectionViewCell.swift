//
//  GroupDealsCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/31/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class GroupDealsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var count: UILabel!
    @IBOutlet weak var status: UILabel!
}
