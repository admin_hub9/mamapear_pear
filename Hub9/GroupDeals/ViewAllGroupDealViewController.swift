//
//  ViewAllGroupDealViewController.swift
//  Hub9
//
//  Created by Deepak on 9/9/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire


class ViewAllGroupDealViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    var native = UserDefaults.standard
    var ProductDeal = [AnyObject]()
    var dealTime = ""
    
    @IBOutlet weak var productDealCollection: UICollectionView!
    
    @IBOutlet weak var UIcontainer: UIView!
    @IBOutlet weak var DealViewContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CustomLoader.instance.showLoaderView()
        self.ProductDeal = BBProductDetails.ProductDeal
        self.productDealCollection.reloadData()
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        // Do any additional setup after loading the view.
    }
    @IBAction func closeDeal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ProductDeal.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize: CGSize = CGSize(width: 0.0, height: 0.0)
        let frameSize = collectionView.frame.size
        
        
        cellSize = CGSize(width: frameSize.width - 10, height: frameSize.height/10)
        
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dealcollectioncell", for: indexPath) as! DealDetailCollectionViewCell
        
        cell.joinButton.layer.cornerRadius = 3
        //        scrollCollection(cltView: collectionView)
        if self.ProductDeal[indexPath.row]["group_orderid"] as! NSNumber == 0
        {
            cell.joinButton.setTitle(" Start Group Buy ", for: .normal)
            cell.topMessage.text = ""
            cell.timeRemaining.text = ""
            
        }
        else
        {
            
            cell.joinButton.setTitle(" Join Group Buy ", for: .normal)
            cell.topMessage.text = "Waiting for \(self.ProductDeal[indexPath.row]["waiting_user"] as! NSNumber) user "
            if self.ProductDeal[indexPath.row]["created_at"] is NSNull
            {}
            else
            {
                let time = self.ProductDeal[indexPath.row]["created_at"] as! NSNumber
                cell.localEndTime = TimeInterval(time.doubleValue)
                cell.startTimerProgress()
                self.dealTime = "\(cell.timeRemaining)"
            }
            
        }
        cell.joinButton.clipsToBounds=true
        if self.ProductDeal[indexPath.row]["profile_pic_url"] is NSNull
        {
            cell.labelText.isHidden = false
            cell.labelText.layer.cornerRadius = cell.labelText.frame.height / 2
            cell.labelText.layer.masksToBounds = true
            if self.ProductDeal[indexPath.row]["user_name"] is NSNull
            {}
            else
            {
                var aString: String = self.ProductDeal[indexPath.row]["user_name"] as! String
                var aChar:unichar = (aString as NSString).character(at: 0)
                cell.labelText.text = "\(Character(UnicodeScalar(aChar)!))"
            }
        }
        else
        {
            var imageUrl = self.ProductDeal[indexPath.row]["profile_pic_url"] as! String
            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
            
            if let url = NSURL(string: imageUrl )
            {
                if url != nil
                {
                    cell.labelText.isHidden = true
                    DispatchQueue.main.async {
                        cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
                else
                {
                    cell.labelText.isHidden = false
                    cell.labelText.layer.cornerRadius = cell.labelText.frame.height / 2
                    cell.labelText.layer.masksToBounds = true
                    cell.labelText.text = "A"
                }
            }
            
        }
        if self.ProductDeal[indexPath.row]["user_name"] is NSNull
        {}
        else
        {
            cell.username.text = self.ProductDeal[indexPath.row]["user_name"] as! String
        }
        
        if native.object(forKey: "userid") != nil
        {
            let userid = native.string(forKey: "userid")!
            
            if self.ProductDeal[indexPath.row]["group_ownerid"] is NSNull
            {}
            else
            {
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let graycolor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1)
                if userid == "\(self.ProductDeal[indexPath.row]["group_ownerid"] as! NSNumber)"
                {
                    cell.joinButton.isEnabled = false
                    cell.joinButton.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.joinButton.isEnabled = true
                    cell.joinButton.backgroundColor = textcolor
                }
            }
        }
        else
        {
            
        }
        cell.joinButton.tag = indexPath.row
        //        cell.joinButton.addTarget(self, action: #selector(BBProductDetails.openGroupAlert(sender:)), for: .touchUpInside)
        
        
        return cell
    }
    
    //autoscroll deal collection
    func scrollCollection(cltView: UICollectionView) {
        var r = self.ProductDeal.count - 1
        
        if self.ProductDeal.count>1
        {
            BBProductDetails.t1 = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
                //            print(r)
                let index = IndexPath(item: r, section: 0)
                cltView.scrollToItem(at: index, at: .top, animated: true)
                
                if r != self.ProductDeal.count - 1 {
                    r = r+1
                }else {
                    r = 0
                }
            }
            BBProductDetails.t1!.fire()
        }
        
    }
    
    
    
    
}
