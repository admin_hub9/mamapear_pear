//
//  GroupDealTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 7/31/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class GroupDealTableViewCell: UITableViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var totalItems: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var userTitle: UILabel!
    @IBOutlet weak var paynow: UIButton!
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var timer: UILabel!
    
    var localEndTime: TimeInterval = 0 //set value from model
    var timer1:Timer?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
    @objc func timerIsRunning(timer: Timer){
        let diff = localEndTime - Date().timeIntervalSince1970
        if diff  > 0 {
            self.showProgress()
            return
        }
        self.stopProgress()
        
        
    }
    
    func showProgress(){
        let endDate = Date(timeIntervalSince1970: localEndTime / 1000)
        let nowDate = Date()
        
        var components = Calendar.current.dateComponents(Set([.day, .hour, .minute, .second, .nanosecond]), from: endDate, to: nowDate)
        // *** Get Individual components from date ***
        self.timer?.text = "Ends in: \(String(format:"%02i:%02i:%02i:%02i", (24*(2-components.day!))+(23-components.hour!) , 59 - components.minute!, 60 - components.second!, Int(Double(components.nanosecond!)/100000000 + 0.5)))"
        
        
    }
    
    func stopProgress(){
        self.timer?.text = String(format:"%02i:%02i:%02i", 0, 0, 0)
        self.timer1?.invalidate()
        self.timer1 = nil
    }
    
    func startTimerProgress() {
        timer1 = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.timerIsRunning(timer:)), userInfo: nil, repeats: true)
        self.timer1?.fire()
    }
    
    
    
}
