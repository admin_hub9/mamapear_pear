//
//  GroupDealsDisplayViewController.swift
//  Hub9
//
//  Created by Deepak on 7/31/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire





class GroupDealsDisplayViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIViewControllerTransitioningDelegate , UIScrollViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var native = UserDefaults.standard
    private let refreshControl = UIRefreshControl()
    var user_purchase_modeid = "1"
    
    var unpaidcount = 0
    var activeStatus = 0
    var Pendingcount = 0
    var Completedcount = 0
    var Expiredcount = 0
    var returncount = 0
    var cancelcount = 0
    var isDataLoading:Bool=false
    var pendingpageNo:Int=0
    var packedpageNo:Int=0
    var shippedpageNo:Int=0
    var returnpageNo:Int=0
    var cancelpageNo:Int=0
    
    var locker_code = ""
    
    
    var unpaidLimit = 10
    var unpaidOffset = 0
    var pendingLimit = 10
    var pendingOffset = 0
    var completedLimit = 10
    var completedOffset = 0
    var ExpiredLimit = 10
    var ExpiredOffset = 0
    
    
    
    
    //unpaid
    var unpaidName = [String]()
    var unpaidItemid = [Int]()
    var unpaidUnit = [String]()
    var unpaidPrice = [String]()
    var unpaidImage = [String]()
    var unpaidDate = [String]()
    var unpaidItem = [String]()
    var unpaidCreatedTime = [NSNumber]()
    var unpaidWaitingUser = [AnyObject]()
    var unpaidGroupDeal = [AnyObject]()
    var unpaidProductID = [AnyObject]()
    
    // pending
    var pendingName = [String]()
    var pendingItemid = [String]()
    var pendingUnit = [String]()
    var pendingPrice = [String]()
    var pendingImage = [String]()
    var PendingDate = [String]()
    var PendingItem = [String]()
    var PendingStatus = [String]()
    var PendingShareLink = [String]()
    var PendingCreatedTime = [NSNumber]()
    var PendingWaitingUser = [AnyObject]()
    var PendingGroupDeal = [AnyObject]()
    var PendingProductID = [AnyObject]()
    
    ///
    var completedName = [String]()
    var completedItemid = [String]()
    var completedUnit = [String]()
    var completedImage = [String]()
    var completedDate = [String]()
    var completedItem = [String]()
    var completedPrice = [String]()
    var completedProductID = [AnyObject]()
    
    
    ///
    var ExpiredName = [String]()
    var ExpiredItemid = [String]()
    var ExpiredUnit = [String]()
    var ExpiredImage = [String]()
    var ExpiredDate = [String]()
    var ExpiredItem = [String]()
    var ExpiredPrice = [String]()
    var ExpiredProductID = [AnyObject]()
    
    
    ///color
    var SelectedColor = UIColor(red:247/255, green:82/255, blue:85/255, alpha: 1)
    var UnSelectedColor = UIColor(red:85/255, green:85/255, blue:85/255, alpha: 1)
    
    
    ///UI connection
    @IBOutlet weak var statusCollection: UICollectionView!
    @IBOutlet weak var GroupDataTable: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.GroupDataTable.tableFooterView = UIView()
        self.GroupDataTable.isHidden = true
        self.statusCollection.isHidden=false
        self.statusCollection.reloadData()
        // Do any additional setup after loading the view.
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        // Do any additional setup after loading the view.
        // refresh Table
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            GroupDataTable.refreshControl = refreshControl
        } else {
            GroupDataTable.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
        parseUnpaidData(limit: self.unpaidLimit, offset: self.unpaidOffset)
        parsePendingData(limit: self.pendingLimit, offset: self.pendingOffset)
        parsePackedData(limit: self.completedLimit, offset: self.completedOffset)
        parseShippedData(limit: self.ExpiredLimit, offset: self.ExpiredOffset)
        
    }
    
    
    
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Api Data
        fetchApiData()
    }
    
    private func fetchApiData() {
        
        if activeStatus == 0
        {
            unpaidName.removeAll()
            unpaidItemid.removeAll()
            unpaidUnit.removeAll()
            unpaidPrice.removeAll()
            unpaidImage.removeAll()
            unpaidDate.removeAll()
            unpaidItem.removeAll()
            unpaidLimit = 10
            unpaidOffset = 0
            self.unpaidGroupDeal.removeAll()
            parseUnpaidData(limit: self.unpaidLimit, offset: self.unpaidOffset)
        }
        else if activeStatus == 1
        {
            pendingName.removeAll()
            pendingItemid.removeAll()
            pendingUnit.removeAll()
            pendingPrice.removeAll()
            pendingImage.removeAll()
            PendingDate.removeAll()
            PendingItem.removeAll()
            pendingLimit = 10
            pendingOffset = 0
            parsePendingData(limit: self.pendingLimit, offset: self.pendingOffset)
        }
            ///
        else if activeStatus == 2
        {
            completedName.removeAll()
            completedItemid.removeAll()
            completedUnit.removeAll()
            completedImage.removeAll()
            completedDate.removeAll()
            completedItem.removeAll()
            completedPrice.removeAll()
            completedLimit = 10
            completedOffset = 0
            parsePackedData(limit: self.completedLimit, offset: self.completedOffset)
        }
            ///
        else if activeStatus == 3
        {
            ExpiredName.removeAll()
            ExpiredItemid.removeAll()
            ExpiredUnit.removeAll()
            ExpiredImage.removeAll()
            ExpiredDate.removeAll()
            ExpiredItem.removeAll()
            ExpiredPrice.removeAll()
            ExpiredLimit = 10
            ExpiredOffset = 0
            parseShippedData(limit: self.ExpiredLimit, offset: self.ExpiredOffset)
        }
        ///
        
        self.updateView()
        
    }
    
    func updateView()
    {
        refreshControl.tintColor = UIColor(red:247/255, green:82/255, blue:85/255, alpha:1.0)
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching  Data ...")
        //        refreshControl.endRefreshing()
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        debugPrint("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        debugPrint("scrollViewDidEndDecelerating")
    }
    
    //Pagination
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        debugPrint("scrollViewDidEndDragging")
        if ((GroupDataTable.contentOffset.y + GroupDataTable.frame.size.height) >= GroupDataTable.contentSize.height)
        {
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: GroupDataTable.bounds.width, height: CGFloat(44))
            
            
            if !isDataLoading{
                isDataLoading = true
                if activeStatus == 0
                {
                    debugPrint(self.unpaidItemid.count, self.unpaidOffset)
                    if self.unpaidItemid.count >= self.unpaidOffset
                    {
                        self.unpaidOffset=self.unpaidOffset+10
                        
                        
                        parseUnpaidData(limit: self.unpaidLimit, offset: self.unpaidOffset)
                        self.GroupDataTable.tableFooterView = spinner
                        self.GroupDataTable.tableFooterView?.isHidden = false
                        //                loadCallLogData(offset: self.offset, limit: self.limit)
                    }
                }
                else if activeStatus == 1
                {
                    debugPrint(self.pendingItemid.count, self.pendingOffset)
                    if self.pendingItemid.count >= self.pendingOffset
                    {
                        self.pendingOffset=self.pendingOffset+10
                        
                        
                        parsePendingData(limit: self.pendingLimit, offset: self.pendingOffset)
                        self.GroupDataTable.tableFooterView = spinner
                        self.GroupDataTable.tableFooterView?.isHidden = false
                        //                loadCallLogData(offset: self.offset, limit: self.limit)
                    }
                }
                else if activeStatus == 2
                {
                    if self.completedItemid.count >= self.completedOffset
                    {
                        self.completedOffset=self.completedOffset + 10
                        
                        
                        parsePackedData(limit: self.completedLimit, offset: self.completedOffset)
                        self.GroupDataTable.tableFooterView = spinner
                        self.GroupDataTable.tableFooterView?.isHidden = false
                    }
                }
                else if activeStatus == 3
                {
                    if self.ExpiredItemid.count >= self.ExpiredOffset
                    {
                        self.ExpiredOffset=self.ExpiredOffset + 10
                        
                        parseShippedData(limit: self.ExpiredLimit, offset: self.ExpiredOffset)
                        self.GroupDataTable.tableFooterView = spinner
                        self.GroupDataTable.tableFooterView?.isHidden = false
                    }
                }
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @objc func ShareNow(sender: UIButton)
    {
        
        // image to share
        let text = ""
        var productImage = "thumbnail"
        var ProductImg = [AnyObject]()
        var product_share_link = ""
        if ProductImg.count>0
        {
            var imageUrl = ProductImg[0] as! String
            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
            
            if let url = NSURL(string: imageUrl )
            {
                if url != nil
                {
                    productImage = imageUrl
                }}}
        let image = UIImage(named: productImage)
        let myWebsite = NSURL(string:product_share_link)
        let shareAll = [text , image , myWebsite!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    /// UI table status
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //        debugPrint(activeStatus, self.pendingItemid.count)
        if activeStatus == 0
        {
            if unpaidItemid.count > 0
            {
                GroupDataTable.showEmptyListMessage("")
            }
            else
            {
                GroupDataTable.showEmptyListMessage("No Item in Unpaid")
                
            }
            return unpaidItemid.count
        }
        else if activeStatus == 1
        {
            if pendingItemid.count > 0
            {
                GroupDataTable.showEmptyListMessage("")
            }
            else
            {
                GroupDataTable.showEmptyListMessage("No Item in Pending")
                
            }
            return pendingItemid.count
        }
        else if activeStatus == 2
        {
            if completedItemid.count > 0
            {
                GroupDataTable.showEmptyListMessage("")
            }
            else
            {
                GroupDataTable.showEmptyListMessage("No Item in Packed")
                
            }
            return completedItemid.count
        }
        else if activeStatus == 3
        {
            if ExpiredItemid.count > 0
            {
                GroupDataTable.showEmptyListMessage("")
            }
            else
            {
                GroupDataTable.showEmptyListMessage("No Item in Shipping")
            }
            return ExpiredItemid.count
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if activeStatus == 0
        {
            let itemid = unpaidProductID[indexPath.row] as! Int
            native.set(itemid, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
            
        }
        else if activeStatus == 1
        {
            let itemid = PendingProductID[indexPath.row] as! Int
            native.set(itemid, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
        }
        else if activeStatus == 2
        {
            let itemid = completedProductID[indexPath.row] as! Int
            native.set(itemid, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
        }
        else if activeStatus == 3
        {
            let itemid = ExpiredProductID[indexPath.row] as! Int
            native.set(itemid, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
        }
        
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as! UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReuseCell = UITableViewCell()
        let redColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1).cgColor
        let grayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1).cgColor
        let cell = tableView.dequeueReusableCell(withIdentifier: "groupDealsTable") as! GroupDealTableViewCell
        
        
        cell.paynow.layer.cornerRadius=cell.paynow.frame.height/2
        cell.paynow.clipsToBounds=true
        cell.cancel.layer.cornerRadius=cell.cancel.frame.height/2
        cell.cancel.layer.borderWidth=1
        cell.cancel.layer.borderColor=grayColor
        cell.cancel.clipsToBounds=true
        cell.status.layer.cornerRadius=cell.status.frame.height/2
        cell.status.clipsToBounds=true
        
        if self.activeStatus == 0 && self.unpaidGroupDeal.count > indexPath.row
        {
            
            
            debugPrint("unpaidDate[indexPath.row]", indexPath.row)
            cell.date.text = unpaidDate[indexPath.row]
            cell.productName.text = unpaidName[indexPath.row]
            cell.totalItems.text = unpaidUnit[indexPath.row]
            cell.price.text = unpaidPrice[indexPath.row]
            cell.paynow.isHidden = false
            cell.paynow.isEnabled=false
            cell.cancel.isHidden = false
            cell.timer.isHidden = false
            cell.userTitle.isHidden = false
            cell.status.text = "  Unpaid  "
            cell.paynow.setTitle("  Pay Now  ", for: .normal)
            let time = self.unpaidGroupDeal[indexPath.row]["deal_created"] as! NSNumber
            if self.unpaidGroupDeal[indexPath.row]["waiting_user"] is NSNull
            {}
            else
            {
                if self.unpaidGroupDeal[indexPath.row]["waiting_user"] as! NSNumber == 1
                {
                    cell.userTitle.text = "Waiting for \(self.unpaidGroupDeal[indexPath.row]["waiting_user"] as! NSNumber) user"
                }
                else
                {
                    cell.userTitle.text = "Waiting for \(self.unpaidGroupDeal[indexPath.row]["waiting_user"] as! NSNumber) users"
                }}
            cell.localEndTime = TimeInterval(time.doubleValue)
            cell.startTimerProgress()
            cell.cancel.tag = indexPath.row
            cell.cancel.addTarget(self, action: #selector(cancelAlert(order_id:)), for: .touchUpInside)
            if self.unpaidImage.count > indexPath.row
            {
                var imageUrl = unpaidImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    } }
            }
            ReuseCell = cell
        }
        else if self.activeStatus == 1 && self.pendingItemid.count > indexPath.row
        {
            
            
            //            debugPrint("PendingDate[indexPath.row]", indexPath.row)
            cell.date.text = PendingDate[indexPath.row]
            cell.productName.text = pendingName[indexPath.row]
            cell.totalItems.text = pendingUnit[indexPath.row]
            cell.price.text = pendingPrice[indexPath.row]
            cell.paynow.isHidden = false
            cell.status.text = "  Paid  "
            cell.paynow.isEnabled=true
            cell.paynow.setTitle("  Share Now  ", for: .normal)
            cell.paynow.tag = indexPath.row
            cell.paynow.addTarget(self, action: #selector(productToShare(IndexRow:)), for: .touchUpInside)
            
            cell.cancel.isHidden = false
            cell.cancel.addTarget(self, action: #selector(cancelHelpAlert), for: .touchUpInside)
            cell.timer.isHidden = false
            cell.userTitle.isHidden = false
            
            if self.PendingGroupDeal.count > indexPath.row
            {
                let time = self.PendingGroupDeal[indexPath.row]["deal_created"] as! NSNumber
                if self.PendingGroupDeal[indexPath.row]["waiting_user"] is NSNull
                {}
                else
                {
                    if self.PendingGroupDeal[indexPath.row]["waiting_user"] as! NSNumber == 1
                    {
                        cell.userTitle.text = "Waiting for \(self.PendingGroupDeal[indexPath.row]["waiting_user"] as! NSNumber) user"
                    }
                    else
                    {
                        cell.userTitle.text = "Waiting for \(self.PendingGroupDeal[indexPath.row]["waiting_user"] as! NSNumber) users"
                    }}
                cell.localEndTime = TimeInterval(time.doubleValue)
                cell.startTimerProgress()
            }
            if self.pendingImage.count > indexPath.row
            {
                var imageUrl = pendingImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }}
            }
            ReuseCell = cell
        }
        else if activeStatus == 2 && self.completedItemid.count > indexPath.row
        {
            cell.date.text = completedDate[indexPath.row]
            cell.productName.text = completedName[indexPath.row]
            cell.totalItems.text = completedUnit[indexPath.row]
            cell.price.text = completedPrice[indexPath.row]
            cell.paynow.isHidden = true
            cell.cancel.isHidden = true
            cell.status.text = "  Paid  "
            cell.timer.isHidden = true
            cell.userTitle.isHidden = true
            if self.completedImage.count > indexPath.row
            {
                var imageUrl = completedImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }}
            }
            ReuseCell = cell
        }
        else if activeStatus == 3 && self.ExpiredItemid.count > indexPath.row
        {   cell.date.text = ExpiredDate[indexPath.row]
            cell.productName.text = ExpiredName[indexPath.row]
            cell.totalItems.text = ExpiredUnit[indexPath.row]
            cell.price.text = ExpiredPrice[indexPath.row]
            cell.paynow.isHidden = true
            cell.cancel.isHidden = true
            cell.timer.isHidden = true
            cell.status.text = "  Paid  "
            cell.userTitle.isHidden = true
            if self.ExpiredImage.count > indexPath.row
            {
                var imageUrl = ExpiredImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    } }
            }
            ReuseCell = cell
        }
        
        
        
        
        
        return ReuseCell
    }
    
    
    ///status collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        activeStatus = indexPath.row
        self.statusCollection.reloadData()
        self.GroupDataTable.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize
        
        let width = (collectionView.frame.width-30)/4
        
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var ReuseCell = UICollectionViewCell()
        if collectionView == statusCollection {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "groupDealsCollection", for: indexPath) as! GroupDealsCollectionViewCell
            debugPrint("collectionindex", indexPath.row)
            if indexPath.row == 0
            {
                cell.status.text = "Unpaid"
                cell.count.text = String(self.unpaidcount)
            }
            else if indexPath.row == 1
            {
                cell.status.text = "Pending"
                cell.count.text = String(self.Pendingcount)
            }
            else if indexPath.row == 2
            {
                
                cell.status.text = "Completed"
                cell.count.text = String(self.Completedcount)
            }
            else if indexPath.row == 3
            {
                
                cell.status.text = "Expired"
                cell.count.text = String(self.Expiredcount)
            }
            
            if activeStatus != indexPath.row
            {
                cell.status?.font = UIFont.systemFont(ofSize: 12.0, weight: .regular)
                cell.count?.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
                cell.status.textColor = UnSelectedColor
                cell.count.textColor = UnSelectedColor
            }
            else
            {
                cell.status?.font = UIFont.systemFont(ofSize: 12.0, weight: .bold)
                cell.count?.font = UIFont.systemFont(ofSize: 14.0, weight: .bold)
                cell.status.textColor = SelectedColor
                cell.count.textColor = SelectedColor
            }
            
            ReuseCell = cell
            
        }
        return ReuseCell
    }
    
    // parse Unpaid data
    func parseUnpaidData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Unpaid&purchase_modeid=\(user_purchase_modeid)&order_typeid=2"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Unpaid&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Pending", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response Pending: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                
                                
                                if let innerdict1 = dict1["data"] as? [AnyObject]
                                {
                                    self.unpaidGroupDeal = innerdict1
                                }
                                
                            }
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    self.unpaidcount = innerdict[i]["unpaid"] as! Int
                                    
                                    self.Pendingcount = innerdict[i]["pending"] as! Int
                                    
                                    self.Completedcount = innerdict[i]["complete"] as! Int
                                    
                                    self.Expiredcount = innerdict[i]["expired"] as! Int
                                    
                                    
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    
                                    
                                    if innerdict1[i]["order_item"] is NSNull
                                    {}
                                    else{
                                        let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                        self.unpaidItemid.append(innerdict1[i]["orderid"] as! Int)
                                        var itemPrice = 0.0
                                        var shipping = 0.0
                                        var shippingInsurance = 0.0
                                        var cod = 0.0
                                        var currency = ""
                                        if order_data[0]["currency_symbol"] is NSNull
                                        {
                                            currency = ""
                                        }
                                        else
                                        {
                                            currency = order_data[0]["currency_symbol"] as! String
                                        }
                                        
                                        if innerdict1[i]["amount"] is NSNull
                                        {}
                                        else{
                                            itemPrice = (innerdict1[i]["amount"] as! NSNumber).doubleValue
                                        }
                                        //                                        if innerdict1[i]["deal_created"] is NSNull
                                        //                                        {}
                                        //                                        else{
                                        //                                            itemPrice = (innerdict1[i]["deal_created"] as! NSNumber).doubleValue
                                        //                                        }
                                        if innerdict1[i]["cod_charge"] is NSNull
                                        {}
                                        else{
                                            cod = (innerdict1[i]["cod_charge"] as! NSNumber).doubleValue
                                        }
                                        if innerdict1[i]["ship_insurance"] is NSNull
                                        {}
                                        else{
                                            shippingInsurance = (innerdict1[i]["ship_insurance"] as! NSNumber).doubleValue
                                        }
                                        if innerdict1[i]["shipping_charge"] is NSNull
                                        {}
                                        else{
                                            shipping = (innerdict1[i]["shipping_charge"] as! NSNumber).doubleValue
                                        }
                                        self.unpaidPrice.append("\(currency)\(String(format: "%.2f",(itemPrice+shippingInsurance+shipping+cod)))")
                                        if order_data[0]["title"] is NSNull
                                        {
                                            self.unpaidName.append("")
                                        }
                                        else
                                        {
                                            self.unpaidName.append(order_data[0]["title"] as! String)
                                        }
                                        if order_data[0]["productid"] is NSNull
                                        {
                                            self.unpaidProductID.append(0 as! NSNumber)
                                        }
                                        else
                                        {
                                            self.unpaidProductID.append(order_data[0]["productid"] as! NSNumber)
                                        }
                                        if let images = order_data[0]["image_url"] as? AnyObject
                                        {
                                            if images["200"] is NSNull
                                            {
                                                self.unpaidImage.append("")
                                            }
                                            else
                                            {
                                                let imagedata = images["200"] as! [AnyObject]
                                                if imagedata.count > 0
                                                {
                                                    var image = imagedata[0] as! String
                                                    self.unpaidImage.append(image)
                                                }
                                            }
                                        }
                                        if innerdict1[i]["inserted_at"] is NSNull
                                        {}
                                        else
                                        {
                                            self.unpaidDate.append(innerdict1[i]["inserted_at"] as! String)
                                        }
                                        if order_data.count > 1
                                        {
                                            self.unpaidUnit.append(String("\(order_data.count) items included"))
                                        }
                                        else
                                        {
                                            self.unpaidUnit.append("")
                                        }
                                        
                                    }
                                }
                            }
                            
                            
                            //                        }
                            
                        }
                    }
                }
                
            }
            UIApplication.shared.endIgnoringInteractionEvents()
            CustomLoader.instance.hideLoaderView()
            debugPrint("=--=-=-=-=-=-=", self.unpaidName)
            //                        DispatchQueue.main.async {
            self.GroupDataTable.isHidden = false
            self.statusCollection.reloadData()
            self.GroupDataTable.reloadData()
            self.GroupDataTable.tableFooterView?.isHidden = true
            self.refreshControl.endRefreshing()
        }
        
    }
    
    // parsePending data
    func parsePendingData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Pending&purchase_modeid=\(user_purchase_modeid)&order_typeid=2"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Pending&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Pending", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response Pending: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                
                                
                                if let innerdict1 = dict1["data"] as? [AnyObject]
                                {
                                    self.PendingGroupDeal = innerdict1
                                }
                                
                            }
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                if let innerdict = dict1["count_data"] as? [AnyObject]
                                {
                                    for var i in 0..<innerdict.count
                                    {
                                        self.unpaidcount = innerdict[i]["unpaid"] as! Int
                                        self.Pendingcount = innerdict[i]["pending"] as! Int
                                        self.Completedcount = innerdict[i]["complete"] as! Int
                                        self.Expiredcount = innerdict[i]["expired"] as! Int
                                        
                                        
                                    }
                                }
                                if let innerdict1 = dict1["data"] as? [AnyObject]
                                {
                                    for var i in 0..<innerdict1.count
                                    {
                                        
                                        if innerdict1[i]["order_item"] is NSNull
                                        {}
                                        else{
                                            if let order_data = innerdict1[i]["order_item"] as? [AnyObject]
                                            { self.pendingItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                                var itemPrice = 0.0
                                                var shipping = 0.0
                                                var shippingInsurance = 0.0
                                                var cod = 0.0
                                                var currency = ""
                                                if order_data[0]["currency_symbol"] is NSNull
                                                {
                                                    currency = ""
                                                }
                                                else
                                                {
                                                    currency = order_data[0]["currency_symbol"] as! String
                                                }
                                                
                                                if innerdict1[i]["amount"] is NSNull
                                                {}
                                                else{
                                                    itemPrice = (innerdict1[i]["amount"] as? NSNumber)!.doubleValue
                                                }
                                                if innerdict1[i]["cod_charge"] is NSNull
                                                {}
                                                else{
                                                    cod = (innerdict1[i]["cod_charge"] as? NSNumber)!.doubleValue
                                                }
                                                if innerdict1[i]["ship_insurance"] is NSNull
                                                {}
                                                else{
                                                    shippingInsurance = (innerdict1[i]["ship_insurance"] as? NSNumber)!.doubleValue
                                                }
                                                if innerdict1[i]["shipping_charge"] is NSNull
                                                {}
                                                else{
                                                    shipping = (innerdict1[i]["shipping_charge"] as? NSNumber)!.doubleValue
                                                }
                                                self.pendingPrice.append("\(currency)\(String(format: "%.2f",(itemPrice+shippingInsurance+shipping+cod)))")
                                                if order_data[0]["title"] is NSNull
                                                {
                                                    self.pendingName.append("")
                                                }
                                                else
                                                {
                                                    self.pendingName.append(order_data[0]["title"] as! String)
                                                }
                                                if order_data[0]["productid"] is NSNull
                                                {
                                                    self.PendingProductID.append(0 as! NSNumber)
                                                }
                                                else
                                                {
                                                    self.PendingProductID.append(order_data[0]["productid"] as! NSNumber)
                                                }
                                                
                                                if let images = order_data[0]["image_url"] as? AnyObject
                                                {
                                                    if images["200"] is NSNull
                                                    {
                                                        self.pendingImage.append("")
                                                    }
                                                    else
                                                    {
                                                        let imagedata = images["200"] as! [AnyObject]
                                                        if imagedata.count > 0
                                                        {
                                                            var image = imagedata[0] as! String
                                                            self.pendingImage.append(image)
                                                        }
                                                    }
                                                }
                                                if innerdict1[i]["inserted_at"] is NSNull
                                                {}
                                                else
                                                {
                                                    self.PendingDate.append((innerdict1[i]["inserted_at"] as? String)!)
                                                }
                                                if order_data.count > 1
                                                {
                                                    self.pendingUnit.append(String("\(order_data.count) items included"))
                                                }
                                                else
                                                {
                                                    self.pendingUnit.append("")
                                                }
                                                
                                            }}}
                                }
                            }
                            
                            
                        }
                    }
                }
                
                //                        }
            }
            UIApplication.shared.endIgnoringInteractionEvents()
            CustomLoader.instance.hideLoaderView()
            debugPrint("=--=-=-=-=-=-=", self.pendingName)
            //                        DispatchQueue.main.async {
            self.GroupDataTable.isHidden = false
            self.statusCollection.reloadData()
            self.GroupDataTable.reloadData()
            self.GroupDataTable.tableFooterView?.isHidden = true
            self.refreshControl.endRefreshing()
            
        }
        
    }
    
    // parse packed data
    func parsePackedData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "changetype")!
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            
            //            if usertype == "20"
            //            {
            //                url = "\(b20burl)/order/seller_orders/?limit=40&offset=0&status_ui=Packed"
            //            }
            //            else if usertype == "1"
            //            {
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Completed&purchase_modeid=\(user_purchase_modeid)&order_typeid=2"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Completed&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("completed", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response completed: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                if let innerdict = dict1["count_data"] as? [AnyObject]
                                {
                                    for var i in 0..<innerdict.count
                                    {
                                        self.unpaidcount = innerdict[i]["unpaid"] as! Int
                                        
                                        self.Pendingcount = innerdict[i]["pending"] as! Int
                                        
                                        self.Completedcount = innerdict[i]["complete"] as! Int
                                        
                                        self.Expiredcount = innerdict[i]["expired"] as! Int
                                        
                                        
                                    }
                                }
                                
                                if let innerdict1 = dict1["data"] as? [AnyObject]
                                {
                                    for var i in 0..<innerdict1.count
                                    {
                                        
                                        if innerdict1[i]["order_item"] is NSNull
                                        {}else{
                                            if let order_data = innerdict1[i]["order_item"] as? [AnyObject]
                                            { self.completedItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                                var itemPrice = 0.0
                                                var shipping = 0.0
                                                var shippingInsurance = 0.0
                                                var cod = 0.0
                                                var currency = ""
                                                if order_data[0]["currency_symbol"] is NSNull
                                                {
                                                    currency = ""
                                                }
                                                else
                                                {
                                                    currency = order_data[0]["currency_symbol"] as! String
                                                }
                                                
                                                if innerdict1[i]["amount"] is NSNull
                                                {}
                                                else{
                                                    itemPrice = (innerdict1[i]["amount"] as! NSNumber).doubleValue
                                                }
                                                if innerdict1[i]["cod_charge"] is NSNull
                                                {}
                                                else{
                                                    cod = (innerdict1[i]["cod_charge"] as! NSNumber).doubleValue
                                                }
                                                if innerdict1[i]["ship_insurance"] is NSNull
                                                {}
                                                else{
                                                    shippingInsurance = (innerdict1[i]["ship_insurance"] as! NSNumber).doubleValue
                                                }
                                                if innerdict1[i]["shipping_charge"] is NSNull
                                                {}
                                                else{
                                                    shipping = (innerdict1[i]["shipping_charge"] as! NSNumber).doubleValue
                                                }
                                                self.completedPrice.append("\(currency)\(String(format: "%.2f",(itemPrice+shippingInsurance+shipping+cod)))")
                                                if order_data[0]["title"] is NSNull
                                                {
                                                    self.completedName.append("")
                                                }
                                                else
                                                {
                                                    self.completedName.append(order_data[0]["title"] as! String)
                                                }
                                                if order_data[0]["productid"] is NSNull
                                                {
                                                    self.completedProductID.append(0 as! NSNumber)
                                                }
                                                else
                                                {
                                                    self.completedProductID.append(order_data[0]["productid"] as! NSNumber)
                                                }
                                                
                                                if let images = order_data[0]["image_url"] as? AnyObject
                                                {
                                                    if images["200"] is NSNull
                                                    {
                                                        self.completedImage.append("")
                                                    }
                                                    else
                                                    {
                                                        let imagedata = images["200"] as! [AnyObject]
                                                        if imagedata.count > 0
                                                        {
                                                            var image = imagedata[0] as! String
                                                            self.completedImage.append(image)
                                                        }
                                                    }
                                                }
                                                self.completedDate.append(innerdict1[i]["inserted_at"] as! String)
                                                if order_data.count > 1
                                                {
                                                    self.completedUnit.append(String("\(order_data.count) items included"))
                                                }
                                                else
                                                {
                                                    self.completedUnit.append("")
                                                }
                                                
                                            }}}
                                }
                            }
                            
                        }
                    }
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                DispatchQueue.main.async {
                    
                    self.GroupDataTable.isHidden = false
                    self.statusCollection.reloadData()
                    self.GroupDataTable.reloadData()
                    self.GroupDataTable.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    
                }
            }
        }
    }
    
    // parse Shipped data
    func parseShippedData(limit: Int, offset: Int)
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "changetype")!
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            
            //            if usertype == "20"
            //            {
            //                url = "\(b20burl)/order/seller_orders/?limit=40&offset=0&status_ui=Shipped"
            //            }
            //            else if usertype == "1"
            //            {
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Expired&purchase_modeid=\(user_purchase_modeid)&order_typeid=2"
            }
            else{
                url = "\(b20burl)/order/buyer_orders/?limit=\(limit)&offset=\(offset)&status_ui=Expired&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Shipped", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response expired: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                if let innerdict = dict1["count_data"] as? [AnyObject]
                                {
                                    for var i in 0..<innerdict.count
                                    {
                                        self.unpaidcount = innerdict[i]["unpaid"] as! Int
                                        
                                        self.Pendingcount = innerdict[i]["pending"] as! Int
                                        
                                        self.Completedcount = innerdict[i]["complete"] as! Int
                                        
                                        self.Expiredcount = innerdict[i]["expired"] as! Int
                                        
                                    }
                                }
                                
                                if let innerdict1 = dict1["data"] as? [AnyObject]
                                {
                                    for var i in 0..<innerdict1.count
                                    {
                                        
                                        if innerdict1[i]["order_item"] is NSNull
                                        {}else{
                                            let order_data = innerdict1[i]["order_item"] as! [AnyObject]
                                            self.ExpiredItemid.append(String(innerdict1[i]["orderid"] as! Int))
                                            var itemPrice = 0.0
                                            var shipping = 0.0
                                            var shippingInsurance = 0.0
                                            var cod = 0.0
                                            var currency = ""
                                            if order_data[0]["currency_symbol"] is NSNull
                                            {
                                                currency = ""
                                            }
                                            else
                                            {
                                                currency = order_data[0]["currency_symbol"] as! String
                                            }
                                            
                                            if innerdict1[i]["amount"] is NSNull
                                            {}
                                            else{
                                                itemPrice = (innerdict1[i]["amount"] as! NSNumber).doubleValue
                                            }
                                            if innerdict1[i]["cod_charge"] is NSNull
                                            {}
                                            else{
                                                cod = (innerdict1[i]["cod_charge"] as! NSNumber).doubleValue
                                            }
                                            if innerdict1[i]["ship_insurance"] is NSNull
                                            {}
                                            else{
                                                shippingInsurance = (innerdict1[i]["ship_insurance"] as! NSNumber).doubleValue
                                            }
                                            if innerdict1[i]["shipping_charge"] is NSNull
                                            {}
                                            else{
                                                shipping = (innerdict1[i]["shipping_charge"] as! NSNumber).doubleValue
                                            }
                                            self.ExpiredPrice.append("\(currency)\(String(format: "%.2f",(itemPrice+shippingInsurance+shipping+cod)))")
                                            if order_data[0]["title"] is NSNull
                                            {
                                                self.ExpiredName.append("")
                                            }
                                            else
                                            {
                                                self.ExpiredName.append(order_data[0]["title"] as! String)
                                            }
                                            if order_data[0]["productid"] is NSNull
                                            {
                                                self.ExpiredProductID.append(0 as! NSNumber)
                                            }
                                            else
                                            {
                                                self.ExpiredProductID.append(order_data[0]["productid"] as! NSNumber)
                                            }
                                            
                                            if let images = order_data[0]["image_url"] as? AnyObject
                                            {
                                                if images["200"] is NSNull
                                                {
                                                    self.ExpiredImage.append("")
                                                }
                                                else
                                                {
                                                    let imagedata = images["200"] as! [AnyObject]
                                                    if imagedata.count > 0
                                                    {
                                                        var image = imagedata[0] as! String
                                                        self.ExpiredImage.append(image)
                                                    }
                                                }
                                            }
                                            self.ExpiredDate.append(innerdict1[i]["inserted_at"] as! String)
                                            if order_data.count > 1
                                            {
                                                self.ExpiredUnit.append(String("\(order_data.count) items included"))
                                            }
                                            else
                                            {
                                                self.ExpiredUnit.append("")
                                            }
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                    }}
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                DispatchQueue.main.async {
                    self.statusCollection.reloadData()
                    self.GroupDataTable.reloadData()
                    self.GroupDataTable.tableFooterView?.isHidden = true
                    self.refreshControl.endRefreshing()
                    
                }
            }
        }
        
    }
    
    
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    
    @objc func cancelHelpAlert()
    {
        let alert = UIAlertController(title: "", message: "Group deal can only be cancelled after the deal ends, any payments made towards a unsuccessful deal will be automatically refunded", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { _ in
            //Cancel Action
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func cancelAlert(order_id: UIButton)
    {
        let alertController = UIAlertController(title: "", message: "Are you sure that you want to cancel This order?", preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: "No" , style: .default) { (_ action) in
            //code here…
            
        }
        let ok = UIAlertAction(title: "Yes" , style: .default) { (_ action) in
            //code here…
            self.cancelDeal(order_id: order_id.tag)
        }
        ok.setValue(UIColor.black, forKey: "titleTextColor")
        cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
        alertController.addAction(cancel)
        alertController.addAction(ok)
        alertController.view.tintColor = .yellow
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    @objc func cancelDeal(order_id: Int)
    {
        if unpaidItemid.count < order_id
        {}
        else
        {
            let token = self.native.string(forKey: "Token")!
            if token != ""
            {
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                debugPrint("Successfully post")
                var parameters = ["order_id": unpaidItemid[order_id]]
                let b2burl = native.string(forKey: "b2burl")!
                debugPrint("param ", b2burl, parameters)
                Alamofire.request("\(b2burl)/order/cancel_order/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    debugPrint("cancel responce", response)
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        //                    self.blurView.alpha = 0
                        //                    self.activity.stopAnimating()
                        //                    self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    if let json = response.result.value {
                        debugPrint("JSON: \(json)")
                        let jsonString = "\(json)"
                        let result = response.result
                        switch response.result
                        {
                        case .failure(let error):
                            //                debugPrint(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                debugPrint("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                debugPrint(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                debugPrint("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                    
                                else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                                {
                                    if dict1["status"] as! String == "success"
                                    {
                                        if let responce = dict1["response"] as? AnyObject
                                        {
                                            if responce["cancelled_order"] is NSNull
                                            {}
                                            else
                                            {
                                                self.fetchApiData()
                                            }
                                            //                                        if responce["order_not_found"] is NSNull
                                            //                                        {}
                                            //                                        else
                                            //                                        {
                                            //                                            self.view.makeToast("Order not found!", duration: 2.0, position: .center)
                                            //                                        }
                                        }
                                        
                                    }
                                    else
                                    {
                                        self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                                    }}
                                else
                                {
                                    self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                                }
                                self.statusCollection.reloadData()
                                self.GroupDataTable.reloadData()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                return
                            }}}
                }
            }
        }
    }
    
    @objc func productToShare(IndexRow: UIButton)
    {
        debugPrint("IndexRow.tag", IndexRow.tag)
        if self.PendingGroupDeal.count>IndexRow.tag
        {
            let order_item = self.PendingGroupDeal[IndexRow.tag]["order_item"] as! [AnyObject]
            if self.PendingGroupDeal[IndexRow.tag]["locker_code"] is NSNull
            {}
            else
            {
                self.locker_code = "\((self.PendingGroupDeal[IndexRow.tag]["locker_code"] as? NSNumber)!)"
            }
            // image to share
            var ProductImage = order_item[0]["image_url"] as! AnyObject
            let image = ProductImage["200"] as! [AnyObject]
            var shareImage = "bb"
            if image.count > 0 {
                var imageUrl = image[0] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: imageUrl)
                if url != nil
                {
                    shareImage = imageUrl
                }}
            var text = "BBProduct"
            var product_share_link = "https://www.bulkbyte.com"
            var productImage = "thumbnail"
            
            if order_item[0]["title"] is NSNull
            {}
            else
            {
                text = order_item[0]["title"] as! String
            }
            if order_item[0]["product_share_link"] is NSNull
            {}
            else
            {
                product_share_link = "\(order_item[0]["product_share_link"] as! String)?locker_code=\(self.locker_code)"
            }
            let image1 = UIImage(named: shareImage)
            let myWebsite = NSURL(string: product_share_link)
            let shareAll = [text , image1 , myWebsite!] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    
    
}


