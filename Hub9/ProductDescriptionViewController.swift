//
//  ProductDescriptionViewController.swift
//  Hub9
//
//  Created by Deepak on 4/10/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Foundation

class ProductDescriptionViewController: UIViewController {
    
    @IBOutlet weak var productDescriptiontext: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if BBProductDetails.productDescription != nil
        {
            var contentHTML =  BBProductDetails.productDescription
            print("description", contentHTML)
            contentHTML = contentHTML.replacingOccurrences(of: "<br><br>", with: "<br>")
            contentHTML = contentHTML.replacingOccurrences(of: "</li><br>", with: "</li>")
            contentHTML = contentHTML.replacingOccurrences(of: "</li><br></ul>", with: "</li></ul>")
            productDescriptiontext.attributedText = NSAttributedString(html: contentHTML)
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    

}
