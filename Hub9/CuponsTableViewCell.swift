//
//  CuponsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 8/20/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class CuponsTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cuponCode: UILabel!
    @IBOutlet weak var offerDetail: UILabel!
    @IBOutlet weak var ApplyButton: UIButton!
    @IBOutlet weak var cuponDescription: UILabel!
    @IBOutlet weak var cuponAlert: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
