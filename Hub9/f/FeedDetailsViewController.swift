//
//  FeedDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 27/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AVKit

class FeedDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    
    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    
    var native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenHeight = 0
    
    /// data
    var feedImage = ""
    var feedVideo = ""
    var QuestionText  = ""
    var like = 0
    var likeCount = 0
    var commentCount = 0
    var Comment = false
    var data = [AnyObject]()
    
    var user_feedid = 0
    var limit = 20
    var offset = 0
    var isFeedID_liked = false
    var shareLink = ""
    var userImage = ""
    var userName = ""
    var uploadTime = 0.0
    
    
    var is_editing = false
    var editIndex = 0
    
    @IBOutlet weak var comment: UITextField!
    
    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var postComment: UIButton!
    @IBOutlet weak var feedDetailsTable: UITableView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(false, animated: true)
           self.tabBarController?.tabBar.isHidden = true
        
        
           self.avPlayer.play()
           }
       
    override func viewWillDisappear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(false, animated: true)
           
           self.tabBarController?.tabBar.isHidden = false
           
           super.viewWillDisappear(animated)
           self.avPlayer.pause()
               NotificationCenter.default.post(name: Notification.Name("avPlayerDidDismiss"), object: nil, userInfo: nil)
           
       }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        comment.resignFirstResponder()
        self.is_editing = false
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.QuestionText != ""
        {
            self.bottomContainer.isHidden=false
        }
        else
        {
            self.bottomContainer.isHidden=true
            
        }
        self.comment.delegate = self
        self.feedDetailsTable.tableFooterView = UIView()
        self.feedDetailsTable.reloadData()
        
        self.getRecommended_product(limit: self.limit, offset: self.offset)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let tabBarHeight = tabBarController?.tabBar.bounds.size.height ?? 0
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
        if self.feedImage != "" || self.feedVideo != ""
        {
            return screenSize.height - (topBarHeight + tabBarHeight)
        }
        else
        {
            return UITableViewAutomaticDimension
        }
                }
                
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.feedVideo == "" && self.feedImage == ""
        {
        return 1+self.data.count
        }
        else
        {
            return 1
        }
                }
                
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
                    var reuseCell = UITableViewCell()
                    let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
        if indexPath.row == 0
        {
                    let cell2 = tableView.dequeueReusableCell(withIdentifier: "feedDetails", for: indexPath) as! FeedDetailsTableViewCell
                        cell2.videoController.isHidden=true
                        cell2.imageController.isHidden=true
                        cell2.userConatiner.isHidden=true
                    cell2.likeButton.tag=indexPath.row
                    
            
            
                    if self.isFeedID_liked == true
                    {
                        cell2.likeButton.setImage(UIImage(named: "liked"), for: .normal)
                    }
                    else
                    {
                        cell2.likeButton.setImage(UIImage(named: "like"), for: .normal)
                    }
                var like1 = ""
                var comment = ""
                        cell2.likeButton.tag=indexPath.row
                        cell2.likeButton.addTarget(self, action: #selector(LikeButton(Feed_Index:)), for: .touchUpInside)
                        cell2.commentButton.tag=indexPath.row
                        cell2.commentButton.addTarget(self, action: #selector(comment_data(sender:)), for: .touchUpInside)
                        cell2.shareButton.tag=indexPath.row
                        cell2.shareButton.addTarget(self, action: #selector(shareButton(sender:)), for: .touchUpInside)
                        
                        
                        print("indexPath.row", indexPath.row)
                    
                        if self.feedVideo  != ""
                        {
                            
                         
                            cell2.videoController.isHidden=false
                            cell2.imageController.isHidden=true
                            
//                            cell2.videoController.constant = 300.0
                            let url = self.feedVideo as! String
                                                                     
                        let videoURL = NSURL(string: url)
                            avPlayer = AVPlayer(url: videoURL! as URL)
                            avPlayer.volume = 10
                            avPlayer.isMuted = true
                            let playerController = AVPlayerViewController()
                            playerController.player = avPlayer
                            
                            self.addChildViewController(playerController)
                            
                            // Add your view Frame
                            
        //                                        playerController.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                            playerController.view.frame = cell2.videoController.bounds
                            playerController.showsPlaybackControls = true
                            playerController.videoGravity = AVLayerVideoGravity.resize.rawValue
                            
                            do {
                                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                            } catch _ {
                            }
                            do {
                                try AVAudioSession.sharedInstance().setActive(true)
                            } catch _ {
                            }
                            do {
                                try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                            } catch _ {
                            }
        //                                        avPlayer.automaticallyWaitsToMinimizeStalling = false
                            avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.pause
                            
                            // Add subview in your view
                            cell2.videoController.isHidden = false
                            
                            cell2.videoController.addSubview(playerController.view)
                            playerController.didMove(toParentViewController: self)

                            avPlayer.play()
                        }
                        else if (self.feedImage as? String)! != ""
                        {
                            
                            cell2.videoController.isHidden=true
                            cell2.imageController.isHidden=false
                            
                           
                          let imagedata = self.feedImage

                            var imageUrl = (imagedata)
                            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                            if let url = NSURL(string: imageUrl )
                            {
                                print("url", url)
                                if url != nil
                                {
                                    cell2.imageController.af_setImage(withURL: url as! URL, placeholderImage: UIImage(named: "thumbnail"))
                                }
                            } }
                        else if self.QuestionText != ""
                        {
                            cell2.videoController.isHidden=true
                            cell2.imageController.isHidden=true
                            cell2.userConatiner.isHidden=false
                            if userImage != ""
                            {
                                var imageUrl = userImage as! String
                                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                                var url = NSURL(string:imageUrl as! String )
                                
                                if url != nil{
                                    
                                    cell2.userImage.af_setImage(withURL: url as! URL, placeholderImage: UIImage(named: "thumbnail"))
                                }
                                }
                            if self.uploadTime != 0
                            {
                                if let timeResult = (self.uploadTime as? Double) {
                                    let date = Date(timeIntervalSince1970: timeResult)
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
                                    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                                    dateFormatter.timeZone = .current
                                    let localDate = dateFormatter.string(from: date)
                                    cell2.uploadTime.text = localDate
                                }
                            }
                            cell2.userName.text = self.userName
                            cell2.QuestionText.attributedText = ( "\(self.QuestionText)".fontName("HelveticaNeue-Regular")).attributedText
        }
                       
            reuseCell = cell2
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
            
             let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
             let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
             
             if self.native.object(forKey: "userid") != nil
             {
                 let userid = self.native.string(forKey: "userid")!
                
                if self.data[indexPath.row - 1]["user_id"] is NSNull
                 {}
                 else
                 {   if Int(userid) != (data[indexPath.row - 1]["user_id"] as? Int)!
                 {
                     cell.deleteButton.isHidden = true
                     cell.editButton.isHidden = true
                 }
                     else
                 {
                     cell.deleteButton.isHidden = false
                     cell.editButton.isHidden = false
                 }
                     
                 }
             }
            if self.data[indexPath.row - 1]["profile_pic_url"] is NSNull
             {}
             else
             {

                 let imagedata = data[indexPath.row - 1]["profile_pic_url"] as? String

                    var imageUrl = (imagedata)!
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                    if let url = NSURL(string: imageUrl )
                    {
                        if url != nil
                        {
                            cell.userImage.af_setImage(withURL: url as! URL, placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                 }

             var like = 0
             var comment = 0
             
             
             if data[indexPath.row - 1]["comment"] is NSNull
             {
                 cell.NameAndText.text = ""
             }
             else
             {
                 
                 cell.NameAndText.attributedText = ("\(data[indexPath.row - 1]["first_name"] as! String) \(data[indexPath.row - 1]["last_name"] as! String): ".color(lightBlackColor).size(16.0)).attributedText
                 cell.commentText.attributedText = ( "\(data[indexPath.row - 1]["comment"] as! String)".fontName("HelveticaNeue-Regular")).attributedText
                 if data[indexPath.row - 1]["inserted_at"] is NSNull
                 {}
                 else
                 {
                     if let date1 = self.data[indexPath.row - 1]["inserted_at"] as? String
                    {
                         let date = Date(timeIntervalSince1970: Double(date1)!)
                         let dateFormatter = DateFormatter()
                         dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
                         dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                         dateFormatter.timeZone = .current
                         let localDate = dateFormatter.string(from: date)
                         cell.commentTime.text = localDate
                     }
                 }
                 
                 
             }
             
             
             cell.editButton.tag = indexPath.row - 1
             cell.editButton.addTarget(self, action: #selector(updateEditButton(Feed_Index:)), for: .touchUpInside)
             cell.deleteButton.tag = indexPath.row - 1
             cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
              
             reuseCell = cell
        }
                    
                   return reuseCell
        }

    
    
    
    @objc func comment_data(sender: UIButton)
        {
        
            FeedViewController.user_feed_id = self.user_feedid
            FeedViewController.selectedIndex = sender.tag
            performSegue(withIdentifier: "feedDetailsComment", sender: self)
            
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "feedDetailsComment") {
        guard let destination = segue.destination as? FeedCommentDataViewController else
        {
            print("return data")
           return
        }
              destination.user_feedid = self.user_feedid
            
            }}
        
      
        
        /// like button
        
        @objc func LikeButton(Feed_Index: UIButton)
        {
            let token = self.native.string(forKey: "Token")!
            var fav = 0
                if token != ""
                {
                    CustomLoader.instance.showLoaderView()
                if self.native.object(forKey: "userid") != nil
                {
                let userid = self.native.string(forKey: "userid")!

                let parameters: [String:Any] = ["user_feedid":self.user_feedid]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")

                let b2burl = native.string(forKey: "b2burl")!
                var url = ""
                    
                    if self.isFeedID_liked == true
                {

                    url = "\(b2burl)/users/dislike_user_feed/"
                }
                else
                {
                    url = "\(b2burl)/users/like_user_feed/"
                }
                    
                print("follow product", url)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in

                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)


                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }

                    switch response.result
                    {


                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }

                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                        print("res;;;;;", jsonString)

                        if self.isFeedID_liked == true
                        {
                            if self.like > 0
                            {
                            self.like = self.like - 1
                            }
                            self.isFeedID_liked = false
                        }
                        else
                        {
                            self.like = self.like + 1
                            self.isFeedID_liked = true
                                }

                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                                DispatchQueue.main.async {
                                    self.feedDetailsTable.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)

                                }}
                    }}
                    }
                    }}
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }


        }
//
        
        @objc func shareButton(sender: UIButton) {
            
            
            // image to share
            let text = "Feed Product"
            var productImage = "thumbnail"
            
            let image = UIImage(named: productImage)
            let myWebsite = NSURL(string:self.shareLink)
            let shareAll = [text , image , myWebsite!] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    
    
    /// delete button
       
       @objc func deleteButton(Feed_Index: UIButton)
       {
           let token = self.native.string(forKey: "Token")!
           var fav = 0
               if token != ""
               {
                   CustomLoader.instance.showLoaderView()
               if self.native.object(forKey: "userid") != nil
               {
                   let userid = self.native.string(forKey: "userid")!
               
                  
               //        print("product id:", id)
                   let parameters: [String:Any] = ["user_feed_commentid":(data[Feed_Index.tag]["user_feed_commentid"] as? Int)!]
               let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
               print("Successfully post")
           
               let b2burl = native.string(forKey: "b2burl")!
               var url = "\(b2burl)/users/delete_feed_comment/"
               
               print("follow product", url)
               Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                   
                   
                   guard response.data != nil else { // can check byte count instead
                       let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                       
                       
                       UIApplication.shared.endIgnoringInteractionEvents()
                       return
                   }
                   
                   switch response.result
                   {
                       
                       
                   case .failure(let error):
                       //                print(error)
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let err = error as? URLError, err.code == .notConnectedToInternet {
                           // Your device does not have internet connection!
                           print("Your device does not have internet connection!")
                           self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                           print(err)
                       }
                       else if error._code == NSURLErrorTimedOut {
                           print("Request timeout!")
                           self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                       }else {
                           // other failures
                           self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                       }
                   case .success:
                       let result = response.result
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let dict1 = result.value as? Dictionary<String, AnyObject>{
                           if let invalidToken = dict1["detail"]{
                               if invalidToken as! String  == "Invalid token."
                               { // Create the alert controller
                                   self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                   appDelegate.window?.rootViewController = redViewController
                               }
                           }
                   
                   else if let json = response.result.value {
                       let jsonString = "\(json)"
                       print("res;;;;;", jsonString)
                       
                               
                               if self.data.count > Feed_Index.tag
                               {
                                   self.data.remove(at: Feed_Index.tag)
                                   
//                                   if self.feedComment_screen == 0
//                                   {
                                   FeedViewData.user_Comment_data[FeedViewController.selectedIndex] = self.data
//                                       if FeedViewData.Comment.count > Feed_Index.tag
//                                       {
//                                          if FeedViewData.Comment[FeedViewController.selectedIndex] > 0
//                                           {
//                                               FeedViewData.Comment[FeedViewController.selectedIndex] = FeedViewData.Comment[FeedViewController.selectedIndex] - 1
//                                           }
//                                       }
//                                      print("comment1", FeedViewData.Comment[FeedViewController.selectedIndex])
//
//
//                                   }
//                                   else if self.feedComment_screen == 1
//                                   {
//                                       userFeedPost.user_Comment_data[UserFeedPostViewController.selectedIndex] = self.data
//                                       if userFeedPost.Comment.count>Feed_Index.tag
//                                       {
//                                           if userFeedPost.Comment[UserFeedPostViewController.selectedIndex] > 0
//                                           { userFeedPost.Comment[UserFeedPostViewController.selectedIndex] = userFeedPost.Comment[UserFeedPostViewController.selectedIndex] - 1
//
//                                           }}
//
//                                       DispatchQueue.main.async {userFeedPost.MyFeedPost.reloadRows(at: [IndexPath(row: UserFeedPostViewController.selectedIndex, section: 0)], with: .automatic)
//                                       }
//                                   }
//                                   else if self.feedComment_screen == 2
//                                   {
//                                   myFeedPost.user_Comment_data[MyFeedPostViewController.selectedIndex] = self.data
//                                       if myFeedPost.Comment.count > Feed_Index.tag
//                                       {
//                                           if myFeedPost.Comment[MyFeedPostViewController.selectedIndex] > 0
//                                           { myFeedPost.Comment[MyFeedPostViewController.selectedIndex] = myFeedPost.Comment[MyFeedPostViewController.selectedIndex] - 1
//                                           }}
//
//                                      DispatchQueue.main.async { myFeedPost.MyFeedPost.reloadRows(at: [IndexPath(row: MyFeedPostViewController.selectedIndex, section: 0)], with: .automatic)
//                                       }
//                                   }
//
                               }
                    DispatchQueue.main.async {
                       self.feedDetailsTable.reloadData()
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                           self.feedDetailsTable.reloadData()
                               }
                       }
                   }}
                   }
                   }}
               else
               {
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                   appDelegate.window?.rootViewController = redViewController
               }
           
           
       }
       
       
       
       @objc func updateEditButton(Feed_Index: UIButton)
       {
           self.is_editing = true
           self.editIndex = Feed_Index.tag
           self.comment.becomeFirstResponder()
           self.comment.text = (data[Feed_Index.tag]["comment"] as? String)!
       }
       
       /// like button
       
       func editButton(editIndex: Int)
       {
           let token = self.native.string(forKey: "Token")!
           var fav = 0
               if token != ""
               {
                   CustomLoader.instance.showLoaderView()
               if self.native.object(forKey: "userid") != nil
               {
                   let userid = self.native.string(forKey: "userid")!
               
                  
               //        print("product id:", id)
                   let parameters: [String:Any] = ["user_feed_commentid":(data[editIndex]["user_feed_commentid"] as? Int)!, "comment": self.comment.text!]
               let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
               print("Successfully post")
           
               let b2burl = native.string(forKey: "b2burl")!
               var url = "\(b2burl)/users/edit_feed_comment/"
               
               print("edit comment", url)
               Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                   
                   
                   guard response.data != nil else { // can check byte count instead
                       let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                       
                       
                       UIApplication.shared.endIgnoringInteractionEvents()
                       return
                   }
                   
                   switch response.result
                   {
                       
                       
                   case .failure(let error):
                       //                print(error)
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let err = error as? URLError, err.code == .notConnectedToInternet {
                           // Your device does not have internet connection!
                           print("Your device does not have internet connection!")
                           self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                           print(err)
                       }
                       else if error._code == NSURLErrorTimedOut {
                           print("Request timeout!")
                           self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                       }else {
                           // other failures
                           self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                       }
                   case .success:
                       let result = response.result
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let dict1 = result.value as? Dictionary<String, AnyObject>{
                           if let invalidToken = dict1["detail"]{
                               if invalidToken as! String  == "Invalid token."
                               { // Create the alert controller
                                   self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                   appDelegate.window?.rootViewController = redViewController
                               }
                           }
                   
                   else if let json = response.result.value {
                       let jsonString = "\(json)"
                       print("res;;;;;", jsonString)
                       
                               var first_name = ""
                                       var last_name = ""
                                       var profile_pic_url = "";
                                       var user_feed_commentid = 0;
                                       var user_id = 0
                                       
                                       if (dict1["status"] as? String)! == "success"
                                       {
                                        self.is_editing = false
                                       if self.native.object(forKey: "userid") != nil
                                       {
                                           user_id = Int(self.native.string(forKey: "userid")!)!
                                       }
                                       
                                       if self.native.object(forKey: "profilepic") != nil
                                       {
                                           profile_pic_url = self.native.string(forKey: "profilepic")!
                                       }
                                       if self.native.object(forKey: "firstname") != nil
                                       {
                                           first_name = self.native.string(forKey: "firstname")!
                                       }
                                       if self.native.object(forKey: "lastname") != nil
                                       {
                                           last_name = self.native.string(forKey: "lastname")!
                                       }
                                       if self.native.object(forKey: "userid") != nil
                                       {
                                           last_name = self.native.string(forKey: "userid")!
                                       }
                                       if self.native.object(forKey: "lastname") != nil
                                       {
                                           last_name = self.native.string(forKey: "lastname")!
                                       }
                                   
                               var timestamp = NSDate().timeIntervalSince1970
                               var commentID = (self.data[editIndex]["user_feed_commentid"] as? Int)!
                               if self.data.count > editIndex
                               {
                                   self.data.remove(at: editIndex)
                                   self.data.insert(["comment": self.comment!.text!,
                                            "first_name": first_name,
                                            "inserted_at": timestamp,
                                            "last_name": last_name,
                                            "profile_pic_url": "\(profile_pic_url)",
                                       "user_feed_commentid": commentID,
                                   "user_id": user_id] as AnyObject, at: editIndex)
                                   self.comment.text = ""
                               }
                           DispatchQueue.main.async {
                       
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                           self.feedDetailsTable.reloadData()
                               }
                       }
                   }}
                   }
                   }}
               else
               {
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                   appDelegate.window?.rootViewController = redViewController
               }
        }
           
       }


       
       
    @IBAction func sendComment(_ sender: Any) {
    
           if self.is_editing == true
           {
               self.editButton(editIndex: self.editIndex)
           }
       else
           {
           if comment.text != ""
           {
               self.comment.resignFirstResponder()
           let token = self.native.string(forKey: "Token")!
           var fav = 0
               if token != ""
               {
                   CustomLoader.instance.showLoaderView()
               if self.native.object(forKey: "userid") != nil
               {
                   let userid = self.native.string(forKey: "userid")!
               
                   var user_feedid = 0
                    
                let parameters: [String:Any] = ["user_feedid":self.user_feedid,"comment": comment.text!]
               let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
               print("Successfully post")
           
               let b2burl = native.string(forKey: "b2burl")!
               var url = "\(b2burl)/users/add_comment_to_feed/"
               
               
               print("comment product", url, parameters)
               Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                   
                   
                   guard response.data != nil else { // can check byte count instead
                       let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                       
                       
                       UIApplication.shared.endIgnoringInteractionEvents()
                       return
                   }
                   print("resonce comment", response)
                   
                   switch response.result
                   {
                       
                       
                   case .failure(let error):
                       //                print(error)
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let err = error as? URLError, err.code == .notConnectedToInternet {
                           // Your device does not have internet connection!
                           print("Your device does not have internet connection!")
                           self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                           print(err)
                       }
                       else if error._code == NSURLErrorTimedOut {
                           print("Request timeout!")
                           self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                       }else {
                           // other failures
                           self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                       }
                   case .success:
                       let result = response.result
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let dict1 = result.value as? Dictionary<String, AnyObject>{
                           if let invalidToken = dict1["detail"]{
                               if invalidToken as! String  == "Invalid token."
                               { // Create the alert controller
                                   self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                   appDelegate.window?.rootViewController = redViewController
                               }
                           }
                   
                   else if let json = response.result.value {
                       let jsonString = "\(json)"
                      
                           
                               var first_name = ""
                               var last_name = ""
                               var profile_pic_url = "";
                               var user_feed_commentid = 0;
                               var user_id = 0
                               
                               if dict1["response"] is NSNull
                               {}
                               else
                               {
                                   if let commentData = dict1["response"] as? AnyObject
                                   {
                                       if commentData["user_feed_commentid"] is NSNull
                                       {}
                                       else
                                       {
                                          user_feed_commentid = (commentData["user_feed_commentid"] as? Int)!
                                       }
                                   }
                               }
                               
                               
                               
                               if self.native.object(forKey: "userid") != nil
                               {
                                   user_id = Int(self.native.string(forKey: "userid")!)!
                               }
                               
                               if self.native.object(forKey: "profilepic") != nil
                               {
                                   profile_pic_url = self.native.string(forKey: "profilepic")!
                               }
                               if self.native.object(forKey: "firstname") != nil
                               {
                                   first_name = self.native.string(forKey: "firstname")!
                               }
                               if self.native.object(forKey: "lastname") != nil
                               {
                                   last_name = self.native.string(forKey: "lastname")!
                               }
                               if self.native.object(forKey: "userid") != nil
                               {
                                   last_name = self.native.string(forKey: "userid")!
                               }
                               if self.native.object(forKey: "lastname") != nil
                               {
                                   last_name = self.native.string(forKey: "lastname")!
                               }
                           
                       var timestamp = NSDate().timeIntervalSince1970
                              
                       self.data.append(["comment": self.comment!.text!,
                                            "first_name": first_name,
                                            "inserted_at": timestamp,
                                            "last_name": last_name,
                                            "profile_pic_url": "\(profile_pic_url)",
                                   "user_feed_commentid": user_feed_commentid,
                                   "user_id": user_id] as AnyObject)
                           
                       
                       self.comment.text = ""
                               
                       DispatchQueue.main.async {
                       self.feedDetailsTable.reloadData()
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       
                               }
                       
                       }
                   }}
                   }
                   }}
               else
               {
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                   appDelegate.window?.rootViewController = redViewController
               }
           
           
       }
           else
           {
               self.view.makeToast("Please type comment first.", duration: 2.0, position: .center)
           }
       }
       }
    
    
    func getRecommended_product(limit: Int, offset: Int)
        {
            
    //        CustomLoader.instance.showLoaderView()
            let token = self.native.string(forKey: "Token")!
            print("token9894579", token)
            let b2burl = native.string(forKey: "b2burl")!
            if native.object(forKey: "userid") != nil
            {
                var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed_data/?user_feedid=\(self.user_feedid)&limit=\(limit)&offset=\(offset)") as! URL)
            if token != ""
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            }
            else{
                
                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
            print("yyugdfhjb", a, token)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                debugPrint(response)
                var url =
                print("feed url recommended", a, token)
                
                
                    switch response.result {
                    case .success:
                        
                        
                        if let result1 = response.result.value
                        {
                            let result = response.result
                           
                           print("result feed video", response)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                else if dict1["status"] as! String == "success"
                                {
                                    if let innerdict = dict1["feed_data"] as? AnyObject
                                    {
                                        
                                var feed_users = [AnyObject]()
                                if innerdict.count > 0
                                   {
                                            
                                   var data = [[AnyObject]]()
                                    
                                    
                                    if innerdict["feed_share_link"] is NSNull
                                    {
                                        
                                    }
                                    else
                                    {
                                        self.shareLink = (innerdict["feed_share_link"] as? String)!
                                    }
                                        
                                    if innerdict["like_data"] is NSNull
                                            {
                                                 self.isFeedID_liked=false
                                            }
                                            else
                                            { feed_users.append(innerdict["like_data"] as! AnyObject)
                                                
                                            }
                                            
                                            if self.native.object(forKey: "userid") != nil
                                            {
                                                let userid = self.native.string(forKey: "userid")!
                                                if feed_users.count > 0
                                                {
                                                if userid != ""
                                                {
                                                    var data = feed_users[0] as! AnyObject
                                                    if data.contains(Int(userid))
                                            {
                                                 self.isFeedID_liked  = true
                                                print("isFeedID_liked", "true")
                                            }
                                            else
                                            {  self.isFeedID_liked=false
                                               
                                            }
                                                    
                                                }
                                                else
                                                {
                                                   self.isFeedID_liked=true
                                                }
                                                }}
                                    if innerdict["comment_data"] is NSNull
                                    {
                                        
                                    }
                                    else
                                    { data.append((innerdict["comment_data"] as? [AnyObject])!)
                                         self.data = data[0] as! [AnyObject]
                                    }
                                    if innerdict["media_url"] is NSNull
                                    {}
                                    else
                                    {
                                            if let data = innerdict["media_url"] as? AnyObject
                                            {
                                                if let imagedata = data["image"] as? [AnyObject]
                                               {
                                                   print("image is not empty", imagedata)
                                                   if imagedata.count > 0
                                                   { self.feedImage = (imagedata[0] as? String)!
                                                   }
                                                
                                                }
                                               else if let videodata = data["image"] as? String
                                               { self.feedImage = (data["image"] as? String)!
                                               }
                                               
                                        }
                                            if let data = innerdict["media_url"] as? AnyObject
                                            {
                                                if let imagedata = data["mp4"] as? [AnyObject]
                                               {
                                               if imagedata.count > 0
                                               { self.feedVideo = (imagedata[0] as? String)!
                                               }
                                                
                                                }
                                               else if let videodata = data["mp4"] as? String
                                               { self.feedVideo = (data["mp4"] as? String)!
                                               }
                                        }
                                        
                                        }
                                            
                                        }
                                        
                                        print("data", self.data)
                                     CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                            
                                        
                                    }
                                    }}}
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                }
                
                DispatchQueue.main.async {
                self.feedDetailsTable.isHidden = false
//                self.refreshControl.endRefreshing()
                self.feedDetailsTable.reloadData()
                }
                }
                
                DispatchQueue.main.async {
                    self.feedDetailsTable.isHidden = false
//                    self.refreshControl.endRefreshing()
                    self.feedDetailsTable.reloadData()
//                    self.refreshControl.endRefreshing()
                    CustomLoader.instance.hideLoaderView()
                }
                UIApplication.shared.endIgnoringInteractionEvents()
                    
                
            }
            else
            {
//                self.refreshControl.endRefreshing()
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
            }
            
        }
        
}
