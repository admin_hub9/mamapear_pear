//
//  FeedDetailsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 27/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class FeedDetailsTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var userConatiner: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var uploadTime: UILabel!
    
    
    @IBOutlet weak var viewController: UIView!
    @IBOutlet weak var videoController: UIView!
    @IBOutlet weak var imageController: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var QuestionText: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
