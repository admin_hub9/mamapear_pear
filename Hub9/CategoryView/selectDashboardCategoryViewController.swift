//
//  selectDashboardCategoryViewController.swift
//  Hub9
//
//  Created by Deepak on 3/13/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire

var selectCategory = selectDashboardCategoryViewController()

class selectDashboardCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var native = UserDefaults.standard
    var parentCategoryData = [AnyObject]()
    var CategoryData = [AnyObject]()
    var selectedIndex = 0
    
    @IBOutlet weak var selectCategoryTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectCategory = self
        selectCategoryTable.tableFooterView = UIView()
        
        self.getCategoryData()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if parentCategoryData[indexPath.row]["children"] is NSNull
        {
            self.dismiss(animated: true, completion: nil)
            
        }
        else
        {
            
            self.CategoryData = parentCategoryData[indexPath.row]["children"] as! [AnyObject]
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "DashboardSubCategory"))! as UIViewController
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
            //        self.navigationController?.popViewController(animated: true)
            self.navigationController?.pushViewController(rootPage, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parentCategoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashCategory", for: indexPath) as! SelectDashboardCategoryTableViewCell
        if parentCategoryData[indexPath.row]["name"] is NSNull
        {}
        else
        {
            if let name = parentCategoryData[indexPath.row]["name"] as? String
            {
                cell.categoryName.text = name
            }
        }
        return cell
    }
    
    
    
    
    func getCategoryData()
    {
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                debugPrint("Successfully post")
                let b2burl = native.string(forKey: "b2burl")!
                Alamofire.request("\(b2burl)/product/category_suggestion/", encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        //                    self.blurView.alpha = 0
                        //                    self.activity.stopAnimating()
                        //                    self.activityIndicator.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    if let json = response.result.value {
                        debugPrint("JSON: \(json)")
                        let jsonString = "\(json)"
                        let result = response.result
                        switch response.result
                        {
                        case .failure(let error):
                            //                debugPrint(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                debugPrint("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                debugPrint(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                debugPrint("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                    
                                else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                                {
                                    //                            self.blurView.alpha = 0
                                    //                            self.activity.stopAnimating()
                                    debugPrint("userdetails", dict1)
                                    if dict1["status"] as! String == "success"
                                    {
                                        if dict1["data"] is NSNull
                                        {}
                                        else
                                        {
                                            if let data = dict1["data"] as? [AnyObject]
                                            {
                                                
                                                self.parentCategoryData = data
                                                
                                            }}
                                        DispatchQueue.main.async {
                                            self.selectCategoryTable.reloadData()
                                        }
                                    }
                                    else
                                    {
                                        self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                                    }}
                                else
                                {
                                    self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                                }
                                
                                UIApplication.shared.endIgnoringInteractionEvents()
                                return
                            }}}
                }}
        }
    }
}
