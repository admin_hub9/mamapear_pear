//
//  selectdashboardSubCategoryViewController.swift
//  Hub9
//
//  Created by Deepak on 3/13/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire

var subCategory1 = selectdashboardSubCategoryViewController()

class selectdashboardSubCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var Category = [AnyObject]()
    var subCategory = [AnyObject]()
    static var subcategoryid = 0
    var native = UserDefaults.standard
    
    
    @IBOutlet weak var selectSubCategoryTable: UITableView!
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subCategory1 = self
//        var category = self.native.string(forKey: "subCategory")!
//        self.navigationController?.navigationBar.topItem?.title = "\(category)"
        selectSubCategoryTable.tableFooterView = UIView()
        
        self.Category = selectCategory.CategoryData
        
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if Category[indexPath.row]["children"] is NSNull
        {
            self.dismiss(animated: true, completion: nil)

        }
        else
        {
        self.subCategory = Category[indexPath.row]["children"] as! [AnyObject]
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "subCategory"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
//            self.navigationController?.popViewController(animated: true)
        self.navigationController?.pushViewController(rootPage, animated: true)
       
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Category.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashSubCategory", for: indexPath) as! selectDashboardSubCategoryTableViewCell
        if self.Category[indexPath.row]["name"] is NSNull
        {}
        else
        {
        cell.subCategoryName.text = self.Category[indexPath.row]["name"] as? String
        }
        return cell
    }
    
}
