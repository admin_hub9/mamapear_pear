//
//  subCategoryViewController.swift
//  Hub9
//
//  Created by Deepak on 25/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire


class subCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var Category = [AnyObject]()
    var subCategory = [AnyObject]()
    static var subcategoryid = 0
    var native = UserDefaults.standard
    
    
    @IBOutlet weak var selectSubCategoryTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        var category = self.native.string(forKey: "subCategory")!
        //        self.navigationController?.navigationBar.topItem?.title = "\(category)"
        selectSubCategoryTable.tableFooterView = UIView()
        
        self.Category = subCategory1.subCategory
        
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.Category[indexPath.row]["category"] is NSNull
        {}
        else if let name = self.Category[indexPath.row]["category"] as? String
        {
            
            if AddDealsView.isDealCategoryActive == true && AddDealsViewController.isediting == false
            {
                //            debugPrint("name", name)
                AddDealsView.categoryText = name
                AddDealsView.categoryid = (self.Category[indexPath.row]["id"] as? Int)!
                self.navigationController?.popToViewController ((self.navigationController?.viewControllers[1]) as! AddDealsViewController, animated: true)
            }
            else if AddDealsView.isDealCategoryActive == true && AddDealsViewController.isediting == true
            {
                //            debugPrint("name", name)
                AddDealsView.categoryText = name
                AddDealsView.categoryid = (self.Category[indexPath.row]["id"] as? Int)!
                self.navigationController?.popToViewController ((self.navigationController?.viewControllers[2]) as! AddDealsViewController, animated: true)
            }
            else if UserFeed.isReviewCategoryActive == true && UserFeedUploadViewController.is_edit == false
            {
                UserFeedUploadViewController.categoryText = name
                UserFeed.categoryid = (self.Category[indexPath.row]["id"] as? Int)!
                self.navigationController?.popToViewController ((self.navigationController?.viewControllers[1]) as! UserFeedUploadViewController, animated: true)
            }
            else if UserFeed.isReviewCategoryActive == true && UserFeedUploadViewController.is_edit == true
            {
                UserFeedUploadViewController.categoryText = name
                UserFeed.categoryid = (self.Category[indexPath.row]["id"] as? Int)!
                self.navigationController?.popToViewController ((self.navigationController?.viewControllers[2]) as! UserFeedUploadViewController, animated: true)
            }
            else if UploadUserClosetViewController.is_Editing == false && UserCloset.isClosetCategoryActive == true
            {
                UploadUserClosetViewController.conditionID = (self.Category[indexPath.row]["id"] as? Int)!
                UploadUserClosetViewController.categoryText = name
                self.navigationController?.popToViewController ((self.navigationController?.viewControllers[2]) as! UploadUserClosetViewController, animated: true)
            }
            else if UploadUserClosetViewController.is_Editing == true && UserCloset.isClosetCategoryActive == true
            {
                UploadUserClosetViewController.conditionID = (self.Category[indexPath.row]["id"] as? Int)!
                UploadUserClosetViewController.categoryText = name
                self.navigationController?.popToViewController ((self.navigationController?.viewControllers[3]) as! UploadUserClosetViewController, animated: true)
            }
            
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Category.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashSubCategory", for: indexPath) as! selectDashboardSubCategoryTableViewCell
        if self.Category[indexPath.row]["category"] is NSNull
        {}
        else
        {
            cell.subCategoryName.text = self.Category[indexPath.row]["category"] as? String
        }
        return cell
    }
    
}
