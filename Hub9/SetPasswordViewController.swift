//
//  SetPasswordViewController.swift
//  Hub9
//
//  Created by Deepak on 6/14/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import Alamofire

class SetPasswordViewController: UIViewController,GIDSignInDelegate, UITextFieldDelegate {
    
    var window: UIWindow?
    let native = UserDefaults.standard
    var dict : [String : AnyObject]!
    var exists = false
    var FBname: String = String()
    var FBid: String = String()
    var FBemail: String = String()
    var FBImage: Data = Data()
    var fbEmailToUsernameuserName: String? //store fb username (username is email provided by FB)
    var fbIdToPassword: String?    //store fb password (password is ID Token provided by FB)
    var screenSize: CGRect?
    var result = [AnyObject]()
    var useremail = ""
    var userpassword = ""
    
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var email: UITextField!
    @IBOutlet var facebook: UIButton!
    @IBOutlet var password: UITextField!
    @IBOutlet var finishSignUp: UIButton!
    @IBOutlet var activity: UIActivityIndicatorView!
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    
    @IBAction func facebookLogin(_ sender: Any) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            
            //if error just kick out the user
            if(error != nil )
            {
                print("Error Logging In\n")
                print(error?.localizedDescription)
                return
            }
            
            //if no error from fbLoginManager
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                // print(fbloginresult.grantedPermissions)
                
                //check user loged in or not
                if let fbUserToken = result?.token{
                    
                    //if user login with correct email
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData() //call function for get acces of email, name first name, last name & profile pic
                        
                        //self.facebookForRealm(self.FBemail, id: self.FBid, name: self.FBname) // Facebook login/check/save for realm
                        
                        //fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    //#MARK: Fetch Facebook Data
    func getFBUserData() {
        facebook.isHidden = true
        //        loginActivityIndicator.startAnimating()
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    print("FB GraphRequest Result: \(result)")
                    
                    let fbResult : [String:Any]? = result as? [String:Any]
                    
                    if fbResult != nil {
                        
                    }
                    
                    DispatchQueue.main.async(execute: {     // Performing actions on main queue
                        
                        // saving fetched results into intermediate variable
                        self.FBid = (fbResult!["id"] as! NSString) as String
                        self.FBname = (fbResult!["name"] as! NSString) as String
                        self.FBemail = (fbResult!["email"] as! NSString) as String
//                        self.FBImage = self.getProfPic(self.FBid)!.pngData()!
                        print("dfsewfw",self.FBemail, self.FBid)
                        self.native.set((fbResult!["email"] as! NSString) as String, forKey: "username")
                        self.native.set((fbResult!["id"] as! NSString) as String, forKey: "password")
                        self.native.synchronize()
                        self.ThirdPartyLogin(email: (fbResult!["email"] as! NSString) as String, password: (fbResult!["id"] as! NSString) as String)
                        
                    })
                    self.facebook.isHidden = false
                }
            })
        }
        
    }
    // #MARK: Fetch facebook profile picture
    func getProfPic(_ fid: String) -> UIImage? {
        if (fid != "") {
            let imgURLString = "http://graph.facebook.com/" + fid + "/picture?type=large" //type=normal
            let imgURL = URL(string: imgURLString)
            let imageData = try? Data(contentsOf: imgURL!)
            let image = UIImage(data: imageData!)
            
            return image
        }
        return nil
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func gmailLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        //if any error stop and print the error
        if error != nil{
            print(error ?? "google error")
            return
        }
        else
        {
            print("successfully Login")
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            print("username", fullName)
            print("userId", userId!)
            print("idToken", idToken)
            print("email", email)
            print("given name", givenName)
            print(user.profile.email!, userId!)
            native.set(user.profile.email!, forKey: "username")
            native.set(userId!, forKey: "password")
            native.synchronize()
            ThirdPartyLogin( email: user.profile.email!, password: userId!)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        email.resignFirstResponder()
        password.resignFirstResponder()
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= 80
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += 80
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)

        email.delegate = self
        password.delegate = self
        ///next button
        
        blurView.alpha = 0
        activity.stopAnimating()
        finishSignUp.layer.cornerRadius = finishSignUp.layer.frame.size.height/2
        finishSignUp.layer.masksToBounds = true
        let loginstartColor = UIColor(red: 238/255.0, green: 90/255.0, blue: 95/255.0, alpha: 1.00).cgColor
        let loginendColor = UIColor(red: 241/255.0, green: 146/255.0, blue: 152/255.0, alpha: 1.00).cgColor
        finishSignUp.applyendGradient(colors: [loginstartColor, loginendColor])
        
        ///textfields for email
        let leftView = UILabel(frame: CGRect(x: 10, y: 0, width: 7, height: 26))
        leftView.backgroundColor = .clear
        
        email.leftView = leftView
        email.leftViewMode = .always

        var frame: CGRect = email.frame
        frame.size.height = 40
        email.frame = frame
        password.delegate = self
        email.layer.cornerRadius = email.layer.frame.height/2
        email.layer.borderWidth = 1
        email.layer.borderColor = UIColor.lightGray.cgColor
        email.layer.masksToBounds = true
        ///textfields for password
        let leftView1 = UILabel(frame: CGRect(x: 10, y: 0, width: 7, height: 26))
        leftView1.backgroundColor = .clear
        password.leftView = leftView1
        password.leftViewMode = .always
//
        var frame1: CGRect = password.frame
        frame1.size.height = 40
        password.frame = frame1
        password.layer.cornerRadius = password.layer.frame.height/2
        password.layer.borderWidth = 1
        password.layer.borderColor = UIColor.lightGray.cgColor
        password.layer.masksToBounds = true


    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 6 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,}")
        return passwordTest.evaluate(with: testStr)
    }
    func validate(YourEMailAddress: String) -> Bool {
        let REGEX: String
        print("REGEX")
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: YourEMailAddress)
    }
    
    func validation() -> Bool {
        var valid: Bool = true
        if ((email.text?.isEmpty))! {
            // change placeholder color to red color for textfield email-id
           
            self.email.attributedPlaceholder = NSAttributedString(string: "not empty", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
    if self.validate(YourEMailAddress: email.text!) == false {
            print("Invalid email address") // prompt alert for invalid email
            email.text?.removeAll()
        self.email.attributedPlaceholder = NSAttributedString(string: "Invalid email address", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        
//        if self.isValidPassword(testStr: password.text) == false {
        if (password.text?.count)! <= 5
        {
             // prompt alert for invalid password
            password.text?.removeAll()
            self.password.attributedPlaceholder = NSAttributedString(string: "Enter atleast 6 digits", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        
        
        return valid
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    @IBAction func finishSignUpButton(_ sender: Any) {
         if validation() == true
         {
        if email.text?.count != 0 && password.text?.count != 0
        {   blurView.alpha = 0.7
            native.set(email.text, forKey: "username")
            native.set(password.text, forKey: "password")
            native.synchronize()
            activity.startAnimating()
            let parameters: [String:Any] = ["email":email.text, "password":password.text]
            let header = ["Content-Type": "application/json"]
            print("Successfully post")
            
            Alamofire.request("https://demo.hub9.io/api/signup_user/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "here seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.blurView.alpha = 0
                    self.activity.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                if let json = response.result.value {
                                    print("JSON: \(json)")
                
                    let jsonString = "\(json)"
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        
                        if (dict1["status"]as AnyObject) as! String  == "success"
                        {   self.blurView.alpha = 0
                            self.activity.stopAnimating()
                            print(dict1["status"]!)
                        print(dict1["response"]!)
                            print(dict1["firebaseid"]!)
                            self.native.set(dict1["firebaseid"]!, forKey: "firebaseid")
                            self.native.set(dict1["userid"]!, forKey: "userid")
                            self.native.synchronize()
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        else
                        {
                            self.blurView.alpha = 0
                            self.activity.stopAnimating()
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                            
                        }
                    }
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                }
            }
        }
        }
    }
    func ThirdPartyLogin(email:String,password:String)
    {
        
        let parameters: [String:Any] = [ "email":email, "password":password]
        
        let header = ["Content-Type": "application/json"]
        print("Successfully post")
        
        Alamofire.request("https://demo.hub9.io/api/thirdLogin/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                //                    self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            
            if let json = response.result.value {
                print("JSON: \(json)")
                var errorval = 0
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                    else if (dict1["status"]as AnyObject) as! String  == "SUCCESS" || (dict1["status"]as AnyObject) as! String  == "success"
                    {   print("success")
                        if (((dict1["token"]as AnyObject) as? String) != nil)
                        {   self.blurView.alpha = 0
                            self.activity.startAnimating()
                            let dict2 = (dict1["token"]as AnyObject)
                            print(((dict1["token"]as AnyObject) as? String) as Any )
                            
                            self.native.set(((dict1["token"]!as AnyObject) as? String), forKey: "Token")
                            let userData = dict1["user_data"] as! NSDictionary
                            //                            print(userData["firebase_userid"] as? String as Any)
                            if userData["firebase_userid"] is NSNull
                            {
                                errorval = 1
                            }
                            else
                            {
                                self.native.set(userData["firebase_userid"] as? String, forKey: "firebaseid")
                                print("firebase", userData["firebase_userid"] as? String)
                            }
                            if userData["account_type_lkpid"]! is NSNull
                            {
                                errorval = 1
                            }
                            else
                            {
                                self.native.set((userData["account_type_lkpid"]!), forKey: "usertype")
                                print("account type", (userData["account_type_lkpid"]! as? String))}
                            if userData["userid"]! is NSNull && userData["shopid"]! is NSNull &&
                                userData["companyid"]! is NSNull
                            {
                                errorval = 1
                                
                            }
                            if errorval == 0
                            {
                                self.native.set(userData["userid"]! as? String, forKey: "userid")
                                self.native.set(userData["shopid"]! as? String, forKey: "shopid")
                                self.native.set(userData["companyid"]! as? String, forKey: "companyid")
                                self.native.synchronize()
                                let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "rootController"))! as UIViewController
                                self.present(rootPage, animated: false, completion: nil)
                            }
                            
                        }
                        else
                        {
                            self.blurView.alpha = 0
                            self.activity.startAnimating()
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                            
                            let innerdict = dict1["user_data"] as! NSDictionary
                            print(innerdict["userid"]!)
                            if innerdict["firebase_userid"]! != nil
                            {
                                print(innerdict["firebase_userid"] as? String)
                                self.native.set(innerdict["firebase_userid"] as? String, forKey: "firebaseid")
                                self.native.set(innerdict["userid"]!, forKey: "userid")
                            }
                            self.native.synchronize()
                            print(innerdict)
                        }
                    }
                    else
                    {
                        self.blurView.alpha = 0
                        self.activity.stopAnimating()
                        print(dict1["status"] as AnyObject)
                        print("responce",dict1["response"] as AnyObject)
                        print("hello")
                        if (dict1["response"] as AnyObject) as! String == "OTP verification pending"
                        {   self.native.set(dict1["userid"]! as? String, forKey: "userid")
                            self.native.synchronize()
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        else if (dict1["response"] as AnyObject) as! String == "User Created Successfully"
                        {   self.native.set(dict1["userid"]! as? String, forKey: "userid")
                            self.native.synchronize()
                            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "user"))! as UIViewController
                            self.present(rootPage, animated: false, completion: nil)
                        }
                        else{
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                            
                        }
                    }
                }
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                    
                    
                    
                }
            }
            
        }}}

