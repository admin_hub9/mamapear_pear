//
//  ShopMainViewController.swift
//  Hub9
//
//  Created by Deepak on 5/9/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire

class ShopMainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    let native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    
    
    var img = [[AnyObject]]()
    var shopID = [AnyObject]()
    var ProductImg = [AnyObject]()

    var shopcount = 0
    var filterdata = [String]()
    var isSearching = false
    var filterclick = 0
    var type = ""
    var Pfilterdata = ["Landmark"]
    var limit = 10
    var offset = 0
    var cellType = 0
    var Category = [AnyObject]()
    var brand = [AnyObject]()
    var selectedCategory = [Int]()
    var selectedbrand = [Int]()
    var sort = ["asc", "desc"]
    var minPrice = 0
    var maxPrice = 10000
    var selectedSort = "asc"
    
    var follow = 0
    var id = 0
    var indexis = 0

    @IBOutlet var ActivityGravity: UIActivityIndicatorView!
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var filtershadwo: UIView!
    @IBOutlet var tableshadwo: UIView!
    @IBOutlet weak var Shoptable: UITableView!
    @IBOutlet var ParentFilter: UITableView!
    @IBOutlet var ChildFilter: UITableView!
    @IBOutlet var CancelFilter: UIButton!
    @IBOutlet var applyFilter: UIButton!
    @IBOutlet var filterView: UIView!
    
    @IBOutlet weak var searchBar: UILabel!
    
    

    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
         self.navigationController?.setNavigationBarHidden(true, animated: true)
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
             statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
             UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        CustomLoader.instance.showLoaderView()
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        CustomLoader.instance.showLoaderView()
        Shoptable.separatorStyle = .none
        Shoptable.tableFooterView = UIView()
        
        if native.object(forKey: "search") != nil
        {
        searchBar.text = native.string(forKey: "search")!
        }
//        if native.string(forKey: "type")! == "dash"
//        {
//          searchBar.text = native.string(forKey: "searchtext")!
//        }
//        else
//        {
//        searchBar.text = searchBarViewController.searchText
//        }
//        self.Shoptable.showEmptyListMessage("")
        self.Shoptable.isHidden = true
        parseData()
        blurView.alpha = 0.4
        ActivityGravity.startAnimating()
        self.ParentFilter.separatorStyle = .none
        self.ChildFilter.separatorStyle = .none
        self.ParentFilter.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        self.filterView.isHidden=true
        self.filtershadwo.isHidden = true
    }
  
 
    @IBAction func CancelFilterButton(_ sender: Any) {
        blurView.alpha = 0
        filterView.isHidden = true
    }
    
   
    @IBAction func filterButton(_ sender: Any) {
        
        UIView.transition(with: filterView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                self.blurView.alpha = 0.7
                self.filterView.transform = CGAffineTransform(translationX: 0, y: 0)
                
                self.filterView.isHidden=false
        }, completion: nil)
        
    }
    
    @objc func enterShop(sender: UIButton) {
        id = shopID[sender.tag] as! Int
        native.set(id, forKey: "entershopid")
        native.synchronize()
        print("product id:", id)
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shopDetails"))! as UIViewController

        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    func parseData()
    {
//        let Token = self.native.string(forKey: "Token")!
//        if Token != nil
//        {
            shopID.removeAll()
            var text = ""
            if native.string(forKey: "type")! == "dash"
            {
                text = native.string(forKey: "searchtext")!
            }
            else
            {
                text = searchBarViewController.searchText
            }
            let b2burl = native.string(forKey: "b2burl")!
            var validateUrl = "\(b2burl)/search/search/?search_type=shop_elastic&search_query=\(text)&limit=10&offset=0"
            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            var a = URLRequest(url: NSURL(string: validateUrl) as! URL)
            let token = self.native.string(forKey: "Token")!
            if token != nil
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            }
            else{
                
                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                                                print("response: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            let alertController = UIAlertController(title: "", message: "Your Seesion Expired Please Login again to continue", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                NSLog("OK Pressed")
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                            // Add the actions
                            alertController.addAction(okAction)
                            //                            alertController.addAction(cancelAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                     else if let innerdict = dict1["data"] as? [AnyObject]{
                        self.ProductImg = innerdict as! [AnyObject]
                        for var i in 0..<innerdict.count
                        {
                            self.shopID.append(innerdict[i]["shopid"] as! AnyObject)
                        }
                      
                    }
                    else if let invaliddata = dict1["status"]{
                        if invaliddata as! String  == "failure"
                        { // Create the alert controller
                            self.blurView.alpha = 0
                            self.ActivityGravity.stopAnimating()
                            let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }}}
                    }
                }
                if self.ProductImg.count > 0
                {
                     self.Shoptable.showEmptyListMessage("")
                    self.Shoptable.isHidden = false
                    self.blurView.alpha = 0
                    self.ActivityGravity.stopAnimating()
//                    print("hello--")
                    self.Shoptable.reloadData()
                    
                }
                else
                {   CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.Shoptable.isHidden = false
                    self.Shoptable.showEmptyListMessage("No Shop Found!")
                    let alert = UIAlertController(title: "Alert", message: "No data found", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                }
            
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    
    func filterData()
    {
        let Token = self.native.string(forKey: "Token")!
        if Token != nil
        {   var text = searchBarViewController.searchText
            print(Token)
            print(limit, offset)
            let b2burl = native.string(forKey: "b2burl")!
            
            var validateUrl = ""
            
            validateUrl = "\(b2burl)/search/search/?search_type=shop_filter_elastic&search_query=\(text)&limit=\(limit)&offset=\(offset)&brand=\(selectedbrand)&category=\(selectedCategory)&min_price=\(minPrice)&max_price=\(maxPrice)&sort=\(selectedSort)"
            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            
            print("/////", validateUrl)
            var a = URLRequest(url: NSURL(string: validateUrl) as! URL)
            let token = self.native.string(forKey: "Token")!
            if token != nil
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(Token)"]
            }
            else{
                
                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if dict1["status"] as! String == "failure"
                    {
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                        let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        //                        self.dismiss(animated: true, completion: nil)
                        self.present(alert, animated: true, completion: nil)
                    }
                        
                    else{
                        
                        let innerdict = dict1["data"] as! [AnyObject]
                        //                       print(innerdict)
                        if innerdict.count >= 1
                        {
                            if let brand = innerdict[0]["brand"] as? [AnyObject]
                            {
                                self.brand = brand
                            }
                            if let category = innerdict[1]["category"] as? [AnyObject]
                            {
                                self.Category = category
                            }
                        }
                        }}
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.ChildFilter.reloadData()
                }
                }
            }
        }
    }
    
    
    
//    @IBAction func backButton(_ sender: Any) {
//        CustomLoader.instance.hideLoaderView()
//        UIApplication.shared.endIgnoringInteractionEvents()
//    self.navigationController?.popViewController(animated: true)
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == Shoptable
        {
        return 220
        }
        else
        {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ParentFilter
        {
            return Pfilterdata.count
        }
        else if tableView == ChildFilter
        {
            return 1
        }
        if ProductImg.count > 0 && tableView == Shoptable
        {
        return ProductImg.count
        }
            return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == Shoptable
        {
        
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReturnCell = UITableViewCell()
        if tableView == Shoptable
        {
        let cell = self.Shoptable.dequeueReusableCell(withIdentifier: "shopcell") as! ShopTableViewCell
        cell.shopcell.layer.masksToBounds = false
        cell.clipsToBounds = true
//        cell.shopcell.layer.shadowColor = UIColor.lightGray.cgColor
//        cell.shopcell.layer.shadowOpacity = 1
//        cell.shopcell.layer.shadowOffset = CGSize(width: 1, height: 2)
//        cell.shopcell.layer.shadowRadius = 3
//        cell.shopcell.layer.cornerRadius = 8
        cell.shopimage.layer.cornerRadius = (cell.shopimage.frame.height / 2)
        cell.shopimage.clipsToBounds = true
        
        if self.ProductImg[indexPath.row]["is_fav"] as? Int == 1
        {
        cell.followButton.setTitle("Unfollow", for: .normal)
        }
        else
        {
            cell.followButton.setTitle("Follow", for: .normal)
        }
        cell.followButton.tag = indexPath.row
        // Add a target to your button making sure that you return the sender like so:
//        cell.followButton.addTarget(self, action: #selector(handleButtonTapped(sender:)), for: .touchUpInside)
            var state = ""
            var country = ""
            if self.ProductImg[indexPath.row]["shop_country_name"] is NSNull
            {}
            else
            {
                country = self.ProductImg[indexPath.row]["shop_country_name"] as! String
            }
            if self.ProductImg[indexPath.row]["shop_state"] is NSNull
            {}
            else
            {
                state = self.ProductImg[indexPath.row]["shop_state"] as! String
            }
            cell.address.text = "\(state) \(country)"
            cell.title.text = self.ProductImg[indexPath.row]["shop_name"] as? String
//            let image = self.Imagetype[0]["image_url"] as! String
            let image = self.ProductImg[indexPath.row]["shop_logo"] as? String
            let url = NSURL(string:image as! String)
            
            DispatchQueue.main.async {
                cell.shopimage.sd_setImage(with: URL(string: image!), placeholderImage: UIImage(named: "thumbnail"))
            }
            cell.enterShop.tag = indexPath.row
            cell.enterShop.addTarget(self, action: #selector(enterShop(sender:)), for: .touchUpInside)
//        cell.enterShop.layer.cornerRadius = 5
//        cell.enterShop.layer.borderWidth = 1
//        cell.enterShop.layer.borderColor = UIColor.blue.cgColor
        shopcount = indexPath.row
            
        cell.shopCollection.tag = indexPath.row
        cell.shopCollection.reloadData()
            
            ReturnCell = cell
        }
        else if tableView == ParentFilter
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "filter", for: indexPath) as! FilterTableViewCell
            cell.name.text = Pfilterdata[indexPath.row]
            ReturnCell = cell
        }
        else if tableView == ChildFilter
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "subfilter", for: indexPath) as! SubFilterTableViewCell
            cell.name.text = "kirti nagar"
            cell.name.frame.size.width = 40
            ReturnCell = cell
            
            
        }
        return ReturnCell
    }
    

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = 0.0
        var height = 0.0
        width = Double(CGFloat(screenWidth / 2 - 15))
        height = Double(CGFloat(Double(screenHeight) / 3 + 20))
        return CGSize(width: width, height: height )
    }

    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        var featured_product = (self.ProductImg[collectionView.tag]["featured_product"] as? NSDictionary)!
        if let data = featured_product["featured_product_meta"] as? [AnyObject]
        {
//            print("------12-", collectionView.tag, data.count)
            if data.count == nil{
                count = 0
            }
            else{
                count = (data.count)
            }
        }
        
        
        return count
    }
    

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! DashboardShopCollectionViewCell
//          print("index", indexPath.row, collectionView.tag)
//        cell.shadwo.layer.cornerRadius = 3.0
//        cell.shadwo.clipsToBounds = true
        let featured_product = (self.ProductImg[collectionView.tag]["featured_product"] as? NSDictionary)!
        if let Producttype = featured_product["featured_product_meta"] as? [AnyObject]
        {
            cell.productName.text = Producttype[indexPath.row]["title"] as? String
            if Producttype[indexPath.row]["product_variant_meta"] is NSNull
            {}
            else
            {
                let price = Producttype[indexPath.row]["product_variant_meta"] as! [AnyObject]
                if price != nil
                {
                    if price[0]["bulk_price"] is NSNull
                    {}
                    else
                    {
                        if native.string(forKey: "Token")! == ""
                        {let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                            let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                            cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                        }
                        else
                        {
                        cell.price.text = "\(price[0]["currency_symbol"] as! String)\(price[0]["bulk_price"] as! NSNumber)"
                        }
                    }
                }
            }
            if Producttype[indexPath.row]["feature_name"] is NSNull
            {
                cell.Producttag.text = ""
            }
            else
            {
                if Producttype[indexPath.row]["feature_name"] as! String != "Default"
                {
                    cell.Producttag.text = " \(Producttype[indexPath.row]["feature_name"] as! String) "
                    cell.Producttag.isHidden = false
                }
                else
                {
                    cell.Producttag.isHidden = true
                }
            }
            
            
            if let image = Producttype[indexPath.row]["image_url"] as? NSDictionary
            {
             if image["200"] is NSNull
             {}
             else
             {
                if let disImage = image["200"] as? [AnyObject]
                {
                    if disImage.count > 0
                    {
                let image = (disImage[disImage.count - 1] as? String)!
                let urlString = image.replacingOccurrences(of: " ", with: "%20")
//                print("image", urlString)
                if verifyUrl(urlString: urlString) == false
                {
//                    print("invalid url")
                    cell.shopProductImage.image = UIImage(named: "Default-coming-soon-(1)")
                }
                else{
                    if disImage.count > 0
                    {
//                        print(urlString)
                    let url = NSURL(string:urlString)
                        DispatchQueue.main.async {
                            cell.shopProductImage.sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }}
                    }}
            }
            }
            
        }
            return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let featured_product = (self.ProductImg[collectionView.tag]["featured_product"] as? NSDictionary)!
        if let Producttype = featured_product["featured_product_meta"] as? [AnyObject]
        {
            if let variantdata = Producttype[indexPath.row]["product_variant_meta"] as? [AnyObject]
            {
                if variantdata.count > 0
                {
                 let variantid = variantdata[0]["product_variantid"] as! NSNumber
                native.set(variantid, forKey: "FavProID")
                native.set("product", forKey: "comefrom")
                native.synchronize()
                //        currntProID = self.ProductID[indexPath.row] as! Int
               
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
                }

            }
        }
        
    }
    func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
}


