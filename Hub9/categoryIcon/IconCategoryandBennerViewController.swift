
//  IconCategoryandBennerViewController.swift
//  Hub9
//
//  Created by Deepak on 9/7/19.
//  Copyright © 2019 Deepak. All rights reserved.


import UIKit
import Alamofire
import AttributedTextView
import SDWebImage

class IconCategoryandBennerViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var returnImage:UIImage = UIImage()
    let native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var user_purchase_modeid = "1"
    
    
    var isSearching = false
    var limit = 20
    var offset = 0
    var cellType = 1
    var count = 0
    var Category = [AnyObject]()
    var brand = [AnyObject]()
    var selectedCategory = [Int]()
    var selectedbrand = [Int]()
    var selectedSort = "asc"
    
    var ProductName = [AnyObject]()
    var shopName = [AnyObject]()
    var shopaddress = [AnyObject]()
    var ProductPrice = [Double]()
    var ProductTag = [String]()
    var RetailPrice = [Double]()
    var ProductMOQ = [AnyObject]()
    var ProductImg = [AnyObject]()
    var VariantID = [AnyObject]()
    var featureTag = [String]()
    var productSaveTag = [AnyObject]()
    var Img = [AnyObject]()
    
    var minPrice = 0
    var maxPrice = 5000
    var Currency = ""
    
    
    
    @IBOutlet var productCollection: UICollectionView!
    @IBOutlet weak var cellStyle: UIButton!
    @IBOutlet weak var searchText: UILabel!
    @IBOutlet weak var navBarImage: UIImageView!
    
    @IBOutlet weak var collectionTopConstraint: NSLayoutConstraint!
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //Scroll view delegate method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.panGestureRecognizer.translation(in: scrollView).y < 10{
            //scrolling down
            self.navBarImage.isHidden=true
            self.collectionTopConstraint.constant = -121
        }
        else{
            //scrolling up
            self.navBarImage.isHidden=false
            self.collectionTopConstraint.constant = 2
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        self.tabBarController?.tabBar.isHidden = true
        CustomLoader.instance.showLoaderView()
        if let savedValue = native.string(forKey: "bannerImage"){
            debugPrint("Here you will get saved value")
            var image = (native.string(forKey: "bannerImage") as? String)!
            
            image = image.replacingOccurrences(of: " ", with: "%20")
            let url = NSURL(string: image)
            if url != nil{
                DispatchQueue.main.async {
                    self.navBarImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "category_thumbnail"))
                }
            }
        } else {
            debugPrint("No value in Userdefault,Either you can save value here or perform other operation")
            
        }
        
        view.endEditing(true)
        view.resignFirstResponder()
        resignFirstResponder()
        
        productCollection.dataSource = self
        productCollection.delegate = self
        //        Product.removeAll()
        ProductName.removeAll()
        shopName.removeAll()
        shopaddress.removeAll()
        ProductPrice.removeAll()
        RetailPrice.removeAll()
        ProductTag.removeAll()
        productSaveTag.removeAll()
        ProductMOQ.removeAll()
        ProductImg.removeAll()
        VariantID.removeAll()
        if native.string(forKey: "searchType") as? String == "product_elastic"
        {
            searchText.text = searchBarViewController.searchText
        }
        else
        {
            var search = native.string(forKey: "search")!
            searchText.text = search
        }
        parseData(limit: limit, offset: offset)
        
        productCollection.isHidden = true
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        
        self.brand.removeAll()
        self.Category.removeAll()
        
        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func cellStyleButton(_ sender: Any) {
        if cellType == 0
        {
            productCollection.reloadData()
            cellType = 1
            cellStyle.setImage(UIImage(named:"GrideView"), for: .normal)
        }
        else
        {
            productCollection.reloadData()
            cellType = 0
            cellStyle.setImage(UIImage(named:"listview"), for: .normal)
            
        }
    }
    
    
    
    func parseData(limit: Int, offset: Int)
    {
        CustomLoader.instance.showLoaderView()
        var text = searchBarViewController.searchText
        //            debugPrint(Token)
        debugPrint(limit, offset)
        let b2burl = native.string(forKey: "b2burl")!
        
        var validateUrl = ""
        //        if native.string(forKey: "searchType") as? String == "product_elastic"
        //        {
        //
        //            validateUrl = "\(b2burl)/search/search/?search_type=product_elastic&search_query=\(text)&limit=\(limit)&offset=\(offset)&brand=\(selectedbrand)&category=\(selectedCategory)&min_price=\(minPrice)&max_price=\(maxPrice)&sort=\(selectedSort)"
        //            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        //        }
        //
        //        else if native.string(forKey: "searchType") as? String == "icon_category_elastic"
        //
        //        {
        //
        text = ""
        var product_collectionid = native.string(forKey: "product_collectionid")!
        validateUrl = "\(b2burl)/product/get_product_collection_data/?product_collectionid=\(product_collectionid)&limit=\(limit)&offset=\(offset)"
        //        }
        
        
        
        debugPrint("/////", validateUrl)
        var a = URLRequest(url: NSURL(string: validateUrl) as! URL)
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
        }
        else{
            
            a.allHTTPHeaderFields = ["Content-Type": "application/json"]
        }
        
        Alamofire.request(a as URLRequestConvertible).responseJSON { response in
            //                                debugdebugPrint(response)
            debugPrint("response: \(response)")
            let result = response.result
            switch response.result
            {
            case .failure(let error):
                //                debugPrint(error)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    // Your device does not have internet connection!
                    //                        debugPrint("Your device does not have internet connection!")
                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                    //                        debugPrint(err)
                }
                else if error._code == NSURLErrorTimedOut {
                    debugPrint("Request timeout!")
                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                }else {
                    // other failures
                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                }
                
            case .success:
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                        
                    else if dict1["status"] as! String == "failure"
                    {
                        
                        let alert = UIAlertController(title: "Alert", message: dict1["response"] as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        //                        self.dismiss(animated: true, completion: nil)
                        self.present(alert, animated: true, completion: nil)
                    }
                        
                    else if dict1["status"] as! String == "success"
                    {
                        debugPrint("searchguhjbfcgbuyhj data", dict1["status"] as! String )
                        if let data = dict1["response"] as? NSDictionary
                        {
                            if let innerdict = data["product_data"] as? [AnyObject]
                            {
                                if innerdict.count > 0
                                {
                                    for i in 0..<innerdict.count {
                                        //                        debugPrint(innerdict[i]["name_json"] as! NSDictionary, "name")
                                        let data = innerdict[i]["name_json"] as! AnyObject
                                        let name = innerdict[i]["title"]! as! String
                                        //                        debugPrint(name)
                                        
                                        
                                        //                        let shopname = innerdict[i]["shop_name"] as? String
                                        let statename = innerdict[i]["state_name"] as? String
                                        if innerdict[i]["feature_name"] is NSNull
                                        {
                                            self.featureTag.append("")
                                        }
                                        else
                                        {
                                            self.featureTag.append(innerdict[i]["feature_name"] as! String)
                                        }
                                        if let product_variant_data = innerdict[i]["product_variant_data"] as? AnyObject
                                        {
                                            //                        for j in 0..<product_variant_data.count
                                            
                                            if product_variant_data.count > 0
                                            {
                                                if innerdict[i]["product_price_tag"] is NSNull
                                                {
                                                    self.ProductTag.append("")
                                                }
                                                else
                                                {
                                                    self.ProductTag.append((innerdict[i]["product_price_tag"] as? String)!)
                                                }
                                                
                                                if product_variant_data["currency_symbol"] is NSNull
                                                {}
                                                else
                                                {
                                                    let myInteger = product_variant_data["currency_symbol"] as! String
                                                    //                                if let myUnicodeScalar = UnicodeScalar(myInteger) {
                                                    //                                    let myString = String(myUnicodeScalar)
                                                    self.Currency = "\(myInteger)"
                                                    //                                }
                                                }
                                                self.ProductName.append(name as AnyObject)
                                                let imagedir = (innerdict[i]["image_url"] as? AnyObject)!
                                                self.ProductImg.append((imagedir["200"] as? AnyObject)!)
                                                self.ProductMOQ.append((product_variant_data["min_order_qty"] as? AnyObject)!)
                                                if product_variant_data["retail_price"] is NSNull
                                                {
                                                    self.RetailPrice.append(Double(0.0))
                                                }
                                                else
                                                {
                                                    self.RetailPrice.append(Double((product_variant_data["retail_price"] as? NSNumber)!))
                                                }
                                                if self.user_purchase_modeid == "1"
                                                {
                                                    if product_variant_data["deal_price"] != nil
                                                    {
                                                        if product_variant_data["deal_price"] is NSNull
                                                        {
                                                            if product_variant_data["selling_price"] is NSNull
                                                            {}
                                                            else
                                                            {
                                                                self.ProductPrice.append(Double((product_variant_data["selling_price"] as? NSNumber)!))
                                                            }
                                                        }
                                                        else{
                                                            if let price = product_variant_data["deal_price"] as? NSNumber!
                                                            {
                                                                if (price != nil){
                                                                    self.ProductPrice.append(Double(price!))                                                                }
                                                                else
                                                                {
                                                                    if product_variant_data["group_price"] is NSNull
                                                                    {}
                                                                    else
                                                                    { if let price = product_variant_data["group_price"] as? NSNumber!
                                                                    {
                                                                        self.ProductPrice.append(Double((product_variant_data["group_price"] as? NSNumber)!))
                                                                        
                                                                        }}}}}} }
                                                else
                                                {
                                                    if product_variant_data["bulk_price"] is NSNull
                                                    {}
                                                    else
                                                    {
                                                        self.ProductPrice.append(Double((product_variant_data["bulk_price"] as? NSNumber)!))
                                                    }}
                                                
                                                if product_variant_data["percentage_off"] is NSNull
                                                {
                                                    self.productSaveTag.append("" as AnyObject)
                                                }
                                                else
                                                {
                                                    self.productSaveTag.append((product_variant_data["percentage_off"] as? AnyObject)!)
                                                }
                                                self.VariantID.append((product_variant_data["product_variantid"] as? AnyObject)!)
                                                //                                self.shopName.append((shopname as? AnyObject)!)
                                                self.shopaddress.append((statename as? AnyObject)!)
                                                
                                                
                                            }
                                        }
                                    }
                                }
                                //                        self.filterData()
                            }
                            
                            self.productCollection.isHidden = false
                            self.productCollection.reloadData()
                            
                        }}}
            }}
        //        }
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleCells: [IndexPath] = productCollection.indexPathsForVisibleItems
        let lastIndexPath = IndexPath(item: (ProductImg.count - 1), section: 0)
        if visibleCells.contains(lastIndexPath) {
            //This means you reached at last of your datasource. and here you can do load more process from server
            debugPrint("last index ", lastIndexPath)
            offset = offset + 10
            parseData(limit: limit, offset: offset)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = 0.0
        var height = 0.0
        if cellType == 0
        {
            width = Double(CGFloat(screenWidth / 2 - 15))
            height = Double(CGFloat(Double(screenHeight-30) / 3 ))
            
        }
        else
        {
            width = Double(CGFloat(screenWidth))
            height = Double(CGFloat(Double(screenHeight-30) / 6))
        }
        return CGSize(width: width, height: height )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.VariantID.count > 0
        {
            
            debugPrint("self.VariantID.count", self.VariantID.count)
            self.productCollection.showEmptyListMessage("")
            return self.VariantID.count
        }
        else {
            self.productCollection.showEmptyListMessage("No Product found!")
            return 0
        }
    }
    
    
    
    
    func returnImageUsingCacheWithURLString(url: NSURL) -> (UIImage) {
        
        // First check if there is an image in the cache
        if let cachedImage = imageCache.object(forKey: url) as? UIImage {
            
            return cachedImage
        }
            
        else {
            // Otherwise download image using the url location in Google Firebase
            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                
                if error != nil {
                    debugPrint(error)
                }
                else {
                    DispatchQueue.global().async {
                        
                        // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                        if let downloadedImage = UIImage(data: data!) {
                            
                            self.imageCache.setObject(downloadedImage, forKey: url)
                            self.returnImage = downloadedImage
                            
                        }
                    }
                }
            }).resume()
            return returnImage
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = UICollectionViewCell()
        if collectionView == productCollection {
            if cellType == 0
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
                
                //            debugPrint("product count \(Product.count)")
                //            cell.product1.layer.cornerRadius = 5.0
                cell.clipsToBounds = true
                cell.layer.cornerRadius = 5
                cell.layer.masksToBounds = true
                
                cell.contentView.layer.cornerRadius = 5
                cell.contentView.layer.borderWidth = 1.0
                
                cell.contentView.layer.borderColor = UIColor.clear.cgColor
                cell.contentView.layer.masksToBounds = true
                
                cell.layer.shadowColor = UIColor.lightGray.cgColor
                cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                cell.layer.shadowRadius = 2.0
                cell.layer.shadowOpacity = 1.0
                cell.layer.masksToBounds = false
                cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
                cell.productImage.image = nil
                if let image1 = self.ProductImg[indexPath.row] as? [AnyObject]
                {
                    if image1.count > 0
                    {
                        var image = image1[image1.count - 1] as! String
                        image = image.replacingOccurrences(of: " ", with: "%20")
                        let url = NSURL(string: image)
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                            } }
                    }
                    
                }
                cell.productName.text = (self.ProductName[indexPath.row] as? String)?.capitalized
                let price = self.ProductPrice[indexPath.row ]
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if native.string(forKey: "Token")! == ""
                {
                    cell.price.attributedText = ("Sign In ".color(textcolor).size(11)).attributedText
                }
                else
                {
                    cell.price.text = String(describing: "\(self.Currency)\(price)\(self.ProductTag[indexPath.row])")
                }
                cell.productTag.layer.cornerRadius = 3
                cell.productTag.clipsToBounds = true
                if self.featureTag[indexPath.row] as! String != "Default"
                {
                    cell.productTag.text = " \((self.featureTag[indexPath.row] as? String)!) "
                }
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "multiproductswitch", for: indexPath) as! ProductCollectionViewCell
                cell.clipsToBounds = true
                cell.productImage.image = nil
                
                
                if let image1 = self.ProductImg[indexPath.row] as? [AnyObject]
                {
                    if image1.count > 0
                    {
                        var image = image1[image1.count - 1] as! String
                        image = image.replacingOccurrences(of: " ", with: "%20")
                        let url = NSURL(string: image)
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                            }
                            
                        }
                    }
                }
                cell.productName.text = (self.ProductName[indexPath.row] as? String)?.capitalized
                let price = self.ProductPrice[indexPath.row ]
                let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                if self.user_purchase_modeid == "1"
                {
                    cell.productSaveTag.isHidden = false
                }
                else
                {
                    cell.productSaveTag.isHidden = true
                }
                if native.string(forKey: "Token")! == ""
                {
                    cell.price.attributedText = ("Sign In ".color(textcolor).size(11) + "to Unlock Wholesale Price".color(lightBlackColor).size(11)).attributedText
                }
                else
                {
                    cell.price.attributedText = ("\(self.Currency)\(String(format:"%.2f", ProductPrice[indexPath.row]))\(self.ProductTag[indexPath.row]) " .color(textcolor).size(20).color(textcolor).size(12) + "\(self.Currency)\(String(format:"%.2f", (RetailPrice[indexPath.row]))) ".color(lightGrayColor).strikethrough(1).size(12)).attributedText
                }
                cell.productSaveTag.text = "Save \(self.productSaveTag[indexPath.row])% vs retail"
                cell.productTag.layer.cornerRadius = 3
                cell.productTag.clipsToBounds = true
                if self.featureTag[indexPath.row] as! String != "Default"
                {
                    cell.productTag.text = " \((self.featureTag[indexPath.row] as? String)!) "
                }
                return cell
            }
            
        }
        count = 0
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        native.set(VariantID[indexPath.row] as! Int, forKey: "FavProID")
        native.set("product", forKey: "comefrom")
        native.synchronize()
        //        currntProID = self.ProductID[indexPath.row] as! Int
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
}
