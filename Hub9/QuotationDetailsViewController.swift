//
//  QuotationDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 12/17/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SCLAlertView
import ZendeskSDK
import ZendeskCoreSDK


var quotationDetails = QuotationDetailsViewController()

class QuotationDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate {

    var native = UserDefaults.standard
    var trackingid = ""
    var address = ""
    var shopname = ""
    var shoplogo = ""
    var orderItem = [AnyObject]()
    var order_seller_detail = [AnyObject]()
    var payment_amount = ""
    var orderid = ""
    var order_date = ""
    var payment_date = ""
    var payment_orderid = ""
    var currency_symbol = ""
    var quotation_item_data: [[String: Any]] = []
    var quotationid = 0
    var user_addressid = 0
    var status = false
    var totalcost = 0
    var payable = 0
    
    var shopFirebaseId = ""
    var shopName = ""
    var ShopLogo = ""
    var quotationChat = false
    
    
    
    @IBOutlet weak var quotationTable: UITableView!
    
    @IBAction func cancelButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomLoader.instance.showLoaderView()
        quotationDetails = self
        quotationTable.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        parseQuotationDetails()
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = quotationTable.cellForRow(at: indexPath)!
        selectedCell.selectionStyle = .none
        
        if indexPath.row == (8 + orderItem.count)
        {
            quotationChat = true
            contactSeller()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if status == true
        {
            return 9 + self.orderItem.count
        }
        else
        {
            return 8 + self.orderItem.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell()
        print("indexPath", indexPath.row)
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationdetailsstatus", for: indexPath) as! QuotationdetailsStatusTableViewCell
           
            
            if status == true
            {
                cell.statusText.text = "Seller Approved"
            }
            else if status == false
            {
                cell.statusText.text = "Approved"
            }
            
            
            cellToReturn = cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationdetailsshopname", for: indexPath) as! QuotationDetailsShopNameTableViewCell
            cell.shopname.text = self.shopname
            let url = NSURL(string: shoplogo as! String)
            if url != nil{
                DispatchQueue.main.async {
                    cell.shopicon.sd_setImage(with: URL(string: self.shoplogo as! String), placeholderImage: UIImage(named: "thumbnail"))
                }
            }
           
            cellToReturn = cell
        }
        else if indexPath.row < (2 + orderItem.count) || indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationdetailssproductdetails", for: indexPath) as! QuotationDetailsProductDetailsTableViewCell
            let index = indexPath.row - 2
            if self.orderItem.count > 0
            {
                if let image = self.orderItem[index]["image_url"] as? AnyObject
                {
                    if let productimage = image["main"] as? [AnyObject]
                    {
                        print("productimage", productimage)
                    if productimage.count > 0
                    {
                        let url = NSURL(string: productimage[0] as! String)
                        if url != nil{
                            DispatchQueue.main.async {
                                cell.productImage.sd_setImage(with: URL(string: productimage[0] as! String), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                    }
                }
                var productColor = ""
                var productSize = ""
                if let color = self.orderItem[index]["color_ui"] as? String
                {
                    productColor = color
                }
                if let size = self.orderItem[index]["size_ui"] as? String
                {
                    productSize = size
                }
                cell.producttype.text = "\((self.orderItem[index]["status"] as? String)!)"
                if self.orderItem[index]["requested_price"] is NSNull
                {}
                else
                {
                cell.value.text = "\(currency_symbol) \((self.orderItem[index]["requested_price"] as? Int)!)"
                }
                if self.orderItem[index]["bulk_price"] is NSNull
                {}
                else
                {
                cell.productprice.text = "\(currency_symbol) \((self.orderItem[index]["bulk_price"] as? NSNumber)!)"
                }
                if self.orderItem[index]["approved_price"] is NSNull
                {
                    cell.actualprice.text = "--"
                }
                else
                {
                cell.actualprice.text = "\(currency_symbol) \((self.orderItem[index]["approved_price"] as? Int)!)"
                }
                cell.productname.text = "\((self.orderItem[index]["title"] as? String)!)"
                cell.unit.text = "\((self.orderItem[index]["quantity"] as? Int)!) units"
            }
            
            cellToReturn = cell
        }
        else if indexPath.row == (3 + orderItem.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationdetailsfirstempty", for: indexPath) as! QuotationDetailsFirstEmptyTableViewCell
          
            cellToReturn = cell
        }
        else if indexPath.row == (4 + orderItem.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationdetailstotalcost", for: indexPath) as! QuotationDetailsTotalCostTableViewCell
            cell.totalCost.text = "\(currency_symbol) \(totalcost)"
            cell.totalPaybleCost.text = "\(currency_symbol) \(payable)"
            
            cellToReturn = cell
        }
        else if indexPath.row == (5 + orderItem.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationdetailssecondempty", for: indexPath) as! QuotationDetailsSecondEmptyTableViewCell
            
            cellToReturn = cell
        }
        else if indexPath.row == (6 + orderItem.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationdetailsstatusdetails", for: indexPath) as! QuotationDetailsStatusDetailsTableViewCell
            cell.copyid.layer.borderColor = UIColor.lightGray.cgColor
            cell.copyid.layer.borderWidth = 1
            cell.copyid.layer.cornerRadius = 3
            cell.copyid.layer.masksToBounds = true
            
            cellToReturn = cell
        }
        else if indexPath.row == (7 + orderItem.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "quotationaction", for: indexPath) as! QuotationActionTableViewCell
            cell.Approved.tag = indexPath.row
            cell.Approved.addTarget(self, action: #selector(approveButton(sender:)), for: .touchUpInside)
            cell.Reject.addTarget(self, action: #selector(rejectButton(sender:)), for: .touchUpInside)
            cellToReturn = cell
        }
        else if indexPath.row == (8 + orderItem.count)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "orderdetailspayment", for: indexPath) as! orderDetailsPaymentTableViewCell
            
            cellToReturn = cell
        }
        
        return cellToReturn
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0
        {
            return 60
        }
        else if indexPath.row == 1
        {
            return 50
        }
        else if indexPath.row < (2 + orderItem.count) || indexPath.row == 2
        {
            return 190
        }
        else if indexPath.row == (3 + orderItem.count)
        {
            return 7
        }
        else if indexPath.row == (4 + orderItem.count)
        {
            return 50
        }
        else if indexPath.row == (5 + orderItem.count)
        {
            return 7
        }
        else if indexPath.row == (6 + orderItem.count)
        {
            return 95
        }
        else if indexPath.row == (7 + orderItem.count)
        {
            return 60
        }
        else if indexPath.row == (8 + orderItem.count)
        {
            return 60
        }
        
        return 0
    }
    /// cpoing order id
    @objc func buttonViewLinkAction(sender:UIButton!) {
        print("Button tapped")
        UIPasteboard.general.string = orderid // or use  sender.titleLabel.text
        
    }
    
    func checkReject()
    {
        for i in 0..<self.orderItem.count
        {
            var quotation_itemid = 0
            var requested_price = 0
            var approved_price = 0
            var quantity = 0
            if self.orderItem[i]["status"] as! String != "Awaiting"
            {
            if self.orderItem[i]["quotation_itemid"] is NSNull
            {}
            else
            {
                quotation_itemid = (self.orderItem[i]["quotation_itemid"] as? Int)!
            }
            if self.orderItem[i]["requested_price"] is NSNull
            {}
            else
            {
                requested_price = (self.orderItem[i]["requested_price"] as? Int)!
            }
            if self.orderItem[i]["approved_price"] is NSNull
            {}
            else
            {
                approved_price = (self.orderItem[i]["approved_price"] as? Int)!
            }
            if self.orderItem[i]["quantity"] is NSNull
            {}
            else
            {
                quantity = (self.orderItem[i]["quantity"] as? Int)!
            }
            
            quotation_item_data.append(["quotation_itemid":quotation_itemid, "quotation_item_statusid": 4, "requested_price": requested_price,"approved_price": approved_price, "quantity": quantity])
            
        }
        }
        ApproveQuotation()
    }
    
    @objc func rejectButton(sender: UIButton)
    {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Yes") {
            print("Second button tapped")
            self.checkReject()
            
        }
        alertView.addButton("No") {
            print("Second button tapped")
            self.dismiss(animated: true, completion: nil)
        }
        alertView.showSuccess("Alert", subTitle: "Are you sure that you wnat to Reject all Quotation")
        
    }
    
    func checkstatus()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc = storyboard.instantiateViewController(withIdentifier: "orderAddress") as! SelectAddresstoSendQuotationViewController
        
        pvc.modalPresentationStyle = .custom
        pvc.transitioningDelegate = self
        
        present(pvc, animated: true, completion: nil)
        
        for i in 0..<self.orderItem.count
        {
            var quotation_itemid = 0
            var requested_price = 0
            var approved_price = 0
            var quantity = 0
            if self.orderItem[i]["status"] as! String != "Awaiting"
            {
                if self.orderItem[i]["quotation_itemid"] is NSNull
                {}
                else
                {
                    quotation_itemid = (self.orderItem[i]["quotation_itemid"] as? Int)!
                }
                if self.orderItem[i]["requested_price"] is NSNull
                {}
                else
                {
                    requested_price = (self.orderItem[i]["requested_price"] as? Int)!
                }
                if self.orderItem[i]["approved_price"] is NSNull
                {}
                else
                {
                    approved_price = (self.orderItem[i]["approved_price"] as? Int)!
                }
                if self.orderItem[i]["quantity"] is NSNull
                {}
                else
                {
                    quantity = (self.orderItem[i]["quantity"] as? Int)!
                }
                
                quotation_item_data.append(["quotation_itemid":quotation_itemid, "quotation_item_statusid": 5, "requested_price": requested_price,"approved_price": approved_price, "quantity": quantity])
                
            }
        }
        print(quotation_item_data)
        
    }
    
    
    @objc func approveButton(sender: UIButton)
    {
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("OK") {
            print("Second button tapped")
            self.checkstatus()
           
        }
        alertView.addButton("cancel") {
            print("Second button tapped")
            self.dismiss(animated: true, completion: nil)
        }
        alertView.showSuccess("Alert", subTitle: "Seller Aprroved quotation will be approved only.")
        
    }
    
    
    func ApproveQuotation()
    {
        let userid = native.string(forKey: "userid")
        let token = self.native.string(forKey: "Token")!
        print("userid:::", userid)
        let number = native.string(forKey: "number")
        let userCode = self.native.string(forKey: "userCode")
        let parameters: [String:Any] = ["quotation_data": ["quotationid": self.quotationid, "address_data": ["user_addressid":self.user_addressid], "quotation_item_data": quotation_item_data]]
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post",parameters )
        let b2burl = native.string(forKey: "b2burl")!
        Alamofire.request("\(b2burl)/quotation/edit_quotation/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in

            print(response)
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)

                //                self.blurView.alpha = 0
                //                self.activity.stopAnimating()
                //                    self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }

            if let json = response.result.value {
                print("JSON: \(json)")
                let jsonString = "\(json)"
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if dict1["status"] as! String  == "success"
                    {   UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        self.quotation_item_data.removeAll()
                        //                        self.blurView.alpha = 0
                        //                        self.activity.stopAnimating()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Close", backgroundColor: UIColorFromRGB(0x4EBB83), textColor: UIColor.white) {
                            alertView.dismiss(animated: true, completion: nil)
                            
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        alertView.showInfo("Success", subTitle: "Order has been SeccessFully placed", colorStyle: 0x4EBB83, colorTextButton: 0xFFFFFF)
                        
                    }
                    else
                    {
                        UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        let alert = UIAlertController(title: dict1["status"]! as! String, message: dict1["response"]! as! String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }

                    }
                
                return



            }

            }
        }
        
    }
    
    // parse packed data
    func parseQuotationDetails()
    {
        let Token = self.native.string(forKey: "Token")!
        print("token", Token)
        if Token != nil
        {
            var quotationid = native.string(forKey: "quotationid")!
            let b2burl = native.string(forKey: "b2burl")!
            var status = native.string(forKey: "quotationStatus")!
            var url = "\(b2burl)/quotation/buyer_quotation_detail/?quotationid=\(quotationid)&quotation_status=\(status)"
            
            
            print("Packed", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response packed: \(response)")
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if (dict1["status"] as? String)! == "success"
                    {
                        if dict1["response"] is NSNull
                        {}
                        else if let innerdict = dict1["response"] as? AnyObject
                                {
                                
                                    if innerdict["firebase_userid"] is NSNull
                                    {}
                                    else
                                    {
                                        self.shopFirebaseId = innerdict["firebase_userid"] as! String
                                    }
                                    if innerdict["logo_url"] is NSNull
                                    {}
                                    else
                                    {
                                        self.ShopLogo = innerdict["logo_url"] as! String
                                    }
                                    if innerdict["shop_name"] is NSNull
                                    {}
                                    else
                                    {
                                        self.shopName = innerdict["shop_name"] as! String
                                    }
                                if innerdict["currency_symbol"] is NSNull
                                {}
                                else
                                {
                                    self.currency_symbol = (innerdict["currency_symbol"] as? String)!
                                }
                                if innerdict["quotationid"] is NSNull
                                {}
                                else
                                {
                                self.quotationid = innerdict["quotationid"] as! Int
                                }
                                if let itemdata = innerdict["quotation_item_data"] as? [AnyObject]
                                {
                                    self.orderItem = itemdata
                                }
                                for i in 0..<self.orderItem.count
                                {
                                    if (self.orderItem[i]["status"] as? String)! == "Seller Approved"
                                    {
                                        self.status = true
                                        
                                    }
                                    if self.orderItem[i]["approved_price"] is NSNull
                                    {}
                                    else
                                    {
                                        self.payable = self.payable + (self.orderItem[i]["approved_price"] as? Int)!
                                    }
                                    if self.orderItem[i]["requested_price"] is NSNull
                                    {}
                                    else
                                    {
                                    
                                        self.totalcost = self.totalcost + (self.orderItem[i]["requested_price"] as? Int)!
                                    }
                                    
                                }
                                if innerdict["quotationid"] is NSNull
                                {
                                    self.orderid = "--"
                                }
                                else
                                {
                                    self.orderid = "\((innerdict["quotationid"] as? Int)!)"
                                }
                                if innerdict["shop_name"] is NSNull
                                {}
                                else
                                {
                                    self.shopname = (innerdict["shop_name"] as? String)!
                                }
                                if innerdict["logo_url"] is NSNull
                                {}
                                else
                                {
                                    self.shoplogo = (innerdict["logo_url"] as? String)!
                                }
                                
                            }
                        }
                    else
                    {
                        UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        let alert = UIAlertController(title: dict1["status"] as? String, message: dict1["response"] as? String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    UIApplication.shared.endIgnoringInteractionEvents()
                    CustomLoader.instance.hideLoaderView()
                    DispatchQueue.main.async {
                        self.quotationTable.isHidden = false
                        self.quotationTable.reloadData()
                        
                    }
                        }}
                }
            }
        }
    }
    
    func contactSeller()
    {
        var firstName = ""
         var lastName = ""
         var userID = ""
         
         if native.object(forKey: "userid") != nil
         {
             userID = native.string(forKey: "userid")!
         }
         if native.object(forKey: "firstname") != nil
         {
             firstName = native.string(forKey: "firstname")!
         }
         if native.object(forKey: "lastname") != nil
         {
             lastName = native.string(forKey: "lastname")!
         }
         print("Zendesk Support")
         let identity = Identity.createAnonymous(name: "\(firstName) \(lastName): \(userID)", email: "")
         Zendesk.instance?.setIdentity(identity)
         Support.initialize(withZendesk: Zendesk.instance)
         let hcConfig = HelpCenterUiConfiguration()
         hcConfig.groupType = .category
         let helpCenter = HelpCenterUi.buildHelpCenterOverview(withConfigs: [hcConfig])
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
             self.tabBarController?.tabBar.isHidden = true
         self.navigationController!.pushViewController(helpCenter, animated: true)
    }
    
//    func contactSeller()
//    {
//        let token = self.native.string(forKey: "Token")!
//        if token != nil
//        {
//
//
//            if self.shopFirebaseId != nil && self.shopName != nil && self.ShopLogo != nil
//
//            {
//
//                CustomLoader.instance.showLoaderView()
//                if shopFirebaseId == native.string(forKey: "firebaseid")! as! String
//                {
//                    let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your own shop", preferredStyle: UIAlertController.Style.alert)
//                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//                    self.present(alert, animated: true, completion: nil)
//                }
//                else
//                {
//
//                    //            topview.isHidden = true
//                    BBProductDetails.quotation = false
//                    native.set("1", forKey: "currentuserid")
//                    native.synchronize()
//                    var chatroom = CheckChatRoomViewController()
//
//                    chatroom.chekChatRoom(shopid: shopFirebaseId, shopname: shopName, shopProfilePic: ShopLogo)
//                    self.navigationController?.setNavigationBarHidden(true, animated: true)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
//                        // Your code with delay
//                        CustomLoader.instance.hideLoaderView()
//                        UIApplication.shared.endIgnoringInteractionEvents()
//
//                        self.performSegue(withIdentifier: "quotationContact", sender: self)
//                    }
//                }
//            }
//        }
//        else
//        {
//
//        }
//    }
    
}
