

import UIKit
import Firebase
import AudioToolbox


var userConversation = ConversationsVC()

class ConversationsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    var items = [Conversation]()
    var selectedUser: User?
    var native = UserDefaults.standard
    
    //MARK: Methods
    func customization()  {
//        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        //NavigationBar customization
        let navigationTitleFont = UIFont(name: "AvenirNext-Regular", size: 18)!
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: navigationTitleFont, NSAttributedString.Key.foregroundColor: UIColor.black]
        // notification setup
//        NotificationCenter.default.addObserver(self, selector: #selector(self.pushToUserMesssages(notification:)), name: NSNotification.Name(rawValue: "showUserMessages"), object: nil)
//         self.navigationItem.leftBarButtonItem = self.leftButton
        self.tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        if let id = native.string(forKey: "firebaseid") {
            User.info(forUserID: id, completion: { [weak weakSelf = self] (user) in
                let image = user.profilePic
                let contentSize = CGSize.init(width: 30, height: 30)
                UIGraphicsBeginImageContextWithOptions(contentSize, false, 0.0)
                let _  = UIBezierPath.init(roundedRect: CGRect.init(origin: CGPoint.zero, size: contentSize), cornerRadius: 14).addClip()
                image.draw(in: CGRect(origin: CGPoint.zero, size: contentSize))
                let path = UIBezierPath.init(roundedRect: CGRect.init(origin: CGPoint.zero, size: contentSize), cornerRadius: 14)
                path.lineWidth = 2
                UIColor.white.setStroke()
                path.stroke()
                let finalImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!.withRenderingMode(.alwaysOriginal)
                UIGraphicsEndImageContext()
                DispatchQueue.main.async {
//                    weakSelf?.leftButton.image = finalImage
                    weakSelf = nil
                }
                
            })
        }
    }
    
    //Downloads conversations
    func fetchData() {
        CustomLoader.instance.showLoaderView()
        Conversation.showConversations { (conversations) in
            self.items.removeAll()
            self.items = conversations
            self.items.sort{ $0.lastMessage.timestamp > $1.lastMessage.timestamp }
            DispatchQueue.main.async {
//                for conversation in self.items {
//                    self.blurView.alpha = 0
//                    if conversation.lastMessage.isRead == false {
//                        self.playSound()
//                        DispatchQueue.main.async {
                if self.items.count == conversations.count{
                    
                            
                }
                            
//                        }
//                        break
//                    }
//
//                }
//
            }
            
        }
        CustomLoader.instance.showLoaderView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.tableView.isHidden = false
            self.tableView.reloadData()
        })
    }
    
    //Shows Chat viewcontroller with given user
        func pushToUserMesssages(notification: NSNotification) {
        if let user = notification.userInfo?["user"] as? User {
            self.selectedUser = user
            self.performSegue(withIdentifier: "segue", sender: self)
        }
    }
    
    func playSound()  {
        var soundURL: NSURL?
        var soundID:SystemSoundID = 0
        let filePath = Bundle.main.path(forResource: "newMessage", ofType: "wav")
        soundURL = NSURL(fileURLWithPath: filePath!)
        AudioServicesCreateSystemSoundID(soundURL!, &soundID)
        AudioServicesPlaySystemSound(soundID)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue" {
            let vc = segue.destination as! ChatVC
            vc.currentUser = self.selectedUser
        }
    }

    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        debugPrint(self.items.count)
     if self.items.count == 0
     {
//        CustomLoader.instance.hideLoaderView()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        self.tableView.showEmptyListMessage("No Conversation yet")
        return 1
     }
//        CustomLoader.instance.hideLoaderView()
        self.tableView.separatorStyle = .singleLine
        self.tableView.showEmptyListMessage("")
        return self.items.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 80
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ConversationsTBCell
            cell.clearCellData()
            if self.items.count == 0
            {
                self.tableView.separatorColor = UIColor.clear
        }
        else
            {
                self.tableView.separatorColor = UIColor.lightGray
        }
            if self.items.count > indexPath.row
            {
                let image = self.items[indexPath.row].user.profilePic
                if image != nil
                {
                    let url = NSURL(string:image as! String )
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            cell.profilePic.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }

            cell.nameLabel.text = self.items[indexPath.row].user.name
//            debugPrint("self.items[indexPath.row].lastMessage.type", self.items[indexPath.row].lastMessage.type, self.items[indexPath.row].user.profilePic)
            switch self.items[indexPath.row].lastMessage.type {
            case .text:
//                debugPrint(self.items[indexPath.row])
                let message = self.items[indexPath.row].lastMessage.content
//                debugPrint(message)
                cell.messageLabel.text = message as! String
            case .quotationJson:
                cell.messageLabel.text = "Quotation"
            case .botJson:
                cell.messageLabel.text = "Bot"
            case.image:
                
                cell.messageLabel.text = "Media"
            }
                let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self.items[indexPath.row].lastMessage.timestamp)/1000)  //UTC time  //YOUR currentTimeInMiliseconds METHOD
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = NSTimeZone.local
//                dateFormatter.dateFormat = "HH:mm"
                let calendar = Calendar.current
                let date = Date(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].lastMessage.timestamp)/1000)
                let startOfNow = calendar.startOfDay(for: Date())
                let startOfTimeStamp = calendar.startOfDay(for: date)
                let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
                let day = components.day!
                if abs(day) < 2 {
                    let formatter = DateFormatter()
                    formatter.dateStyle = .short
                    formatter.timeStyle = .none
                    formatter.doesRelativeDateFormatting = true
//                    dateFormatter.dateStyle = DateFormatter.Style.short
                    dateFormatter.timeStyle = DateFormatter.Style.medium
                    debugPrint(formatter.string(from: date))
                }
                else if day > 1 && day<2 {
                    debugPrint("In \(day) days")
                    dateFormatter.date(from: "yesterday")
//                    dateFormatter.dateStyle = DateFormatter.Style.short
//                    dateFormatter.timeStyle = DateFormatter.Style.short
                }
                else if day > 2 {
                    debugPrint("In \(day) days")
                    dateFormatter.dateStyle = DateFormatter.Style.short
                    dateFormatter.timeStyle = DateFormatter.Style.short
                } else {
                    debugPrint("\(-day) days ago")
                    dateFormatter.dateStyle = DateFormatter.Style.short
                    dateFormatter.timeStyle = DateFormatter.Style.short
                }
                
                
                let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
                let dateFormatter2 = DateFormatter()
                dateFormatter2.timeZone = NSTimeZone(name: "UTC") as! TimeZone
                
                
                
                let date3 = dateFormatter.date(from: strDateSelect)
               
                
                cell.timeLabel.text = dateFormatter.string(from: date3!)
            if  self.items[indexPath.row].lastMessage.owner == .sender &&
                self.items[indexPath.row].lastMessage.isRead == false {
                cell.unreadDot.image = UIImage(named: "unreadIcon")
            }
                else
            {
                cell.unreadDot.image = nil
                }
        }
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.items.count > 0 {
            
            self.selectedUser = self.items[indexPath.row].user
            self.performSegue(withIdentifier: "segue", sender: self)
        }
    }
       
    //MARK: ViewController lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isHidden = true
        
        userConversation = self
        if native.string(forKey: "Token")! != ""
        {
        self.blurView.alpha = 1
        
        self.customization()
        self.fetchData()
        native.set("2", forKey: "currentuserid")
        native.synchronize()
        }
        else
        {
            
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.showEmailAlert()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
    self.tabBarController?.tabBar.isHidden = false
//        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear
        UIApplication.shared.statusBarStyle = .default //Set Style
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if let selectionIndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectionIndexPath, animated: animated)
            self.tableView.reloadData()
        }
    }
}





