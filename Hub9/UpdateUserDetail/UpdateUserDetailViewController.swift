//
//  UpdateUserDetailViewController.swift
//  Hub9
//
//  Created by Deepak on 26/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire


 var updateUserData = UpdateUserDetailViewController()
class UpdateUserDetailViewController: UIViewController {
    
    
    var native = UserDefaults.standard
    /// chat
    var shopFirebaseId = ""
    var shopName = ""
    var ShopLogo = ""
    var banner_url = ""
    var userid = 0
    var phyzioChat = false
    var followers_count = 0
    var following_count = 0
    var is_following = false
    
    // color
    var greenColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
    
    
    @IBOutlet weak var topViewContainer: UIView!
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var userCountData: UILabel!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var userBio: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    
    ///container view
    @IBOutlet weak var reviewContainer: UIView!
    @IBOutlet weak var dealContainer: UIView!
    @IBOutlet weak var closetContainer: UIView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var followContainer: UIView!
    @IBOutlet weak var followText: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func contact(_ sender: Any) {
            let token = self.native.string(forKey: "Token")!
            if token != nil
            {
                
                self.phyzioChat = true
                if self.shopFirebaseId != nil && self.shopName != nil && self.ShopLogo != nil

                {

                    CustomLoader.instance.showLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if  native.object(forKey: "userid") != nil
                    {
                    if String(FeedViewController.user_feed_id) == native.string(forKey: "userid")!
                    {
                        CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your profile", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {

                        
                        native.set("1", forKey: "currentuserid")
                        native.synchronize()
                        var chatroom = CheckChatRoomViewController()

                        chatroom.chekChatRoom(shopid: shopFirebaseId, shopname: shopName, shopProfilePic: ShopLogo)
    //
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                            // Your code with delay
                            CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()

                            self.performSegue(withIdentifier: "UserChat", sender: self)
                        }
                        }
                    }
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
            else
            {

            }
        }
    
    
    
    
    @IBAction func optionController(_ sender: Any) {
        segmentControl.changeUnderlinePosition()
        switch segmentControl.selectedSegmentIndex
        {
        case 1:
            reviewContainer.isHidden=true
            dealContainer.isHidden=false
            closetContainer.isHidden=true
        case 0:
            reviewContainer.isHidden=false
                dealContainer.isHidden=true
                closetContainer.isHidden=true
            //show Product view
            
        case 2:
        reviewContainer.isHidden=true
        dealContainer.isHidden=true
        closetContainer.isHidden=false
        
        default:
            break;
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = true
        
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        
        super.viewWillDisappear(animated)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.followContainer.isHidden = true
        self.chatButton.isHidden = true
        updateUserData = self
        self.followContainer.layer.cornerRadius = self.followContainer.frame.height/2
        self.followContainer.layer.borderWidth = 0.5
        self.followContainer.layer.borderColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1).cgColor
        self.followContainer.backgroundColor = UIColor.white
        
       reviewContainer.isHidden=false
        dealContainer.isHidden=true
        closetContainer.isHidden=true
        self.getUserData()
        segmentControl.addUnderlineForSelectedSegment()
//        segmentControl.setFontSize(fontSize: 20)
        // Do any additional setup after loa bding the view.
    }
    

    @IBAction func userFollowButton(_ sender: Any) {
    //        self.blurView.alpha = 1
    //        self.activity.startAnimating()
            
            let token = self.native.string(forKey: "Token")!
            if token != "" && native.object(forKey: "feed_userid")! != nil
            {
                CustomLoader.instance.showLoaderView()
            
         let b2burl = native.string(forKey: "b2burl")!
         var url = ""
         var fav = ""
             if( self.is_following == true)
            {
                 fav = "userid_to_unfollow"
                url = "\(b2burl)/users/unfollow_user/"
            }
            else
            {
                 fav = "userid_to_follow"
                url = "\(b2burl)/users/follow_user/"
            }
            //        print("product id:", id)
            let parameters: [String:Any] = ["\(fav)": "\(native.string(forKey: "feed_userid")!)"]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            
            
            print("follow user", url, parameters)
            Alamofire.request(url ,method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    if dict1["status"] as! String == "success"
                    {
                        
                     
                     if( self.is_following == true)
                     {
                         self.is_following = false
                        self.view.makeToast("Remove from your Favourite", duration: 2.0, position: .center)
                     }
                     else
                     {
                          self.is_following = true
                        self.view.makeToast("Added in your Favourite", duration: 2.0, position: .center)
                     }
                         }
                    if self.is_following == false
                    {
                    self.followContainer.layer.borderColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1).cgColor
                    self.followContainer.backgroundColor = UIColor.white
                    self.followText.textColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                    self.followText.text = "Follow"
                    }
                    else
                    {
                    self.followContainer.layer.borderColor = UIColor.white.cgColor
                    self.followContainer.backgroundColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                    self.followText.textColor = UIColor.white
                    self.followText.text = "Following"
                    }
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    }
                }}
                }
            }
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
    func getUserData()
    {
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            if self.native.object(forKey: "feed_userid") != nil
            {
                
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
                Alamofire.request("\(b2burl)/users/get_user_detail/?userid_for_detail=\(native.string(forKey: "feed_userid")!)&feed_typeid=2", encoding: JSONEncoding.default, headers: header).responseJSON { response in
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    //                    self.blurView.alpha = 0
                    //                    self.activity.stopAnimating()
                    //                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                if let json = response.result.value {
                    print("JSON user details: \(json)\(b2burl)/users/get_user_detail/?userid_for_detail=\(self.native.string(forKey: "feed_userid")!)&feed_typeid=2")
                    let jsonString = "\(json)"
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                        
                        else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                        {
                            //                            self.blurView.alpha = 0
                            //                            self.activity.stopAnimating()
                            print("userdetails", dict1)
                            if dict1["status"] as! String == "success"
                            {
                                if let data = dict1["data"] as? [AnyObject]
                                {
                                    if data.count > 0
                                    {
                                    print("firebaseid", (data[0]["firebase_userid"] as? String)!)
                                        
                                    
                                    if data[0]["bio"] is NSNull
                                    {}
                                    else
                                    {
                                        self.userBio.attributedText =  ("\((data[0]["bio"] as? String)!)".size(15.0)).attributedText
                                    }
                                    if data[0]["profile_pic_url"] is NSNull
                                    {}
                                    else
                                    {
                                        self.ShopLogo = (data[0]["profile_pic_url"] as? String)!
                                    }
                                    if data[0]["banner_url"] is NSNull
                                    {}
                                    else
                                    {
                                        self.banner_url = (data[0]["banner_url"] as? String)!
                                    }
                                    if data[0]["firebase_userid"] is NSNull
                                    {}
                                    else
                                    {
                                        self.shopFirebaseId = (data[0]["firebase_userid"] as? String)!
                                    }
                                    if data[0]["userid"] is NSNull
                                    {}
                                    else
                                    {
                                        self.userid = (data[0]["userid"] as? Int)!
                                    }
                                    var first_name = ""
                                    var last_name = ""
                                    if data[0]["first_name"] is NSNull
                                    {}
                                    else
                                    {
                                        first_name = (data[0]["first_name"] as? String)!
                                    }
                                    if data[0]["last_name"] is NSNull
                                    {}
                                    else
                                    {
                                        last_name = (data[0]["last_name"] as? String)!
                                    }
                                    
                                    self.shopName = "\(first_name) \(last_name)"
                                    
                                    if data[0]["followers_count"] is NSNull
                                    {}
                                    else
                                    {
                                        self.followers_count = (data[0]["followers_count"] as? Int)!
                                    }
                                    if data[0]["following_count"] is NSNull
                                    {}
                                    else
                                    {
                                        self.following_count = (data[0]["following_count"] as? Int)!
                                    }
                                    if data[0]["is_following"] is NSNull
                                    {}
                                    else
                                    {
                                        if (data[0]["is_following"] as? Int)! == 0
                                        {
                                           self.is_following = false
                                        }
                                        else
                                        {
                                           self.is_following = true
                                        }
                                         
                                    }
                               
                                }
                                }
                                DispatchQueue.main.async {
                                if self.banner_url != ""
                                {
                                var imageUrl = self.banner_url
                                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                                var url = NSURL(string:imageUrl as! String )
                                
                                }
                                // user image
                                if self.ShopLogo != ""
                                {
                                var imageUrl = self.ShopLogo
                                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                                var url = NSURL(string:imageUrl as! String )
                                
                                if url != nil{
                                    DispatchQueue.main.async {
                                        self.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                   }
                                    }}
                                    self.userCountData.text = "\(self.following_count) Following | \(self.followers_count) Follower"
                                    self.userName.text = self.shopName
                                    if  self.native.object(forKey: "userid") != nil
                                {
                                    if self.userid == Int(self.native.string(forKey: "userid")!)
                                {
                                    self.followContainer.isHidden = true
                                    self.chatButton.isHidden = true
                                    
                                    }
                                    else
                                    {
                                        self.followContainer.isHidden = false
                                        self.chatButton.isHidden = false
                                    }
                                    }
                                    if self.is_following == false
                                    {
                                    self.followContainer.layer.borderColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1).cgColor
                                    self.followContainer.backgroundColor = UIColor.white
                                    self.followText.textColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                                    self.followText.text = "Follow"
                                    }
                                    else
                                    {
                                    self.followContainer.layer.borderColor = UIColor.white.cgColor
                                    self.followContainer.backgroundColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
                                    self.followText.textColor = UIColor.white
                                    self.followText.text = "Following"
                                    }
                                }
                            }
                        else
                        {
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                            }}
                        else
                        {
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                        }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                            
                return
                        }}
                    
                    }
                }
                
                
            }
            
        }
    }
    
}


extension UISegmentedControl {

    func setFontSize(fontSize: CGFloat) {

        let normalTextAttributes: [NSObject : AnyObject] = [
            NSAttributedStringKey.foregroundColor as NSObject: UIColor.black,
            kCTFontNameAttribute: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.regular)
        ]

        let boldTextAttributes: [NSObject : AnyObject] = [
            NSAttributedStringKey.foregroundColor as NSObject : UIColor.white,
            kCTFontNameAttribute : UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.medium),
        ]

        self.setTitleTextAttributes(normalTextAttributes, for: .normal)
        self.setTitleTextAttributes(normalTextAttributes, for: .highlighted)
        self.setTitleTextAttributes(boldTextAttributes, for: .selected)
    }
}
