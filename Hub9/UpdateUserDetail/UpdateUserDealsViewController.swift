//
//  UpdateUserDealsViewController.swift
//  Hub9
//
//  Created by Deepak on 27/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import AVKit
import Alamofire
import AttributedTextView



var updateUserDeal = UpdateUserDealsViewController()

class UpdateUserDealsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var returnImage:UIImage = UIImage()
    let native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var user_purchase_modeid = "1"


    var isSearching = false
    var limit = 20
    var offset = 0
    var cellType = 1
    var count = 0
    var Category = [AnyObject]()
    var brand = [AnyObject]()
    var selectedCategory = [Int]()
    var selectedbrand = [Int]()
    var selectedSort = "asc"
    
    var ProductName = [String]()
    var shopName = [AnyObject]()
    var shopaddress = [AnyObject]()
    var ProductPrice = [Double]()
    var ProductTag = [String]()
    var RetailPrice = [Double]()
    var ProductMOQ = [AnyObject]()
    var ProductImg = [AnyObject]()
    var ProductVideo = [String]()
    var VariantID = [AnyObject]()
    var featureTag = [String]()
    var marketPlaceName = [String]()
    var productSaveTag = [AnyObject]()
    var Img = [AnyObject]()
    
    var priceTag = [String]()
    var isFeedID_liked = [Bool]()
    var shareLink = [String]()
    var isFeedID_bookmark = [Bool]()
    var user_feedid = [Int]()
    var like_users = [AnyObject]()
    var bookmark_user = [AnyObject]()
    var like = [Int]()
    var Comment = [Int]()
    var bookmark = [Int]()
    
    
    var minPrice = 0
    var maxPrice = 5000
    var Currency = ""
    var searchText = ""
    
    var user_feed_status_id = [Int]()
    var uploadTime =  [String]()


  
    @IBOutlet weak var productCollection: UICollectionView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        
        super.viewWillDisappear(animated)
        
        
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            attemptToHidKeyboard()
        }
    }

    override func willMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            attemptToHidKeyboard()
        }
    }
    
    private func attemptToHidKeyboard() {
        
        self.view.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUserDeal = self
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        self.tabBarController?.tabBar.isHidden = true
        CustomLoader.instance.showLoaderView()
        
        
        view.endEditing(true)
        view.resignFirstResponder()
        resignFirstResponder()
        
        productCollection.dataSource = self
        productCollection.delegate = self
//        Product.removeAll()
        ProductName.removeAll()
        shopName.removeAll()
        shopaddress.removeAll()
        ProductPrice.removeAll()
        RetailPrice.removeAll()
        ProductTag.removeAll()
        productSaveTag.removeAll()
        ProductMOQ.removeAll()
        ProductImg.removeAll()
        self.featureTag.removeAll()
        self.ProductVideo.removeAll()
        VariantID.removeAll()
        self.uploadTime.removeAll()
        self.user_feed_status_id.removeAll()
        getRecommended_product(limit: limit, offset: offset)
        
        productCollection.isHidden = true
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        
        self.brand.removeAll()
        self.Category.removeAll()
        
        // Do any additional setup after loading the view.
        
    }
    
     
     
     
     func deleteData()
     {
         ProductName.removeAll()
         shopName.removeAll()
         shopaddress.removeAll()
         ProductPrice.removeAll()
         RetailPrice.removeAll()
         ProductTag.removeAll()
         productSaveTag.removeAll()
         ProductMOQ.removeAll()
         ProductImg.removeAll()
         self.ProductVideo.removeAll()
         VariantID.removeAll()
        self.featureTag.removeAll()
        self.uploadTime.removeAll()
        self.user_feed_status_id.removeAll()
         DispatchQueue.main.async {
         self.productCollection.reloadData()
         }
         self.limit = 20
         self.offset = 0
     }
     
     
    
     


     
        func getRecommended_product(limit: Int, offset: Int)
        {
            
    //        CustomLoader.instance.showLoaderView()
            let token = self.native.string(forKey: "Token")!
            
                let b2burl = native.string(forKey: "b2burl")!
            searchText = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_deals/?limit=\(limit)&offset=\(offset)&userid_for_deals=\(native.string(forKey: "feed_userid")!)") as! URL)
            if token != ""
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            }
            else{
                
                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
            print(a, token)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                var url =
                print("url recommended", a, token)
                
                
                    switch response.result {
                    case .success:
                if let result1 = response.result.value
                {
                    let result = response.result
                   
                   
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
            else if dict1["status"] as! String == "success"
            {
               
                    print("recommended product",dict1 ,dict1["deal_data"])
                if let innerdict1 = dict1["deal_data"] as? [AnyObject]
                {
                            
                 for i in 0..<innerdict1.count {
                
                 print("recommended product1", innerdict1)
               if innerdict1.count > 0
               {
                if innerdict1[i]["title"] is NSNull
                {
                    self.ProductName.append("")
                }
                else if let name = innerdict1[i]["title"]! as? String
               {
                  
                       self.ProductName.append(name)
                    }
                if innerdict1[i]["price_reference_text"] is NSNull
                 {
                     self.priceTag.append("")
                 }
                 else if let name = innerdict1[i]["price_reference_text"]! as? String
                {
                   
                        self.priceTag.append(name)
                     }
                if innerdict1[i]["user_feed_status_id"] is NSNull
                 {
                     self.user_feed_status_id.append(0)
                 }
                 else if let feed_status_id = innerdict1[i]["user_feed_status_id"]! as? Int
                {
                    self.user_feed_status_id.append(feed_status_id)
                }
                if innerdict1[i]["feed_uploaded"] is NSNull
                 {
                     self.uploadTime.append("")
                 }
                 else if let feed_uploaded = innerdict1[i]["feed_uploaded"]! as? String
                {
                    self.uploadTime.append(feed_uploaded)
                }
                if innerdict1[i]["label"] is NSNull
                 {
                     self.uploadTime.append("")
                 }
                 else if let label = innerdict1[i]["label"]! as? String
                {
                    self.featureTag.append(label)
                }
                if innerdict1[i]["product_url"] is NSNull
                 {
                     self.shareLink.append("")
                 }
                 else if let name = innerdict1[i]["product_url"]! as? String
                {
                   
                        self.shareLink.append(name)
                     }
                if let likedata = innerdict1[i]["like_data"]
                {
                    if innerdict1[i]["like_data"] is NSNull
                            {
                                self.isFeedID_liked.append(false)
                            }
                            else
                            { self.like_users.append(innerdict1[i]["like_data"] as! AnyObject)
                                
                            }
                }
                else
                {
                   self.isFeedID_liked.append(false)
                }
                if let marketplace = innerdict1[i]["marketplace"]
                {
                if innerdict1[i]["marketplace"] is NSNull
                    {
                        self.marketPlaceName.append("")
                }
                    else
                    {
                        self.marketPlaceName.append((innerdict1[i]["marketplace"] as? String)!)
                }
                }
                else
                {
                    self.marketPlaceName.append("")
                }
                
                if let likedata = innerdict1[i]["bookmark_data"]
               {
                   if innerdict1[i]["bookmark_data"] is NSNull
                           {
                               self.isFeedID_bookmark.append(false)
                           }
                           else
                           { self.bookmark_user.append(innerdict1[i]["bookmark_data"] as! AnyObject)
                               
                           }
               }
               else
               {
                  self.isFeedID_bookmark.append(false)
               }
                    self.Comment.append(0)
                if let likedata = innerdict1[i]["like_count"]
                {
                if innerdict1[i]["like_count"] is NSNull
                    {
                        self.like.append(0)
                }
                    else
                    {
                        self.like.append((innerdict1[i]["like_count"] as? Int)!)
                }
                }
                else
                {
                    self.like.append(0)
                }
                if let bookmark_count = innerdict1[i]["bookmark_count"]
                {
                if innerdict1[i]["bookmark_count"] is NSNull
                    {
                        self.bookmark.append(0)
                }
                    else
                    {
                        self.bookmark.append((innerdict1[i]["bookmark_count"] as? Int)!)
                }
                }
                else
                {
                    self.bookmark.append(0)
                }
                
                if innerdict1[i]["user_feedid"] is NSNull
                
                {
                    self.user_feedid.append(0)
                }
                else
                {
                    self.user_feedid.append((innerdict1[i]["user_feedid"] as? Int)!)
                }
                
                
                if self.native.object(forKey: "userid") != nil
                {
                    let userid = self.native.string(forKey: "userid")!
                    if self.like_users.count > i
                    {
                    if userid != ""
                    {
                        var data = self.like_users[i] as! AnyObject
                        if data.contains(Int(userid))
                        {
                             self.isFeedID_liked.append(true)
                            print("isFeedID_liked", "true")
                        }
                        else
                        {  self.isFeedID_liked.append(false)
                           
                        }
                        
                    }
                    else
                    {
                       self.isFeedID_liked.append(true)
                    }
                    }
                    
                    // bookamrk
                    
                    if self.bookmark_user.count > i
                    {
                    if userid != ""
                    {
                        var data = self.bookmark_user[i] as! AnyObject
                        if data.contains(Int(userid))
                        {
                             self.isFeedID_bookmark.append(true)
                            print("isFeedID_liked", "true")
                        }
                        else
                        {  self.isFeedID_bookmark.append(false)
                           
                        }
                        
                    }
                    else
                    {
                       self.isFeedID_bookmark.append(true)
                    }
                    }
                }
                
                            let imagedir = (innerdict1[i]["media_url"] as? AnyObject)!
                            self.ProductImg.append((imagedir["image"] as? AnyObject)!)
                            if imagedir["mp4"] is NSNull
                            {
                                self.ProductVideo.append("")
                            }
                            else{
                                if let videodata = imagedir["mp4"] as? [AnyObject]
                            {             if videodata.count > 0
                                {
                                    self.ProductVideo.append(videodata[0] as! String)
                                }
                            }
                                
                            }
                            
                               
                              
                            
                               
                           }
                       
                   }
               
                
            }
                    }
                
                        }
                        print("self.Product", self.ProductName, self.VariantID)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                    }
                
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    }
                self.productCollection.isHidden = false
                self.productCollection.reloadData()
                
            }
            
        }



    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleCells: [IndexPath] = productCollection.indexPathsForVisibleItems
        let lastIndexPath = IndexPath(item: (ProductImg.count - 1), section: 0)
        if visibleCells.contains(lastIndexPath) {
            //This means you reached at last of your datasource. and here you can do load more process from server
            print("last index ", lastIndexPath)
            offset = offset + 10
            getRecommended_product(limit: limit, offset: offset)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = 0.0
        var height = 0.0
        
        width = Double(CGFloat(screenWidth))
        height = Double(CGFloat(Double(screenHeight-30) / 6))
        
        return CGSize(width: width, height: height )
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.ProductName.count > 0
        {
            
            print("self.VariantID.count", self.VariantID.count)
            self.productCollection.showEmptyListMessage("")
            return self.ProductName.count
        }
        else {
            self.productCollection.showEmptyListMessage("No Product found!")
            return 0
        }
    }
    
    
    
    
    func returnImageUsingCacheWithURLString(url: NSURL) -> (UIImage) {
        
        // First check if there is an image in the cache
        if let cachedImage = imageCache.object(forKey: url) as? UIImage {
            
            return cachedImage
        }
            
        else {
            // Otherwise download image using the url location in Google Firebase
            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                
                if error != nil {
                    print(error)
                }
                else {
                    DispatchQueue.global().async {
                        
                        // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                        if let downloadedImage = UIImage(data: data!) {
                            
                            self.imageCache.setObject(downloadedImage, forKey: url)
                            self.returnImage = downloadedImage
                            
                        }
                    }
                }
            }).resume()
            return returnImage
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell: UICollectionViewCell = UICollectionViewCell()
            if collectionView == productCollection {
               
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "multiproductswitch", for: indexPath) as! ProductCollectionViewCell
                    cell.clipsToBounds = true
                    cell.featuredName.layer.cornerRadius = cell.featuredName.frame.height/2
                    cell.featuredName.clipsToBounds=true
                    cell.productImage.image = nil
                    let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                    let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                    
                    let textbackcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 0.2)
                    let lightGrayColor = UIColor(red: 87/255, green: 87/255, blue: 87/255, alpha: 1)
                    let lightGrayBackColor = UIColor(red: 87/255, green: 87/255, blue: 87/255, alpha: 0.2)
                    
                if self.user_feed_status_id.count > indexPath.row
                {
                if self.user_feed_status_id[indexPath.row] == 5
                    {
                     cell.featuredName.backgroundColor = lightGrayBackColor
                     cell.featuredName.textColor = lightGrayColor
                        cell.productName.attributedText = ( "\((self.ProductName[indexPath.row]).capitalized) ".color(UIColor.lightGray).strikethrough(1)).attributedText
                     
                    }
                    else
                    {
                     cell.featuredName.backgroundColor = textbackcolor
                     cell.featuredName.textColor = textcolor
                        cell.productName.attributedText = ( "\((self.ProductName[indexPath.row]).capitalized) ".color(lightBlackColor).strikethrough(0)).attributedText
                    }
                    if self.featureTag.count>indexPath.row
                    {
                        if self.featureTag[indexPath.row] != ""
                        {
                            cell.featuredName.text = "  \(self.featureTag[indexPath.row])  "
                        }
                    }
                }
                    if let image1 = self.ProductImg[indexPath.row] as? [AnyObject]
                    {
                        if image1.count > 0
                        {
                            var image = image1[image1.count - 1] as! String
                            image = image.replacingOccurrences(of: " ", with: "%20")
                            let url = NSURL(string: image)
                            if url != nil
                            {
                                DispatchQueue.main.async {
                                    cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        }
                    }
                cell.price.attributedText = ( "\(self.priceTag[indexPath.row])".fontName("HelveticaNeue-Regular").color(textcolor)).attributedText
                if self.marketPlaceName.count>indexPath.row
                {
                    cell.marketPlaceName.text = self.marketPlaceName[indexPath.row]
                    
                }
                
                
                
                if self.uploadTime.count>indexPath.row
                {
                    if self.uploadTime[indexPath.row] != ""
                    {
                        let time  = self.relativeDate( time: Double(self.uploadTime[indexPath.row])!)
                        cell.uploadTime.text = "\(time)"
                        
                    }
                }
                if self.isFeedID_liked.count>indexPath.row
                {
                if self.isFeedID_liked[indexPath.row] == true
                {
                    cell.likeButton.setImage(UIImage(named: "liked"), for: .normal)
                    
                }
                else
                {
                    cell.likeButton.setImage(UIImage(named: "like"), for: .normal)
                }
                }
                if like[indexPath.row] > 0
                {
                cell.likeButton.setTitle(" \(like[indexPath.row])", for: .normal)
                }
                else
                {
                  cell.likeButton.setTitle("", for: .normal)
                }
                if self.Comment.count > indexPath.row
                {
                    if self.Comment[indexPath.row] > 0
                    {
                        cell.commentButton.setTitle(" \(self.Comment[indexPath.row])", for: .normal)
                    }
                    else
                    {
                        cell.commentButton.setTitle("", for: .normal)
                    }
                }
                else
                {
                  cell.commentButton.setTitle("", for: .normal)
                }
                cell.likeButton.tag = indexPath.row
                cell.likeButton.addTarget(self, action: #selector(LikeButton(Feed_Index:)), for: .touchUpInside)
                cell.likeButton.setTitle("\(Comment[indexPath.row])", for: .focused)
                cell.commentButton.tag=indexPath.row
                cell.commentButton.addTarget(self, action: #selector(comment_data(sender:)), for: .touchUpInside)
    //            cell.shareButton.tag=indexPath.row
    //            cell.shareButton.addTarget(self, action: #selector(shareButton(sender:)), for: .touchUpInside)
                if self.bookmark[indexPath.row] > 0
                {
                cell.followButton.setTitle("", for: .normal)
                }
                else
                {
                  cell.followButton.setTitle("", for: .normal)
                }
                if self.isFeedID_bookmark.count>indexPath.row
                {
                if self.isFeedID_bookmark[indexPath.row] == true
                { cell.followButton.setImage(UIImage(named: "bookmarkfill"), for: .normal)
                    cell.followButton.tintColor = textcolor
                }
                else
                { cell.followButton.setImage(UIImage(named: "bookmark"), for: .normal)
                    cell.followButton.tintColor = lightBlackColor
                }
                }
                cell.followButton.tag=indexPath.row
                cell.followButton.addTarget(self, action: #selector(bookmarkButton(Feed_Index:)), for: .touchUpInside)
                    
                    return cell
                }
            
            return cell
        }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.user_feedid.count > indexPath.row
        {
        native.set(self.user_feedid[indexPath.row], forKey: "FavProID")
        native.set("product", forKey: "comefrom")
        native.synchronize()
        //        currntProID = self.ProductID[indexPath.row] as! Int
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "dealDetails"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
        }
    }

    
    func relativeDate(time: Double) -> String {
        
        let theDate = Date(timeIntervalSince1970: TimeInterval(time))
        let components = Calendar.current.dateComponents([.day, .year, .month, .weekOfYear], from: theDate, to: Date())
        if let year = components.year, year == 1{
            return "\(year) year ago"
        }
        if let year = components.year, year > 1{
            return "\(year) years ago"
        }
        if let month = components.month, month == 1{
            return "\(month) month ago"
        }
        if let month = components.month, month > 1{
            return "\(month) months ago"
        }

        if let week = components.weekOfYear, week == 1{
            return "\(week) week ago"
        }
        if let week = components.weekOfYear, week > 1{
            return "\(week) weeks ago"
        }

        if let day = components.day{
            if day > 1{
                return "\(day) days ago"
            }else{
                "Yesterday"
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm a"
        //Specify your format that you want
        let strDate = dateFormatter.string(from: theDate)
        return strDate
    }
    
    /// like button
       
       @objc func LikeButton(Feed_Index: UIButton)
       {
           let token = self.native.string(forKey: "Token")!
           var fav = 0
               if token != ""
               {
                   CustomLoader.instance.showLoaderView()
               
               let parameters: [String:Any] = ["user_feedid":self.user_feedid[Feed_Index.tag]]
               let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
               print("Successfully post")
           
               let b2burl = native.string(forKey: "b2burl")!
               var url = ""
               if self.isFeedID_liked.count > Feed_Index.tag
               {
                if self.isFeedID_liked[Feed_Index.tag] == true
               {
                   
                   url = "\(b2burl)/users/dislike_user_feed/"
               }
               else
               {
                   url = "\(b2burl)/users/like_user_feed/"
               }
                   }
               print("follow product", url)
               Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                   
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   guard response.data != nil else { // can check byte count instead
                       let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                       
                       
                       UIApplication.shared.endIgnoringInteractionEvents()
                       return
                   }
                   
                   switch response.result
                   {
                       
                       
                   case .failure(let error):
                       //                print(error)
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let err = error as? URLError, err.code == .notConnectedToInternet {
                           // Your device does not have internet connection!
                           print("Your device does not have internet connection!")
                           self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                           print(err)
                       }
                       else if error._code == NSURLErrorTimedOut {
                           print("Request timeout!")
                           self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                       }else {
                           // other failures
                           self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                       }
                   case .success:
                       let result = response.result
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let dict1 = result.value as? Dictionary<String, AnyObject>{
                           if let invalidToken = dict1["detail"]{
                               if invalidToken as! String  == "Invalid token."
                               { // Create the alert controller
                                   self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                   appDelegate.window?.rootViewController = redViewController
                               }
                           }
                   
                   else if let json = response.result.value {
                       let jsonString = "\(json)"
                       print("res;;;;;", jsonString)
                       
                               if self.isFeedID_liked[Feed_Index.tag] == true
                       {
                        
                        self.view.makeToast("Remove from your favourite", duration: 2.0, position: .bottom)
                           self.like[Feed_Index.tag] = self.like[Feed_Index.tag] - 1
                           self.isFeedID_liked[Feed_Index.tag] = false
                       }
                       else
                       {
                        self.view.makeToast("Added in your favourite", duration: 2.0, position: .bottom)
                           self.like[Feed_Index.tag] = self.like[Feed_Index.tag] + 1
                           self.isFeedID_liked[Feed_Index.tag] = true
                               }
                               
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                               DispatchQueue.main.async {
                                self.productCollection.reloadItems(at: [IndexPath(row: Feed_Index.tag, section: 0)])
                               }}
                   }}
                   }
                   }
               else
               {
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                   appDelegate.window?.rootViewController = redViewController
               }
           
           
       }
    
    
    // bookmark
    
    @objc func bookmarkButton(Feed_Index: UIButton)
          {
              let token = self.native.string(forKey: "Token")!
              var fav = 0
                  if token != ""
                  {
                      CustomLoader.instance.showLoaderView()
                  
                  let parameters: [String:Any] = ["user_feedid":self.user_feedid[Feed_Index.tag]]
                  let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                  print("Successfully post")
              
                  let b2burl = native.string(forKey: "b2burl")!
                  var url = ""
                      if self.isFeedID_bookmark.count > Feed_Index.tag
                      {
                      if self.isFeedID_bookmark[Feed_Index.tag] == true
                  {
                      
                      url = "\(b2burl)/users/remove_bookmark_deal/"
                  }
                  else
                  {
                      url = "\(b2burl)/users/bookmark_deal/"
                  }
                      }
                  print("bookmark api", url)
                  Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                      
                      CustomLoader.instance.hideLoaderView()
                      UIApplication.shared.endIgnoringInteractionEvents()
                      guard response.data != nil else { // can check byte count instead
                          let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                          alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                          self.present(alert, animated: true, completion: nil)
                          
                          
                          UIApplication.shared.endIgnoringInteractionEvents()
                          return
                      }
                      
                      switch response.result
                      {
                          
                          
                      case .failure(let error):
                          //                print(error)
                          CustomLoader.instance.hideLoaderView()
                          UIApplication.shared.endIgnoringInteractionEvents()
                          if let err = error as? URLError, err.code == .notConnectedToInternet {
                              // Your device does not have internet connection!
                              print("Your device does not have internet connection!")
                              self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                              print(err)
                          }
                          else if error._code == NSURLErrorTimedOut {
                              print("Request timeout!")
                              self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                          }else {
                              // other failures
                              self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                          }
                      case .success:
                          let result = response.result
                          CustomLoader.instance.hideLoaderView()
                          UIApplication.shared.endIgnoringInteractionEvents()
                          if let dict1 = result.value as? Dictionary<String, AnyObject>{
                              if let invalidToken = dict1["detail"]{
                                  if invalidToken as! String  == "Invalid token."
                                  { // Create the alert controller
                                      self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                      let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                      let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                      let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                      appDelegate.window?.rootViewController = redViewController
                                  }
                              }
                      
                      else if let json = response.result.value {
                          let jsonString = "\(json)"
                          print("res;;;;;", jsonString)
                          
                      if self.isFeedID_bookmark[Feed_Index.tag] == true
                          {
                            self.view.makeToast("Remove from your saved", duration: 2.0, position: .bottom)
                            self.bookmark[Feed_Index.tag] = self.bookmark[Feed_Index.tag] - 1
                              self.isFeedID_bookmark[Feed_Index.tag] = false
                          }
                          else
                          {
                            self.view.makeToast("Added in your saved", duration: 2.0, position: .bottom)
                              self.bookmark[Feed_Index.tag] = self.bookmark[Feed_Index.tag] + 1
                              self.isFeedID_bookmark[Feed_Index.tag] = true
                                  }
                                  
                          CustomLoader.instance.hideLoaderView()
                          UIApplication.shared.endIgnoringInteractionEvents()
                                  DispatchQueue.main.async {
                                      self.productCollection.reloadItems(at: [IndexPath(row: Feed_Index.tag, section: 0)])
                          
                                  }}
                      }}
                      }
                      }
                  else
                  {
                      CustomLoader.instance.hideLoaderView()
                      UIApplication.shared.endIgnoringInteractionEvents()
                      let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                      let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                      let appDelegate = UIApplication.shared.delegate as! AppDelegate
                      appDelegate.window?.rootViewController = redViewController
                  }
              
              
          }
       
       @objc func comment_data(sender: UIButton)
       {
       
           FeedViewController.user_feed_id = self.user_feedid[sender.tag]
           FeedViewController.selectedIndex = sender.tag
           performSegue(withIdentifier: "userDealComment", sender: self)

           
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if (segue.identifier == "userDealComment") {
            guard let destination = segue.destination as? FeedCommentDataViewController else
            {
                print("return data")
               return
            }
            
                destination.feedComment_screen = 0
                destination.user_feedid = self.user_feedid[FeedViewController.selectedIndex]
                }
    //        else if (segue.identifier == "feedDetails1") {
    //        guard let destination = segue.destination as? FeedDetailsViewController else
    //        {
    //            print("return data")
    //           return
    //        }
    //            if self.feedVideo.count > feedDetailsIndex
    //            {
    //                destination.user_feedid = self.user_feedid[feedDetailsIndex]
    //                if (self.feedVideo[feedDetailsIndex] as? String)! != ""
    //                {
    //                    destination.feedVideo = (self.feedVideo[feedDetailsIndex] as? String)!
    //                }
    //                else if (self.feedImage[feedDetailsIndex] as? String)! != ""
    //                {
    //                   destination.feedImage = (self.feedImage[feedDetailsIndex] as? String)!
    //                }
    //                else
    //                {
    //                    destination.isFeedID_liked = self.isFeedID_liked[feedDetailsIndex]
    //                    destination.userImage = self.userImage[feedDetailsIndex]
    //                    destination.userName = self.userName[feedDetailsIndex]
    //                    destination.uploadTime = self.uploadTime[feedDetailsIndex]
    //
    //                    destination.QuestionText = "\(self.feed_text[feedDetailsIndex] as! String)"
    //
    //                }
    //
    //            }
    //            else
    //            {}
    //
    //        }
            
        }
    
    
       @objc func shareButton(sender: UIButton) {
           
        if self.shareLink.count>sender.tag
        {
           // image to share
           let text = "Feed Product"
           var productImage = "thumbnail"
           
           let image = UIImage(named: productImage)
           let myWebsite = NSURL(string:self.shareLink[sender.tag])
           let shareAll = [text , image , myWebsite!] as [Any]
           let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
           activityViewController.popoverPresentationController?.sourceView = self.view
           self.present(activityViewController, animated: true, completion: nil)
       }
    }
    
}
