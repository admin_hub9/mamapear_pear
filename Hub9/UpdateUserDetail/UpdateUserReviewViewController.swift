//
//  UpdateUserReviewViewController.swift
//  Hub9
//
//  Created by Deepak on 27/05/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AttributedTextView


var userFeedPost = UpdateUserReviewViewController()

class UpdateUserReviewViewController: UIViewController, UIViewControllerTransitioningDelegate, UICollectionViewDelegate, UICollectionViewDataSource, PinterestLayoutDelegate {

    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var returnImage:UIImage = UIImage()
    let native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var user_purchase_modeid = "1"
    var feedUser_Image = [String]()
    
    
    var followers_count = 0
    var following_count = 0
    var is_following = false
    var limit = 20
    var offset = 0
    
    /// data
    var userImage = [String]()
    var userName = [String]()
    var uploadTime = [Double]()
    var feedVideo = [String]()
    var feedGif = [String]()
    var feedImage = [String]()
    var like = [Int]()
    var Comment = [Int]()
    var user_feedid = [Int]()
    
    var feed_users = [AnyObject]()
    var feed_id = [Int]()
    var feed_text = [String]()
    var user_Comment_data = [[AnyObject]]()
    var isFeedID_liked = [Bool]()
    var shareLink = [String]()
    static var selectedIndex = 0
    static var user_feed_id = 0
    var feedDetailsIndex = 0
    
    
    
    // color
    var greenColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)

    
    
    
    @IBOutlet weak var productCollection: UICollectionView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
        
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.tabBarController?.tabBar.isHidden = false
        
        super.viewWillDisappear(animated)
        
        
    }
        
        func clearData()
        {
            
            feedImage.removeAll()
            feed_text.removeAll()
            userImage.removeAll()
            userName.removeAll()
            uploadTime.removeAll()
            feedVideo.removeAll()
            feedGif.removeAll()
            like.removeAll()
            Comment.removeAll()
            feed_users.removeAll()
            feed_id.removeAll()
            user_Comment_data.removeAll()
            isFeedID_liked.removeAll()
            shareLink.removeAll()
            FeedViewController.selectedIndex = 0
            DispatchQueue.main.async {
            self.productCollection.isHidden = false
            
            self.productCollection.reloadData()
            
            }
            self.limit = 20
            self.offset = 0
            self.getRecommended_product(limit: self.limit, offset: self.offset)
            
        }
       
        
        override func viewDidLoad() {
            super.viewDidLoad()
            userFeedPost = self
            if let layout = productCollection.collectionViewLayout as? PinterestLayout
            {
                layout.delegate = self
            }
            productCollection.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
            self.productCollection.isHidden=true
            self.clearData()
            
            
            // Do any additional setup after loading the view.
        }
        
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
                let visibleCells: [IndexPath] = productCollection.indexPathsForVisibleItems
                let lastIndexPath = IndexPath(item: (feedImage.count - 1), section: 0)
                if visibleCells.contains(lastIndexPath) {
                    //This means you reached at last of your datasource. and here you can do load more process from server
                    print("last index ", lastIndexPath)
                    offset = offset + 10
                    getRecommended_product(limit: limit, offset: offset)
                    
                }
            }
            
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        print("gfghffhj")
        var heightInPoints = 250
        let image =  UIImage(named: "\(self.feedImage[indexPath.row])")
        print(image?.size.height)
        
        //last cell's width
        if(  (indexPath.row + 1) % 2 == 0){
           
        }else {
            heightInPoints = heightInPoints + 60
        }
        
        
        return CGFloat(heightInPoints)
    }
            
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                if self.feed_id.count > 0
                {
                    return self.feed_id.count
                }
                else {
        //            self.productCollection.showEmptyListMessage("No Product found!")
                    return 0
                }
            }
            
            
            
            
            func returnImageUsingCacheWithURLString(url: NSURL) -> (UIImage) {
                
                // First check if there is an image in the cache
                if let cachedImage = imageCache.object(forKey: url) as? UIImage {
                    
                    return cachedImage
                }
                    
                else {
                    // Otherwise download image using the url location in Google Firebase
                    URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                        
                        if error != nil {
                            print(error)
                        }
                        else {
                            DispatchQueue.global().async {
                                
                                // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                                if let downloadedImage = UIImage(data: data!) {
                                    
                                    self.imageCache.setObject(downloadedImage, forKey: url)
                                    self.returnImage = downloadedImage
                                    
                                }
                            }
                        }
                    }).resume()
                    return returnImage
                }
            }
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                var reuseCell: UICollectionViewCell = UICollectionViewCell()
                if collectionView == productCollection {
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
                        
                        //            print("product count \(Product.count)")
                        //            cell.product1.layer.cornerRadius = 5.0
                        cell.clipsToBounds = true
                        cell.layer.cornerRadius = 5
                        cell.layer.masksToBounds = true
                        
                        cell.contentView.layer.cornerRadius = 5
                        cell.contentView.layer.borderWidth = 1.0
                        
                        cell.contentView.layer.borderColor = UIColor.clear.cgColor
                        cell.contentView.layer.masksToBounds = true
                        
                        cell.layer.shadowColor = UIColor.lightGray.cgColor
                        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                        cell.layer.shadowRadius = 2.0
                        cell.layer.shadowOpacity = 1.0
                        cell.layer.masksToBounds = false
                        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
                        cell.productImage.image = nil
                    if self.feed_text.count > indexPath.row
                    {
                    if self.feed_text[indexPath.row] != ""
                    {
                    cell.productName.text = self.feed_text[indexPath.row]
                    }}
                    
                    
                                            
                            if self.feedVideo[indexPath.row] as String != ""
                            {
                                if self.feedGif[indexPath.row] as String != ""
                                {
                                    cell.videoContainer.isHidden = true
                                    cell.productImage.isHidden = false
                                    
                                        // You can also set it with an URL pointing to your gif
                                    let url = URL(string: "\(self.feedGif[indexPath.row])")
                                    let loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
                                    if url != nil
                                    {
                                    DispatchQueue.main.async {
                                        cell.productImage.sd_setImage(with: URL(string: self.feedImage[indexPath.row]), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                    }
                                }

                            }
                         else if self.feedImage.count > indexPath.row
                        {
                            cell.videoContainer.isHidden = true
                            cell.productImage.isHidden = false
                            
                            if self.feedImage[indexPath.row] as String != ""
                            {
                                var image = self.feedImage[indexPath.row] as String
                                image = image.replacingOccurrences(of: " ", with: "%20")
                                let url = NSURL(string: image)
                                if url != nil{
                                    DispatchQueue.main.async {
                                        cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                }
                            
                        }
                        }
                    if self.userImage.count > indexPath.row
                    {
                        
                        if self.userImage[indexPath.row] as String != ""
                        {
                            var image = self.feedUser_Image[indexPath.row] as String
                            image = image.replacingOccurrences(of: " ", with: "%20")
                            let url = NSURL(string: image)
                            if url != nil{
                                DispatchQueue.main.async {
                                    cell.userImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        
                    }
                    }
                    if self.userName.count > indexPath.row
                    {
                        cell.userName.text = self.userName[indexPath.row]
                    }
                    if self.isFeedID_liked.count>indexPath.row
                    {
                    if self.isFeedID_liked[indexPath.row] == true
                    {
                        cell.likeProduct.setImage(UIImage(named: "liked"), for: .normal)
                    }
                    else
                    {
                        cell.likeProduct.setImage(UIImage(named: "like"), for: .normal)
                    }
                    }
                        
                        reuseCell = cell
                    }
                
                return reuseCell
            }
            
            
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                self.feedDetailsIndex = indexPath.row
                performSegue(withIdentifier: "feedDetails2", sender: self)
            }
        

        

        func getRecommended_product(limit: Int, offset: Int)
            {

                CustomLoader.instance.showLoaderView()
                let token = self.native.string(forKey: "Token")!

                    let b2burl = native.string(forKey: "b2burl")!

                if native.object(forKey: "feed_userid")! != nil
                {
                    var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed/?limit=\(limit)&offset=\(offset)&userid_for_posts=\(native.string(forKey: "feed_userid")!)&feed_typeid=2") as! URL)
                    
//                    var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed/?limit=\(limit)&offset=\(offset)&userid_for_detail=\(native.string(forKey: "feed_userid")!)&feed_typeid=2") as! URL)
                if token != ""
                {
                    a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
                }
                else{

                    a.allHTTPHeaderFields = ["Content-Type": "application/json"]
                }
                print(a, token)
                Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                    //                                debugPrint(response)
                    var url =
                    print("feed url recommended", a, token)


                        switch response.result {
                        case .success:


                            if let result1 = response.result.value
                            {
                                let result = response.result

                               print("result feed video", response)
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                    if let invalidToken = dict1["detail"]{
                                        if invalidToken as! String  == "Invalid token."
                                        { // Create the alert controller
                                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            appDelegate.window?.rootViewController = redViewController
                                        }
                                    }
                                    else if dict1["status"] as! String == "success"
                                    {
                                        if let innerdict = dict1["feed_data"] as? [AnyObject]
                                        {
                                            if innerdict.count > 0
                                               {
                                    for i in 0..<innerdict.count {

                                        if innerdict[i]["feed_share_link"] is NSNull
                                        {
                                            self.shareLink.append("pears.com")
                                        }
                                        else
                                        {
                                          self.shareLink.append(innerdict[i]["feed_share_link"] as! String)
                                        }


                                        if innerdict[i]["user_feedid"] is NSNull
                                        {
                                            self.feed_id.append(0)
                                        }
                                        else
                                        {
                                           self.feed_id.append((innerdict[i]["user_feedid"] as? Int)!)
                                        }

                                        if innerdict[i]["user_feedid"] is NSNull
                                        
                                        {
                                            self.user_feedid.append(0)
                                        }
                                        else
                                        {
                                            self.user_feedid.append((innerdict[i]["user_feedid"] as? Int)!)
                                        }
                                        
                                        if innerdict[i]["comment_data"] is NSNull
                                        {
                                            var data = [AnyObject]()
                                            self.user_Comment_data.append(data)
                                        }
                                        else
                                        {
                                           print(i)
                                            self.user_Comment_data.append((innerdict[i]["comment_data"] as? [AnyObject])!)
                                        }

                                        if innerdict[i]["like_data"] is NSNull
                                        {
                                            self.isFeedID_liked.append(false)
                                        }
                                        else
                                        {
                                           self.feed_users.append(innerdict[i]["like_data"] as! AnyObject)

                                        }

                                        if self.native.object(forKey: "userid") != nil
                                        {
                                            let userid = self.native.string(forKey: "userid")!
                                            if self.feed_users.count > i
                                            {
                                            if userid != ""
                                            {
                                                var data = self.feed_users[i] as! AnyObject
                                                if data.contains(Int(userid))
                                        {
                                             self.isFeedID_liked.append(true)
                                            
                                        }
                                        else
                                        {  self.isFeedID_liked.append(false)

                                        }

                                            }
                                            else
                                            {
                                               self.isFeedID_liked.append(true)
                                            }
                                            }}


                                            if let commentData =  innerdict[i]["feed_user_data"] as? AnyObject
                                            {

                                        if innerdict.count > 0
                                        {
                                            if innerdict[i]["feed_uploaded"] is NSNull

                                            {
                                            self.uploadTime.append(0)
                                            }
                                        else
                                            {
                                        let date = innerdict[i]["feed_uploaded"] as! String
                                                self.uploadTime.append(Double(date)!)
                                            }



                                            var firstName = ""
                                            var lastName = ""

                                            var commentCount = 0
                                            var like = 0
                                            if innerdict[i]["comment_count"] is NSNull
                                                {
                                                    commentCount = 0
                                            }
                                                else
                                                {
                                              commentCount = (innerdict[i]["comment_count"] as? Int)!
                                            }

                                            if innerdict[i]["like_count"] is NSNull
                                                {
                                                like = 0
                                                }
                                                else
                                                {
                                              like = (innerdict[i]["like_count"] as? Int)!
                                            }

                                            self.like.append(like)
                                            self.Comment.append(commentCount)

                                            if innerdict[i]["feed_user_data"] is NSNull

                                            {}
                                            else
                                            {

                                            if commentData["first_name"] as? String != ""
                                            {
                                            firstName = (commentData["first_name"] as? String)!
                                            }

                                                if (commentData["last_name"] as? String)! != ""
                                            {
                                            lastName = (commentData["last_name"] as? String)!
                                            }
                                             self.userName.append("\(firstName) \(lastName)")

                                            if commentData["profile_pic_url"] as? String != ""
                                            {
                                                self.userImage.append("")
                                            }
                                            else
                                            {self.userImage.append((commentData["profile_pic_url"] as? String)!)
                                            }
                                            }}
                                        }
                                    if innerdict[i]["title"] is NSNull
                                    {
                                        self.feed_text.append("")
                                    }
                                    else
                                    {
                                        self.feed_text.append((innerdict[i]["title"] as? String)!)
                                        }
                                
                                    if innerdict[i]["media_url"] is NSNull
                                    {
                                        self.feedImage.append("")
                                        self.feedVideo.append("")
                                    }
                                    else
                                    {
                                         if let data = innerdict[i]["media_url"] as? AnyObject
                                        {
                                            
                                
                                  if let imagedata = data["image"] as? [AnyObject]
                                           {
                                               print("image is not empty", imagedata)
                                               if imagedata.count > 0
                                               { self.feedImage.append((imagedata[0] as? String)!)
                                               }
                                            else
                                            {
                                                self.feedImage.append("")
                                                
                                                }
                                            }
                                           else if let videodata = data["image"] as? String
                                           { self.feedVideo.append((data["image"] as? String)!)
                                           }
                                           else
                                           {
                                               print("outer image is empty")
                                               self.feedImage.append("")
                                           }
                                            if data["mp4"] is NSNull
                                            {
                                                
                                                self.feedVideo.append("")
                                            }
                                             
                                            else if let videodata = data["mp4"] as? [AnyObject]
                                            {
                                                
                                                if videodata.count > 0
                                                {
                                                self.feedVideo.append((videodata[0] as? String)!)
                                                if let imagedata = data["gif"] as? [AnyObject]
                                                {
                                                    
                                                    if imagedata.count > 0
                                                    { self.feedGif.append((imagedata[0] as? String)!)
                                                    }
                                                 else
                                                 {
                                                     self.feedGif.append("")
                                                     
                                                     }
                                                 }
                                                    else
                                                {
                                                    self.feedGif.append("")
                                                    }
                                                }
                                                else
                                                {
                                                    self.feedVideo.append("")
                                                    self.feedGif.append("")
                                                    }
                                                
                                            }
                                            
                                                else
                                            {
                                                self.feedVideo.append("")
                                                self.feedGif.append("")
                                                } }

                                        }


                                        CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()

                                                }}

                                        }
                                        }}
                                DispatchQueue.main.async {
                                    CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                    self.productCollection.reloadData()
                                    }
                            }
                            

                        case .failure(let error):
                            //                print(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                print("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                print(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                print("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                    }
                    
                    }
                    
                    
                }
                

            }
        
        @objc func contactSeller()
            {
                let token = self.native.string(forKey: "Token")!
                if token != nil
                {
                    
                    updateUserData.phyzioChat = true
                    if updateUserData.shopFirebaseId != nil && updateUserData.shopName != nil && updateUserData.ShopLogo != nil

                    {

                        CustomLoader.instance.showLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if  native.object(forKey: "userid") != nil
                        {
                        if String(FeedViewController.user_feed_id) == native.string(forKey: "userid")!
                        {
                            CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                            let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your profile", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                        else
                        {

                            
                            native.set("1", forKey: "currentuserid")
                            native.synchronize()
                            var chatroom = CheckChatRoomViewController()

                            chatroom.chekChatRoom(shopid: updateUserData.shopFirebaseId, shopname: updateUserData.shopName, shopProfilePic: updateUserData.ShopLogo)
        //
                            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                                // Your code with delay
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()

                                self.performSegue(withIdentifier: "UserChat", sender: self)
                            }
                            }
                        }
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                }
                else
                {

                }
            }
        
        

    //    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    //        let visibleCells: [IndexPath] = self.MyFeedPost.indexPathsForVisibleRows!
    //        let lastIndexPath = IndexPath(item: (self.feed_id.count - 1), section: 0)
    //        if visibleCells.contains(lastIndexPath) {
    //            //This means you reached at last of your datasource. and here you can do load more process from server
    //            print("last index ", lastIndexPath)
    //            offset = offset + 10
    //            getRecommended_product(limit: limit, offset: offset)
    //
    //        }
    //    }
        
        

  @objc func favourite(_ sender: UIButton) {
        //        self.blurView.alpha = 1
        //        self.activity.startAnimating()
                
                let token = self.native.string(forKey: "Token")!
                if token != "" && native.object(forKey: "feed_userid")! != nil
                {
                    CustomLoader.instance.showLoaderView()
                
             let b2burl = native.string(forKey: "b2burl")!
             var url = ""
             var fav = ""
                 if( self.is_following == true)
                {
                     fav = "userid_to_unfollow"
                    url = "\(b2burl)/users/unfollow_user/"
                }
                else
                {
                     fav = "userid_to_follow"
                    url = "\(b2burl)/users/follow_user/"
                }
                //        print("product id:", id)
                let parameters: [String:Any] = ["\(fav)": "\(native.string(forKey: "feed_userid")!)"]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
                
                
                print("follow user", url, parameters)
                Alamofire.request(url ,method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    
                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                        print("res;;;;;", jsonString)
                        if dict1["status"] as! String == "success"
                        {
                         
                         if( self.is_following == true)
                         {
                             self.is_following = false
                        
                         }
                         else
                         {
                              self.is_following = true
                         }
                             }
//                        self.getUserData()
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                        }
                    }}
                    }
                }
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
            }
     
     
         
//         @objc func comment_data(sender: UIButton)
//         {
//
//             UserFeedPostViewController.user_feed_id = self.user_feedid[sender.tag]
//             UserFeedPostViewController.selectedIndex = sender.tag
//             performSegue(withIdentifier: "userFeedComments", sender: self)
//
//         }
         
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
             if (segue.identifier == "userFeedComments") {
             guard let destination = segue.destination as? FeedCommentDataViewController else
             {
                 print("return data")
                return
             }
                 destination.user_feedid = self.user_feedid[self.feedDetailsIndex]
               }
             else if (segue.identifier == "feedDetails2") {
             guard let destination = segue.destination as? FeedDetailsViewController else
             {
                 print("return data")
                return
             }
                print("self.user_feedid[self.feedDetailsIndex]", self.user_feedid[self.feedDetailsIndex])
                destination.user_feedid = self.user_feedid[self.feedDetailsIndex]
                destination.QuestionText = "\(self.feed_text[self.feedDetailsIndex])"
                 if self.feedVideo.count > self.feedDetailsIndex
                 {
                     

                     if (self.feedVideo[self.feedDetailsIndex] as? String)! != ""
                     {
                         destination.feedVideo = (self.feedVideo[self.feedDetailsIndex] as? String)!
                     }
                     else if (self.feedImage[self.feedDetailsIndex] as? String)! != ""
                     {
                        destination.feedImage = (self.feedImage[self.feedDetailsIndex] as? String)!
                     }
                     else
                     {
                         destination.isFeedID_liked = self.isFeedID_liked[self.feedDetailsIndex]
                         destination.userImage = self.userImage[self.feedDetailsIndex]
                         destination.userName = self.userName[self.feedDetailsIndex]
//                         destination.uploadTime = self.uploadTime[self.feedDetailsIndex]

                         destination.QuestionText = "\(self.feed_text[self.feedDetailsIndex] as! String)"

                     }

                 }
                 else
                 {}

             }
             
         }
         
       
         
         
         /// like button
         
         @objc func LikeButton(Feed_Index: UIButton)
         {
             let token = self.native.string(forKey: "Token")!
             var fav = 0
                 if token != ""
                 {
                     CustomLoader.instance.showLoaderView()
                 if self.native.object(forKey: "userid") != nil
                 {
                 let userid = self.native.string(forKey: "userid")!
                 
                 let parameters: [String:Any] = ["user_feedid":native.string(forKey: "feed_userid")!]
                 let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                 print("Successfully post")
             
                 let b2burl = native.string(forKey: "b2burl")!
                 var url = ""
                     if self.isFeedID_liked.count > Feed_Index.tag
                     {
                     if self.isFeedID_liked[Feed_Index.tag] == true
                 {
                     
                     url = "\(b2burl)/users/dislike_user_feed/"
                 }
                 else
                 {
                     url = "\(b2burl)/users/like_user_feed/"
                 }
                     }
                 print("follow product", url)
                 Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                     
                     
                     guard response.data != nil else { // can check byte count instead
                         let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                         alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                         self.present(alert, animated: true, completion: nil)
                         
                         
                         UIApplication.shared.endIgnoringInteractionEvents()
                         return
                     }
                     
                     switch response.result
                     {
                         
                         
                     case .failure(let error):
                         //                print(error)
                         CustomLoader.instance.hideLoaderView()
                         UIApplication.shared.endIgnoringInteractionEvents()
                         if let err = error as? URLError, err.code == .notConnectedToInternet {
                             // Your device does not have internet connection!
                             print("Your device does not have internet connection!")
                             self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                             print(err)
                         }
                         else if error._code == NSURLErrorTimedOut {
                             print("Request timeout!")
                             self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                         }else {
                             // other failures
                             self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                         }
                     case .success:
                         let result = response.result
                         CustomLoader.instance.hideLoaderView()
                         UIApplication.shared.endIgnoringInteractionEvents()
                         if let dict1 = result.value as? Dictionary<String, AnyObject>{
                             if let invalidToken = dict1["detail"]{
                                 if invalidToken as! String  == "Invalid token."
                                 { // Create the alert controller
                                     self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                     let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                     let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                     let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                     appDelegate.window?.rootViewController = redViewController
                                 }
                             }
                     
                     else if let json = response.result.value {
                         let jsonString = "\(json)"
                         print("res;;;;;", jsonString)
                         
                                 if self.isFeedID_liked[Feed_Index.tag] == true
                         {
                             self.like[Feed_Index.tag] = self.like[Feed_Index.tag] - 1
                             self.isFeedID_liked[Feed_Index.tag] = false
                         }
                         else
                         {
                             self.like[Feed_Index.tag] = self.like[Feed_Index.tag] + 1
                             self.isFeedID_liked[Feed_Index.tag] = true
                                 }
                                 
                         CustomLoader.instance.hideLoaderView()
                         UIApplication.shared.endIgnoringInteractionEvents()

                         
                         }
                     }}
                     DispatchQueue.main.async {
                                    self.productCollection.isHidden = false
                                     self.productCollection.reloadData()
                                   
                                    }
                     }
                     
                     }}
                 else
                 {
                     CustomLoader.instance.hideLoaderView()
                     UIApplication.shared.endIgnoringInteractionEvents()
                     let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                     let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                     let appDelegate = UIApplication.shared.delegate as! AppDelegate
                     appDelegate.window?.rootViewController = redViewController
                 }
             
             
         }
         
         
         @objc func shareButton(sender: UIButton) {
             
             
             // image to share
             let text = "Feed Product"
             var productImage = "thumbnail"
             
             let image = UIImage(named: productImage)
             let myWebsite = NSURL(string:self.shareLink[sender.tag])
             let shareAll = [text , image , myWebsite!] as [Any]
             let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
             activityViewController.popoverPresentationController?.sourceView = self.view
             self.present(activityViewController, animated: true, completion: nil)
         }
         
     
 }


