//
//  MyOrderViewController.swift
//  Hub9
//
//  Created by Deepak on 5/15/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class MyOrderViewController: UIViewController{
    var titleIndex = 0
    var native = UserDefaults.standard
    
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var container: UIView!
    @IBOutlet var orderContainer: UIView!
    @IBOutlet var qoutationContainer: UIView!
    


    
    
    
    @IBAction func orderandqoutationButton(_ sender: Any) {
        segmentControl.changeUnderlinePosition()
        switch segmentControl.selectedSegmentIndex
        {
        case 0:
            orderContainer.isHidden = false
            qoutationContainer.isHidden = true
        //show Product view
        case 1:
            orderContainer.isHidden = true
            qoutationContainer.isHidden = false
            
        //show Shop view
        default:
            break;
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
             statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
             UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.setNavigationBarHidden(false, animated: true)
        var user_purchase_modeid = "1"
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        if user_purchase_modeid == "1"
        {
            segmentControl.isHidden = true
            UIView.transition(with: container, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0
                    self.container.transform = CGAffineTransform(translationX: 0, y: -40)
                    
                    
                    //                self.searchbar.isHidden=false
            }, completion: nil)
        }
        else
        {
            segmentControl.isHidden = false
            UIView.transition(with: container, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                                self.blueView.alpha = 0
                    self.container.transform = CGAffineTransform(translationX: 0, y: 0)
                    
                    
                    //                self.searchbar.isHidden=false
            }, completion: nil)
        }
//        CustomLoader.instance.showLoaderView()
        segmentControl.addUnderlineForSelectedSegment()
        orderandqoutationButton((Any).self)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
