//
//  FavShopViewController.swift
//  Hub9
//
//  Created by Deepak on 7/3/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AttributedTextView


class FavShopViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let native = UserDefaults.standard
    var productData = [AnyObject]()
    
    
    @IBOutlet var FavShopTable: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.FavShopTable.tableFooterView = UIView()
        self.FavShopTable.isHidden = true
        self.emptyView.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.productData.removeAll()
        parseData()
    }
    
    func parseData()
    {
        //        FavProductID1.startWaiting()
        let Token = self.native.string(forKey: "Token")!
        print("token", Token)
        if Token != nil
        {
            
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/get_fav_shop/?all_shop=true"
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            print(a)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("responsefavshop: \(response)")
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                            
                        else if let innerdict = dict1["data"]{
                            self.productData = innerdict as! [AnyObject]
                            
                        }
                        
                        
                    }
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if self.productData.count>0
                    {
                        self.FavShopTable.isHidden = false
                        self.emptyView.isHidden = true
                        self.FavShopTable.reloadData()
                    }
                    else
                    {
                        self.FavShopTable.isHidden = true
                        self.emptyView.isHidden = false
                    }
                }
                
                
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if productData.count > 0
        {
            
            self.emptyView.isHidden = true
            return productData.count
        }
        else {
            self.FavShopTable.isHidden = true
            
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 220
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardShopTable", for: indexPath) as! DashboardShopTableViewCell
        var data = self.productData[indexPath.row] as! AnyObject
        if let image = data["logo_url"] as? String
        {
            if image != nil
            {
                var imageUrl = image
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: imageUrl)
                if url != nil{
                    DispatchQueue.main.async {
                        cell.shopLogo.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
        }
        if let address = data["location"] as? String
        {
            cell.shopAddress.text = address
        }
        //            print(index)
        
        if let name = data["shop_name"] as? String
        {
            cell.shopTitle.text = name
        }
        let greencolor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
        cell.enterShop.layer.cornerRadius = cell.enterShop.frame.height / 2
        cell.enterShop.layer.borderWidth = 0.8
        cell.enterShop.layer.borderColor = greencolor.cgColor
        cell.enterShop.clipsToBounds = true
        var shopid = (data["shopid"] as! NSNumber)
        cell.unfollowShop.tag = Int(shopid)
        cell.unfollowShop.addTarget(self, action: #selector(followButton(sender:)), for: .touchUpInside)
        cell.shopPage.tag = Int(shopid)
        cell.shopPage.addTarget(self, action: #selector(shoppage(sender:)), for: .touchUpInside)
        cell.shopCollection.tag = indexPath.row
        cell.shopCollection.reloadData()
        
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var data = self.productData[indexPath.row] as! AnyObject
        
        let id = data["shopid"] as! NSNumber
        
        
    }
    
    @objc func shoppage(sender: UIButton)
    {
        native.set(sender.tag, forKey: "entershopid")
        native.synchronize()
        print("product id:", sender.tag)
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "shopDetails"))! as UIViewController
        self.navigationController?.pushViewController(editPage, animated: true)
    }
    
    
    /// product collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //        print("collectionView.tag", collectionView.tag)
        var gridSize = 0
        var data = self.productData[collectionView.tag] as! AnyObject
        
        
        if data["product_data"] is NSNull
        {
            gridSize = 0
        }
        else
        {
            var productcount = data["product_data"] as! [AnyObject]
            gridSize = productcount.count
        }
        
        return gridSize
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //        print("index", indexPath.row,  collectionView.tag, self.product_top_category_data.count + 4)
        var ReuseCell = UICollectionViewCell()
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashShop", for: indexPath) as! DashboardShopCollectionViewCell
        var data = self.productData[collectionView.tag] as! AnyObject
        if let shopproduct = data["product_data"] as? [AnyObject]
        {
            //                print(indexPath.row)
            if indexPath.row < shopproduct.count
            {
                cell.productName.text = shopproduct[indexPath.row]["title"] as? String
                if shopproduct[indexPath.row]["product_variant_data"] is NSNull
                {}
                else
                {
                    let price = shopproduct[indexPath.row]["product_variant_data"] as! AnyObject
                    if price != nil
                    {
                        
                        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                        let graycolor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                        let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                        if price["bulk_price"] is NSNull{}
                        else{
                            let actualprice = price["bulk_price"] as! NSNumber
                            let retailsPrice = price["retail_price"] as! NSNumber
                            var product_price_tag = ""
                            if shopproduct[indexPath.row]["product_price_tag"] is NSNull
                            {}
                            else
                            {
                                product_price_tag = shopproduct[indexPath.row]["product_price_tag"] as! String
                            }
                            
                            cell.price.attributedText = ("\(data["currency_symbol"] as! String)\(actualprice)".color(textcolor).size(11) + "\(product_price_tag) ".color(textcolor).size(9) + "\(data["currency_symbol"] as! String)\(retailsPrice)".color(graycolor).strikethrough(1).size(9)).attributedText
                        }
                        
                    }
                }
                cell.Producttag.layer.cornerRadius = 5
                cell.Producttag.clipsToBounds = true
                
                if shopproduct[indexPath.row]["feature_name"] is NSNull
                {
                    cell.Producttag.text = ""
                }
                else
                {
                    if shopproduct[indexPath.row]["feature_name"] as! String != "Default"
                    {
                        cell.Producttag.text = " \(shopproduct[indexPath.row]["feature_name"] as! String) "
                        cell.Producttag.isHidden = false
                    }
                    else
                    {
                        cell.Producttag.isHidden = true
                    }
                }
                if let data = shopproduct[indexPath.row]["image_url"] as? AnyObject
                {
                    if let image = data["200"] as? [AnyObject]
                    {
                        if image.count > 0
                        {
                            var imageUrl = image[0] as! String
                            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                            let url = NSURL(string: imageUrl)
                            if url != nil{
                                DispatchQueue.main.async {
                                    cell.shopProductImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                }
                            }
                        }
                    }
                }
            }
            ReuseCell = cell
        }
        return ReuseCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        var data = self.productData[collectionView.tag] as! AnyObject
        if let shopproduct = data["product_data"] as? [AnyObject]
        {
            
            native.set(shopproduct[indexPath.row]["productid"]! as! Int, forKey: "FavProID")
            native.set("fav", forKey: "comefrom")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "productDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
        }
    }
    
    
    
    @objc func followButton(sender: UIButton) {
        CustomLoader.instance.showLoaderView()
        let shopid = sender.tag
        let parameters: [String:Any] = ["shopid":shopid]
        //        print("userid:", native.string(forKey: "userid")!)
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            let header = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            print("Successfully post")
            var url = ""
            let b2burl = native.string(forKey: "b2burl")!
            
            url = "\(b2burl)/users/unfollow_shop/"
            
            print("active url", url, parameters)
            Alamofire.request(url,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                guard response.data != nil else { // can check byte count instead
                    //                self.blurView.alpha = 0
                    //                self.ActivityGravity.stopAnimating()
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            let alertController = UIAlertController(title: "", message: "Your Seesion Expired Please Login again to continue", preferredStyle: .alert)
                            
                            // Create the actions
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                NSLog("OK Pressed")
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                            // Add the actions
                            alertController.addAction(okAction)
                            //                            alertController.addAction(cancelAction)
                            
                            // Present the controller
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                        
                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                        print("jsonString", jsonString)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.productData.removeAll()
                        self.parseData()
                        
                    }
                }
            }
        }
        else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    }
    
    
}
