//
//  testingTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 11/28/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class testingTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mark: UIImageView!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var priceView: UIView!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
