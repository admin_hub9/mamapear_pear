//
//  SelectInetrestViewController.swift
//  Hub9
//
//  Created by Deepak on 6/15/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout
import Alamofire


class SelectInetrestViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate {
 
    var Product = [AnyObject]()
    var Data = [AnyObject]()
    var icon_image_url = [String]()
    var ParentProduct = [AnyObject]()
    var ChildProduct = [AnyObject]()
    var selectedCell = [Int]()
    var currentParentIndexIs = 0
    var totalcount = 0
    var selectcount = 0
    var native = UserDefaults.standard
    var window: UIWindow?

    
//    @IBOutlet var Activity: UIActivityIndicatorView!
//    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var InterestSet: UICollectionView!
    @IBOutlet var ChooseOption: UICollectionView!
    @IBOutlet var numberofInterest: UILabel!
    
    
    // skip interest
    @IBAction func skipInterestButton(_ sender: Any) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var ReturnCell = 0
        if collectionView == InterestSet{
            ReturnCell = Data.count
        }
        else
        {
            ReturnCell = ParentProduct.count
        }
        return ReturnCell
    }
    

    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        if collectionView == InterestSet
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interestset", for: indexPath) as! InterestCollectionViewCell
//            print("select", Data[indexPath.row]["id"]!)
            if selectedCell.contains(self.Data[indexPath.row]["parent_categoryid"] as! Int) == false{
            selectedCell.append(Data[indexPath.row]["parent_categoryid"]! as! Int)
                
            }
            else{
                selectedCell.remove(at: selectedCell.index(of: Data[indexPath.row]["parent_categoryid"]! as! Int)!)
                
            }
            InterestSet.reloadItems(at: [IndexPath(item: indexPath.row, section: 0)])
        }
        if collectionView == ChooseOption{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interest", for: indexPath) as! SelectInterestCollectionCell
           currentParentIndexIs = indexPath.row
            Data.removeAll()
            for var i in 0..<self.ChildProduct[indexPath.row].count {
                self.Data.insert(self.ChildProduct[indexPath.row][i] as AnyObject, at: i)
//                print("gf", self.Data[i]["id"])
//                print("gf", self.Data[i]["name"])
                
            }
            ChooseOption.reloadData()
            InterestSet.reloadData()
        }
        
        return true
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var returnCell = UICollectionViewCell()
        if collectionView == ChooseOption
        {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interest", for: indexPath) as! SelectInterestCollectionCell
            cell.selectedInterest.isHidden = true
            
            if currentParentIndexIs == indexPath.row{
                cell.selectedInterest.isHidden = false
            }
            else{
                cell.selectedInterest.isHidden = true
            }
            if let imageURl = self.icon_image_url[indexPath.row] as? String
            {
                var imageUrl = imageURl
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                if let url = NSURL(string: imageUrl )
                {
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            cell.images.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
            }
            
            cell.name.text = ParentProduct[indexPath.row] as? String
            
        returnCell = cell
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "interestset", for: indexPath) as! InterestCollectionViewCell
            cell.button.setTitle(self.Data[indexPath.row]["category_name"] as! String, for: .normal)
           numberofInterest.text = "\(selectedCell.count)/\(totalcount) selected"
            
            cell.button.layer.borderColor = UIColor.lightGray.cgColor
            if selectedCell.contains(self.Data[indexPath.row]["parent_categoryid"] as! Int) == true
           {
            let redColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 0.20)
            let textColor = UIColor(red: 247/255.0, green: 82/255.0, blue: 85/255.0, alpha: 1.00)
            cell.button.setTitleColor(textColor, for: .normal)
            cell.button.layer.borderWidth = 0.0
            cell.button.backgroundColor = redColor
            
            }
           else{
            let textColor = UIColor(red: 135/255.0, green: 135/255.0, blue: 135/255.0, alpha: 1.00)
            cell.button.setTitleColor(textColor, for: .normal)
            let buttoncolor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.00)
            cell.button.layer.borderWidth = 0.5
            cell.button.backgroundColor = buttoncolor
            }
            cell.button.layer.cornerRadius = cell.button.layer.frame.size.height/2
            
            cell.button.clipsToBounds=true
            cell.button.layer.borderColor = UIColor.lightGray.cgColor
            returnCell = cell
        }
        return returnCell
    }

    @IBAction func doneButton(_ sender: Any) {
        sendInterest()
    }
    
    
    public func sendInterest() {
        print(selectedCell)
            let parameters: [String:Any] = ["category_data":selectedCell]
            let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            CustomLoader.instance.showLoaderView()
            let b2burl = native.string(forKey: "b2burl")!
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            
            Alamofire.request("\(b2burl)/users/set_category_pref/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                if let json = response.result.value {
                                    print("JSON: \(json)")
                    let jsonString = "\(json)"
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController

                    
                }
//                    UIApplication.shared.endIgnoringInteractionEvents()
               
                
                    return
                }}
        }
        else
        {
            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        parseData()
         let flowLayout = InterestSet?.collectionViewLayout as? AlignedCollectionViewFlowLayout
        flowLayout?.horizontalAlignment = .left
    }
    func parseData()
    {
        var Token = self.native.string(forKey: "Token")!
        print(Token)
        if Token != nil
        {
            
            let b2burl = native.string(forKey: "b2burl")!
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_category_preference/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                   else  if let innerdict = dict1["data"]{
//                        print(innerdict["category_data"])
                        let Product = innerdict["category_data"] as! [AnyObject]
//                        print(Product.count)
//                        print("huuf", Product)
                        for var i in 0..<Product.count {
                            self.ChildProduct.insert(Product[i]["category"] as AnyObject, at: i)
//                            print("Child Product---", self.ChildProduct[i])
                            if Product[i]["icon_image_url"] is NSNull
                            {
                            
                                self.icon_image_url.append("")
                            }
                            else
                            {
                                let image = Product[i]["icon_image_url"] as! [AnyObject]
                                if image[1]["mobile_icon"] is NSNull
                                {
                                    self.icon_image_url.append("")
                                }
                                else
                                {
                                   if let img = image[1]["mobile_icon"] as? String
                                    
                                   {
                                    print(img, image[1]["mobile_icon"] as! String)
                                    self.icon_image_url.insert(img , at: i)
                                    }
                                }
                                
                            }
                            self.ParentProduct.insert(Product[i]["parent_category"] as AnyObject, at: i)
//                            print("Parent---", self.ParentProduct[i])
                            
                        }
//                        print("parent Product", self.ParentProduct)
                        for var i in 0..<self.ChildProduct[0].count {
                            self.Data.insert(self.ChildProduct[0][i] as AnyObject, at: i)
//                            print("gf", self.Data[i])
                            
                        }
                        for var i in 0..<self.ChildProduct.count
                        {
//                            print(self.ChildProduct[i].count)
                            self.totalcount = self.totalcount + self.ChildProduct[i].count
                        }
                        
                    }
//                    print("child",self.totalcount)
                    if self.Data.count > 0
                    {
//                        self.blurView.alpha = 0
//                        self.Activity.stopAnimating()
                        self.InterestSet.reloadData()
                        
                        self.ChooseOption.reloadData()
                    }
                }
                }
                
            }
            
        }
        
    }

}
extension SelectInetrestViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var ReturnCell = CGSize(width: 0, height: 0)
        if collectionView == InterestSet
        {
            
            let widthSize = (self.Data[indexPath.row]["category_name"] as! NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0)])
            let width = Double(self.InterestSet.bounds.width)
            
            ReturnCell = CGSize(width: widthSize.width + 20, height: widthSize.height + 20)

        }
        else if collectionView == ChooseOption
        {
            ReturnCell = CGSize(width: 80, height: 95)
        }

        return ReturnCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        var ReturnCell = CGSize(width: 0, height: 0)
        if collectionView == InterestSet
        {
        ReturnCell = CGSize(width: self.view.bounds.width+20, height: 40)
        }
        else if collectionView == ChooseOption
        {
         ReturnCell = CGSize(width: 80, height: 95)
        }
        return ReturnCell
    }

}

