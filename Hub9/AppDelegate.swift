
//  AppDelegate.swift
//  Hub9
//
//  Created by Deepak on 5/8/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications
import FBSDKLoginKit
import Firebase
import AWSSNS
import AWSCore
import GoogleSignIn
import AWSMobileClient
import AWSPinpoint
import Sentry
//import FirebaseMessaging
//import FirebaseInstanceID
//import FirebaseInAppMessagingDisplay
import FirebaseDynamicLinks
import FirebaseAnalytics
import Stripe
import ZendeskSDK
import ZendeskCoreSDK
//import Amplify
//import AmplifyPlugins






@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    // get a reference to the app delegate
    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
    
    
    var window: UIWindow?
    let native = UserDefaults.standard
    // push notification
    let gcmMessageIDKey = "gcm.message_id"
     var pinpoint: AWSPinpoint?
    /// The SNS Platform application ARN
    let SNSPlatformApplicationArn = "arn:aws:sns:us-west-2:650369420974:app/APNS/Mamapear"
    
    
//    }
    
    func homeRedirect()
    {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let connectPage : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "rootController") as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = connectPage
        self.window?.makeKeyAndVisible()
    }

    func loggedInRedirect() {
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let connectPage : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "root") as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = connectPage
        self.window?.makeKeyAndVisible()
        self.window?.tintColor = UIColor.black
        
    }
    /// App badge number
    func incrementBadgeNumberBy(badgeNumberIncrement: Int) {
        let currentBadgeNumber = UIApplication.shared.applicationIconBadgeNumber
        let updatedBadgeNumber = currentBadgeNumber + badgeNumberIncrement
        if (updatedBadgeNumber > -1) {
            UIApplication.shared.applicationIconBadgeNumber = updatedBadgeNumber
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // call didFinishLaunchWithOptions ... why?
        appDelegate?.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        
            
        self.window?.tintColor = UIColor.black
//        Zendesk.initialize(appId: "e418779559574a6b144c59ec7e287cfbf3402c4406d77987",
//            clientId: "mobile_sdk_client_18bebe7d3921b6037aea",
//            zendeskUrl: "https://bulkbyte.zendesk.com")
//        Support.initialize(withZendesk: Zendesk.instance)

        let Navitemcolor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        let NavBackColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
        
        let cancelButtonAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black]
    UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
            UINavigationBar.appearance().tintColor = Navitemcolor
    UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-1000, 0), for:UIBarMetrics.default)

        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
             statusBar.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
             UIApplication.shared.keyWindow?.addSubview(statusBar)
                // get the navigation bar from the current navigation controller if there is one
                
            UINavigationBar.appearance().barTintColor = NavBackColor
                }
        else {
                UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
                 UINavigationBar.appearance().barTintColor = NavBackColor
                UIApplication.shared.statusBarStyle = .default
                }
        
        
        // Initialize the Amazon Cognito credentials provider
        // sentry
        // Create a Sentry client and start crash handler
        do {
            Client.shared = try Client(dsn: "https://376be359f9184cbab4203036eca9c177@sentry.io/1357707")
            try Client.shared?.startCrashHandler()
        } catch let error {
//            print("\(error)")
        }
//        print("::::::::::::: user is online ::::::::::::")
//        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USEast1,
//        identityPoolId:"us-east-1:afbd7b6c-336a-45b4-9442-729abd96a060")
//        let configuration = AWSServiceConfiguration(region:.USEast1, credentialsProvider:credentialsProvider)
        // Initialize the Amazon Cognito credentials provider
        // Initialize the Amazon Cognito credentials provider

//        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USWest2,
//           identityPoolId:"us-west-2:0fd4e937-3bb3-4068-9779-396ec90fe51b")
//
//        let configuration = AWSServiceConfiguration(region:.USWest2, credentialsProvider:credentialsProvider)
//
//        AWSServiceManager.default().defaultServiceConfiguration = configuration

        // #MARK: Redirect if logged in
        // native.set(1, forKey: "changetype")
        native.set("https://api.mamapear.us", forKey: "b2burl")
            native.synchronize()
//        native.set("http://api.mamapear.us:8080", forKey: "b2burl")
//        native.synchronize()
        if native.object(forKey: "loggedIn") != nil {
            if native.object(forKey: "loggedIn") as! Int == 1 {
                loggedInRedirect()
            }
            else if native.object(forKey: "loggedIn") as! Int == 0
            {
                homeRedirect()

            }
        }
       // Do any additional setup after loading the view.
       //configure google signin
        GIDSignIn.sharedInstance().clientID = "289176556474-9u1m806f0dodiufbdeeivn6gd2b2jmbf.apps.googleusercontent.com"
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        FirebaseApp.configure()
        
        // Override point for customization after application launch.
        pinpoint =
            AWSPinpoint(configuration:
                AWSPinpointConfiguration.defaultPinpointConfiguration(launchOptions: launchOptions))
       
        return AWSMobileClient.sharedInstance().interceptApplication(
            application,
            didFinishLaunchingWithOptions: launchOptions)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
//        FIRMessaging.messaging().shouldEstablishDirectChannel = false
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }


    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    
    func applicationDidReceiveMemoryWarning(application: UIApplication) {
        URLCache.shared.removeAllCachedResponses()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        ///pinpoint
        pinpoint!.notificationManager.interceptDidRegisterForRemoteNotifications(
            withDeviceToken: deviceToken)
        /// Attach the device token to the user defaults
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
//        print("token1", token)
        UserDefaults.standard.set(token, forKey: "B2BTokenForSNS")
        /// Create a platform endpoint. In this case, the endpoint is a
        /// device endpoint ARN
        let sns = AWSSNS.default()
        let request = AWSSNSCreatePlatformEndpointInput()
        request?.token = token
        request?.platformApplicationArn = SNSPlatformApplicationArn
        sns.createPlatformEndpoint(request!).continueWith(executor: AWSExecutor.mainThread(), block: { (task: AWSTask!) -> AnyObject! in
            if task.error != nil {
//                print("Error: \(String(describing: task.error))")
            } else {
                let createEndpointResponse = task.result! as AWSSNSCreateEndpointResponse
                if let endpointArnForSNS = createEndpointResponse.endpointArn {
//                    print("endpointArn: \(endpointArnForSNS)")
                    UserDefaults.standard.set(endpointArnForSNS, forKey: "B2BendpointArnForSNS")
//                    print("hjhjh", token)
                }
            }
            return nil
        })
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print(error.localizedDescription)
    }
    
    
    
    
    /// dynamic link
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            // ...
        }
        
        return handled
    }


func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
    if let incomingURL = userActivity.webpageURL {
        let handleLink = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL, completion: { (dynamicLink, error) in
            if let dynamicLink = dynamicLink, let _ = dynamicLink.url
            {
                print("Your Dynamic Link parameter1: \(dynamicLink)")
                self.handleDynamicLink(dynamicLink)
                
            } else {
                // Check for errors
                print("Your Dynamic Link parameter2: \(dynamicLink)")
            }
        })
        return handleLink
    }
    return false
}

//    @available(iOS 9.0, *)
//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
//        return application(app, open: url,
//                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
//                           annotation: "")
//        let stripeHandled = Stripe.handleURLCallback(with: url)
//
//        if (stripeHandled) {
//            return true
//        }
//        else {
//            // This was not a stripe url, do whatever url handling your app
//            // normally does, if any.
//        }
//    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
//            print("url \(url)")
//            print("url host :\(url.host!)")
//            print("url path :\(url.path)")
            
            
            let urlPath : String = url.path as String!
            let urlHost : String = url.host as String!
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            print("www.bulkbyte.com", urlHost)
            if(urlHost != "www.bulkbyte.com")
            {
                print("Host is not correct")
                return false
            }
            print("Host is correct")
            
            
            if(urlPath == "/product"){
                
                //        let innerPage: InnerPageViewController = mainStoryboard.instantiateViewController(withIdentifier: "InnerPageViewController") as! InnerPageViewController
                //        self.window?.rootViewController = innerPage
            } else if (urlPath == "/about"){
                
            }
            //    self.window?.makeKeyAndVisible()
            return true
//        }
//        return false
    }
    
    func openDeepLink(productID: Int)
    {
    native.set(productID, forKey: "FavProID")
    native.set("fav", forKey: "comefrom")
    native.set("true", forKey: "deeplink")
    native.synchronize()
    //        currntProID = self.ProductID[indexPath.row] as! Int
    
        
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let connectPage : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "rootController") as UIViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = connectPage
        self.window?.makeKeyAndVisible()

    }
    
func handleDynamicLink(_ dynamicLink: DynamicLink) {
    guard let url = dynamicLink.url else {
        print("dynamic url not found")
        return
    }
    print("Your Dynamic Link parameter1: \(url.absoluteString)")
    
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        print("url1 \(url)")
        print("url host :\(url.host!)")
        print("url path :\(url.path)")
        
        
        let urlPath : String = url.path as String!
        let urlHost : String = url.host as String!
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        print("www.bulkbyte.com", urlHost)
        if(urlHost != "www.bulkbyte.com")
        {
            print("Host is not correct")
            
        }
        print("Host is correct")
        
        if let comps = url.path as? String
        {
        var elements = comps.split(separator: "/")
        // ["forums", "hot-deals-online", "topics", "smart-tv-carniva"]
        
        
        // To build an url back, example:
//        print("0",elements[0])
//        print("1",elements[1])
//        print("2",elements[2])
            if Int(elements[1]) != nil
            {
            var productid = Int(elements[1])
                print("productid", productid!)
                self.openDeepLink(productID: productid!)
            }
    
        // Result: www.dd.com/forums/hot-deals-online
    
        if(urlPath == "/product"){
            
            
        } else if (urlPath == "/about"){
            
        }
        //    self.window?.makeKeyAndVisible()
    }
    
}


}


@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification
        userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("APN recieved")
        // print(userInfo)
        
        let state = application.applicationState
        switch state {
            
        case .inactive:
            print("Inactive")
            
        case .background:
            print("Background")
            // update badge count here
            application.applicationIconBadgeNumber = application.applicationIconBadgeNumber + 1
            
        case .active:
            print("Active")
            
        }
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // reset badge count
        application.applicationIconBadgeNumber = 0
    }
    
    
    ///Stripe method
    // This method is where you handle URL opens if you are using a native scheme URLs (eg "yourexampleapp://")
//    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//        let stripeHandled = Stripe.handleURLCallback(with: url)
//
//        if (stripeHandled) {
//            return true
//        }
//        else {
//            // This was not a stripe url, do whatever url handling your app
//            // normally does, if any.
//        }
//
//        return false
//    }
    
    // This method is where you handle URL opens if you are using univeral link URLs (eg "https://example.com/stripe_ios_callback")
//    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
//        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
//            if let url = userActivity.webpageURL {
//                let stripeHandled = Stripe.handleURLCallback(with: url)
//
//                if (stripeHandled) {
//                    return true
//                }
//                else {
//                    // This was not a stripe url, do whatever url handling your app
//                    // normally does, if any.
//                }
//            }
//
//        }
//        return false
//    }
    
}


extension AppDelegate: MessagingDelegate
{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)

        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("message data", remoteMessage.appData)
    }

}

extension UIImageView {
    static func fromGif(frame: CGRect, resourceName: String) -> UIImageView? {
        guard let path = Bundle.main.path(forResource: resourceName, ofType: "gif") else {
//            print("Gif does not exist at that path")
            return nil
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
            let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return nil }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)
        for i in 0 ..< imageCount {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(UIImage(cgImage: image))
            }
        }
        let gifImageView = UIImageView(frame: frame)
        gifImageView.animationImages = images
        return gifImageView
    }
}




extension UIViewController {
    /// Function for presenting AlertViewController with fixed colors for iOS 9
    func presentAlert(alert: UIAlertController, animated flag: Bool, completion: (() -> Swift.Void)? = nil){
        // Temporary change global colors
        UIView.appearance().tintColor = UIColor.black// Set here whatever color you want for text
        UIApplication.shared.keyWindow?.tintColor = UIColor.black // Set here whatever color you want for text

        //Present the controller
        self.present(alert, animated: flag, completion: {
            // Rollback change global colors
            UIView.appearance().tintColor = UIColor.black // Set here your default color for your application.
            UIApplication.shared.keyWindow?.tintColor = UIColor.black // Set here your default color for your application.
            if completion != nil {
                completion!()
            }
        })
    }
}



extension UITextView {
    
    func addDoneButton(title: String, target: Any, selector: Selector) {
        
        let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                              y: 0.0,
                                              width: UIScreen.main.bounds.size.width,
                                              height: 44.0))//1
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
        let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)//3
        toolBar.setItems([flexible, barButton], animated: false)//4
        self.inputAccessoryView = toolBar//5
    }
}


extension UISegmentedControl{
    func removeBorder(){
        let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: self.bounds.size)
        self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
        self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)

        let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor, andSize: CGSize(width: 1.0, height: self.bounds.size.height))
        self.setDividerImage(deviderImage, forLeftSegmentState: .selected, rightSegmentState: .normal, barMetrics: .default)
        let normalFont = UIFont(name: "HelveticaNeue", size: 15.0)
        let boldFont = UIFont(name: "HelveticaNeue-Bold", size: 15.0)
        
        let normalTextAttributes: [NSObject : AnyObject] = [
            NSAttributedStringKey.foregroundColor as NSObject: UIColor.gray,
            kCTFontAttributeName: normalFont!
        ]

        let boldTextAttributes: [NSObject : AnyObject] = [
            NSAttributedStringKey.foregroundColor as NSObject : UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1.0),
            kCTFontAttributeName : boldFont!,
        ]

        self.setTitleTextAttributes(normalTextAttributes, for: .normal)
        self.setTitleTextAttributes(normalTextAttributes, for: .highlighted)
        self.setTitleTextAttributes(boldTextAttributes, for: .selected)
    
    }

    func addUnderlineForSelectedSegment(){
        removeBorder()
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 3.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 4.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1.0)
        underline.tag = 1
        self.addSubview(underline)
    }

    func changeUnderlinePosition(){
        guard let underline = self.viewWithTag(1) else {return}
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        UIView.animate(withDuration: 0.1, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}

extension UIImage{

    class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext?.setFillColor(color)
        let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        graphicsContext?.fill(rectangle)
        let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return rectangleImage!
    }
}



extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}

extension UIApplication {
var statusBarUIView: UIView? {
    if #available(iOS 13.0, *) {
        let tag = 38482
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if let statusBar = keyWindow?.viewWithTag(tag) {
            return statusBar
        } else {
            guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
            let statusBarView = UIView(frame: statusBarFrame)
            statusBarView.tag = tag
            keyWindow?.addSubview(statusBarView)
            return statusBarView
        }
    } else if responds(to: Selector(("statusBar"))) {
        return value(forKey: "statusBar") as? UIView
    } else {
        return nil
    }
  }
}

