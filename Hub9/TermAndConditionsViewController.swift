//
//  TermAndConditionsViewController.swift
//  Hub9
//
//  Created by Deepak on 5/20/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import WebKit

class TermAndConditionsViewController: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // 1
        CustomLoader.instance.showLoaderView()
        let seconds = 4.0
        
        let url = URL(string: "https://help.bulkbyte.com/hc/en-us/articles/360031342332-Terms-Conditions")!
        webView.load(URLRequest(url: url))
        
        // 2
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        toolbarItems = [refresh]
        navigationController?.isToolbarHidden = false
       DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
           CustomLoader.instance.hideLoaderView()
           UIApplication.shared.endIgnoringInteractionEvents()
           // Put your code which should be executed with a delay here
       }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
