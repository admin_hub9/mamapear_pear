//
//  StripePaymentViewController.swift
//  Hub9
//
//  Created by Deepak on 9/4/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Stripe

class StripePaymentViewController: UIViewController, STPPaymentCardTextFieldDelegate {

    @IBOutlet weak var paymentCardTextField = STPPaymentCardTextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    
    
    private func getToken(){
        let cardParams = STPCardParams()
        cardParams.number = paymentCardTextField?.cardNumber
        cardParams.expMonth = (paymentCardTextField?.expirationMonth)!
        cardParams.expYear = (paymentCardTextField?.expirationYear)!
        cardParams.cvc = paymentCardTextField?.cvc
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                return
            }
//            self.dictPayData["stripe_token"] = token.tokenId
//            print(self.dictPayData)
            
        }
}
}
