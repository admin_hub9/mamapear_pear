//
//  BBPaymentViewController.swift
//  Hub9
//
//  Created by Deepak on 8/21/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import Stripe


class BBPaymentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var paymentContainer: UIView!
    @IBOutlet weak var BBpointContainer: UIView!
    @IBOutlet weak var totalBBPoints: UILabel!
    @IBOutlet weak var BBPointsMarkIcon: UIImageView!
    
    @IBOutlet weak var paymentMethodTable: UITableView!
    
    var BBpointsSelected = false
    var paymentMethod = [AnyObject]()
    var paymentOptionText = ["Credit/Debit Card", "NetBanking", "Wallet", "Paytm Wallet", "UPI"]
    var stripePayment = ["Credit/Debit Card"]
    var selectedPaymentlkpid = 5
    var paymentMethodIndex = -1
    var BBpoints = 0.0
    var paymentMethod1 = 0
    
    
    //    var data = [Any]()
    var native = UserDefaults.standard
    var amount = 0.0
    var address_lkpid = ""
    var paymentmode = 1
    var RazorPaypayment_orderid = ""
    var RazorPayorderid = ""
    var RazorPayamount = ""
    var ship_insurance = 0.0
    var shippingPrice = 0.0
    var payment_id = ""
    
    
    var payment_orderid = ""
    var gateway_paymentid = ""
    var orderid = ""
    var insurance = false
    var totalCost = ""
    
    //    var data: [String: Any] = [String: Any]()
    
    
    
    @IBAction func backButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.paymentMethodTable.tableFooterView = UIView()
        
        if BBProductDetails.order_typeid != 2
        {
            self.paymentMethod = PaymentDetails.paymentMethod
            
        }
        else
        {
            self.paymentMethod = updateCart.paymentMethod
        }
        for var i in 0..<paymentMethod.count
        {
            print("89567099- m, 4 7575  ,", paymentMethod[i]["payment_mode"] as! String)
            if paymentMethod[i]["payment_mode"] as! String == "Credit/Debit/UPI"
            {
                
                selectedPaymentlkpid = 4
                self.paymentMethod1 = 0
                print("razor pay")
                break
            }
            else if paymentMethod[i]["payment_mode"] as! String == "Stripe"{
                
                selectedPaymentlkpid = 5
                self.paymentMethod1 = 1
                print("stripe data")
                self.paymentMethod.removeAll()
                self.paymentMethod.append(["Credit/Debit/UPI"] as AnyObject)
                break
            }
        }
        
        if paymentMethod.count > 1 && paymentMethod.count != 0 && PaymentAddress.iscodApplicable == true
        {
            paymentOptionText.append("Cash")
            
            }
        // Do any additional setup after loading the view.
    }
    

    @IBAction func selectBBpoints(_ sender: Any) {
        if BBpoints != 0.0
        {
        if BBpointsSelected == false
        {
            BBpointsSelected = true
            BBPointsMarkIcon.image = UIImage(named: "filledCheckmark")
        }
        else
        {
            BBpointsSelected = false
            BBPointsMarkIcon.image = UIImage(named: "unfilledCheckmark")
        }
        }
        else{
            self.view.makeToast("You have no balance!", duration: 2.0, position: .center)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5
        {
            PaymentAddress.selectedPaymentMethod = "Cash on Delivery"
            PaymentAddress.selectedPayementlkpid = 1
            PaymentAddress.CardPayment()
        }
        else
        {
            PaymentAddress.selectedPaymentMethod = "Credit/Debit/UPI"
            PaymentAddress.selectedPayementlkpid = self.selectedPaymentlkpid
            PaymentAddress.paymenttype()
//             totalPrice.text = "Total: \(String(format:"%.2f", PaymentAddress.TotalCost))"
        }
        
        self.paymentMethodIndex = indexPath.row
        self.paymentMethodTable.reloadData()
        PaymentAddress.checkPaymentDetails()
        if paymentMethodIndex != -1
        {
            self.dismiss(animated: true, completion: nil)
            
            if PaymentAddress.paymentCardTextField.cardNumber == nil && PaymentAddress.selectedPayementlkpid == 5
            {
                PaymentAddress.openStripe()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if self.selectedPaymentlkpid == 5
        {
            
            count = 1
        }
        else
        {
            count = paymentOptionText.count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "payment", for: indexPath) as! PaymentTableViewCell
        cell.paymentMethodName.text = self.paymentOptionText[indexPath.row]
        
        if indexPath.row == 5
        {
            cell.codCharge.isHidden = false
            cell.codCharge.text = "\(PaymentAddress.currency)\(PaymentAddress.codCost)"
            var cost = PaymentAddress.TotalCost
            
           
        }
        else
        {
            cell.codCharge.isHidden = true
            
            
        }
        
        if indexPath.row == 0
        {
           
            cell.image1.image = UIImage(named: "visa")
            cell.image2.image = UIImage(named: "mascard")
            cell.image3.image = UIImage(named: "maestro")
            cell.image4.image = UIImage(named: "rupay_logo")
            cell.image5.image = nil
        }
        else if indexPath.row == 1
        {
            cell.image1.image = UIImage(named: "sbi_logo")
            cell.image2.image = UIImage(named: "hdfc_logo")
            cell.image3.image = UIImage(named: "icici_logo")
            cell.image4.image = UIImage(named: "axis_logo")
            cell.image5.image = nil
        }
        else if indexPath.row == 2
        {
            cell.image1.image = UIImage(named: "freecharge_logo")
            cell.image2.image = UIImage(named: "mobikwik_logo")
            cell.image3.image = UIImage(named: "phonePay_logo")
            cell.image4.image = nil
            cell.image5.image = nil
        }
        else if indexPath.row == 3
        {
            cell.image1.image = UIImage(named: "paytm_logo")
            cell.image2.image = nil
            cell.image3.image = nil
            cell.image4.image = nil
            cell.image5.image = nil
        }
        else if indexPath.row == 4
        {
            cell.image1.image = UIImage(named: "upi")
            cell.image2.image = nil
            cell.image3.image = nil
            cell.image4.image = nil
            cell.image5.image = nil
        }
        else if indexPath.row == 5
        {
            cell.image1.image = UIImage(named: "COD")
            cell.image2.image = nil
            cell.image3.image = nil
            cell.image4.image = nil
            cell.image5.image = nil
            }
        if PaymentAddress.selectedPayementlkpid == 4
        {
            
            
        }
        else
        {
            
            
            
        }
        if paymentMethodIndex == indexPath.row
        {
            cell.checkMarkIcon.image = UIImage(named: "filledCheckmark")
        }
        else
        {
            cell.checkMarkIcon.image = UIImage(named: "unfilledCheckmark")
        }
        return cell
    }
    
    
    // delegete strip method
    func paymentProcessing()
    {// Setup add card view controller
        
    }


}



extension BBPaymentViewController: STPAddCardViewControllerDelegate {
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock) {
        print("jhjjstripe", paymentMethod)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping STPErrorBlock) {
        _ = token.tokenId
        completion(nil)
        dismiss(animated: true, completion: nil)
    }}
