//
//  SettingViewController.swift
//  Hub9
//
//  Created by Deepak on 10/10/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import ZendeskSDK
import ZendeskCoreSDK



var settingPage = SettingViewController()

class SettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    
    
    
    var native = UserDefaults.standard
    var content = ["Address Manager","Notification", "Business Account", "Wallet","Add Closet","My Cart","My Feed", "Sign Out"]
    var contenticon = [ "useraddress","notification","businessIcon","Wallet","businessIcon","cart","businessIcon","logout"]
    
//    var
    var notificationis = "1"
    var user_purchase_modeid = "1"
    var settingChat = false
    var gradientLayer: CAGradientLayer!
    var t: Timer?
    var userName = "Demo User"
    var userImage = ""
    var selectedOrderStatus = 0
    var follow_shop_count = 0
    var closetCount = 0
    var follow_product_count = 0
    var product_recent_view_count = 0
    var GroupDeal = [AnyObject]()
    var x = 0
    var locker_code = ""
    
    
    @IBOutlet weak var moreTable: UITableView!
    
    
    func switch_user()  {
        let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        if usertype ==
        let changetype = native.string(forKey: "usertype")!
        debugPrint(changetype)
        if changetype == "1"
        {
            native.set(2, forKey: "usertype")
            native.synchronize()

        }
        else
        {
            native.set(1, forKey: "usertype")
            native.synchronize()
        }
        let SingleProduct = UIAlertAction(title: "Change Account Type", style: .default) { (alert: UIAlertAction) in
            //            debugPrint("Add Single Product")
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
            //            debugPrint("Cancelled")
        }
        
        actionMenu.addAction(SingleProduct)
        actionMenu.addAction(cancelAction)
        
        self.present(actionMenu, animated: true, completion: nil)
    }
   
    
    /**
     Simple Alert
     - Show alert with title and alert message and basic two actions
    */
    func showSimpleAlert() {
        
        self.tabBarController?.tabBar.isHidden = true
        
        if (native.object(forKey: "userid") != nil)
        {
           if native.object(forKey: "userid") != nil
            {
        native.set(native.object(forKey: "userid")!, forKey: "feed_userid")
        native.synchronize()
        
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "UserFeedPost"))! as UIViewController

        self.navigationController?.pushViewController(editPage, animated: true)
            }
        }
//        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .default //Set Style
        UIApplication.shared.isStatusBarHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
             statusBar.backgroundColor = UIColor.init(red: 247/250, green: 82/250, blue: 85/250, alpha: 1)
             UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
             UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.init(red: 247/250, green: 82/250, blue: 85/250, alpha: 1)
        }
        UIApplication.shared.statusBarStyle = .lightContent //Set Style
        UIApplication.shared.isStatusBarHidden = false //Set if hidden
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.setNeedsStatusBarAppearanceUpdate()
        getUserNavData()
        parseDealData()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    

    override func viewWillDisappear(_ animated: Bool) {
    if #available(iOS 13.0, *) {
        let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
         statusBar.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
         UIApplication.shared.keyWindow?.addSubview(statusBar)
            // get the navigation bar from the current navigation controller if there is one
            
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
            }
    else {
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
            self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 247/250, green: 247/250, blue: 247/250, alpha: 1)
            UIApplication.shared.statusBarStyle = .default
            }
            
            t?.invalidate()
            t = nil
        }
        
//    // add timer for auto scrolling
//    func scrollCollection(cltView: UICollectionView){
//        let _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(autoScroll(CuIndex: cltView)), userInfo: nil,
//                                     repeats: true)
//    }
//
//    // create auto scroll
//    @objc func autoScroll(CuIndex: UICollectionView) {
//
//        if self.x < self.GroupDeal.count {
//            let indexPath = IndexPath(item: x, section: 0)
//            CuIndex.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
//            self.x = self.x + 1
//        } else {
//            self.x = 0
//            CuIndex.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: true)
//        }
//    }
    
    //autoscroll deal collection
    func scrollCollection(cltView: UICollectionView) {
        var currentindex = 0
        var isScroll = true
        if self.GroupDeal.count>1
        {
            t = Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (timer) in
                debugPrint("isScroll", isScroll, currentindex)
                if (currentindex < self.GroupDeal.count) {
                    if currentindex == self.GroupDeal.count-1 {
                        isScroll = false;
                    } else if (currentindex == 0) {
                        isScroll = true;
                    }
                    if isScroll == true
                    {
                        currentindex += 1
                    }
                    else {
                        
                        currentindex -= 1
                    }
                    
                    let index = IndexPath(item: currentindex, section: 0)
                    cltView.scrollToItem(at: index, at: .centeredVertically, animated: true)
//                        cltView.delaysContentTouches = true
                    
//                    sleep(2)

                    }}
//            self.t!.invalidate()
            self.t!.fire()
            
            
            }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        settingPage = self
        
        if native.string(forKey: "Token") != ""
        {
        if native.string(forKey: "user_purchase_modeid") != nil
        {
        user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
            }
        self.moreTable.tableFooterView = UIView(frame: CGRect.zero)
        
        if native.string(forKey: "notification") != nil
        {
            notificationis = native.string(forKey: "notification")!
            
            
        }
        }
        else
        {
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = redViewController
        }
        // Do any additional setup after loading the view.
    }
    
    
    // set Notification
    @objc func updateNotification()
    {
        
        //            let userCode = native.string(forKey: "userCode")
        let parameters: [String:Any]
        var Pushtoken = ""
        debugPrint("Pushtoken", Pushtoken)
        var notifydata = false
        if self.notificationis == "1"
        {
            notifydata = false
        }
        else
        {
            notifydata = true
        }
        parameters = ["is_active":notifydata, "device_channelid":"1"]
        
        let token = self.native.string(forKey: "Token")!
        debugPrint(parameters)
        if token != ""
        {
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        debugPrint("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        Alamofire.request("\(b2burl)/users/edit_notification/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                //                    self.blurView.alpha = 0
                //                    self.activity.stopAnimating()
                //                    self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            if let json = response.result.value {
                debugPrint("JSON: \(json)")
                let jsonString = "\(json)"
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                debugPrint(result)
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    
                    else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                    {
                        //                            self.blurView.alpha = 0
                        //                            self.activity.stopAnimating()
                        debugPrint(dict1["status"]!)
                        if self.notificationis == "1"
                        {
                            self.notificationis = "0"
                            self.native.set("0", forKey: "notification")
                            self.native.synchronize()
                            
                        }
                        else
                        {
                            self.notificationis = "1"
                            self.native.set("1", forKey: "notification")
                            self.native.synchronize()
                            
                        }
                        
                    }
                    else
                    {
                        //                            self.blurView.alpha = 0
                        //                            self.activity.stopAnimating()
                        self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                        
                    }
                }
                self.moreTable.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
            }
            UIApplication.shared.endIgnoringInteractionEvents()
            return
            }}
        }
    }
    
    @objc func getUserNavData()
    {
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            debugPrint("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            Alamofire.request("\(b2burl)/users/user_profile_bar/", encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    //                    self.blurView.alpha = 0
                    //                    self.activity.stopAnimating()
                    //                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                if let json = response.result.value {
                    debugPrint("JSON: \(json)")
                    let jsonString = "\(json)"
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                debugPrint(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            debugPrint("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            debugPrint(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            debugPrint("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                        
                        else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                        {
                            //                            self.blurView.alpha = 0
                            //                            self.activity.stopAnimating()
                            debugPrint(dict1["status"]!)
                            if dict1["status"] as! String == "success"
                            {
                                if let data = dict1["response"] as? AnyObject
                                {
                                    
                                if data.count > 0
                                {
                                   
                                    if data["business_enabled"] is NSNull
                                    {}
                                    else{
                                        self.native.set(Int(data["business_enabled"] as! NSNumber), forKey: "business_enabled")
                                        self.native.synchronize()
                                    }
                                    if data["follow_shop_count"] is NSNull
                                    {}
                                    else{
                                        self.follow_shop_count = Int(data["follow_shop_count"] as! NSNumber)
                                    }
                                    if data["follow_shop_count"] is NSNull
                                    {}
                                    else{
                                        self.follow_shop_count = Int(data["follow_shop_count"] as! NSNumber)
                                    }
                                    if data["closet_post_count"] is NSNull
                                    {}
                                    else if let count = data["closet_post_count"] as? NSNumber{
                                        self.closetCount =  Int(count)
                                    }
                                    
                                    if data["product_recent_view_count"] is NSNull
                                    {}
                                    else{
                                        self.product_recent_view_count = Int(data["product_recent_view_count"] as! NSNumber)
                                    }
                                }
                                }
                            }
                        else
                        {
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                            }}
                        else
                        {
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                        }
                        self.moreTable.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
                        }}}
            }
        }
    }
    
  
    
    @objc func changeType()
    {
        
        let parameters: [String:Any]
        var Pushtoken = ""
        debugPrint("Pushtoken", Pushtoken)
        var notifydata = 1
        if self.user_purchase_modeid == "1"
        {
            notifydata = 2
        }
        else
        {
            notifydata = 1
        }
        parameters = ["purchase_modeid":notifydata]
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
        debugPrint(parameters)
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        debugPrint("Successfully post")
        let b2burl = native.string(forKey: "b2burl")!
        Alamofire.request("\(b2burl)/users/switch_purchase_mode/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            
            guard response.data != nil else { // can check byte count instead
                let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                //                    self.blurView.alpha = 0
                //                    self.activity.stopAnimating()
                //                    self.activityIndicator.stopAnimating()
                UIApplication.shared.endIgnoringInteractionEvents()
                return
            }
            if let json = response.result.value {
                debugPrint("JSON: \(json)")
                let jsonString = "\(json)"
                    let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        
                        else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                    {
                        //                            self.blurView.alpha = 0
                        //                            self.activity.stopAnimating()
                        debugPrint(dict1["status"]!)
                        if dict1["status"] as! String == "success" && dict1["response"] as! String == "User purchase mode updated."
                        {
                            if self.user_purchase_modeid == "1"
                            {
                                self.user_purchase_modeid = "2"
                                self.native.set("2", forKey: "user_purchase_modeid")
                                self.native.synchronize()
                                
                            }
                            else
                            {
                                self.user_purchase_modeid = "1"
                                self.native.set("1", forKey: "user_purchase_modeid")
                                self.native.synchronize()
                                
                            }
                            self.rootViewController()
                        }
                        
                    }
                    else
                    {
                        self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                    }
                        }
                }
                self.moreTable.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
            }
            UIApplication.shared.endIgnoringInteractionEvents()
            return
        }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row == 0
        {
            self.tabBarController?.tabBar.isHidden = true
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "userProfile"))! as UIViewController
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
        
        else if indexPath.row == 2
        {
            self.tabBarController?.tabBar.isHidden = true
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "orderpage"))! as UIViewController
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
        else if indexPath.row == 4
        {
            self.tabBarController?.tabBar.isHidden = true
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "GroupDeals"))! as UIViewController
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
        else if indexPath.row == 6
         {
            self.tabBarController?.tabBar.isHidden = true
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "displayAddress"))! as UIViewController
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1)
            self.navigationController?.pushViewController(rootPage, animated: true)
            
        }
            else if indexPath.row == 7
         {
            self.updateNotification()
         }
            else if indexPath.row == 8
         {
            var business_enabled = native.string(forKey: "business_enabled")!
            if business_enabled == "1"
            {
            self.changeType()
            }
            else
            {
                self.view.makeToast("Please contact our support to enable your business account", duration: 2.0, position: .bottom)
            }
         }
            else if indexPath.row == 9
        {
            self.tabBarController?.tabBar.isHidden = true
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "BBPoints"))! as UIViewController
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
            else if indexPath.row == 10
         {
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "userCloset"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
            
//            var firstName = ""
//            var lastName = ""
//            var userID = ""
//
//            if native.object(forKey: "userid") != nil
//            {
//                userID = native.string(forKey: "userid")!
//            }
//            if native.object(forKey: "firstname") != nil
//            {
//                firstName = native.string(forKey: "firstname")!
//            }
//            if native.object(forKey: "lastname") != nil
//            {
//                lastName = native.string(forKey: "lastname")!
//            }
//            debugPrint("Zendesk Support")
//            let identity = Identity.createAnonymous(name: "\(firstName) \(lastName): \(userID)", email: "")
//            Zendesk.instance?.setIdentity(identity)
//            Support.initialize(withZendesk: Zendesk.instance)
//            let hcConfig = HelpCenterUiConfiguration()
//            hcConfig.groupType = .category
//            let helpCenter = HelpCenterUi.buildHelpCenterOverview(withConfigs: [hcConfig])
//           self.navigationController?.setNavigationBarHidden(false, animated: true)
//
//                self.tabBarController?.tabBar.isHidden = true
//            self.navigationController!.pushViewController(helpCenter, animated: true)
         }
        else if indexPath.row == 12
        {
            self.showSimpleAlert()
        }
        else if indexPath.row == 11
        {
            BBProductDetails.senddataAddtocart = true
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "buying"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)
        }
        else if indexPath.row == 13
        {
           
            /**
             Simple Alert with Distructive button
             */
            let alertController = UIAlertController(title: "", message: "Are you sure that you want to logout of this app?", preferredStyle: .alert)
            
            let cancel = UIAlertAction(title: "Cancel" , style: .default) { (_ action) in
                        //code here…
                   
                    }
            let ok = UIAlertAction(title: "Logout" , style: .default) { (_ action) in
                 //code here…
                 self.Parsedata()
            }
            ok.setValue(UIColor.black, forKey: "titleTextColor")
            cancel.setValue(UIColor.lightGray, forKey: "titleTextColor")
            alertController.addAction(cancel)
            alertController.addAction(ok)
            alertController.view.tintColor = .yellow
            self.present(alertController, animated: true, completion: nil)
              
        }
        
    }
    @objc func ClosetItem()
    {
        native.set("My Closet", forKey: "search")
        native.synchronize()
        self.tabBarController?.tabBar.isHidden = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "mycloset"))! as UIViewController
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    @objc func ViewedItem()
    {
        native.set("Viewed Item", forKey: "search")
        native.synchronize()
        self.tabBarController?.tabBar.isHidden = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "viewedItem"))! as UIViewController
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    @objc func FavPage(selectedIndex: UIButton)
    {
        FavProductID1.selectedIndex = selectedIndex.tag
        self.tabBarController?.tabBar.isHidden = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "favouritepage"))! as UIViewController
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    
    @objc func UnpaidPage()
    {
        
        selectedOrderStatus = 0
        self.tabBarController?.tabBar.isHidden = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "orderpage"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    @objc func PendingPage()
    {
        
        selectedOrderStatus = 1
        self.tabBarController?.tabBar.isHidden = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "orderpage"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    @objc func ShippedPage()
    {
        
        selectedOrderStatus = 3
        self.tabBarController?.tabBar.isHidden = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "orderpage"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    @objc func DeliveredPage()
    {
        
        selectedOrderStatus = 4
        self.tabBarController?.tabBar.isHidden = true
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "orderpage"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var size = 0
        
        if indexPath.row == 0
        {
            size = 80
        }
        else if indexPath.row == 1
        {
            size = 60
            
        }
        else if indexPath.row == 2
        {
            size = 50
        }
        else if indexPath.row == 3
        {
            size = 70
        }
        else if indexPath.row == 4
        {
            if self.user_purchase_modeid == "1"
            {
                size = 50
            }
            else
            {
                size = 0
            }
        }
        else if indexPath.row == 5
        {
            if self.GroupDeal.count > 0 && self.user_purchase_modeid == "1"
            {
                size = 65
            }
            else
            {
                size = 0
            }
        }
        else if indexPath.row == 10
        {
            size =  0
        }
        else
        {
            size = 60
        }
        
        return CGFloat(size)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count + 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReuseCell = UITableViewCell()
        
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "userdata", for: indexPath) as! UserDataTableViewCell
            let image = native.string(forKey: "profilepic")!
            let firstname = native.string(forKey: "firstname")!
            let lastname = native.string(forKey: "lastname")!
            let username = "\(firstname) \(lastname)"
            debugPrint("gjegrgfje", "\(firstname), \(lastname)", image)
            if image != nil{
                let url = URL(string: image)
                cell.username.text = username
                let boderColor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                cell.userImage.layer.borderColor = boderColor.cgColor
                cell.userImage.layer.borderWidth = 1
                cell.userImage.clipsToBounds = true
                if url != nil
                {
                    DispatchQueue.main.async {
                        cell.userImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            ReuseCell = cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "usernavdata", for: indexPath) as! UserNavTableViewCell
            cell.favShopCount.text = "\(self.follow_shop_count)"
            cell.favProductCounts.text = "\(self.follow_product_count)"
            cell.viewedCount.text = "\(self.product_recent_view_count)"
            cell.productButton.tag = 0
            cell.productButton.addTarget(self, action: #selector(FavPage(selectedIndex:)), for: .touchUpInside)
            cell.shopButton.tag = 1
            cell.shopButton.addTarget(self, action: #selector(FavPage(selectedIndex:)), for: .touchUpInside)
            cell.ViewedItenButton.tag = 2
            cell.ViewedItenButton.addTarget(self, action: #selector(ViewedItem), for: .touchUpInside)
            cell.closetCount.text = "\(self.closetCount)"
            cell.closetButton.tag = 3
            cell.closetButton.addTarget(self, action: #selector(ClosetItem), for: .touchUpInside)
            ReuseCell = cell
        }
        else if indexPath.row == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "meorderheader", for: indexPath) as! MeOrderHeaderTableViewCell
            ReuseCell = cell
        }
        else if indexPath.row == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "meorderdata", for: indexPath) as! MeOrderDataTableViewCell
            cell.unpaidButton.addTarget(self, action: #selector(UnpaidPage), for: .touchUpInside)
            cell.pendingButton.addTarget(self, action: #selector(PendingPage), for: .touchUpInside)
            cell.shippedButton.addTarget(self, action: #selector(ShippedPage), for: .touchUpInside)
            cell.deliveredButton.addTarget(self, action: #selector(DeliveredPage), for: .touchUpInside)
            ReuseCell = cell
        }
        else if indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "medealsheader", for: indexPath) as! MedealsHeaderTableViewCell
            ReuseCell = cell
        }
        else if indexPath.row == 5
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "medealsdata", for: indexPath) as! MedealsDataTableViewCell
            if user_purchase_modeid == "2"
            {}
            else
            {
            cell.dealCollection.tag = indexPath.row
                cell.dealCollection.reloadData()
                
            }
            ReuseCell = cell
        }
        else
        {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moresetting", for: indexPath) as! MoreTableViewCell
        cell.notification.isHidden = true
        cell.icon.image = UIImage(named: contenticon[indexPath.row - 6])
        cell.content.text = content[indexPath.row - 6]
        
        if indexPath.row == 7
        {
            cell.notification.isHidden = false
            cell.notification.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
            
            if notificationis == "1"
            {
                cell.notification.setOn(true, animated: true)
            }
            else
            {
                cell.notification.setOn(false, animated: true)
            }
            cell.notification.addTarget(self, action: #selector(updateNotification), for: .touchUpInside)
            cell.nextIcon.isHidden = true
        }
        else if indexPath.row == 8
        {
            
            cell.notification.isHidden = false
            cell.notification.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
            cell.notification.addTarget(self, action: #selector(changeType), for: .touchUpInside)
            var business_enabled = "0"
            if native.string(forKey: "business_enabled") != nil
            {
            business_enabled = native.string(forKey: "business_enabled")!
            }
            if business_enabled == "1"
            {
                cell.notification.isEnabled = true
                if user_purchase_modeid == "1"
                {
                    cell.notification.setOn(false, animated: true)
                }
                else
                {
                    cell.notification.setOn(true, animated: true)
                }
            }
            else
            {
                cell.notification.isEnabled = false
                cell.notification.setOn(false, animated: true)
            }
            
            cell.nextIcon.isHidden = true
        }
            else if indexPath.row == 13
        {
            cell.nextIcon.isHidden = true
        }
            else
        {
            cell.nextIcon.isHidden = false
        }
        ReuseCell = cell
        }
        return ReuseCell
    }
    
    
    func Parsedata()
    {
        let token = self.native.string(forKey: "Token")!
                if token != ""
                {
                    CustomLoader.instance.showLoaderView()
                    let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                    debugPrint("Successfully post")
                    let b2burl = native.string(forKey: "b2burl")!
                    Alamofire.request("\(b2burl)/users/logout_user/",method: .post, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                        debugPrint(response)
                        debugPrint("response logout: \(response)")
                        let result = response.result
                        switch response.result
                        {
                        case .failure(let error):
                            //                debugPrint(error)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let err = error as? URLError, err.code == .notConnectedToInternet {
                                // Your device does not have internet connection!
                                debugPrint("Your device does not have internet connection!")
                                self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                debugPrint(err)
                            }
                            else if error._code == NSURLErrorTimedOut {
                                debugPrint("Request timeout!")
                                self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                            }else {
                                // other failures
                                self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                            }
                            
                        case .success:
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                            
                                else if let status = dict1["status"]{
                                    
                                    if status as! String  == "success"
                                    {
                                        
                                        debugPrint("status", status)
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        self.native.set("", forKey: "Token")
                                        self.native.set(1, forKey: "loggedIn")
                                        self.native.synchronize()
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                    else
                                    {
                                        CustomLoader.instance.hideLoaderView()
                                        UIApplication.shared.endIgnoringInteractionEvents()
                                        self.native.set("", forKey: "Token")
                                        self.native.set(1, forKey: "loggedIn")
                                        self.native.synchronize()
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                    CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                }  }}
                        
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.native.set("", forKey: "Token")
                        self.native.set(1, forKey: "loggedIn")
                        self.native.synchronize()
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = redViewController
                    }
                    }
        
       
}
    func Notification()
    {
        let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            CustomLoader.instance.showLoaderView()
            let header = ["Content-Type": "application/json", "Authentication": "Token\(token)"]
            debugPrint("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            Alamofire.request("\(b2burl)/users/edit_notification/",method: .post, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                debugPrint(response)
                debugPrint("response shop: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                    else if let status = dict1["status"]{
                        debugPrint("logout")
                        
                        if status as! String  == "success"
                        {
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                        }  }}
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
        }
    
    func helpBB()
    {
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            
            debugPrint(native.string(forKey: "support_firebaseid"))
            if native.string(forKey: "support_firebaseid") != nil && native.string(forKey: "support_name") != nil && native.string(forKey: "support_profile_pic_url") != nil
                
            {
        var shopFirebaseId = native.string(forKey: "support_firebaseid")!
        var shopName = native.string(forKey: "support_name")!
        var ShopLogo = native.string(forKey: "support_profile_pic_url")!
        CustomLoader.instance.showLoaderView()
        if shopFirebaseId == native.string(forKey: "firebaseid")! as! String
        {
            let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your own shop", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            //            topview.isHidden = true
            BBProductDetails.quotation = false
            
            native.set("1", forKey: "currentuserid")
            native.synchronize()
            var chatroom = CheckChatRoomViewController()
            
            chatroom.chekChatRoom(shopid: shopFirebaseId, shopname: shopName, shopProfilePic: ShopLogo)
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                // Your code with delay
                self.settingChat = true
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                
                self.performSegue(withIdentifier: "help", sender: self)
            }
                }
        }
        }
        else
        {
            
        }
    }
    
    func rootViewController()
    {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "rootController") as! UIViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = redViewController
        
    }
    
    
    
    ///status collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if user_purchase_modeid == "1"
        {
            return GroupDeal.count
        }
        else
        {
        return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize
        
        let width = (collectionView.frame.width)
        
        return CGSize(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var ReuseCell = UICollectionViewCell()
        if collectionView.tag == 5  {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "medealscollection", for: indexPath) as! MedealsCollectionViewCell
            scrollCollection(cltView: collectionView)
            if user_purchase_modeid == "1"
            {
            let order_item = self.GroupDeal[indexPath.row]["order_item"] as! [AnyObject]
            var ProductImage = order_item[0]["image_url"] as! AnyObject
            let image = ProductImage["200"] as! [AnyObject]
            var shareImage = "bb"
            if image.count > 0 {
                var imageUrl = image[0] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: imageUrl)
                if url != nil
                {
                    shareImage = imageUrl
                    cell.productImage.image = nil
                    DispatchQueue.main.async {
                        cell.productImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                }
            }
            cell.shareButton.layer.cornerRadius = 5
            cell.shareButton.clipsToBounds = true
            cell.shareButton.tag = indexPath.row
            cell.shareButton.addTarget(self, action: #selector(productToShare(IndexRow:)), for: .touchUpInside)
            if user_purchase_modeid == "2"
            {}
            else
            {
                if self.GroupDeal[indexPath.row]["waiting_user"] is NSNull
                {}
                else
                {
            cell.waitingUsers.text = "Waiting for \((self.GroupDeal[indexPath.row]["waiting_user"] as? NSNumber)!) user "
            if self.GroupDeal[indexPath.row]["deal_created"] is NSNull
            {}
            else
            {
                if self.GroupDeal[indexPath.row]["deal_created"] is NSNull
                {}
                else
                {
                let time = self.GroupDeal[indexPath.row]["deal_created"] as! NSNumber
                cell.localEndTime = TimeInterval(time.doubleValue)
                cell.startTimerProgress()
                }
                }
                }}}
            ReuseCell = cell
        }
        return ReuseCell
    }
    
    
    // parsePending data
    func parseDealData()
    {
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            let b20burl = native.string(forKey: "b2burl")!
            var url = ""
            if user_purchase_modeid == "1"
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(10)&offset=\(0)&status_ui=Pending&purchase_modeid=\(user_purchase_modeid)&order_typeid=2"
            }
            else
            {
                url = "\(b20burl)/order/buyer_orders/?limit=\(10)&offset=\(0)&status_ui=Pending&purchase_modeid=\(user_purchase_modeid)&order_typeid=3"
            }
            //            }
            
            debugPrint("Unpaid url", url)
            var a = URLRequest(url: NSURL(string: url) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response Unpaid: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                
                                
                                if let innerdict1 = dict1["data"] as? [AnyObject]
                                {   self.GroupDeal.removeAll()
                                    self.GroupDeal = innerdict1
                                }
                                
                            }
                            
                            
                        }
                    }
                }
                self.moreTable.reloadData()
                UIApplication.shared.endIgnoringInteractionEvents()
                CustomLoader.instance.hideLoaderView()
                
                
                //                        }
            }
        }
        
    }
    
    
    
     @objc func productToShare(IndexRow: UIButton)
    {
        let order_item = self.GroupDeal[IndexRow.tag]["order_item"] as! [AnyObject]
        if self.GroupDeal[IndexRow.tag]["locker_code"] is NSNull
        {}
        else
        {
            self.locker_code = "\((self.GroupDeal[IndexRow.tag]["locker_code"] as? NSNumber)!)"
        }
        // image to share
        var ProductImage = order_item[0]["image_url"] as! AnyObject
        let image = ProductImage["200"] as! [AnyObject]
        var shareImage = "bb"
        if image.count > 0 {
            var imageUrl = image[0] as! String
            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
            let url = NSURL(string: imageUrl)
            if url != nil
            {
                shareImage = imageUrl
            }}
        var text = "BBProduct"
        var product_share_link = "https://www.bulkbyte.com"
        var productImage = "thumbnail"
        
        if order_item[0]["title"] is NSNull
        {}
        else
        {
           text = order_item[0]["title"] as! String
        }
        if order_item[0]["product_share_link"] is NSNull
        {}
        else
        {
            product_share_link = "\(order_item[0]["product_share_link"] as! String)?locker_code=\(self.locker_code)"
        }
        let image1 = UIImage(named: shareImage)
        let myWebsite = NSURL(string: product_share_link)
        let shareAll = [text , image1 , myWebsite!] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    }




