//
//  UserChatTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 10/10/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class UserChatTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var usertext: UILabel!
    @IBOutlet weak var senderText: UILabel!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
