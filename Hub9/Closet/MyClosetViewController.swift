//
//  MyClosetViewController.swift
//  Hub9
//
//  Created by Deepak on 25/06/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import AttributedTextView



class MyClosetViewController: UIViewController, UIViewControllerTransitioningDelegate, UICollectionViewDelegate, UICollectionViewDataSource, PinterestLayoutDelegate {
 
 // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var returnImage:UIImage = UIImage()
    let native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var user_purchase_modeid = "1"
    var feedUser_Image = [String]()
    var listing_price = [String]()
    
 
 
 var followers_count = 0
 var following_count = 0
 var is_following = false
 var limit = 20
 var offset = 0
 
 /// data
 var userImage = [String]()
 var locationName = [String]()
 var uploadTime = [Double]()
 var feedVideo = [String]()
 var feedImage = [String]()
 var price = [String]()
 var user_feedid = [Int]()
 
 var feed_users = [AnyObject]()
 var feed_id = [Int]()
 var feed_text = [String]()
 var user_Comment_data = [[AnyObject]]()
 var isFeedID_liked = [Bool]()
 var shareLink = [String]()
 static var selectedIndex = 0
 static var user_feed_id = 0
 var feedDetailsIndex = 0
    
    
 
 
 // color
 var greenColor = UIColor(red: 18/255, green: 132/255, blue: 136/255, alpha: 1)
 
 
 @IBOutlet weak var userCloset: UICollectionView!
 
  override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.setNavigationBarHidden(false, animated: true)
         self.tabBarController?.tabBar.isHidden = true
         
         }
     
     override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.setNavigationBarHidden(false, animated: true)
         
         self.tabBarController?.tabBar.isHidden = false
         
         super.viewWillDisappear(animated)
         
         
     }
         
         func clearData()
         {
             
             feedImage.removeAll()
             feed_text.removeAll()
             userImage.removeAll()
             locationName.removeAll()
             uploadTime.removeAll()
             feedVideo.removeAll()
             
             feed_users.removeAll()
             feed_id.removeAll()
             user_Comment_data.removeAll()
             isFeedID_liked.removeAll()
             shareLink.removeAll()
             FeedViewController.selectedIndex = 0
             DispatchQueue.main.async {
             self.userCloset.isHidden = false
             
             self.userCloset.reloadData()
             
             }
             self.limit = 20
             self.offset = 0
             self.getRecommended_product(limit: self.limit, offset: self.offset)
             
         }
        
         
         override func viewDidLoad() {
             super.viewDidLoad()
             
             if let layout = userCloset.collectionViewLayout as? PinterestLayout
             {
                 layout.delegate = self
             }
             userCloset.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
             self.userCloset.isHidden=true
             self.clearData()
             
             // Do any additional setup after loading the view.
         }
 
    
        @IBAction func addCloset(_ sender: Any) {
            UploadUserClosetViewController.is_Editing = false
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "userCloset"))! as UIViewController
            self.navigationController?.pushViewController(editPage, animated: true)

        }
    
    
    
         
         func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
                 let visibleCells: [IndexPath] = userCloset.indexPathsForVisibleItems
                 let lastIndexPath = IndexPath(item: (feedImage.count - 1), section: 0)
                 if visibleCells.contains(lastIndexPath) {
                     //This means you reached at last of your datasource. and here you can do load more process from server
                     debugPrint("last index ", lastIndexPath)
                     offset = offset + 10
                     getRecommended_product(limit: limit, offset: offset)
                     
                 }
             }
             
     
     func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
         
         var heightInPoints = 250
         let image =  UIImage(named: "\(self.feedImage[indexPath.row])")
         debugPrint(image?.size.height)
         
         //last cell's width
         if(  (indexPath.row + 1) % 2 == 0){
            
         }else {
             heightInPoints = heightInPoints + 60
         }
         
         
         return CGFloat(heightInPoints)
     }
             
             func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                 if self.feed_id.count > 0
                 {
                     return self.feed_id.count
                 }
                 else {
         //            self.productCollection.showEmptyListMessage("No Product found!")
                     return 0
                 }
             }
             
             
             
             
             func returnImageUsingCacheWithURLString(url: NSURL) -> (UIImage) {
                 
                 // First check if there is an image in the cache
                 if let cachedImage = imageCache.object(forKey: url) as? UIImage {
                     
                     return cachedImage
                 }
                     
                 else {
                     // Otherwise download image using the url location in Google Firebase
                     URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                         
                         if error != nil {
                             debugPrint(error)
                         }
                         else {
                             DispatchQueue.global().async {
                                 
                                 // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                                 if let downloadedImage = UIImage(data: data!) {
                                     
                                     self.imageCache.setObject(downloadedImage, forKey: url)
                                     self.returnImage = downloadedImage
                                     
                                 }
                             }
                         }
                     }).resume()
                     return returnImage
                 }
             }
             func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                 var reuseCell: UICollectionViewCell = UICollectionViewCell()
                 if collectionView == userCloset {
                         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productswitch", for: indexPath) as! ProductCollectionViewCell
                         
                         //            debugPrint("product count \(Product.count)")
                         //            cell.product1.layer.cornerRadius = 5.0
                         cell.clipsToBounds = true
                         cell.layer.cornerRadius = 5
                         cell.layer.masksToBounds = true
                         
                         cell.contentView.layer.cornerRadius = 5
                         cell.contentView.layer.borderWidth = 1.0
                         
                         cell.contentView.layer.borderColor = UIColor.clear.cgColor
                         cell.contentView.layer.masksToBounds = true
                         
                         cell.layer.shadowColor = UIColor.lightGray.cgColor
                         cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                         cell.layer.shadowRadius = 2.0
                         cell.layer.shadowOpacity = 1.0
                         cell.layer.masksToBounds = false
                         cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
                         cell.productImage.image = nil
                     if self.feed_text.count > indexPath.row
                     {
                     if self.feed_text[indexPath.row] != ""
                     {
                     cell.productName.text = self.feed_text[indexPath.row]
                     }}
                                             
                             if self.feedVideo[indexPath.row] as String != ""
                             {
                                 let url = self.feedVideo[indexPath.row] as String
                                 
                                 let videoURL = NSURL(string: url)
                                 
                                 avPlayer = AVPlayer(url: videoURL! as URL)
                                 avPlayer.volume = 10
                                 avPlayer.isMuted = true
                                 let playerController = AVPlayerViewController()
                                 playerController.player = avPlayer

                                 self.addChildViewController(playerController)

                                 // Add your view Frame

                                 playerController.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                                 playerController.view.frame = cell.videoContainer.bounds
                                 playerController.showsPlaybackControls = false

                                 do {
                                     try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                                 } catch _ {
                                 }
                                 do {
                                     try AVAudioSession.sharedInstance().setActive(true)
                                 } catch _ {
                                 }
                                 do {
                                     try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                                 } catch _ {
                                 }
         //                        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.advance

                                 // Add subview in your view
                                 cell.videoContainer.isHidden = false
                                 cell.productImage.isHidden = true
                                 cell.videoContainer.addSubview(playerController.view)
                                 playerController.didMove(toParentViewController: self)

 //                                avPlayer.play()


                             }
                          else if self.feedImage.count > indexPath.row
                         {
                             cell.videoContainer.isHidden = true
                             cell.productImage.isHidden = false
                             
                             if self.feedImage[indexPath.row] as String != ""
                             {
                                 var image = self.feedImage[indexPath.row] as String
                                 image = image.replacingOccurrences(of: " ", with: "%20")
                                 let url = NSURL(string: image)
                                 if url != nil{
                                    DispatchQueue.main.async {
                                        cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                 }
                             
                         }
                         }
                     if self.userImage.count > indexPath.row
                     {
                         
                         if self.userImage[indexPath.row] as String != ""
                         {
                             var image = self.feedUser_Image[indexPath.row] as String
                             image = image.replacingOccurrences(of: " ", with: "%20")
                             let url = NSURL(string: image)
                             if url != nil{
                                DispatchQueue.main.async {
                                    cell.userImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                                }
                             }
                         
                     }
                     }
                     if self.locationName.count > indexPath.row
                     {
                         cell.locationName.text = self.locationName[indexPath.row]
                     }
                     if self.listing_price.count > indexPath.row
                     {
                         cell.priceTag.text = self.listing_price[indexPath.row]
                     }
                     
                         reuseCell = cell
                     }
                 
                 return reuseCell
             }
             
             
             func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                 self.feedDetailsIndex = indexPath.row
                 performSegue(withIdentifier: "closetDetails1", sender: self)
             }
         


         

         func getRecommended_product(limit: Int, offset: Int)
             {

                 CustomLoader.instance.showLoaderView()
                 let token = self.native.string(forKey: "Token")!

                     let b2burl = native.string(forKey: "b2burl")!

                 if native.object(forKey: "userid")! != nil
                 {
                     var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_closet/?limit=\(limit)&offset=\(offset)&userid_for_closet=\(native.string(forKey: "userid")!)") as! URL)
                     
 //                    var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed/?limit=\(limit)&offset=\(offset)&userid_for_detail=\(native.string(forKey: "feed_userid")!)&feed_typeid=2") as! URL)
                 if token != ""
                 {
                     a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
                 }
                 else{

                     a.allHTTPHeaderFields = ["Content-Type": "application/json"]
                 }
                 debugPrint(a, token)
                 Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                     //                                debugdebugPrint(response)
                     var url =
                     debugPrint("closet data", a, token)


                         switch response.result {
                         case .success:


                             if let result1 = response.result.value
                             {
                                 let result = response.result

                                debugPrint("result feed video", response)
                                 CustomLoader.instance.hideLoaderView()
                                 UIApplication.shared.endIgnoringInteractionEvents()
                                 if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                     if let invalidToken = dict1["detail"]{
                                         if invalidToken as! String  == "Invalid token."
                                         { // Create the alert controller
                                             self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                             let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                             let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                             let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                             appDelegate.window?.rootViewController = redViewController
                                         }
                                     }
                                     else if dict1["status"] as! String == "success"
                                     {
                                         if let innerdict = dict1["closet_data"] as? [AnyObject]
                                         {
                                             if innerdict.count > 0
                                                {
                                     for i in 0..<innerdict.count {

                                         if innerdict[i]["closet_share_link"] is NSNull
                                         {
                                             self.shareLink.append("pears.com")
                                         }
                                         else
                                         {
                                           self.shareLink.append(innerdict[i]["closet_share_link"] as! String)
                                         }


                                         if innerdict[i]["user_closetid"] is NSNull
                                         {
                                             self.feed_id.append(0)
                                         }
                                         else
                                         {
                                            self.feed_id.append((innerdict[i]["user_closetid"] as? Int)!)
                                         }

                                         if innerdict[i]["user_closetid"] is NSNull
                                         
                                         {
                                             self.user_feedid.append(0)
                                         }
                                         else
                                         {
                                             self.user_feedid.append((innerdict[i]["user_closetid"] as? Int)!)
                                         }
                                         
                                         if innerdict[i]["location_name"] is NSNull
                                         {
                                             self.locationName.append("")
                                         }
                                         else if let location =  innerdict[i]["location_name"] as? String
                                         {
                                            self.locationName.append(location)
                                         }
                                         if innerdict[i]["listing_price"] is NSNull
                                         {
                                             self.listing_price.append("")
                                         }
                                         else if let price =  innerdict[i]["listing_price"] as? String
                                         {
                                            self.listing_price.append(price)
                                         }
                                         
                                             if let commentData =  innerdict[i]["closet_user_data"] as? AnyObject
                                             {

                                         if innerdict.count > 0
                                         {
                                             if innerdict[i]["closet_uploaded"] is NSNull

                                             {
                                             self.uploadTime.append(0)
                                             }
                                         else
                                             {
                                         let date = innerdict[i]["closet_uploaded"] as! String
                                                 self.uploadTime.append(Double(date)!)
                                             }

                                      
                                             if innerdict[i]["closet_user_data"] is NSNull

                                             {}
                                             else
                                             {
                                                 
                                             if commentData["profile_pic_url"] as? String != ""
                                             {
                                                 self.userImage.append("")
                                             }
                                             else
                                             {self.userImage.append((commentData["profile_pic_url"] as? String)!)
                                             }
                                             }}
                                         }
                                     if innerdict[i]["title"] is NSNull
                                     {
                                         self.feed_text.append("")
                                     }
                                     else
                                     {
                                         self.feed_text.append((innerdict[i]["title"] as? String)!)
                                         }
                                 
                                     if innerdict[i]["media_url"] is NSNull
                                     {
                                         self.feedImage.append("")
                                         self.feedVideo.append("")
                                     }
                                     else
                                     {
                                          if let data = innerdict[i]["media_url"] as? AnyObject
                                         {
                                             if let imagedata = data["image"] as? [AnyObject]
                                            {
                                                debugPrint("image is not empty", imagedata)
                                                if imagedata.count > 0
                                                { self.feedImage.append((imagedata[0] as? String)!)
                                                }
                                             else
                                             {
                                                 self.feedImage.append("")
                                                 
                                                 }
                                             }
                                            else if let videodata = data["image"] as? String
                                            { self.feedVideo.append((data["image"] as? String)!)
                                            }
                                            else
                                            {
                                                debugPrint("outer image is empty")
                                                self.feedImage.append("")
                                            }
                                             if data["mp4"] is NSNull
                                             {
                                                 
                                                 self.feedVideo.append("")
                                             }
                                              
                                             else if let videodata = data["mp4"] as? [AnyObject]
                                             {
                                                 
                                                 if videodata.count > 0
                                                 {
                                                 self.feedVideo.append((videodata[0] as? String)!)
                                                 }
                                                 else
                                                 {
                                                     self.feedVideo.append("")
                                                     
                                                     }
                                                 
                                             }
                                             else if let videodata = data["mp4"] as? String
                                             {
                                                 debugPrint("outer video is empty")
                                                 self.feedVideo.append((data["mp4"] as? String)!)
                                             }
                                                 else
                                             {
                                                 self.feedVideo.append("")
                                                 
                                                 } }

                                         }
                                                 }}

                                         }
                                         }}
                                 DispatchQueue.main.async {
                                 CustomLoader.instance.hideLoaderView()
                                 UIApplication.shared.endIgnoringInteractionEvents()
                                     self.userCloset.reloadData()
                                     }
                             }
                             

                         case .failure(let error):
                             //                debugPrint(error)
                             CustomLoader.instance.hideLoaderView()
                             UIApplication.shared.endIgnoringInteractionEvents()
                             if let err = error as? URLError, err.code == .notConnectedToInternet {
                                 // Your device does not have internet connection!
                                 debugPrint("Your device does not have internet connection!")
                                 self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                 debugPrint(err)
                             }
                             else if error._code == NSURLErrorTimedOut {
                                 debugPrint("Request timeout!")
                                 self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                             }else {
                                 // other failures
                                 self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                             }
                             
                     }
                     
                     }
                     
                     
                 }
                 

             }
         
        
         

     //    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
     //        let visibleCells: [IndexPath] = self.MyFeedPost.indexPathsForVisibleRows!
     //        let lastIndexPath = IndexPath(item: (self.feed_id.count - 1), section: 0)
     //        if visibleCells.contains(lastIndexPath) {
     //            //This means you reached at last of your datasource. and here you can do load more process from server
     //            debugPrint("last index ", lastIndexPath)
     //            offset = offset + 10
     //            getRecommended_product(limit: limit, offset: offset)
     //
     //        }
     //    }
         
         

          
 //         @objc func comment_data(sender: UIButton)
 //         {
 //
 //             UserFeedPostViewController.user_feed_id = self.user_feedid[sender.tag]
 //             UserFeedPostViewController.selectedIndex = sender.tag
 //             performSegue(withIdentifier: "userFeedComments", sender: self)
 //
 //         }
          
          override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
              if (segue.identifier == "userFeedComments") {
              guard let destination = segue.destination as? FeedCommentDataViewController else
              {
                  debugPrint("return data")
                 return
              }
                  destination.user_feedid = self.user_feedid[self.feedDetailsIndex]
                }
              else if (segue.identifier == "closetDetails1") {
              guard let destination = segue.destination as? ClosetDetailViewController else
              {
                  debugPrint("return data")
                 return
              }
                
                 destination.user_feedid = self.user_feedid[self.feedDetailsIndex]
//                 destination.QuestionText = "\(self.feed_text[self.feedDetailsIndex])"
//                  if self.feedVideo.count > self.feedDetailsIndex
//                  {
//
//
//                      if (self.feedVideo[self.feedDetailsIndex] as? String)! != ""
//                      {
//                          destination.feedVideo = (self.feedVideo[self.feedDetailsIndex] as? String)!
//                      }
//                      else if (self.feedImage[self.feedDetailsIndex] as? String)! != ""
//                      {
//                         destination.feedImage = (self.feedImage[self.feedDetailsIndex] as? String)!
//                      }
//                      else
//                      {
//                          destination.isFeedID_liked = self.isFeedID_liked[self.feedDetailsIndex]
//                          destination.userImage = self.userImage[self.feedDetailsIndex]
//
//                          destination.QuestionText = "\(self.feed_text[self.feedDetailsIndex] as! String)"
//
//                      }
//
//                  }
//                  else
//                  {}

              }
              
          }
        
  }

