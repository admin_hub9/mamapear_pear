//
//  ClosetDetailViewController.swift
//  Hub9
//
//  Created by Deepak on 25/06/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import AVKit


var ClosetDetails = ClosetDetailViewController()

class ClosetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    
    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    var commentText = ""
    var native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenHeight = 0
    var phyzioChat = false
    
    /// data
    var locationName = ""
    var listing_price = ""
    var closet_condition = "New"
    var closet_conditionid = 1
    var feedImage = ""
    var feedVideo = ""
    var QuestionText  = ""
    var like = 0
    var likeCount = 0
    var Comment = false
    var data = [AnyObject]()
    var comment_data  = [AnyObject]()
    var comment_count = 0
    
    var user_feedid = 0
    var closet_feedid = 0
    var limit = 20
    var offset = 0
    var isFeedID_liked = false
    var shareLink = ""
    var userImage = ""
    var userFirbaseID = ""
    
    var userName = ""
    var uploadTime = 0.0
    var feed_text = ""
    var tagText = ""
    var isFollowing = false
    
    var is_editing = false
    var editIndex = 0
    
    ////edit option data
    var categoryid = 0
    var categoryText = ""
    var feed_share_link = ""
    var latitude = 0.0000000000
    var longitude = 0.00000000000
    var location_name = "Location*"
  
    
    @IBOutlet weak var shopButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var closetDetailsTable: UITableView!
    @IBOutlet weak var userImage1: RoundedImageView!
    @IBOutlet weak var userName1: UILabel!
    @IBOutlet weak var uploadTime1: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(false, animated: true)
           self.tabBarController?.tabBar.isHidden = true
        
        
           self.avPlayer.play()
           }
       
    override func viewWillDisappear(_ animated: Bool) {
           self.navigationController?.setNavigationBarHidden(false, animated: true)
           
           self.tabBarController?.tabBar.isHidden = false
           
           super.viewWillDisappear(animated)
           self.avPlayer.pause()
               NotificationCenter.default.post(name: Notification.Name("avPlayerDidDismiss"), object: nil, userInfo: nil)
           
       }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        self.is_editing = false
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ClosetDetails = self
        
        self.closetDetailsTable.tableFooterView = UIView()
        self.closetDetailsTable.isHidden = true
        
        self.getRecommended_product(limit: self.limit, offset: self.offset)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        // Do any additional setup after loading the view.
    }
    
    func relativeDate(time: Double) -> String {
        
        let theDate = Date(timeIntervalSince1970: TimeInterval(time))
        let components = Calendar.current.dateComponents([.day, .year, .month, .weekOfYear], from: theDate, to: Date())
        if let year = components.year, year == 1{
            return "\(year) year ago"
        }
        if let year = components.year, year > 1{
            return "\(year) years ago"
        }
        if let month = components.month, month == 1{
            return "\(month) month ago"
        }
        if let month = components.month, month > 1{
            return "\(month) months ago"
        }

        if let week = components.weekOfYear, week == 1{
            return "\(week) week ago"
        }
        if let week = components.weekOfYear, week > 1{
            return "\(week) weeks ago"
        }

        if let day = components.day{
            if day > 1{
                return "\(day) days ago"
            }else{
                "Yesterday"
            }
        }
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: theDate)
        return strDate
    }
    
    
    func updateProfile()
    {
            if self.userImage != ""
            {
                
                
              let imagedata = self.userImage

                var imageUrl = (imagedata)
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                if let url = NSURL(string: imageUrl )
                {
                    print("url", url)
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            self.userImage1.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
             }}
            self.userName1.text = self.userName
            
            if uploadTime != 0.0
            {
                
                let time  = self.relativeDate(time: uploadTime)
                    
                self.uploadTime1.text = "Posetd: \(time)"
            }
            
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var cellSize = 0
        switch indexPath.row {
        case 0:
            cellSize = Int(UITableViewAutomaticDimension)
        case 1:
            cellSize = Int(UITableViewAutomaticDimension)
        case 2:
        
            cellSize = 60
        case 3:
        
            cellSize = 50
        case 4:
            if self.comment_data.count>0
            {
                cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        case 5:
        if self.comment_data.count>1
            {
                cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        case 6:
        if self.comment_data.count>2
            {
        cellSize = Int(UITableViewAutomaticDimension)
            }
            else
            {
                cellSize = 0
            }
        case 7:
            cellSize = 60
        default:
        break
        }
    return CGFloat(cellSize)
                }
     
    /// number of row in table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return 2
        
        }
    
    // table view cell
    
    var postCell = postCommentsTableViewCell()
                
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                    
        var reuseCell = UITableViewCell()
        let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
        var ReuseCell = UITableViewCell()
        switch indexPath.row {
        case 0:
                    let cell2 = tableView.dequeueReusableCell(withIdentifier: "feedDetails", for: indexPath) as! FeedDetailsTableViewCell
                        cell2.videoController.isHidden=true
                        cell2.imageController.isHidden=true
//                        cell2.userConatiner.isHidden=true
                    cell2.tagName.layer.cornerRadius = cell2.tagName.frame.height/2
                    
                    cell2.tagName.clipsToBounds = true
                    
                    if self.tagText != ""
                    {
                        cell2.tagName.text = "  \(self.tagText)  "
                    }
                    else
                    {
                       cell2.tagName.text = ""
                    }
                    
                        if self.feedVideo  != ""
                        {
                            
                         
                            cell2.videoController.isHidden=false
                            cell2.imageController.isHidden=true
                            
//                            cell2.videoController.constant = 300.0
                            let url = self.feedVideo as! String
                                                                     
                        let videoURL = NSURL(string: url)
                            avPlayer = AVPlayer(url: videoURL! as URL)
                            avPlayer.volume = 10
                            avPlayer.isMuted = true
                            let playerController = AVPlayerViewController()
                            playerController.player = avPlayer
                            
                            self.addChildViewController(playerController)
                            
                            // Add your view Frame
                            
        //                                        playerController.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                            playerController.view.frame = cell2.videoController.bounds
                            playerController.showsPlaybackControls = true
                            playerController.videoGravity = AVLayerVideoGravity.resize.rawValue
                            
                            do {
                                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                            } catch _ {
                            }
                            do {
                                try AVAudioSession.sharedInstance().setActive(true)
                            } catch _ {
                            }
                            do {
                                try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                            } catch _ {
                            }
        //                                        avPlayer.automaticallyWaitsToMinimizeStalling = false
                            avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.pause
                            
                            // Add subview in your view
                            cell2.videoController.isHidden = false
                            
                            cell2.videoController.addSubview(playerController.view)
                            playerController.didMove(toParentViewController: self)

                            avPlayer.play()
                        }
                     
                        
                        else if (self.feedImage as? String)! != ""
                        {
                            
                            cell2.videoController.isHidden=true
                            cell2.imageController.isHidden=false
                            
                           
                          let imagedata = self.feedImage

                            var imageUrl = (imagedata)
                            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                            if let url = NSURL(string: imageUrl )
                            {
                                print("url", url)
                                if url != nil
                                {
                                    DispatchQueue.main.async {
                                        cell2.imageController.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                }
                            } }
                if self.feed_text != ""
                {
                cell2.textData.attributedText = ( "\(self.feed_text)".fontName("HelveticaNeue-Regular")).attributedText
                    }
                       
            ReuseCell = cell2
            case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "description", for: indexPath) as! ReviewDescriptionTableViewCell
            cell.descriptionText.attributedText = ( "\(self.QuestionText)".fontName("HelveticaNeue-Regular")).attributedText
                ReuseCell = cell
            
        
        
        case 2:
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentHeader", for: indexPath) as! commentHeaderTableViewCell
        if self.comment_count == 1
        {
            cell.totalComments.text = "\(self.comment_count) Comment"
            
        }
        else if self.comment_count > 1
        {
            cell.totalComments.text = "\(self.comment_count) Comments"
            
        }
        else
        {
            cell.totalComments.text = "Add Comments"
        }
        if self.comment_count > 3
        {
            cell.viewAllButton.isHidden = false
        }
        else
        {
            
            cell.viewAllButton.isHidden = true
        }
        cell.viewAllButton.tag = 4
        cell.viewAllButton.addTarget(self, action: #selector(comment_data(sender:)), for: .touchUpInside)
        
        ReuseCell = cell
        
        case 3:
     let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
     if self.comment_data.count>0
     {
         let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
         let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
         
         if self.native.object(forKey: "userid") != nil
         {
             let userid = self.native.string(forKey: "userid")!
             
             
             if self.comment_data[0]["user_id"] is NSNull
             {}
             else
             {   if Int(userid) != (self.comment_data[0]["user_id"] as? Int)!
             {
                 cell.deleteButton.isHidden = true
                 cell.editButton.isHidden = true
             }
                 else
             {
                 cell.deleteButton.isHidden = false
                 cell.editButton.isHidden = true
             }
                 
             }
         }
         if self.comment_data[0]["profile_pic_url"] is NSNull
         {}
         else
         {

             let imagedata = self.comment_data[0]["profile_pic_url"] as? String

                var imageUrl = (imagedata)!
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                if let url = NSURL(string: imageUrl )
                {
                    if url != nil
                    {
                        DispatchQueue.main.async {
                            cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                        }
                    }
                }
             }

         var like = 0
         var comment = 0
         
         
         if self.comment_data[0]["comment"] is NSNull
         {
             cell.NameAndText.text = ""
         }
         else
         {
             
             cell.NameAndText.attributedText = ("\(self.comment_data[0]["first_name"] as! String) \(self.comment_data[0]["last_name"] as! String): ".color(lightBlackColor).size(16.0)).attributedText
             cell.commentText.attributedText = ( "\(self.comment_data[0]["comment"] as! String)".color(lightGrayColor).size(14.0)).attributedText
             if self.comment_data[0]["inserted_at"] is NSNull
             {}
             else
             {
                  if let date1 = self.self.comment_data[0]["inserted_at"] as? String
                     {
                     let date = Date(timeIntervalSince1970: Double(date1)!)
                     let time  = self.relativeDate( time: Double(date1)!)
                         print("time", time)
                     cell.commentTime.text = "\(time)"
                         
                 }
             }
             
             
         }
         
         
         
         cell.deleteButton.tag = 0
         cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
     }
     ReuseCell = cell
     case 4:
     let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
     if self.comment_data.count>1
                 {
                     let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                     let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                     
                     if self.native.object(forKey: "userid") != nil
                     {
                         let userid = self.native.string(forKey: "userid")!
                         print("bjhfb 74", userid, self.native.string(forKey: "userid")!, self.comment_data[1])
                         
                         if self.comment_data[1]["user_id"] is NSNull
                         {}
                         else
                         {   if Int(userid) != (self.comment_data[1]["user_id"] as? Int)!
                         {
                             cell.deleteButton.isHidden = true
                             cell.editButton.isHidden = true
                         }
                             else
                         {
                             cell.deleteButton.isHidden = false
                             cell.editButton.isHidden = true
                         }
                             
                         }
                     }
                     if self.comment_data[1]["profile_pic_url"] is NSNull
                     {}
                     else
                     {

                         let imagedata = self.comment_data[1]["profile_pic_url"] as? String

                            var imageUrl = (imagedata)!
                            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                            if let url = NSURL(string: imageUrl )
                            {
                                if url != nil
                                {
                                    DispatchQueue.main.async {
                                        cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                }
                            }
                         }

                     var like = 0
                     var comment = 0
                     
                     
                     if self.comment_data[1]["comment"] is NSNull
                     {
                         cell.NameAndText.text = ""
                     }
                     else
                     {
                         
                         cell.NameAndText.attributedText = ("\(self.comment_data[1]["first_name"] as! String) \(self.comment_data[1]["last_name"] as! String): ".color(lightBlackColor).size(16.0)).attributedText
                         cell.commentText.attributedText = ( "\(self.comment_data[1]["comment"] as! String)".color(lightGrayColor).size(14.0)).attributedText
                         if self.comment_data[1]["inserted_at"] is NSNull
                         {}
                         else
                         {
                              if let date1 = self.self.comment_data[1]["inserted_at"] as? String
                                 {
                                 let date = Date(timeIntervalSince1970: Double(date1)!)
                                 let time  = self.relativeDate( time: Double(date1)!)
                                     print("time", time)
                                 cell.commentTime.text = "\(time)"
                             }
                         }
                         
                         
                     }
                     
                     
                     cell.deleteButton.tag = 1
                     cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
                 }
     ReuseCell = cell
     case 5:
     let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! CommentTableViewCell
     if self.comment_data.count>2
                 {
                     let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                     let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                     
                     if self.native.object(forKey: "userid") != nil
                     {
                         let userid = self.native.string(forKey: "userid")!
                         print("bjhfb 74", userid, self.native.string(forKey: "userid")!, self.comment_data[2])
                         
                         if self.comment_data[2]["user_id"] is NSNull
                         {}
                         else
                         {   if Int(userid) != (self.comment_data[2]["user_id"] as? Int)!
                         {
                             cell.deleteButton.isHidden = true
                             cell.editButton.isHidden = true
                         }
                             else
                         {
                             cell.deleteButton.isHidden = false
                             cell.editButton.isHidden = true
                         }
                             
                         }
                     }
                     if self.comment_data[2]["profile_pic_url"] is NSNull
                     {}
                     else
                     {

                         let imagedata = self.comment_data[2]["profile_pic_url"] as? String

                            var imageUrl = (imagedata)!
                            imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                            if let url = NSURL(string: imageUrl )
                            {
                                if url != nil
                                {
                                    DispatchQueue.main.async {
                                        cell.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                }
                            }
                         }

                     var like = 0
                     var comment = 0
                     
                     
                     if self.comment_data[2]["comment"] is NSNull
                     {
                         cell.NameAndText.text = ""
                     }
                     else
                     {
                         
                         cell.NameAndText.attributedText = ("\(self.comment_data[2]["first_name"] as! String) \(self.comment_data[2]["last_name"] as! String): ".color(lightBlackColor).size(16.0)).attributedText
                         cell.commentText.attributedText = ( "\(self.comment_data[2]["comment"] as! String)".color(lightGrayColor).size(14.0)).attributedText
                         if self.comment_data[2]["inserted_at"] is NSNull
                         {}
                         else
                         {
                              if let date1 = self.self.comment_data[2]["inserted_at"] as? String
                                 {
                                 let date = Date(timeIntervalSince1970: Double(date1)!)
                                 let time  = self.relativeDate( time: Double(date1)!)
                                     print("time", time)
                                 cell.commentTime.text = "\(time)"
                             }
                         }
                         
                         
                     }
                     
                     cell.deleteButton.tag = 2
                     cell.deleteButton.addTarget(self, action: #selector(deleteButton(Feed_Index:)), for: .touchUpInside)
                 }
     ReuseCell = cell
     case 6:
         postCell = tableView.dequeueReusableCell(withIdentifier: "postComment") as! postCommentsTableViewCell
    
         postCell.configure(text: "", placeholder: "Write a comment ")
         postCell.comment.delegate = self
         ReuseCell = postCell
    default:
            break
    
        }
                   return ReuseCell
        }

    /// delete button
    
    @objc func deleteButton(Feed_Index: UIButton)
    {
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
            
               
            //        print("product id:", id)
                let parameters: [String:Any] = ["user_feed_commentid":(comment_data[Feed_Index.tag]["user_feed_commentid"] as? Int)!]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/delete_feed_comment/"
            
            print("follow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    
                            
                            if self.comment_data.count > Feed_Index.tag
                            {
                                self.comment_data.remove(at: Feed_Index.tag)
                                if self.comment_count>0
                                {
                                    self.comment_count = self.comment_count - 1
                                }
                                
                            }
                            DispatchQueue.main.async {
                    self.closetDetailsTable.reloadData()
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                            }
                    }
                }}
                }
                }}
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
    
    
    
    
    @objc func comment_data(sender: UIButton)
    {
       FeedViewController.user_feed_id = self.user_feedid
        
         performSegue(withIdentifier: "feedDetailsComment", sender: self)
    }
    
    
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "feedDetailsComment") {
        guard let destination = segue.destination as? FeedCommentDataViewController else
        {
            print("return data")
           return
        }
              destination.user_feedid = self.user_feedid
            
            }}
        
    
    @objc func userFollowButton(sender: UIButton) {
            let token = self.native.string(forKey: "Token")!
        if native.object(forKey: "feed_userid") != nil
        {
            if self.user_feedid != 0
            {
                CustomLoader.instance.showLoaderView()
            
         let b2burl = native.string(forKey: "b2burl")!
         var url = ""
         var fav = ""
             if( self.isFollowing == true)
            {
                 fav = "userid_to_unfollow"
                url = "\(b2burl)/users/unfollow_user/"
            }
            else
            {
                 fav = "userid_to_follow"
                url = "\(b2burl)/users/follow_user/"
            }
            //        print("product id:", id)
            let parameters: [String:Any] = ["\(fav)": "\(self.user_feedid)"]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            
            
            print("follow user", url, parameters)
            Alamofire.request(url ,method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    if dict1["status"] as! String == "success"
                    {
                        
                     
                     if( self.isFollowing == true)
                     {
                         self.isFollowing = false
                     }
                     else
                     {
                          self.isFollowing = true
                     }
                         }
                    self.closetDetailsTable.reloadData()
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    }
                }}
                }
            }
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
        }
        
       
    
    
    @IBAction func menuOption(_ sender: Any) {
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
                if userid != ""
                {
                if Int(userid) == self.closet_feedid
                {
                    self.myOption()
                }
                else
                {
                    self.otherUserOption()
                }
                }}
        
           }
         
            func myOption()
            {
                    let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

                    let saveAction = UIAlertAction(title: "Delete Closet", style: .default, handler:
                    {
                        (alert: UIAlertAction!) -> Void in
                     self.deleteCloset()
                        
                    })

                    let editAction = UIAlertAction(title: "Edit Closet", style: .default, handler:
                    {
                        (alert: UIAlertAction!) -> Void in
                        print("edit")
                        self.editButton()
                    })
                    let soldAction = UIAlertAction(title: "Mark as  Sold", style: .default, handler:
                    {
                        (alert: UIAlertAction!) -> Void in
                        print("sold")
                        self.soldMark()
                        
                    })
                    

                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                    {
                        (alert: UIAlertAction!) -> Void in
                        print("Cancelled")
                    })
                    optionMenu.addAction(saveAction)
                    optionMenu.addAction(editAction)
                    optionMenu.addAction(soldAction)
                    optionMenu.addAction(cancelAction)
                    self.present(optionMenu, animated: true, completion: nil)
                }
    
    
    func editButton()
    {
        UploadUserClosetViewController.is_Editing = true
        let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "userCloset"))! as UIViewController
        
        self.navigationController?.pushViewController(editPage, animated: true)
        
    }
    
            func otherUserOption()
                {
                        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

                        let dealReport = UIAlertAction(title: "Report Closet", style: .default, handler:
                        {
                            (alert: UIAlertAction!) -> Void in
                            self.reportCloset()
                            print("delete")
                        })

                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
                        {
                            (alert: UIAlertAction!) -> Void in
                            print("Cancelled")
                        })
                        optionMenu.addAction(dealReport)
                        optionMenu.addAction(cancelAction)
                        self.present(optionMenu, animated: true, completion: nil)
            }
    
            /// edit button
    
                   
           /// delete button
           @objc func deleteCloset()
           {
               print("Successfully delete post")
               let token = self.native.string(forKey: "Token")!
               var fav = 0
                   if token != ""
                   {
                       CustomLoader.instance.showLoaderView()
                   if self.native.object(forKey: "userid") != nil
                   {
                       let userid = self.native.string(forKey: "userid")!
                   
                      
                   //        print("product id:", id)
                    let parameters: [String:Any] = ["user_closetid":self.user_feedid]
                   let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                   print("Successfully post")
               
                   let b2burl = native.string(forKey: "b2burl")!
                   var url = "\(b2burl)/users/delete_closet/"
                   
                   print("delete closet", url)
                   Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                       
                       
                       guard response.data != nil else { // can check byte count instead
                           let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                           alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                           self.present(alert, animated: true, completion: nil)
                           
                           
                           UIApplication.shared.endIgnoringInteractionEvents()
                           return
                       }
                       
                       switch response.result
                       {
                           
                           
                       case .failure(let error):
                           //                print(error)
                           CustomLoader.instance.hideLoaderView()
                           UIApplication.shared.endIgnoringInteractionEvents()
                           if let err = error as? URLError, err.code == .notConnectedToInternet {
                               // Your device does not have internet connection!
                               print("Your device does not have internet connection!")
                               self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                               print(err)
                           }
                           else if error._code == NSURLErrorTimedOut {
                               print("Request timeout!")
                               self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                           }else {
                               // other failures
                               self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                           }
                       case .success:
                           let result = response.result
                           CustomLoader.instance.hideLoaderView()
                           UIApplication.shared.endIgnoringInteractionEvents()
                           if let dict1 = result.value as? Dictionary<String, AnyObject>{
                               if let invalidToken = dict1["detail"]{
                                   if invalidToken as! String  == "Invalid token."
                                   { // Create the alert controller
                                       self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                       let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                       let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                       let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                       appDelegate.window?.rootViewController = redViewController
                                   }
                               }
                       
                       else if let json = response.result.value {
                           let jsonString = "\(json)"
                           print("res;;;;;", jsonString)
                           
                               if (dict1["status"] as? String)! == "success"
                               {
                                   self.view.makeToast("Your deal has been successfully deleted", duration: 2.0, position: .top)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.navigationController?.popViewController(animated: true)
                                }
                                   
                           }
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                       
                   
                   }
               }}
               }
               }}
           else
           {
               CustomLoader.instance.hideLoaderView()
               UIApplication.shared.endIgnoringInteractionEvents()
               let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                       let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                       let appDelegate = UIApplication.shared.delegate as! AppDelegate
                       appDelegate.window?.rootViewController = redViewController
                   }
               
               
           }
        
    @objc func soldMark()
    {
        print("Successfully sold mark")
        let token = self.native.string(forKey: "Token")!
        var fav = 0
            if token != ""
            {
                CustomLoader.instance.showLoaderView()
            if self.native.object(forKey: "userid") != nil
            {
                let userid = self.native.string(forKey: "userid")!
            
               
            //        print("product id:", id)
             let parameters: [String:Any] = ["user_closetid":self.user_feedid]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
        
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/mark_closet_sold/"
            
            print("delete closet", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    
                        if (dict1["status"] as? String)! == "success"
                        {
                            self.view.makeToast("Your deal has been successfully deleted", duration: 2.0, position: .top)
                         DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                     self.navigationController?.popViewController(animated: true)
                         }
                            
                    }
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
                
            
            }
        }}
        }
        }}
    else
    {
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        
        
    }
            @objc func reportCloset()
              {
                  print("Successfully report closet")
                  let token = self.native.string(forKey: "Token")!
                  var fav = 0
                      if token != ""
                      {
                          CustomLoader.instance.showLoaderView()
                      if self.native.object(forKey: "userid") != nil
                      {
                          let userid = self.native.string(forKey: "userid")!
                      
                         
                      //        print("product id:", id)
                       let parameters: [String:Any] = ["user_closetid":self.user_feedid]
                      let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                      print("Successfully post")
                  
                      let b2burl = native.string(forKey: "b2burl")!
                      var url = "\(b2burl)/users/report_closet/"
                      
                      print("report closet", url, parameters)
                      Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                          
                          
                          guard response.data != nil else { // can check byte count instead
                              let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                              alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                              self.present(alert, animated: true, completion: nil)
                              
                              
                              UIApplication.shared.endIgnoringInteractionEvents()
                              return
                          }
                          
                          switch response.result
                          {
                              
                              
                          case .failure(let error):
                              //                print(error)
                              CustomLoader.instance.hideLoaderView()
                              UIApplication.shared.endIgnoringInteractionEvents()
                              if let err = error as? URLError, err.code == .notConnectedToInternet {
                                  // Your device does not have internet connection!
                                  print("Your device does not have internet connection!")
                                  self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                  print(err)
                              }
                              else if error._code == NSURLErrorTimedOut {
                                  print("Request timeout!")
                                  self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                              }else {
                                  // other failures
                                  self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                              }
                          case .success:
                              let result = response.result
                              CustomLoader.instance.hideLoaderView()
                              UIApplication.shared.endIgnoringInteractionEvents()
                              if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                  if let invalidToken = dict1["detail"]{
                                      if invalidToken as! String  == "Invalid token."
                                      { // Create the alert controller
                                          self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                          let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                          let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                          let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                          appDelegate.window?.rootViewController = redViewController
                                      }
                                  }
                          
                          else if let json = response.result.value {
                              let jsonString = "\(json)"
                              print("res;;;;;", jsonString)
                              
                                  if (dict1["status"] as? String)! == "success"
                                  {
                                      self.view.makeToast("Your deal has been successfully deleted", duration: 2.0, position: .top)
                                   DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                               self.navigationController?.popViewController(animated: true)
                                   }
                                      
                              }
                      CustomLoader.instance.hideLoaderView()
                      UIApplication.shared.endIgnoringInteractionEvents()
                          
                      
                      }
                  }}
                  }
                  }}
              else
              {
                  CustomLoader.instance.hideLoaderView()
                  UIApplication.shared.endIgnoringInteractionEvents()
                  let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                          let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                          let appDelegate = UIApplication.shared.delegate as! AppDelegate
                          appDelegate.window?.rootViewController = redViewController
                      }
                  
                  
              }
        
    @IBAction func share_button(_ sender: Any) {
    
            // image to share
            let text = "Feed Product"
            var productImage = "thumbnail"
            
            let image = UIImage(named: productImage)
            let myWebsite = NSURL(string:self.shareLink)
            let shareAll = [text , image , myWebsite!] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
    
    
   
    
    func getRecommended_product(limit: Int, offset: Int)
                {

                    CustomLoader.instance.showLoaderView()
                    let token = self.native.string(forKey: "Token")!

                        let b2burl = native.string(forKey: "b2burl")!

//                    if native.object(forKey: "feed_userid")! != nil
//                    {
                        var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_closet_data/?limit=\(limit)&offset=\(offset)&user_closetid=\(self.user_feedid)") as! URL)
      
                    if token != ""
                    {
                        a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
                    }
                    else{

                        a.allHTTPHeaderFields = ["Content-Type": "application/json"]
                    }
                    print(a, token)
                    Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                        //                                debugPrint(response)
                        var url =
                        print("closet data", a, token)


                            switch response.result {
                            case .success:


                                if let result1 = response.result.value
                                {
                                    let result = response.result

                                   print("result feed video", response)
                                    CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                        if let invalidToken = dict1["detail"]{
                                            if invalidToken as! String  == "Invalid token."
                                            { // Create the alert controller
                                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                                appDelegate.window?.rootViewController = redViewController
                                            }
                                        }
                                        else if dict1["status"] as! String == "success"
                                        {
                                            if let innerdict = dict1["closet_data"] as? [AnyObject]
                                            {
                                                if innerdict.count > 0
                                                   {
                                        for i in 0..<innerdict.count {

                                            if innerdict[i]["sub_category_name"] is NSNull
                                            {
                                                
                                            }
                                            else
                                            {
                                              self.categoryText = innerdict[i]["sub_category_name"] as! String
                                                if innerdict[i]["sub_categoryid"] is NSNull
                                                {
                                                    
                                                }
                                                else if let data = innerdict[i]["sub_categoryid"] as? [AnyObject]
                                                {
                                                    self.categoryid = data[0] as! Int
                                                    
                                                }
                                            }
                                            
                                            if innerdict[i]["closet_condition"] is NSNull
                                            {
                                                self.closet_condition = "New"
                                            }
                                            else
                                            {
                                              self.closet_condition = innerdict[i]["closet_condition"] as! String
                                            }
                                            
                                            if innerdict[i]["closet_conditionid"] is NSNull
                                            {
                                                self.closet_conditionid = 1
                                            }
                                            else
                                            {
                                              self.closet_conditionid = (innerdict[i]["closet_conditionid"] as? Int)!
                                            }
                                            
                                            if innerdict[i]["location_name"] is NSNull
                                            {
                                                self.location_name = "Location*"
                                            }
                                            else
                                            {
                                              self.location_name = (innerdict[i]["location_name"] as? String)!
                                                if innerdict[i]["latitude"] is NSNull
                                                {
                                                    self.latitude = 0.000000000
                                                }
                                                else
                                                {
                                                    self.latitude = Double((innerdict[i]["latitude"] as? NSNumber)!)
                                                }
                                                
                                                if innerdict[i]["longitude"] is NSNull
                                                {
                                                    self.longitude = 0.000000000
                                                }
                                                else
                                                {
                                                    self.longitude = Double((innerdict[i]["longitude"] as? NSNumber)!)
                                                }
                                            }
                                            
                                            if innerdict[i]["closet_share_link"] is NSNull
                                            {
                                                self.shareLink = "pears.com"
                                            }
                                            else
                                            {
                                              self.shareLink = innerdict[i]["closet_share_link"] as! String
                                            }
                                            if innerdict[i]["label"] is NSNull
                                            {
                                                self.tagText = ""
                                            }
                                            else
                                            {
                                              self.tagText = innerdict[i]["label"] as! String
                                            }



                                            if innerdict[i]["user_closetid"] is NSNull
                                            
                                            {
                                                self.user_feedid = 0
                                            }
                                            else
                                            {
                                                self.user_feedid = ((innerdict[i]["user_closetid"] as? Int)!)
                                            }
                                            
                                            if innerdict[i]["location_name"] is NSNull
                                            {
                                                self.locationName = ""
                                            }
                                            else if let location =  innerdict[i]["location_name"] as? String
                                            {
                                               self.locationName = location
                                            }
                                            if innerdict[i]["listing_price"] is NSNull
                                            {
                                                self.listing_price = ""
                                            }
                                            else if let price =  innerdict[i]["listing_price"] as? String
                                            {
                                               self.listing_price = price
                                            }
                                            
                                                if let commentData =  innerdict[i]["closet_user_data"] as? AnyObject
                                                {

                                            if innerdict.count > 0
                                            {
                                                if innerdict[i]["closet_uploaded"] is NSNull

                                                {
                                                self.uploadTime = 0
                                                }
                                            else
                                                {
                                            let date = innerdict[i]["closet_uploaded"] as! String
                                                    self.uploadTime = (Double(date)!)
                                                }

                                         
                                                if innerdict[i]["closet_user_data"] is NSNull

                                                {}
                                                else
                                                {
                                                
                                                    if commentData["userid"] is NSNull
                                                    {
                                                        self.closet_feedid = 0
                                                    }
                                                    else if let id = commentData["userid"] as? Int
                                                    {
                                                       self.closet_feedid = id
                                                    }
                                                var firstname = ""
                                                var lastname = ""
                                                if commentData["first_name"] is NSNull
                                                {}
                                                else if let name = commentData["first_name"] as? String
                                                {
                                                    firstname = name
                                                }
                                                if commentData["last_name"] is NSNull
                                                {}
                                                else if let last = commentData["last_name"] as? String
                                                {
                                                    lastname = last
                                                }
                                                self.userName = "\(firstname) \(lastname)"
                                                    
                                                if commentData["profile_pic_url"] as? String != ""
                                                {
                                                    self.userImage = ""
                                                }
                                                else
                                                {self.userImage = ((commentData["profile_pic_url"] as? String)!)
                                                }
                                                }}
                                            }
                                        if innerdict[i]["title"] is NSNull
                                        {
                                            self.feed_text = ""
                                        }
                                        else
                                        {
                                            self.feed_text = ((innerdict[i]["title"] as? String)!)
                                            }
                                        if innerdict[i]["sub_category_name"] is NSNull
                                        {
                                            
                                        }
                                        else if let categoryName = innerdict[i]["sub_category_name"] as? String
                                        {
                                            self.categoryText = categoryName
                                            if innerdict[i]["sub_categoryid"] is NSNull
                                            {}
                                            else if let id = innerdict[i]["sub_categoryid"] as? AnyObject
                                            {
                                                print("id is ", id)
//                                                self.categoryid = id[0]
                                            }
                                            
                                            }
                                    
                                        if innerdict[i]["media_url"] is NSNull
                                        {
                                            self.feedImage = ""
                                        self.feedVideo = ""
                                        }
                                        else
                                        {
                                             if let data = innerdict[i]["media_url"] as? AnyObject
                                            {
                                                if let imagedata = data["image"] as? [AnyObject]
                                               {
                                                   print("image is not empty", imagedata)
                                                   if imagedata.count > 0
                                                   { self.feedImage = ((imagedata[0] as? String)!)
                                                   }
                                                else
                                                {
                                                    self.feedImage = ""
                                                    
                                                    }
                                                }
                                               else if let videodata = data["image"] as? String
                                               { self.feedVideo = ((data["image"] as? String)!)
                                               }
                                               else
                                               {
                                                   print("outer image is empty")
                                                   self.feedImage = ""
                                               }
                                                if data["mp4"] is NSNull
                                                {
                                                    
                                                    self.feedVideo = ""
                                                }
                                                 
                                                else if let videodata = data["mp4"] as? [AnyObject]
                                                {
                                                    
                                                    if videodata.count > 0
                                                    {
                                                    self.feedVideo = ((videodata[0] as? String)!)
                                                    }
                                                    else
                                                    {
                                                        self.feedVideo = ""
                                                        
                                                        }
                                                    
                                                }
                                                else if let videodata = data["mp4"] as? String
                                                {
                                                    print("outer video is empty")
                                                    self.feedVideo = ((data["mp4"] as? String)!)
                                                }
                                                    else
                                                {
                                                    self.feedVideo = ""
                                                    
                                                    } }

                                            }
                                                    }}

                                            }
                                            }}
                                    DispatchQueue.main.async {
                                    CustomLoader.instance.hideLoaderView()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                        self.closetDetailsTable.isHidden=false
                                        self.updateProfile()
                                        self.closetDetailsTable.reloadData()
                                        }
                                }
                                

                            case .failure(let error):
                                //                print(error)
                                CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let err = error as? URLError, err.code == .notConnectedToInternet {
                                    // Your device does not have internet connection!
                                    print("Your device does not have internet connection!")
                                    self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                                    print(err)
                                }
                                else if error._code == NSURLErrorTimedOut {
                                    print("Request timeout!")
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                                }else {
                                    // other failures
                                    self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                                }
                                
                        }
                        
                        }
                        
                        
//                    }
                    

                }
    
    
    
    /// post comment
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
         //cell = tbl.dequeueReusableCell(withIdentifier: "CELL") as! TableViewCell


       print( postCell.returnTextOfTextField() )
        print(postCell.comment.text)
        self.commentText = postCell.comment.text!
        postCell.comment.resignFirstResponder()
        self.postComment()
        return true
    }
     
    func postComment()
    {
      self.view.endEditing(true)
        
         if commentText != ""
         {
             
         let token = self.native.string(forKey: "Token")!
         var fav = 0
             if token != ""
             {
                 CustomLoader.instance.showLoaderView()
             if self.native.object(forKey: "userid") != nil
             {
                 
                 let parameters: [String:Any] = ["user_feedid":self.user_feedid,"comment": commentText]
             let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
             print("Successfully post")

             let b2burl = native.string(forKey: "b2burl")!
             var url = "\(b2burl)/users/add_comment_to_feed/"


             print("comment product", url, parameters)
             Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in


                 guard response.data != nil else { // can check byte count instead
                     let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                     self.present(alert, animated: true, completion: nil)


                     UIApplication.shared.endIgnoringInteractionEvents()
                     return
                 }
                 print("resonce comment", response)

                 switch response.result
                 {


                 case .failure(let error):
                     //                print(error)
                     CustomLoader.instance.hideLoaderView()
                     UIApplication.shared.endIgnoringInteractionEvents()
                     if let err = error as? URLError, err.code == .notConnectedToInternet {
                         // Your device does not have internet connection!
                         print("Your device does not have internet connection!")
                         self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                         print(err)
                     }
                     else if error._code == NSURLErrorTimedOut {
                         print("Request timeout!")
                         self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                     }else {
                         // other failures
                         self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                     }
                 case .success:
                     let result = response.result
                     CustomLoader.instance.hideLoaderView()
                     UIApplication.shared.endIgnoringInteractionEvents()
                     if let dict1 = result.value as? Dictionary<String, AnyObject>{
                         if let invalidToken = dict1["detail"]{
                             if invalidToken as! String  == "Invalid token."
                             { // Create the alert controller
                                 self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                 let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                 let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                 let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                 appDelegate.window?.rootViewController = redViewController
                             }
                         }

                 else if let json = response.result.value {
                     let jsonString = "\(json)"


                             var first_name = ""
                             var last_name = ""
                             var profile_pic_url = "";
                             var user_feed_commentid = 0;
                             var user_id = 0

                             if dict1["response"] is NSNull
                             {}
                             else
                             {
                                 if let commentData = dict1["response"] as? AnyObject
                                 {
                                     if let data = commentData["response"]
                                     {
                                     if commentData["response"] is NSNull
                                     {}
                                     else
                                     {
                                        user_feed_commentid = (commentData["response"] as? Int)!
                                     }
                                     }
                                 }
                             }



                             if self.native.object(forKey: "userid") != nil
                             {
                                 user_id = Int(self.native.string(forKey: "userid")!)!
                             }

                             if self.native.object(forKey: "profilepic") != nil
                             {
                                 profile_pic_url = self.native.string(forKey: "profilepic")!
                             }
                             if self.native.object(forKey: "firstname") != nil
                             {
                                 first_name = self.native.string(forKey: "firstname")!
                             }
                             if self.native.object(forKey: "lastname") != nil
                             {
                                 last_name = self.native.string(forKey: "lastname")!
                             }
                             if self.native.object(forKey: "userid") != nil
                             {
                                 last_name = self.native.string(forKey: "userid")!
                             }
                             if self.native.object(forKey: "lastname") != nil
                             {
                                 last_name = self.native.string(forKey: "lastname")!
                             }

                     var timestamp = NSDate().timeIntervalSince1970
                        
                        var allComment = self.comment_data
                             self.comment_data.removeAll()
                             
                        self.comment_data.append(["comment": self.commentText,
                                             "first_name": first_name,
                                             "inserted_at": "\(timestamp)",
                                             "last_name": last_name,
                                             "profile_pic_url": "\(profile_pic_url)",
                                    "user_feed_commentid": user_feed_commentid,
                                    "user_id": user_id] as AnyObject)
                            self.comment_count = self.comment_count + 1
                             for var i in 0..<allComment.count
                             {
                                 self.comment_data.append(allComment[i])
                             }
                     


                     self.commentText = ""

                     DispatchQueue.main.async {
                     self.closetDetailsTable.reloadData()
                     CustomLoader.instance.hideLoaderView()
                     UIApplication.shared.endIgnoringInteractionEvents()

                             }

                     }
                 }}
                 }
                 }}
             else
             {
                 CustomLoader.instance.hideLoaderView()
                 UIApplication.shared.endIgnoringInteractionEvents()
                 let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                 let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                 let appDelegate = UIApplication.shared.delegate as! AppDelegate
                 appDelegate.window?.rootViewController = redViewController
             }


     }
         else
         {
             self.view.makeToast("Please type comment first.", duration: 2.0, position: .center)
         }
     }
    
    
    @IBAction func contactUser(_ sender: Any) {
    
            let token = self.native.string(forKey: "Token")!
            if token != nil
            {
                CustomLoader.instance.showLoaderView()
                self.phyzioChat = true
                if self.userFirbaseID != "" && self.userName != ""
                {if  native.object(forKey: "firebaseid") != nil
                    {
                    if self.userFirbaseID == native.string(forKey: "firebaseid")!
                    {
                    UIApplication.shared.endIgnoringInteractionEvents()
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your profile", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        native.set("1", forKey: "currentuserid")
                        native.synchronize()
                        var chatroom = CheckChatRoomViewController()

                        chatroom.chekChatRoom(shopid: userFirbaseID, shopname: userName, shopProfilePic: userImage)
    //
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                            // Your code with delay
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()

                            self.performSegue(withIdentifier: "ReviewChat", sender: self)
                        }
                        }
                    }
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    }
                    
                }
                else
                {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            
        }
        else
            {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
}
