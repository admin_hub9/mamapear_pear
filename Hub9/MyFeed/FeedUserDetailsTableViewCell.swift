//
//  FeedUserDetailsTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 09/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class FeedUserDetailsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var UserImage: RoundedImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var followButton: UIButton!
    
    @IBOutlet weak var chatButton: UIButton!
    
    @IBOutlet weak var totalFollower: UILabel!
    
    @IBOutlet weak var totalFollowing: UILabel!
    
    @IBOutlet weak var userBio: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
