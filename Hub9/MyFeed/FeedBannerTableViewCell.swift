//
//  FeedBannerTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 09/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class FeedBannerTableViewCell: UITableViewCell {

    @IBOutlet weak var bannerImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
