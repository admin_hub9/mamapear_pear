//
//  FeedPostContainerTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 09/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class FeedPostContainerTableViewCell: UITableViewCell {

    
    /// user details
    
    @IBOutlet weak var userImage: RoundedImageView!
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var uploadTime: UILabel!
    
    
    // container
    @IBOutlet weak var videoContainer: UIView!
    
    @IBOutlet weak var videoConstrains: NSLayoutConstraint!
    
    @IBOutlet weak var imageConstrains: NSLayoutConstraint!
    @IBOutlet weak var imageContainer: UIImageView!
    
    @IBOutlet weak var userComments: UILabel!

    
    
    @IBOutlet weak var deletePost: UIButton!
    
    @IBOutlet weak var like_comment: UILabel!
    
    /// button
    
    @IBOutlet weak var feedDetailButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var displayImageButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
