//
//  UserFeedPostViewController.swift
//  Hub9
//
//  Created by Deepak on 13/04/20.
//  Copyright © 2020 Deepak. All rights reserved.
//


import UIKit
import Alamofire
import AVKit
import AttributedTextView


//var userFeedPost = UserFeedPostViewController()

class UserFeedPostViewController: UIViewController, UIViewControllerTransitioningDelegate, UITableViewDelegate, UITableViewDataSource {

    
    /// chat
    var shopFirebaseId = ""
    var shopName = ""
    var ShopLogo = ""
    var banner_url = ""
    var ShopId = 0
    var phyzioChat = false
    
    
    // add video for product
    var avPlayer = AVPlayer()
    var avPlayerLayer = AVPlayerLayer()
    var playerController = AVPlayerViewController()
    
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var returnImage:UIImage = UIImage()
    let native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var user_purchase_modeid = "1"

    
    
    var followers_count = 0
    var following_count = 0
    var is_following = false
    var limit = 20
    var offset = 0
    
    /// data
    var userImage = [String]()
    var userName = [String]()
    var uploadTime = [Double]()
    var feedVideo = [String]()
    var feedImage = [String]()
    var like = [Int]()
    var Comment = [Int]()
    var user_feedid = [Int]()
    
    var feed_users = [AnyObject]()
    var feed_id = [Int]()
    var feed_text = [String]()
    var user_Comment_data = [[AnyObject]]()
    var isFeedID_liked = [Bool]()
    var shareLink = [String]()
    static var selectedIndex = 0
    static var user_feed_id = 0
    
    
    
    
  
    @IBOutlet weak var MyFeedPost: UITableView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        
        
        
    }
    
    func clearData()
    {
        
        feedImage.removeAll()
        feed_text.removeAll()
        userImage.removeAll()
        userName.removeAll()
        uploadTime.removeAll()
        feedVideo.removeAll()
        like.removeAll()
        Comment.removeAll()
        feed_users.removeAll()
        feed_id.removeAll()
        user_Comment_data.removeAll()
        isFeedID_liked.removeAll()
        shareLink.removeAll()
        FeedViewController.selectedIndex = 0
        DispatchQueue.main.async {
        self.MyFeedPost.isHidden = false
        self.MyFeedPost.beginUpdates()
        self.MyFeedPost.reloadData()
        self.MyFeedPost.endUpdates()
        }
        self.limit = 20
        self.offset = 0
        self.getRecommended_product(limit: self.limit, offset: self.offset)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        userFeedPost = self
        self.MyFeedPost.tableFooterView = UIView()
        self.MyFeedPost.isHidden=true
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        self.getUserData()
        
        }
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         
        if indexPath.row == 0
        {
            return 250
        }
        else
        {
            return UITableViewAutomaticDimension
        }}
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            print("total", self.feedVideo.count  + 2)
            return self.feed_id.count  + 2
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            var reuseCell = UITableViewCell()
            let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
            
            if indexPath.row == 0
            {
            let cell = tableView.dequeueReusableCell(withIdentifier: "feedBanner", for: indexPath) as! FeedBannerTableViewCell
                if self.banner_url != ""
                {
                var imageUrl = self.banner_url
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                var url = NSURL(string:imageUrl as! String )
                
                if url != nil{
                    DispatchQueue.main.async {
                        cell.bannerImg.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                    }}
                reuseCell = cell
            }
            else if indexPath.row == 1
            {
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "feedUserData", for: indexPath) as! FeedUserDetailsTableViewCell
                if userImage.count > indexPath.row - 1
                {
                    print("userdata", self.userImage, self.userName)
                if self.ShopLogo != ""
                {
                var imageUrl = self.ShopLogo
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                var url = NSURL(string:imageUrl as! String )
                
                if url != nil{
                    DispatchQueue.main.async {
                        cell1.UserImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                    }}
                cell1.userName.text = shopName
                }
                
                cell1.chatButton.addTarget(self, action: #selector(contactSeller), for: .touchUpInside)
                cell1.totalFollower.text = "\(self.followers_count) follower"
                cell1.totalFollowing.text = "\(self.following_count) following"
                if( self.is_following == true)
                {
                
                    cell1.followButton.setTitle("unfollow", for: .normal)
                }
                else
                {
                    cell1.followButton.setTitle("follow", for: .normal)
                }
                cell1.followButton.layer.cornerRadius = 13
                cell1.followButton.layer.borderWidth = 1
                cell1.followButton.tag = indexPath.row
                cell1.followButton.addTarget(self, action: #selector(favourite(_:)), for: .touchUpInside)
                cell1.followButton.layer.borderColor = UIColor(red: 75/255, green: 35/255, blue: 162/255, alpha: 1).cgColor
                cell1.followButton.clipsToBounds=true
                
                reuseCell = cell1
            }
            else
            {
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "feedPost", for: indexPath) as! FeedPostContainerTableViewCell
                cell2.videoContainer.isHidden=true
                cell2.imageContainer.isHidden=true
                cell2.userComments.attributedText = ( "\(self.feed_text[indexPath.row - 2] as! String)".fontName("HelveticaNeue-Regular")).attributedText
                if self.uploadTime.count>indexPath.row
                {
                    if let timeResult = (self.uploadTime[indexPath.row] as? Double) {
                        let date = Date(timeIntervalSince1970: timeResult)
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
                        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                        dateFormatter.timeZone = .current
                        let localDate = dateFormatter.string(from: date)
                        cell2.uploadTime.text = "\(localDate) > Q&A"
                    }
                }
                if userImage.count > indexPath.row - 2
                {
                    print("userdata", self.userImage, self.userName)
                if self.ShopLogo != ""
                {
                var imageUrl = self.ShopLogo
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                var url = NSURL(string:imageUrl as! String )
                
                if url != nil{
                    DispatchQueue.main.async {
                        cell2.userImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                    }
                    }}
                cell2.userName.text = shopName
                }
               
                if (self.feedVideo[indexPath.row - 2] as? String)! != ""
                {
                    cell2.userComments.attributedText = ( "\(self.feed_text[indexPath.row - 2] as! String)".fontName("HelveticaNeue-Regular")).attributedText
                    if self.uploadTime.count>indexPath.row
                    {
                        if let timeResult = (self.uploadTime[indexPath.row] as? Double) {
                            let date = Date(timeIntervalSince1970: timeResult)
                            let dateFormatter = DateFormatter()
                            dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
                            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                            dateFormatter.timeZone = .current
                            let localDate = dateFormatter.string(from: date)
                            cell2.uploadTime.text = localDate
                        }
                    }
                    cell2.videoContainer.isHidden=false
                    cell2.imageContainer.isHidden=true
                    cell2.videoConstrains.constant = 300.0
                    let url = self.feedVideo[indexPath.row - 2] as! String
                                                             
                let videoURL = NSURL(string: url)
                    avPlayer = AVPlayer(url: videoURL! as URL)
                    avPlayer.volume = 10
                    avPlayer.isMuted = true
                    let playerController = AVPlayerViewController()
                    playerController.player = avPlayer
                    
                    self.addChildViewController(playerController)
                    
                    // Add your view Frame
                    
//                                        playerController.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                    playerController.view.frame = cell2.videoContainer.bounds
                    playerController.showsPlaybackControls = true
                    playerController.videoGravity = AVLayerVideoGravity.resize.rawValue
                    
                    do {
                        try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord)
                    } catch _ {
                    }
                    do {
                        try AVAudioSession.sharedInstance().setActive(true)
                    } catch _ {
                    }
                    do {
                        try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
                    } catch _ {
                    }
//                                        avPlayer.automaticallyWaitsToMinimizeStalling = false
                    avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.pause
                    
                    // Add subview in your view
                    cell2.videoContainer.isHidden = false
                    
                    cell2.videoContainer.addSubview(playerController.view)
                    playerController.didMove(toParentViewController: self)

                    avPlayer.play()
                }
                else if (self.feedImage[indexPath.row - 2] as? String)! != ""
                {
                    cell2.userComments.attributedText = ( "\(self.feed_text[indexPath.row - 2] as! String)".fontName("HelveticaNeue-Regular")).attributedText
                    if self.uploadTime.count>indexPath.row
                    {
                        if let timeResult = (self.uploadTime[indexPath.row] as? Double) {
                            let date = Date(timeIntervalSince1970: timeResult)
                            let dateFormatter = DateFormatter()
                            dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
                            dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                            dateFormatter.timeZone = .current
                            let localDate = dateFormatter.string(from: date)
                            cell2.uploadTime.text = localDate
                        }
                    }
                    cell2.videoContainer.isHidden=true
                    cell2.imageContainer.isHidden=false
                    cell2.videoConstrains.constant = 300.0
                    
                if let timeResult = (self.uploadTime[indexPath.row - 2] as? Double) {
                    let date = Date(timeIntervalSince1970: timeResult)
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.short //Set time style
                    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
                    dateFormatter.timeZone = .current
                    let localDate = dateFormatter.string(from: date)
                    cell2.uploadTime.text = localDate
                }
                  let imagedata = self.feedImage[indexPath.row - 2]

                    var imageUrl = (imagedata)
                    imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")

                    if let url = NSURL(string: imageUrl )
                    {
                        print("url", url)
                        if url != nil
                        {
                            DispatchQueue.main.async {
                                cell2.imageContainer.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "thumbnail"))
                            }
                        }
                    }
                }
                else{
                    cell2.videoConstrains.constant = 0.0
                }
                
                    
               
                    
                
                
                if self.isFeedID_liked.count>indexPath.row - 2
                    {
                    if self.isFeedID_liked[indexPath.row - 2] == true
                    {
                        cell2.likeButton.setImage(UIImage(named: "liked"), for: .normal)
                    }
                    else
                    {
                        cell2.likeButton.setImage(UIImage(named: "like"), for: .normal)
                    }
                    }
                    
                var like = ""
                var comment = ""
                if self.like[indexPath.row-2] > 1
                 {
                 like = "\(self.like[indexPath.row-2]) likes"
                 }
                else if self.like[indexPath.row-2] == 1
                {
                like = "\(self.like[indexPath.row-2]) like"
                }
                 
                 if self.Comment[indexPath.row-2] > 1
                  {
                  comment = "\(self.Comment[indexPath.row-2]) comments"
                  }
                else if self.Comment[indexPath.row-2] == 1
                {
                comment = "\(self.Comment[indexPath.row-2]) comment"
                }
                    cell2.like_comment.text = "\(like)  \(comment)"
                    cell2.likeButton.tag = indexPath.row - 2
                    cell2.likeButton.addTarget(self, action: #selector(LikeButton(Feed_Index:)), for: .touchUpInside)
                    cell2.commentButton.tag=indexPath.row - 2
                    cell2.commentButton.addTarget(self, action: #selector(comment_data(sender:)), for: .touchUpInside)
                    cell2.shareButton.tag=indexPath.row - 2
                    cell2.shareButton.addTarget(self, action: #selector(shareButton(sender:)), for: .touchUpInside)
                cell2.displayImageButton.tag=indexPath.row - 2
                cell2.displayImageButton.addTarget(self, action: #selector(displayImage(imgaeIndex:)), for: .touchUpInside)
                
                reuseCell = cell2
            }
            
           return reuseCell
//            let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
//            let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
//
//            if data[indexPath.row]["profile_pic_url"] is NSNull
//            {}
//            else
//            {
//
//                let imagedata = data[indexPath.row]["profile_pic_url"] as? String
//
//                   var imageUrl = (imagedata)!
//                   imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
//
//                   if let url = NSURL(string: imageUrl )
//                   {
//                       if url != nil
//                       {
//                           cell.userImage.af_setImage(withURL: url as! URL, placeholderImage: UIImage(named: "thumbnail"))
//                       }
//                   }
//                }
//
//                if data[indexPath.row]["comment"] is NSNull
//            {
//                cell.NameAndText.text = ""
//            }
//            else
//            {
//
//                cell.NameAndText.attributedText = ("\(data[indexPath.row]["first_name"] as! String) \(data[indexPath.row]["last_name"] as! String): ".color(lightBlackColor).size(16.0) + "\(data[indexPath.row]["comment"] as! String)".color(lightGrayColor).size(14.0)).attributedText
//
//
//            }
            
             
            
            
        }
    
    
    @objc func displayImage(imgaeIndex: UIButton)
    {
        UserFeedPostViewController.selectedIndex = imgaeIndex.tag
       performSegue(withIdentifier: "feedDetails2", sender: self)
    }
    

    func getRecommended_product(limit: Int, offset: Int)
        {

            CustomLoader.instance.showLoaderView()
            let token = self.native.string(forKey: "Token")!

                let b2burl = native.string(forKey: "b2burl")!

            if native.object(forKey: "feed_userid")! != nil
            {
                
                var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_user_feed/?limit=\(limit)&offset=\(offset)&userid_for_detail=\(native.string(forKey: "feed_userid")!)&feed_typeid=2") as! URL)
            if token != ""
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            }
            else{

                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
            print(a, token)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                var url =
                print("feed url recommended", a, token)


                    switch response.result {
                    case .success:


                        if let result1 = response.result.value
                        {
                            let result = response.result

                           print("result feed video", response)
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            if let dict1 = result.value as? Dictionary<String, AnyObject>{
                                if let invalidToken = dict1["detail"]{
                                    if invalidToken as! String  == "Invalid token."
                                    { // Create the alert controller
                                        self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                        let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = redViewController
                                    }
                                }
                                else if dict1["status"] as! String == "success"
                                {
                                    if let innerdict = dict1["feed_data"] as? [AnyObject]
                                    {
                                        if innerdict.count > 0
                                           {
                                for i in 0..<innerdict.count {

                                    if innerdict[i]["feed_share_link"] is NSNull
                                    {
                                        self.shareLink.append("pears.com")
                                    }
                                    else
                                    {
                                      self.shareLink.append(innerdict[i]["feed_share_link"] as! String)
                                    }


                                    if innerdict[i]["user_feedid"] is NSNull
                                    {
                                        self.feed_id.append(0)
                                    }
                                    else
                                    {
                                       self.feed_id.append((innerdict[i]["user_feedid"] as? Int)!)
                                    }

                                    if innerdict[i]["user_feedid"] is NSNull
                                    
                                    {
                                        self.user_feedid.append(0)
                                    }
                                    else
                                    {
                                        self.user_feedid.append((innerdict[i]["user_feedid"] as? Int)!)
                                    }
                                    
                                    if innerdict[i]["comment_data"] is NSNull
                                    {
                                        var data = [AnyObject]()
                                        self.user_Comment_data.append(data)
                                    }
                                    else
                                    {
                                       print(i)
                                        self.user_Comment_data.append((innerdict[i]["comment_data"] as? [AnyObject])!)
                                    }

                                    if innerdict[i]["like_data"] is NSNull
                                    {
                                        self.isFeedID_liked.append(false)
                                    }
                                    else
                                    {
                                       self.feed_users.append(innerdict[i]["like_data"] as! AnyObject)

                                    }

                                    if self.native.object(forKey: "userid") != nil
                                    {
                                        let userid = self.native.string(forKey: "userid")!
                                        if self.feed_users.count > i
                                        {
                                        if userid != ""
                                        {
                                            var data = self.feed_users[i] as! AnyObject
                                            if data.contains(Int(userid))
                                    {
                                         self.isFeedID_liked.append(true)
                                        
                                    }
                                    else
                                    {  self.isFeedID_liked.append(false)

                                    }

                                        }
                                        else
                                        {
                                           self.isFeedID_liked.append(true)
                                        }
                                        }}


                                        if let commentData =  innerdict[i]["feed_user_data"] as? AnyObject
                                        {

                                    if innerdict.count > 0
                                    {


                                        if innerdict[i]["feed_uploaded"] is NSNull

                                        {
                                        self.uploadTime.append(0)
                                        }
                                    else
                                        {
                                    let date = innerdict[i]["feed_uploaded"] as! String
                                            self.uploadTime.append(Double(date)!)
                                        }



                                        var firstName = ""
                                        var lastName = ""

                                        var commentCount = 0
                                        var like = 0
                                        if innerdict[i]["comment_count"] is NSNull
                                            {
                                                commentCount = 0
                                        }
                                            else
                                            {
                                          commentCount = (innerdict[i]["comment_count"] as? Int)!
                                        }

                                        if innerdict[i]["like_count"] is NSNull
                                            {
                                            like = 0
                                            }
                                            else
                                            {
                                          like = (innerdict[i]["like_count"] as? Int)!
                                        }

                                        self.like.append(like)
                                        self.Comment.append(commentCount)

                                        if innerdict[i]["feed_user_data"] is NSNull

                                        {}
                                        else
                                        {

                                        if commentData["first_name"] as? String != ""
                                        {
                                        firstName = (commentData["first_name"] as? String)!
                                        }

                                            if (commentData["last_name"] as? String)! != ""
                                        {
                                        lastName = (commentData["last_name"] as? String)!
                                        }
                                         self.userName.append("\(firstName) \(lastName)")

                                        if commentData["profile_pic_url"] as? String != ""
                                        {
                                            self.userImage.append("")
                                        }
                                        else
                                        {self.userImage.append((commentData["profile_pic_url"] as? String)!)
                                        }
                                        }}
                                    }
                                if innerdict[i]["feed_text"] is NSNull
                                {
                                    self.feed_text.append("")
                                }
                                else
                                {
                                    self.feed_text.append((innerdict[i]["feed_text"] as? String)!)
                                    }
                            
                                if innerdict[i]["media_url"] is NSNull
                                {
                                    self.feedImage.append("")
                                    self.feedVideo.append("")
                                }
                                else
                                {
                                     if let data = innerdict[i]["media_url"] as? AnyObject
                                    {
                                        if let imagedata = data["image"] as? [AnyObject]
                                       {
                                           print("image is not empty", imagedata)
                                           if imagedata.count > 0
                                           { self.feedImage.append((imagedata[0] as? String)!)
                                           }
                                        else
                                        {
                                            self.feedImage.append("")
                                            
                                            }
                                        }
                                       else if let videodata = data["image"] as? String
                                       { self.feedVideo.append((data["image"] as? String)!)
                                       }
                                       else
                                       {
                                           print("outer image is empty")
                                           self.feedImage.append("")
                                       }
                                        if data["mp4"] is NSNull
                                        {
                                            
                                            self.feedVideo.append("")
                                        }
                                         
                                        else if let videodata = data["mp4"] as? [AnyObject]
                                        {
                                            
                                            if videodata.count > 0
                                            {
                                            self.feedVideo.append((videodata[0] as? String)!)
                                            }
                                            else
                                            {
                                                self.feedVideo.append("")
                                                
                                                }
                                            
                                        }
                                        else if let videodata = data["mp4"] as? String
                                        {
                                            print("outer video is empty")
                                            self.feedVideo.append((data["mp4"] as? String)!)
                                        }
                                            else
                                        {
                                            self.feedVideo.append("")
                                            
                                            } }

                                    }


                                    CustomLoader.instance.hideLoaderView()
                                UIApplication.shared.endIgnoringInteractionEvents()

                                            }}

                                    }
                                    }}
                            DispatchQueue.main.async {
                                CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                                self.MyFeedPost.reloadData()
                                }
                        }
                        

                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                }
                
                }
                
                
            }
            

        }
    
    @objc func contactSeller()
        {
            let token = self.native.string(forKey: "Token")!
            if token != nil
            {
                
                self.phyzioChat = true
                if self.shopFirebaseId != nil && self.shopName != nil && self.ShopLogo != nil

                {

                    CustomLoader.instance.showLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if  native.object(forKey: "userid") != nil
                    {
                    if String(FeedViewController.user_feed_id) == native.string(forKey: "userid")!
                    {
                        CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                        let alert = UIAlertController(title: "Uh Oh", message: "You are trying to connect with your profile", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else
                    {

                        
                        native.set("1", forKey: "currentuserid")
                        native.synchronize()
                        var chatroom = CheckChatRoomViewController()

                        chatroom.chekChatRoom(shopid: shopFirebaseId, shopname: shopName, shopProfilePic: ShopLogo)
    //                   
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { // change 2 to desired number of seconds
                            // Your code with delay
                            CustomLoader.instance.hideLoaderView()
                            UIApplication.shared.endIgnoringInteractionEvents()

                            self.performSegue(withIdentifier: "UserChat", sender: self)
                        }
                        }
                    }
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
            else
            {

            }
        }
    
    

//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let visibleCells: [IndexPath] = self.MyFeedPost.indexPathsForVisibleRows!
//        let lastIndexPath = IndexPath(item: (self.feed_id.count - 1), section: 0)
//        if visibleCells.contains(lastIndexPath) {
//            //This means you reached at last of your datasource. and here you can do load more process from server
//            print("last index ", lastIndexPath)
//            offset = offset + 10
//            getRecommended_product(limit: limit, offset: offset)
//
//        }
//    }
    
    
    
    
    func getUserData()
    {
        
        let token = self.native.string(forKey: "Token")!
        if token != ""
        {
            if self.native.object(forKey: "feed_userid") != nil
            {
                
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
                Alamofire.request("\(b2burl)/users/get_user_detail/?userid_for_detail=\(native.string(forKey: "feed_userid")!)&feed_typeid=2", encoding: JSONEncoding.default, headers: header).responseJSON { response in
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    //                    self.blurView.alpha = 0
                    //                    self.activity.stopAnimating()
                    //                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                if let json = response.result.value {
                    print("JSON user details: \(json)\(b2burl)/users/get_user_detail/?userid_for_detail=\(self.native.string(forKey: "feed_userid")!)&feed_typeid=2")
                    let jsonString = "\(json)"
                    let result = response.result
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                        
                        else if (dict1["status"]as AnyObject) as! String  == "Success" || (dict1["status"]as AnyObject) as! String  == "success"
                        {
                            //                            self.blurView.alpha = 0
                            //                            self.activity.stopAnimating()
                            print("userdetails", dict1)
                            if dict1["status"] as! String == "success"
                            {
                                if let data = dict1["data"] as? [AnyObject]
                                {
                                    if data.count > 0
                                    {
                                    print("firebaseid", (data[0]["firebase_userid"] as? String)!)
                                    if data[0]["profile_pic_url"] is NSNull
                                    {}
                                    else
                                    {
                                        self.ShopLogo = (data[0]["profile_pic_url"] as? String)!
                                    }
                                    if data[0]["banner_url"] is NSNull
                                    {}
                                    else
                                    {
                                        self.banner_url = (data[0]["banner_url"] as? String)!
                                    }
                                    if data[0]["firebase_userid"] is NSNull
                                    {}
                                    else
                                    {
                                        self.shopFirebaseId = (data[0]["firebase_userid"] as? String)!
                                    }
                                    var first_name = ""
                                    var last_name = ""
                                    if data[0]["first_name"] is NSNull
                                    {}
                                    else
                                    {
                                        first_name = (data[0]["first_name"] as? String)!
                                    }
                                    if data[0]["last_name"] is NSNull
                                    {}
                                    else
                                    {
                                        last_name = (data[0]["last_name"] as? String)!
                                    }
                                    
                                    self.shopName = "\(first_name) \(last_name)"
                                    
                                    if data[0]["followers_count"] is NSNull
                                    {}
                                    else
                                    {
                                        self.followers_count = (data[0]["followers_count"] as? Int)!
                                    }
                                    if data[0]["following_count"] is NSNull
                                    {}
                                    else
                                    {
                                        self.following_count = (data[0]["following_count"] as? Int)!
                                    }
                                    if data[0]["is_following"] is NSNull
                                    {}
                                    else
                                    {
                                        if (data[0]["is_following"] as? Int)! == 0
                                        {
                                           self.is_following = false
                                        }
                                        else
                                        {
                                           self.is_following = true
                                        }
                                         
                                    }
                               
                                }
                                }}
                        else
                        {
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                            }}
                        else
                        {
                            self.view.makeToast(dict1["response"]! as! String, duration: 2.0, position: .bottom)
                        }
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                            
                return
                        }}
                    
                    }
                }
                
                
            }
            DispatchQueue.main.async {
            self.MyFeedPost.isHidden = false
            self.MyFeedPost.reloadData()
            self.getRecommended_product(limit: self.limit, offset: self.offset)
            }
        }
    }
    
    
       @objc func favourite(_ sender: UIButton) {
       //        self.blurView.alpha = 1
       //        self.activity.startAnimating()
               
               let token = self.native.string(forKey: "Token")!
               if token != "" && native.object(forKey: "feed_userid")! != nil
               {
                   CustomLoader.instance.showLoaderView()
               
            let b2burl = native.string(forKey: "b2burl")!
            var url = ""
            var fav = ""
                if( self.is_following == true)
               {
                    fav = "userid_to_unfollow"
                   url = "\(b2burl)/users/unfollow_user/"
               }
               else
               {
                    fav = "userid_to_follow"
                   url = "\(b2burl)/users/follow_user/"
               }
               //        print("product id:", id)
               let parameters: [String:Any] = ["\(fav)": "\(native.string(forKey: "feed_userid")!)"]
               let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
               print("Successfully post")
               
               
               print("follow user", url, parameters)
               Alamofire.request(url ,method: .post,parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                   
                   
                   guard response.data != nil else { // can check byte count instead
                       let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                       self.present(alert, animated: true, completion: nil)
                       
                       
                       UIApplication.shared.endIgnoringInteractionEvents()
                       return
                   }
                   
                   switch response.result
                   {
                       
                       
                   case .failure(let error):
                       //                print(error)
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let err = error as? URLError, err.code == .notConnectedToInternet {
                           // Your device does not have internet connection!
                           print("Your device does not have internet connection!")
                           self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                           print(err)
                       }
                       else if error._code == NSURLErrorTimedOut {
                           print("Request timeout!")
                           self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                       }else {
                           // other failures
                           self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                       }
                   case .success:
                       let result = response.result
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       if let dict1 = result.value as? Dictionary<String, AnyObject>{
                           if let invalidToken = dict1["detail"]{
                               if invalidToken as! String  == "Invalid token."
                               { // Create the alert controller
                                   self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                   appDelegate.window?.rootViewController = redViewController
                               }
                           }
                   
                   else if let json = response.result.value {
                       let jsonString = "\(json)"
                       print("res;;;;;", jsonString)
                       if dict1["status"] as! String == "success"
                       {
                        
                        if( self.is_following == true)
                        {
                            self.is_following = false
                        }
                        else
                        {
                             self.is_following = true
                        }
                            }
                       self.getUserData()
                       CustomLoader.instance.hideLoaderView()
                       UIApplication.shared.endIgnoringInteractionEvents()
                       
                       }
                   }}
                   }
               }
               else
               {
                   CustomLoader.instance.hideLoaderView()
                   UIApplication.shared.endIgnoringInteractionEvents()
                   let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                   let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                   let appDelegate = UIApplication.shared.delegate as! AppDelegate
                   appDelegate.window?.rootViewController = redViewController
               }
           }
    
    
        
        @objc func comment_data(sender: UIButton)
        {
        
            UserFeedPostViewController.user_feed_id = self.user_feedid[sender.tag]
            UserFeedPostViewController.selectedIndex = sender.tag
            performSegue(withIdentifier: "userFeedComments", sender: self)
            
        }
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if (segue.identifier == "userFeedComments") {
            guard let destination = segue.destination as? FeedCommentDataViewController else
            {
                print("return data")
               return
            }
                destination.user_feedid = self.user_feedid[UserFeedPostViewController.selectedIndex]
              }
            else if (segue.identifier == "feedDetails2") {
            guard let destination = segue.destination as? FeedDetailsViewController else
            {
                print("return data")
               return
            }
                if self.feedVideo.count > UserFeedPostViewController.selectedIndex
                {
                    destination.user_feedid = self.user_feedid[UserFeedPostViewController.selectedIndex]

                    if (self.feedVideo[UserFeedPostViewController.selectedIndex] as? String)! != ""
                    {
                        destination.feedVideo = (self.feedVideo[UserFeedPostViewController.selectedIndex] as? String)!
                    }
                    else if (self.feedImage[UserFeedPostViewController.selectedIndex] as? String)! != ""
                    {
                       destination.feedImage = (self.feedImage[UserFeedPostViewController.selectedIndex] as? String)!
                    }
                    else
                    {
                        destination.isFeedID_liked = self.isFeedID_liked[UserFeedPostViewController.selectedIndex]
                        destination.userImage = self.userImage[UserFeedPostViewController.selectedIndex]
                        destination.userName = self.userName[UserFeedPostViewController.selectedIndex]
//                        destination.uploadTime = self.uploadTime[UserFeedPostViewController.selectedIndex]

                        destination.QuestionText = "\(self.feed_text[UserFeedPostViewController.selectedIndex] as! String)"

                    }

                }
                else
                {}

            }
            
        }
        
      
        
        
        /// like button
        
        @objc func LikeButton(Feed_Index: UIButton)
        {
            let token = self.native.string(forKey: "Token")!
            var fav = 0
                if token != ""
                {
                    CustomLoader.instance.showLoaderView()
                if self.native.object(forKey: "userid") != nil
                {
                let userid = self.native.string(forKey: "userid")!
                
                let parameters: [String:Any] = ["user_feedid":native.string(forKey: "feed_userid")!]
                let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
                print("Successfully post")
            
                let b2burl = native.string(forKey: "b2burl")!
                var url = ""
                    if self.isFeedID_liked.count > Feed_Index.tag
                    {
                    if self.isFeedID_liked[Feed_Index.tag] == true
                {
                    
                    url = "\(b2burl)/users/dislike_user_feed/"
                }
                else
                {
                    url = "\(b2burl)/users/like_user_feed/"
                }
                    }
                print("follow product", url)
                Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                    
                    
                    guard response.data != nil else { // can check byte count instead
                        let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                        return
                    }
                    
                    switch response.result
                    {
                        
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                    case .success:
                        let result = response.result
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    
                    else if let json = response.result.value {
                        let jsonString = "\(json)"
                        print("res;;;;;", jsonString)
                        
                                if self.isFeedID_liked[Feed_Index.tag] == true
                        {
                            self.like[Feed_Index.tag] = self.like[Feed_Index.tag] - 1
                            self.isFeedID_liked[Feed_Index.tag] = false
                        }
                        else
                        {
                            self.like[Feed_Index.tag] = self.like[Feed_Index.tag] + 1
                            self.isFeedID_liked[Feed_Index.tag] = true
                                }
                                
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()

                        
                        }
                    }}
                    DispatchQueue.main.async {
                                   self.MyFeedPost.isHidden = false
                                   self.MyFeedPost.beginUpdates()
                                   self.MyFeedPost.reloadData()
                                   self.MyFeedPost.endUpdates()
                                   }
                    }
                    
                    }}
                else
                {
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = redViewController
                }
            
            
        }
        
        
        @objc func shareButton(sender: UIButton) {
            
            
            // image to share
            let text = "Feed Product"
            var productImage = "thumbnail"
            
            let image = UIImage(named: productImage)
            let myWebsite = NSURL(string:self.shareLink[sender.tag])
            let shareAll = [text , image , myWebsite!] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
        
    
}

