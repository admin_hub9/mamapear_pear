//
//  SettingTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 10/10/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //User
    @IBOutlet weak var UserIcon: UIImageView!
    @IBOutlet weak var userNotification: UILabel!
    // Client
    @IBOutlet weak var clientIcon: UIImageView!
    @IBOutlet weak var ClientName: UILabel!
    @IBOutlet weak var ClientAddress: UILabel!
    @IBOutlet weak var ClientMessage: UILabel!
    @IBOutlet weak var Time: UILabel!
    
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
