//
//  SelectShopAddressViewController.swift
//  Hub9
//
//  Created by Deepak on 12/11/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire


class SelectShopAddressViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    
    var native = UserDefaults.standard
    var indexNumber:NSInteger = -1
    var searchdata : NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.becomeFirstResponder()
        self.shopAddressTable.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var shopAddressTable: UITableView!
    
    
    
    @IBAction func cancelButton(_ sender: Any) {
        searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        let selecteddata = searchdata[indexPath.row] as! NSDictionary
        let data = selecteddata["country_state_city_name"] as! String
        print(data)
        
        shopdetails.shoplocation.setTitle("  " + data, for: .normal)
        let cityid = selecteddata["cityid"] as! Int
        shopdetails.selectedCitylkpd = cityid
        self.dismiss(animated: true, completion: nil)
    
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == tableView
        {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shopaddress", for: indexPath) as! ShopAddressTableViewCell
        let data = searchdata[indexPath.row] as! NSDictionary
        cell.shopaddress?.text = data["country_state_city_name"] as! String
        if indexNumber == indexPath.row{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return filteredData.count
        
        return searchdata.count
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchBar.text?.count)! > 2
        {
            searchdata = []
            shopAddressTable.reloadData()
            getShopAddress()
        }
    }


    
    
    func getShopAddress()
    {
        let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            let countryid = native.string(forKey: "countryid")!
            let b2burl = native.string(forKey: "b2burl")!
            var validateUrl =  "\(b2burl)/search/country_state_city/?countryid=\(countryid)&search_query=\(searchBar.text!.lowercased())"
            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                        print("/////", validateUrl)
            var a = URLRequest(url: NSURL(string:validateUrl)! as URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                //                                debugPrint(response)
                print("response shop: \(response)")
                let result = response.result
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    if let invalidToken = dict1["detail"]{
                        if invalidToken as! String  == "Invalid token."
                        { // Create the alert controller
                            self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController = redViewController
                        }
                    }
                    switch response.result
                    {
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    case .success:
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let dict1 = result.value as? Dictionary<String, AnyObject>{
                            if let invalidToken = dict1["detail"]{
                                if invalidToken as! String  == "Invalid token."
                                { // Create the alert controller
                                    self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.window?.rootViewController = redViewController
                                }
                            }
                    else if dict1["status"] as! String == "failure"
                    {
                        
                    }
                    else
                    {
                        self.searchdata = dict1["data"] as! NSArray
                    
                            }}
                }
                
                self.shopAddressTable.isHidden = false
                self.shopAddressTable.reloadData()
            }
        }
    }
    }
}
