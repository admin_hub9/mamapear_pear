//
//  DashboardProductCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/12/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DashboardProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var stateName: UILabel!
    @IBOutlet weak var productTag: UILabel!
    @IBOutlet weak var productSaveTag: UILabel!
    
    
}
