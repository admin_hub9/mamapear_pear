//
//  DashboardShopCollectionViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/12/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DashboardShopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var shopProductImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var Producttag: UILabel!
}
