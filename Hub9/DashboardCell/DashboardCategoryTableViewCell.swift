//
//  DashboardCategoryTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/12/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DashboardCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryCollection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
