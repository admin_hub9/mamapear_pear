//
//  RecommendedProductTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 8/30/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class RecommendedProductTableViewCell: UITableViewCell {

    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTag: UILabel!
    @IBOutlet weak var priceTag: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var priceSaveTag: UILabel!
    @IBOutlet weak var priceTagIcon: UIImageView!
    @IBOutlet weak var productName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
