//
//  DashboardCategorBannerTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 4/20/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DashboardCategorBannerTableViewCell: UITableViewCell {

    @IBOutlet weak var contentBackground: UIView!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryProduct: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
         self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
