//
//  DashboardShopTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 3/12/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DashboardShopTableViewCell: UITableViewCell {

    @IBOutlet weak var ShopContainer: UIView!
    @IBOutlet weak var shopLogo: UIImageView!
    @IBOutlet weak var shopTitle: UILabel!
    @IBOutlet weak var shopAddress: UILabel!
    @IBOutlet weak var shopCollection: UICollectionView!
    @IBOutlet weak var enterShop: UILabel!
    @IBOutlet weak var shopPage: UIButton!
    
    // unfollow shop
    @IBOutlet weak var unfollowShop: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
