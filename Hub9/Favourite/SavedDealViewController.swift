//
//  SavedDealViewController.swift
//  Hub9
//
//  Created by Deepak on 05/06/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit
import Alamofire


class SavedDealViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout{

    var native = UserDefaults.standard
    let screenSize = UIScreen.main.bounds
    var screenWidth = 0
    var screenHeight = 0
    var limit = 20
    var offset = 0
    
    
    var ProductName = [String]()
    var ProductImg = [AnyObject]()
    var marketPlaceName = [String]()
    var priceTag = [String]()
    var featuredLabel = [String]()
    var user_feedid = [Int]()
    
   
    @IBOutlet weak var dealTable: UITableView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.clearDealData()
        getRecommended_product(limit: limit, offset: offset)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    func clearDealData()
    {
        ProductName.removeAll()
        ProductImg.removeAll()
        marketPlaceName.removeAll()
        featuredLabel.removeAll()
        priceTag.removeAll()
        user_feedid.removeAll()
        self.dealTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        dealTable.isHidden = true
        dealTable.tableFooterView = UIView()
        screenWidth = Int(screenSize.width)
        screenHeight = Int(screenSize.height)
        
        // Do any additional setup after loading the view.
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleCells: [IndexPath] = dealTable.indexPathsForVisibleRows!
            let lastIndexPath = IndexPath(item: (ProductImg.count - 1), section: 0)
            if visibleCells.contains(lastIndexPath) {
                //This means you reached at last of your datasource. and here you can do load more process from server
                print("last index ", lastIndexPath)
                offset = offset + 10
                getRecommended_product(limit: limit, offset: offset)
                
            }
        }
        
        func collectionView(_ collectionView: UICollectionView,
                            layout collectionViewLayout: UICollectionViewLayout,
                            sizeForItemAt indexPath: IndexPath) -> CGSize {
            var width = 0.0
            var height = 0.0
            
            width = Double(CGFloat(screenWidth))
            height = Double(CGFloat(Double(screenHeight-30) / 6))
            
            return CGSize(width: width, height: height )
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.ProductName.count > 0
                {
                    
                    
                    self.dealTable.showEmptyListMessage("")
                    return self.ProductName.count
                }
                else {
        //            self.productCollection.showEmptyListMessage("No Product found!")
                    return 0
                }
    }
        
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dealCell", for: indexPath) as! DealTableViewCell
                        cell.clipsToBounds = true
                        cell.productImage.image = nil
                        let textcolor = UIColor(red: 247/255, green: 82/255, blue: 85/255, alpha: 1)
                        let lightGrayColor = UIColor(red: 135/255, green: 135/255, blue: 135/255, alpha: 1)
                        let lightBlackColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
                        
                        if let image1 = self.ProductImg[indexPath.row] as? [AnyObject]
                        {
                            if image1.count > 0
                            {
                                var image = image1[image1.count - 1] as! String
                                image = image.replacingOccurrences(of: " ", with: "%20")
                                let url = NSURL(string: image)
                                if url != nil
                                {
                                    DispatchQueue.main.async {
                                        cell.productImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "thumbnail"))
                                    }
                                }
                            }
                        }
        cell.featureName.layer.cornerRadius = cell.featureName.frame.height/2
        cell.featureName.clipsToBounds=true
        
        if self.featuredLabel[indexPath.row] != "Default"
            {
                cell.featureName.text = " \(self.featuredLabel[indexPath.row]) "
                cell.featureName.isHidden = false
            }
            else
            {
                cell.featureName.isHidden = true
            }
        
        cell.productPrice.attributedText = ( "\(self.priceTag[indexPath.row] as! String)".fontName("HelveticaNeue-Regular").color(textcolor)).attributedText
                        cell.productName.text = (self.ProductName[indexPath.row] as? String)?.capitalized
        //                let price = self.ProductPrice[indexPath.row ]
                        
        //            cell.like_comment.text = "\(like)  \(comment)"
                    if self.marketPlaceName.count>indexPath.row
                    {
                        cell.marketPlaceName.text = self.marketPlaceName[indexPath.row]
                        
                    }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 140
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
       
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.favourite(index: indexPath.row)
        }
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    
    func favourite(index: Int) {
    //        self.blurView.alpha = 1
    //        self.activity.startAnimating()
            
            let token = self.native.string(forKey: "Token")!
        if token != "" && self.user_feedid.count > index
            {
                CustomLoader.instance.showLoaderView()
            
            var id = user_feedid[index]
            //        print("product id:", id)
            let parameters: [String:Any] = ["user_feedid":id]
            let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            print("Successfully post")
            let b2burl = native.string(forKey: "b2burl")!
            var url = "\(b2burl)/users/dislike_user_feed/"
            
            print("unfollow product", url)
            Alamofire.request(url ,method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                
                
                guard response.data != nil else { // can check byte count instead
                    let alert = UIAlertController(title: "Uh Oh", message: "There seem to be server issues! Please try again later", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    return
                }
                
                switch response.result
                {
                    
                    
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    let result = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                
                else if let json = response.result.value {
                    let jsonString = "\(json)"
                    print("res;;;;;", jsonString)
                    self.user_feedid.remove(at: index)
                    self.ProductName.remove(at: index)
                    self.marketPlaceName.remove(at: index)
                            self.featuredLabel.remove(at: index)
                    self.priceTag.remove(at: index)
                    self.ProductImg.remove(at: index)
                    self.dealTable.reloadData()
                    
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    }
                }}
                }
            }
            else
            {
                CustomLoader.instance.hideLoaderView()
                UIApplication.shared.endIgnoringInteractionEvents()
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = redViewController
            }
        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if self.user_feedid.count > indexPath.row
            {
            native.set(self.user_feedid[indexPath.row], forKey: "FavProID")
            native.set("product", forKey: "comefrom")
            native.synchronize()
            //        currntProID = self.ProductID[indexPath.row] as! Int
            
            let editPage = (self.storyboard?.instantiateViewController(withIdentifier: "dealDetails"))! as UIViewController
            
            self.navigationController?.pushViewController(editPage, animated: true)
            }
        }

    
    func getRecommended_product(limit: Int, offset: Int)
        {
            
    //        CustomLoader.instance.showLoaderView()
            let token = self.native.string(forKey: "Token")!
            
                let b2burl = native.string(forKey: "b2burl")!
            
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/get_saved_deals/?limit=\(limit)&offset=\(offset)") as! URL)
            if token != ""
            {
                a.allHTTPHeaderFields = ["Content-Type": "application/json", "Authorization": "Token \(token)"]
            }
            else{
                
                a.allHTTPHeaderFields = ["Content-Type": "application/json"]
            }
            print(a, token)
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                var url =
                print("url recommended", a, token)
                
                
                    switch response.result {
                    case .success:
                if let result1 = response.result.value
                {
                    let result = response.result
                   
                   
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
            else if dict1["status"] as! String == "success"
            {
               
                    print("recommended product",dict1 ,dict1["deal_data"])
                if let innerdict1 = dict1["deal_data"] as? [AnyObject]
                {
                            
                 for i in 0..<innerdict1.count {
                
                 print("recommended product1", innerdict1)
               if innerdict1.count > 0
               {
                if innerdict1[i]["title"] is NSNull
                {
                    self.ProductName.append("")
                }
                else if let name = innerdict1[i]["title"]! as? String
               {
                  
                       self.ProductName.append(name)
                    }
                if innerdict1[i]["price_reference_text"] is NSNull
                 {
                     self.priceTag.append("")
                 }
                 else if let name = innerdict1[i]["price_reference_text"]! as? String
                {
                   
                        self.priceTag.append(name)
                     }
                
                
                if let marketplace = innerdict1[i]["marketplace"]
                {
                if innerdict1[i]["marketplace"] is NSNull
                    {
                        self.marketPlaceName.append("")
                }
                    else
                    {
                        self.marketPlaceName.append((innerdict1[i]["marketplace"] as? String)!)
                }
                
                }
                    
                    
                else
                {
                    self.marketPlaceName.append("")
                }

                if innerdict1[i]["label"] is NSNull
                    {
                        self.featuredLabel.append("")
                }
                    else
                    {
                        self.featuredLabel.append((innerdict1[i]["label"] as? String)!)
                }
                
                if innerdict1[i]["user_feedid"] is NSNull
                
                {
                    self.user_feedid.append(0)
                }
                else
                {
                    self.user_feedid.append((innerdict1[i]["user_feedid"] as? Int)!)
                }
                
                
                
                
                            let imagedir = (innerdict1[i]["media_url"] as? AnyObject)!
                            self.ProductImg.append((imagedir["image"] as? AnyObject)!)
                            
                             } }  }
                            }
                
                        }
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                    }
                
                        
                    case .failure(let error):
                        //                print(error)
                        CustomLoader.instance.hideLoaderView()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        if let err = error as? URLError, err.code == .notConnectedToInternet {
                            // Your device does not have internet connection!
                            print("Your device does not have internet connection!")
                            self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                            print(err)
                        }
                        else if error._code == NSURLErrorTimedOut {
                            print("Request timeout!")
                            self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                        }else {
                            // other failures
                            self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                        }
                        
                    }
                self.dealTable.isHidden = false
                self.dealTable.reloadData()
                
            }
            
        }
    
}
