//
//  DealTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 04/09/20.
//  Copyright © 2020 Deepak. All rights reserved.
//

import UIKit

class DealTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var uploadTime: UILabel!
    @IBOutlet weak var featureName: UILabel!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var productPrice: UILabel!
    
    @IBOutlet weak var marketPlaceName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
