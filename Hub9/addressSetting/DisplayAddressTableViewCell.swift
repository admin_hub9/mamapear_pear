//
//  DisplayAddressTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 4/4/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class DisplayAddressTableViewCell: UITableViewCell {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var cityandstate: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var addressType: UILabel!
    @IBOutlet weak var useAddressType: UILabel!
    @IBOutlet weak var editaddress: UIButton!
    @IBOutlet weak var DeleteAddress: UIButton!
    @IBOutlet weak var mobileNumber: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
