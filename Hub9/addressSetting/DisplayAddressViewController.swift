//
//  DisplayAddressViewController.swift
//  Hub9
//
//  Created by Deepak on 4/4/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit
import Alamofire

var DisplayAddress = DisplayAddressViewController()
class DisplayAddressViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var native = UserDefaults.standard
    var selectAddress = [AnyObject]()
    var addressSender = "getAddress"
    var selectedindex = 0
    
    
    @IBOutlet weak var addressTable: UITableView!
    @IBOutlet weak var addAddressButton: UIBarButtonItem!
   
    
    @IBAction func addAddress(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "addnewaddress"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)

        
    }
    
    @objc func editAddress(sender: UIButton) {
        
        selectedindex = sender.tag
        addressSender = "edit"
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "addnewaddress"))! as UIViewController
        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
        self.navigationController?.pushViewController(rootPage, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
         self.navigationController?.setNavigationBarHidden(false, animated: true)
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
             statusBar.backgroundColor = UIColor.clear
            UIApplication.shared.statusBarStyle = .darkContent
             UIApplication.shared.keyWindow?.addSubview(statusBar)
        } else {
            UIApplication.shared.statusBarStyle = .default
            UIApplication.shared.statusBarUIView!.backgroundColor = UIColor.clear
        }
        addressSender = "getAddress"
        getAddressData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.navigationController?.setNavigationBarHidden(false, animated: true)
        DisplayAddress = self
        
        CustomLoader.instance.showLoaderView()
        addressTable.tableFooterView=UIView()
        addressTable.isHidden = true
        getAddressData()
        // Do any additional setup after loading the view.
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//
//
//
//        }
//    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectAddress.count <= 0
        {
            self.addressTable.showEmptyListMessage("No Address Found!")
            return 0
        }
        else
        {
            self.addressTable.showEmptyListMessage("")
        return selectAddress.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "displayaddress", for: indexPath) as! DisplayAddressTableViewCell
        cell.username.text = self.selectAddress[indexPath.row]["consignee_name"] as? String
        var address = ""
        if  self.selectAddress[indexPath.row]["address1"] is NSNull
        {}
        else
        {
            address = self.selectAddress[indexPath.row]["address1"] as! String
        }
        if  self.selectAddress[indexPath.row]["address2"] is NSNull
        {}
        else
        {
            if address != ""
            {
            address = "\(address), \(self.selectAddress[indexPath.row]["address2"] as! String)"
            }
            else
            {
                address = self.selectAddress[indexPath.row]["address2"] as! String
            }
        }
        cell.address.text = address
        if self.selectAddress[indexPath.row]["contact_number"] is NSNull{}
        else
        {
        cell.mobileNumber.text = "Mobile: \((self.selectAddress[indexPath.row]["contact_number"] as? NSString)!)"
        }
        //        print(indexPath.row, "saved address")
        var cityname = ""
        var statename = ""
        var regionName = ""
        var pincode = ""
//        cell.addressType
        cell.editaddress.tag = indexPath.row
        cell.editaddress.addTarget(self, action: #selector(editAddress(sender:)), for: .touchUpInside)
        cell.DeleteAddress.tag = indexPath.row
        cell.DeleteAddress.addTarget(self, action: #selector(deleteAlert(sender:)), for: .touchUpInside)
        cell.addressType.layer.cornerRadius = cell.addressType.frame.height/2
        cell.addressType.clipsToBounds = true
        if self.selectAddress[indexPath.row]["address_name"] is NSNull
        {}
        else
        {
        cell.addressType.text = " \(self.selectAddress[indexPath.row]["address_name"] as! String)   "
        }
        if let data = self.selectAddress[indexPath.row]["city"] as? String
        {
            cityname = data
        }
        if let data = self.selectAddress[indexPath.row]["state"] as? String
        {
            statename = data
        }
        if let data = self.selectAddress[indexPath.row]["region"] as? String
        {
            regionName = data
        }
        if let data = self.selectAddress[indexPath.row]["pincode"] as? String
        {
            pincode = data
        }
        
        cell.cityandstate.text = "\(cityname), \(statename), \(pincode)"
        if let data = self.selectAddress[indexPath.row]["country_name"] as? String
        {
            cell.country.text = data
        }
        
        if (selectAddress[indexPath.row]["is_billing_default"] as? Int)! == 1 && (selectAddress[indexPath.row]["is_shipping_default"] as? Int)! == 1
        {
            cell.useAddressType.text = "Use as Billing and Shipping"
        }
        else
        {
            
//            if (selectAddress[indexPath.row]["is_billing_default"] as? Int)! == 1
//        {
//            cell.useAddressType.text = "  Default Billing address "
//        }
             if (selectAddress[indexPath.row]["is_shipping_default"] as? Int)! == 1
            {
               cell.useAddressType.text = "Default"
            }
            else
            {
             cell.useAddressType.text = ""
             cell.useAddressType.textColor = UIColor.lightGray
            }
        }
        
//        if selectshippingindex == indexPath.row
//        {
//            cell.mark.image = UIImage(named: "checkmark")
//            cell.edit.isHidden = false
//            cell.edit.tag = indexPath.row
//
//        }
//        else
//        {
//            cell.mark.image = UIImage(named: "Uncheckmark")
//            cell.edit.isHidden = true
//        }
        return cell
    }
    
    
    @objc func deleteAlert(sender: UIButton)
    {
        let alert = UIAlertController(title: "", message: "Are you sure that you want to delete this address?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "Yes",
                                      style: UIAlertActionStyle.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        self.delete(sender: sender)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    ////delete address
    @objc func delete(sender: UIButton)
    {
        CustomLoader.instance.showLoaderView()
        var Token = self.native.string(forKey: "Token")!
        print(Token)
        if Token != nil
        {
            let header = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            print("Successfully post")
            let parameters: [String:Any] = ["user_addressid": selectAddress[sender.tag]["user_addressid"]]
            print( parameters)
            let b2burl = native.string(forKey: "b2burl")!
            Alamofire.request("\(b2burl)/users/delete_address/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        
                    
                    else if (dict1["status"] as? String )! != "failure"
                    {
                        
                        CustomLoader.instance.hideLoaderView()
                        
                            self.selectAddress.remove(at: sender.tag)
                            self.addressTable.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .fade)
                            self.view.makeToast(dict1["data"] as! String, duration: 2.0, position: .center)
                    }
                    else
                    {
                        
                        CustomLoader.instance.hideLoaderView()
                        self.view.makeToast(dict1["data"] as! String, duration: 2.0, position: .center)
                    }
                }
                
                
                
                CustomLoader.instance.hideLoaderView()
                self.addressTable.reloadData()
                }}
            
        }}
    
    
    ////get all address
    func getAddressData()
    { var Token = self.native.string(forKey: "Token")!
        print(Token)
        if Token != nil
        {
            let b2burl = native.string(forKey: "b2burl")!
            
            var a = URLRequest(url: NSURL(string: "\(b2burl)/users/saved_address/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                print(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        print("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        print(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                    
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                    else if (dict1["status"] as? String )! != "failure"
                    {
               
                    let innerdict = dict1["data"] as? [AnyObject]
                    if innerdict!.count > 0
                    {
                        self.selectAddress = innerdict!
                        
                    }
                }
                    else
                    {
                        
                        CustomLoader.instance.hideLoaderView()
                        self.view.makeToast(dict1["status"] as! String, duration: 2.0, position: .center)
                    }
                
                
                CustomLoader.instance.hideLoaderView()
                self.addressTable.isHidden = false
                self.addressTable.reloadData()
                    }}
            
        }}
    }}
