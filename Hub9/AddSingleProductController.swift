//
//  AddSingleProductController.swift
//  Hub9
//  Created by Deepak on 6/6/18.
//  Copyright © 2018 Deepak. All rights reserved.



import Gallery
import Lightbox
import AVFoundation
import AVKit
import Photos
import SVProgressHUD
import UIKit
import AWSCore
import AWSS3
import Alamofire
import iOSDropDown
import RSSelectionMenu



class AddSingleProductController: UIViewController, UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource, UITableViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, LightboxControllerDismissalDelegate, GalleryControllerDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedunits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToReturn = UITableViewCell() // Dummy value
        if tableView == singleProductTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "addproductTable", for: indexPath) as! AddProductTableCell
            cell.units.text = selectedunits[indexPath.row]
            cell.cost.text = String(selctedprice[indexPath.row])
//            cell.cellView.layer.cornerRadius = cell.cellView.layer.frame.height/2
//            cell.cellView.clipsToBounds = true
            if indexPath.row == Pvariant.count - 1
            {
                cell.delete.isHidden = false
                cell.delete.tag = indexPath.row
                cell.delete.addTarget(self, action: #selector(delete_Disscount(sender:)), for: .touchUpInside)
            }
            else{
                cell.delete.isHidden = true
            }
            cellToReturn = cell
        }
        return cellToReturn
    }
    
    ////for images and videos
    var gallery: GalleryController!
    let editor: VideoEditing = VideoEditor()
    ///fetch dynamic price disscount
    var imagePicker: UIImagePickerController!
    var selectedImage = [UIImage]()
    let native = UserDefaults.standard
    var pickerData = [[String]]()
    var time1 = 0
    var i = 0
    var minutes = 1
    var recievedString: String = ""
    var seconds = Array(1...100)
    var selectedunits = [String]()
    var selctedprice = [Int]()
    var numberOfVariant = 0
    var s3count = 0
    var url = ""
    var ProductID = [AnyObject]()
    var currntProID = -1
    var ProductImg = [AnyObject]()
    var variantData = [AnyObject]()
    var favouriteIndex = 0
    var Img = [AnyObject]()
    ///editing data
    var name = ""
    var ProductImage = ""
    var sku = ""
    var hsncode = ""
    var supplyAbility = ""
    var insertVariant = false
    var pickerType = "unit"
    var variantype = ""
    let simpleDataArray = ["Pajama", "Nighty", "Shirt", "T-Shirt", "Jeans", "Bra"]
    var simpleSelectedArray = [String]()
    var DiscountNumber = Array(1...500)
    var Pvariant = [["index": Int.self, "low": String(), "high": String(), "price" : String()]]
    var companyID = ""
    var ParentProductID = ""
    var videourl = [URL]()
    
    /// select category and brand
    var categoryname = ""
    var subCategoryId = -1
    var brandname = ""
    var brandid = -1
    
    
    
    

    @IBOutlet var productCollection: UICollectionView!
    @IBOutlet var sPrice: UITextField!
    @IBOutlet var cancel: UIButton!
    @IBOutlet var post: UIButton!
    @IBOutlet var sName: UITextField!
    @IBOutlet var sSku: UITextField!
    @IBOutlet var sHsnCode: UITextField!
    @IBOutlet var sSupply: UITextField!
    @IBOutlet var sLenght: UITextField!
    @IBOutlet var sWidth: UITextField!
    @IBOutlet var singleProductTable: UITableView!
    @IBOutlet var sWeight: UITextField!
    @IBOutlet var sHeight: UITextField!
    @IBOutlet var sDescription: UITextField!
    @IBOutlet var price: UITextField!
    @IBOutlet var stock: UITextField!
    ///picker
    @IBOutlet var unitPicker: UIPickerView!
    @IBOutlet var pikcerDone: UIButton!
    @IBOutlet var pickerCancel: UIButton!
    @IBOutlet var pickerViewContainer: UIView!
    @IBOutlet var blurView: UIVisualEffectView!
    @IBOutlet var addVariantButtonView: UIView!
    
    @IBOutlet var selectCategoryLabel: UILabel!
    @IBOutlet var selectBrandLabel: UILabel!
    @IBOutlet var moreDetails: UIView!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var collectdata_view: UIView!
    @IBOutlet var collection_Container: UIView!
    
    @IBOutlet var unitClicked: UITextField!
    @IBOutlet var imagesCountLabel: UILabel!
    @IBOutlet var moq: UITextField!
    @IBOutlet var mrp: UITextField!
    @IBOutlet var activityindicator: UIActivityIndicatorView!
    
    
    
    
    @IBAction func backButton(_ sender: Any) {
        native.set("", forKey: "categoryname")
        native.set("", forKey: "brand")
        native.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    
    // to display number of row for each colomn in picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //        print(pickerView)
        //        return pickerData[component].count
        var row = pickerView.selectedRow(inComponent: 0)
        print("this is the pickerView\(row)")
        if pickerType == "discount"
        {
            if component == 0
            {
                return DiscountNumber.count
            }
                
            else {
                return 1
            }
        }
        else{
            
            if component == 0 {
                return 1
            }
            if component == 1
            {
                return 1
            }
                
            else {
                return seconds.count
            }
            
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerType == "discount"
        {
            return 2
        }
        else
        {
            return 3
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerType == "discount"
        {
            print("value", DiscountNumber[pickerView.selectedRow(inComponent: 0)])
            sPrice.text = String("\(DiscountNumber[pickerView.selectedRow(inComponent: 0)])")
        }
        else{
            if pickerType == "unit"
            {
                time1 = seconds[pickerView.selectedRow(inComponent: 2)]
                
                print("value", minutes, seconds[pickerView.selectedRow(inComponent: 2)])
                unitClicked.text = String("\(minutes) - \(seconds[pickerView.selectedRow(inComponent: 2)])")
            }
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //        return pickerData[component][row]
        if pickerType == "discount"
        {
            if component == 0
            {
                return String(DiscountNumber[row])
            }
            else
            {
                return "%"
            }
        }
        else
        {
            if component == 0 {
                return String(minutes)
                
                
            }
            else if component == 1
            {
                return "to"
            }
            else {
                
                return String(seconds[row])
            }
        }
    }
    
    @IBAction func selectProduct_photos(_ sender: Any) {
        buttonTouched()
        
    }
    
    
    
    // to display product images and video
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if selectedImage.count == 0
        {
            imagesCountLabel.text = ""
            UIView.transition(with: collectdata_view, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                    self.blurView.alpha = 0.6
                    self.collectdata_view.transform = CGAffineTransform(translationX: 0, y: -90)
                    self.collection_Container.isHidden = false
            }, completion: nil)
        }
        else
        {
            imagesCountLabel.text = String(selectedImage.count)
            UIView.transition(with: collectdata_view, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                    self.blurView.alpha = 0.6
                    self.collectdata_view.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.collection_Container.isHidden = false
            }, completion: nil)
        }
        if selectedImage.count > 0
        {
            print(selectedImage.count, selectedImage)
            return selectedImage.count + 1
        }
        else
        {
        return 0
        }    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var CellCount = UICollectionViewCell()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addproduct", for: indexPath) as! AddProductCell
        if indexPath.row == 0
        {
            cell.cancel.isHidden = true
            cell.images.image = nil
            cell.images.layer.cornerRadius = 8
            cell.images.clipsToBounds = true
            cell.camera.isHidden = false
            cell.photos.isHidden = false
            cell.clickPhotos.isHidden = false
            CellCount = cell
        }
            else
        {
            cell.cancel.layer.cornerRadius = cell.cancel.layer.frame.size.height/2
            cell.cancel.clipsToBounds = true
            cell.images.layer.cornerRadius = 8
            cell.images.image = selectedImage[indexPath.row - 1]
            cell.images.clipsToBounds = true
            cell.camera.isHidden = true
            cell.photos.isHidden = true
            cell.clickPhotos.isHidden = true
            cell.cancel.isHidden = false
            cell.cancel.tag = indexPath.row
            cell.cancel.addTarget(self, action: #selector(RemoveImages(sender:)), for: .touchUpInside)
            //            cell.clickPhotos.isHidden = true
            let button = UIButton(frame: CGRect())
            button.layer.cornerRadius = button.layer.bounds.size.width / 2
            button.layer.masksToBounds = true
            cell.cancel.layer.masksToBounds = true
            CellCount = cell
        }
        
        
        return CellCount
}
////delete Images
    @objc func RemoveImages(sender: UIButton)
    {
            if sender.tag - 1 == 0
            {
                videourl.removeAll()
                selectedImage.remove(at: sender.tag - 1 )
            }
        else
            {
            selectedImage.remove(at: sender.tag - 1 )
        }
            productCollection.reloadData()
            i = i - 1
        if selectedImage.count > 0
        {
            imagesCountLabel.text = String(selectedImage.count)
        }
        if selectedImage.count == 0
        {
        UIView.transition(with: collectdata_view, duration: 0.4, options: .beginFromCurrentState, animations:
            {
//                self.blurView.alpha = 0.6
                self.collectdata_view.transform = CGAffineTransform(translationX: 0, y: -90)
                self.collection_Container.isHidden = false
        }, completion: nil)
        }
    }
    
    func uploadVideo(fileUrl : URL){
        
        let accessKey = "AKIAJYCW54FQC6MMHIDQ"
        let secretKey = "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"
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.EUWest1, credentialsProvider:credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        let uploadingFileURL = fileUrl
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        companyID = native.string(forKey: "shopid")!
        print("company id is", companyID)
        uploadRequest?.bucket = "b2b-product-images/"+companyID+"/"+String(self.ParentProductID) + "/video"
        uploadRequest?.key = sSku.text! + String(i) + ".mp4"
        uploadRequest?.body = uploadingFileURL
        uploadRequest?.contentType = "video/mp4"
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith(executor: AWSExecutor.mainThread())
        { (task) -> Any? in
            if task.error == nil
            {
                print("uploaded to :")
                self.blurView.alpha = 0
                self.activityindicator.stopAnimating()
                self.native.set("", forKey: "categoryname")
                self.native.set("", forKey: "brand")
                self.native.synchronize()
                self.dismiss(animated: true, completion: nil)
                return nil
            }
            else
            {
                print("Error: \(String(describing: task.error))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent((uploadRequest?.bucket)!).appendingPathComponent(uploadRequest!.key!)
                if let absoluteString = publicURL?.absoluteString {
                    print("Uploaded to:\(absoluteString)")
                    self.s3count = (self.s3count) + 1
                    self.blurView.alpha = 0
                    self.activityindicator.stopAnimating()
                    self.dismiss(animated: true, completion: nil)
                    
                }
            }
            return nil
        }
    }
    
    func uploadPhotos()
        
    { for var i in 0..<selectedImage.count
    {
        let accessKey = "AKIAJYCW54FQC6MMHIDQ"
        let secretKey = "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"
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        companyID = native.string(forKey: "shopid")!
        print(companyID)
        let S3BucketName = "b2b-product-images/"+companyID+"/"+String(self.ParentProductID) + "/image"
        let remoteName = sSku.text! + String(i) + ".jpeg"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
//                let image = UIImage(named: "car")
        
        if let data = UIImageJPEGRepresentation(selectedImage[0], 0.2) {
            print(data.count)
        
        do {
            print("-------", fileURL)
            try data.write(to: fileURL)
        }
        catch {}
        }
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/JPEG"
        uploadRequest.acl = .bucketOwnerRead
        
        let transferManager = AWSS3TransferManager.default()
        
        
        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
            DispatchQueue.main.async {
                
            }
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    print("Uploaded to:\(absoluteString)")
                    self?.s3count = (self?.s3count)! + 1
                    if(self?.s3count == self?.selectedImage.count)
                    {
                        print("uplaoding successfully")
                        if self!.videourl.count == 0
                        {
                            self!.native.set("", forKey: "categoryname")
                            self!.native.set("", forKey: "brand")
                            self!.native.synchronize()
                            self!.blurView.alpha = 0
                            self!.activityindicator.stopAnimating()
                            self!.dismiss(animated: true, completion: nil)
                        }
                        
                    }
                }
            }
            
            return nil
        }
        }
    }
    
   //  create product
    @IBAction func postSingleProduct(_ sender: Any) {
        if (sName.text?.count != 0 && selectBrandLabel.text?.count != 0 && sSku.text?.count != 0  && sDescription.text?.count != 0 && selectedImage.count > 0)
        {
            
            if Parentvalidated() == true
            {
            var imageurl = [String]()
            for var i in 0..<self.selectedImage.count
            {
                imageurl.append(sSku.text! + String(i) + ".jpeg")
            }
            var videourlis = [String]()
            if videourl.count == 1
            {
                videourlis.append(sSku.text! + String(i) + ".mp4")
            }
            
            var innerdata = [String: Any]()
                
                innerdata["is_single_product"] = true
                innerdata["title"] = sName.text!
                innerdata["sku"] = sSku.text!
                innerdata["selling_price"] = price.text!
            
                innerdata["stock"] = stock.text!
            if mrp.text!.count != 0
            {
                innerdata["retail_price"] = mrp.text!
            }else
            {
                innerdata["retail_price"] = price.text!
            }
            if sHsnCode.text?.count != 0
            {
                innerdata["hsn_code"] = sHsnCode.text!
            }
            if moq.text?.count != 0 && Int(moq.text!)! >= 1
            {
                innerdata["min_order_qty"] = moq.text!
            
            
                innerdata["brand"] = ["brandid": brandid, "brand_name": brandname]
                innerdata["image_url"] = imageurl
            if (sSupply.text?.count)! > 0
            {
                innerdata["supply_ability_daily"] = sSupply.text!
            }
                innerdata["description"] = sDescription.text!
                innerdata["dimension"] = ["weight": sWeight.text!, "height": sHeight.text!, "length": sLenght.text!, "breadth": sWidth.text!]
                innerdata["dynamic_discount_range"] = Pvariant
                innerdata["sub_categoryid"] = subCategoryId
                innerdata["parent_image_count"] = imageurl.count
                innerdata["mp4"] = videourlis
                innerdata["parent_video_count"] = videourlis.count

            
            
        let parameters: [String: Any] = ["parent_product_data": innerdata]
            
        let token = self.native.string(forKey: "Token")!
        let header = ["Content-Type": "application/json","Authorization": "Token \(token)"]
        print("Successfully post",parameters)
            self.blurView.alpha = 1
            self.activityindicator.startAnimating()
                let b2burl = native.string(forKey: "b2burl")!
            Alamofire.request("\(b2burl)/product/create_listing/",method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            print("response: \(response)")
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    print(response.result.value)
                    let res = response.result
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = res.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if dict1["status"] as! String == "failure"
                        {
                            self.blurView.alpha = 0
                            let alert = UIAlertController(title: "Alert", message: dict1["status"] as! String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }else
                        {
                            let innerdict = dict1["data"] as! NSDictionary
                                if dict1["status"] as! String == "success"
                                {
                                    self.ParentProductID = ""
                                    self.ParentProductID = ""
//                                    self.ParentProductID = String(innerdict["parent_productid"] as! Int)
                                    
                                let alertController = UIAlertController(title: "Success", message: "Product Succfully Updated", preferredStyle: .alert)
                                let data = innerdict
                                var productDeatil = innerdict["parent_product_data"] as! NSDictionary
                                print("gjhdbc", productDeatil)
                                print("vh", productDeatil["image_url"])
                                print("gvhdsv", productDeatil["parent_productid"])
                                self.ParentProductID = String(productDeatil["productid"] as! Int)
                                print(" json", data)
                                print(self.ParentProductID)
                            }
                        }
                        self.uploadPhotos()
                        if self.videourl.count == 1
                        {
                            self.uploadVideo(fileUrl: self.videourl[0])
                        }
                        
                    }
                }
                break
            case .failure(_):
                print("fail")
                self.blurView.alpha = 0
                self.activityindicator.stopAnimating()
                let alert = UIAlertController(title: "Alert", message: "Failed to create product", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
                } }else
                {
                    let alert = UIAlertController(title: "Alert", message: "MOQ will always grater than 0", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: " Please add Brand, Category, Description and one image First", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedImage.remove(at: indexPath.row - 1)
        productCollection.reloadData()
    }
   
        
    
    @objc func tappedUnit(_ sender: UITapGestureRecognizer) {
        print("detected tap!")
        
        pickerType = "unit"
        unitPicker.reloadAllComponents()
        UIView.transition(with: pickerViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                self.blurView.alpha = 0.6
                self.pickerViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                self.pickerViewContainer.isHidden = false
        }, completion: nil)
    }
    
    @objc func tappedTextView(_ sender: UITapGestureRecognizer) {
        print("detected tap!")
        pickerType = "discount"
        unitPicker.reloadAllComponents()
        UIView.transition(with: pickerViewContainer, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                self.blurView.alpha = 0.6
                self.pickerViewContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                self.pickerViewContainer.isHidden = false
        }, completion: nil)
    }
    
    //############################## gallery controller #########################
    
    @IBAction func selectGalleryButton(_ sender: Any) {
        gallery = GalleryController()
        gallery.delegate = self
        
        present(gallery, animated: true, completion: nil)
    }
    func buttonTouched() {
        gallery = GalleryController()
        gallery.delegate = self
        
        present(gallery, animated: true, completion: nil)
    }
    
    // MARK: - LightboxControllerDismissalDelegate
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    
    
    // MARK: - GalleryControllerDelegate
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func requestImage(for asset: PHAsset,
                      targetSize: CGSize,
                      contentMode: PHImageContentMode,
                      completionHandler: @escaping (UIImage?) -> ()) {
        let imageManager = PHImageManager()
        imageManager.requestImage(for: asset,
                                  targetSize: targetSize,
                                  contentMode: contentMode,
                                  options: nil) { (image, _) in
                                    completionHandler(image)
        }
    }
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        
        self.dismiss(animated: true, completion: nil)
        gallery = nil
        
        
        editor.edit(video: video) { (editedVideo: Video?, tempPath: URL?) in
            print("video url is", video.asset.mediaSubtypes)
            let asset = video.asset// your existing PHAsset
            let targetSize = CGSize(width: 100, height: 100)
            let contentModel = PHImageContentMode.aspectFit
            self.requestImage(for: asset, targetSize: targetSize, contentMode: contentModel, completionHandler: { image in
                // Do something with your image if it exists
//                print("images", image)
                if self.videourl.count < 1
                {
                self.videourl.append(tempPath!)
                    if self.selectedImage.count == 0
                    {
                        self.selectedImage.append(image!)
                    }
                    else
                    {
                        let firstimage = self.selectedImage[0]
                        self.selectedImage.remove(at: 0)
                        self.selectedImage.insert(image!, at: 0)
                        self.selectedImage.append(firstimage)
                    }
                
                DispatchQueue.main.async {
                    self.productCollection.reloadData()
                }
                }
                else
                {
                    let alert = UIAlertController(title: "Alert", message: "Can't Upload Multiple Videos", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                
            })
            
//            DispatchQueue.main.async {
//                if let tempPath = tempPath {
//                    let controller = AVPlayerViewController()
//                    controller.player = AVPlayer(url: tempPath)
//
//                    self.present(controller, animated: true, completion: nil)
//                }
//            }
        }
        
    }
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        self.dismiss(animated: true, completion: nil)
        print(images)
        
        //    let data = UIImageJPEGRepresentation(image!, 0.9)
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            if self!.selectedImage.count + resolvedImages.count < 8
            {
            for var i in 0..<resolvedImages.count
            {
                let image = resolvedImages[i]
                self!.selectedImage.append(image!)
            }
            self!.productCollection.reloadData()
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: "Size must be less than 8", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self!.present(alert, animated: true, completion: nil)
            }
        })
        gallery = nil
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        LightboxConfig.DeleteButton.enabled = true
        for var i in 0..<images.count
        {
            print(",,,,,", images[i].asset.description)
        }
        SVProgressHUD.show()
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            print("9999", resolvedImages)
            print("345", images[0].asset.mediaSubtypes)
            SVProgressHUD.dismiss()
            self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
        })
    }
    
    // MARK: - Helper
    
    func showLightbox(images: [UIImage]) {
        guard images.count > 0 else {
            return
        }
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.dismissalDelegate = self
        
        gallery.present(lightbox, animated: true, completion: nil)
    }
    
    //############################ end Gallery Controller ##########################
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /// gallery permision
        Gallery.Config.VideoEditor.savesEditedVideoToLibrary = true
//        parseData()
        blurView.alpha = 0
        activityindicator.stopAnimating()
        cancel.layer.cornerRadius = cancel.layer.frame.size.height/2
        cancel.clipsToBounds = true
        post.layer.cornerRadius = post.layer.frame.size.height/2
        post.clipsToBounds = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        singleProductTable.isHidden = true
        ///discount
        let Discount = UITapGestureRecognizer()
        Discount.addTarget(self, action: #selector(tappedTextView(_:)))
        sPrice.addGestureRecognizer(Discount)
        sPrice.layer.cornerRadius = 8
        sPrice.layer.borderColor = UIColor.lightGray.cgColor
        sPrice.layer.borderWidth = 0.5
        sPrice.clipsToBounds = true
        let Unit = UITapGestureRecognizer()
        Unit.addTarget(self, action: #selector(tappedUnit(_:)))
        unitClicked.addGestureRecognizer(Unit)
        unitClicked.layer.cornerRadius = 8
        unitClicked.layer.borderColor = UIColor.lightGray.cgColor
        unitClicked.layer.borderWidth = 0.5
        unitClicked.clipsToBounds = true
        Pvariant.removeAll()
//        moreDetails.isHidden = true
        scroll.delegate = self
        scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 900)
        pickerViewContainer.isHidden = true
        UIView.transition(with: addVariantButtonView, duration: 0.4, options: .beginFromCurrentState, animations:
            {
                //                                self.blueView.alpha = 0.6
                self.addVariantButtonView.transform = CGAffineTransform(translationX: 0, y: 0)
                
                self.addVariantButtonView.isHidden = false
        }, completion: nil)
        
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        if let branddata = UserDefaults.standard.object(forKey: "brand")
        {
            if let brandiddata = UserDefaults.standard.object(forKey: "brandID")
        {
        brandname = native.string(forKey: "brand")!
        brandid = Int(native.string(forKey: "brandID")!)!
            }
        }
        if let categorydata = UserDefaults.standard.object(forKey: "categoryname")
        {
            if let subcategoryid = UserDefaults.standard.object(forKey: "subCategoryId")
        {
        categoryname = native.string(forKey: "categoryname")!
        subCategoryId = Int(native.string(forKey: "subCategoryId")!)!
            }
        }
        
        
        if selectedImage.count >= 0
            
        {   if selectedunits.count > 0
        {
            scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1300)
            }
            else
        {
            scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
            }
            if selectedImage.count == 0
            {
                imagesCountLabel.text = ""
                UIView.transition(with: collectdata_view, duration: 0.4, options: .beginFromCurrentState, animations:
                    {
                        //                    self.blurView.alpha = 0.6
                        self.collectdata_view.transform = CGAffineTransform(translationX: 0, y: -90)
                        self.collection_Container.isHidden = false
                }, completion: nil)
            }
            else
            {
            imagesCountLabel.text = String(selectedImage.count)
            UIView.transition(with: collectdata_view, duration: 0.4, options: .beginFromCurrentState, animations:
                {
//                    self.blurView.alpha = 0.6
                    self.collectdata_view.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.collection_Container.isHidden = false
            }, completion: nil)
            }
        }
        else{
            imagesCountLabel.text = ""
            scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
            UIView.transition(with: collectdata_view, duration: 0.4, options: .beginFromCurrentState, animations:
                {
//                    self.blurView.alpha = 0.6
                    self.collectdata_view.transform = CGAffineTransform(translationX: 0, y: -90)
                    self.collection_Container.isHidden = true
            }, completion: nil)
        }
        
        selectBrandLabel.text = brandname
        selectCategoryLabel.text = categoryname

        
    }
    
    // display category list
    @IBAction func select_Category(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "category") as! SelectCategoryViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
        
        
    }
    
    func collectData()
    {
       sName.text = name
        selectBrandLabel.text = brandname
        sSku.text = sku
    }
    
    
    
    
    @IBAction func picker_dataSelected(_ sender: Any) {
        pickerViewContainer.isHidden = true
        blurView.alpha = 0
        
        print(selectedunits)
    }
    
    @IBAction func cancel_picker(_ sender: Any) {
        pickerViewContainer.isHidden = true
        blurView.alpha = 0
    }
    
    //delete dynamic Disscount
    @objc func delete_Disscount(sender: UIButton)
    {   print("selectedunits", Pvariant)
        
        print(Pvariant[sender.tag]["low"])
        minutes = Pvariant[sender.tag]["low"] as! Int
        seconds = Array(minutes...minutes + 100)
        unitPicker.reloadAllComponents()
        Pvariant.remove(at: sender.tag)
        selectedunits.remove(at: sender.tag)
        selctedprice.remove(at: sender.tag)
        if self.selectedunits.count == 0
        {   self.singleProductTable.isHidden = true
            self.addVariantButtonView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1000)
        }
        else
        {   self.singleProductTable.isHidden = false
            self.addVariantButtonView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1300)
        }
        numberOfVariant = numberOfVariant - 1
        singleProductTable.reloadData()
        
        print("selectedunits", selectedunits)
        print("selectedunits", Pvariant)
        
    }
    
    //insert Dynamic Disscount
    @IBAction func Add_DynamicDisscount(_ sender: Any) {
        insertVariant = true
        
        if((sPrice.text?.isEmpty)! != true ) && (unitClicked.text?.isEmpty)! != true{
            Pvariant.append(["index": numberOfVariant, "low": minutes, "high": time1, "price" : sPrice.text!])
            print("variant product", Pvariant)
            selctedprice.append(Int(sPrice.text!)!)
            selectedunits.insert(String(String(minutes) + " - " + String(time1) + " Units"), at: numberOfVariant)
            minutes = time1 + 1
            if minutes <= 100
            {
                seconds = Array(minutes...100)
            }
            else
            {   seconds = Array(minutes...minutes + 100)
                print("Invalid number")
            }
            
            unitPicker.reloadAllComponents()
            
            
            numberOfVariant = numberOfVariant + 1
            if selectedunits.count > 0 {
                singleProductTable.isHidden = false
            }
            else{
                singleProductTable.isHidden = true
            }
            scroll.contentSize = CGSize(width: self.view.frame.size.width, height: 1250)
            singleProductTable.reloadData()
            sPrice.text = ""
            unitClicked.text = ""
            
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Unit and price can't be empty", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    var clickcount = 0
    //validation...................
    // sku and product name
    func isValidInput(Input:String) -> Bool {
        let RegEx = "[a-zA-Z.?0-9/_-]*"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    
    //special characters
    func validateHsnCode(Input:String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "[A-Za_z0-9]]").inverted
        let inputString = Input.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  Input == filtered
    }
    //number
    func validate(Number: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = Number.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  Number == filtered
    }
    //decimal
    func valueCheck(_ d: String) -> String {
        var result = ""
        do {
            let regex = try NSRegularExpression(pattern: "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$", options: [])
            let results = regex.matches(in: String(d), options: [], range: NSMakeRange(0, String(d).characters.count))
            if results.count > 0 {result = d}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
        }
        print("check regex",result)
        return result
    }
    
    // validation check
    func Parentvalidated() -> Bool {
        var valid: Bool = true
        if ((sHsnCode.text?.isEmpty) != true) && validateHsnCode(Input: sHsnCode.text!) == false{
            sHsnCode.text?.removeAll()
            // change placeholder color to red color for textfield email-id
            sHsnCode.attributedPlaceholder = NSAttributedString(string: " HSN Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
        if ((sSku.text?.isEmpty) == true || isValidInput(Input: sSku.text!) == false) {
            // change placeholder color to red for textfield username
            sSku.text?.removeAll()
            sSku.attributedPlaceholder = NSAttributedString(string:" Invalid SKU",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            valid = false
        }
//        if ((sName.text?.isEmpty) == true) || isValidInput(Input: sName.text!) == false{
//            // change placeholder color to red for textfield password
//            sName.text?.removeAll()
//            sName.attributedPlaceholder = NSAttributedString(string:"Name",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
//            valid = false
//        }
        if ((sSupply.text?.isEmpty) != true) && validate(Number: sSupply.text!) == false{
            // change placeholder color to red for textfield mobile no.
            sSupply.text?.removeAll()
            sSupply.attributedPlaceholder = NSAttributedString(string:"Supply Ability",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
//        if ((sDescription.text?.isEmpty) == true) || isValidInput(Input: sDescription.text!) == false{
//            sDescription.text?.removeAll()
//            // change placeholder color to red for textfield mobile no.
//            sDescription.attributedPlaceholder = NSAttributedString(string:"Please fill Description",attributes: [NSAttributedStringKey.foregroundColor:UIColor.red])
//            valid = false
//        }
        if ((sLenght.text?.isEmpty) != true) && valueCheck(sLenght.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            sLenght.text?.removeAll()
            sLenght.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((sWeight.text?.isEmpty) != true) && valueCheck(sWeight.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            sWeight.text?.removeAll()
            sWeight.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((sHeight.text?.isEmpty) != true) && valueCheck(sHeight.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            sHeight.text?.removeAll()
            sHeight.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        if ((sWidth.text?.isEmpty) != true) && valueCheck(sWeight.text!) == ""{
            // change placeholder color to red for textfield mobile no.
            sWidth.text?.removeAll()
            sWidth.attributedPlaceholder = NSAttributedString(string:"Inavlid Type",attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            valid = false
        }
        
        //
        return valid
    }
    //dissmiss keyboard
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        sName.resignFirstResponder()
        sSku.resignFirstResponder()
        sDescription.resignFirstResponder()
        sHsnCode.resignFirstResponder()
        sSupply.resignFirstResponder()
        sLenght.resignFirstResponder()
        sWidth.resignFirstResponder()
        sHeight.resignFirstResponder()
        sWeight.resignFirstResponder()
        
    }
    ///Display Brand
    @IBAction func select_brand(_ sender: Any) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "brand") as! SelectBrandViewController
        self.navigationController?.pushViewController(newViewController, animated: true)
    
    }
    
}



//create video thumbnail image
extension PHAsset {
    var thumbnailImage : UIImage {
        get {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var thumbnail = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: self, targetSize: CGSize(width: 300, height: 300), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                thumbnail = result!
            })
            return thumbnail
        }
    }
}
