//
//  SelecttoCreateVariantViewController.swift
//  Hub9
//
//  Created by Deepak on 7/23/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import iOSDropDown
import AWSS3
import AWSCore
import Alamofire
import RSSelectionMenu

class SelecttoCreateVariantViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    let model: [[UIColor]] = generateRandomData()
    var storedOffsets = [Int: CGFloat]()
    
    var imagePicker: UIImagePickerController!
     var selectedImage:[[UIImage]] = []
    var parentsku = ""
    
    
    var native = UserDefaults.standard
    var variantType = ["Color", "Size", "Material"]
    var colorcount = ["","",""]
    
    var selectVariantTypeIndex = 0
    var allVariantType = [String]()
    var sku = [String]()
    var price = [String]()
    var moq = [String]()
    var stock = [String]()
    var edittext = -1
    var i = 0
    var activeTableIndex = 0
    var check = 1
    var Color = [String]()
    var Colorlkid = [String]()
    var selectedColor = [String]()
    var selectedColorID = [String]()
    var Size = [String]()
    var Sizelkid = [String]()
    var selectedSize = [String]()
    var selectedSizeID = [String]()
    var Material = [String]()
    var Materiallkid = [String]()
    var selectedMaterial = [String]()
    var selectedMaterialID = [String]()

    
    
    @IBOutlet var DisplayVariantTable: UITableView!
    @IBOutlet var productVariantImages: UICollectionView!
    @IBAction func CancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // craete lkpid
    func createLK()
    {
        self.selectedMaterialID.removeAll()
        for var i in 0..<self.Material.count
        {
            for var j in 0..<self.selectedMaterial.count
            {
                if self.Material[i] == self.selectedMaterial[j]
                {
                    print("....",self.Material[i])
                    self.selectedMaterialID.insert(self.Materiallkid[i], at: i)
                }
                
                }
            }
        self.selectedSizeID.removeAll()
        for var i in 0..<self.Size.count
        {
            for var j in 0..<self.selectedSize.count
            {
                if self.Size[i] == self.selectedSize[j]
                {
                    print("....",self.Size[i])
                    self.selectedSizeID.insert(self.Sizelkid[i], at: i)
                }
            }
        }
        self.selectedColorID.removeAll()
        for var i in 0..<self.Color.count
        {
            for var j in 0..<self.selectedColor.count
            {
                if self.Color[i] == self.selectedColor[j]
                {
                    print("....",self.Color[i])
                    self.selectedColorID.insert(self.Colorlkid[i], at: i)
                }
            }
        }
        print(self.selectedSizeID)
        print(self.selectedColorID)
        print(self.selectedMaterialID)
            
        }


    
    
    
    
    
    @IBAction func DoneButton(_ sender: Any) {
        var checkValid = true
//        createLK()
        for var i in 0..<sku.count
        {
        if (isValidInput(Input: sku[i]) == false)
        {
            i = sku.count
            print(sku, i, sku.count)
            sku[i - 1] = "0"
            checkValid = false
        }
          else if  valueCheck(price[i]) == ""
            {
                i = sku.count
                price[i - 1] = "a"
                checkValid = false
            }
            else if validate(Number: moq[i]) == false
            {
                i = sku.count
                checkValid = false
                moq[i - 1] = "a"
            }
            else if validate(Number: stock[i]) == false
            {
                i = sku.count
                checkValid = false
                sku[i - 1] = "a"
            }
//            else if moq[i] > stock[i]
//             {
//                let alert = UIAlertController(title: "Alert", message: "MOQ can't be greater than stock", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
        }
        if checkValid == true
        {
            ///create default storage for array
            native.set("true", forKey: "createvarient")
            native.synchronize()
            UserDefaults.standard.set(sku, forKey: "sku")
            native.synchronize()
            UserDefaults.standard.set(price, forKey: "price")
            native.synchronize()
            UserDefaults.standard.set(moq, forKey: "moq")
            native.synchronize()
            UserDefaults.standard.set(stock, forKey: "stock")
            native.synchronize()
            UserDefaults.standard.set(allVariantType, forKey: "title")
            native.synchronize()
            let vc = AllSellVariantViewController()
            vc.copy_data(images: selectedImage, skudata: sku)
            vc.copyVariantType(colorlk: self.selectedColorID, sizelk: self.selectedSizeID, materiallk: self.selectedMaterialID)
            
        self.dismiss(animated: true, completion: nil)
        }
        else{
            DisplayVariantTable.reloadData()
            let alert = UIAlertController(title: "Alert", message: "Invalid data", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         check = 1
        CreateColor()
        CreateSize()
        CreateMaterial()
        
        
       
        // Do any additional setup after loading the view.
    }
    @objc func SelectColor()
    {
        let selectionMenu = RSSelectionMenu(selectionStyle: .multiple, dataSource: Color, cellType: .basic) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        selectionMenu.setSelectedItems(items: selectedColor, maxSelected: 8) { (text, index, selected, selectedItems) in
            self.selectedColor = selectedItems
            print(selectedItems)
            
           
        }
        // show searchbar
        selectionMenu.showSearchBar { (searchtext) -> ([String]) in
            
            // return filtered array based on any condition
            // here let's return array where name starts with specified search text
            
            return self.Color.filter({ $0.lowercased().hasPrefix(searchtext.lowercased()) })
        }
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .push, from: self)
    }
    @objc func SelectSize()
    {
        let selectionMenu = RSSelectionMenu(selectionStyle: .multiple, dataSource: Size, cellType: .basic) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        selectionMenu.setSelectedItems(items: selectedSize, maxSelected: 8) { (text, index, selected, selectedItems) in
            self.selectedSize = selectedItems
            print(selectedItems)
            
            
        }
        // show searchbar
        selectionMenu.showSearchBar { (searchtext) -> ([String]) in
            
            // return filtered array based on any condition
            // here let's return array where name starts with specified search text
            
            return self.Size.filter({ $0.lowercased().hasPrefix(searchtext.lowercased()) })
        }
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .push, from: self)
    }
    @objc func SelectMaterial()
    {
        let selectionMenu = RSSelectionMenu(selectionStyle: .multiple, dataSource: Material, cellType: .basic) { (cell, object, indexPath) in
            cell.textLabel?.text = object
        }
        selectionMenu.setSelectedItems(items: selectedMaterial, maxSelected: 8) { (text, index, selected, selectedItems) in
            self.selectedMaterial = selectedItems

            print(selectedItems)
        }
        // show searchbar
        selectionMenu.showSearchBar { (searchtext) -> ([String]) in
            
            // return filtered array based on any condition
            // here let's return array where name starts with specified search text
            
            return self.Material.filter({ $0.lowercased().hasPrefix(searchtext.lowercased()) })
        }
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .push, from: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        
        let defaults = UserDefaults.standard
        if selectVariantTypeIndex == 0
        {
            if selectedColor.count > 0
            {
                colorcount[0] = String(selectedColor.count)
            }
            else
            {
                colorcount[0] = ""
            }
            print("selectedColor",selectedColor)
        }
        else if selectVariantTypeIndex == 1
        {
            if selectedSize.count > 0
            {
                colorcount[1] = String(selectedSize.count)
            }
            else
            {
                colorcount[1] = ""
            }
            print("selectedSize",selectedSize)
        }
        else if selectVariantTypeIndex == 2
        {
            if selectedMaterial.count > 0
            {
                colorcount[2] = String(selectedMaterial.count)
            }
            else
            {
                colorcount[2] = ""
            }
            print("selectedMaterial",selectedMaterial)
        }
        print("all", selectedColor, selectedSize, selectedMaterial)
        print("---------",check)
        if check == 1
        {
        createVariant()
            
        }
        
    }
    ////create variant
    func createVariant()
    {
//        createLK()
        allVariantType.removeAll()
        selectedImage.removeAll()
        sku.removeAll()
        price.removeAll()
        moq.removeAll()
        stock.removeAll()
        if selectedColor.count != 0 && selectedSize.count != 0 && selectedMaterial.count != 0
        {
            for var i in 0..<selectedColor.count
            {
                for var j in 0..<selectedSize.count
                {
                    for var k in 0..<selectedMaterial.count
                    {
                        let color1 = selectedColor[i]
                        let size1 = selectedSize[j]
                        let material1 = selectedMaterial[k]
                        allVariantType.append(parentsku + color1 + " " + size1 + " " + material1)
                    }
                }
            }
        }
        else if selectedColor.count != 0 && selectedSize.count != 0 && selectedMaterial.count == 0
        {
            for var i in 0..<selectedColor.count
            {
                for var j in 0..<selectedSize.count
                {
                    
                    let color1 = selectedColor[i]
                    let size1 = selectedSize[j]
                    allVariantType.append(parentsku + color1 + " " + size1)
                    
                }
            }
        }
        else if selectedColor.count != 0 && selectedSize.count == 0 && selectedMaterial.count != 0
        {
            for var i in 0..<selectedColor.count
            {
                
                for var k in 0..<selectedMaterial.count
                {
                    let color1 = selectedColor[i]
                    let material1 = selectedMaterial[k]
                    allVariantType.append(parentsku + color1 + " " + material1)
                }
                
            }
        }
        else if selectedColor.count == 0 && selectedSize.count != 0 && selectedMaterial.count != 0
        {
            
            for var j in 0..<selectedSize.count
            {
                for var k in 0..<selectedMaterial.count
                {
                    let size1 = selectedSize[j]
                    let material1 = selectedMaterial[k]
                    allVariantType.append(parentsku + size1 + " " + material1)
                }
            }
            
        }
        else if selectedColor.count == 0 && selectedSize.count == 0 && selectedMaterial.count != 0
        {
            
            
            for var k in 0..<selectedMaterial.count
            {
                let material1 = selectedMaterial[k]
                allVariantType.append( parentsku + material1)
            }
            
            
        }
        else if selectedColor.count == 0 && selectedSize.count != 0 && selectedMaterial.count == 0
        {
            
            for var j in 0..<selectedSize.count
            {
                
                let size1 = selectedSize[j]
                allVariantType.append(parentsku + size1 )
                
            }
            
        }
        else if selectedColor.count != 0 && selectedSize.count == 0 && selectedMaterial.count == 0
        {
            for var i in 0..<selectedColor.count
            {
                
                let color1 = selectedColor[i]
                allVariantType.append(parentsku + color1)
            }
        }
        print("all data", allVariantType)
        let auto = 100
        for var i in 0..<allVariantType.count
        {
            let trimmedString = allVariantType[i].replacingOccurrences(of: " ", with: "")
            sku.append("\(trimmedString)-\(auto + i)")
            price.append("0")
            moq.append("0")
            stock.append("0")
            
            selectedImage.insert([UIImage(named:"car")!], at: i)
        }
        print("selectedImage:::", selectedImage)
        DisplayVariantTable.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return variantType.count + allVariantType.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        if tableView == DisplayVariantTable
        {
            check = 1
            if indexPath.row == 0
        {
            selectVariantTypeIndex = 0
            SelectColor()
        }
            else if indexPath.row == 1
        {
            selectVariantTypeIndex = 1
            SelectSize()
        }
            else if indexPath.row == 2
        {
            selectVariantTypeIndex = 2
            SelectMaterial()
        }
        else{
            selectVariantTypeIndex = 3
             print("index", indexPath.row)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == DisplayVariantTable
        {
            if indexPath.row < 3
            {
            return 40
            }
            else{
                return 300
            }
        }
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         var cellToReturn = UITableViewCell()
        if indexPath.row < 3  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "displayvarianttype", for: indexPath) as! DisplayVariantTypeTableViewCell
            cell.name.text = variantType[indexPath.row]
            cell.count.text = colorcount[indexPath.row]
            cellToReturn = cell
        }
        else   {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "displayvarianttable", for: indexPath) as! DisplayvariantTableViewCell
            cell.productVariantImages.delegate = self
            cell.productVariantImages.dataSource = self
            cell.productVariantImages.tag = indexPath.row
            cell.productVariantImages.reloadData()
            cell.shadwo.layer.cornerRadius = 8
            cell.shadwo.clipsToBounds = true
            cell.sku.delegate = self
            cell.price.delegate = self
            cell.stock.delegate = self
            cell.moq.delegate = self
            cell.sku.tag = indexPath.row
            print("fgd",cell.sku.tag)
            cell.price.tag = indexPath.row
            cell.stock.tag = indexPath.row
            cell.moq.tag = indexPath.row
            cell.deletecell.tag = indexPath.row 
            cell.deletecell.addTarget(self, action: #selector(deleteCell(sender:)), for: .touchUpInside)
            cell.sku.addTarget(self, action: #selector(sku(_:)), for: .editingChanged)
            cell.price.addTarget(self, action: #selector(price(_:)), for: .editingChanged)
            cell.stock.addTarget(self, action: #selector(stock(_:)), for: .editingChanged)
            cell.moq.addTarget(self, action: #selector(moq(_:)), for: .editingChanged)
            cell.VariantTitle.text = allVariantType[indexPath.row - 3]
            cell.sku.layer.borderColor = UIColor.red.cgColor
            cell.sku.clipsToBounds = true
            print("sku", sku[indexPath.row - 3])
            if sku[indexPath.row - 3] == "0"
            {
            cell.sku.text?.removeAll()
                cell.sku.attributedPlaceholder = NSAttributedString(string: "Invalid sku", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            }
            else
            {
                cell.sku.text = sku[indexPath.row - 3]
            }
            if price[indexPath.row - 3] == "a"
            {
                cell.price.text?.removeAll()
                cell.price.attributedPlaceholder = NSAttributedString(string: "Invalid Price", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            }
            else{
            cell.price.text = price[indexPath.row - 3]
            }
            if moq[indexPath.row - 3] == "a"
            {
                cell.moq.text?.removeAll()
                cell.moq.attributedPlaceholder = NSAttributedString(string: "Invalid MOQ Type", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            }
            else
            {
            cell.moq.text = moq[indexPath.row - 3]
            }
            if stock[indexPath.row - 3] == "a"
            {
                cell.stock.text?.removeAll()
                cell.stock.attributedPlaceholder = NSAttributedString(string: "Invalid stock", attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            }
            else
            {
            cell.stock.text = stock[indexPath.row - 3]
            }
            cellToReturn = cell
        }
        return cellToReturn
    }
    
    public func uploadPhotos()
    {   print("uploading", selectedImage, sku)
        for var i in 0..<selectedImage.count
    {
        let accessKey = "AKIAJYCW54FQC6MMHIDQ"
        let secretKey = "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"
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let S3BucketName = "android-image-bucket/19/pj-10"
        let remoteName = "test\(i).png"
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(remoteName)
        //                let image = UIImage(named: "car")
        if let data = UIImageJPEGRepresentation(selectedImage[0][i], 0.2) {
            print(data.count)
        
        do {
            try data.write(to: fileURL)
        }
        catch {}
        }
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = remoteName
        uploadRequest.bucket = S3BucketName
        uploadRequest.contentType = "image/jpeg"
        uploadRequest.acl = .bucketOwnerRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith { [weak self] (task) -> Any? in
            DispatchQueue.main.async {
                
            }
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    print("Uploaded to:\(absoluteString)")
                }
            }
            
            return nil
        }
        }
    }
    
    
    
    
    @IBAction func clickPhotos(sender: UIButton) {
        check = 0
        activeTableIndex = sender.tag
        print( "active table index is:" ,activeTableIndex)
        let actionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (alert: UIAlertAction) in
            print("Take Photo Code")
            if(UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                let myPickerController = UIImagePickerController()
                myPickerController.delegate = self
                myPickerController.allowsEditing = true
                myPickerController.sourceType = .camera
                self.present(myPickerController, animated: true, completion: nil)
            }
            else
            {
                let actionController: UIAlertController = UIAlertController(title: "Camera is not available",message: "", preferredStyle: .alert)
                let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) {
                    action -> Void  in
                    //Just dismiss the action sheet
                }
                actionController.addAction(cancelAction)
                self.present(actionController, animated: true, completion: nil)
            }}
        
        let gallerySelect = UIAlertAction(title: "Choose from Photos", style: .default) { (alert: UIAlertAction) in
            print("Gallery Select Code")
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        
        //            // Uncomment here and below at actionMenu to add a save photo action to the action list
        
        //            let savePhoto = UIAlertAction(title: "Save Photo", style: .Default, handler: { (alert: UIAlertAction) in
        //                print("Save photo code")
        //            })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction) in
            print("Cancelled")
        }
        
        actionMenu.addAction(takePhoto)
        actionMenu.addAction(gallerySelect)
        //          actionMenu.addAction(savePhoto)
        actionMenu.addAction(cancelAction)
        
        self.present(actionMenu, animated: true, completion: nil)
    }
    //#MARK: Image picker protocol functions
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if info[UIImagePickerControllerOriginalImage] as? UIImage != nil {
//            imagePicker.dismiss(animated: true, completion: nil)
            //            profileImage.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            print("gallery Images:", info[UIImagePickerControllerOriginalImage] as? UIImage)
            let data = (info[UIImagePickerControllerOriginalImage] as? UIImage)
            
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage
            {
                selectedImage[activeTableIndex - 3].insert(image, at: selectedImage[activeTableIndex - 3].count)
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            {
                print(activeTableIndex - 3)
//                selectedImage[0].insert(image, at: 0)
                print("current index count", selectedImage[activeTableIndex - 3].count)
                selectedImage[activeTableIndex - 3].insert(image, at: selectedImage[activeTableIndex - 3].count)
                
               print("selectedImage", selectedImage, selectedImage.count)
            }
            else
            {
                print("Something went wrong")
            }
            
        }
        self.dismiss(animated: true, completion: nil)
        DisplayVariantTable.reloadData()
       
        
    }
    
    //delete cell
    @objc func deleteCell(sender: UIButton)
    { var check = false
        sku.remove(at: sender.tag - 3)
        price.remove(at: sender.tag - 3)
        moq.remove(at: sender.tag - 3)
        stock.remove(at: sender.tag - 3)
        selectedImage.remove(at: sender.tag - 3)
        allVariantType.remove(at: sender.tag - 3)
        for var i in 0..<self.selectedColor.count
        { print(i)
            if check == false
            {
            if allVariantType.contains(selectedColor[i]) == false
            {
            selectedColor.remove(at: i)
                colorcount[0] = String(selectedColor.count)
                print(colorcount)
                check = true
            }
            }
        }
        check = false
        for var i in 0..<self.selectedSize.count
        {
            print(i, selectedSize, selectedSize.count)
            
            if check == false
            {
            if allVariantType.contains(selectedSize[i]) == false
            {   print(allVariantType.contains(selectedSize[i]),selectedSize)
                selectedSize.remove(at: i)
                colorcount[1] = String(selectedSize.count)
//                print("delete", selectedSize[i])
                print(colorcount)
                check = true
                            }
            }
        }
        check = false
        for var i in 0..<self.selectedMaterial.count
        {
            if check == false
            {
            print(i)
            if allVariantType.contains(selectedMaterial[i]) == false
            {
                selectedMaterial.remove(at: i)
                colorcount[2] = String(selectedMaterial.count)
                check = true
            }
        }
        }
    
        self.DisplayVariantTable.reloadData()
    }
    
    @objc func sku(_ textField: UITextField)
    {
        edittext = 0
        print("index is ", edittext)
    }
    @objc func price(_ textField: UITextField)
    {
        edittext = 1
        print("index is ", edittext)
    }
    @objc func stock(_ textField: UITextField)
    {
        edittext = 3
        print("index is ", edittext)
    }
    @objc func moq(_ textField: UITextField)
    {
        edittext = 2
        print("index is ", edittext)
    }
    func valueCheck(_ d: String) -> String {
        var result = ""
        do {
            let regex = try NSRegularExpression(pattern: "^(?:|0|[1-9]\\d*)(?:\\.\\d*)?$", options: [])
            let results = regex.matches(in: String(d), options: [], range: NSMakeRange(0, String(d).characters.count))
            if results.count > 0 {result = d}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
        }
        print("check regex",result)
        return result
    }
    
    func validate(Number: String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = Number.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  Number == filtered
    }
    func isValidInput(Input:String) -> Bool {
        let RegEx = "[a-zA-Z.?0-9/_-]*"
        let Test = NSPredicate(format:"SELF MATCHES %@", RegEx)
        return Test.evaluate(with: Input)
    }
    func CreateColor()
    { let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_color/") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response shop: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    let innerdict = dict1["data"] as! [[String:Any]]
                    
//                    print("inner dict", innerdict)
                    for var i in 0..<innerdict.count
                    {
//                        print("color", innerdict[i]["color_ui"]!, innerdict[i]["color_lkpid"]!)
                        self.Color.append(innerdict[i]["color_ui"]! as! String)
                        self.Colorlkid.append(String(innerdict[i]["color_lkpid"]! as! Int))
                    }
                    let data = innerdict
                    print(" json", data)
                    

                }
            }
        }
    }
    func CreateSize()
    { let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            let category = self.native.string(forKey: "subcategoryID")!
            print(category,"category id is...")
            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_size/?categoryid=\(category)") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response shop: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    let innerdict = dict1["data"] as! [[String:Any]]
                    
//                    print("inner dict", innerdict)
                    for var i in 0..<innerdict.count
                    {
//                  print("Size", innerdict[i]["size_ui"]!, innerdict[i]                                      ["size_lkpid"]!)
                        self.Size.append(innerdict[i]["size_ui"]! as! String)
                        self.Sizelkid.append(String(innerdict[i]["size_lkpid"]! as! Int))
                    }
                    let data = innerdict
                    print(" json", data)
                    
                   //////
                    if result.isSuccess == true
                    {
                    let number = self.native.string(forKey: "selectVariantList")
                        self.parentsku = self.native.string(forKey: "Psku")!
                    if number == "0"
                    {
                        self.selectVariantTypeIndex = 0
                        self.SelectColor()
                    }
                    else if number == "1"
                    {
                        self.selectVariantTypeIndex = 1
                        self.SelectSize()
                    }
                    else {
                        self.selectVariantTypeIndex = 2
                        self.SelectMaterial()
                    }
                    }
                }
            }
        }
    }
    func CreateMaterial()
    { let token = self.native.string(forKey: "Token")!
        if token != nil
        {
            let category = self.native.string(forKey: "subcategoryID")!
            print(category,"category id is...")
            var a = URLRequest(url: NSURL(string: "https://demo.hub9.io/api/get_listing_material/?categoryid=\(category)") as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugPrint(response)
                print("response shop: \(response)")
                let result = response.result
                if let dict1 = result.value as? Dictionary<String, AnyObject>{
                    let innerdict = dict1["data"] as! [[String:Any]]
                    
//                    print("inner dict", innerdict)
                    for var i in 0..<innerdict.count
                    {
//                        print("Materail", innerdict[i]["material_ui"]!, innerdict[i]["material_lkpid"]!)
                        self.Material.append(innerdict[i]["material_ui"]! as! String)
                        self.Materiallkid.append(String(innerdict[i]["material_lkpid"]! as! Int))
                    }
                    let data = innerdict
                    print(" json", data)
                    
                    
                    
                }
            }
        }
    }
//    func skuValidate(sku: String) -> Bool {
//        let charcterSet  = NSCharacterSet(charactersIn: "^(?=.*)(?=.*[a-z])[a-z]{6}").inverted
//        let inputString = sku.components(separatedBy: charcterSet)
//        let filtered = inputString.joined(separator: "")
//        return  sku == filtered
//    }
}


extension SelecttoCreateVariantViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //        texts[textField.tag] = textField.text
        
        print(textField.tag)
        print("data",textField.text)
        
        if let item = textField.text {
            if edittext == 0
            {
            if item == "" || textField.text?.contains(" ") == true || textField.text?.contains("'") == true || textField.text?.contains("@") == true || isValidInput(Input: textField.text!) == false
            {   print("index is ", edittext)
                sku.remove(at: textField.tag - 3)
                sku.insert("0", at: textField.tag - 3)
                print("validate sku ", sku)
                edittext = -1
                
            }
            else{
                sku.remove(at: textField.tag - 3)
                sku.insert(textField.text!, at: textField.tag - 3)
                print("validate sku ", sku)
                edittext = -1
                
            }
            }
            else if edittext == 1{
                if (textField.text?.count)! > 0 && validate(Number: textField.text!) == true{
                    price.remove(at: textField.tag - 3)
                    price.insert(textField.text!, at: textField.tag - 3)
                    print("validate price ", price)
                    edittext = -1
                    
                }
                else
                {   print("index is ", edittext)
                    price.remove(at: textField.tag - 3)
                    price.insert("a", at: textField.tag - 3)
                    print("validate price ", price)
                    edittext = -1
                    
                }
                
            }
            else if edittext == 3{
                
                if (textField.text?.count)! > 0 && validate(Number: textField.text!) == true{
                    stock.remove(at: textField.tag - 3)
                    stock.insert(textField.text!, at: textField.tag - 3)
                    print("validate stock ", stock)
                    edittext = -1
                    
                }
            else
                {   print("index is ", edittext)
                    stock.remove(at: textField.tag - 3)
                    stock.insert("a", at: textField.tag - 3)
                    print("validate stock ", stock)
                    edittext = -1
                    
                }
                
            }
            else if edittext == 2{
                if (textField.text?.count)! > 0 && validate(Number: textField.text!) == true{
                    moq.remove(at: textField.tag - 3)
                    moq.insert(textField.text!, at: textField.tag - 3)
                    print("validate moq ", moq)
                    edittext = -1
                    
                }
                else
                {   print("index is ", edittext)
                    moq.remove(at: textField.tag - 3)
                    moq.insert("a", at: textField.tag - 3)
                    print("validate moq ", moq)
                    edittext = -1
                    
                }
                
            }
        }
        self.DisplayVariantTable.reloadData()
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //        selectindexforediting = textField.tag
        //        pickerViewData()
        return true
    }
    func delete_variantImages(Start: Int, end: Int)
    {
        print(selectedImage, Start, end)
     selectedImage[Start].remove(at: end)
    }
}
extension SelecttoCreateVariantViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    // collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("image count of index",collectionView.tag - 3, selectedImage[collectionView.tag - 3].count)
        return selectedImage[collectionView.tag - 3].count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addproduct", for: indexPath) as! AddProductCell
        print("index is :", cell.tag)
        if indexPath.row > 0
        {
            
            cell.images.layer.cornerRadius = 8
            cell.cancel.layer.cornerRadius = cell.cancel.layer.frame.size.height/2
            cell.cancel.clipsToBounds = true
            cell.images.image = selectedImage[collectionView.tag - 3][indexPath.row]
            cell.images.clipsToBounds = true
            //        cell.images.image = UIImage(named: "car")
            cell.photos.isHidden = true
            cell.camera.isHidden = true
            cell.cancel.isHidden = false
            cell.clickPhotos.isHidden = true
            let button = UIButton(frame: CGRect())
            button.layer.cornerRadius = button.layer.bounds.size.width / 2
            button.layer.masksToBounds = true
            //cell.cancel.layer.cornerRadius = 0.5 * cancel.bounds.selectedSize.width
            cell.cancel.layer.masksToBounds = true
            
        }else{
            cell.images.layer.cornerRadius = 8
            cell.images.clipsToBounds = true
            cell.images.image = nil
            cell.photos.isHidden = false
            cell.camera.isHidden = false
            cell.cancel.isHidden = true
            cell.clickPhotos.isHidden = false
            cell.clickPhotos.tag = collectionView.tag
            cell.clickPhotos.addTarget(self, action: #selector(clickPhotos(sender:)), for: .touchUpInside)
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("collection index is", collectionView.tag, indexPath.row)
        if indexPath.row > 0
        {
            delete_variantImages(Start: collectionView.tag - 3, end: indexPath.row)
            
//            print(selectedImage)
            DisplayVariantTable.reloadData()
        }
        
    }
    
    
}
