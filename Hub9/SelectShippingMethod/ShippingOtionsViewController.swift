//
//  ShippingOtionsViewController.swift
//  Hub9
//
//  Created by Deepak on 9/11/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShippingOtionsViewController: UIViewController, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
    
    var native = UserDefaults.standard
    
    @IBOutlet weak var ShippingContainer: UIView!
    @IBOutlet weak var pickupLocationContainer: UIView!
    @IBOutlet weak var expressShippingContainer: UIView!
    @IBOutlet weak var standerdShippingContainer: UIView!
    
    @IBOutlet weak var selectPickupIcon: UIImageView!
    @IBOutlet weak var expressIcon: UIImageView!
    @IBOutlet weak var standerdIcon: UIImageView!
    
    @IBOutlet weak var pickupLocCharge: UILabel!
    @IBOutlet weak var expShipCharge: UILabel!
    @IBOutlet weak var stdShipCharge: UILabel!
    
    @IBOutlet weak var pickupHeader: UILabel!
    @IBOutlet weak var expHeader: UILabel!
    @IBOutlet weak var stdHeader: UILabel!
    
    @IBOutlet weak var expressTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var standerdTopConstraint: NSLayoutConstraint!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let stdShipping = PaymentAddress.stdShipMethod as? AnyObject
        {
            var startDate = 0
            var endDate = 0
        if let price = stdShipping["shipping_charge"] as? NSNumber
        {
            stdShipCharge.text = "Standard charge: \(PaymentAddress.currency)\(price)"
        }
        if let dateFrom = stdShipping["processing_from"] as? NSNumber
        {
        startDate = Int(dateFrom)
        }
        if let dateTo = stdShipping["processing_to"] as? NSNumber
        {
        endDate = Int(dateTo)
        }
            if startDate == endDate
            {
                var days = displayDay(day: startDate)
                stdHeader.text = "Estimated Delivery by \(days)"
            }
            else
            {
                print("gjhgjhgj", startDate, endDate)
               var firstDate = displayDate(day: startDate)
                var sec = displayDate(day: endDate)
               stdHeader.text = "Estimated Delivery \(firstDate) to \(sec)"
            }
            
        }
        if let expShipping = PaymentAddress.expShipMethod as? AnyObject
        {
            var startDate = 0
            var endDate = 0
            if let dateFrom = expShipping["processing_from"] as? NSNumber
            {
                startDate = Int(dateFrom)
            }
            if let dateTo = expShipping["processing_to"] as? NSNumber
            {
                endDate = Int(dateTo)
            }
            if let price = expShipping["express_shipping_charge"] as? NSNumber
            {
                expShipCharge.text = "Express charge: \(PaymentAddress.currency)\(price)"
            }
//            if startDate == endDate
//            {
//                var days = displayDay(day: startDate)
//                expHeader.text = "Estimated Delivery by \(days)"
//            }
//            else
//            {
                print("gjhgjhgj", startDate, endDate)
                var firstDate = displayDate(day: startDate)
                var sec = displayDate(day: endDate)
                expHeader.text = "Estimated Delivery \(firstDate) to \(sec)"
//            }
            
        }
        
        
        if PaymentAddress.expressShipping == true && PaymentAddress.pickupLocation == true
        {
            UIView.transition(with: self.expressShippingContainer, duration: 0.4, options: .beginFromCurrentState, animations:
                {
                    //                    self.blurView.alpha = 0.6
                    self.expressShippingContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.standerdShippingContainer.transform = CGAffineTransform(translationX: 0, y: 0)
                    self.pickupLocationContainer.isHidden = false
                    self.expressShippingContainer.isHidden = false
                    self.standerdShippingContainer.isHidden = false
            }, completion: nil)
        }
        else if PaymentAddress.expressShipping == true && PaymentAddress.pickupLocation == false
        {
            self.expressTopConstraint.constant = -115
            self.pickupLocationContainer.isHidden = true
            self.expressShippingContainer.isHidden = false
            self.standerdShippingContainer.isHidden = false
        }
        else if PaymentAddress.expressShipping == false && PaymentAddress.pickupLocation == true
        {
             self.expressShippingContainer.transform = CGAffineTransform(translationX: 0, y: 0)
            self.standerdTopConstraint.constant = 68
            self.expressShippingContainer.isHidden = true
            self.standerdShippingContainer.isHidden = false
            self.pickupLocationContainer.isHidden = false
           
        }
        // Do any additional setup after loading the view.
    }
    
    
    func displayDate(day:Int)->String
    {
        let today = Date()
        var count1 = day
        var DateName = ""
        var i = 0
        while i < count1
        {
            
            let nextDate = Calendar.current.date(byAdding: .day, value: i, to: today)
            let dateFormatter = DateFormatter()
            dateFormatter.setLocalizedDateFormatFromTemplate("EEEE")
            var daysName = dateFormatter.string(from: nextDate!)
            if String(daysName) == "Sunday" || String(daysName) == "Saturday"
            {
                count1 += 1
            }
            i += 1
            let dateFormatter1 = DateFormatter()
            dateFormatter1.setLocalizedDateFormatFromTemplate("MMMdd")
            var daysName1 = dateFormatter1.string(from: nextDate!)
            DateName = String(daysName1)
            print("6437", daysName1, count1, i)
        }
        return DateName
    }
    
    func displayDay(day:Int)->String
    {
        let today = Date()
        var count1 = day
        var DayName = ""
        var prefixText = ""
        var i = 0
        while i < count1
        {
            
            let nextDate = Calendar.current.date(byAdding: .day, value: i, to: today)
            let dateFormatter = DateFormatter()
            dateFormatter.setLocalizedDateFormatFromTemplate("EEEE")
            var daysName = dateFormatter.string(from: nextDate!)
            if String(daysName) == "Sunday" || String(daysName) == "Saturday"
            {
                count1 += 1
            }
            i += 1
            DayName = String(daysName)
            print("6437", daysName, count1, i)
        }
        return DayName
    }
    

    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil )
    }
    
    //location pickup
    @IBAction func pickupLocationButton(_ sender: Any) {
        
        self.selectPickupIcon.image = UIImage(named: "filledCheckmark")
        self.expressIcon.image = UIImage(named: "unfilledCheckmark")
        self.standerdIcon.image = UIImage(named: "unfilledCheckmark")
        self.dismiss(animated: true, completion: nil)
        PaymentAddress.openShippingAddress()
        
    }
    
    // express shipping
    @IBAction func expressButton(_ sender: Any) {
        PaymentAddress.shippingCost = PaymentAddress.expressCharge
        PaymentAddress.calculatePrice()
        self.selectPickupIcon.image = UIImage(named: "unfilledCheckmark")
        self.expressIcon.image = UIImage(named: "filledCheckmark")
        self.standerdIcon.image = UIImage(named: "unfilledCheckmark")
        self.dismiss(animated: true, completion: nil)
    }
    
    // standerd shipping
    @IBAction func standerdButton(_ sender: Any) {
        PaymentAddress.shippingCost = PaymentAddress.standerdCharge
        PaymentAddress.calculatePrice()
        self.selectPickupIcon.image = UIImage(named: "unfilledCheckmark")
        self.expressIcon.image = UIImage(named: "unfilledCheckmark")
        self.standerdIcon.image = UIImage(named: "filledCheckmark")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
