//
//  ShippingAddressViewController.swift
//  Hub9
//
//  Created by Deepak on 9/16/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShippingAddressViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var pickupLocation = [AnyObject]()
    var selectedIndex = -1

    @IBOutlet weak var shippingAddressTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        shippingAddressTable.tableFooterView = UIView()
        if PaymentAddress.pickupLocationData.count>0
        {
            pickupLocation = PaymentAddress.pickupLocationData
        }
        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if pickupLocation.count>indexPath.row
        {
            self.selectedIndex = indexPath.row
            print((pickupLocation[indexPath.row]["charge"] as? NSNumber)!)
            if let charge = pickupLocation[indexPath.row]["charge"] as? NSNumber
            {
            let address = PaymentAddress.selectAddress as! [AnyObject]
            PaymentAddress.selectAddress.removeAll()
            PaymentAddress.selectAddress.append(pickupLocation[indexPath.row])
            for var i in 0..<address.count
            {
                PaymentAddress.selectAddress.append(address[i])
            }
            
                print("address added", PaymentAddress.selectAddress)
                PaymentAddress.shippingCost = Double(charge) as! Double
                PaymentAddress.lockerApplied = false
                PaymentAddress.pickupApplied = true
                PaymentAddress.selectshippingindex = 0
                PaymentAddress.selectbillingindex = 0
                PaymentAddress.shippingaddress.reloadData()
                PaymentAddress.calculatePrice()
                self.navigationController?.popViewController(animated: true)
            }
            shippingAddressTable.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pickupLocation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shippingTable", for: indexPath) as! ShippingAddressTableViewCell
        cell.shippingAddress.text = "\((pickupLocation[indexPath.row]["address_name"] as? String)!) \((pickupLocation[indexPath.row]["city_name"] as? String)!) \((pickupLocation[indexPath.row]["state_name"] as? String)!)"
        if selectedIndex == indexPath.row
        {
            cell.addressIcon.image = UIImage(named: "filledCheckmark")
        }
        else
        {
            cell.addressIcon.image = UIImage(named: "unfilledCheckmark")
        }
        return cell
    }

}
