//
//  ShippingAddressTableViewCell.swift
//  Hub9
//
//  Created by Deepak on 9/16/19.
//  Copyright © 2019 Deepak. All rights reserved.
//

import UIKit

class ShippingAddressTableViewCell: UITableViewCell {

    
    @IBOutlet weak var addressIcon: UIImageView!
    @IBOutlet weak var shippingAddress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
