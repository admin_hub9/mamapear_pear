//
//  QoutationDetailsViewController.swift
//  Hub9
//
//  Created by Deepak on 9/24/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class QoutationDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var native = UserDefaults.standard
    var Awaitingcount = 0
    var Approvedcount = 0
    var Rejectedcount = 0
    var Allcount = 0
    var ActiveIndex = 0
    var currency = ""
    var isDataLoading:Bool=false
    private let refreshControl = UIRefreshControl()
    var user_purchase_modeid = "1"
    
    ///Awaiting
    var AwaitingName = [String]()
    var AwaitingItemid = [String]()
    var AwaitingquotationId = [String]()
    var AwaitingUnit = [String]()
    var AwaitingImage = [String]()
    var AwaitingPrice = [String]()
    var AwaitingStatus = [String]()
    var AwaitingDate = [String]()
    var AwaitingCurrency = [String]()
    var Awaitinglimit = 10
    var Awaitingoffset = 0
    ///Approved
    var ApprovedName = [String]()
    var ApprovedItemid = [String]()
    var ApprovedquotationId = [String]()
    var ApprovedUnit = [String]()
    var ApprovedImage = [String]()
    var ApprovedPrice = [String]()
    var ApprovedStatus = [String]()
    var Approveddate = [String]()
    var ApprovedCurrency = [String]()
    var Approvedlimit = 10
    var ApprovedOffset = 0
    ///Rejected
    var RejectedName = [String]()
    var RejectedItemid = [String]()
    var RejectedquotationId = [String]()
    var RejectedUnit = [String]()
    var RejectedImage = [String]()
    var RejectedPrice = [String]()
    var RejectedStatus = [String]()
    var RejectedDate = [String]()
    var RejectedCurrency = [String]()
    var RejectLimit = 10
    var RejectOffset = 0
    
    var SelectedColor = UIColor(red:247/255, green:82/255, blue:85/255, alpha: 1)
    var UnSelectedColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 1)
    
    
    @IBOutlet var statusCollection: UICollectionView!
    
    @IBOutlet var statusTable: UITableView!
    @IBOutlet weak var statusView: UIView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        //        CustomLoader.instance.showLoaderView()
        //        if native.object(forKey: "user_purchase_modeid") != nil {
        //            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        //        }
        //        if user_purchase_modeid != "1"
        //        {
        //            fetchWeatherData()
        //        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusTable.tableFooterView = UIView()
        statusTable.isHidden = true
        // parse Quotation Api
        
        //        let selectedIndexPath = IndexPath(item: 0, section: 0)
        statusTable.alwaysBounceVertical = true
        statusTable.bounces  = true
        //        statusCollection.selectItem(at: selectedIndexPath, animated: true, scrollPosition: .left)
        // Do any additional setup after loading the view.
        
        
        // refresh Table
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            statusTable.refreshControl = refreshControl
        } else {
            statusTable.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        // Fetch Weather Data
        if native.object(forKey: "user_purchase_modeid") != nil {
            user_purchase_modeid = native.string(forKey: "user_purchase_modeid")!
        }
        if user_purchase_modeid != "1"
        {
            fetchWeatherData()
        }else
        {
            CustomLoader.instance.hideLoaderView()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    private func fetchWeatherData() {
        CustomLoader.instance.hideLoaderView()
        UIApplication.shared.endIgnoringInteractionEvents()
        if ActiveIndex == 0
        {
            self.AwaitingName.removeAll()
            self.AwaitingUnit.removeAll()
            self.AwaitingImage.removeAll()
            self.AwaitingItemid.removeAll()
            self.AwaitingPrice.removeAll()
            self.AwaitingquotationId.removeAll()
            self.AwaitingStatus.removeAll()
            self.Awaitinglimit = 10
            self.Awaitingoffset = 0
            self.parseAwaiting(limit: self.Awaitinglimit, offset: self.Awaitingoffset)
            
            
        }
        else if ActiveIndex == 1
        {
            self.ApprovedName.removeAll()
            self.ApprovedUnit.removeAll()
            self.ApprovedImage.removeAll()
            self.ApprovedItemid.removeAll()
            self.ApprovedPrice.removeAll()
            self.ApprovedquotationId.removeAll()
            self.ApprovedStatus.removeAll()
            self.Approvedlimit = 10
            self.ApprovedOffset = 0
            self.parseApproved(limit: self.Approvedlimit, offset: self.ApprovedOffset)
            
        }
        else if ActiveIndex == 2
        {
            self.RejectedName.removeAll()
            self.RejectedUnit.removeAll()
            self.RejectedImage.removeAll()
            self.RejectedItemid.removeAll()
            self.RejectedPrice.removeAll()
            self.RejectedquotationId.removeAll()
            self.RejectedStatus.removeAll()
            self.RejectLimit = 10
            self.RejectOffset = 0
            self.parseRejected(limit: self.RejectLimit, offset: self.RejectOffset)
            
        }
        
        
        self.updateView()
        
    }
    
    func updateView()
    {
        refreshControl.tintColor = UIColor(red:0.225, green:0.252, blue:0.85, alpha:1.0)
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching  Data ...")
        //        refreshControl.endRefreshing()
        //        statusTable.isHidden = true
        
    }
    
    /// UI table status
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if ActiveIndex == 0{
            debugPrint(AwaitingName.count , "87")
            if AwaitingItemid.count == 0
            {
                statusTable.showEmptyListMessage("No Data found")
            }
            else
            {
                statusTable.showEmptyListMessage("")
            }
            return AwaitingUnit.count
        }
        else if ActiveIndex == 1
        {
            if ApprovedItemid.count == 0
            {
                statusTable.showEmptyListMessage("No Data found")
            }
            else
            {
                statusTable.showEmptyListMessage("")
            }
            return ApprovedUnit.count
            
        }
        else
        {
            if RejectedItemid.count == 0
            {
                statusTable.showEmptyListMessage("No Data found")
            }
            else
            {
                statusTable.showEmptyListMessage("")
            }
            return RejectedUnit.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ActiveIndex == 0
        {
            if AwaitingStatus[indexPath.row] == "Seller Approved"
            {
                native.set("true", forKey: "sellerApproved")
            }
            else
            {
                native.set("false", forKey: "sellerApproved")
            }
            native.set(self.AwaitingItemid[indexPath.row], forKey: "quotationid")
            native.set("Awaiting", forKey: "quotationStatus")
            native.synchronize()
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "quotationview"))! as UIViewController
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
        else if ActiveIndex == 1
        {
            native.set(self.ApprovedItemid[indexPath.row], forKey: "quotationid")
            native.set("Buyer%20Approved", forKey: "quotationStatus")
            native.synchronize()
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "quotationview"))! as UIViewController
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
        else if ActiveIndex == 2
        {
            native.set(self.RejectedItemid[indexPath.row], forKey: "quotationid")
            native.set("Rejected", forKey: "quotationStatus")
            native.synchronize()
            let rootPage = (self.storyboard?.instantiateViewController(withIdentifier: "quotationview"))! as UIViewController
            self.navigationController?.pushViewController(rootPage, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var ReuseCell = UITableViewCell()
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myordercell2") as! MyOrderTableViewCell2
        if ActiveIndex == 0 && self.AwaitingItemid.count > 0
        {
            cell.totalunits.text = "Qty: \(self.AwaitingUnit[indexPath.row])"
            cell.productname.text = self.AwaitingName[indexPath.row]
            cell.price.text = "\(self.AwaitingCurrency[indexPath.row])\(self.AwaitingPrice[indexPath.row])"
            cell.date.text = self.AwaitingDate[indexPath.row]
            if self.AwaitingImage.count > indexPath.row
            {
                var imageUrl = AwaitingImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                
                let url = NSURL(string:imageUrl)
                if url != nil
                {
                    DispatchQueue.main.async {
                    let url = NSURL(string:imageUrl)
                    let imageView = SDAnimatedImageView()
                    let animatedImage = SDAnimatedImage(named: "image.gif")
                    cell.icon.image = animatedImage
                    }
                }
            }
            if AwaitingStatus[indexPath.row] == "Seller Approved"
            {
                cell.sellerApproved.isHidden = false
            }
            else
            {
                cell.sellerApproved.isHidden = true
            }
            ReuseCell = cell
        }
        else if ActiveIndex == 1 && self.ApprovedItemid.count > 0
        {
            debugPrint(indexPath.row)
            cell.sellerApproved.isHidden = true
            cell.totalunits.text = "Qty: \(self.ApprovedUnit[indexPath.row])"
            cell.productname.text = self.ApprovedName[indexPath.row]
            //            cell.price.text = "\(self.ApprovedCurrency[indexPath.row])\(self.ApprovedPrice[indexPath.row])"
            cell.date.text = self.Approveddate[indexPath.row]
            if self.ApprovedImage.count > indexPath.row
            {var imageUrl = ApprovedImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                DispatchQueue.main.async {
                let url = NSURL(string:imageUrl)
                let imageView = SDAnimatedImageView()
                let animatedImage = SDAnimatedImage(named: "image.gif")
                cell.icon.image = animatedImage
                }
            }
            ReuseCell = cell
        }
        else if ActiveIndex == 2 && self.RejectedItemid.count > 0
        {
            cell.sellerApproved.isHidden = true
            cell.totalunits.text = "Qty: \(self.RejectedUnit[indexPath.row])"
            cell.productname.text = self.RejectedName[indexPath.row]
            cell.price.text = "\(self.RejectedCurrency[indexPath.row])\(self.RejectedPrice[indexPath.row])"
            cell.date.text = self.RejectedDate[indexPath.row]
            if self.RejectedImage.count > indexPath.row
            {
                var imageUrl = RejectedImage[indexPath.row] as! String
                imageUrl = imageUrl.replacingOccurrences(of: " ", with: "%20")
                DispatchQueue.main.async {
                let url = NSURL(string:imageUrl)
                let imageView = SDAnimatedImageView()
                let animatedImage = SDAnimatedImage(named: "image.gif")
                cell.icon.image = animatedImage
                }
            }
            ReuseCell = cell
        }
        
        return cell
    }
    
    
    ///status collection
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        ActiveIndex = indexPath.row
        
        self.statusCollection.reloadData()
        self.statusTable.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var ReuseCell = UICollectionViewCell()
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "qoutationcell", for: indexPath) as! OrdersandQoutationCollectionViewCell
        if indexPath.row == 0
        {
            cell.title.text = "Awaiting"
            cell.count.text = String(self.AwaitingName.count)
            
            //                cell.icon.image = UIImage(named: "Awaiting")
            
        }
        else if indexPath.row == 1
        {
            cell.title.text = "Approved"
            cell.count.text = String(self.ApprovedName.count)
            //                cell.icon.image = UIImage(named: "Approved")
        }
        else if indexPath.row == 2
        {
            cell.title.text = "Rejected"
            
            cell.count.text = String(self.RejectedName.count)
            //                cell.icon.image = UIImage(named: "cancel-1")
        }
        if ActiveIndex == indexPath.row
        {
            cell.isSelected = true
            cell.title.textColor = SelectedColor
            cell.count.textColor = SelectedColor
        }
        else
        {
            cell.title.textColor = UnSelectedColor
            cell.count.textColor = UnSelectedColor
        }
        
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 80 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        debugPrint("gfvhg", leftInset, rightInset)
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
    }
    
    
    ////parse all qoutation data
    func parseAwaiting(limit: Int, offset: Int)
    {
        Allcount = 0
        Awaitingcount = 0
        Approvedcount = 0
        Rejectedcount = 0
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        
        if Token != nil
        {
            //            let usertype = native.string(forKey: "usertype")!
            //            let userchange = native.string(forKey: "changetype")!
            let b2burl = native.string(forKey: "b2burl")!
            var url = ""
            
            //            if usertype == "2"
            //            {
            //
            //                    url = "\(b2burl)/quotation/seller_quotation/?limit=40&offset=0&status_ui=Awaiting"
            //
            //            }
            //            else if usertype == "1"
            //            {
            
            url = "\(b2burl)/quotation/buyer_quotation/?limit=\(limit)&offset=\(offset)&status_ui=Awaiting"
            
            
            //            }
            debugPrint(url)
            var a = URLRequest(url: NSURL(string: url)! as URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response awaiting: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else if (dict1["status"] as? String)! == "success"
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    if innerdict[i]["status_ui"] as! String == "Rejected"
                                    {
                                        
                                        debugPrint(innerdict[i]["status_ui"])
                                        self.Rejectedcount = innerdict[i]["quotation_count"] as! Int
                                    }
                                        
                                    else if innerdict[i]["status_ui"] as! String == "Seller Approved"
                                    {
                                        //                                    if usertype == "2"
                                        //                                    {
                                        debugPrint(innerdict[i]["status_ui"])
                                        self.Approvedcount = (innerdict[i]["quotation_count"] as? Int)!
                                        //                                    }
                                    }
                                    else if innerdict[i]["status_ui"] as! String == "Buyer Approved"
                                    {
                                        //                                    if usertype != "2"
                                        //                                    {
                                        debugPrint(innerdict[i]["status_ui"], "79874958")
                                        self.Approvedcount = (innerdict[i]["quotation_count"] as? Int)!
                                        //                                    }
                                    }
                                    else if innerdict[i]["status_ui"] as! String == "Awaiting"
                                    {
                                        debugPrint(innerdict[i]["status_ui"])
                                        self.Awaitingcount = (innerdict[i]["quotation_count"] as? Int)!
                                    }
                                    
                                    
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    if innerdict1[i]["currency_symbol"] is NSNull
                                    {
                                        self.AwaitingCurrency.append("")
                                    }
                                    else
                                    {
                                        self.AwaitingCurrency.append(innerdict1[i]["currency_symbol"] as! String)
                                    }
                                    if innerdict1[i]["quotation_date"] is NSNull
                                    {
                                        self.AwaitingDate.append("")
                                    }
                                    else
                                    {
                                        self.AwaitingDate.append(innerdict1[i]["quotation_date"] as! String)
                                    }
                                    self.AwaitingItemid.append("\((innerdict1[i]["quotationid"] as? Int)!)")
                                    let quotation_data = innerdict1[i]["quotation_item_data"] as! [AnyObject]
                                    self.Awaitingcount = quotation_data.count
                                    var totalunits = 0
                                    var totalprice = 0
                                    
                                    //                    for var j in 0..<quotation_data.count
                                    //                    {
                                    if quotation_data.count > 0
                                    {
                                        
                                        totalunits = totalunits + Int(quotation_data[0]["quantity"] as! Int)
                                        totalprice = totalprice + Int(quotation_data[0]["requested_price"] as! Int)
                                        if quotation_data[0]["product_image_url"] is NSNull
                                        {
                                            if innerdict1[i]["parent_product_image_url"] is NSNull
                                            {}
                                            else
                                            {
                                                if let image = innerdict1[i]["parent_product_image_url"] as? AnyObject{
                                                    if image["200"] is NSNull
                                                    {
                                                        self.AwaitingStatus.append("")
                                                    }
                                                    else
                                                    {
                                                        let imagedata = image["200"] as! [AnyObject]
                                                        if imagedata.count > 0
                                                        {
                                                            var image = imagedata[0] as! String
                                                            self.AwaitingImage.append(image)
                                                        }
                                                    }}
                                                
                                            }
                                        }
                                        else{
                                            if let images = quotation_data[0]["product_image_url"] as? AnyObject{
                                                if images["200"] is NSNull
                                                {
                                                    self.AwaitingStatus.append("")
                                                }
                                                else
                                                {
                                                    let imagedata = images["200"] as! [AnyObject]
                                                    if imagedata.count > 0
                                                    {
                                                        var image = imagedata[0] as! String
                                                        self.AwaitingImage.append(image)
                                                    }}
                                            }
                                        }
                                        if quotation_data[0]["quotation_item_status_ui"] is NSNull
                                        {
                                            
                                        }
                                        else
                                        { self.AwaitingStatus.append((quotation_data[0]["quotation_item_status_ui"] as? String)!)
                                        }
                                        self.AwaitingName.append(innerdict1[i]["parent_product_name"] as! String)
                                        self.AwaitingUnit.append(String(totalunits))
                                        
                                        self.AwaitingPrice.append(String(totalprice))
                                        
                                        self.AwaitingquotationId.append(String(innerdict1[i]["quotationid"] as! Int))
                                    }   } }
                        }
                        //            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                        // Your code with delay
                        UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        self.statusTable.isHidden = false
                        self.statusCollection.reloadData()
                        self.statusTable.reloadData()
                        self.refreshControl.endRefreshing()
                        
                        //            }
                    }
                    
                }}
            
        }
    }
    
    
    ////parse all qoutation data
    func parseApproved(limit: Int, offset: Int)
    {
        
        Allcount = 0
        Awaitingcount = 0
        Approvedcount = 0
        Rejectedcount = 0
        let Token = self.native.string(forKey: "Token")!
        //        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "usertype")!
            //            let userchange = native.string(forKey: "changetype")!
            let b2burl = native.string(forKey: "b2burl")!
            
            var validateUrl = ""
            
            //            if usertype == "2"
            //            {
            //
            //                validateUrl =  "\(b2burl)/quotation/seller_quotation/?limit=40&offset=0&status_ui=Seller Approved"
            //
            //                validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            ////                debugPrint("========",validateUrl)
            //
            //
            //            }
            //            else if usertype == "1"
            //            {
            
            validateUrl =  "\(b2burl)/quotation/buyer_quotation/?limit=\(limit)&offset=\(offset)&status_ui=Buyer Approved"
            
            validateUrl = validateUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            //                debugPrint("========",validateUrl)
            
            
            
            //            }
            var a = URLRequest(url: NSURL(string: validateUrl) as! URL)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response approved: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    if innerdict[i]["status_ui"] as! String == "Rejected"
                                    {
                                        
                                        debugPrint(innerdict[i]["status_ui"])
                                        self.Rejectedcount = (innerdict[i]["quotation_count"] as? Int)!
                                    }
                                        
                                        
                                    else if innerdict[i]["status_ui"] as! String == "Seller Approved"
                                    {
                                        //                                    if usertype == "2"
                                        //                                    {
                                        debugPrint(innerdict[i]["status_ui"])
                                        self.Approvedcount = (innerdict[i]["quotation_count"] as? Int)!
                                        //                                }
                                    }
                                    else if innerdict[i]["status_ui"] as! String == "Buyer Approved"
                                    {
                                        //                                if usertype != "2"
                                        //                                {
                                        debugPrint(innerdict[i]["status_ui"], "79874958")
                                        self.Approvedcount = innerdict[i]["quotation_count"] as! Int
                                        //                                }
                                    }
                                    else if innerdict[i]["status_ui"] as! String == "Awaiting"
                                    {
                                        debugPrint(innerdict[i]["status_ui"])
                                        //                                    self.Awaitingcount = (innerdict[i]["quotation_count"] as? Int)!
                                    }
                                    
                                    
                                }
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                
                                for var i in 0..<innerdict1.count
                                {
                                    if innerdict1[i]["currency_symbol"] is NSNull
                                    {
                                        self.AwaitingCurrency.append("")
                                    }
                                    else
                                    {
                                        self.AwaitingCurrency.append(innerdict1[i]["currency_symbol"] as! String)
                                    }
                                    if innerdict1[i]["quotation_date"] is NSNull
                                    {
                                        self.Approveddate.append("")
                                    }
                                    else
                                    {
                                        self.Approveddate.append(innerdict1[i]["quotation_date"] as! String)
                                    }
                                    self.ApprovedItemid.append("\((innerdict1[i]["quotationid"] as? Int)!)")
                                    let quotation_data = innerdict1[i]["quotation_item_data"] as! [AnyObject]
                                    self.Approvedcount = quotation_data.count
                                    var totalunits = 0
                                    var totalprice = 0
                                    
                                    //                            for var j in 0..<quotation_data.count
                                    
                                    if quotation_data.count>0 {
                                        
                                        totalunits = totalunits + Int(quotation_data[0]["quantity"] as! Int)
                                        totalprice = totalprice + Int(quotation_data[0]["requested_price"] as! Int)
                                        if quotation_data[0]["product_image_url"] is NSNull
                                        {
                                            if innerdict1[i]["parent_product_image_url"] is NSNull
                                            {}
                                            else
                                            {
                                                let image = innerdict1[i]["parent_product_image_url"] as! AnyObject
                                                if image["200"] is NSNull
                                                {
                                                    self.ApprovedImage.append("")
                                                }
                                                else
                                                {
                                                    let imagedata = image["200"] as! [AnyObject]
                                                    if imagedata.count > 0
                                                    {
                                                        var image = imagedata[0] as! String
                                                        self.ApprovedImage.append(image)
                                                    }
                                                }
                                                
                                            }
                                        }
                                        else
                                        {
                                            if let images = quotation_data[0]["product_image_url"] as? AnyObject
                                            {
                                                if images["200"] is NSNull
                                                {
                                                    self.ApprovedImage.append("")
                                                }
                                                else
                                                {
                                                    let imagedata = images["200"] as! [AnyObject]
                                                    if imagedata.count > 0
                                                    {
                                                        var image = imagedata[0] as! String
                                                        self.ApprovedImage.append(image)
                                                    }
                                                }
                                            }
                                        }
                                        
                                        
                                    }
                                    self.ApprovedName.append(innerdict1[i]["parent_product_name"] as! String)
                                    self.ApprovedUnit.append(String(totalunits))
                                    self.ApprovedPrice.append(String(totalprice))
                                    self.ApprovedquotationId.append(String(innerdict1[i]["quotationid"] as! Int))
                                    
                                }
                            }
                        }
                        UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                        // Your code with delay
                        self.statusTable.isHidden = false
                        self.statusCollection.reloadData()
                        self.statusTable.reloadData()
                        self.refreshControl.endRefreshing()
                        
                        //            }
                    }
                    
                }}
            //
        }
        
    }
    
    
    ////parse all qoutation data
    func parseRejected(limit: Int, offset: Int)
    {
        Awaitingcount = 0
        Approvedcount = 0
        Rejectedcount = 0
        let Token = self.native.string(forKey: "Token")!
        debugPrint("token", Token)
        if Token != nil
        {
            //            let usertype = native.string(forKey: "usertype")!
            //            let userchange = native.string(forKey: "changetype")!
            let b2burl = native.string(forKey: "b2burl")!
            
            var url = ""
            
            //            if usertype == "2"
            //            {
            //
            //                    url = "\(b2burl)/quotation/seller_quotation/?limit=40&offset=0&status_ui=Rejected"
            //
            //            }
            //            else if usertype == "1"
            //            {
            
            url = "\(b2burl)/quotation/buyer_quotation/?limit=\(limit)&offset=\(offset)&status_ui=Rejected"
            
            //            }
            var a = URLRequest(url: NSURL(string: url) as! URL)
            debugPrint(a)
            a.allHTTPHeaderFields = ["Content-Type": "application/json","Authorization": "Token \(Token)"]
            
            Alamofire.request(a as URLRequestConvertible).responseJSON { response in
                //                                debugdebugPrint(response)
                debugPrint("response rejected: \(response)")
                let result = response.result
                switch response.result
                {
                case .failure(let error):
                    //                debugPrint(error)
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let err = error as? URLError, err.code == .notConnectedToInternet {
                        // Your device does not have internet connection!
                        debugPrint("Your device does not have internet connection!")
                        self.view.makeToast("Your device does not have internet connection!", duration: 2.0, position: .center)
                        debugPrint(err)
                    }
                    else if error._code == NSURLErrorTimedOut {
                        debugPrint("Request timeout!")
                        self.view.makeToast("Please try again later", duration: 2.0, position: .center)
                    }else {
                        // other failures
                        self.view.makeToast("Please try again later", duration: 2.0, position: .bottom)
                    }
                case .success:
                    CustomLoader.instance.hideLoaderView()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    if let dict1 = result.value as? Dictionary<String, AnyObject>{
                        if let invalidToken = dict1["detail"]{
                            if invalidToken as! String  == "Invalid token."
                            { // Create the alert controller
                                self.view.makeToast("Your session has expired! Please log in again.", duration: 2.0, position: .bottom)
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let redViewController = mainStoryBoard.instantiateViewController(withIdentifier: "root") as! UIViewController
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.window?.rootViewController = redViewController
                            }
                        }
                        else
                        {
                            if dict1["count_data"] is NSNull
                            {}
                            else
                            {
                                let innerdict = dict1["count_data"] as! [AnyObject]
                                for var i in 0..<innerdict.count
                                {
                                    if innerdict[i]["status_ui"] as! String == "Rejected"
                                    {
                                        
                                        debugPrint(innerdict[i]["status_ui"])
                                        self.Rejectedcount = (innerdict[i]["quotation_count"] as? Int)!
                                    }
                                        
                                    else if innerdict[i]["status_ui"] as! String == "Seller Approved"
                                    {
                                        //                                    if usertype == "2"
                                        //                                    {
                                        debugPrint(innerdict[i]["status_ui"])
                                        self.Approvedcount = (innerdict[i]["quotation_count"] as? Int)!
                                        //                                    }
                                    }
                                    else if innerdict[i]["status_ui"] as! String == "Buyer Approved"
                                    {
                                        //                                    if usertype != "2"
                                        //                                    {
                                        debugPrint(innerdict[i]["status_ui"], "79874958")
                                        self.Approvedcount = (innerdict[i]["quotation_count"] as? Int)!
                                        //                                    }
                                    }
                                    else if innerdict[i]["status_ui"] as! String == "Awaiting"
                                    {
                                        debugPrint(innerdict[i]["status_ui"])
                                        //                                    self.Awaitingcount = (innerdict[i]["quotation_count"] as? Int)!
                                    }
                                    
                                    
                                }
                                
                                let innerdict1 = dict1["data"] as! [AnyObject]
                                for var i in 0..<innerdict1.count
                                {
                                    if innerdict1[i]["currency_symbol"] is NSNull
                                    {
                                        self.RejectedCurrency.append("")
                                    }
                                    else
                                    {
                                        self.RejectedCurrency.append(innerdict1[i]["currency_symbol"] as! String)
                                    }
                                    if innerdict1[i]["quotation_date"] is NSNull
                                    {
                                        self.RejectedDate.append("")
                                    }
                                    else
                                    {
                                        self.RejectedDate.append(innerdict1[i]["quotation_date"] as! String)
                                    }
                                    self.RejectedItemid.append("\((innerdict1[i]["quotationid"] as? Int)!)")
                                    let quotation_data = innerdict1[i]["quotation_item_data"] as! [AnyObject]
                                    self.Rejectedcount = quotation_data.count
                                    var totalunits = 0
                                    var totalprice = 0
                                    
                                    //                            for var j in 0..<quotation_data.count
                                    
                                    if quotation_data.count>0
                                    {
                                        
                                        totalunits = totalunits + Int(quotation_data[0]["quantity"] as! Int)
                                        totalprice = totalprice + Int(quotation_data[0]["requested_price"] as! Int)
                                        if quotation_data[0]["product_image_url"] is NSNull
                                        {
                                            if innerdict1[i]["parent_product_image_url"] is NSNull
                                            {}
                                            else
                                            {
                                                if let image = innerdict1[i]["parent_product_image_url"] as? AnyObject
                                                {
                                                    if image["200"] is NSNull
                                                    {
                                                        self.RejectedImage.append("")
                                                    }
                                                    else
                                                    {
                                                        let imagedata = image["200"] as! [AnyObject]
                                                        if imagedata.count > 0
                                                        {
                                                            var image = imagedata[0] as! String
                                                            self.RejectedImage.append(image)
                                                        }
                                                    }}
                                                
                                            }
                                        }
                                        else
                                        {
                                            if let images = quotation_data[0]["product_image_url"] as? AnyObject
                                            {
                                                if images["200"] is NSNull
                                                {
                                                    self.RejectedImage.append("")
                                                }
                                                else
                                                {
                                                    let imagedata = images["200"] as! [AnyObject]
                                                    if imagedata.count > 0
                                                    {
                                                        var image = imagedata[0] as! String
                                                        self.RejectedImage.append(image)
                                                    }
                                                }}
                                        }
                                        
                                    }
                                    self.RejectedName.append(innerdict1[i]["parent_product_name"] as! String)
                                    self.RejectedUnit.append(String(totalunits))
                                    self.RejectedPrice.append(String(totalprice))
                                    self.RejectedquotationId.append(String(innerdict1[i]["quotationid"] as! Int))
                                    
                                }
                            }
                        }
                        UIApplication.shared.endIgnoringInteractionEvents()
                        CustomLoader.instance.hideLoaderView()
                        //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { // change 2 to desired number of seconds
                        // Your code with delay
                        self.statusTable.isHidden = false
                        self.statusCollection.reloadData()
                        self.statusTable.reloadData()
                        self.refreshControl.endRefreshing()
                        
                        //            }
                    }
                }
                //
            }}
        
    }
    
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        debugPrint("scrollViewWillBeginDragging")
        isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        debugPrint("scrollViewDidEndDecelerating")
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        debugPrint("scrollViewDidEndDragging")
        if ((statusTable.contentOffset.y + statusTable.frame.size.height) >= statusTable.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                if ActiveIndex == 0
                {
                    debugPrint(self.AwaitingItemid.count, self.Awaitingoffset)
                    if self.AwaitingItemid.count >= self.Awaitingoffset
                    {
                        self.Awaitingoffset=self.Awaitingoffset+10
                        CustomLoader.instance.showLoaderView()
                        self.parseAwaiting(limit: Awaitinglimit, offset: Awaitingoffset)
                        //                loadCallLogData(offset: self.offset, limit: self.limit)
                    }
                }
                else if ActiveIndex == 1
                {
                    if self.ApprovedItemid.count >= self.ApprovedOffset
                    {
                        self.ApprovedOffset=self.ApprovedOffset + 10
                        parseApproved(limit: Approvedlimit, offset: ApprovedOffset)
                    }
                }
                else if ActiveIndex == 2
                {
                    if self.RejectedItemid.count >= self.RejectOffset
                    {
                        self.RejectOffset=self.RejectOffset + 10
                        
                        
                        
                        parseRejected(limit: RejectLimit, offset: RejectOffset)
                    }
                }
                
                
                
            }
        }
        
        
    }
    
    
    
}
